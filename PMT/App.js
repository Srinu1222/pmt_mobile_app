import React, { useState, useMemo } from 'react';
// Scale: 
// 1. For fontSize:
// --> 1 Pts = 0.18% * hp

// 2. For Image Size:
// --> width: 1 Pts = 0.32% * wp
// --> height: 1 Pts = 0.18% * hp
import {
  StyleSheet,
  Button,
  SafeAreaView,
  Platform,
  View,
  Text,
  TouchableOpacity,
  StatusBar,
} from 'react-native';
import { Provider } from 'react-redux';
import SplashScreen from './components/SplashScreen';
import Navigator from './components/navigator';
import ReduxThunk from 'redux-thunk';
import reducers from './reducers/index';
import { createStore, applyMiddleware, compose } from 'redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { AuthContext } from './context';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { Provider as PaperProvider } from 'react-native-paper';
import { ApplicationProvider, IconRegistry } from '@ui-kitten/components';
import * as eva from '@eva-design/eva';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import { default as mapping } from './mapping.json';
import NetworkLogger from 'react-native-network-logger';

const store = createStore(reducers, {}, compose(applyMiddleware(ReduxThunk)));

const App = () => {

  const [loading, setLoading] = useState(true);
  const [token, setToken] = useState(null);
  //("app token",token);
  const changeLoading = () => {
    setLoading(false);
  };

  const authContext = useMemo( () => {
    return {
      signIn: token => {
        setToken(token);
      },
      signOut: () => {
        setToken(null);
        AsyncStorage.removeItem('userDetails');
        setLoading(true);
      },
      getToken: () => {
        return token;
      }
    };
  }, []);

  const getToken = async () => {
    try {
      const userDetails = await AsyncStorage.getItem('userDetails');
      const value = JSON.parse(userDetails);
      setToken(value.key);
    } catch (e) {
      setToken(null);
     
    }
  };

  if (loading) {
    {
      getToken();
    }
    return <SplashScreen changeLoading={changeLoading} />;
  }

  return (
    <Provider store={store}>
      <IconRegistry icons={EvaIconsPack} />
      <ApplicationProvider
        {...eva}
        theme={eva.light}
        customMapping={mapping} // <-- apply mapping
      >
        <PaperProvider>
          <AuthContext.Provider value={authContext}>
         
          <Navigator token={token}/>
        

          </AuthContext.Provider>
        </PaperProvider>
      </ApplicationProvider>
    </Provider>
  );

};
const styles=StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor:  'white',
      paddingTop: Platform.OS === 'android' ? 25 : 0,
    },
    header: {
      flexDirection: 'row',
    },
    navButton: {
      flex: 1,
    },
    backButtonText: {
      color:  'black',
      paddingHorizontal: 20,
      fontSize: 30,
      fontWeight: 'bold',
    },
    title: {
      flex: 5,
      color:  'black',
      textAlign: 'center',
      padding: 10,
      fontSize: 18,
      fontWeight: 'bold',
    },
    bottomView: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingHorizontal: 30,
    },
  });
export default App;
