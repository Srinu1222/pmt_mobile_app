import {
    GET_DOCS_LOADING,
    GET_DOCS_SUCCESS,
    GET_DOCS_FAILURE,
    GET_DOCS_RESET
} from '../types';
import axios from 'axios';
import config from "../../config";


export const getDocs = (token, is_image=false) => dispatch => {

    dispatch({ type: GET_DOCS_LOADING, payload: null });
   
    axios.get('https://pmt.safearth-api.in/api/get_company_projects_databank/', {
      headers: {
        Authorization: 'Token ' + token,
      },
      params: {
        is_image: is_image
      }
    })
        .then((result) => {
            dispatch({ type: GET_DOCS_SUCCESS, payload: result.data });
        })
        .catch(err => {
            //err)
            dispatch({ type: GET_DOCS_FAILURE, payload: { message: err.message } });
        })
};