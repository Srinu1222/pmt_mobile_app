import {
    GET_TASK_LIST_LOADING,
    GET_TASK_LIST_SUCCESS,
    GET_TASK_LIST_FAILURE
} from '../../actions/types';
import axios from 'axios';
import config from '../../config'


export const getTaskList = (token) => dispatch => {

    dispatch({ type: GET_TASK_LIST_LOADING, payload: null });
    axios.get('https://pmt.safearth-api.in/api/'+'get_daily_tasks/', {
        headers: {
            Authorization: 'Token ' + token
        }
    })
        .then(result => {
            dispatch({ type: GET_TASK_LIST_SUCCESS, payload: result.data });
        })
        .catch(err => {
            //err.response)
            dispatch({ type: GET_TASK_LIST_FAILURE, payload: { message: err.message } });
        });
};