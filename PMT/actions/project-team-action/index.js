import {
    GET_TEAMS_LOADING,
    GET_TEAMS_SUCCESS,
    GET_TEAMS_FAILURE
} from '../project/types';
import axios from 'axios';
import { getItem } from '../../SecureStorage';

export const getTeams = () => dispatch => {

    let userData;

    if (getItem('userData')) {
        userData = JSON.parse(getItem('userData'));
    }
    dispatch({ type: GET_TEAMS_LOADING, payload: null });
    axios.get(`${process.env.REACT_APP_BASE_URL}get_project_team/`, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        }
    })
        .then(result => {
            dispatch({ type: GET_TEAMS_SUCCESS, payload: result.data });
        })
        .catch(err => {
            dispatch({ type: GET_TEAMS_FAILURE, payload: { message: err.message } });
        })
};
