import {
    GET_COMMENT_LOADING,
    GET_COMMENT_SUCCESS,
    GET_COMMENT_FAILURE,
    POST_COMMENT_LOADING,
    POST_COMMENT_SUCCESS,
    POST_COMMENT_FAILURE,
    GET_COMMENT_FILE_LOADING,
    GET_COMMENT_FILE_SUCCESS,
    GET_COMMENT_FILE_FAILURE,
    GET_UPDATE_SUCCESS
} from '../types';
import axios from 'axios';
import config from '../../config'
import Toast from "react-native-toast-message";


export const getComments = (eventId, preRequisiteId, comment_belongs_to, token, isSubContractor) => dispatch => {

    let params;

    if (comment_belongs_to === 'pre_requisites') {
        params = {
            event_id: eventId,
            pre_requisite_id: preRequisiteId,
            is_sub_contractor: isSubContractor
        }
    } else {
        params = {
            event_id: eventId,
            checklist_id: preRequisiteId,
            is_sub_contractor: isSubContractor
        }
    }

    dispatch({ type: GET_COMMENT_LOADING, payload: null });

    axios.get(config.API_URL+'get_comments/', {
        headers: {
            Authorization: 'Token ' + token
        },
        params: params
    })
        .then(result => {
            //(result.data)
            dispatch({ type: GET_COMMENT_SUCCESS, payload: result.data });
        })
        .catch(err => {
            //(err.response)
            dispatch({ type: GET_COMMENT_FAILURE, payload: { message: err.message } });
        })
}

export const getCommentFile = ({ eventId, preRequisiteId, commentId, attachment }) => dispatch => {


    let userData;

    if (getItem('userData')) {
        userData = JSON.parse(getItem('userData'));
    }
    const isSubContractor = JSON.parse(getItem('isSubContractor'))

    dispatch({ type: GET_COMMENT_FILE_LOADING, payload: null });

    axios.get(`${process.env.REACT_APP_BASE_URL}get_comment_file/`, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        },
        params: {
            event_id: eventId,
            pre_requisite_id: preRequisiteId,
            comment_id: commentId,
            attachment: attachment,
            is_sub_contractor: isSubContractor
        }
    })
        .then(result => {
            dispatch({ type: GET_COMMENT_FILE_SUCCESS, payload: result.data });
        })
        .catch(err => {
            dispatch({ type: GET_COMMENT_FILE_FAILURE, payload: { message: err.message } });
        })
}

export const postComments = (data, token, isSubContractor) => dispatch => {

    axios.post(config.API_URL+`add_comment/`, data, {
        headers: {
            Authorization: 'Token ' + token
        },
        params: {
            is_sub_contractor: isSubContractor
        }
    })
        .then(result => {
            dispatch({ type: POST_COMMENT_SUCCESS, payload: result.data });
            Toast.show({
                text1: 'New Comment Added!',
                type: 'success',
                autoHide: true,
                position: 'bottom',
                visibilityTime: 1000
              })
        })
        .catch(err => {
            //(err.response);
            return '';
        })
}

export const deleteComment = (data, token, isSubContractor) => dispatch => {

    axios.post(config.API_URL+`delete_comment/`, data, {
        headers: {
            Authorization: 'Token ' + token
        },
        params: {
            is_sub_contractor: isSubContractor
        }
    })
        .then(result => {
            dispatch({ type: GET_UPDATE_SUCCESS, payload: null });
            Toast.show({
                text1: 'Comment Deleted!',
                type: 'success',
                autoHide: true,
                position: 'bottom',
                visibilityTime: 1000
              })
        })
        .catch(err => {
            return message.error('Error while deleting comment!');
            dispatch({ type: POST_COMMENT_FAILURE, payload: { message: err.message } });
        })
}