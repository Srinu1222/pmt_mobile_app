import {
    GET_PROFILE_LOADING,
    GET_PROFILE_SUCCESS,
    GET_PROFILE_FAILURE,
    GET_UPDATE_SUCCESS
} from '../types';
import axios from 'axios';

import notifyMessage from '../../ToastMessages';

export const getCompanyProfile = () => dispatch => {

    dispatch({ type: GET_PROFILE_LOADING, payload: null });

    axios.get(`${process.env.REACT_APP_BASE_URL}get_company_profile/`, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        }
    })
        .then(result => {
            dispatch({ type: GET_PROFILE_SUCCESS, payload: result.data });
        })
        .catch(err => {
            dispatch({ type: GET_PROFILE_FAILURE, payload: { message: err.message } });
        })
}

export const editProject = (data) => dispatch => {

    axios.post(`${process.env.REACT_APP_BASE_URL}edit_project/`, data, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        }
    })
        .then(result => {
            dispatch({ type: GET_UPDATE_SUCCESS, payload: null });
        })
        .catch(err => {
        })
}

export const postUpdateCompanyProfile = (data) => dispatch => {

    axios.post(`${process.env.REACT_APP_BASE_URL}add_company_profile/`, data, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        }
    })
        .then(result => {
            dispatch({ type: GET_UPDATE_SUCCESS, payload: null });
            notifyMessage('Profile is updated!');
        })
        .catch(err => {
            notifyMessage('Error while updating profile!');
        })
}

export const postBasicInfo = (data) => dispatch => {

    axios.post(`${process.env.REACT_APP_BASE_URL}add_basic_info/`, data, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        }
    })
        .then(result => {
            dispatch({ type: GET_UPDATE_SUCCESS, payload: null });
            notifyMessage('Basic Info is updated!');
        })
        .catch(err => {
            notifyMessage('Error while updating baisc info!');
        })
}

export const postAward = (data) => dispatch => {
    axios.post(`${process.env.REACT_APP_BASE_URL}add_award/`, data, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        }
    })
        .then(result => {
            dispatch({ type: GET_UPDATE_SUCCESS, payload: null });
            notifyMessage('Award is added!');
        })
        .catch(err => {
            notifyMessage('Error while adding award!');
        })
}

export const postKeyPersonal = (data) => dispatch => {

    axios.post(`${process.env.REACT_APP_BASE_URL}add_key_personnel/`, data, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        }
    })
        .then(result => {
            dispatch({ type: GET_UPDATE_SUCCESS, payload: null });
            notifyMessage('Key Personal is added!');
        })
        .catch(err => {
            notifyMessage('Error while adding key personal!');
        })
}

export const postTestimonials = (data) => dispatch => {

    axios.post(`${process.env.REACT_APP_BASE_URL}add_testimonial/`, data, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        }
    })
        .then(result => {
            dispatch({ type: GET_UPDATE_SUCCESS, payload: null });
            notifyMessage('Key Personal is added!');
        })
        .catch(err => {
            notifyMessage('Error while adding key personal!');
        })
}

export const deleteProject = (data) => dispatch => {

    axios.post(`${process.env.REACT_APP_BASE_URL}delete_project/`, data, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        }
    })
        .then(result => {
            dispatch({ type: GET_UPDATE_SUCCESS, payload: null });
            notifyMessage('Project is deleted!');
        })
        .catch(err => {
            notifyMessage('Error while deleting project!');
        })
}

export const deleteAward = (data) => dispatch => {


    axios.post(`${process.env.REACT_APP_BASE_URL}delete_award/`, data, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        }
    })
        .then(result => {
            dispatch({ type: GET_UPDATE_SUCCESS, payload: null });
            notifyMessage('Award is deleted!');
        })
        .catch(err => {
            notifyMessage('Error while deleting award!');
        })
}

export const deleteKeyPersonnel = (data) => dispatch => {


    axios.post(`${process.env.REACT_APP_BASE_URL}delete_key_personnel/`, data, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        }
    })
        .then(result => {
            dispatch({ type: GET_UPDATE_SUCCESS, payload: null });
            notifyMessage('Key Personnel is deleted!');
        })
        .catch(err => {
            notifyMessage('Error while deleting key personnel!');
        })
}

export const deleteTestimonial = (data) => dispatch => {


    axios.post(`${process.env.REACT_APP_BASE_URL}delete_testimonial/`, data, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        }
    })
        .then(result => {
            dispatch({ type: GET_UPDATE_SUCCESS, payload: null });
            notifyMessage('Testimonial is deleted!');
        })
        .catch(err => {
            notifyMessage('Error while deleting key testimonial!');
        })
}

export const changePassword = (data) => dispatch => {

    axios.post(`${process.env.REACT_APP_BASE_URL}password_change/`, data, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        }
    })
        .then(result => {
            notifyMessage('Password change is successful!');
        })
        .catch(err => {
            notifyMessage('Error while changing password!');
        })
}

export const addCredits = (data) => dispatch => {

    axios.post(`${process.env.REACT_APP_BASE_URL}add_credit/`, data, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        }
    })
        .then(result => {

            let totalAmount = JSON.parse(JSON.stringify(Object.fromEntries(data)));

            const openModal = () => {
                const options = {
                    key: "rzp_test_6E9Ompahiy30dc",
                    amount: data.totalAmount * 100,
                    name: "SafEarth Marketplace",
                    order_id: result.data.order_id,
                    // callback_url: "/",
                    // redirect: true,
                    modal: {
                        ondismiss: function () {
                            if (window.location.pathname === "/") {
                                window.location.href = "/";
                            }
                        },
                    },
                    handler: function (response) {
                        let parameters = {
                            razorpay_payment_id: response.razorpay_payment_id,
                            razorpay_order_id: response.order_id,
                            razorpay_signature: response.signature,
                        };
                        const data = new FormData();

                        data.append("razorpay_payment_id", response.razorpay_payment_id);
                        data.append("razorpay_order_id", response.razorpay_order_id);
                        data.append("razorpay_signature", response.razorpay_signature);
                        data.append('credits_to_add', (totalAmount.total_amount / 100).toFixed(0));
                        data.append('invoice_id', result.data.invoice_id);

                        return new Promise((resolve, reject) => {
                            axios.defaults.headers = {
                                "Content-Type": "application/json",
                                Authorization: `Token ` + userData.authToken,
                            };
                            axios
                                .post(`${process.env.REACT_APP_BASE_URL}verify_payment/`, data)
                                .then((res) => {
                                    if (res.status == 200) {
                                        userData.credits = res.data.credits;
                                        // setItem('userData', userData);
                                        notifyMessage('Payment successful!');
                                        // props.save_response();
                                    } else {
                                        // props.failedPayment();
                                    }
                                })
                                .catch((err) => {
                                });
                        })
                    },
                    theme: {
                        zIndex: "1",
                        color: "#3D87F1",
                        hide_topbar: true,
                    },
                    order_id: result.data.order_id,
                };
                var rzp1 = new window.Razorpay(options);
                rzp1.open();
            }
            openModal()
        })
        .catch(err => {
            notifyMessage('Error while purchasing credits!');
        })
}

export const getBilling = (data) => dispatch => {

    axios.get(`${process.env.REACT_APP_BASE_URL}get_billing/`, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        }
    })
        .then(result => {
            dispatch({ type: GET_UPDATE_SUCCESS, payload: null });
        })
        .catch(err => {
            notifyMessage('Some error occurred');
        })
}

export const resetUserPassword = (passwordDetails,token) =>async(dispatch)  => {
    const {
        id,
        password,
    } = passwordDetails;

    let form = new FormData();
    form.append("id", id);
    form.append("password", password);

    axios.defaults.headers = {
        "Content-Type": "application/json",
        Authorization: 'Token ' + token,
    };

    try {
        let res = await axios.post('https://pmt.safearth-api.in/api/reset_password/', form);
        if (res.status === 200) {
            return res;
        }
    } catch (error) {
        //error, error.request)
        if (error.response && error.response.data.message) {
            let msg = error.response.data.message.split(".");
            notifyMessage(msg[0])
        } else if (error.request) {
            notifyMessage("No Internet Connection!");
        } else {
            notifyMessage(error.message);
        }
    }
}
