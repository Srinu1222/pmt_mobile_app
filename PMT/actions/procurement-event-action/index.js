import {
    GET_PROCUREMENT_LOADING,
    GET_PROCUREMENT_SUCCESS,
    GET_PROCUREMENT_FAILURE
} from '../types';
import axios from 'axios';
import { getItem } from '../../SecureStorage';

export const getProcurementEvent = () => dispatch => {

    let userData;
    let projectId;

    if (getItem('userData')) {
        userData = JSON.parse(getItem('userData'));
    }

    if (getItem('projectId')) {
        projectId = JSON.parse(getItem('projectId'));
    }

    dispatch({ type: GET_PROCUREMENT_LOADING, payload: null });
    axios.get(`${process.env.REACT_APP_BASE_URL}get_stage_events/`, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        },
        params: {
            project_id: projectId,
            stage: 'PROCUREMENT'
        }
    })
        .then(result => {
            dispatch({ type: GET_PROCUREMENT_SUCCESS, payload: result.data });
        })
        .catch(err => {
            dispatch({ type: GET_PROCUREMENT_FAILURE, payload: { message: err.message } });
        });
};