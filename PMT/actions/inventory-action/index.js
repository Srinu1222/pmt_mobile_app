import {
    GET_INVENTORY_LOADING,
    GET_INVENTORY_SUCCESS,
    GET_INVENTORY_FAILURE,
} from "../types";
import axios from "axios";
import config from "../../config";
import notifyMessage from "../../ToastMessages";


export const getInventory = (token, isSort, isFilter, value) => dispatch => {

    dispatch({ type: GET_INVENTORY_LOADING, payload: null })

    axios.defaults.headers = {
        'Content-Type': 'application/json',
        Authorization: 'Token ' + token
    }

    axios
        .get('https://pmt.safearth-api.in/api/get_inventory/')
        .then(res => {
            dispatch({ type: GET_INVENTORY_SUCCESS, payload: res.data })
        })
        .catch(
            err => {
                dispatch({ type: GET_INVENTORY_FAILURE, payload: { message: err.message } })
            }
        )
}

// REMOVE TEAM MEMBERS
export const removeInventory = (token, inventoryList) => (dispatch) => {

    let form = new FormData();

    form.append("inventory_list", JSON.stringify(inventoryList));
    axios.defaults.headers = {
        "Content-Type": "application/json",
        Authorization: 'Token ' + token,
    };
    axios
        .post('https://pmt.safearth-api.in/api/' + 'delete/inventories/', form)
        .then((res) => {
            notifyMessage("Inventories removed successfully!");
            // setTimeout(() => window.location.reload(), 500);
        })
        .catch((err) =>
            notifyMessage(err.message)
        );
};