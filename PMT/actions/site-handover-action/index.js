import {
    GET_SITE_HANDOVER_LOADING,
    GET_SITE_HANDOVER_SUCCESS,
    GET_SITE_HANDOVER_FAILURE
} from '../types';
import axios from 'axios';
import { getItem } from '../../SecureStorage';

export const getHandoverEvent = () => dispatch => {

    let projectId;
    let userData;

    if (getItem('projectId')) {
        projectId = JSON.parse(getItem('projectId'));
    }

    if (getItem('userData')) {
        userData = JSON.parse(getItem('userData'));
    }

    dispatch({ type: GET_SITE_HANDOVER_LOADING, payload: null });
    axios.get(`${process.env.REACT_APP_BASE_URL}get_stage_events/`, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        },
        params: {
            project_id: projectId,
            stage: 'SITE HAND OVER'
        }
    })
        .then(result => {
            dispatch({ type: GET_SITE_HANDOVER_SUCCESS, payload: result.data });
        })
        .catch(err => {
            dispatch({ type: GET_SITE_HANDOVER_FAILURE, payload: { message: err.message } });
        });
};