import {
    GET_CONSTRUCTION_LOADING,
    GET_CONSTRUCTION_SUCCESS,
    GET_CONSTRUCTION_FAILURE
} from '../types';
import axios from 'axios';
import { getItem } from '../../SecureStorage';

export const getConstructionEvent = () => dispatch => {

    let userData;
    let projectId;

    if (getItem('userData')) {
        userData = JSON.parse(getItem('userData'));
    }

    if (getItem('projectId')) {
        projectId = JSON.parse(getItem('projectId'));
    }

    dispatch({ type: GET_CONSTRUCTION_LOADING, payload: null });
    axios.get(`${process.env.REACT_APP_BASE_URL}get_stage_events/`, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        },
        params: {
            project_id: projectId,
            stage: 'CONSTRUCTION'
        }
    })
        .then(result => {
            dispatch({ type: GET_CONSTRUCTION_SUCCESS, payload: result.data });
        })
        .catch(err => {
            dispatch({ type: GET_CONSTRUCTION_FAILURE, payload: { message: err.message } });
        });
};