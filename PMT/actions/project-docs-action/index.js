import {
    GET_PROJECT_DOCS_LOADING,
    GET_PROJECT_DOCS_SUCCESS,
    GET_PROJECT_DOCS_FAILURE,
} from "../types";
import axios from "axios";
import config from '../../config'
import Toast from "react-native-toast-message";



// GET PROJECT DOCUMENTS
export const getProjectDocs = (project_id, token, is_image = false) => dispatch => {

    dispatch({ type: GET_PROJECT_DOCS_LOADING, payload: null })
    // axios.defaults.headers = {
    //     'Content-Type': 'application/json',
    //     Authorization: `Token'+ 'f81bac3e9de9a1a89e2a9a3874caa21988d980dc`
    // }
    axios.get(`https://pmt.safearth-api.in/api/get_project_databank/`, {
        headers: {
            Authorization: 'Token ' + token,
        },
        params: {
            project_id: project_id,
            is_image: is_image
        }
    })
        .then(res => {
            let projectDocs = res.data;

            dispatch({ type: GET_PROJECT_DOCS_SUCCESS, payload: projectDocs })
        })
        .catch(err => {
            dispatch({ type: GET_PROJECT_DOCS_FAILURE, payload: { message: err.message } })
        }
        )
}


export const postImageTitleAndDescription = (data, token) => {
    axios.post(`https://pmt.safearth-api.in/api/change_event_doc/`, data, {
        headers: {
            Authorization: 'Token ' + token,
        }
    })
        .then(res => {
            if (res.status == 200) {
                Toast.show({
                    text1: 'Updated Successfully!',
                    type: 'success',
                    autoHide: true,
                    position: 'bottom',
                    visibilityTime: 1000
                })
            }
        })
        .catch(err => {})
}