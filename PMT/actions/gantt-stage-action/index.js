import {
    GET_GANTT_EVENTS_LOADING,
    GET_GANTT_EVENTS_SUCCESS,
    GET_GANTT_EVENTS_FAILURE,
    GET_GANTT_EVENT_STAGE_LOADING,
    GET_GANTT_EVENT_STAGE_SUCCESS,
    GET_GANTT_EVENT_STAGE_FAILURE,
    GET_EVENT_DATA_LOADING,
    GET_EVENT_DATA_SUCCESS,
    GET_EVENT_DATA_FAILURE,
    GET_VIEW_SPECIFICATION_LOADING,
    GET_VIEW_SPECIFICATION_SUCCESS,
    GET_VIEW_SPECIFICATION_FAILURE,
    GET_TICKET_LOADING,
    GET_TICKET_SUCCESS,
    GET_TICKET_FAILURE,
    GET_SPOC_LOADING,
    GET_SPOC_SUCCESS,
    GET_SPOC_FAILURE,
    GET_UPDATE_LOADING,
    GET_UPDATE_SUCCESS,
    GET_UPDATE_FAILURE,
    GET_UPLOAD_DOCS_LOADING,
    GET_UPLOAD_DOCS_SUCCESS,
    GET_UPLOAD_DOCS_FAILURE,
    GET_GANTT_EVENTS_MENU_LOADING,
    GET_GANTT_EVENTS_MENU_SUCCESS,
    GET_GANTT_EVENTS_MENU_FAILURE,
    GET_GANTT_DATA_LOADING,
    GET_GANTT_DATA_SUCCESS,
    GET_GANTT_DATA_FAILURE,
    GET_GANTT_DATA_RESET,
    GET_DRAWING_LOADING,
    GET_DRAWING_SUCCESS,
    GET_DRAWING_FAILURE,
    GET_SAMPLE_GANTTS_LOADING,
    GET_SAMPLE_GANTTS_SUCCESS,
    GET_SAMPLE_GANTTS_FAILURE,
    GET_SAMPLE_GANTT_DETAILS_LOADING,
    GET_SAMPLE_GANTT_DETAILS_SUCCESS,
    GET_SAMPLE_GANTTS_DETAILS_FAILURE,
    GET_SAMPLE_GANTTS_PROJECT_DETAILS_LOADING,
    GET_SAMPLE_GANTTS_PROJECT_DETAILS_SUCCESS,
    GET_SAMPLE_GANTTS_PROJECT_DETAILS_FAILURE,
    GET_GANTTS_PROJECT_DETAILS_LOADING,
    GET_GANTTS_PROJECT_DETAILS_SUCCESS,
    GET_GANTTS_PROJECT_DETAILS_FAILURE,
    GET_SAMPLE_GANTTS_DETAILS_RESET,
    GET_SAMPLE_EVENT_LOADING,
    GET_SAMPLE_EVENT_SUCCESS,
    POST_SAMPLE_EVENT_LOADING,
    POST_SAMPLE_EVENT_SUCCESS,
    POST_SAMPLE_EVENT_FAILURE,
    GET_ALL_SAMPLE_EVENT_LOADING,
    GET_ALL_SAMPLE_EVENT_SUCCESS
} from "../types";
import axios from 'axios';
import config from "../../config";
import Toast from "react-native-toast-message";


export const getGanttEvents = (token, projectId) => dispatch => {

    const isSubContractor = false

    dispatch({ type: GET_GANTT_EVENTS_LOADING, payload: null });
    axios.get('https://pmt.safearth-api.in/api/' + 'gantt_view/', {
        headers: {
            Authorization: 'Token ' + token
        },
        params: {
            project_id: projectId,
            is_sub_contractor: isSubContractor
        }
    })
        .then(result => {
            dispatch({ type: GET_GANTT_EVENTS_SUCCESS, payload: result.data });
        })
        .catch(err => {
            dispatch({ type: GET_GANTT_EVENTS_FAILURE, payload: { message: err.message } });
        });
};


export const getGantEventDetails = (stageName, subContractorMode) => dispatch => {

    let userData;
    let projectId;

    if (getItem('userData')) {
        userData = JSON.parse(getItem('userData'));
    }

    if (getItem('projectId')) {
        projectId = JSON.parse(getItem('projectId'));
    }
    const isSubContractor = JSON.parse(getItem('isSubContractor'))

    dispatch({ type: GET_GANTT_EVENT_STAGE_LOADING, payload: null });
    axios.get(`${process.env.REACT_APP_BASE_URL}gantt_view_events/`, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        },
        params: {
            stage_name: stageName,
            project_id: projectId,
            sub_contractor_mode: subContractorMode,
            is_sub_contractor: isSubContractor
        }
    })
        .then(result => {
            dispatch({ type: GET_GANTT_EVENT_STAGE_SUCCESS, payload: result.data });
        })
        .catch(err => {
            dispatch({ type: GET_GANTT_EVENT_STAGE_FAILURE, payload: { message: err.message } });
        });
};

export const getGantEventDetailsMenu = (stageName, subContractorMode) => dispatch => {

    let userData;
    let projectId;

    if (getItem('userData')) {
        userData = JSON.parse(getItem('userData'));
    }

    if (getItem('projectId')) {
        projectId = JSON.parse(getItem('projectId'));
    }

    const isSubContractor = JSON.parse(getItem('isSubContractor'))

    dispatch({ type: GET_GANTT_EVENTS_MENU_LOADING, payload: null });
    axios.get(`${process.env.REACT_APP_BASE_URL}gantt_view_events/`, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        },
        params: {
            stage_name: stageName,
            project_id: projectId,
            sub_contractor_mode: subContractorMode,
            is_sub_contractor: isSubContractor
        }
    })
        .then(result => {
            dispatch({ type: GET_GANTT_EVENTS_MENU_SUCCESS, payload: result.data });
        })
        .catch(err => {
            dispatch({ type: GET_GANTT_EVENTS_MENU_FAILURE, payload: { message: err.message } });
        });
};

export const getCustomEventMenuDetails = (token, projectId, stageName) => dispatch => {

    dispatch({ type: GET_GANTT_EVENTS_MENU_LOADING, payload: null });
    axios.get('https://pmt.safearth-api.in/api/' + 'gantt_view_events/', {
        headers: {
            Authorization: 'Token ' + token
        },
        params: {
            stage_name: stageName,
            project_id: projectId,
            sub_contractor_mode: false,
            is_sub_contractor: false
        }
    })
        .then(result => {
            dispatch({ type: GET_GANTT_EVENTS_MENU_SUCCESS, payload: result.data });
        })
        .catch(err => {
            dispatch({ type: GET_GANTT_EVENTS_MENU_FAILURE, payload: { message: err.message } });
        });
};

export const getEventData = (token, eventId, subContractorMode, is_sub_contractor) => dispatch => {
    dispatch({ type: GET_EVENT_DATA_LOADING, payload: null });
    axios.get('https://pmt.safearth-api.in/api/' + 'get_event/' + eventId.toString() + '/', {
        headers: {
            Authorization: 'Token ' + token
        },
        params: {
            sub_contractor_mode: subContractorMode,
            is_sub_contractor: is_sub_contractor
        }
    })
        .then(result => {
            dispatch({ type: GET_EVENT_DATA_SUCCESS, payload: result.data });
        })
        .catch(err => {
            dispatch({ type: GET_EVENT_DATA_FAILURE, payload: { message: err.message } });
        });
};

export const ganttChartData = (token, project_id) => dispatch => {

    dispatch({ type: GET_GANTT_DATA_LOADING, payload: null });
    axios.get(`https://pmt.safearth-api.in/api/get_gantt_data/`, {
        headers: {
            Authorization: 'Token ' + token
        },
        params: {
            project_id: project_id
        }
    })
        .then(result => {
            dispatch({ type: GET_GANTT_DATA_SUCCESS, payload: result.data });
        })
        .catch(err => {
            dispatch({ type: GET_GANTT_DATA_FAILURE, err: { message: err.message } });
            return message.error('Some error occurred!');
        });
};

export const checkListTask = (data, token) => dispatch => {

    axios.post(config.API_URL + `change/checklist_task/`, data, {
        headers: {
            'Content-Type': 'multipart/form-data',
            Authorization: 'Token ' + token
        }
    })
        .then(result => {
            dispatch({ type: GET_UPDATE_SUCCESS, payload: null });
            return Toast.show({
                text1: 'Successfully Updated....',
                type: 'success',
                autoHide: true,
                position: 'bottom',
                visibilityTime: 1000
            })
        })
        .catch(err => {
            // return //(err.response);
        });
};

export const preRequisiteTask = (data, token) => dispatch => {

    axios.post(config.API_URL + `change/prerequiste_task/`, data, {
        headers: {
            'Content-Type': 'multipart/form-data',
            Authorization: 'Token ' + token
        }
    })
        .then(result => {
            dispatch({ type: GET_UPDATE_SUCCESS, payload: null });
            return Toast.show({
                text1: 'Successfully Changed Status!',
                type: 'success',
                autoHide: true,
                position: 'bottom',
                visibilityTime: 1000
            })
        })
        .catch(err => {
            return //(err.response);
        });
};

export const getTickets = (eventId) => dispatch => {

    let userData;

    if (getItem('userData')) {
        userData = JSON.parse(getItem('userData'));
    }
    const isSubContractor = JSON.parse(getItem('isSubContractor'))

    dispatch({ type: GET_TICKET_LOADING, payload: null });
    axios.get(`${process.env.REACT_APP_BASE_URL}get_tickets/`, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        },
        params: {
            event_id: eventId,
            is_sub_contractor: isSubContractor
        }
    })
        .then(result => {
            dispatch({ type: GET_TICKET_SUCCESS, payload: result.data });
        })
        .catch(err => {
            dispatch({ type: GET_TICKET_FAILURE, payload: { message: err.message } });
        });
};

export const postTickets = (data, token) => async (dispatch) => {
    try {
        const result = await axios.post(config.API_URL + 'add_ticket/', data, {
            headers: {
                'Content-Type': 'multipart/form-data',
                Authorization: 'Token ' + token
            }
        })
        return result;
    } catch (e) {
        return //('Some error occurred!');
    }
};

export const changeTicketStatus = (data, token) => dispatch => {
    axios.post(config.API_URL + 'change_ticket_status/', data, {
        headers: {
            'Content-Type': 'multipart/form-data',
            Authorization: 'Token ' + token
        }
    })
        .then(result => {
            dispatch({ type: GET_UPDATE_SUCCESS, payload: null });
            Toast.show({
                text1: 'Status changed successfully!',
                type: 'success',
                autoHide: true,
                position: 'bottom',
                visibilityTime: 1000
            })
        })
        .catch(err => {
            return message.error('Some error occurred!');
        });
};

export const getSpoc = (token, params) => dispatch => {

    dispatch({ type: GET_SPOC_LOADING, payload: null });

    axios.get('https://pmt.safearth-api.in/api/' + 'get_spoc/', {
        headers: {
            Authorization: 'Token ' + token
        },
        params: params
    })
        .then(result => {
            dispatch({ type: GET_SPOC_SUCCESS, payload: result.data });
        })
        .catch(err => {
            dispatch({ type: GET_SPOC_FAILURE, payload: { message: err.message } });
        });
};

export const getDocs = (eventId) => dispatch => {

    let userData;

    if (getItem('userData')) {
        userData = JSON.parse(getItem('userData'));
    }
    const isSubContractor = JSON.parse(getItem('isSubContractor'))

    dispatch({ type: GET_UPLOAD_DOCS_LOADING, payload: null });
    axios.get(`${process.env.REACT_APP_BASE_URL}get_event_docs/`, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        },
        params: {
            event_id: eventId,
            is_sub_contractor: isSubContractor
        }
    })
        .then(result => {
            dispatch({ type: GET_UPLOAD_DOCS_SUCCESS, payload: result.data });
        })
        .catch(err => {
            dispatch({ type: GET_UPLOAD_DOCS_FAILURE, payload: { message: err.message } });
        });
};

export const postDocs = (data, token, isSubContractor) => async (dispatch) => {
    try {
        const result = await axios.post(config.API_URL + 'add_event_doc/', data, {
            headers: {
                Authorization: 'Token ' + token
            },
            params: {
                is_sub_contractor: isSubContractor
            }
        })
        return result;
    } catch (e) {
        //('nnnn', e.response);
    }
};

export const postLinkedTo = (data) => dispatch => {
    let userData;

    if (getItem('userData')) {
        userData = JSON.parse(getItem('userData'));
    }
    axios.post(`${process.env.REACT_APP_BASE_URL}checklist_linked_to/`, data, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        }
    })
        .then(result => {
            dispatch({ type: GET_UPDATE_SUCCESS, payload: null });
            return message.info('Action completed successfully!');
        })
        .catch(err => {
            return message.error('Error while completing action!');
        });
};

export const followUp = (data, token, isSubContractor) => dispatch => {

    axios.post(config.API_URL + `follow_up_for_task/`, data, {
        headers: {
            Authorization: 'Token ' + token
        },
        params: {
            is_sub_contractor: isSubContractor
        }
    })
        .then(result => {
            Toast.show({
                text1: 'Follow-up completed successfully!',
                type: 'success',
                autoHide: true,
                position: 'bottom',
                visibilityTime: 1000
            })
        })
        .catch(err => {
            return message.error('Error while completing follow-up!');
        });
};

export const editTaskListOfDrawings = (data, token) => dispatch => {
    axios.post(config.API_URL + `list_of_drawings_edit_spec/`, data, {
        headers: {
            'Content-Type': 'multipart/form-data',
            Authorization: 'Token ' + token
        }
    })
        .then(result => {
            Toast.show({
                text1: 'Successfully Updated....',
                type: 'success',
                autoHide: true,
                position: 'bottom',
                visibilityTime: 1000
            })
        })
        .catch(err => {
        });
};

export const editTaskListOfCable = (data, token) => dispatch => {

    axios.post(config.API_URL + `cable_calculation_edit_spec/`, data, {
        headers: {
            'Content-Type': 'multipart/form-data',
            Authorization: 'Token ' + token
        }
    })
        .then(result => {
            Toast.show({
                text1: 'Successfully Updated....',
                type: 'success',
                autoHide: true,
                position: 'bottom',
                visibilityTime: 1000
            })
        })
        .catch(err => {
        });
};

export const editTaskListOfInternalApproval = (data, token) => dispatch => {
    axios.post(config.API_URL + `internal_design_review_edit_spec/`, data, {
        headers: {
            'Content-Type': 'multipart/form-data',
            Authorization: 'Token ' + token
        }
    })
        .then(result => {
            Toast.show({
                text1: 'Successfully Updated....',
                type: 'success',
                autoHide: true,
                position: 'bottom',
                visibilityTime: 1000
            })
        })
        .catch(err => {
        });
};

export const editTaskListOfProcurementPlan = (data, token) => dispatch => {
    axios.post(config.API_URL + `procurement_plan_edit_spec/`, data, {
        headers: {
            'Content-Type': 'multipart/form-data',
            Authorization: 'Token ' + token
        }
    })
        .then(result => {
            Toast.show({
                text1: 'Successfully Updated....',
                type: 'success',
                autoHide: true,
                position: 'bottom',
                visibilityTime: 1000
            })
        })
        .catch(err => {
            //(err.response)
        });
};

export const editTaskListOfProcurementSpecification = (data, token) => dispatch => {

    axios.post(config.API_URL + `procurement_spec_edit_spec/`, data, {
        headers: {
            Authorization: 'Token ' + token
        }
    })
        .then(result => {
            return Toast.show({
                text1: 'Successfully Updated....',
                type: 'success',
                autoHide: true,
                position: 'bottom',
                visibilityTime: 1000
            })
        })
        .catch(err => {
        });
};

export const editTaskListOfFinalSpecification = (data, token) => dispatch => {

    axios.post(config.API_URL + `procurement_final_edit_spec/`, data, {
        headers: {
            Authorization: 'Token ' + token
        }
    })
        .then(result => {
            return Toast.show({
                text1: 'Successfully Updated....',
                type: 'success',
                autoHide: true,
                position: 'bottom',
                visibilityTime: 1000
            })
        })
        .catch(err => {
            // //(err.response)
        });
};

export const editTaskListOfOEMGuideLines = (data, token) => dispatch => {

    axios.post(config.API_URL + `installation_of_walkways_edit_spec/`, data, {
        headers: {
            Authorization: 'Token ' + token
        }
    })
        .then(result => {
            if(result?.data?.status=='FAIL'){
                Toast.show({
                    text1: result?.data?.error,
                    type: 'info',
                    autoHide: true,
                    position: 'bottom',
                    visibilityTime: 1000
                })
            }
            else {
                Toast.show({
                    text1: 'Successfully Updated....',
                    type: 'success',
                    autoHide: true,
                    position: 'bottom',
                    visibilityTime: 1000
                })
            }
        })
        .catch(err => {
        });
};

export const editPreDispatchInspection = (data, token) => dispatch => {

    axios.post(config.API_URL + `pre_dispatch_inspection_edit_spec/`, data, {
        headers: {
            Authorization: 'Token ' + token
        }
    })
        .then(result => {
            return Toast.show({
                text1: 'Successfully Updated....',
                type: 'success',
                autoHide: true,
                position: 'bottom',
                visibilityTime: 1000
            })
        })
        .catch(err => {
            return message.info('Error while updating!');
        });
};

export const viewSpecifications = (eventId, checkListId, isSubContractor, token) => dispatch => {

    dispatch({ type: GET_VIEW_SPECIFICATION_LOADING, payload: null });
    axios.get(config.API_URL + 'view_specifications/', {
        headers: {
            Authorization: 'Token ' + token
        },
        params: {
            event_id: eventId,
            checklist_id: checkListId,
            is_sub_contractor: isSubContractor
        }
    })
        .then(result => {
            dispatch({ type: GET_VIEW_SPECIFICATION_SUCCESS, payload: result.data });
        })
        .catch(err => {
            //(err.response)
            dispatch({ type: GET_VIEW_SPECIFICATION_FAILURE, payload: err.message });
        });
};


export const viewDrawingSpecifications = (eventId, checkListId, isSubContractor, token) => dispatch => {

    dispatch({ type: GET_VIEW_SPECIFICATION_LOADING, payload: null });
    axios.get(config.API_URL + 'view_specifications_for_lod/', {
        headers: {
            Authorization: 'Token ' + token
        },
        params: {
            event_id: eventId,
            checklist_id: checkListId,
            is_sub_contractor: isSubContractor
        }
    })
        .then(result => {
            dispatch({ type: GET_VIEW_SPECIFICATION_SUCCESS, payload: result.data });
        })
        .catch(err => {
            dispatch({ type: GET_VIEW_SPECIFICATION_FAILURE, payload: err.message });
        });
};

export const requestApproval = (data, token, isSubContractor) => dispatch => {

    axios.post(config.API_URL + `is_event_requested/`, data, {
        headers: {
            Authorization: 'Token ' + token
        },
        params: {
            is_sub_contractor: isSubContractor
        }
    })
        .then(result => {
            dispatch({ type: GET_UPDATE_SUCCESS, payload: null });
            Toast.show({
                text1: 'Approval request sent successfully!',
                type: 'success',
                autoHide: true,
                position: 'bottom',
                visibilityTime: 1000
            })
        })
        .catch(err => {
            return message.error('Error while sending approval request!');
        });
};

export const postApproval = (data, token, isSubContractor) => dispatch => {

    axios.post(config.API_URL + `event_approval/`, data, {
        headers: {
            Authorization: 'Token ' + token
        },
        params: {
            is_sub_contractor: isSubContractor
        }
    })
        .then(result => {
            dispatch({ type: GET_UPDATE_SUCCESS, payload: null });
            Toast.show({
                text1: 'Event status approved succesfully!',
                type: 'success',
                autoHide: true,
                position: 'bottom',
                visibilityTime: 1000
            })
        })
        .catch(err => {
            return //('Error while approving status!');
        });
};

export const getDrawings = () => dispatch => {

    let userData;
    let projectId;

    if (getItem('userData')) {
        userData = JSON.parse(getItem('userData'));
    }

    if (getItem('projectId')) {
        projectId = JSON.parse(getItem('projectId'));
    }
    dispatch({ type: GET_DRAWING_LOADING, payload: null });
    axios.get(`${process.env.REACT_APP_BASE_URL}list_of_drawings/`, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        },
        params: {
            project_id: projectId
        }
    })
        .then(result => {
            dispatch({ type: GET_DRAWING_SUCCESS, payload: result.data });
        })
        .catch(err => {
            return message.error('Error while getting drawings!');
        });
};

export const postBoq = (data) => dispatch => {

    let userData;

    if (getItem('userData')) {
        userData = JSON.parse(getItem('userData'));
    }
    const isSubContractor = JSON.parse(getItem('isSubContractor'))

    axios.post(`${process.env.REACT_APP_BASE_URL}upload_boq_file/`, data, {
        headers: {
            'Content-Type': 'multipart/form-data',
            Authorization: 'Token ' + userData.authToken
        },
        params: {
            is_sub_contractor: isSubContractor
        }
    })
        .then(result => {
            return message.info('BOQ Uploaded successfully!');
        })
        .catch(err => {
            return message.error('Error while uploading BOQ!');
        });
};

export const downloadGantt = () => dispatch => {

    let userData;
    let projectId;

    if (getItem('userData')) {
        userData = JSON.parse(getItem('userData'));
    }

    if (getItem('projectId')) {
        projectId = JSON.parse(getItem('projectId'));
    }
    axios.get(`${process.env.REACT_APP_BASE_URL}download_gantt/`, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        },
        params: {
            project_id: projectId
        }
    })
        .then(result => {
            var win = window.open(result.data.gantt, '_blank');
            win.focus();
        })
        .catch(err => {
            return message.error('Error while downloading gantt...');
        });
};

export const getSampleGantts = (token) => dispatch => {

    axios.get(`https://pmt.safearth-api.in/api/get_sample_gantt/`, {
        headers: {
            Authorization: 'Token ' + token
        }

    })

        .then(result => {
            dispatch({ type: GET_SAMPLE_GANTTS_SUCCESS, payload: result.data });
        })
        .catch(err => {
            //err)
            dispatch({ type: GET_SAMPLE_GANTTS_FAILURE, payload: err.message });
        });
};

export const getSampleGanttSpecifications = (id, token) => dispatch => {


    axios.get(`https://pmt.safearth-api.in/api/get_gantt_spec/`, {
        headers: {
            Authorization: 'Token ' + token
        },
        params: {
            gantt_id: id
        }
    })
        .then(result => {
            dispatch({ type: GET_SAMPLE_GANTT_DETAILS_SUCCESS, payload: result.data });
        })
        .catch(err => {
            dispatch({ type: GET_SAMPLE_GANTTS_DETAILS_FAILURE, payload: err.message });
        });
};

export const resetSampleGanttSpecifications = (id) => dispatch => {
    dispatch({ type: GET_SAMPLE_GANTTS_DETAILS_RESET, payload: null })
};

export const getSampleGanttEvents = (ganttId, stage) => dispatch => {

    let userData;

    if (getItem('userData')) {
        userData = JSON.parse(getItem('userData'));
    }

    dispatch({ type: GET_SAMPLE_GANTT_DETAILS_LOADING, payload: null });
    axios.get(`${process.env.REACT_APP_BASE_URL}get_gantt_events/`, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        },
        params: {
            gantt_id: ganttId,
            stage: stage
        }
    })
        .then(result => {
            dispatch({ type: GET_SAMPLE_GANTT_DETAILS_SUCCESS, payload: result.data });
        })
        .catch(err => {
            dispatch({ type: GET_SAMPLE_GANTTS_DETAILS_FAILURE, payload: err.message });
        });
};

export const getSampleGanttProjectDetails = (id) => dispatch => {

    let userData;

    if (getItem('userData')) {
        userData = JSON.parse(getItem('userData'));
    }

    dispatch({ type: GET_SAMPLE_GANTTS_PROJECT_DETAILS_LOADING, payload: null });
    axios.get(`${process.env.REACT_APP_BASE_URL}get_gantt_details_for_a_project/`, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        },
        params: {
            gantt_id: id
        }
    })
        .then(result => {
            dispatch({ type: GET_SAMPLE_GANTTS_PROJECT_DETAILS_SUCCESS, payload: result.data });
        })
        .catch(err => {
            dispatch({ type: GET_SAMPLE_GANTTS_PROJECT_DETAILS_FAILURE, payload: err.message });
        });
};

export const updateSpecification = (data) => dispatch => {

    let userData;

    if (getItem('userData')) {
        userData = JSON.parse(getItem('userData'));
    }

    axios.post(`${process.env.REACT_APP_BASE_URL}update_project_spec/`, data, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        }
    })
        .then(result => {
            dispatch({ type: GET_UPDATE_SUCCESS, payload: null })
        })
        .catch(err => {
            return message.error('Somethig went wrong')
        });
};

export const deleteTaksHandler = (data) => dispatch => {

    let userData;

    if (getItem('userData')) {
        userData = JSON.parse(getItem('userData'));
    }

    axios.post(`${process.env.REACT_APP_BASE_URL}delete_task/`, data, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        }
    })
        .then(result => {
            dispatch({ type: GET_UPDATE_SUCCESS, payload: null })
        })
        .catch(err => {
            return message.error('Some error ocurred!')
        });
};

export const ganttDetailsForProject = () => dispatch => {

    let userData;

    if (getItem('userData')) {
        userData = JSON.parse(getItem('userData'));
    }
    const isSubContractor = JSON.parse(getItem('isSubContractor'))

    dispatch({ type: GET_GANTTS_PROJECT_DETAILS_LOADING, payload: null })
    axios.get(`${process.env.REACT_APP_BASE_URL}get_sample_gantt/`, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        },
        params: {
            is_sub_contractor: isSubContractor
        }
    })
        .then(result => {
            dispatch({ type: GET_GANTTS_PROJECT_DETAILS_SUCCESS, payload: result.data })
        })
        .catch(err => {
            dispatch({ type: GET_GANTTS_PROJECT_DETAILS_FAILURE, payload: err.message })
            return message.error('Some error occurred!')
        });
};

export const deleteSampleGantt = (data) => dispatch => {

    let userData;

    if (getItem('userData')) {
        userData = JSON.parse(getItem('userData'));
    }

    axios.post(`${process.env.REACT_APP_BASE_URL}delete_gantt/`, data, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        }
    })
        .then(result => {
            dispatch({ type: GET_UPDATE_SUCCESS, payload: null })
        })
        .catch(err => {
            if (err.response.status === 500) {
                dispatch({ type: GET_UPDATE_SUCCESS, payload: err.response.status })
            } else {
                return message.error('Some error occurred!')
            }
        });
};

export const changeStatus = (data, token, isSubContractor) => async dispatch => {

    try {
        const result = await axios.post(config.API_URL+`change_event_status/`, data, {
            headers: {
                Authorization: 'Token ' + token
            },
            params: {
                is_sub_contractor: isSubContractor
            }
        })
        dispatch({ type: GET_UPDATE_SUCCESS, payload: null })
        return result
    } catch (e) {
        return message.error('Some error occurred!')
    }
};

export const getSampleEvents = () => dispatch => {

    let projectId;
    let userData;

    if (getItem('projectId')) {
        projectId = JSON.parse(getItem('projectId'));
    }

    if (getItem('userData')) {
        userData = JSON.parse(getItem('userData'));
    }


    dispatch({ type: GET_ALL_SAMPLE_EVENT_LOADING, payload: null })
    axios.get(`${process.env.REACT_APP_BASE_URL}get_all_sample_events/`, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        },
        params: {
            project_id: projectId
        }
    })
        .then(result => {
            dispatch({ type: GET_ALL_SAMPLE_EVENT_SUCCESS, payload: result.data })
        })
        .catch(err => {
            return message.error('Some error occurred!')
        });
};

export const getSampleEvent = ({ sampleEventId }) => dispatch => {

    let projectId;
    let userData;

    if (getItem('projectId')) {
        projectId = JSON.parse(getItem('projectId'));
    }

    if (getItem('userData')) {
        userData = JSON.parse(getItem('userData'));
    }


    dispatch({ type: GET_SAMPLE_EVENT_LOADING, payload: null })
    axios.get(`${process.env.REACT_APP_BASE_URL}get_sample_event/`, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        },
        params: {
            sample_event_id: sampleEventId
        }
    })
        .then(result => {
            dispatch({ type: GET_SAMPLE_EVENT_SUCCESS, payload: result.data })
        })
        .catch(err => {
            return message.error('Some error occurred!')
        });
};

export const addEvent = (data) => dispatch => {

    let userData;

    if (getItem('userData')) {
        userData = JSON.parse(getItem('userData'));
    }


    dispatch({ type: POST_SAMPLE_EVENT_LOADING, payload: null })
    axios.post(`${process.env.REACT_APP_BASE_URL}add_new_event/`, data, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        }
    })
        .then(result => {
            dispatch({ type: POST_SAMPLE_EVENT_SUCCESS, payload: result.data })
        })
        .catch(err => {
            return message.error('Some error occurred!')
        });
}