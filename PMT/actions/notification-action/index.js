import {
    GET_NOTIFICATION_LOADING,
    GET_NOTIFICATION_SUCCESS,
    GET_NOTIFICATION_FAILURE
} from '../../actions/types';
import axios from 'axios';
import config from "../../config";


export const getNotifications = (token) => dispatch => {

    dispatch({ type: GET_NOTIFICATION_LOADING, payload: null });
    axios.get(config.API_URL + 'get_notifications/', {
        headers: {
            Authorization: 'Token ' + token
        },
        params: {
            is_mobile: true,
            see_all: true,
        }
    })
        .then(result => {
            dispatch({ type: GET_NOTIFICATION_SUCCESS, payload: result.data });
        })
        .catch(err => {
            dispatch({ type: GET_NOTIFICATION_FAILURE, payload: { message: err.message } });
        });
};