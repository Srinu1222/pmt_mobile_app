import {
    GET_PROJECT_LEVEL_TEAMS_LOADING,
    GET_PROJECT_LEVEL_TEAMS_SUCCESS,
    GET_PROJECT_LEVEL_TEAMS_FAILURE,
    GET_TEAM_MEMBERS_NAME_LOADING,
    GET_TEAM_MEMBERS_NAME_SUCCESS,
    GET_TEAM_MEMBERS_NAME_FAILURE,
    POST_TEAM_MEMBER_DETAILS_LOADING,
    POST_TEAM_MEMBER_DETAILS_SUCCESS,
    POST_TEAM_MEMBER_DETAILS_FAILURE,
    CREATE_NEW_RIGHT_LOADING,
    CREATE_NEW_RIGHT_SUCCESS,
    CREATE_NEW_RIGHT_FAILURE,
} from "../types";
import axios from "axios";
import config from '../../config'
import Toast from "react-native-toast-message";


export const getProjectLevelTeam = (token, projectId) => dispatch => {

    dispatch({ type: GET_PROJECT_LEVEL_TEAMS_LOADING, payload: null })
    axios.defaults.headers = {
        'Content-Type': 'application/json',
        Authorization: 'Token ' + token
    }
    axios
        .get('https://pmt.safearth-api.in/api/' + 'get_project_team/', {
            params: {
                project_id: projectId,
            }
        })
        .then(res => {
            let projectLevelTeam = res.data.TeamMembers_list
            dispatch({ type: GET_PROJECT_LEVEL_TEAMS_SUCCESS, payload: projectLevelTeam })
        })
        .catch(err => {
            //err.response)
            dispatch({ type: GET_PROJECT_LEVEL_TEAMS_FAILURE, payload: { message: err.message } })
        })
}

export const getTeamMembersName = (token) => dispatch => {

    dispatch({ type: GET_TEAM_MEMBERS_NAME_LOADING, payload: null })
    axios.defaults.headers = {
        'Content-Type': 'application/json',
        Authorization: `Token `+token
    }

    axios
        .get(config.API_URL+`get_all_members/`)
        .then(res => {
            let teamMembersNameList = res.data.team_members_list
            dispatch({ type: GET_TEAM_MEMBERS_NAME_SUCCESS, payload: teamMembersNameList })
        })
        .catch(err => dispatch({ type: GET_TEAM_MEMBERS_NAME_FAILURE, payload: { message: err.message } }))
}

export const postTeamMemberDetails = (token, memberId, roles, projectId) => dispatch => {

    let form = new FormData()
    form.append('team_member_id', memberId)
    form.append('roles', JSON.stringify(roles))
    form.append('project_id', projectId)

    //(form)
    axios.defaults.headers = {
        'Content-Type': 'multipart/form-data',
        Authorization: `Token `+token
    }

    axios
        .post(config.API_URL+`add_member_to_project/`, form)
        .then(res => {
            Toast.show({
                text1: 'Team updated successfully!',
                type: 'success',
                autoHide: true,
                position: 'bottom',
                visibilityTime: 1000
            })
        })
        .catch(err => {})
}

export const createNewRight = (token, rightType, stage, projectId, teamMemberId) => dispatch => {
    let form = new FormData()
    form.append("project_id", projectId);
    form.append("team_member_id", teamMemberId)
    form.append("right_type", rightType)
    form.append("stage", stage)

    axios.defaults.headers = {
        'Content-Type': 'multipart/form-data',
        Authorization: `Token `+token
    }
    axios
        .post(config.API_URL+`create_right/`, form)
        .then(res => {
            Toast.show({
                text1: 'Right created successfully!',
                type: 'info',
                autoHide: true,
                position: 'bottom',
                visibilityTime: 1000
            })
        })
        .catch(err => {})
}