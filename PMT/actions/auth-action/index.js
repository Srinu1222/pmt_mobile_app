import {
  SIGNUP_USER_LOADING,
  SIGNUP_USER_SUCCESS,
  SIGNUP_USER_FAILURE,
  LOGIN_USER_LOADING,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAILURE,
  GET_COMPANY_DETAILS_LOADING,
  GET_COMPANY_DETAILS_SUCCESS,
  GET_COMPANY_DETAILS_FAILURE,
  GET_EMAIL_CONFIRMATION_LOADING,
  GET_EMAIL_CONFIRMATION_SUCCESS,
  GET_EMAIL_CONFIRMATION_FAILURE,
  VERIFY_OTP_LOADING,
  VERIFY_OTP_SUCCESS,
  VERIFY_OTP_FAILURE,
} from "../types";

import axios from 'axios';
import notifyMessage from '../../ToastMessages'
import AsyncStorage from '@react-native-async-storage/async-storage';
import config from "../../config";
import Toast from "react-native-toast-message";


export const signUpUser = (user) => (dispatch) => {
  dispatch({ type: SIGNUP_USER_LOADING, payload: null });
  axios
    .post(`${process.env.REACT_APP_BASE_URL}signup/`, user)
    .then((result) => {
      dispatch({ type: SIGNUP_USER_SUCCESS, payload: result.data });
    })
    .catch((e) => {
      dispatch({
        type: SIGNUP_USER_FAILURE,
        payload: { message: e.response.data.msg },
      });
    });
};

export const loginUser = user => async (dispatch) => {
  // https://pmt.safearth-api.in/rest-auth/login/
  // https://pmt.safearth-api.in/rest-auth/login/
  dispatch({ type: LOGIN_USER_LOADING, payload: null });
  axios.post('https://pmt.safearth-api.in/rest-auth/login/', user)
    .then(async result => {
      try {
        const jsonValue = JSON.stringify(result.data)
        await AsyncStorage.setItem('userDetails', jsonValue)
        Toast.show({
          text1: 'Logging in..',
          type: 'info',
          autoHide: true,
          position: 'bottom',
          visibilityTime: 1000
        })
      } catch (e) {
        // saving error
      }
      dispatch({ type: LOGIN_USER_SUCCESS, payload: result.data });
    })
    .catch(e => {
      Toast.show({
        text1: 'Invalid Username or password!',
        type: 'error',
        autoHide: true,
        position: 'bottom',
        visibilityTime: 1000
      })     
      dispatch({ type: LOGIN_USER_FAILURE, payload: { message: e.message } });
    });                                                                                                                                                              
  };
  // get company details
export const getCompanyDetails = (username, password) => (dispatch) => {
  dispatch({ type: GET_COMPANY_DETAILS_LOADING, payload: null });
  const form = new FormData();
  form.append("username", username);
  form.append("password", password);

  
  axios
    .post("https://pmt.safearth-api.in/rest-auth/login/", form)
    .then((res) => {
      if (res.data.key) {
        const token = res.data.key;
        axios.defaults.headers = {
          "Content-Type": "application/json",
          Authorization: `Token ${token}`,
        };
        axios
          .get("https://safearth-api.in/users/get_vendor_details")
          .then((res) => {
            const companyDetails = res.data.vendor;
            dispatch({
              type: GET_COMPANY_DETAILS_SUCCESS,
              payload: companyDetails,
            });
          })
          .catch((err) => message.error(err.message));
      }
      dispatch({ type: GET_COMPANY_DETAILS_SUCCESS, payload: res.data });
    })
    .catch((err) => {
      dispatch({ type: GET_COMPANY_DETAILS_FAILURE, payload: err.message });
    });
};

// get email confirmation
export const getEmailConfirmation = (email) => (dispatch) => {
  axios.defaults.headers = {
    "Content-Type": "application/json",
  };
  axios
    .get(`https://pmt.safearth-api.in/api/send_email_confirmation/`, {
      params: {
        email: email,
      },
    })
    .then((res) =>

    notifyMessage(
        "Confirmation email has been sent to the provided email address."
      )
    )
    .catch((err) => notifyMessage(err.message));
};

// verify otp
export const verifyOtp = (form) => async (dispatch) => {
  dispatch({ type: VERIFY_OTP_LOADING, payload: null });
  //('index')

  axios.defaults.headers = {
    "Content-Type": "application/json",
  };
  try {
    let res = await axios.post(
      `https://pmt.safearth-api.in/api/verify_otp/`,
      form
    );
    //('index res',res)
    if (res.status === 200) {
      return res;
    }
  } catch (error) {
    //('error',error)
    if (error.response && error.response.data.message) {
      let msg = error.response.data.message.split(".");
      notifyMessage('Invalid username or password!')
      (msg[0]);
    } else if (error.request) {
      notifyMessage(error.message);
    } else {
      notifyMessage(error.message);
    }
  }
};
