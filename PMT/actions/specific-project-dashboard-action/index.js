import {
  GET_SPECIFIC_PROJECT_DETAILS_LOADING,
  GET_SPECIFIC_PROJECT_DETAILS_SUCCESS,
  GET_SPECIFIC_PROJECT_DETAILS_FAILURE,
  GET_PROJECT_SPECIFICATION_LOADING,
  GET_PROJECT_SPECIFICATION_SUCCESS,
  GET_PROJECT_SPECIFICATION_FAILURE,
  GET_PROJECT_SPECIFICATION_EVENTS_LOADING,
  GET_PROJECT_SPECIFICATION_EVENTS_SUCCESS,
  GET_PROJECT_SPECIFICATION_EVENTS_FAILURE
} from "../types";
import axios from "axios";
import config from "../../config";


// GET SPECIFIC PROJECT DETAILS
export const getSpecificProjectDetails = (token, id) => dispatch => {
  dispatch({ type: GET_SPECIFIC_PROJECT_DETAILS_LOADING, payload: null })
  axios.defaults.headers = {
    "Content-Type": "application/json",
    Authorization: 'Token ' + token
  }
  axios
    .get('https://pmt.safearth-api.in/api/get_project_dashboard/', {
      params: {
        project_id: id
      }
    })
    .then(res => {
      let specificProjectDetails = res.data
      dispatch({ type: GET_SPECIFIC_PROJECT_DETAILS_SUCCESS, payload: specificProjectDetails })
    })
    .catch(err => {
      //err.response)
      dispatch({ type: GET_SPECIFIC_PROJECT_DETAILS_FAILURE, payload: { message: err.message } })
    })
}

export const getProjectSpcification = (token, stage, projectId) => (dispatch) => {

  dispatch({ type: GET_PROJECT_SPECIFICATION_LOADING, payload: null });
  axios
    .get('https://pmt.safearth-api.in/api/'+'get_specification_events/', {
      headers: {
        Authorization: "Token " + token,
      },
      params: {
        project_id: projectId,
        stage: stage
      }
    })
    .then((result) => {
      dispatch({ type: GET_PROJECT_SPECIFICATION_SUCCESS, payload: result.data });
    })
    .catch((err) => {
      return message.error('Something went wrong!')
    });
};

export const getProjectSpcificationEvents = (token, eventId) => (dispatch) => {

  const isSubContractor = false

  dispatch({ type: GET_PROJECT_SPECIFICATION_EVENTS_LOADING, payload: null });
  axios
    .get('https://pmt.safearth-api.in/api/'+'get_event_specifications/', {
      headers: {
        Authorization: "Token " + token,
      },
      params: {
        event_id: eventId,
        is_sub_contractor: isSubContractor
      }
    })
    .then((result) => {
      dispatch({ type: GET_PROJECT_SPECIFICATION_EVENTS_SUCCESS, payload: result.data });
    })
    .catch((err) => {
      return message.error('Something went wrong!')
    });
};

