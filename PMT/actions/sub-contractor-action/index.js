import axios from 'axios'
import {
    GET_SUB_CONTRACTOR_LOADING,
    GET_SUB_CONTRACTOR_SUCCESS,
    GET_SUB_CONTRACTOR_FAILURE,
    GET_UPDATE_RESET,
    GET_SUB_CONTRACTOR_PROJECT_LOADING,
    GET_SUB_CONTRACTOR_PROJECT_FAILURE,
    GET_SUB_CONTRACTOR_PROJECT_SUCCESS
} from '../types'
import config from "../../config";
import notifyMessage from '../../ToastMessages';
import Toast from "react-native-toast-message";


export const getSubContractors = (token) => dispatch => {

    dispatch({ type: GET_SUB_CONTRACTOR_LOADING, payload: null })
    axios.get('https://pmt.safearth-api.in/api/get_company_sub_contractors/', {
        headers: {
            Authorization: 'Token ' + token
        }
    })
        .then(result => {
            dispatch({ type: GET_SUB_CONTRACTOR_SUCCESS, payload: result.data })
        })
        .catch(err => {
        })
}

export const getProjectSubContractors = (token) => dispatch => {

    

    dispatch({ type: GET_SUB_CONTRACTOR_PROJECT_LOADING, payload: null })
    axios.get(`https://pmt.safearth-api.in/api/get_all_sub_contractors/`, {
        headers: {
            Authorization: 'Token ' + token
        }
    })
        .then(result => {
            dispatch({ type: GET_SUB_CONTRACTOR_PROJECT_SUCCESS, payload: result.data })
        })
        .catch(err => {
            return message.error('Something went wrong!')
        })
}

export const addSubContractors = (data, token) => dispatch => {

    axios.post(config.API_URL+`add_sub_contractor/`, data, {
        headers: {
            Authorization: 'Token ' + token
        }
    })
        .then(result => {
            dispatch({ type: GET_UPDATE_RESET, payload: null })
            Toast.show({
                text1: 'Added Sub-Contractor!',
                type: 'success',
                autoHide: true,
                position: 'bottom',
                visibilityTime: 1000
              })
        })
        .catch(err => {
        })
}

export const verifyOTP = (data) => async (dispatch) => {

    let userData

    if (getItem('userData')) {
        userData = JSON.parse(getItem('userData'))
    }

    try {
        const result = await axios.post(`${process.env.REACT_APP_BASE_URL}access_provided/`, data, {
            headers: {
                Authorization: 'Token ' + userData.authToken
            }
        })

        if (result) {
            dispatch({ type: GET_UPDATE_RESET, payload: null })
            message.info('OTP verified successfully!')
            return result
        }

    } catch (e) {
        return message.error('Something went wrong!')
    }
}