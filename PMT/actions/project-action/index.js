import {
  GET_PROJECT_LOADING,
  GET_PROJECT_SUCCESS,
  GET_PROJECT_FAILURE,
  POST_PROJECT_LOADING,
  POST_PROJECT_SUCCESS,
  POST_PROJECT_FAILURE,
  GET_RESPONSIBILITY_MATRIX_LOADING,
  GET_RESPONSIBILITY_MATRIX_SUCCESS,
  GET_RESPONSIBILITY_MATRIX_FAILURE,
  GET_ALL_PROJECT_RESET,
  UPDATE_SELECTED_PROJECT,
  POST_PROJECT_RESET,
} from "../types";
import axios from "axios";
import config from "../../config";
import Toast from "react-native-toast-message";


export const getProjects = (token) => (dispatch) => {  
  dispatch({ type: GET_PROJECT_LOADING, payload: null });
  //'https://pmt.safearth-api.in/api/')
  axios
    .get('https://pmt.safearth-api.in/api/get_projects/', {
      headers: {
        Authorization: "Token " + token,
      },
    })
    .then((result) => {
      dispatch({ type: GET_PROJECT_SUCCESS, payload: result.data });
      config.notificationCount = result.data.notification_count
      if(result.data.active_projects && result.data.active_projects.length > 0){
        dispatch({ type: UPDATE_SELECTED_PROJECT, payload: result.data.active_projects[0] });
      }
    })
    .catch((err) => {
      // //('hhhhhhhhhhhh',err.response)
      dispatch({
        type: GET_PROJECT_FAILURE,
        payload: { message: err.message },
      });
    });
};

export const updateSelectedProject = project => (dispatch) => {
  dispatch({ type: UPDATE_SELECTED_PROJECT, payload: project });
}

export const postProjects = (project,token) => (dispatch) => {
 
  axios
    .post(`https://pmt.safearth-api.in/api/add_project/`, project, {
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: "Token " + token,
      },
    })
    .then((result) => {
      dispatch({ type: POST_PROJECT_SUCCESS, payload: result.data });
    })
    .catch((err) => {
      // //("err",err.response)
      dispatch({
        type: POST_PROJECT_FAILURE,
        payload: { message: err.message },
      });
    });
};

export const changeProjectStatus = (project, token) => (dispatch) => {

  axios
    .post(config.API_URL+`change_project_completion/`, project, {
      headers: {
        Authorization: "Token " + token,
      },
    })
    .then((result) => {
      Toast.show({
        text1: 'Project status changed successfully!',
        type: 'info',
        autoHide: true,
        position: 'bottom',
        visibilityTime: 1000
      })
    })
    .catch((err) => {
      return message.error('Something went wrong!')
    });
}

export const changeSubContractorProjectStatus = () => (dispatch) => {

  let userData
  let projectId

  if (getItem('userData')) {
    userData = JSON.parse(getItem('userData'))
  }
  if (getItem('projectId')) {
    projectId = JSON.parse(getItem('projectId'))
  }

  const formData = new FormData()
  formData.append('project_id', projectId)

  axios
    .post(`${process.env.REACT_APP_BASE_URL}sub_contractor_project_planning/`, formData, {
      headers: {
        Authorization: "Token " + userData.authToken,
      }
    })
    .then((result) => {
      return message.info('Project status changed successfully!')
    })
    .catch((err) => {
      return message.error('Something went wrong!')
    });
}

// GET RESPONSIBILITY MATRIX
export const getResponsibilityMatrix = () => (dispatch) => {

  let userData;

  if (getItem('userData')) {
    userData = JSON.parse(getItem('userData'));
  }
  dispatch({ type: GET_RESPONSIBILITY_MATRIX_LOADING, payload: null });
  axios.defaults.headers = {
    "Content-Type": "application/json",
    Authorization: `Token ${userData.authToken}`,
  };
  axios
    .get(`${process.env.REACT_APP_BASE_URL}get_default_project_rights/`)
    .then((res) =>
      dispatch({
        type: GET_RESPONSIBILITY_MATRIX_SUCCESS,
        payload: res.data.rights_list,
      })
    )
    .catch((err) =>
      dispatch({
        type: GET_RESPONSIBILITY_MATRIX_FAILURE,
        payload: { message: err.message },
      })
    );
};

export const resetPostProject = () => (dispatch) => {
  dispatch({ type: POST_PROJECT_RESET, payload: null })
};
