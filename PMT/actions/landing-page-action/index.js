import * as actionTypes from "../types";
import axios from "axios";
import { message } from "antd";

export const requestADemo = (form) => (dispatch) => {
  axios.defaults.headers = {
    "Content-Type": "application/json",
  };
  axios
    .post(`${process.env.REACT_APP_BASE_URL}request_demo/`, form)
    .then((res) => message.success("Thank you! Our team will get back to you."))
    .catch((err) => message.error(err.message));
};
