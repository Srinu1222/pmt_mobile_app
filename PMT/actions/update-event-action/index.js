import {
    GET_DELETE_LOADING,
    GET_DELETE_SUCCESS,
    GET_DELETE_FAILURE,
    GET_UPDATE_LOADING,
    GET_UPDATE_SUCCESS,
    GET_UPDATE_FAILURE
} from '../types';
import axios from 'axios';
import config from "../../config";
import notifyMessage from "../../ToastMessages";
import Toast from 'react-native-toast-message';


export const updateEvent = (token, updatedEvent, eventId) => dispatch => {

    dispatch({ type: GET_UPDATE_LOADING, payload: null });
    axios.patch('https://pmt.safearth-api.in/api/' + 'change/event/' + eventId + '/', updatedEvent, {
        headers: {
            Authorization: "Token " + token,
        }
    })
        .then(result => {
            Toast.show({
                text1: 'Event Details Updated!',
                type: 'info',
                autoHide: true,
                position: 'bottom',
                visibilityTime: 1000
            })
            dispatch({ type: GET_UPDATE_SUCCESS, payload: result.data });
        })
        .catch(err => {
            // //(err.response)
            dispatch({ type: GET_UPDATE_FAILURE, payload: { message: err.message } });
        })
};

export const deleteEvent = (token, eventId) => dispatch => {
    dispatch({ type: GET_DELETE_LOADING, payload: null });

    axios.delete('https://pmt.safearth-api.in/api/' + 'delete/event/' + eventId, {
        headers: {
            Authorization: "Token " + token,
        }
    })
        .then(result => {
            Toast.show({
                text1: 'Event removed successfully!',
                position: 'bottom'
            });
        })
        .catch(err => {
            //err)
            dispatch({ type: GET_DELETE_FAILURE, payload: { message: err.message } });
        });
};


export const updateSampleGanttEvents = (updatedEvent) => dispatch => {

    let userData;

    if (getItem('userData')) {
        userData = JSON.parse(getItem('userData'));
    }
    dispatch({ type: GET_UPDATE_LOADING, payload: null });
    axios.post(`${process.env.REACT_APP_BASE_URL}update_gantt_events/`, updatedEvent, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        }
    })
        .then(result => {
            dispatch({ type: GET_UPDATE_SUCCESS, payload: result.data });
        })
        .catch(err => {
            dispatch({ type: GET_UPDATE_FAILURE, payload: { message: err.message } });
        })
}

export const deleteSampleGanttEvent = (data) => dispatch => {

    let userData;

    if (getItem('userData')) {
        userData = JSON.parse(getItem('userData'));
    }
    dispatch({ type: GET_DELETE_LOADING, payload: null });
    axios.post(`${process.env.REACT_APP_BASE_URL}delete_event/`, data, {
        headers: {
            Authorization: 'Token ' + userData.authToken
        }
    })
        .then(result => {
            dispatch({ type: GET_DELETE_SUCCESS, payload: null });
        })
        .catch(err => {
            dispatch({ type: GET_DELETE_FAILURE, payload: { message: err.message } });
        });
}
