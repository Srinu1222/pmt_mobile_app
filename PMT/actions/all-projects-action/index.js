import {
    GET_ALL_PROJECT_LOADING,
    GET_ALL_PROJECT_SUCCESS,
    GET_ALL_PROJECT_FAILURE
} from '../types';
import axios from 'axios';
import config from "../../config";


export const getAllProjects = (token) => dispatch => {

    dispatch({ type: GET_ALL_PROJECT_LOADING, payload: null });

    axios.get('https://pmt.safearth-api.in/api/get_all_projects/', {
        headers: {
            Authorization: 'Token ' + token
        }
    })
    .then((result) => {
      // //(result.data.projects)
      dispatch({ type: GET_ALL_PROJECT_SUCCESS, payload: result.data });
    })
    .catch((err) => {
      // //(err.response)
      dispatch({
        type: GET_ALL_PROJECT_FAILURE,
        payload: { message: err.message },
      });
    });
};
