import {
  GET_DAILY_UPDATE_PROJECTS_LOADING,
  GET_DAILY_UPDATE_PROJECTS_SUCCESS,
  GET_DAILY_UPDATE_PROJECTS_FAILURE,
  POST_DAILY_UPDATE_LOADING,
  POST_DAILY_UPDATE_SUCCESS,
  POST_DAILY_UPDATE_FAILURE,
  GET_TODAY_TASKS_LOADING,
  GET_TODAY_TASKS_SUCCESS,
  GET_TODAY_TASKS_FAILURE,
  GET_TOMORROW_TASKS_LOADING,
  GET_TOMORROW_TASKS_SUCCESS,
  GET_TOMORROW_TASKS_FAILURE,
  GET_PROJECT_REPORTS_LOADING,
  GET_PROJECT_REPORTS_SUCCESS,
  GET_PROJECT_REPORTS_FAILURE,
  UPLOAD_EVENT_DOCS_LOADING,
  UPLOAD_EVENT_DOCS_SUCCESS,
  UPLOAD_EVENT_DOCS_FAILURE,
} from "../types";
import axios from "axios";
import config from "../../config";
import notifyMessage from "../../ToastMessages";
import Toast from "react-native-toast-message";


// GET Project Reports
export const getProjectReports = (token, projectId) => (dispatch) => {

  dispatch({ type: GET_PROJECT_REPORTS_LOADING, payload: null });
  axios.defaults.headers = {
    "Content-Type": "application/json",
    Authorization: "Token " + token,
  };
  axios
    .get(
      'https://pmt.safearth-api.in/api/' + 'get_reports/',
      {
        params: {
          project_id: projectId,
        },
      }
    )
    .then((res) => {
      // //(res.data)
      dispatch({
        type: GET_PROJECT_REPORTS_SUCCESS,
        payload: res.data,
      });
    })
    .catch((err) => {
      //(err.response)
      dispatch({
        type: GET_PROJECT_REPORTS_FAILURE,
        payload: { message: err.message },
      });
    });
};


// GET DAILY UPDATE PROJECTS
export const getDailyUpdateProjects = (token) => (dispatch) => {

  dispatch({ type: GET_DAILY_UPDATE_PROJECTS_LOADING, payload: null });
  axios.defaults.headers = {
    "Content-Type": "application/json",
    Authorization: "Token " + token,
  };
  axios
    .get('https://pmt.safearth-api.in/api/' + 'get_daily_update_projects/')
    .then((res) => {
      dispatch({
        type: GET_DAILY_UPDATE_PROJECTS_SUCCESS,
        payload: res.data,
      });
    })
    .catch((err) => {
      dispatch({
        type: GET_DAILY_UPDATE_PROJECTS_FAILURE,
        payload: { message: err.message },
      });
    });
};


// GET TODAY'S TASKS FOR DAILY UPDATES
export const getTodayTasks = (token, projectId, isToday) => (dispatch) => {


  dispatch({ type: GET_TODAY_TASKS_LOADING, payload: null });

  axios.defaults.headers = {
    "Content-Type": "application/json",
    Authorization: "Token " + token
  };

  axios
    .get(
      'https://pmt.safearth-api.in/api/' + 'get_member_tasks_for_daily_updates/',
      {
        params: {
          project_id: projectId,
          is_today: isToday,
        },
      }
    )
    .then((res) => {
      dispatch({
        type: GET_TODAY_TASKS_SUCCESS,
        payload: res.data,
      });
    })
    .catch((err) => {

      dispatch({
        type: GET_TODAY_TASKS_FAILURE,
        payload: { message: err.message },
      })
    }

    );
};

// GET TOMORROW'S TASKS FOR DAILY UPDATES
export const getTomorrowTasks = (token, projectId, isToday) => (dispatch) => {

  dispatch({ type: GET_TOMORROW_TASKS_LOADING, payload: null });

  axios.defaults.headers = {
    "Content-Type": "application/json",
    Authorization: "Token " + token
  };

  axios
    .get(
      'https://pmt.safearth-api.in/api/' + 'get_member_tasks_for_daily_updates/',
      {
        params: {
          project_id: projectId,
          is_today: isToday,
        },
      }
    )
    .then((res) => {
      dispatch({
        type: GET_TOMORROW_TASKS_SUCCESS,
        payload: res.data,
      });
    })
    .catch((err) => {
      dispatch({
        type: GET_TOMORROW_TASKS_FAILURE,
        payload: { message: err.message },
      });
    }
    );
};

// POST DAILY UPDATE
export const postDailyUpdate = (
  token, dailyUpdate, DocumentDataList, uploadDocumentsList, PicturesDataList, uploadPicturesList
) => (dispatch) => {

  const { projectId, todayCompletedTasks, onGoingTasks, tomorrowTasks } = dailyUpdate;
  let form = new FormData(this);

  form.append("project_id", projectId);
  form.append("today_completed_tasks", JSON.stringify(todayCompletedTasks));
  form.append("on_going_tasks", JSON.stringify(onGoingTasks));
  form.append("tomorrow_tasks", JSON.stringify(tomorrowTasks));
  form.append("documents_list", JSON.stringify(DocumentDataList));
  form.append("pictures_list", JSON.stringify(PicturesDataList));
  uploadDocumentsList.map((i) => form.append("document_files", i))
  uploadPicturesList.map((i) => form.append("picture_files", i))
  console.log(DocumentDataList)

  //(form)

  axios.defaults.headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: "Token " + token
  };

  axios
    .post('https://pmt.safearth-api.in/api/' + 'create_daily_update/', form)
    .then(res => {
      Toast.show({
        text1: 'Your update has been submitted!',
        type: 'success',
        autoHide: true,
        position: 'bottom',
        visibilityTime: 500
      })
    })
    .catch(err => notifyMessage(err.message))
};


export const uploadEventDocument = (data, token) => dispatch => {

  dispatch({ type: UPLOAD_EVENT_DOCS_LOADING, payload: null });

  axios.post(config.API_URL + `upload_event_document`, data, {
    headers: {
      'Content-Type': 'multipart/form-data',
      Authorization: 'Token ' + token
    }
  })
    .then(res => {
      dispatch({
        type: UPLOAD_EVENT_DOCS_SUCCESS,
        payload: res.data,
      });
      return Toast.show({
        text1: 'File Uploaded Successfully!!!',
        type: 'success',
        autoHide: true,
        position: 'bottom',
        visibilityTime: 1000
      })
    })
    .catch(err => {
      dispatch({
        type: UPLOAD_EVENT_DOCS_FAILURE,
        payload: { message: err.message },
      });
      return //(err.response);
    });
};

export const deleteEventDocument = (data, token) => dispatch => {

  axios.post(config.API_URL + `delete_event_document`, data, {
    headers: {
      'Content-Type': 'multipart/form-data',
      Authorization: 'Token ' + token
    }
  })
    .then(result => {
      return Toast.show({
        text1: 'Deleted Successfully!!!',
        type: 'success',
        autoHide: true,
        position: 'bottom',
        visibilityTime: 1000
      })
    })
    .catch(err => {
      return //(err.response);
    });
};

