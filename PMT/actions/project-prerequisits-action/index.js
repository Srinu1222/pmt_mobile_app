import {
    GET_PREREQUISITS_LOADING,
    GET_PREREQUISITS_SUCCESS,
    GET_PREREQUISITS_FAILURE
} from '../types';
import axios from 'axios';
import config from "../../config";


export const getPreRequisits = (token, project_id, stage) => dispatch => {
    // //(token, project_id, stage)
    dispatch({ type: GET_PREREQUISITS_LOADING, payload: null });
    axios.get('https://pmt.safearth-api.in/api/get_stage_events/', {
        headers: {
            Authorization: 'Token '+token
        },
        params: {
            project_id: project_id,
            stage: stage
        }
    })
        .then(result => {
            // //(result.data)
            dispatch({ type: GET_PREREQUISITS_SUCCESS, payload: result.data });
        })
        .catch(err => {
            //(err.response)
            dispatch({ type: GET_PREREQUISITS_FAILURE, payload: { message: err.message } });
        });
};

