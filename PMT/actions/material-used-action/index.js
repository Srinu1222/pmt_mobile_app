import Axios from "axios";
import {
  GET_PROJECT_INVENTORY_LOADING,
  GET_PROJECT_INVENTORY_SUCCESS,
  GET_PROJECT_INVENTORY_FAILURE,
  ADD_MATERIAL_LOADING,
  ADD_MATERIAL_SUCCESS,
  ADD_MATERIAL_FAILURE,
} from "../types";
import axios from "axios";
import config from '../../config'
import Toast from "react-native-toast-message";


// GET PROJECT LEVEL INVENTORY
export const getProjectInventory = (token, projectId) => (dispatch) => {

  dispatch({ type: GET_PROJECT_INVENTORY_LOADING, payload: null });
  axios.defaults.headers = {
    "Content-Type": "application/json",
    Authorization: 'Token '+token,
  };
  axios
    .get('https://pmt.safearth-api.in/api/get_project_inventory/', {
      params: {
        project_id: projectId
      },
    })
    .then((res) =>
      dispatch({ type: GET_PROJECT_INVENTORY_SUCCESS, payload: res.data })
    )
    .catch((err) =>
      dispatch({
        type: GET_PROJECT_INVENTORY_FAILURE,
        payload: { message: err.message },
      })
    );
};

// ADD USED MATERIAL TO MATERIAL_USED TABLE
export const addMaterial = (materialDetails, token) => dispatch => {
  const {
    project_id,
    category,
    product_name,
    quantity,
    location,
    rate,
    value,
    upload_po_file,
    specification,
    unitForamt
  } = materialDetails
  let form = new FormData()
  form.append('project_id', project_id)
  form.append('category', category)
  form.append('product_name', product_name)
  form.append('quantity', quantity)
  form.append('location', location)
  form.append('rate', rate)
  form.append('value', value)
  form.append('specification', specification)
  form.append('quantity_format', unitForamt)
  form.append('upload_po_file', upload_po_file)

  axios.defaults.headers = {
    'Content-Type': 'multipart/form-data',
    Authorization: `Token `+token
  }

  axios
    .post(config.API_URL+`add_project_material/`, form)
    .then(res => {
      Toast.show({
        text1: 'Material added successfully!',
        type: 'success',
        autoHide: true,
        position: 'bottom',
        visibilityTime: 1000
      })
    })
    .catch(err => {
      //(err.response)
    })
}
