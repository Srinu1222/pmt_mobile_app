import * as actionTypes from "../types";
import axios from "axios";
import config from "../../config";
import Toast from "react-native-toast-message";


export const getBudgetedFinanceDetails = (token, projectId) => (dispatch) => {

  dispatch({
    type: actionTypes.GET_BUDGETED_FINANCE_DETAILS_LOADING,
    payload: null,
  });
  axios.defaults.headers = {
    "Content-Type": "application/json",
    Authorization: 'Token ' + token
  };

  axios
    .get('https://pmt.safearth-api.in/api/' + 'get_budgeted/', {
      params: {
        project_id: projectId,
      },
    })
    .then((res) =>
      dispatch({
        type: actionTypes.GET_BUDGETED_FINANCE_DETAILS_SUCCESS,
        payload: res.data,
      })
    )
    .catch((err) => {
      //(err.response)
      dispatch({
        type: actionTypes.GET_BUDGETED_FINANCE_DETAILS_FAILURE,
        payload: { message: err.message },
      })
    }
    );
};

export const getActualFinanceDetails = (token, projectId) => (dispatch) => {

  dispatch({
    type: actionTypes.GET_ACTUAL_FINANCE_DETAILS_LOADING,
    payload: null,
  });
  axios.defaults.headers = {
    "Content-Type": "application/json",
    Authorization: 'Token ' + token
  };

  axios
    .get('https://pmt.safearth-api.in/api/' + 'get_actual/', {
      params: {
        project_id: projectId,
      },
    })
    .then((res) =>
      dispatch({
        type: actionTypes.GET_ACTUAL_FINANCE_DETAILS_SUCCESS,
        payload: res.data,
      })
    )
    .catch((err) =>
      dispatch({
        type: actionTypes.GET_ACTUAL_FINANCE_DETAILS_FAILURE,
        payload: { message: err.message },
      })
    );
};

export const uploadFinanceFile = (FileObject, token, project_id) => (dispatch) => {

  let form = new FormData();
  form.append("project_id", project_id);
  form.append("finance_file", FileObject);
  axios.defaults.headers = {
    "Content-Type": "multipart/form-data",
    Authorization: `Token ` + token,
  };
  axios
    .post(config.API_URL + `upload_finance_file/`, form)
    .then((res) =>
      Toast.show({
        text1: 'Finance File Uploaded Successfully!',
        type: 'success',
        autoHide: true,
        position: 'bottom',
        visibilityTime: 1000
      })
    )
    .catch((err) => {});
};

export const calculatePnL = (token, data, projectId) => (dispatch) => {
  const {
    mode,
    basicPrice,
    gst,
    totalPrice,
    size,
    signingPrice,
    epcPriceTarget,
  } = data;

  let form = new FormData();
  form.append("project_id", projectId);
  form.append("mode", mode);
  form.append("size", size);
  if (mode === "CAPEX") {
    form.append("basic", basicPrice);
    form.append("gst", gst);
    form.append("total_price", totalPrice);
  } else {
    form.append("signing_price", signingPrice);
    form.append("epc_price_target", epcPriceTarget);
  }

  dispatch({ type: actionTypes.CALCULATE_P_N_L_LOADING, payload: null });
  axios.defaults.headers = {
    "Content-Type": "multipart/form-data",
    Authorization: 'Token ' + token
  };

  axios
    .post('https://pmt.safearth-api.in/api/calculate_p_and_l/', form)
    .then((res) =>
      dispatch({ type: actionTypes.CALCULATE_P_N_L_SUCCESS, payload: res.data })
    )
    .catch((err) => {
      //err.response)
      dispatch({
        type: actionTypes.CALCULATE_P_N_L_FAILURE,
        payload: { message: err.response.data.response },
      });
    });
};

export const updateBudgetedData = (token, category, updatedData, projectId) => (dispatch) => {

  let form = new FormData();
  if (category === "budget_supply_side") {
    form.append("budget_supply_side", JSON.stringify(updatedData));
  } else {
    form.append("budget_service_side", JSON.stringify(updatedData));
  }
  form.append("project_id", projectId);

  axios.defaults.headers = {
    "Content-Type": "multipart/form-data",
    Authorization: 'Token ' + token,
  };

  axios
    .post(config.API_URL + 'update_budgeted/', form)
    .then((res) => Toast.show({
      text1: 'Data Updated Successfully...',
      type: 'info',
      autoHide: true,
      position: 'bottom',
      visibilityTime: 1000
    }))
    .catch((err) => {});
};
