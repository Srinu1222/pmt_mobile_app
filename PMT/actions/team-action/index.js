import {
  GET_TEAMS_LOADING,
  GET_TEAMS_SUCCESS,
  GET_TEAMS_FAILURE,
  POST_TEAM_DETAILS_LOADING,
  POST_TEAM_DETAILS_SUCCESS,
  POST_TEAM_DETAILS_FAILURE,
  POST_MEMBERS_LIST_LOADING,
  POST_MEMBERS_LIST_SUCCESS,
  POST_MEMBERS_LIST_FAILURE,
  CHANGE_DEPARTMENT_LOADING,
  CHANGE_DEPARTMENT_SUCCESS,
  CHANGE_DEPARTMENT_FAILURE,
} from "../types";
import axios from "axios";
import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useState, useMemo } from 'react';
import notifyMessage from '../../ToastMessages';
import config from "../../config";
import Toast from 'react-native-toast-message';


export const getTeams = (token, is_mobile) => async(dispatch) => {

  dispatch({ type: GET_TEAMS_LOADING, payload: null });
  const userDetails = await AsyncStorage.getItem('userDetails');
  const value = JSON.parse(userDetails);
  //("form getTeams",value?.key)
  axios
    .get('https://pmt.safearth-api.in/api/get_teams/', {
      headers: {
        Authorization: 'Token ' + token,
      },
      params: {
        is_mobile: is_mobile
      }
    })
    .then((result) => {
      dispatch({ type: GET_TEAMS_SUCCESS, payload: result.data });
    })
    .catch((err) => {
      dispatch({ type: GET_TEAMS_FAILURE, payload: { message: err.message } });
    });
};

// POST TEAM DETAILS
export const addTeam = (teamDetails, token) => async (dispatch) => {

  const {
    name,
    number,
    email,
    designation,
    location,
    department,
  } = teamDetails;

  let form = new FormData();
  form.append("name", name);
  form.append("number", number);
  form.append("email", email);
  form.append("designation", designation);
  form.append("location", location);
  form.append("department", department);

  axios.defaults.headers = {
    "Content-Type": "application/json",
    Authorization: 'Token ' + token,
  };

  try {
    let res = await axios.post('https://pmt.safearth-api.in/api/add_team/', form);
    if (res.status === 200) {
      return res;
    }
  } catch (error) {
    // if (error.response && error.response.data.message) {
    //   let msg = error.response.data.message.split(".");
    //   notifyMessage(msg[0])
    // } else if (error.request) {
    //   notifyMessage("No Internet Connection!");
    // } else {
    //   notifyMessage(error.message);
    // }
  }
};

export const addGroupTeam = (data) => async(dispatch) => {

  let userData;

  const userDetails = await AsyncStorage.getItem('userDetails');
  const value = JSON.parse(userDetails);
  axios
    .post(`${process.env.REACT_APP_BASE_URL}add_team_members/`, data, {
      headers: {
        Authorization: 'Token ' + value.key
      }
    })
    .then((res) => {
      message.success("Team added successfully!");
    })
    .catch((err) => message.error(err.message));
};

// REMOVE TEAM MEMBERS
export const removeTeamMembers = (token, membersIdList, sub_contractor_member) => async(dispatch) => {

  let form = new FormData();
  form.append("members_id_list", JSON.stringify(membersIdList));
  form.append("sub_contractor_member", sub_contractor_member);

  axios.defaults.headers = {
    "Content-Type": "application/json",
    Authorization: 'Token '+ token,
  };

  axios
    .post('https://pmt.safearth-api.in/api/delete/members/', form)
    .then((res) => {
      Toast.show({
        text1: "Members removed successfully!",
      });
      // setTimeout(() => window.location.reload(), 500);
    })
    .catch((err) => {});
};

// CHANGE DEPARTMENT
export const changeDepartment = (newDepartment, memberId) => (dispatch) => {

  let userData;

  if (getItem('userData')) {
    userData = JSON.parse(getItem('userData'));
  }
  let form = new FormData();
  form.append("member_id", memberId);
  form.append("department", newDepartment);
  axios.defaults.headers = {
    "Content-Type": "multipart/form-data",
    Authorization: `Token ${userData.authToken}`,
  };
  axios
    .post(`${process.env.REACT_APP_BASE_URL}update_department/`, form)
    .then((res) => message.success("Department changed successfully!", 2))
    .catch((err) => message.error(err.message));
};
