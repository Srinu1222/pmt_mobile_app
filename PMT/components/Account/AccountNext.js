import React from 'react'
import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    Button
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import EmptyScreenHeader from '../EmptyScreens/EmptyScreenHeader';
import FastImage from 'react-native-fast-image';
import color from 'color';


const AccountNext = (props) => {
    return (
     <SafeAreaView>
            <KeyboardAvoidingView>
               
                <View style={styles.container}>
                    <EmptyScreenHeader {...props} title='Account' />
               
                    <View style={styles.parent2}>
                    <ScrollView>
                    <View style={{ display: 'flex', flexDirection: 'column' }}>
                        <View style={{backgroundColor:'#3d87f1',height:hp('35%'),justifyContent:'center', alignItems:'center'}}>
                            <View>
                            < FastImage
                  style={{ width: wp('38%'), height: hp('80%'),marginTop:-60,color:'#fff' }}
                  source={require('../../assets/icons/SafEarth_Logo_White.png')}
                  resizeMode={FastImage.resizeMode.contain}
                />
                            </View>
                      
                        </View>
                        <View style={styles.item_card}>
                            <TextInput
                            placeholder="abc@gmail.com"
                            placeholderTextColor="rgb(68,67,67)"
                            style={styles.textInput}
                            autoCapitalize="none"
                            // onChangeText={val => textInputChange(val)}
                            // onEndEditing={e => handleValidUser(e.nativeEvent.text)}
                        />
                        <TextInput
                            placeholder="Current Password"
                            placeholderTextColor="rgb(179,179,179)"
                            style={styles.textInput}
                            autoCapitalize="none"
                            // onChangeText={val => textInputChange(val)}
                            // onEndEditing={e => handleValidUser(e.nativeEvent.text)}
                        />
                        <TextInput
                            placeholder="New Password"
                            placeholderTextColor="rgb(179,179,179)"
                            style={styles.textInput}
                            autoCapitalize="none"
                            // onChangeText={val => textInputChange(val)}
                            // onEndEditing={e => handleValidUser(e.nativeEvent.text)}
                        />
                        <TextInput
                            placeholder="Re-type New Password"
                            placeholderTextColor="rgb(179,179,179)"
                            style={styles.textInput}
                            autoCapitalize="none"
                            // onChangeText={val => textInputChange(val)}
                            // onEndEditing={e => handleValidUser(e.nativeEvent.text)}
                        />

                <TouchableOpacity style={styles.signIn} onPress={() => login()}>
                <Text
                  style={[
                    styles.textSign,
                    {
                      color: '#fff',
                    },
                  ]}>
                  Confirm
                </Text>
              </TouchableOpacity>
                        

                        </View>
                        <View style={{flexDirection:'row', marginTop:100,marginBottom:50,backgroundColor:'#000',marginRight:10,marginLeft:10,height:hp('8%')}}>
                      
                        <Text style={{color:'#fff',fontSize:13,padding:'2%'}}>An email has been sent to you with password reset instructions.</Text>
                        </View>
                        </View>
         
                    </ScrollView>
                    </View>
                </View>
              
            </KeyboardAvoidingView>
        </SafeAreaView >
    )
}

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    parent2: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        flex: 11,
        backgroundColor: 'white',


    },
    item_card: {
        shadowColor: 'rgba(0,0,0,0.2)',
        shadowOffset: {
          width: 0,
          height: 4,
        },
        height: hp('65%'),
        width: wp('88%'),
        marginRight: 10,
        shadowOpacity: 0.2,
        shadowRadius: 4.46,
        elevation: 2,
        borderWidth: 0,
        borderRadius:4,
        marginTop: 16.5,
        borderTopLeftRadius: 4,
        borderBottomLeftRadius: 4,
        backgroundColor:'#fff',
        marginLeft:20,
        marginTop:-80,
        // padding: '5%',
        paddingRight: '4%'
      },
      textInput: {
        width: wp('74%'),
        left: 25,
        top: 40,
        height: hp('8%'),
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: 'white',
        marginBottom: 30,
        backgroundColor: 'rgba(179,179,179, 0.1)',
        borderRadius: 5,
        fontFamily: 'SourceSansPro-Regular',
      },
    option_text: {
        fontSize: hp('1.2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center'
    },
    signIn: {
        width: wp('74%'),
        height: hp('8%'),
        left: 25,
        top: 80,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        color: '#05375a',
        backgroundColor: '#3A80E3',
        borderRadius: 5,
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: 'SourceSansPro-Regular',
      },
    option_text: {
        fontSize: hp('1.2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center'
    },
    signIn: {
        width: wp('79.4%'),
        height: hp('8%'),
        left: 25,
        top: 80,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        color: '#05375a',
        backgroundColor: '#3A80E3',
        borderRadius: 5,
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: 'SourceSansPro-Regular',
      },
    textSign: {
        width: wp('16.1%'),
        fontSize: 15,
        lineHeight: 20,
        fontFamily: 'SourceSansPro-Regular',
      },
    
    btnText: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular'
    },

    btn: {
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        width: wp('40%'),
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center'
    }

});

export default AccountNext
