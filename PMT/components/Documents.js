import React, { useEffect } from 'react';
import {
  View,
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Dimensions,
  Image,
  Text,
  TextInput,
  PermissionsAndroid,
  TouchableOpacity,
  KeyboardAvoidingView,
  Animated,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import * as actionCreator from '../actions/docs-action/index';
import { useDispatch, useSelector } from 'react-redux';
import { Searchbar } from 'react-native-paper';
import { FlatList } from 'react-native-gesture-handler';
import Pagination from './Pagination';
import FastImage from 'react-native-fast-image';
import RNFetchBlob from 'rn-fetch-blob';
import EmptyLoader from './EmptyLoader';
import {
  useCollapsibleSubHeader,
  CollapsibleSubHeaderAnimator,
} from 'react-navigation-collapsible';
import { ButtonGroup, FAB, Icon } from 'react-native-elements';
import EmptyDashBoard from '../components/EmptyScreens/EmptyDashBoard'
import ElevatedView from './ElevatedView';
import Search from './teams/SearchComponent';
import EmptyDocs from './ProjectDocuments/EmptyDocs';
import Toast from 'react-native-toast-message';

function Item({ id, name, total_files_count }) {

  return (
    <View style={styles.item_card1}>
      <View>
        <Text style={styles.name_text}>{name}</Text>
        <Text style={styles.department_text}>TOTAL FILES - {total_files_count}</Text>
      </View>
      <View style={{ alignItems: 'center', justifyContent: 'center' }}>
        <Icon type='feather' name='chevron-right' color='#888' />
      </View>
    </View>
  );
}
const Projlist = ({ navigation, route }) => {
  const [searchQuery, setSearchQuery] = React.useState('');
  const onChangeSearch = query => setSearchQuery(query);
  const [activeButton, setactiveButton] = React.useState(1);
  const [pbutton, setpbutton] = React.useState(0);
  const [onStockButtonPress, setonStockButtonPress] = React.useState(false);
  const [onConsumedButtonPress, setonConsumedButtonPress] = React.useState(
    false,
  );
  const [selectedIndex, setSelectedIndex] = React.useState(0);


  const [search, setSearch] = React.useState('');
  const SetSearch = (query) => {
    setSearch(query)
  }
  const filterItems = (items, filter) => {
    if(onConsumedButtonPress){
      return items.filter(item => item.document_name.toLowerCase().includes(filter.toLowerCase()))

    }else{
      return items.filter(item => item.project_name.toLowerCase().includes(filter.toLowerCase()))

    }
  }
  const {
    onScroll /* Event handler */,
    containerPaddingTop /* number */,
    scrollIndicatorInsetTop /* number */,
    translateY,
  } = useCollapsibleSubHeader();

  const buttons = ['Projects', 'Document Name'];

  const Header = () => (
    <ElevatedView
      elevation={8}
      style={{
        paddingTop: 15,
        paddingLeft: wp('5%'),
        paddingRight: wp('5%'),
        width: '100%',
        height: 'auto',
        backgroundColor: '#fff',
      }}>
      
<Search
        style={{ justifyContent: 'center', alignItems: 'center', marginBottom: '5%', marginTop: '2%' }}
          setSearch={SetSearch}
          search={search}
        />
      <ElevatedView elevation={2}>
        <ButtonGroup
          onPress={updateIndex}
          selectedIndex={selectedIndex}
          buttons={buttons}
          containerStyle={{ height: hp('6%'), borderWidth: 0 }}
          textStyle={{
            fontFamily: 'SourceSansPro-Regular',
            fontSize: hp('2%'),
          }}
          selectedButtonStyle={{ backgroundColor: '#3d87f1' }}
        />
      </ElevatedView>
      <View style={{ marginBottom: hp('2%') }}>
        <Pagination
          activeButton={activeButton}
          setactiveButton={setactiveButton}
          reducerLength={
            onStockButtonPress
              ? docsReducers.success.data.projects.length
              : docsReducers.success.data.basic_project_documents_details.length
          }
          style={{ paddingBottom: hp('5%') }}
        />
      </View>
    </ElevatedView>
    
  );

  const updateIndex = index => {
    if (index == 0) {
      onPressStock();
    } else {
      onPressConsumed();
    }
    setSelectedIndex(index);
  };

  const onPressStock = () => {
    setonStockButtonPress(true);
    setonConsumedButtonPress(false);
  };

  const onPressConsumed = () => {
    setonConsumedButtonPress(true);
    setonStockButtonPress(false);
  };

  const dispatch = useDispatch();
  const docsReducers = useSelector(state => {
    return state.docsReducers.docs.get;
  });

  useEffect(() => {
    setonStockButtonPress(true);
    setonConsumedButtonPress(false);
    dispatch(actionCreator.getDocs(route.params.token));
  }, []);

  const checkPermission = async Image_URI => {
    // Function to check the platform
    // If iOS then start downloading
    // If Android then ask for permission

    if (Platform.OS === 'ios') {
      downloadImage();
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Storage Permission Required',
            message: 'App needs access to your storage to download files',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          // Once user grant the permission start downloading
          downloadImage(Image_URI);
        } else {
          // If permission denied then show alert
        }
      } catch (err) {
        // To handle permission related exception
        console.warn(err);
      }
    }
  };

  const downloadImage = URI => {
    // Main function to download the image

    // To add the time suffix in filename
    let date = new Date();
    // Image URL which we want to download
    let image_URL = URI;
    // Getting the extention of the file
    let ext = getExtention(image_URL);
    ext = '.' + ext[0];
    // Get config and fs from RNFetchBlob
    // config: To pass the downloading related options
    // fs: Directory path where we want our image to download
    const { config, fs } = RNFetchBlob;
    let PictureDir = fs.dirs.PictureDir;
    let options = {
      fileCache: true,
      addAndroidDownloads: {
        // Related to the Android only
        useDownloadManager: true,
        notification: true,
        path:
          PictureDir +
          '/safearth_files_' +
          Math.floor(date.getTime() + date.getSeconds() / 2) +
          ext,
        description: 'Pdf',
      },
    };
    config(options)
      .fetch('GET', image_URL)
      .then(res => {
        // Showing alert after successful downloading
        //'res -> ', JSON.stringify(res));
        Toast.show({
          type: 'success',
          text1: 'File',
          text2: 'File has been downloaded Successfully.'
        });
      });
  };
  const getExtention = filename => {
    // To get the file extension
    return /[.]/.exec(filename) ? /[^.]+$/.exec(filename) : undefined;
  };

  const navproject =(project_id) =>{
    
    navigation.navigate('Project Name', { project_id: project_id, token: route.params.token })
  }

  if (docsReducers.success.ok) {
    return (
      <SafeAreaView style={{ height: '100%' }}>
        {
          onConsumedButtonPress ? (
            <Animated.FlatList
              ListEmptyComponent={
                <EmptyDashBoard />
              }
              keyExtractor={(item, index) => index}
              data={

                filterItems(docsReducers.success.data.projects.slice(activeButton * 10 - 10, activeButton * 10), search)
                // docsReducers.success.data.projects.slice(
                //   activeButton * 10 - 10,
                //   activeButton * 10,
                // )

              }
              renderItem={({ item, index }) => (

                <TouchableOpacity onPress={() => navigation.navigate('Document Name', { project_id: item.project_id, Docs: item, AllDocs:docsReducers.success.data.projects,token: route.params.token  })}>
                  <ElevatedView  >

                    <View style={styles.item_card1}>

                      <View>
                        <Text style={styles.name_text}>{item.document_name}</Text>
                        <Text style={styles.department_text}>{item.project_name}</Text>
                      </View>
                      <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                        <TouchableOpacity
                          onPress={() => { checkPermission(item.file) }}
                        >
                          <Image
                            style={{ height: 36, width: 36 }}
                            source={require('../assets/icons/Download.png')}
                          />
                        </TouchableOpacity>

                      </View>
                    </View>
                  </ElevatedView>
                </TouchableOpacity>
              )}
              onScroll={onScroll}
              contentContainerStyle={{
                paddingTop: containerPaddingTop,
                paddingLeft: hp('3%'),
                paddingRight: hp('3%'),
                paddingBottom: hp('1%'),
              }}
              scrollIndicatorInsets={{ top: scrollIndicatorInsetTop }}
            />) : (
            <Animated.FlatList
              ListEmptyComponent={
                <EmptyDashBoard />
              }
              keyExtractor={(item, index) => index}
              data={
                filterItems(docsReducers.success.data.basic_project_documents_details.slice(activeButton * 10 - 10, activeButton * 10), search)

                
              }
              renderItem={({ item, index }) =>(
                <TouchableOpacity onPress={() =>navproject(item.project_id) }>
                  <ElevatedView>
                    <Item
                      name={item.project_name}
                      total_files_count={item.total_files_count}
                    />
                  </ElevatedView>
                </TouchableOpacity>
            )
        }
        onScroll={onScroll}
        contentContainerStyle={{
          paddingTop: containerPaddingTop,
          paddingLeft: hp('3%'),
          paddingRight: hp('3%'),
          paddingBottom: hp('1%'),
        }}
        scrollIndicatorInsets={{ top: scrollIndicatorInsetTop }}
        />)}
        <CollapsibleSubHeaderAnimator translateY={translateY}>
          <Header />
        </CollapsibleSubHeaderAnimator>

      </SafeAreaView>
    );
  }
  else {
    return <EmptyLoader />;
  }
};

const styles = StyleSheet.create({
  name_text: {
    fontSize: hp('2.4%'),
    fontFamily: 'SourceSansPro-SemiBold',
    color: 'rgb(68, 67, 67)',
  },

  email_text: {
    fontSize: hp('2%'),
    fontFamily: 'SourceSansPro-Regular',
    color: 'rgb(68, 67, 67)',
  },

  department_text: {
    fontSize: hp('1.7%'),
    fontFamily: 'SourceSansPro-SemiBold',
    color: 'rgb(61, 135, 241)',
  },


  item_card: {
    shadowColor: 'rgba(0,0,0,0.4)',
    height: wp('19%'),
    elevation: 8,
    borderWidth: 0,
    borderLeftColor: 'rgb(61, 135, 241)',
    borderLeftWidth: wp('1.3%'),
    marginTop: 16.5,
    borderRadius: 4,
    padding: '5%',
    paddingRight: '4%',
    backgroundColor: '#fff',

  },
  item_card1: {
    shadowColor: 'rgba(0,0,0,0.4)',
    height: wp('19%'),
    elevation: 8,
    borderWidth: 0,
    borderLeftColor: 'rgb(61, 135, 241)',
    borderLeftWidth: wp('1.3%'),
    marginTop: 16.5,
    borderRadius: 4,
    padding: '5%',
    paddingRight: '4%',
    flexDirection: 'row',
    backgroundColor: '#fff',
    justifyContent: 'space-between',
  },
  container: {
    width: Dimensions.get('window').width,
    height: hp('95%'),
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    // marginBottom:hp('50%'),
    // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },

  btnText: {
    fontSize: hp('2%'),
    fontFamily: 'SourceSansPro-Regular',
  },

  btn: {
    borderWidth: 1,
    borderColor: 'rgb(232, 232, 232)',
    width: wp('40%'),
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  parent2: {
    width: Dimensions.get('window').width,
    height: hp('95%'),
    flex: 11,
    backgroundColor: 'white',
    // marginBottom:hp('50%'),

    // padding: '5%'
  },

  buy_now_text: {
    fontSize: hp('2.3%'),
    fontFamily: 'SourceSansPro-Regular',
    color: 'rgb(61, 135, 241)',
  },

  subscription_text: {
    fontSize: hp('1.8%'),
    fontFamily: 'SourceSansPro-Regular',
    color: 'rgb(194, 194, 194)',
  },

  option_text: {
    fontSize: hp('1.2%'),
    fontFamily: 'SourceSansPro-Regular',
    color: 'rgb(68, 67, 67)',
    alignSelf: 'center',
  },
  projectsDisplaySection: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    flex: 3,
    alignItems: 'center',
    backgroundColor: 'white'
  },
});
export default Projlist;
