import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,

} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { useDispatch, useSelector } from 'react-redux';
// import * as actionCreator from '../../actions/team-action/index';
import { AuthContext } from '../../context';
import { SearchBar, Icon } from 'react-native-elements';
import { FlatList } from 'react-native-gesture-handler';
// import Pagination from '../Pagination'
import EmptyScreenHeader from '../components/EmptyScreens/EmptyScreenHeader'
import { Avatar, Card, Button, IconButton } from 'react-native-paper';
import config from '../config'
import ProjectSpecification1 from './ProjectSpecification/ProjectSpecification1';
import TouchableScale from 'react-native-touchable-scale';
import DropDown from '../components/ProjectChangeSpecification/DropDown'
import {
    drawings, projectTypes, components,
    accessories, areaOfInstallation, Termination
} from '../components/ProjectChangeSpecification/constants'
import { IndexPath, Layout, Select, SelectItem } from '@ui-kitten/components';


const ProjectChangeSpecification = (props) => {

    const dropDownData = [
        {
            title: 'Type of Project',
            data: projectTypes
        },
        {
            title: 'Drawings Needed',
            data: drawings
        },
        {
            title: 'Accessories Needed',
            data: accessories
        },
        {
            title: 'Area of installation',
            data: areaOfInstallation
        },
        {
            title: 'Termination',
            data: Termination
        }
    ]



    const Footer = () => (

        <View style={{
            flex: 1,
        }}>
            <View
                style={{
                    justifyContent: 'space-around',
                    flexDirection: 'row',
                }}
            >
                <Button
                    style={{ width: wp('27.8%'), borderWidth: 1, borderColor: '#3d87f1', backgroundColor: 'white' }}
                    labelStyle={{ color: '#3d87f1', textTransform: 'capitalize', fontSize: hp('1.6%') }}
                    mode="contained"
                    onPress={() => onPressPrevious()}>
                    Previous
                </Button>

                <Button
                    style={{ width: wp('30%'), borderWidth: 1, borderColor: 'white', backgroundColor: '#3d87f1' }}
                    labelStyle={{ color: 'white', textTransform: 'capitalize', fontSize: hp('1.6%') }}
                    mode="contained"
                    onPress={() => onPressProjectPlanning()}>
                    To Project Planning
                </Button>
                <TouchableOpacity
                    onPress={() => onPressSaveForLater()}
                    style={{ alignSelf: 'center' }}
                >
                    <Text style={styles.openText}>Save for later</Text>
                </TouchableOpacity>
            </View>
        </View>
    );

    // const [selectedIndex, setSelectedIndex] = React.useState([]);
    const [selectedDrawing, setSelectedDrawing] = useState([]);
    //'ghjk', selectedDrawing)
    // const [selectedAccessories, setSelectedAccessories] = useState([...gantt_data.selected_accessories]);
    // const [installationArea, setInstallationArea] = useState([...gantt_data.selected_area_of_installation]);
    // const [selectedTermination, setSelectedTermination] = useState([...gantt_data.selected_termination]);
    // const [inverterToACDB, setInverterToACDB] = useState([...gantt_data.selected_inverter_to_ACDB]);
    // const [ACDBToTransformer, setACDBToTransformer] = useState([...gantt_data.selected_ACDB_to_Transformer]);
    // const [transformerToHTPanel, setTransformerToHTPanel] = useState([...gantt_data.selected_Transformer_to_HT_Panel]);
    // const [selectComponent, setSelectComponent] = useState([...gantt_data.selected_components]);

    // const [HTSelector, setHTSelector] = useState(gantt_data.selected_termination[0] === 'HT' ? true : false);
    // const [LTSelector, setLTSelector] = useState(gantt_data.selected_termination[0] === 'LT' ? true : false);
    // const [spinning, setSpinning] = useState(false);

    return (
        <View style={styles.container}>
            <EmptyScreenHeader {...props} title='Change Spec..' />
            <View style={styles.body}>
                <Card
                    style={{ elevation: 8, shadowColor: 'rgba(0,0,0,0.4)', marginVertical: hp('2%'), marginHorizontal: wp('4%'), backgroundColor: '#fff', }}
                >
                    <View style={{ flexDirection: 'row', height: hp('7.4%'), justifyContent: 'space-between', paddingLeft: wp('3%'), paddingRight: wp('8%'), alignItems: 'center' }}>
                        <Text style={[styles.stageText, { fontSize: hp('2.6%'), paddingRight: wp('4%'), borderRightColor: 'rgb(179, 179, 179)', borderRightWidth: 1 }]}>Use your Exisiting Gantts</Text>
                        <Text style={styles.openText}>OPEN</Text>
                    </View>
                </Card>

                {
                    dropDownData.map(
                        (item, index) => {
                            return (
                                <View key={index}
                                    style={{
                                        marginVertical: hp('2%'),
                                        marginHorizontal: wp('3%')
                                    }}>
                                    {/* <DropDown 
                                        dataItems={item['data']} 
                                        placeHolder={item['title']}
                                        var={selectedDrawing}
                                        setMethod={setSelectedDrawing}
                                    /> */}
                                </View>
                            );
                        }
                    )
                }
            </View>
            <Footer />
        </View>
    );

};


const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: "white",
        flex: 1,
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    body: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        flex: 10,
        backgroundColor: 'white',
        paddingTop: hp('2%')
    },

    stageText: {
        fontSize: hp('2.3%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        paddingLeft: wp('2%')
    },

    openText: {
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-Regular',
        color: '#3d87f1',
        textDecorationLine: 'underline'
    },

});
// <TouchableScale
//     key={index}
//     onPress={() => onPressArrow(index)}
//     activeScale={0.95}
// >
//     <Card
//         style={{ elevation: 8, shadowColor: 'rgba(0,0,0,0.4)', marginVertical: hp('2%'), marginHorizontal: wp('4%'), backgroundColor: '#fff', }}
//     >
//         <View style={{ flexDirection: 'row', height: hp('7.4%'), justifyContent: 'space-between', paddingHorizontal: wp('3%'), alignItems: 'center' }}>
//             <Text style={styles.stageText}>{item}</Text>
//             <Icon color={'rgb(143, 143, 143)'} type='feather' name='chevron-down' />
//         </View>
//     </Card>
// </TouchableScale>

export default ProjectChangeSpecification;
