import React, { useState, useMemo, useEffect } from 'react';
import {
  View,
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Dimensions,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  Button
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import EmptyScreenHeader from './EmptyScreens/EmptyScreenHeader';
import FastImage from 'react-native-fast-image';
import color from 'color';
import Toast from 'react-native-simple-toast';
import notifyMessage from '../ToastMessages';
import * as actionCreators from "../actions/profile-action/index";
import { useDispatch } from "react-redux";
import { getToken } from '../App';

const Account = (props, { route, navigation }) => {


  const dispatch = useDispatch();
  const [id, setId] = useState("");
  const [email, setEmail] = useState('');
  const [currentpassword, setCurrentPassword] = useState("");
  const [newpassword, setNewPassword] = useState("");
  const [retypenewpassword, setRetypenewpassword] = useState("");
  // const id= getToken();

  useEffect(async () => {
    const userDetails = await AsyncStorage.getItem('userDetails');
    const value = JSON.parse(userDetails);
    setId(value.user)
    const loginDetails = await AsyncStorage.getItem('loginDetails');
    const loginvalue = JSON.parse(loginDetails);
    setEmail(loginvalue.email)
  }, []);
  const confirm = () => {
    // //"id", id)
    if (email && currentpassword && newpassword && retypenewpassword) {
      if (newpassword == retypenewpassword) {

        let passwordDetails = {
          id: id,
          password: newpassword,
        };
        //"passwordDetails", passwordDetails)
        Promise.resolve(dispatch(actionCreators.resetUserPassword(passwordDetails,props.route.params.token)))
          .then((res) => {
            if (res?.status === 200) {
              notifyMessage('An email has been sent to you with password reset instructions.');
            }
          });
      } else {
        notifyMessage("Please match new passowrd and Re-type new password");
      }
    } else {
      notifyMessage("Please fill all the fields");
    }
  }
  return (
    <>
      <KeyboardAvoidingView>

        <View style={styles.container}>
          <EmptyScreenHeader {...props} title='Account' />

          <View style={styles.parent2}>
            <ScrollView>
              <View style={{ display: 'flex', flexDirection: 'column' }}>
                <View style={{ backgroundColor: '#3d87f1', height: hp('35%'), justifyContent: 'center', alignItems: 'center' }}>
                  <View>
                    < FastImage
                      style={{ width: wp('38%'), height: hp('80%'), marginTop: -60, color: '#fff' }}
                      source={require('../assets/icons/SafEarth_Logo_White.png')}
                      resizeMode={FastImage.resizeMode.contain}
                    />
                  </View>

                </View>
                <View style={styles.item_card}>
                  <TextInput
                    placeholderTextColor="rgb(68,67,67)"
                    style={styles.textInput}
                    autoCapitalize="none"
                    value={email}
                    onChangeText={val => setEmail(val)}
                  />
                  <TextInput
                    placeholder="Current Password"
                    placeholderTextColor="rgb(179,179,179)"
                    style={styles.textInput}
                    autoCapitalize="none"
                    onChangeText={val => setCurrentPassword(val)}
                  />
                  <TextInput
                    placeholder="New Password"
                    placeholderTextColor="rgb(179,179,179)"
                    style={styles.textInput}
                    autoCapitalize="none"
                    onChangeText={val => setNewPassword(val)}
                  />
                  <TextInput
                    placeholder="Re-type New Password"
                    placeholderTextColor="rgb(179,179,179)"
                    style={styles.textInput}
                    autoCapitalize="none"
                    onChangeText={val => setRetypenewpassword(val)}
                  />

                  <TouchableOpacity style={styles.signIn} onPress={() => confirm()}>
                    <Text
                      style={[
                        styles.textSign,
                        {
                          color: '#fff',
                        },
                      ]}>
                      Confirm
                </Text>
                  </TouchableOpacity>


                </View>
                <View style={{ flexDirection: 'row', marginTop: 100, marginBottom: 50, marginRight: 30 }}>
                  < FastImage
                    style={{ width: wp('12%'), height: hp('4%') }}
                    source={require('../assets/icons/danger.png')}
                    resizeMode={FastImage.resizeMode.contain}
                  />
                  <Text style={{ color: 'rgb(143,143,143)', fontSize: 11, marginRight: 30 }}>More features of account settings can only be accessed on
our website. Please visit https://pmt.safearth.in/</Text>
                </View>
              </View>

            </ScrollView>
          </View>
        </View>

      </KeyboardAvoidingView>
    </ >
  )
}

const styles = StyleSheet.create({
  container: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "white",
    // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },

  parent2: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    flex: 11,
    backgroundColor: 'white',


  },
  item_card: {
    shadowColor: 'rgba(0,0,0,0.2)',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    height: hp('65%'),
    width: wp('88%'),
    marginRight: 10,
    shadowOpacity: 0.2,
    shadowRadius: 4.46,
    elevation: 2,
    borderWidth: 0,
    borderRadius: 4,
    marginTop: 16.5,
    borderTopLeftRadius: 4,
    borderBottomLeftRadius: 4,
    backgroundColor: '#fff',
    marginLeft: 20,
    marginTop: -80,
    // padding: '5%',
    paddingRight: '4%'
  },
  textInput: {
    width: wp('74%'),
    left: 25,
    top: 40,
    height: hp('8%'),
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: 'black',
    marginBottom: 30,
    backgroundColor: 'rgba(179,179,179, 0.1)',
    borderRadius: 5,
    fontFamily: 'SourceSansPro-Regular',
  },
  option_text: {
    fontSize: hp('1.2%'),
    fontFamily: 'SourceSansPro-Regular',
    color: 'rgb(68, 67, 67)',
    alignSelf: 'center'
  },
  signIn: {
    width: wp('74%'),
    height: hp('8%'),
    left: 25,
    top: 80,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    color: '#05375a',
    backgroundColor: '#3A80E3',
    borderRadius: 5,
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: 'SourceSansPro-Regular',
  },
  textSign: {
    width: wp('16.1%'),
    fontSize: 15,
    lineHeight: 20,
    fontFamily: 'SourceSansPro-Regular',
  },

  btnText: {
    fontSize: hp('2%'),
    fontFamily: 'SourceSansPro-Regular'
  },

  btn: {
    borderWidth: 1,
    borderColor: 'rgb(232, 232, 232)',
    width: wp('40%'),
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center'
  }

});

export default Account
