import React, { useRef, useEffect, useState } from 'react';
import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    PermissionsAndroid,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
} from 'react-native'; import { Searchbar, Button } from 'react-native-paper';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import EmptyScreenHeader from '../EmptyScreens/EmptyScreenHeader';
import { useDispatch, useSelector } from 'react-redux';
import { SearchBar, ButtonGroup, FAB, Icon } from 'react-native-elements';

import { FlatList } from 'react-native-gesture-handler';
import ActionSheet from 'react-native-actionsheet';
import Pagination from '../Pagination';
import FastImage from 'react-native-fast-image';
import * as actionCreator from '../../actions/project-docs-action/index';
import RNFetchBlob from 'rn-fetch-blob';
import { sortBy } from 'lodash';
import EmptyLoader from '../EmptyLoader';
import BottomSheet from "react-native-gesture-bottom-sheet";
import SelectMultiple from 'react-native-select-multiple';
import Search from '../teams/SearchComponent';
import Toast from 'react-native-toast-message';

var _ = require('lodash')

function Item({ id, name, department, project_name, stage, owner, file, date }) {

    const [selectedId, setselectedId] = React.useState(null);
    const [isExpandable, setisExpandable] = React.useState(false);
    
    const changeExpandable = (id) => {
        setisExpandable(!isExpandable)
        setselectedId(id)
    }
    const checkPermission = async (Image_URI) => {

        // Function to check the platform
        // If iOS then start downloading
        // If Android then ask for permission

        if (Platform.OS === 'ios') {
            downloadImage();
        } else {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                    {
                        title: 'Storage Permission Required',
                        message:
                            'App needs access to your storage to download Photos',
                    }
                );
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    // Once user grant the permission start downloading
                    //'Storage Permission Granted.');
                    downloadImage(Image_URI);
                } else {
                    // If permission denied then show alert
                    Toast.show({
                        type: 'error',
                        text1: 'Storage Permission',
                        text2: 'Storage Permission Not Granted'
                      });
                }
            } catch (err) {
                // To handle permission related exception
                console.warn(err);
            }
        }
    };

    const downloadImage = (URI) => {
        // Main function to download the image

        // To add the time suffix in filename
        let date = new Date();
        // Image URL which we want to download
        let image_URL = URI;
        // Getting the extention of the file
        let ext = getExtention(image_URL);
        ext = '.' + ext[0];
        // Get config and fs from RNFetchBlob
        // config: To pass the downloading related options
        // fs: Directory path where we want our image to download
        const { config, fs } = RNFetchBlob;
        let PictureDir = fs.dirs.PictureDir;
        let options = {
            fileCache: true,
            addAndroidDownloads: {
                // Related to the Android only
                useDownloadManager: true,
                notification: true,
                path:
                    PictureDir +
                    '/image_' +
                    Math.floor(date.getTime() + date.getSeconds() / 2) +
                    ext,
                description: 'Image',
            },
        };
        config(options)
            .fetch('GET', image_URL)
            .then(res => {
                // Showing alert after successful downloading
                //'res -> ', JSON.stringify(res));
                Toast.show({
                    type: 'success',
                    text1: 'File',
                    text2: 'File has been downloaded Successfully.'
                  });
            });
    };
    const getExtention = filename => {
        // To get the file extension
        return /[.]/.exec(filename) ?
            /[^.]+$/.exec(filename) : undefined;
    };
    return (
        <View style={styles.item_card}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <View>
                    <Text style={styles.name_text}>{name}</Text>
                    <Text style={styles.department_text}>{stage}</Text>
                </View>
                <View>
                    <TouchableOpacity
                        onPress={() => { checkPermission(file) }}
                    >
                        < FastImage
                            style={{ width: wp('12%'), height: hp('4%') }}
                            source={require('../../assets/icons/Download.png')}
                            resizeMode={FastImage.resizeMode.contain}
                        />
                    </TouchableOpacity>
                </View>
            </View>

            <Text style={{ height: hp('0.2%'), marginTop: 8, marginBottom: 8, backgroundColor: 'rgb(232, 232, 232)' }}></Text>
            <View style={{ display: 'flex', marginBottom: 10, flexDirection: 'row', justifyContent: 'space-between' }}>
                <View style={{width:wp('40%')}}>
                    <Text style={styles.email_text}>Project Name</Text>
                    <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>{project_name}</Text>
                </View>
                <View style={{width:wp('40%')}}>
                    <Text style={styles.email_text}>Owner</Text>
                    <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>{owner}</Text>
                </View>
            </View>
            <View>
                <Text style={[styles.department_text, { marginTop: 30 }]}>{date.split("T")[0]}</Text>
            </View>




        </View>
    )
}
const ProjectDocs = (props) => {


    const { docslist1, ownerdata } = props;

    const dispatch = useDispatch();
    const [searchQuery, setSearchQuery] = React.useState('');
    const [docslist, setdocslist] = React.useState(docslist1);
    const [selectedstage, setselectedstage] = React.useState([]);
    const [selectedowner, setselectedowner] = React.useState([]);

    const [sort, setsort] = useState('');
    const onChangeSearch = query => setSearchQuery(query);
    const [activeButton, setactiveButton] = React.useState(1);
    const [profileCreatedFor, handleProfileCreatedFor] = React.useState('');
    const fruits = ['Apples', 'Oranges', 'Pears']
    const stages = ['Approvals', 'Engineering', 'Procurement', 'Material Handling', 'Construction', 'Site Hand over', 'PRE-REQUISITES']

    const buttons = ['Stage', 'Owner'];
    const buttons1 = ['Apply'];

    const [onStockButtonPress, setonStockButtonPress] = React.useState(false);
    const [onConsumedButtonPress, setonConsumedButtonPress] = React.useState(
        false,
    );
    const [selectedIndex, setSelectedIndex] = React.useState(0);
    //  //"docsReducers.success.data.projects",docsReducers.success.data.projects)
    // const projectDocsReducer = useSelector((state) => {
    //     return state.projectDocsReducer.projectDocs.get;
    // });

    // useEffect(() => {
    //     dispatch(actionCreator.getProjectDocs(project_id, token));
    // }, []);

   

    const filterapply = () => {
     

        let selectedownerArray = [];
        if (selectedowner != '') {
            selectedowner.map((i, index) => {

                selectedownerArray.push(i.value)
            });
        } else {
            ownerdata.map((i, index) => {

                selectedownerArray.push(i)
            });
        }


        let selectedstageArray = [];
        if (selectedstage != '') {
            selectedstage.map((i, index) => {

                selectedstageArray.push(i.value)
            });
        } else {
            stages.map((i, index) => {

                selectedstageArray.push(i)
            });
        }




        var filterBy = { stage: selectedstageArray, owner: selectedownerArray },
            result = docslist1.filter(function (o) {
                return Object.keys(filterBy).every(function (k) {
                    return filterBy[k].some(function (f) {
                        return o[k] === f;
                    });
                });
            });
        setdocslist(result);
        bottomSheet.current.close();
    }
    const updateIndex = index => {
        if (index == 0) {
            onPressStock();
        } else {
            onPressConsumed();
        }
        setSelectedIndex(index);
    };

    const onPressStock = () => {
        setonStockButtonPress(true);
        setonConsumedButtonPress(false);
    };

    const onPressConsumed = () => {
        setonConsumedButtonPress(true);
        setonStockButtonPress(false);
    };

    useEffect(() => {
    }, [sort || selectedowner || selectedstage]);

    let actionSheet = useRef();
    let optionArray = [
        <View style={{ flexDirection: 'row', marginLeft: -280 }}>
            <FastImage
                style={{ width: wp('9%'), height: hp('3%') }}
                source={require('../../assets/icons/sort.png')}
                resizeMode={FastImage.resizeMode.contain}
            />
            <Text style={{ color: '#000', fontFamily: 'SourceSansPro-SemiBold', fontSize: hp('2.3%') }}>Sort By</Text></View>,
        <View style={{ flexDirection: 'row', marginLeft: -300 }}>
            < FastImage
                style={{ width: wp('9%'), height: hp('3%') }}
                source={require('../../assets/icons/filter.png')}
                resizeMode={FastImage.resizeMode.contain}
            />
            <Text style={{ color: '#000', fontFamily: 'SourceSansPro-SemiBold', fontSize: hp('2.3%') }}>Filter</Text></View>,
        <View style={{ flexDirection: 'row', marginLeft: -255 }}>
            <Text style={{ color: '#000', fontFamily: 'SourceSansPro-SemiBold', fontSize: hp('2.3%') }}>Cancel</Text></View>,
    ]


    let actionSheet1 = useRef();
    let optionArray1 = [
        <View style={{ flexDirection: 'row', marginLeft: -300 }}>
            {/* <FastImage
                style={{ width: wp('9%'), height: hp('3%') }}
                source={require('../../assets/icons/sort.png')}
                resizeMode={FastImage.resizeMode.contain}
            /> */}
            <Text style={{ color: '#000', fontFamily: 'SourceSansPro-SemiBold', fontSize: hp('2.3%') }}>date</Text></View>,
        <View style={{ flexDirection: 'row', marginLeft: -300 }}>
            {/* < FastImage
                style={{ width: wp('9%'), height: hp('3%') }}
                source={require('../../assets/icons/filter.png')}
                resizeMode={FastImage.resizeMode.contain}
            /> */}
            <Text style={{ color: '#000', fontFamily: 'SourceSansPro-SemiBold', fontSize: hp('2.3%') }}>stage</Text></View>,
        <View style={{ flexDirection: 'row', marginLeft: -300 }}>
            {/* < FastImage
                style={{ width: wp('9%'), height: hp('3%') }}
                source={require('../../assets/icons/filter.png')}
                resizeMode={FastImage.resizeMode.contain}
            /> */}
            <Text style={{ color: '#000', fontFamily: 'SourceSansPro-SemiBold', fontSize: hp('2.3%') }}>owner</Text></View>,
    ]

    const showActionSheet = () => {
        actionSheet.current.show();
    }
    const showActionSheet1 = (index) => {

        if (index == 0) {
            actionSheet1.current.show();

        } else if (index == 1) {
            bottomSheet.current.show()

        }

    }
    const sortBy = (index) => {
        setsort(index)
        if (index == 0) {
            docslist.sort(function (a, b) {
                var x = a.date; var y = b.date;
                return ((x < y) ? -1 : ((x > y) ? 1 : 0));
            });
            setdocslist(docslist)
        } else if (index == 1) {
            docslist.sort(function (a, b) {
                var x = a.stage; var y = b.stage;
                return ((x < y) ? -1 : ((x > y) ? 1 : 0));
            });
            setdocslist(docslist)

        } else {
            docslist.sort(function (a, b) {
                var x = a.owner; var y = b.owner;
                return ((x < y) ? -1 : ((x > y) ? 1 : 0));
            });
            setdocslist(docslist)


        }

    }
    const bottomSheet = useRef();


    const [search, setSearch] = React.useState('');
    const SetSearch = (query) => {
      setSearch(query)
    }
    const filterItems = (items, filter) => {
        return items.filter(item => item.doc_name.toLowerCase().includes(filter.toLowerCase()))
    }
    return (
        <SafeAreaView style={{ backgroundColor: "#fff" }}>
            <KeyboardAvoidingView>
            <EmptyScreenHeader {...props} title='Documents' />

                <View style={styles.container}>
                    <View style={styles.parent2}>

                        <View style={{ display: 'flex', marginTop: 24, flexDirection: 'row', justifyContent: 'space-between' }}>
                        
                        <Search
        style={{ justifyContent: 'center', alignItems: 'center', marginBottom: '5%', marginTop: '2%' }}
          setSearch={SetSearch}
          search={search}
        />
                            <TouchableOpacity
                                onPress={() => { showActionSheet() }}
                                style={{}}                            >
                                < FastImage
                                    style={{ width: wp('12%'), height: hp('4%') }}
                                    source={require('../../assets/icons/Options.png')}
                                    resizeMode={FastImage.resizeMode.contain}
                                />
                                <Text style={styles.option_text}>Options</Text>
                            </TouchableOpacity>
                            <ActionSheet
                                ref={actionSheet}
                                options={optionArray}
                                cancelButtonIndex={2}
                                destructiveButtonIndex={1}
                                onPress={(index) => { showActionSheet1(index) }}
                            />
                            <ActionSheet
                                ref={actionSheet1}
                                options={optionArray1}
                                destructiveButtonIndex={1}
                                title={'Sort By'}
                                onPress={(index) => { sortBy(index) }}
                            />
                            <BottomSheet useNativeDriver={false} hasDraggableIcon ref={bottomSheet} height={850} >
                                <Text style={{ color: '#000', marginLeft: 30, fontFamily: 'SourceSansPro-SemiBold', fontSize: hp('2.3%') }}>Filter</Text>
                                <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                                    <Text style={{ height: hp('0.3%'), width: wp('85%'), marginTop: 8, marginBottom: 8, backgroundColor: 'rgb(232, 232, 232)' }}></Text>
                                    <ButtonGroup
                                        onPress={updateIndex}
                                        selectedIndex={selectedIndex}
                                        buttons={buttons}
                                        containerStyle={{ height: hp('5.5%'), width: wp('80%'), borderWidth: 0 }}
                                        textStyle={{
                                            fontFamily: 'SourceSansPro-Regular',
                                            fontSize: hp('2%'),
                                        }}
                                        selectedButtonStyle={{ backgroundColor: '#3d87f1' }}
                                    />
                                    {
                                        onConsumedButtonPress ? (
                                            <SelectMultiple
                                                style={{ width: wp('100%'), marginTop: 10 }}
                                                items={ownerdata}
                                                selectedItems={selectedowner}
                                                onSelectionsChange={(value) => setselectedowner(value)} />) : (
                                            <SelectMultiple
                                                style={{ width: wp('100%'), marginTop: 10 }}
                                                items={stages}
                                                selectedItems={selectedstage}
                                                onSelectionsChange={(value) => setselectedstage(value)} />

                                        )}

                                    <ButtonGroup
                                        onPress={filterapply}
                                        buttons={buttons1}
                                        containerStyle={{ height: hp('5.5%'), width: wp('80%'), borderWidth: 0, marginTop: 20, backgroundColor: '#3d87f1' }}
                                        textStyle={{
                                            fontFamily: 'SourceSansPro-Regular',
                                            fontSize: hp('2%'),
                                            color: '#fff'
                                        }}
                                    />

                                </View>


                            </BottomSheet>

                        </View>
                        <Pagination activeButton={activeButton} setactiveButton={setactiveButton} reducerLength={docslist.length} />
                        <Text style={{ height: hp('0.2%'), marginTop: 8, marginBottom: 8, backgroundColor: 'rgb(232, 232, 232)' }}></Text>


                        <FlatList
                            data={

                                filterItems(docslist.slice(activeButton * 10 - 10, activeButton * 10), search)
                            }
                            renderItem={
                                ({ item, index }) => (
                                    <Item
                                        name={item.doc_name}
                                        department={item.department}
                                        project_name={item.project_name}
                                        owner={item.owner}
                                        date={item.date}
                                        file={item.file}
                                        stage={item.stage}
                                    />
                                )
                            }
                        />
                        <View style={{ height:70, width: '100%' }}>

                        </View>
                    </View>
                </View>
            </KeyboardAvoidingView>
        </SafeAreaView >
    );

};
const styles = StyleSheet.create({
    name_text: {
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-SemiBold',
        color: 'rgb(68, 67, 67)',
    },

    email_text: {
        fontSize: hp('1.8%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
    },

    department_text: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-SemiBold',
        color: 'rgb(194, 194, 194)',
    },

    

    item_card: {
        shadowColor: 'rgba(0,0,0,0.4)',
        elevation: 8,
        borderLeftColor: '#3d87f1',
        backgroundColor: '#fff',
        borderRadius: 4,
        borderLeftWidth: 3,
        marginTop: hp('2%'),
        padding: '5%',
        paddingRight: '4%',
    },

    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
        
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },


    parent2: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        flex: 11,
        backgroundColor: 'white',
        padding: '5%'

    },

    buy_now_text: {
        fontSize: hp('2.3%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(61, 135, 241)'
    },

    subscription_text: {
        fontSize: hp('1.8%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(194, 194, 194)'
    },

    option_text: {
        fontSize: hp('1.2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center'

    },

});
export default ProjectDocs;
