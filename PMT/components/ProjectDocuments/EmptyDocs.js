import React, { useRef, useEffect, useState } from 'react';
import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    PermissionsAndroid,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
} from 'react-native'; import { Searchbar, Button } from 'react-native-paper';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import EmptyScreenHeader from '../EmptyScreens/EmptyScreenHeader';
import { useDispatch, useSelector } from 'react-redux';
import { SearchBar, ButtonGroup, FAB, Icon } from 'react-native-elements';

import { FlatList } from 'react-native-gesture-handler';
import ActionSheet from 'react-native-actionsheet';
import Pagination from '../Pagination';
import FastImage from 'react-native-fast-image';
import * as actionCreator from '../../actions/project-docs-action/index';
import RNFetchBlob from 'rn-fetch-blob';
import { sortBy } from 'lodash';
import EmptyLoader from '../EmptyLoader';
import BottomSheet from "react-native-gesture-bottom-sheet";
import SelectMultiple from 'react-native-select-multiple';
var _ = require('lodash')


const EmptyDocs = (props) => {

    //('EmptyDocs', props)

    const { docslist1, ownerdata } = props;

    const dispatch = useDispatch();
    const [searchQuery, setSearchQuery] = React.useState('');
    const [docslist, setdocslist] = React.useState(docslist1);
    const [selectedstage, setselectedstage] = React.useState('');
    const [selectedowner, setselectedowner] = React.useState('');

    const [sort, setsort] = useState('');
    const onChangeSearch = query => setSearchQuery(query);
    const [activeButton, setactiveButton] = React.useState(1);
    const [profileCreatedFor, handleProfileCreatedFor] = React.useState('');
    const fruits = ['Apples', 'Oranges', 'Pears']
    const stages = ['Approvals', 'Engineering', 'Procurement', 'Material Handling', 'Construction', 'Site Hand over', 'PRE-REQUISITES']

    const buttons = ['Stage', 'Owner'];
    const buttons1 = ['Apply'];

    const [onStockButtonPress, setonStockButtonPress] = React.useState(false);
    const [onConsumedButtonPress, setonConsumedButtonPress] = React.useState(
        false,
    );
    const [selectedIndex, setSelectedIndex] = React.useState(0);
    //  //"docsReducers.success.data.projects",docsReducers.success.data.projects)
    // const projectDocsReducer = useSelector((state) => {
    //     return state.projectDocsReducer.projectDocs.get;
    // });

    // useEffect(() => {
    //     dispatch(actionCreator.getProjectDocs(project_id, token));
    // }, []);

    

    return (
        <SafeAreaView style={{ backgroundColor: "#fff" }}>
            <KeyboardAvoidingView>
            <EmptyScreenHeader {...props} title='Documents' />

                <View style={styles.container}>
                    <View style={styles.parent2}>

                    < FastImage
                                style={{ width: wp('30%'), height: hp('40%') }}
                                source={require('../../assets/icons/group.png')
                                }
                                resizeMode={FastImage.resizeMode.contain}
                            />

                            <Text style={{ width: wp('100%'), height: hp('5%'), textAlign: 'center', fontSize: hp('2.5%'), color: 'rgb(143,143,143)' }}>No documents to display.</Text>
                            <Text style={{ width: wp('100%'), height: hp('4%'), fontSize: hp('2%'), textAlign: 'center', color: 'rgb(190,198,214)' }}>Please come back when a new project is added</Text>

                    </View>
                </View>
            </KeyboardAvoidingView>
        </SafeAreaView >
    );

};
const styles = StyleSheet.create({
    name_text: {
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-SemiBold',
        color: 'rgb(68, 67, 67)',
    },

    email_text: {
        fontSize: hp('1.8%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
    },

    department_text: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-SemiBold',
        color: 'rgb(194, 194, 194)',
    },

    item_card: {
        shadowColor: 'rgba(0,0,0,0.2)',
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.2,
        shadowRadius: 4.46,
        elevation: 2,
        borderWidth: 0,
        borderRadius: 4,
        borderLeftColor: 'rgb(61, 135, 241)',
        borderLeftWidth: wp('1.3%'),
        marginTop: 16.5,
        borderTopLeftRadius: 4,
        borderBottomLeftRadius: 4,
        padding: '5%',
        paddingRight: '4%'
    },

    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
        
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },


    parent2: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        flex: 6,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        padding: '5%'

    },

    buy_now_text: {
        fontSize: hp('2.3%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(61, 135, 241)'
    },

    subscription_text: {
        fontSize: hp('1.8%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(194, 194, 194)'
    },

    option_text: {
        fontSize: hp('1.2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center'

    },

});
export default EmptyDocs;
