import React, { Component } from 'react';
import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
} from 'react-native';
import LottieView from 'lottie-react-native';

export default class EmptyLoader extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (

            <SafeAreaView>
                <View style={[styles.container]}>
                    <LottieView
                        style={{ height: 180, width: 180 }}
                        source={require('../data.json')}
                        autoPlay
                        loop={false}
                        onAnimationFinish={this.props.onAnimationFinish}
                    />
                </View>
            </SafeAreaView>


        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    text: {
        color: 'red',
        fontSize: 25,
    },
});