import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    Animated,
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image';
import { useDispatch, useSelector } from 'react-redux';
import { SearchBar } from 'react-native-elements';
import { FlatList } from 'react-native-gesture-handler';
import { ButtonGroup } from 'react-native-elements';
import {
    useCollapsibleSubHeader,
    CollapsibleSubHeaderAnimator,
} from 'react-navigation-collapsible';
import { FAB, Icon } from 'react-native-elements';
import { Avatar, Button, Divider, Card, IconButton } from 'react-native-paper';
import Icons from "react-native-vector-icons/Ionicons";


const ReportList = (props) => {
    const {
        onScroll /* Event handler */,
        containerPaddingTop /* number */,
        scrollIndicatorInsetTop /* number */,
        translateY,
    } = useCollapsibleSubHeader();

    const data = props.data

    const ReportItem = (props) => {

        const { image_url, project_name, date, id } = props.item;

        const [selectedId, setselectedId] = React.useState(null);
        const [isClickOnEllipses, setisClickOnEllipses] = React.useState(false);

        const OnPressEllipses = (index) => {
            setisClickOnEllipses(!isClickOnEllipses)
            setselectedId(index)
        }

        const EllipsesCard = (params) => {
            const downloadPO = (params) => {

            }

            const duplicate = (params) => {

            }

            const reAssign = (params) => {

            }

            const assign = (params) => {

            }

            return (
                <View style={styles.ellipsesCard}>
                    <TouchableOpacity
                        onPress={() => { downloadPO() }}
                        style={styles.ellipsesbtn}
                    >
                        <Text style={styles.ellipsestxt}>Download</Text>
                    </TouchableOpacity>


                    <TouchableOpacity
                        onPress={() => { duplicate() }}
                        style={styles.ellipsesbtn}
                    >
                        <Text style={styles.ellipsestxt}>Delete</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => { reAssign() }}
                        style={styles.ellipsesbtn}
                    >
                        <Text style={styles.ellipsestxt}>Share</Text>
                    </TouchableOpacity>
                </View>
            );


        }


        return (
            <Card
                style={{ elevation: 2, backgroundColor: '#fff', marginBottom: hp('1.5%'), shadowColor: 'rgba(0,0,0,1)' }}
            >
                <Card.Title
                    titleStyle={styles.titleStyle}
                    leftStyle={{ width: wp('15%') }}
                    title={project_name}
                    subtitle={date}
                    subtitleStyle={{ color: 'rgb(179, 179, 179)' }}
                    left={(props) =>
                        <FastImage
                            style={{ width: wp('14%'), height: hp('6%'), borderRadius: 0 }}
                            source={{ uri: image_url }}
                            resizeMode={FastImage.resizeMode.center}
                        />
                    }
                    right={(props) =>
                        <TouchableOpacity>
                            <Icons.Button
                                name="ellipsis-vertical"
                                size={25}
                                color='rgb(143, 143, 143)'
                                backgroundColor='white'
                                onPress={() => { OnPressEllipses(id) }}
                            />
                        </TouchableOpacity>}
                />

                { selectedId == id && isClickOnEllipses ? <EllipsesCard /> : null}

            </Card>
        );
    }

    return (
        <View style={styles.container}>
            <View style={{ height: hp('9%'), justifyContent: 'space-between', padding: '5%', flexDirection: 'row' }}>
                <Text style={styles.option_text}>{data.title}</Text>
                <Icon
                    iconStyle={{ fontSize: hp('4.5%') }}
                    name='close-outline'
                    type='ionicon'
                    color='rgb(68, 67, 67)'
                    onPress={() => props.setisSelectedCard(false)}
                />
            </View>

            <Divider style={{ backgroundColor: 'rgb(232,232,232)', marginHorizontal: '5%', height: hp('0.3%') }} />

            <View style={{ marginHorizontal: '5%' }}>
                <Animated.FlatList
                    keyExtractor={(item, index) => index}
                    ListEmptyComponent={
                        <View style={{justifyContent: 'center', alignItems: 'center', height: hp('80%')}}>
                            <Text style={styles.ellipsestxt}>
                                No reports are available for this section.
                            </Text>
                        </View>
                    }
                    data={data.data}
                    renderItem={({ item, index }) => <ReportItem item={item} id={index} />}
                    onScroll={onScroll}
                    contentContainerStyle={{
                        paddingBottom: hp('1%'),
                    }}
                    scrollIndicatorInsets={{ top: scrollIndicatorInsetTop }}
                />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        flex: 1,
        backgroundColor: '#fff',
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    option_text: {
        fontSize: hp('3%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
    },

    titleStyle: {
        fontFamily: 'SourceSansPro-Semibold',
        fontSize: hp('2%'),
        color: 'rgb(68, 67, 67)'
    },

    ellipsesCard: {
        position: 'absolute',
        marginLeft: (Dimensions.get('window').width) * 0.4,
        marginTop: hp('6.5%'),
        // zIndex: 2,
        backgroundColor: 'white',
        width: wp('42%'),
        height: hp('19%'),
        borderWidth: 0,
        shadowColor: 'rgba(0,0,0,0.4)',
        elevation: 10,
        padding: '5%',
        borderRadius: 4,
    },

    ellipsesbtn: {
        marginVertical: 5,
    },

    ellipsestxt: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(143, 143, 143)',
    }
});

export default ReportList;