import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    Animated,
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image';
import { useDispatch, useSelector } from 'react-redux';
import { SearchBar } from 'react-native-elements';
import { FlatList } from 'react-native-gesture-handler';
import EmptyScreenHeader from '../EmptyScreens/EmptyScreenHeader';
import { ButtonGroup } from 'react-native-elements';
import {
    useCollapsibleSubHeader,
    CollapsibleSubHeaderAnimator,
} from 'react-navigation-collapsible';
import { FAB, Icon } from 'react-native-elements';
import { Avatar, Button, Card, IconButton } from 'react-native-paper';
import Icons from "react-native-vector-icons/Ionicons";
import checkPermission from '../../components/downloadFile';


const Gantts = props => {

    const onPressCard = (index) => {
        if(index == 0) {
            checkPermission(props.data.details_url)
        } else {
            checkPermission(props.data.basic_url)
        }
    }

    const ganttTypes = ['Detailed Gantt', 'Basic Gantt']

    return (
        <View style={{ marginHorizontal: wp('8%'), marginTop: hp('3%'), borderWidth: 0 }}>
            {
                ganttTypes.map((i, index) => {
                    return (
                        <TouchableOpacity
                            key={index}
                            onPress={() => onPressCard(index)}
                        >
                            <View
                                style={{ backgroundColor: 'rgb(236, 243, 254)', alignItems: 'center', justifyContent: 'flex-start', borderRadius: 4, paddingHorizontal: wp('3%'), flexDirection: 'row', borderWidth: 0, height: hp('7.4%'), marginBottom: hp('2%') }}
                            >
                                <Icons.Button
                                    name="folder"
                                    size={25}
                                    color='#3d87f1'
                                    backgroundColor='rgb(236, 243, 254)'
                                />
                                <Text style={styles.option_text}>{i}</Text>
                            </View>
                        </TouchableOpacity>
                    );
                })
            }
        </View>
    );

};

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        flex: 1,
        backgroundColor: '#fff',
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    option_text: {
        fontSize: hp('2.2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center',
    },

    // textMessage: {
    //     fontFamily: 'SourceSansPro-Regular',
    //     fontSize: hp('2%'),
    //     color: '#3d87f1'
    // },
});

export default Gantts;
