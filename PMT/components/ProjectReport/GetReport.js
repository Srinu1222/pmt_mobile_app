import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    Animated,
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image';
import { useDispatch, useSelector } from 'react-redux';
import { SearchBar } from 'react-native-elements';
import { FlatList } from 'react-native-gesture-handler';
import EmptyScreenHeader from '../EmptyScreens/EmptyScreenHeader';
import { ButtonGroup } from 'react-native-elements';
import {
    useCollapsibleSubHeader,
    CollapsibleSubHeaderAnimator,
} from 'react-navigation-collapsible';
import { FAB, Icon } from 'react-native-elements';
import { Avatar, Button, Card, IconButton } from 'react-native-paper';
import Icons from "react-native-vector-icons/Ionicons";
import ReportList from './ReportList'
import Gantts from './Gantts'



const GetReport = props => {

    const [selectedCardIndex, setselectedCardIndex] = React.useState(null);
    const [isSelectedCard, setisSelectedCard] = React.useState(false);


    const onPressCard = (index) => {
        setisSelectedCard(true)
        setselectedCardIndex(index)
    }

    const buttons = ['Updates', 'Gantts']
    const [selectedIndex, setselectedIndex] = React.useState(0);

    const reportTypes = props.data.reports_list

    const ganttData = {
        'basic_url': props.data.basic_gantt_url,
        'details_url': props.data.details_gantt_url
    }
    
    // const reportTypes = [
    //     {
    //         'title': 'Daily Updates',
    //         'count': 6,
    //         'data': [
    //             {
    //                 'id': 1,
    //                 'image_url': 'https://safearth-pmt-static.s3.amazonaws.com/project_images/Screenshot_1.png',
    //                 'project_name': 'PMT',
    //                 'date': '12/02/2020'
    //             },
    //             {
    //                 'id': 2,
    //                 'image_url': 'https://safearth-pmt-static.s3.amazonaws.com/project_images/Screenshot_1.png',
    //                 'project_name': 'PMT',
    //                 'date': '12/02/2020'
    //             },
    //             {
    //                 'id': 3,
    //                 'image_url': 'https://safearth-pmt-static.s3.amazonaws.com/project_images/Screenshot_1.png',
    //                 'project_name': 'PMT',
    //                 'date': '12/02/2020'
    //             },
    //             {
    //                 'id': 4,
    //                 'image_url': 'https://safearth-pmt-static.s3.amazonaws.com/project_images/Screenshot_1.png',
    //                 'project_name': 'PMT',
    //                 'date': '12/02/2020'
    //             }
    //         ]
    //     },
    //     {
    //         'title': 'Weekly Updates',
    //         'count': 4,
    //         'data': [
    //             {
    //                 'id': 1,
    //                 'image_url': 'https://safearth-pmt-static.s3.amazonaws.com/project_images/Screenshot_1.png',
    //                 'project_name': 'PMT',
    //                 'date': '12/02/2020'
    //             }
    //         ]
    //     },
    //     {
    //         'title': 'Monthly Updates',
    //         'count': 8,
    //         'data': [
    //             {
    //                 'id': 1,
    //                 'image_url': 'https://safearth-pmt-static.s3.amazonaws.com/project_images/Screenshot_1.png',
    //                 'project_name': 'PMT',
    //                 'date': '12/02/2020'
    //             }
    //         ]
    //     },
    // ]

    const renderGetReport = (params) => {
        if (isSelectedCard) {
            return (<ReportList setisSelectedCard={setisSelectedCard} data={reportTypes[selectedCardIndex]} />);
        } else {
            return (
                <View style={styles.container}>
                    <EmptyScreenHeader title='Reports' navigation={props.navigation} />
                    <View style={{ flex: 11 }}>
                        <View style={{ marginHorizontal: wp('8%'), borderWidth: 0 }}>
                            <ButtonGroup
                                onPress={(index) => setselectedIndex(index)}
                                buttons={buttons}
                                containerStyle={{ height: hp('5.3%'), marginTop: hp('3%') }}
                                selectedIndex={selectedIndex}
                                buttonContainerStyle={{ backgroundColor: 'white' }}
                                selectedButtonStyle={{ backgroundColor: '#3d87f1', }}
                                textStyle={{
                                    fontSize: hp('2%'),
                                    fontFamily: 'SourceSansPro-Regular',
                                    color: 'rgb(143, 143, 143)'
                                }}
                            />
                        </View>
                        {
                            selectedIndex == 1 ?
                                <Gantts data={ganttData} />
                                :
                                <View style={{ marginHorizontal: wp('8%'), marginTop: hp('3%'), borderWidth: 0 }}>
                                    {
                                        reportTypes.map((i, index) => {
                                            return (
                                                <TouchableOpacity
                                                    key={index}
                                                    onPress={() => onPressCard(index)}
                                                >
                                                    <View
                                                        style={{ backgroundColor: 'rgb(236, 243, 254)', alignItems: 'center', justifyContent: 'space-between', borderRadius: 4, paddingHorizontal: wp('3%'), flexDirection: 'row', borderWidth: 0, height: hp('7.4%'), marginBottom: hp('2%') }}
                                                    >
                                                        <View style={{ flexDirection: 'row' }}>
                                                            <Icons.Button
                                                                name="folder"
                                                                size={25}
                                                                color='#3d87f1'
                                                                backgroundColor='rgb(236, 243, 254)'
                                                            />
                                                            <Text style={styles.option_text}>{i.title}</Text>
                                                        </View>
                                                        <Text style={styles.textMessage}>{i.count}</Text>
                                                    </View>
                                                </TouchableOpacity>
                                            );
                                        })
                                    }
                                </View>
                        }

                    </View>
                </View>
            );
        }
    }

    return renderGetReport();
};

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        flex: 1,
        backgroundColor: '#fff',
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    option_text: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center',
    },

    textMessage: {
        fontFamily: 'SourceSansPro-Regular',
        fontSize: hp('2%'),
        color: '#3d87f1'
    },
});

export default GetReport;
