import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    Animated,
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image';
import { useDispatch, useSelector } from 'react-redux';
import { SearchBar } from 'react-native-elements';
import { FlatList } from 'react-native-gesture-handler';
import EmptyScreenHeader from '../EmptyScreens/EmptyScreenHeader';
import { ButtonGroup } from 'react-native-elements';
import {
    useCollapsibleSubHeader,
    CollapsibleSubHeaderAnimator,
} from 'react-navigation-collapsible';
import { FAB, Icon } from 'react-native-elements';
import { Avatar, Button, Checkbox, Card, Divider, IconButton } from 'react-native-paper';
import * as actionCreators from '../../actions/specific-project-dashboard-action/index';
import Icons from "react-native-vector-icons/Ionicons";
import TouchableScale from 'react-native-touchable-scale';
import EmptyLoader from '../../components/EmptyLoader'


const ProjectSpecification2 = props => {

    const {
        onScroll /* Event handler */,
        containerPaddingTop /* number */,
        scrollIndicatorInsetTop /* number */,
        translateY,
    } = useCollapsibleSubHeader();

    const stage = props.stage
    
    const stageEventsList = props.specReducerdata

    const [selectedEventId, setselectedEventId] = useState(null)
    const [isExpandable, setisExpandable] = useState(false)

    const dispatch = useDispatch();
    const onPressArrow = (id) => {
        setselectedEventId(id)
        setisExpandable(!isExpandable)
        dispatch(actionCreators.getProjectSpcificationEvents(props.token, id))
    }

    const specificationEventReducer = useSelector(state => {
        return state.specificationEventReducers.specEvent.get
    })

    const ExpandableCard = (props) => {
        if (specificationEventReducer.success.ok) {
            if (specificationEventReducer.success.data.list_of_specifications.length == 0) {
                return <Text>No Data</Text>
            } else {
                if (['Approvals', 'Procurement', 'Construction', 'Site Hand Over', 'Material Handling' ].includes(stage) ) {
                    return (
                        specificationEventReducer.success?.
                            data?.list_of_specifications.map(i => {
                                return Object.entries(i).map(([key, value]) => {
                                    return (
                                        <View key={index} style={{ backgroundColor: index % 2 == 0 && 'rgb(236, 243, 254)', justifyContent: 'center', height: hp('5.6%'), }}>
                                            <Text style={styles.itemText}>
                                                {
                                                    key.includes('_') ?
                                                        key.split('_').join(' ') :
                                                        key
                                                }
                                            </Text>
                                            <Text style={styles.itemText}>{value}</Text>
                                        </View>
                                    )
                                })
                            })
                    );
                } else if (stage == 'Engineering') {
                    return (
                        specificationEventReducer.success?.
                            data?.list_of_specifications.map((i, index) => {
                                if (props.data.name === 'List of Drawings') {
                                    return (
                                        <View key={index} style={{backgroundColor: index%2==0&&'rgb(236, 243, 254)', justifyContent: 'center', height: hp('5.4%')}}>
                                            <Text style={styles.itemText}>{i.drawing}</Text>
                                        </View>
                                    );
                                } else {
                                    return Object.entries(i).map(([key, value]) => {
                                        return (
                                            <View key={index} style={{flexDirection: 'row', backgroundColor: index % 2 == 0 && 'rgb(236, 243, 254)', justifyContent: 'space-between', alignItems: 'center', height: hp('5.6%'), }}>
                                                <Text style={styles.itemText}>
                                                    {
                                                        key.includes('_') ?
                                                        key.split('_').join(' ') :
                                                        key
                                                    }
                                                </Text>
                                                <Text style={styles.itemText}>{value}</Text>
                                            </View>
                                        )
                                    })
                                }
                            })
                    );
                } 
                
            }
        } else {
            return <EmptyLoader />;
        }
    }

    const FlatlistItem = (props) => {
        const { item, index } = props

        return (

            <Card
                style={{ elevation: 2, paddingHorizontal: wp('0%'), shadowColor: 'rgba(0,0,0,1)', marginVertical: hp('1%'), marginHorizontal: wp('5%'), backgroundColor: '#fff', }}
            >
                <TouchableScale
                    key={index}
                    onPress={() => onPressArrow(item.id)}
                    activeScale={1}
                >
                    <View style={{ flexDirection: 'row', height: hp('7.9%'), paddingHorizontal: wp('3%'), justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={styles.stageText}>{item.name}</Text>
                        {
                            isExpandable ?
                                <Icon color={'rgb(143, 143, 143)'} type='feather' name='chevron-up' />
                                :
                                <Icon color={'rgb(143, 143, 143)'} type='feather' name='chevron-down' />
                        }

                    </View>
                </TouchableScale>

                {
                    (isExpandable && selectedEventId == item.id) && <ExpandableCard data={item} />
                }

            </Card>
        );
    }

    const Empty = (params) => {
        return (
            <View style={{ justifyContent: 'center', alignItems: 'center', height: Dimensions.get('window').height * 0.7, }}>
                <Text style={{ fontSize: hp('4%'), color: 'rgb(179, 179, 179)' }}>No Data</Text>
            </View>
        );
    }

    return (
        <View style={styles.container}>
            <View style={{ height: hp('8.5%'), justifyContent: 'space-between', padding: '4%', flexDirection: 'row' }}>
                <Text style={styles.option_text}>{stage}</Text>
                <Icon
                    iconStyle={{ fontSize: hp('4.5%') }}
                    name='close-outline'
                    type='ionicon'
                    color='rgb(68, 67, 67)'
                    onPress={() => props.navigation.navigate('projectSpecification')}
                />
            </View>

            <Divider style={{ backgroundColor: 'rgb(232,232,232)', marginHorizontal: '5%', height: hp('0.3%') }} />

            <View style={{ marginTop: hp('2%') }}>
                <Animated.FlatList
                    ListEmptyComponent={<Empty />}
                    keyExtractor={(item, index) => index}
                    data={stageEventsList}
                    renderItem={
                        ({ item, index }) => <FlatlistItem index={index} item={item} />}
                    onScroll={onScroll}
                    contentContainerStyle={{
                        paddingBottom: hp('2%'),
                    }}
                    scrollIndicatorInsets={{ top: scrollIndicatorInsetTop }}
                />
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: '#fff',
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    option_text: {
        fontSize: hp('3%'),
        fontFamily: 'SourceSansPro-Semibold',
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center',
        // fontWeight: "100"
    },

    itemText: {
        fontSize: hp('2.3%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        textTransform: 'capitalize',
        paddingLeft: wp('3%')
    },

    stageText: {
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-Semibold',
        color: 'rgb(68, 67, 67)',

    },

    textMessage: {
        fontFamily: 'SourceSansPro-Regular',
        fontSize: hp('2%'),
        color: '#3d87f1'
    },
});

export default ProjectSpecification2;
