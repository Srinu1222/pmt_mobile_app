import { View, Text } from 'react-native'
import React, { useEffect } from "react";
import * as actionCreators from '../../actions/specific-project-dashboard-action/index';
import { useDispatch, useSelector } from "react-redux";
import { parseSync } from '@babel/core';
import { heightPercentageToDP } from 'react-native-responsive-screen';
import EmptyLoader from '../../components/EmptyLoader'
import config from '../../config'
import ProjectSpecification2 from './ProjectSpecification2'


const ProjectSpecificationStageList = (props) => {

    const dispatch = useDispatch();
    const token = props.route.params.token

    const stage = props.route.params.stage

    useEffect(() => {
        dispatch(actionCreators.getProjectSpcification(token, stage.toUpperCase(), props.route.params.project_id));
    }, [props]);

    const specificationReducer = useSelector(state => {
        return state.specificationReducers.spec.get;
    });

    const renderProjectSpecification = (params) => {
        if (specificationReducer.success.ok) {
            return <ProjectSpecification2
                style={{ backgroundColor: '#ffffff' }}
                navigation={props.navigation}
                token={token}
                stage={stage}
                specReducerdata={specificationReducer.success.data.list_of_event_names}
            />;
        } else if (specificationReducer.failure.error) {
            return notifyMessage('Something went wrong');
        } else {
            return <EmptyLoader />
        }
    }

    return renderProjectSpecification();
};

export default ProjectSpecificationStageList;
