import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { useDispatch, useSelector } from 'react-redux';
import { AuthContext } from '../../context';
import { SearchBar } from 'react-native-elements';
import { FlatList } from 'react-native-gesture-handler';
import EmptyScreenHeader from '../components/EmptyScreens/EmptyScreenHeader';
import EmptyLoader from '../components/EmptyLoader'
import { ButtonGroup } from 'react-native-elements';
import { Avatar, Card, Divider, Button, IconButton } from 'react-native-paper';
import { Icon } from 'react-native-elements'
import * as actionCreators from "../actions/project-finance-action/index";
import BudgetedScreen from '../components/ProjectFinance/BudgetedScreen'
import PandLScreen from '../components/ProjectFinance/PandLScreen'


const ProjectFinance = (props) => {

    const {token, project_id} = props.route.params
    const buttons = ['Budgeted', 'Actual', 'P&L']
    const [selectedIndex, setselectedIndex] = React.useState(0);
    const [selectedBtnData, setselectedBtnData] = React.useState([]);

    const [reload, setReload] = useState(false);

    const changeReload = () => {
        setReload(!reload)
    }
    
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(actionCreators.getBudgetedFinanceDetails(token, project_id))
    }, [reload]);

    const FinanceReducer = useSelector(state => {
        if (selectedIndex == 0) {
            return state.projectFinanceReducer.budgetedFinance.get;
        } else if (selectedIndex == 1) {
            return state.projectFinanceReducer.actualFinance.get;
        } else {
            return state.projectFinanceReducer.pnlDetails.get;
        }
    })

    const onPressBtn = (index) => {
        setselectedIndex(index)
        switch (index) {
            case 0:
                dispatch(actionCreators.getBudgetedFinanceDetails(token, props.route.params.project_id))
                break;
            case 1:
                dispatch(actionCreators.getActualFinanceDetails(token, props.route.params.project_id))
                break;
            case 2:
                // dispatch(actionCreators.getBudgetedFinanceDetails(token, 9))
                break;
            default:
                break
        }
    }

    return (
        <View style={styles.container}>
            <EmptyScreenHeader {...props} title='Finance' />
            <View style={styles.body}>
                <View>
                    <ButtonGroup
                        onPress={(index) => { onPressBtn(index) }}
                        buttons={buttons}
                        containerStyle={{ height: hp('5.6%'), left: 0, }}
                        selectedIndex={selectedIndex}
                        buttonContainerStyle={{ backgroundColor: 'white' }}
                        textStyle={{
                            fontSize: hp('1.8%'),
                            fontFamily: 'SourceSansPro-Regular',
                            color: 'rgb(143, 143, 143)'
                        }}
                        selectedButtonStyle={{ backgroundColor: '#3d87f1', }}
                    />
                </View>

                {
                    (FinanceReducer.success.ok && selectedIndex == 0) &&
                    <BudgetedScreen
                        navigation={props.navigation}
                        type={'BUDGETED'}
                        token={token}
                        project_id={project_id}
                        changeReload={changeReload}
                        total_budget_service_side_cost={FinanceReducer.success.data.actual_total_budget_service_side}
                        total_budget_supply_side_cost={FinanceReducer.success.data.actual_total_budget_supply_side}
                        SupplySideList={FinanceReducer.success.data.list_of_budgeted_supply_side}
                        ServiceSideList={FinanceReducer.success.data.list_of_budgeted_service_side}
                    />
                }

                {
                    (FinanceReducer.success.ok && selectedIndex == 1) &&
                    <BudgetedScreen
                        navigation={props.navigation}
                        type={'ACTUAL'}
                        token={token}
                        project_id={project_id}
                        changeReload={changeReload}
                        total_budget_service_side_cost={FinanceReducer.success.data.actual_total_budget_service_side}
                        total_budget_supply_side_cost={FinanceReducer.success.data.actual_total_budget_supply_side}
                        SupplySideList={FinanceReducer.success.data.list_of_actual_supply_side}
                        ServiceSideList={FinanceReducer.success.data.list_of_actual_service_side}
                    />
                }

                {
                    (selectedIndex == 2) &&
                    <PandLScreen navigation={props.navigation} token={token} />
                }

                {(!FinanceReducer.success.ok && selectedIndex != 2) && <EmptyLoader />}

            </View>
        </View>
    )
}

export default ProjectFinance;

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: "white",
        flex: 1,
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    body: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        flex: 11,
        backgroundColor: 'white',
        paddingTop: hp('2%')
    },

});
