import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    Button,
    Keyboard
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import Icons from "react-native-vector-icons/Ionicons";
import { white } from 'react-native-paper/lib/typescript/styles/colors';
import FastImageComponent from '../FastImageComponent'
import { Avatar, Card, Divider, IconButton } from 'react-native-paper';
import color from 'color';


const Dot = (params) => {
    return (
        <Text style={[styles.titleStyle, { color: 'rgb(143, 143, 143)', },]}>{'\u2022'}</Text>
    );
}


function ActivitiesCard(props) {
    const { event_name, date } = props.data
    var dateArray = date.split('/')
    var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"];
    const day = dateArray[0]
    const month = monthNames[parseInt(dateArray[1][1])]
    const year = dateArray[2]

    return (

        <View style={{ padding: '3%' }}>
            <Text style={[styles.titleStyle, { paddingVertical: hp('1%'), color: 'rgb(143, 143, 143)' }]}> <Dot /> {'  '}{event_name}</Text>
            <Text style={[styles.titleStyle,]}>{props.type==0 ?  `      Completed On ` + day + 'th' + month + ','+ year : `      To be Completed On ` + day + 'th' + month + ','+ year}</Text>
            <Divider style={{marginTop: '3%'}} />
        </View>

        // <View
        //     style={{ paddingVertical: '3%', }}
        // >
        //     <Card
        //         elevation={0}
        //         style={{ backgroundColor: '#fff', paddin }}
        //     >
        //         {/* <View style={{ display: 'flex', flexDirection: 'row', borderWidth: 0, flexWrap: 'wrap' }}>
        //             <Text style={styles.titleStyle}> <Dot /> Anand Prakash</Text>
        //             <Text style={[styles.titleStyle,]}> <Dot /> {event_name}</Text>
        //         </View>
        //         <View style={{ marginTop: hp('1%'), display: 'flex', flexDirection: 'row' }}>
        //             <Text style={[styles.titleStyle, { color: 'rgb(143, 143, 143)' }]}> <Dot /> From Approvals</Text>
        //             <Text style={[styles.titleStyle, { color: 'rgb(143, 143, 143)' }]}> <Dot /> On {day}th {month}, {year}</Text>
        //         </View> */}
        //     </Card >
        //     <Divider />
        // </View>
    );
}


const styles = StyleSheet.create({

    titleStyle: {
        fontSize: hp('2.1%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68,67,67)',
    },

});


export default ActivitiesCard;