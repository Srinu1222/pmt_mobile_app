import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    Button,
    Keyboard,
    PermissionsAndroid,
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import Icons from "react-native-vector-icons/Ionicons";
import { white } from 'react-native-paper/lib/typescript/styles/colors';
import FastImageComponent from '../FastImageComponent'
import { Avatar, Card, Divider, IconButton } from 'react-native-paper';
import color from 'color';
import { TouchableHighlight } from 'react-native';
import RNFetchBlob from 'rn-fetch-blob';



function DailyReportCard(props) {
    const checkPermission = async (Image_URI) => {

        // Function to check the platform
        // If iOS then start downloading
        // If Android then ask for permission
    
        if (Platform.OS === 'ios') {
          downloadImage();
        } else {
          try {
            const granted = await PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
              {
                title: 'Storage Permission Required',
                message:
                  'App needs access to your storage to download Photos',
              }
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
              // Once user grant the permission start downloading
              //'Storage Permission Granted.');
              downloadImage(Image_URI);
            } else {
              // If permission denied then show alert
              Toast.show({
                type: 'error',
                text1: 'Storage Permission',
                text2: 'Storage Permission Not Granted'
              });
            }
          } catch (err) {
            // To handle permission related exception
            console.warn(err);
          }
        }
      };
    
      const downloadImage = (URI) => {
        // Main function to download the image
    
        // To add the time suffix in filename
        let date = new Date();
        // Image URL which we want to download
        let image_URL = URI;
        // Getting the extention of the file
        let ext = getExtention(image_URL);
        ext = '.' + ext[0];
        // Get config and fs from RNFetchBlob
        // config: To pass the downloading related options
        // fs: Directory path where we want our image to download
        const { config, fs } = RNFetchBlob;
        let PictureDir = fs.dirs.PictureDir;
        let options = {
          fileCache: true,
          addAndroidDownloads: {
            // Related to the Android only
            useDownloadManager: true,
            notification: true,
            path:
              PictureDir +
              '/daily_report' +
              Math.floor(date.getTime() + date.getSeconds() / 2) +
              ext,
            description: 'Daily Report',
          },
        };
        config(options)
          .fetch('GET', image_URL)
          .then(res => {
            // Showing alert after successful downloading
            //'res -> ', JSON.stringify(res));
            Toast.show({
              type: 'success',
              text1: 'Daily Report',
              text2: 'Daily Report Downloaded Successfully.'
            });
          });
      };
      const getExtention = filename => {
        // To get the file extension
        return /[.]/.exec(filename) ?
          /[^.]+$/.exec(filename) : undefined;
      };
    const { date, filled_by, not_filled_by, report } = props.data
    return (
        <Card
            elevation={3}
            style={{ borderWidth: 0, backgroundColor: '#fff', margin: '5%', paddingRight: '3%' }}
            onPress={() => { }}
        >
            <Card.Title
                titleStyle={styles.titleStyle}
                title='14/05/2021'
            />
            <Divider style={{ marginLeft: wp('3%') }} />
            <View style={{ marginLeft: wp('5%'), display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                <View>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        < FastImage
                            style={{ alignSelf: 'center', width: wp('4%'), height: hp('4%') }}
                            source={require('../../assets/icons/info.png')}
                            resizeMode={FastImage.resizeMode.contain}
                        />
                        <Text style={styles.textStyle}>{'  '}{filled_by ? filled_by.length : 0} Filled</Text>
                    </View>
                    <Text style={styles.name_text}>Name</Text>
                </View>
                <View>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        < FastImage
                            style={{ alignSelf: 'center', width: wp('4%'), height: hp('4%') }}
                            source={require('../../assets/icons/info.png')}
                            resizeMode={FastImage.resizeMode.contain}
                        />
                        <Text style={styles.textStyle}>{'  '}{not_filled_by ? not_filled_by.length : 0} Not Filled</Text>
                    </View>
                    <Text style={styles.name_text}>Name</Text>

                </View>
                <View>
                    <TouchableOpacity 
                              onPress={() => { checkPermission(report) }}
>
                    < FastImage
                        style={{ alignSelf: 'center', width: wp('10%'), height: hp('10%') }}
                        source={require('../../assets/images/android/all_projects_page_icons/view.png')}
                        resizeMode={FastImage.resizeMode.contain}
                    /></TouchableOpacity>
                </View>
            </View>
        </Card>
    );
}

const styles = StyleSheet.create({
    titleStyle: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Semibold',
        color: 'rgb(68,67,67)'
    },

    name_text: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
    },

    textStyle: {
        fontSize: hp('1.8%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(143, 143, 143)'
    }
});


export default DailyReportCard;