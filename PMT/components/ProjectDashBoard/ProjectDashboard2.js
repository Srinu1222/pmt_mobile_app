import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { useDispatch, useSelector } from 'react-redux';
import * as actionCreator from '../../actions/team-action/index';
import { AuthContext } from '../../context';
import { SearchBar } from 'react-native-elements';
import { FlatList } from 'react-native-gesture-handler';
import Pagination from '../Pagination'
import EmptyScreenHeader from '../EmptyScreens/EmptyScreenHeader'
import { Avatar, Divider, Card, Button, Colors, ProgressBar, Title, Paragraph } from 'react-native-paper';
import { ButtonGroup } from 'react-native-elements';
import ActivitiesCard from './ActivitiesCard'
import DailyReportCard from './DailyReportCard'
import EmptyProjectDashboard from '../EmptyScreens/ProjectDashboard'
import config from "../../config";


const ProjectDashboard2 = (props) => {

    const { navigation, token, data, project_id } = props

    const { daily_reports, recent_active_events, recent_closed_events, project_name, project_stage, total_number_of_events, uncompleted_events } = data
    
    //('reports', daily_reports)
    let progress = (total_number_of_events-uncompleted_events) / total_number_of_events
    progress = Object.is(progress, NaN) ? 1 : progress

    const buttons = ['Completed', 'On-Going', 'Daily Reports']
    const [selectedIndex, setselectedIndex] = React.useState(0);
    const [selectedBtnData, setselectedBtnData] = React.useState(recent_closed_events);

    const onPressBtn = (index) => {
        setselectedIndex(index)
        switch (index) {
            case 0:
                setselectedBtnData(recent_closed_events)
                break;
            case 1:
                setselectedBtnData(recent_active_events)
                break;
            case 2:
                setselectedBtnData(daily_reports)
                break;
            default:
                break
        }
    }

    return (
        <>
            <View style={styles.container}>
                <View style={{ flex: .09 }}>
                    <EmptyScreenHeader title={'Dashboard'} navigation={navigation} />
                </View>
                <View style={{ flex: 1 }}>
                    <Card
                        elevation={5}
                        style={{ borderWidth: 0, backgroundColor: '#fff', margin: '5%', paddingRight: '3%' }}
                        onPress={() => { }}
                    >
                        <Card.Title
                            titleStyle={styles.titleStyle1}

                            title={project_name}

                            right={
                                (props) =>
                                    <Button
                                        style={{ backgroundColor: 'rgb(236, 243, 254)' }}
                                        onPress={() => null}
                                        labelStyle={styles.labelStyle}
                                    >
                                        ON TIME
                                    </Button>
                            }
                        />
                        
                        <Divider style={{ marginLeft: wp('3%'), backgroundColor: 'rgba(179, 179, 179, 0.5)' }} />

                        <View style={{ display: 'flex', padding: '4%', flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View>
                                <Text style={styles.titleStyle}>Stage</Text>
                                <Text style={[styles.option_text, { marginTop: hp('0.5%') }]}>{project_stage.charAt(0).toUpperCase() + project_stage.slice(1).toLowerCase()}</Text>
                            </View>
                            <View>
                                <Text style={styles.titleStyle}>Progress</Text>
                                <View style={{ flexDirection: 'row' }}>
                                    <ProgressBar style={{ marginTop: hp('1.6%'), width: wp('35%'), borderRadius: 4, backgroundColor: 'rgb( 236 ,243 ,254)' }} progress={progress} color='#3D87F1' />
                                    <Text style={styles.labelStyle}>{'  '}{total_number_of_events-uncompleted_events}/{total_number_of_events}</Text>
                                </View>
                            </View>
                        </View>
                    </Card>
                    <View>
                        <ButtonGroup
                            onPress={(index) => { onPressBtn(index) }}
                            buttons={buttons}
                            containerStyle={{ height: 40 }}
                            selectedIndex={selectedIndex}
                            buttonContainerStyle={{ backgroundColor: 'white' }}
                            selectedButtonStyle={{ backgroundColor: '#3d87f1', }}
                            textStyle={{
                                fontSize: hp('1.67%'),
                                fontFamily: 'SourceSansPro-Regular',
                                color: 'rgb(143, 143, 143)'
                            }}
                        />
                    </View>

                    <View style={{ height: hp('100%') }}>
                        {
                            selectedBtnData.length != 0 ?
                                <FlatList
                                    data={selectedBtnData}
                                    keyExtractor={(item, index) => index}
                                    renderItem={
                                        ({ item, index }) => (
                                            selectedIndex == 2 ? <DailyReportCard data={item} /> : <ActivitiesCard data={item} type={selectedIndex}/>
                                        )
                                    }
                                />
                                :
                                <EmptyProjectDashboard index={selectedIndex} />

                        }

                    </View>
                </View>
            </View>
        </ >
    );
};

const styles = StyleSheet.create({

    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        display: 'flex',
        backgroundColor: "white",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    titleStyle: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(194,194,194)'
    },

    titleStyle1: {
        fontSize: hp('2.2%'),
        fontFamily: 'SourceSansPro-Semibold',
        color: 'rgb(68, 67, 67)'
    },

    labelStyle: {
        color: '#3D87F1',
        fontSize: hp('1.8%'),
        fontFamily: 'SourceSansPro-Regular',
    },

    option_text: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
    },

});

export default ProjectDashboard2;
