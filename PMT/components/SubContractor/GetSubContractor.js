import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    Button
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { useDispatch, useSelector } from 'react-redux';
import * as actionCreator from '../../actions/team-action/index';
import { AuthContext } from '../../context';
import { FlatList } from 'react-native-gesture-handler';
import EmptyScreenHeader from '../EmptyScreens/EmptyScreenHeader';
import Pagination from '../Pagination'
import SubContractorItem from './SubContractorItem'
import SubContractorEmptyScreen from '../EmptyScreens/EmptyCaseSubContractor'
import { FAB, Icon, BottomSheet, SearchBar } from 'react-native-elements';
import { Divider } from 'react-native-paper';



const GetSubContractor = (props) => {

    const {subContractor:data, navigation, changeReload} = props

    const [activeButton, setactiveButton] = React.useState(1);

    const [searchQuery, setSearchQuery] = React.useState('');

    const [isVisible, setIsVisible] = useState(false);

    const onChangeSearch = query => setSearchQuery(query);

    const filterItems = (items, filter) => {
        return items.filter(item => item.email.toLowerCase().includes(filter.toLowerCase()))
    }

    const onPressFilter = (params) => {
    }

    const onPressRemove = (params) => {
        setIsVisible(false)
        navigation.navigate(
          'team_remove',
          {
            'data': data,
            'sub_contractor_member': true,

          }
        )
      }

    // Bottom Components
    const bottomList = ['Remove']
    const BottomComponent = (params) => {
        return (
            <View style={{ height: hp('100%') }}>
                <TouchableOpacity onPress={() => setIsVisible(false)}>
                    <View style={{ height: hp('86%') }}></View>
                </TouchableOpacity>
                <View style={{ backgroundColor: 'white', padding: '4%', height: hp('16%'), borderTopStartRadius: 10, borderTopRightRadius: 10, }}>
                    {
                        bottomList.map(
                            (item, index) => {
                                return (
                                    <View key={index} >
                                        <TouchableOpacity
                                            onPress={() => item == 'Filter' ? onPressFilter() : onPressRemove()}
                                        >
                                            <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: hp('1%') }}>
                                                <Icon
                                                    iconStyle={{ fontSize: hp('3%') }}
                                                    name={item == 'Filter' ? "funnel-outline" : "trash-outline"}
                                                    type='ionicon'
                                                    color='rgb(68, 67, 67)'
                                                />
                                                <Text style={styles.filterText}>{item}</Text>
                                            </View>

                                        </TouchableOpacity>

                                        {index != bottomList.length - 1 && <Divider style={{ backgroundColor: 'rgb(232, 232, 232)' }} />}
                                    </View>
                                );
                            }
                        )
                    }
                </View>
            </View>
        );
    }

    if (data.length == 0) {
        return <SubContractorEmptyScreen {...props} />
    } else {
        return (
            <>
                <View style={styles.container}>
                    <EmptyScreenHeader {...props} title='Sub-Contractor' />
                    <View style={styles.parent2}>

                        <View style={{ display: 'flex', marginTop: 12, flexDirection: 'row', justifyContent: 'space-between' }}>
                            <SearchBar
                                showLoading={false}
                                platform={Platform.OS}
                                containerStyle={{ borderWidth: 1, borderRadius: 4, borderColor: 'rgb(232, 232, 232)', backgroundColor: 'white', justifyContent: 'center', width: wp('73.3%'), height: hp('6%') }}
                                clearIcon={true}
                                onChangeText={onChangeSearch}
                                onClearText={() => null}
                                placeholder=''
                                cancelButtonTitle='Cancel'
                            />
                            <TouchableOpacity
                                onPress={() => setIsVisible(true)}
                                style={{ alignItems: 'center', justifyContent: 'center' }}>
                                < FastImage
                                     style={{ width: wp('6%'), height: wp('6%') }}
                                    source={require('../../assets/icons/rectangle.png')}
                                    resizeMode={FastImage.resizeMode.contain}
                                />
                                <Text style={styles.option_text}>Options</Text>
                            </TouchableOpacity>
                        </View>
                        <Pagination activeButton={activeButton} setactiveButton={setactiveButton} reducerLength={data.length} />
                        <Text style={{ height: hp('0.2%'), marginTop: 8, marginBottom: 8, backgroundColor: 'rgb(232, 232, 232)' }}></Text>
                        <TouchableOpacity
                            onPress={() => {
                                props.navigation.navigate('addSubContractor')
                            }}
                            style={{ backgroundColor: 'black', width: wp('15.6%'), height: hp('7.4%'), zIndex: 4, borderRadius: 28, position: 'absolute', alignSelf: 'flex-end', right: 10, top: hp('75%') }}
                        >
                            < FastImage
                                style={{ alignSelf: 'center', width: wp('6%'), height: hp('7%') }}
                                source={require('../../assets/icons/subcontractorlogo.png')}
                                resizeMode={FastImage.resizeMode.contain}
                            />
                        </TouchableOpacity>
                        <FlatList
                            keyExtractor={(item, index) => item.member_id}
                            data={filterItems(data.slice(activeButton * 10 - 10, activeButton * 10), searchQuery)}
                            renderItem={({ item, index }) => (<SubContractorItem item={item} />)}
                        />
                        <BottomSheet
                            isVisible={isVisible}
                            containerStyle={{ backgroundColor: 'rgba(0.5, 0.25, 0, 0.2)' }}
                        >
                            {BottomComponent()}
                        </BottomSheet>
                    </View>
                </View>
            </ >
        );
    }
};

const styles = StyleSheet.create({

    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },


    parent2: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        flex: 11,
        backgroundColor: 'white',
        padding: '5%'

    },

    buy_now_text: {
        fontSize: hp('2.3%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(61, 135, 241)'
    },

    subscription_text: {
        fontSize: hp('1.8%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(194, 194, 194)'
    },

    option_text: {
        fontSize: hp('1.2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center'

    },

    filterText: {
        fontSize: hp('2.8%'),
        fontFamily: 'SourceSansPro-Semibold',
        color: 'rgb(68, 67, 67)',
        paddingLeft: wp('5%')
      },
    
      subscription_text: {
        fontSize: hp('1.8%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(194, 194, 194)',
      },

});

export default GetSubContractor;
