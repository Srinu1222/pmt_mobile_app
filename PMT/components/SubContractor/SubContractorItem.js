import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    Button
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import Icons from "react-native-vector-icons/Ionicons";
import { white } from 'react-native-paper/lib/typescript/styles/colors';


function SubContractorItem(props) {

    const { company_name, member_name, email, phone_number } = props.item;

    return (
        <View style={styles.item_card}>

            <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>

                <View>
                    <Text style={styles.name_text}>{company_name}</Text>
                    <Text style={styles.department_text}>{member_name}</Text>
                </View>

            </View>
            <Text style={{ height: hp('0.2%'), marginVertical: '4%', backgroundColor: 'rgb(232, 232, 232)' }}></Text>
            <View style={{ display: 'flex', marginBottom: 20, width: wp('70%'), flexDirection: 'row', justifyContent: 'space-between' }}>
                <View>
                    <Text style={styles.email_text}>{email}</Text>
                    <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>Email</Text>
                </View>
                <View>
                    <Text style={styles.email_text}>{phone_number}</Text>
                    <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>Phone Number</Text>
                </View>
            </View>
        </View>
    )
}


const styles = StyleSheet.create({
    name_text: {
        fontSize: hp('2.8%'),
        fontFamily: 'SourceSansPro-SemiBold',
        color: 'rgb(68, 67, 67)',
    },

    email_text: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
    },

    department_text: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-SemiBold',
        color: 'rgb(194, 194, 194)',
    },

    item_card: {
        borderLeftColor: '#3d87f1',
        backgroundColor: '#fff',
        borderRadius: 4,
        borderLeftWidth: 3,
        marginTop: hp('2%'),
        padding: '5%',
        paddingRight: '4%',
      },

});


export default SubContractorItem;