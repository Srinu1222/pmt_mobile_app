import { useDispatch } from "react-redux";
import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TouchableOpacity,
    TextInput,
    KeyboardAvoidingView,
    ScrollView,
    Animated, Keyboard
} from 'react-native';
import FastImage from 'react-native-fast-image';
import LottieView from 'lottie-react-native';
import React, { useState, useMemo, useEffect } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import notifyMessage from '../../ToastMessages';
import DropDownPicker from 'react-native-dropdown-picker';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import * as actionCreators from '../../actions/sub-contractor-action/index';
import { Icon } from 'react-native-elements';
import { Divider } from "react-native-elements/dist/divider/Divider";
import * as Animatable from 'react-native-animatable';



const AddSubContractor = (props) => {

    const { route, navigation } = props
    const { token } = route.params

    const [companyName, setCompanyName] = useState('')
    const [contactPersonName, setContactPersonName] = useState('')
    const [email, setEmail] = useState('')
    const [phoneNumber, setPhoneNumber] = useState('')


    // VALIDATIONS
    const [isValidemail, setisValidEmail] = useState(true);
    const [isValidphoneNo, setisValidPhoneNo] = useState(true);
    const [isValidCompanyName, setisValidCompanyName] = useState(true);

    const handleEmail = val => {
        const isValid = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
            val,
        );
        if (isValid) {
            setEmail(val)
            setisValidEmail(true)
        } else {
            setisValidEmail(false)
        }
    }

    const handlePhoneNo = val => {
        const isValid1 = /^[98765]\d{9}$/.test(
            val,
        );
        if (isValid1) {
            setPhoneNumber(val)
            setisValidPhoneNo(true)

        } else {
            setisValidPhoneNo(false)
        }
    }

    const handleCompanyName = val => {
        if (val.trim().length >= 4) {
            setCompanyName(val)
            setisValidCompanyName(true)
        } else {
            setisValidCompanyName(false)
        }
    }


    const dispatch = useDispatch();

    const addSubContractorHandler = () => {
        const formData = new FormData()
        formData.append('company_name', companyName)
        formData.append('number', phoneNumber)
        formData.append('email', email)
        formData.append('contact_person_name', contactPersonName)
        dispatch(actionCreators.addSubContractors(formData, token))
        props.navigation.navigate('subContractor')
    }

    return (
        <SafeAreaView>
            <KeyboardAvoidingView>
                <ScrollView>
                    <View style={styles.container}>
                        <View style={styles.containerHeader}>
                            <Text style={styles.titleText}>Add Sub-Contractor</Text>
                            <Icon color={'rgb(68, 67, 67)'}
                                onPress={() =>
                                    // props.navigation.reset({
                                    // routes: [{'name': 'subContractor'}]})
                                    props.navigation.navigate('subContractor')
                                }
                                type='feather' name={'x'} />
                        </View>

                        <Divider style={{ backgroundColor: 'rgba(0,0,0,0.3)', width: wp('90%'), height: hp('0.1%') }} />

                        <View style={styles.containerBody}>
                            <TextInput
                                placeholder="Company Name"
                                placeholderTextColor='rgb(179, 179, 179)'
                                style={styles.textInput}
                                autoCapitalize="none"
                                onChangeText={val => handleCompanyName(val)}
                            />

                            {isValidCompanyName ? null : (
                                <Animatable.View animation="fadeInLeft" duration={500}>
                                    <Text style={styles.errorMsg}>Enter a Valid Company Name.</Text>
                                </Animatable.View>
                            )}

                            <TextInput
                                placeholder="Contact's Person Name"
                                placeholderTextColor='rgb(179, 179, 179)'
                                style={styles.textInput}
                                autoCapitalize="none"
                                onChangeText={val => setContactPersonName(val)}
                            />

                            <TextInput
                                placeholder="Email"
                                placeholderTextColor='rgb(179, 179, 179)'
                                style={styles.textInput}
                                autoCapitalize="none"
                                onChangeText={val => handleEmail(val)}
                            />

                            {isValidemail ? null : (
                                <Animatable.View animation="fadeInLeft" duration={500}>
                                    <Text style={styles.errorMsg}>Enter a Valid Email.</Text>
                                </Animatable.View>
                            )}

                            <TextInput
                                placeholder="Phone Number"
                                placeholderTextColor='rgb(179, 179, 179)'
                                style={styles.textInput}
                                autoCapitalize="none"
                                onChangeText={val => handlePhoneNo(val)}
                            />

                            {isValidphoneNo ? null : (
                                <Animatable.View animation="fadeInLeft" duration={500}>
                                    <Text style={styles.errorMsg}>Enter a Valid PhoneNo.</Text>
                                </Animatable.View>
                            )}

                            <TouchableOpacity
                                onPress={() => { addSubContractorHandler() }}
                                style={styles.addbtn}>
                                <Text style={styles.btnText}>+Add</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        </SafeAreaView >
    );
}

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        display: 'flex',
        alignItems: 'center',
        backgroundColor: "white",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
    containerHeader: {
        height: hp('10%'),
        flexDirection: 'row',
        // borderBottomWidth: 3,
        // borderBottomColor: 'rgb(232,232,232)',
        width: wp('100%'),
        padding: '5%',
        justifyContent: 'space-between',
        alignItems: 'center'
    },

    titleText: {
        fontSize: hp('3.15%'),
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center',
        fontFamily: 'SourceSansPro-SemiBold',
    },

    image_logo: {
        width: wp('4%'),
        height: hp('2%'),
    },

    images: {
        alignSelf: 'center'
    },

    containerBody: {
        flex: 11,
        display: 'flex',
        justifyContent: 'center'
    },

    errorMsg: {

        color: '#FF0000',
        fontSize: 14,
        marginBottom: hp('1%'),
        fontFamily: 'SourceSansPro-Regular',
    },

    textInput: {
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        width: wp('79.4%'),
        height: hp('7.4%'),
        borderRadius: 4,
        fontSize: hp('2.3%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'black',
        textAlign: 'left',
        paddingHorizontal: 10,
        marginBottom: Platform.OS === 'ios' ? 6 : 8
    },

    addbtn: {
        width: wp('79.4%'),
        height: hp('7.4%'),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#3d87f1',
        borderRadius: 5,
        marginTop: 20,
    },

    btnText: {
        color: 'white',
        fontSize: hp('3%'),
        textAlign: 'center',
        fontFamily: 'SourceSansPro-Regular',
    }

});


export default AddSubContractor;
