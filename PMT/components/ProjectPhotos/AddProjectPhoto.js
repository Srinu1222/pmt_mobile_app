import { useDispatch } from "react-redux";
import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TouchableOpacity,
    TextInput,
    KeyboardAvoidingView,
    ScrollView,
    Animated, Keyboard
} from 'react-native';
import FastImage from 'react-native-fast-image';
import LottieView from 'lottie-react-native';
import React, { useState, useMemo, useEffect } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import notifyMessage from '../../ToastMessages';
import DropDownPicker from 'react-native-dropdown-picker';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Divider, Card, Avatar } from 'react-native-paper';
import { FAB, Icon, BottomSheet, Badge, Input, ButtonGroup } from 'react-native-elements';
import { FlatList } from "react-native";
import MyModalDropDown from './ModalDropDown';
// import config from '../../config';
import axios from 'axios';
import DocumentPicker from 'react-native-document-picker';
import * as actionCreators from '../../actions/gantt-stage-action/index';
import Toast from "react-native-toast-message";


const AddProjectPhoto = ({ route, navigation }) => {

    const { projectName, token, projectId, stage, eventName, is_sub_contractor } = route.params

    const dispatch = useDispatch();

    const Header = () => {
        const handleCloseButton = () => {
            navigation.navigate('projectPhotos', { projectId: projectId, is_sub_contractor: is_sub_contractor, projectName: projectName })
        }

        return (
            <View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingVertical: '3%', marginHorizontal: wp('5%') }}>
                    <Text style={styles.addPhoto}>Add Photo</Text>
                    <Icon color={'rgb(68, 67, 67)'} onPress={() => handleCloseButton()} type='feather' name='x' />
                </View>

                <View style={{ paddingLeft: '5%' }}>
                    <Text style={[styles.projectName, { color: 'rgb(143, 143, 143)' }]}>You are adding images to
                    {
                            !stage ? <Text style={styles.projectName}>"{projectName}"</Text>
                                :
                                <Text style={styles.projectName}>{'  '}{projectName}{'>' + stage}</Text>
                        }
                    </Text>
                </View>
                <Divider style={{ backgroundColor: 'rgba(0, 0, 0, 0.1)', margin: '5%', height: hp('0.2%') }} />
            </View>
        );
    }

    const Stages = [
        'PRE-REQUISITES', 'APPROVALS', 'ENGINEERING',
        'PROCUREMENT', 'MATERIAL HANDLING', 'CONSTRUCTION', 'SITE HAND OVER'
    ]

    const [selectedEventId, setselectedEventId] = useState('');
    const [selectedEventName, setselectedEventName] = useState('');
    const [selectedStage, setselectedStage] = useState(stage?stage:'');
    const [stageEventsList, setstageEventsList] = useState([]);
    const [stageEventsNamesList, setstageEventsNamesList] = useState([]);
    const [picactiveIndex, setpicactiveIndex] = useState(0);
    const [uploadPicturesList, setuploadPicturesList] = useState([]);
    const [uploadDataList, setuploadDataList] = useState([]);
    const [fileName, setfileName] = useState('');
    const [description, setdescription] = useState('');

    useEffect(() => {
        axios.get(`https://pmt.safearth-api.in/api/get_stage_events_basic_details/`, {
            headers: {
                Authorization: 'Token ' + token,
            },
            params: {
                project_id: projectId,
                stage: selectedStage
            }
        })
            .then(result => {
                let stageEventObjectsList = result.data.stage_events_list
                setstageEventsList(stageEventObjectsList)
                let eventNamesList = stageEventObjectsList.map((i, index) => i.name)
                setstageEventsNamesList(eventNamesList)
            })
            .catch(err => console.log(err.response));
    }, [selectedStage])

    const SetSelectedStage = (index, value) => {
        setselectedStage(value)
    }

    const SetSelectedEvent = (index, value) => {
        let eventId = stageEventsList[index].id
        setselectedEventName(value)
        setselectedEventId(eventId)
    }

    const onPressUploadDocument = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.allFiles],
            });
            let uploadFile = {
                uri: res.uri,
                type: res.type,
                name: res.name
            }
            setuploadPicturesList([...uploadPicturesList, uploadFile])
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
            } else {
                throw err;
            }
        }
    }

    const handleRemoveFile = () => {
        let tempList = [...uploadPicturesList]
        tempList.splice(picactiveIndex, 1)
        setuploadPicturesList([...tempList])
        let tempDataList = [...uploadDataList]
        tempDataList.splice(picactiveIndex, 1)
        setuploadDataList([...tempDataList])
    }

    const handleImageData = (text, type, imageIndex) => {
        let tempList = [...uploadDataList]
        tempList[imageIndex] = { ...tempList[imageIndex], [type]: text }
        setuploadDataList([...tempList])
    }

    const handleAddButton = () => {
        const formData = new FormData();
        formData.append('event_id', selectedEventId);
        formData.append('is_image', true);
        formData.append("files_data", JSON.stringify(uploadDataList));
        uploadPicturesList.map((i) => formData.append('files', i))

        Promise.resolve(dispatch(actionCreators.postDocs(formData, token, is_sub_contractor)))
            .then(result => {
                if (result?.status === 200) {
                    Toast.show({
                        text1: 'Event Images got added',
                        type: 'success',
                        autoHide: true,
                        position: 'bottom',
                        visibilityTime: 1000
                    })
                    navigation.navigate('projectPhotos', { projectId: projectId, is_sub_contractor: is_sub_contractor, projectName: projectName })
                }
                // setuploadDataList([])
                // setuploadPicturesList([])
            })
    };

    const UploadButton = () => {
        return (
            <TouchableOpacity
                onPress={() => onPressUploadDocument()}
            >
                <View style={{ flexDirection: 'row', height: hp('7%'), alignItems: 'center', paddingHorizontal: '4%', borderRadius: 4, backgroundColor: 'rgb(236, 243, 254)', width: wp('80%'), justifyContent: 'space-between' }}>
                    <Text style={styles.titleText}>Upload Photos</Text>
                    <Icon
                        color={'#3d87f1'}
                        type='feather'
                        name={'file-plus'}
                    />
                </View>
            </TouchableOpacity>
        );
    }

    return (
        <View style={styles.container}>
            <Header />
            <View style={{ height: hp('85%') }}>
                <FlatList
                    data={['Hello']}
                    keyExtractor={(item, index) => index}
                    // contentContainerStyle={{width: wp('100%'), borderWidth: 1}}
                    renderItem={({ item, index }) => {
                        return (
                            <>


                                <View style={{ alignSelf: 'center' }}>
                                    <Text style={{ color: 'rgb(68, 67, 67)', width: wp('80%'), marginBottom: hp('2%'), fontSize: hp('2%'), fontFamily: 'SourceSansPro-Regular' }}>
                                        Please link the <Text style={{ fontWeight: 'bold' }}>Stage {' &'}</Text>  <Text style={{ fontWeight: 'bold' }}>Event</Text> of the photos you are uploading
                                        </Text>
                                    {
                                        !stage &&
                                        <MyModalDropDown
                                            title={'Stage'}
                                            data={Stages}
                                            setFunction={SetSelectedStage}
                                        />
                                    }
                                    <MyModalDropDown
                                        title={'Event'}
                                        data={stageEventsNamesList}
                                        setFunction={SetSelectedEvent}
                                    />
                                </View>


                                <View style={{ alignSelf: 'center' }}>
                                    <UploadButton />
                                    <Text style={{ color: 'rgb(68, 67, 67)', width: wp('80%'), marginTop: hp('5%'), fontSize: hp('2%'), fontFamily: 'SourceSansPro-Regular' }}>
                                        Please add <Text style={{ fontWeight: 'bold' }}>Name {' &'}</Text>  <Text style={{ fontWeight: 'bold' }}>Description</Text> to the photos.
                                    </Text>
                                </View>

                                <View style={{ marginHorizontal: wp('5%'), alignItems: 'center', marginTop: hp('2%') }}>
                                    <FlatList
                                        data={uploadPicturesList}
                                        ListEmptyComponent={
                                            <View style={{ height: hp('10%'), width: wp('80%'), alignItems: 'center', borderRadius: 4, justifyContent: 'center', borderWidth: 1, borderStyle: 'dashed', borderColor: 'rgb(179, 179, 179)' }}>
                                                <Text style={styles.noImages}>No photos added yet</Text>
                                            </View>
                                        }
                                        keyExtractor={(item, index) => index}
                                        horizontal={true}
                                        renderItem={({ item, index }) => {
                                            // console.log(index, picactiveIndex)
                                            return (
                                                <TouchableOpacity
                                                    onPress={() => setpicactiveIndex(index)}
                                                >
                                                    <View style={{ borderWidth: 0, paddingTop: 10, paddingRight: 10 }}>
                                                        < FastImage
                                                            style={{ width: wp('21%'), height: hp('10%'), borderRadius: 3 }}
                                                            source={{ uri: item.uri }}
                                                            resizeMode={FastImage.resizeMode.cover}
                                                        />
                                                        {
                                                            index == picactiveIndex &&
                                                            <Badge
                                                                status="error"
                                                                value={
                                                                    <Icon
                                                                        color={'white'}
                                                                        type='feather'
                                                                        name={'x'}
                                                                        size={10}
                                                                    />
                                                                }
                                                                onPress={() => handleRemoveFile()}
                                                                containerStyle={{ position: 'absolute', top: 0, right: 4 }}
                                                            />
                                                        }

                                                    </View>
                                                </TouchableOpacity>
                                            );
                                        }}
                                    />
                                </View>

                                <View style={{ alignSelf: 'center' }}>
                                    <Input
                                        placeholder={'Enter Image Name'}
                                        onChangeText={(text) => handleImageData(text, 'title', picactiveIndex)}
                                        inputStyle={{ fontSize: hp('2.2%'), width: wp('80%'), color: 'rgb(68, 67, 67)' }}
                                        value={uploadDataList[picactiveIndex]?.title}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        containerStyle={{ width: wp('80%'), marginVertical: hp('2%'), backgroundColor: 'rgba(179,179, 179, 0.1)', height: hp('7%'), borderRadius: 4 }}
                                    />

                                    <Input
                                        placeholder={'Enter Image Description Here...'}
                                        onChangeText={(text) => handleImageData(text, 'description', picactiveIndex)}
                                        inputStyle={{ fontSize: hp('2.2%'), width: wp('80%'), color: 'rgb(68, 67, 67)' }}
                                        value={uploadDataList[picactiveIndex]?.description}
                                        multiline={true}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        containerStyle={{ width: wp('80%'), marginBottom: hp('1%'), backgroundColor: 'rgba(179,179, 179, 0.1)', borderRadius: 4 }}
                                    />

                                    <TouchableOpacity
                                        style={styles.buttonContainer}
                                        onPress={() => handleAddButton()}
                                    >
                                        <Text style={{
                                            color: 'white',
                                            fontSize: hp('3%'),
                                            textAlign: 'center',
                                            fontFamily: 'SourceSansPro-Regular',
                                        }}>+Add</Text>
                                    </TouchableOpacity>
                                </View>

                            </>
                        )
                    }}
                />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: "white",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    addPhoto: {
        fontSize: hp('3%'),
        color: 'rgb(68, 67, 67)',
        fontFamily: 'SourceSansPro-SemiBold',
    },

    projectName: {
        fontSize: hp('2.15%'),
        color: '#3d87f1',
        textTransform: 'capitalize',
        fontFamily: 'SourceSansPro-Regular',
    },

    buttonContainer: {
        marginTop: hp('3%'),
        width: wp('80%'),
        height: hp('7%'),
        backgroundColor: 'rgb(61, 135, 241)',
        borderRadius: 5,
        alignSelf: 'center',
        justifyContent: 'center',
        // marginTop: hp('20%')
    },

    image_logo: {
        width: wp('4%'),
        height: hp('2%'),
    },

    images: {
        alignSelf: 'center'
    },

    titleText: {
        fontSize: hp('2.2%'),
        color: '#3d87f1',
        fontFamily: 'SourceSansPro-SemiBold',
    },

    noImages: {
        fontSize: hp('2.2%'),
        color: 'rgb(143, 143,143)',
        fontFamily: 'SourceSansPro-Regular',
    },

    containerBody: {
        flex: 11,
        display: 'flex',
        justifyContent: 'center'
    },

    textInput: {
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        width: wp('79.4%'),
        height: hp('7.4%'),
        borderRadius: 4,
        fontSize: hp('2.3%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'black',
        textAlign: 'left',
        paddingHorizontal: 10,
        marginBottom: Platform.OS === 'ios' ? 6 : 8
    },

    addbtn: {
        width: wp('79.4%'),
        height: hp('7.4%'),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#3d87f1',
        borderRadius: 5,
        marginTop: 20,
    },

    btnText: {
        color: 'white',
        fontSize: hp('3%'),
        textAlign: 'center',
        fontFamily: 'SourceSansPro-Regular',
    }

});


export default AddProjectPhoto;
