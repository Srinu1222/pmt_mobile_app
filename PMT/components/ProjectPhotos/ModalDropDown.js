import React, { useState } from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { FAB, Icon, BottomSheet, Input } from 'react-native-elements';


const MyModalDropDown = (props) => {
    const {title, data, setFunction} = props
    const [IconPosition, setIconPosition] = useState(false);

    return (
        <ModalDropdown
            options={data}
            textStyle={{ fontSize: hp('2%'), width: wp('65%'), color: 'rgb(68, 67, 67)' }}
            dropdownStyle={{ width: '80%', height: hp('30%'), marginLeft: -15, elevation: 8, marginTop: 0 }}
            dropdownTextStyle={{ fontSize: hp('2%'), color: 'rgb(143, 143, 143)' }}
            onSelect={(index, value) => setFunction(index, value)}
            defaultValue={title}
            dropdownTextHighlightStyle={{ backgroundColor: 'rgb(236, 243, 254)' }}
            showsVerticalScrollIndicator={true}
            style={{ width: wp('80%'), borderWidth: 0, height: hp('7%'), justifyContent: 'center', borderColor: 'rgb(232, 232, 232)', backgroundColor: 'rgba(179, 179, 179, .1)', borderRadius: 4, padding: 15, marginVertical: hp('1%') }}
            renderRightComponent={() =>
                <View style={{ marginLeft: wp('0%'), borderWidth: 0 }}>
                    <Icon color={'#9B9B9B'} type='feather' name={IconPosition ? 'chevron-up' : 'chevron-down'} />
                </View>
            }
            defaultTextStyle={{ color: 'rgb(143, 143, 143)' }}
            onDropdownWillShow={() => setIconPosition(true)}
            onDropdownWillHide={() => setIconPosition(false)}
        />
    );
};

export default MyModalDropDown;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding: 40,
    },
    dropdown: {
        backgroundColor: 'white',
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
        marginTop: 20,
    },
    dropdown2: {
        backgroundColor: 'white',
        borderColor: 'gray',
        borderWidth: 0.5,
        marginTop: 20,
        padding: 8,
    },
    icon: {
        marginRight: 5,
        width: 18,
        height: 18,
    },
    item: {
        paddingVertical: 17,
        paddingHorizontal: 4,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    textItem: {
        flex: 1,
        fontSize: 16,
    },
});