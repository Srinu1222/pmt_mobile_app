import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    Animated,
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image';
import { useDispatch, useSelector } from 'react-redux';
import * as actionCreator from '../../actions/team-action/index';
import { AuthContext } from '../../context';
import { SearchBar } from 'react-native-elements';
import { FlatList } from 'react-native-gesture-handler';
import EmptyScreenHeader from '../EmptyScreens/EmptyScreenHeader';
import Pagination from '../Pagination';
import {
    useCollapsibleSubHeader,
    CollapsibleSubHeaderAnimator,
} from 'react-navigation-collapsible';
import { FAB, Icon, BottomSheet, Input } from 'react-native-elements';
import EmptyLoader from '../EmptyLoader'
import { Button } from 'react-native-paper'
import ElevatedView from '../ElevatedView';
import EmptyPhotos from '../EmptyScreens/EmptyPhotos';
import TouchableScale from 'react-native-touchable-scale';
import { ScrollView } from 'react-native';
import * as actionCreators from '../../actions/project-docs-action/index';


const GetProjectPhotos = props => {

    const {
        onScroll /* Event handler */,
        containerPaddingTop /* number */,
        scrollIndicatorInsetTop /* number */,
        translateY,
    } = useCollapsibleSubHeader();

    const { token, changeReload, getProjectPhotosDict, projectId, projectName, is_sub_contractor, navigation } = props;

    const [getProjectPhotos, setProjectPhotos] = useState(getProjectPhotosDict);
    const [isStagePhotos, setisStagePhotos] = useState(false);
    const [selectedStage, setSelectedStage] = useState('');
    const [selectedStagePhotosList, setselectedStagePhotosList] = useState([]);

    const [isEventPhoto, setIsEventPhoto] = useState(false);
    const [selectedEventPhotoIndex, setselectedEventPhotoIndex] = useState(null);
    const [isClickEditPhoto, setisClickEditPhoto] = useState(false);

    const handleStagePhotos = (stage, stagePhotosList) => {
        setisStagePhotos(true)
        setSelectedStage(stage)
        setselectedStagePhotosList([...stagePhotosList])
    }

    const handleEventPhoto = (item, index) => {
        setIsEventPhoto(true)
        setselectedEventPhotoIndex(index)
    }

    const handleEditPhoto = () => {
        setisClickEditPhoto(true)
    }

    const handleSave = () => {
        let selectedDoc = getProjectPhotos[selectedStage][selectedEventPhotoIndex]
        const form = new FormData();
        form.append("title", selectedDoc.doc_name);
        form.append("description", selectedDoc.description);
        form.append("doc_id", selectedDoc.doc_id);
        actionCreators.postImageTitleAndDescription(form, token)
        setisClickEditPhoto(false)
    }

    const handleAddPhoto = (selectedStage) => {
        navigation.navigate('addProjectPhoto', { projectId: projectId, is_sub_contractor: is_sub_contractor, stage: selectedStage, projectName: projectName })
    }

    return (
        <View style={styles.container}>
            <EmptyScreenHeader navigation={navigation} title='Photos' {...props} />
            {
                !isEventPhoto &&
                <ElevatedView elevation={4} style={{ width: '100%', backgroundColor: '#fff' }}>
                    {
                        isStagePhotos ?
                            <>
                                <TouchableScale activeScale={0.99} friction={10} tension={0}
                                    onPress={() => {
                                        setisStagePhotos(false)
                                    }}
                                >
                                    <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: hp('1%'), paddingHorizontal: '5%', }}>
                                        <Icon color={'rgb(68, 67, 67)'} type='feather' name={'chevron-left'} />
                                        <View style={{ paddingLeft: '4%', }}>
                                            <Text style={[styles.projectName]}>{projectName}</Text>
                                            <Text style={[styles.textMessage]}>{selectedStage}</Text>
                                        </View>
                                    </View>
                                </TouchableScale>
                                <View style={{ justifyContent: 'space-between', paddingHorizontal: wp('7%'), paddingVertical: hp('2%'), flexDirection: 'row' }}>
                                    <Text style={[styles.option_text]}>Photos({selectedStagePhotosList.length})</Text>
                                    <TouchableOpacity onPress={() => handleAddPhoto(selectedStage)}>
                                        <Text style={styles.addphoto}>+Add Photos</Text>
                                    </TouchableOpacity>
                                </View>
                            </>
                            :
                            <View style={{ justifyContent: 'space-between', padding: '5%', flexDirection: 'row' }}>
                                <Text style={[styles.allAlbums]}>{projectName}</Text>
                                <TouchableOpacity onPress={() => handleAddPhoto()}>
                                    <Text style={styles.addphoto}>+Add Photos</Text>
                                </TouchableOpacity>
                            </View>
                    }
                </ElevatedView >
            }

            {
                !isEventPhoto ?
                    <SafeAreaView style={{ backgroundColor: '#fff' }}>
                        {
                            isStagePhotos ?
                                <View style={{ alignSelf: 'center', height: hp('73%'), marginLeft: wp('6%'), margin: wp('3%'), borderWidth: 0, justifyContent: 'center', width: wp('100%') }}>
                                    <FlatList
                                        data={selectedStagePhotosList}
                                        showsVerticalScrollIndicator
                                        keyExtractor={(item, index) => index}
                                        numColumns={4}
                                        renderItem={({ item, index }) => {
                                            return (
                                                <TouchableScale activeScale={0.99} friction={10} tension={0}
                                                    onPress={() => handleEventPhoto(item, index)}
                                                >
                                                    <View style={{ width: wp('21%'), margin: '3%', borderWidth: 0, marginRight: '0%', }}>
                                                        <FastImage
                                                            style={{ width: wp('21%'), height: hp('10%'), borderRadius: 3 }}
                                                            source={{ uri: item.file }}
                                                            resizeMode={FastImage.resizeMode.cover}
                                                        />
                                                    </View>
                                                </TouchableScale>

                                            );
                                        }
                                        }
                                    />
                                </View>
                                :
                                <Animated.FlatList
                                    ListEmptyComponent={<EmptyPhotos onAdd={handleAddPhoto}/>}
                                    keyExtractor={(item, index) => index}
                                    data={Object.keys(getProjectPhotos)}
                                    renderItem={({ item, index }) => {
                                        const stagesPhotosList = getProjectPhotos[item]
                                        return (
                                            <TouchableScale activeScale={0.99} friction={10} tension={0}
                                                onPress={() => handleStagePhotos(item, stagesPhotosList)}
                                            >
                                                <View style={{ borderWidth: 0, padding: '3%', marginBottom: hp('3%'), width: wp('47%'), }}>
                                                    <View style={{ flexDirection: 'row', height: hp('21%'), flexWrap: 'wrap' }}>
                                                        {
                                                            stagesPhotosList.slice(0, 4).map((i, iIndex) => {
                                                                const shadowStyle = iIndex == 3 ? { opacity: 0.4, backgroundColor: '#3d87f1', } : {}
                                                                return (
                                                                    <View key={iIndex}>
                                                                        <View style={{ ...shadowStyle }}>
                                                                            <FastImage
                                                                                style={{ width: wp('20%'), height: hp('10%'), borderRadius: 3, margin: '1%', marginRight: '0%', color: '#fff' }}
                                                                                source={{ uri: i.file }}
                                                                                resizeMode={FastImage.resizeMode.cover}
                                                                            />
                                                                        </View>
                                                                        {
                                                                            iIndex == 3 &&
                                                                            <View style={{ position: 'absolute', alignItems: 'center', marginLeft: wp('5%'), marginTop: hp('2%'), borderWidth: 0 }}>
                                                                                <Text style={styles.more}>{stagesPhotosList.length - 3}</Text>
                                                                                <Text style={styles.more}>More</Text>
                                                                            </View>
                                                                        }
                                                                    </View>
                                                                );
                                                            })
                                                        }
                                                    </View>
                                                    <Text style={[styles.allAlbums, { textTransform: 'capitalize', fontSize: hp('2%') }]}>{item}</Text>
                                                </View>
                                            </TouchableScale>
                                        );
                                    }}
                                    onScroll={onScroll}
                                    horizontal={false}
                                    numColumns={2}
                                    contentContainerStyle={{
                                        paddingTop: containerPaddingTop,
                                        paddingLeft: hp('2%'),
                                        paddingRight: hp('2%'),
                                        paddingBottom: hp('8%'),
                                        marginTop: hp('3%'),
                                    }}
                                    scrollIndicatorInsets={{ top: scrollIndicatorInsetTop }}
                                />
                        }
                    </SafeAreaView >
                    :
                    <SafeAreaView style={{ backgroundColor: '#fff', width: wp('100%') }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', padding: '5%', }}>
                            <Icon color={'rgb(68, 67, 67)'} onPress={() => setIsEventPhoto(false)} type='feather' name={'chevron-left'} />
                            {
                                isClickEditPhoto &&
                                <TouchableOpacity
                                    onPress={() => setisClickEditPhoto(false)}
                                >
                                    <Text style={styles.addphoto}>Cancel</Text>
                                </TouchableOpacity>
                            }
                        </View>
                        <ScrollView>
                            <KeyboardAvoidingView keyboardVerticalOffset={5} behavior='position'>
                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <FastImage
                                        style={{ width: wp('90%'), height: hp('30%'), borderRadius: 3 }}
                                        source={{ uri: getProjectPhotos[selectedStage][selectedEventPhotoIndex].file }}
                                        resizeMode={FastImage.resizeMode.cover}
                                    />

                                    {
                                        isClickEditPhoto ?
                                            <View>
                                                <Input
                                                    placeholder={'Image Name'}
                                                    onChangeText={text => {
                                                        let tempDict = { ...getProjectPhotos }
                                                        tempDict[selectedStage][selectedEventPhotoIndex] = { ...tempDict[selectedStage][selectedEventPhotoIndex], doc_name: text }
                                                        setProjectPhotos(tempDict)
                                                    }}
                                                    inputStyle={{ fontSize: hp('2%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                                    value={getProjectPhotos[selectedStage][selectedEventPhotoIndex].doc_name}
                                                    inputContainerStyle={{ borderBottomWidth: 0 }}
                                                    containerStyle={{ width: wp('80%'), margin: '2%', height: hp('6%'), backgroundColor: 'rgba(179, 179, 179, 0.1)', borderColor: 'rgb(143, 143, 143)', borderRadius: 4 }}
                                                />
                                                <Input
                                                    placeholder={'Description'}
                                                    onChangeText={text => {
                                                        let tempDict = { ...getProjectPhotos }
                                                        tempDict[selectedStage][selectedEventPhotoIndex] = { ...tempDict[selectedStage][selectedEventPhotoIndex], description: text }
                                                        setProjectPhotos(tempDict)
                                                    }}
                                                    inputStyle={{ fontSize: hp('2%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                                    value={getProjectPhotos[selectedStage][selectedEventPhotoIndex].description}
                                                    multiline={true}
                                                    inputContainerStyle={{ borderBottomWidth: 0 }}
                                                    containerStyle={{ width: wp('80%'), height: hp('30%'), margin: '2%', backgroundColor: 'rgba(179, 179, 179, 0.1)', borderColor: 'rgb(143, 143, 143)', borderRadius: 4 }}
                                                />
                                            </View>
                                            :
                                            <>
                                                <Text style={[styles.allAlbums, { alignSelf: 'flex-start', padding: '5%', }]}>{
                                                    getProjectPhotos[selectedStage][selectedEventPhotoIndex].doc_name
                                                }</Text>
                                                <ScrollView style={{ height: hp('30%'), marginBottom: hp('2%'), width: wp('90%') }}>
                                                    <Text style={[styles.textMessage]}>{
                                                        getProjectPhotos[selectedStage][selectedEventPhotoIndex].description
                                                    }</Text>
                                                </ScrollView>
                                            </>
                                    }

                                    {
                                        isClickEditPhoto ?
                                            <TouchableOpacity
                                                style={styles.buttonContainer}
                                                onPress={() => handleSave()}
                                            >
                                                <Text style={{
                                                    color: 'white',
                                                    fontSize: hp('3%'),
                                                    textAlign: 'center',
                                                    fontFamily: 'SourceSansPro-Regular',
                                                }}>Save</Text>
                                            </TouchableOpacity>
                                            :
                                            <TouchableOpacity
                                                style={styles.buttonContainer}
                                                onPress={() => handleEditPhoto()}
                                            >
                                                <Text style={{
                                                    color: 'white',
                                                    fontSize: hp('3%'),
                                                    textAlign: 'center',
                                                    fontFamily: 'SourceSansPro-Regular',
                                                }}>
                                                    <Icon color={'rgb(68, 67, 67)'} color='white' size={18} type='feather' name={'edit'} />{'  '}Edit
                                                </Text>
                                            </TouchableOpacity>
                                    }
                                </View>
                            </KeyboardAvoidingView >
                        </ScrollView>

                    </SafeAreaView >
            }

        </View >


    );
};

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: '#fff',
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    allAlbums: {
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-Bold',
        color: 'rgb(68, 67, 67)',
    },

    buttonContainer: {
        width: wp('80%'),
        height: hp('8%'),
        backgroundColor: 'rgb(61, 135, 241)',
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        // marginTop: hp('20%')
    },

    more: {
        fontSize: hp('2.2%'),
        fontFamily: 'SourceSansPro-Bold',
        color: '#fff',
    },

    count: {
        marginTop: hp('5%'),
        fontSize: hp('2.1%'),
        color: 'rgb(143, 143, 143)',
        fontFamily: 'SourceSansPro-Regular',
    },

    projectName: {
        fontSize: hp('2.5%'),
        color: '#3d87f1',
        fontFamily: 'SourceSansPro-Bold',
    },

    addphoto: {
        fontSize: hp('2.2%'),
        color: '#3d87f1',
        fontFamily: 'SourceSansPro-Regular',
    },

    textMessage: {
        fontSize: hp('2.2%'),
        color: 'rgb(143,143,143)',
        fontFamily: 'SourceSansPro-Regular',
        textTransform: 'capitalize'
    },
    thumbnailText: {
        width: wp('90%'),
        height: hp('4%'),
        fontSize: hp('1.8%'),
        textAlign: 'center',
        color: 'rgb(190,198,214)'
    },

    groupimage: {
        height: hp('50.5%'),
        width: wp('80.3%'),
    },
    option_text: {
        fontSize: hp('2.2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
    },
});

export default GetProjectPhotos;
