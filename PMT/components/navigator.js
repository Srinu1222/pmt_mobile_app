import React from "react";
import { StatusBar } from 'react-native'
import {
  createStackNavigator,
  TransitionPresets,
} from "@react-navigation/stack";
import { NavigationContainer, useNavigationState, useNavigation } from "@react-navigation/native";
import { createDrawerNavigator } from "@react-navigation/drawer";
import Routes from "../Routes/Routes";
import ProjectRoutes from "../Routes/ProjectRoutes"
import SignUpScreen from "./SignUpScreen";
import SignUpScreen1 from "./SignUp/SignUpScreen1";
import SignUpScreen2 from "./SignUp/SignUpScreen2";
import SignUpScreen3 from "./SignUp/SignUpScreen3";
import TeamMembers from "./SignUp/TeamMembers";
import EditTeam from "./SignUp/EditTeam";
import RemoveTeam from "./SignUp/RemoveTeam";
import Onboard from "./SignUp/Onboard";


import Login from "./Login";
import DrawerRoutes from "../Routes/DrawerRoutes";
import SampleGantts from './SampleGantts'
import SubContractor from './SubContractor'
import Inventory from './Inventory'
import Account from './Account'
import AccountNext from './Account/AccountNext'
import InventoryFilter from './Inventory/InventoryFilter'
import ProjectAnalytics from './ProjectAnalytics'
import ProjectDashboard from './ProjectDashboard'
import ProjectGanttChart from './ProjectGanttChart'
import ProjectFinance from './ProjectFinance'
import ProjectDocuments from './ProjectDocuments'
import ProjectReports from './ProjectReports'
import ProjectMaterialsUsed from './ProjectMaterialsUsed'
import ProjectTeam from './ProjectTeam'
import ProjectLevelProjectAnalytics from './ProjectLevelProjectAnalytics'
import ProjectChangeSpecification from './ProjectChangeSpecification'
import ProjectSpecification from './ProjectSpecification'
import CustomStatusBar from './CustomStatusBar'
import DailyReport from '../components/DailyReports/DailyReport'
import AddTeam from '../components/teams/AddTeam'
import AddInventory from '../components/Inventory/AddInventory'
import AddSubContractor from '../components/SubContractor/AddSubContractor'
import Settings from '../components/Settings'
import Feedback from '../components/Feedback'
import NewProject from './NewProjectBasic'
import CustomerDetails from './NewProject/CustomerDetails';
import TeamDetails from './NewProject/TeamDetails';
import SubContractorDetails from './NewProject/SubContractorDetails';
import Databank from './NewProject/Databank';
import ProjectSpecification1 from './NewProject/ProjectSpecification';
import ProjectPlaning from './NewProject/ProjectPlanning';
import ProjectPlanning1 from '../components/ProjectPlanning/projectPlanning1'
import ProjectStage from '../components/ProjectPlanning/ProjectStage';
// import EventScreen from '../components/DetailedEventPortraitView/EventScreen'
import MaterialFilter from '../components/ProjectMaterialUsed/MaterialFilter'
import ProjectMaterialUsed from '../components/ProjectMaterialsUsed'
import TeamFilter from '../components/teams/TeamFilter'
import TeamRemove from '../components/teams/TeamRemove'
import Team from '../components/Team'
import InventoryAssign from '../components/Inventory/InventoryAssign'
import ProjectSpecificationStageList from '../components/ProjectSpecification/ProjectSpecification1'
import Notification from '../components/Notification/Notification'
import TasksList from '../components/TasksList/TasksList'
import GanttChartcomponent from '../components/GanttChart/GanttChartcomponent'
import GanttChartView from "./GanttChart/GanttChartView";
import ResponsibilityMatrix from "./NewProject/ResponsibilityMatrix";
import Selectscreen from './NewProject/Selectscreen'
import Toast from "react-native-toast-message";
import Dashboard from "../components/Dashboard";
import { DailyReportsStackScreen } from '../Routes/Routes';
import { PhotosStackScreen } from '../Routes/Routes';
import { ProjectPhotosStackScreen } from '../Routes/ProjectRoutes';
import StageSlider from "./DetailedEventPortraitView/StageSlider";
import StageScreen1 from './DetailedEventPortraitView/StageScreen1';
import { useDispatch, useSelector } from "react-redux";
import Photos from './Photos';
import ProjectPhotos from './ProjectPhotos';
import AddProjectPhoto from '../components/ProjectPhotos/AddProjectPhoto';


const AuthStack = createStackNavigator();
const Drawer = createDrawerNavigator();
const AccountStack = createStackNavigator();

const GanttChartStack = createStackNavigator();
const NewProjectStack = createStackNavigator();

const NewProjectStackScreen = ({ navigation, route }) => (
  <NewProjectStack.Navigator
    screenOptions={{
      headerShown: false,
      headerMode: 'screen',
    }}
  >
    <NewProjectStack.Screen initialParams={{ token: route.params.token }} name="NewProject" component={NewProject} />
    <NewProjectStack.Screen initialParams={{ token: route.params.token }}
      name="CustomerDetails" component={CustomerDetails} />
    <NewProjectStack.Screen initialParams={{ token: route.params.token }}
      name="TeamDetails" component={TeamDetails} />
    <NewProjectStack.Screen initialParams={{ token: route.params.token }}
      name="SubContractorDetails" component={SubContractorDetails} />
    <NewProjectStack.Screen initialParams={{ token: route.params.token }}
      name="Databank" component={Databank} />
    <NewProjectStack.Screen initialParams={{ token: route.params.token }}
      name="ProjectSpecification1" component={ProjectSpecification1} />
    <NewProjectStack.Screen initialParams={{ token: route.params.token }}
      name="ProjectPlaning" component={ProjectPlaning} />
    <NewProjectStack.Screen initialParams={{ token: route.params.token }}
      name="ResponsibilityMatrix" component={ResponsibilityMatrix} />
    <NewProjectStack.Screen initialParams={{ token: route.params.token }}
      name="Selectscreen" component={Selectscreen} />
  </NewProjectStack.Navigator>
)
// const GanttChartScreen = ({ navigation }) => (
//   <GanttChartStack.Navigator
//     screenOptions={{
//       headerShown: false,
//     }}>
//     <GanttChartStack.Screen name="Home" component={GanttChartcomponent} />

//   </GanttChartStack.Navigator>
// );

export default Navigator = (props) => {





  return (
    <NavigationContainer  >


      <StatusBar backgroundColor="#236AD0" StatusBarStyle="light-content" {...props} />

      {/* <CustomStatusBar backgroundColor="#236AD0" StatusBarStyle="light-content" {...props}/> */}

      {props.token === null ? (
        <AuthStack.Navigator
          screenOptions={({ route, navigation }) => ({
            headerShown: false,
          })}
        >

          <AuthStack.Screen name="Login" component={Login} />
          <AuthStack.Screen name="Signup" component={SignUpScreen} />
          <AuthStack.Screen name="Signup1" component={SignUpScreen1} />
          <AuthStack.Screen name="Signup2" component={SignUpScreen2} />
          <AuthStack.Screen name="Signup3" component={SignUpScreen3} />
          <AuthStack.Screen name="Signup4" component={TeamMembers} />
          <AuthStack.Screen name="Signup5" component={EditTeam} />
          <AuthStack.Screen name="Signup6" component={RemoveTeam} />
          <AuthStack.Screen name="Onboard" component={Onboard} />





        </AuthStack.Navigator>
      ) : (
        <Drawer.Navigator
          // headerMode='screen'
          drawerContentOptions={{ activeBackgroundColor: '#ECF3FE', activeTintColor: '#3d87f1' }}
          drawerContent={(props) => <DrawerRoutes {...props} screenOptions={{ headerShown: true, headerMode: 'screen' }} />}
        >
          {/* Home Section Drawer Routes */}
          <Drawer.Screen initialParams={{ token: props.token }} name="homeDrawer" component={Routes} />     
          <Drawer.Screen initialParams={{ token: props.token }} name="project_dashboard" component={ProjectRoutes} />


          <Drawer.Screen initialParams={{ token: props.token }} name="sampleGantts" component={SampleGantts} />
          <Drawer.Screen initialParams={{ token: props.token }} name="subContractor" component={SubContractor} />
          <Drawer.Screen initialParams={{ token: props.token }} name="inventory" component={Inventory} />
          <Drawer.Screen initialParams={{ token: props.token }} name="account" component={Account} />
          <Drawer.Screen initialParams={{ token: props.token }} name="projectAnalytics" component={ProjectAnalytics} />
          {/* Project Section Drawer Routes */}
          <Drawer.Screen initialParams={{ token: props.token }} name="projectFinance" component={ProjectFinance} />
          <Drawer.Screen initialParams={{ token: props.token }} name="projectDocuments" component={ProjectDocuments} />
          <Drawer.Screen
            initialParams={{
              token: props.token,
              project_id: props.project_id,
              is_sub_contractor: props.is_sub_contractor
            }}
            name="projectReports"
            component={ProjectReports}
          />
          <Drawer.Screen
            initialParams={{
              token: props.token,
              project_id: props.project_id,
              is_sub_contractor: props.is_sub_contractor
            }}
            name="projectMaterialsUsed"
            component={ProjectMaterialsUsed}
          />
          <Drawer.Screen initialParams={{ token: props.token }} name="projectTeam" component={ProjectTeam} />
          <Drawer.Screen initialParams={{ token: props.token }} name="projectChangeSpecification" component={ProjectChangeSpecification} />
          <Drawer.Screen initialParams={{ token: props.token }} name="projectSpecification" component={ProjectSpecification} />
          <Drawer.Screen initialParams={{ token: props.token }} name="settings" component={Settings} />
          <Drawer.Screen initialParams={{ token: props.token }} name="Feedback" component={Feedback} />

          {/* projectGanttChart Section Drawer Routes */}
          <Drawer.Screen initialParams={{ token: props.token }} name="projectGanttChart" component={ProjectGanttChart} options={{
            swipeEnabled: false,
          }} />
          <Drawer.Screen initialParams={{ token: props.token }} name="GanttChartcomponent" component={GanttChartcomponent} options={{
            swipeEnabled: false,
          }} />
          {/* <Drawer.Screen initialParams={{ token: props.token }} name="GanttChartView" component={GanttChartView} options={{
          swipeEnabled: false,
        }}/> */}

          {/* Start New Project */}
          <Drawer.Screen initialParams={{ token: props.token }} name="NewProjectStack" component={NewProjectStackScreen} />

          {/* Start New Project */}
          {/* <Drawer.Screen initialParams={{ token: props.token }} name="NewProject" component={NewProject} />
          <Drawer.Screen initialParams={{ token: props.token }} name="CustomerDetails" component={CustomerDetails} />
          <Drawer.Screen initialParams={{ token: props.token }} name="TeamDetails" component={TeamDetails} />
          <Drawer.Screen initialParams={{ token: props.token }} name="SubContractorDetails" component={SubContractorDetails} />
          <Drawer.Screen initialParams={{ token: props.token }} name="Databank" component={Databank} />
          <Drawer.Screen initialParams={{ token: props.token }} name="ProjectSpecification1" component={ProjectSpecification1} />
          <Drawer.Screen initialParams={{ token: props.token }} name="ProjectPlaning" component={ProjectPlaning} /> */}

          {/* Start Gnatt Chart View */}

          {/* Add team , inventory, subcontractor*/}
          <Drawer.Screen initialParams={{ token: props.token }} name="addInventory" component={AddInventory} />
          <Drawer.Screen initialParams={{ token: props.token }} name="addSubContractor" component={AddSubContractor} />

          {/* New Project and Daily Report Routes */}
          <Drawer.Screen initialParams={{ token: props.token }} name="DailyReports" component={DailyReportsStackScreen} />

          {/* project planning */}

          {/* Project DashBoard */}
          {/* <Drawer.Screen
            initialParams={{
              token: props.token,
              project_id: props.project_id,
              is_sub_contractor: props.is_sub_contractor
            }}
            name="project_dashboard"
            component={ProjectDashboard}
          /> */}

          {/* Detailed Events Portrait View */}
          {/* <Drawer.Screen initialParams={{ token: props.token }} name="DetailedEventPortraitView" component={StageScreen1} /> */}
          
          {/* Company level Photos */}
          <Drawer.Screen initialParams={{ token: props.token }} name="photosSection" component={PhotosStackScreen} />
          {/* Project level Photos */}
          <Drawer.Screen initialParams={{
            token: props.token,
            projectId: props.projectId,
            projectName: props.projectName,
            is_sub_contractor: props.is_sub_contractor
          }}
            name="projectPhotos"
            component={ProjectPhotos}
          />
          <Drawer.Screen initialParams={{token: props.token}} name="addProjectPhoto" component={AddProjectPhoto}/>



          {/* Inventory Filter*/}
          <Drawer.Screen initialParams={{ token: props.token }} name="inventory_filter" component={InventoryFilter} />
          <Drawer.Screen initialParams={{ token: props.token }} name="inventory_assign" component={InventoryAssign} />

          {/* Project MaterialUsed filter */}
          <Drawer.Screen initialParams={{ token: props.token }} name="material_used_filter" component={MaterialFilter} />
          <Drawer.Screen initialParams={{ token: props.token }} name="project_material" component={ProjectMaterialUsed} />

          {/* Team filter and remove */}
          <Drawer.Screen initialParams={{ token: props.token }} name="team_filter" component={TeamFilter} />
          <Drawer.Screen initialParams={{ token: props.token }} name="company_team" component={Team} />
          <Drawer.Screen initialParams={{ token: props.token }} name="team_remove" component={TeamRemove} />

          {/* Specification */}
          <Drawer.Screen initialParams={{ token: props.token }} name="project_specification_stage_list" component={ProjectSpecificationStageList} />

          {/* Notification */}
          <Drawer.Screen initialParams={{ token: props.token }} name="notification" component={Notification} />

          {/* Notification */}
          <Drawer.Screen initialParams={{ token: props.token }} name="taskslist" component={TasksList} />

          {/* Dashboard */}
          <Drawer.Screen initialParams={{ token: props.token }} name="dashboard" component={Dashboard} />

        </Drawer.Navigator>
      )}
      <Toast ref={(ref) => Toast.setRef(ref)} />

    </NavigationContainer>
  );
};
