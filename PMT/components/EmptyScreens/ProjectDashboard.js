import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView
} from 'react-native';
import React, { useState, useMemo } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { SearchBar } from 'react-native-elements';
import EmptyScreenHeader from './EmptyScreenHeader';


const EmptyProjectDashboard = (props) => {

    

    const textArray = ['No recent activities', 'No closed activities', "Can't find data"]

    return (
        <View style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
            {
                props.index != 2 ?
                    < FastImage
                        style={{ width: wp('70%'), height: hp('40%') }}
                        source={require('../../assets/icons/NoRecentActivities.png')}
                        resizeMode={FastImage.resizeMode.contain}
                    />
                    :
                    < FastImage
                        style={{ width: wp('30%'), height: hp('40%') }}
                        source={require('../../assets/icons/dailyreport.png')}
                        resizeMode={FastImage.resizeMode.contain}
                    />
            }

            <Text style={styles.text}>
                {textArray[props.index]}
            </Text>

        </View>


    );
};

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    text: {
        fontFamily: 'SourceSansPro-Regular',
        fontSize: hp('3%'),
        textAlign: 'center',
        color: 'rgb(143, 143, 143)'
    },
});

export default EmptyProjectDashboard;
