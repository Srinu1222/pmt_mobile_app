import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView
} from 'react-native';
import React, { useState, useMemo } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { SearchBar } from 'react-native-elements';
import EmptyScreenHeader from './EmptyScreenHeader';


const EmptyDashBoard = (props) => {

    const newProject = (params) => {
        props.navigation.navigate('NewProjectStack')
    };

    return (
        <View style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            <FastImage
                style={styles.noProjectImg}
                source={require("../../assets/icons/group.png")}
                resizeMode={FastImage.resizeMode.contain}
            />
            <View style={{display: 'flex', alignItems: 'center', marginVertical: hp('4%') }}>
                <Text style={[styles.textMessage]}>No projects to display</Text>
                <Text style={styles.thumbnailText}>{props.type=='active' ? 'Please start a new project!' : 'None of your projects have been completed yet!'}</Text>
            </View>

            <TouchableOpacity
                onPress={() => { newProject() }}
                style={styles.buttonContainer}>
                <Text style={{
                    color: 'white',
                    fontSize: hp('3%'),
                    textAlign: 'center',
                    fontFamily: 'SourceSansPro-Regular',
                }}>Start New Project</Text>
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    textMessage: {
        width: wp('80%'),
        textAlign: 'center',
        fontSize: hp('2.3%'),
        color: 'rgb(143,143,143)'
    },

    thumbnailText: {
        width: wp('90%'),
        height: hp('4%'),
        fontSize: hp('1.8%'),
        textAlign: 'center',
        color: 'rgb(190,198,214)'
    },

    groupimage: {
        height: hp('40.5%'),
        width: wp('50.3%'),
    },

    buttonContainer: {
        width: wp('80%'),
        height: hp('8%'),
        backgroundColor: 'rgb(61, 135, 241)',
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 30
    },
    noProjectImg: {
        height: hp('40%'),
        width: wp('50.3%'),
    },
});

export default EmptyDashBoard;
