import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView
} from 'react-native';
import React, { useState, useMemo } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { SearchBar } from 'react-native-elements';
import EmptyScreenHeader from './EmptyScreenHeader';


const EmptyTaskList = (props) => {

    return (
        <View style={styles.container}>
            <FastImage
                style={styles.noProjectImg}
                source={require("../../assets/icons/notasks.png")}
                resizeMode={FastImage.resizeMode.contain}
            />
            <View style={{display: 'flex', alignItems: 'center', marginVertical: hp('4%') }}>
                <Text style={[styles.textMessage]}>No tasks yet!</Text>
                <Text style={styles.thumbnailText}>We will notify you when something arrives.</Text>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({

    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height*0.8,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    textMessage: {
        width: wp('80%'),
        textAlign: 'center',
        fontSize: hp('3%'),
        paddingVertical: hp('2%'),
        color: 'rgb(143,143,143)'
    },

    thumbnailText: {
        width: wp('90%'),
        height: hp('4%'),
        fontSize: hp('2%'),
        textAlign: 'center',
        color: 'rgb(190,198,214)'
    },

    noProjectImg: {
        height: hp('40%'),
        width: wp('80.3%'),
    },
});

export default EmptyTaskList;
