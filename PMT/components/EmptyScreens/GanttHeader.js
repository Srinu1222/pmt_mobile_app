import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView
} from 'react-native';
import React, { useState, useMemo } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Icons from "react-native-vector-icons/Ionicons";
import ProgressCircle from 'react-native-progress-circle';
import FastImage from 'react-native-fast-image'
import ElevatedView from '../ElevatedView'
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

const GanttHeader = (props) => {

    return (
        <ElevatedView elevation={8} style={styles.customHeaderContainer}>
            <TouchableWithoutFeedback onPress={props.navigation.goBack}>
            <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
            {/* <StatusBar hidden={true} /> */}

            <FastImage
                                    style={styles.image_logo}
                                    source={require('../../assets/icons/crossbar.png')
                                    }
                                    resizeMode={FastImage.resizeMode.contain} 
                                    /> 
        <Text style={[styles.pageTitle1]}>Exit gantt </Text>

            </View>
            </TouchableWithoutFeedback>
            <View style={{marginLeft:150,flexDirection:'row',justifyContent:'center'}}>

            <View  style={{backgroundColor:'#3d87f1',height:15,width:15}}></View>
            <Text style={[styles.pageTitle]}>Planned </Text>
            <View  style={{backgroundColor:'#0db501',height:15,width:15}}></View>

            <Text style={[styles.pageTitle]}>Actual </Text>
            <View  style={{backgroundColor:'#fff000',height:15,width:15 }}></View>

            <Text style={[styles.pageTitle]}>In-progress </Text>
            <View  style={{backgroundColor:'#fc051d',height:15,width:15 }}></View>

            <Text style={[styles.pageTitle]}>Delay </Text>

            </View>
           
            {/* <View style={[styles.customHeaderRightIconsContainer]}>
               <Text style={[styles.pageTitle]}>|</Text>
               <Text style={[styles.pageTitle1]}>Cancel</Text>
            </View> */}
        </ElevatedView>
    )
}


const styles = StyleSheet.create({

    customHeaderContainer: {
        width: hp('110%'),
        height: hp('5%'),
        alignItems: 'center',
        backgroundColor: "#fff",
        paddingHorizontal: '3%',
        flexDirection: 'row',
    },

    customHeaderRightIconsContainer: {
        flexDirection: "row",
        width: wp("38%"),
        marginRight:-20,
        
    },

    pageTitle: {
        fontSize: 14,
        marginLeft:5,
        marginRight:20,
        fontFamily: "SourceSansPro-Regular",
        color: "rgb(68,67,67)",
    },
    pageTitle1: {
        fontSize: 20,
        marginLeft:20,
        fontFamily: "SourceSansPro-Regular",
        color: "rgb(68,67,67)",
    },

    image_logo: {
        // color:'#000',
        width: wp("4%"),
        height: wp("4%"),
        borderRadius: hp("0.35%"), // Equivalent to borderRadius: 2.
    },
    images: {
        backgroundColor: "#4F92F2",
        maxWidth: wp("11%"),
        height: wp("11%"), //
        borderRadius: 4,
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },

});

export default GanttHeader;
