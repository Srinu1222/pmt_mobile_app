import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView
} from 'react-native';
import React, { useState, useMemo } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { SearchBar } from 'react-native-elements';
import EmptyScreenHeader from './EmptyScreenHeader';
import AddSubContractor from '../SubContractor/AddSubContractor'

const SubContractorEmptyScreen = (props) => {

    const [addSubContractor, setaddSubContractor] = React.useState(false);

    const onClickOnAddButton = (params) => {
        setaddSubContractor(true)
    }

    if (addSubContractor) {
        return (<AddSubContractor />);
    } else {
        return (
            <>
                <KeyboardAvoidingView>
                    <View style={styles.container}>
                        <EmptyScreenHeader {...props} title='Contractor' />
                        <View style={styles.parent2}>
                            < FastImage
                                style={styles.groupimage}
                                source={require('../../assets/icons/subcontractor.png')
                                }
                                resizeMode={FastImage.resizeMode.contain}
                            />
                            <Text style={{ width: wp('80%'), height: hp('10%'), top: hp('8%'), textAlign: 'center', fontSize: hp('2.4%'), color: 'rgb(143,143,143)' }}>No Sub-Contractors Added.</Text>
                            <Text style={{ width: wp('80%'), height: hp('4%'), top: hp('4%'), fontSize: hp('2%'), textAlign: 'center', color: 'rgb(190,198,214)' }}>Please add a new sub-contractor.</Text>

                            <TouchableOpacity
                                onPress={() => {onClickOnAddButton()}}
                                style={{
                                    top: hp('50%'),
                                    position: 'absolute',
                                    width: wp('80%'),
                                    height: hp('8%'),
                                    backgroundColor: 'rgb(61, 135, 241)',
                                    borderRadius: 5,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}>
                                <Text style={{
                                    color: 'white',
                                    fontSize: hp('2.8%'),
                                    textAlign: 'center',
                                    fontFamily: 'SourceSansPro-Regular',
                                }}>+Add Sub-Contractor</Text>

                            </TouchableOpacity>
                        </View>
                    </View>
                </KeyboardAvoidingView>
            </ >
        );
    }
};

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    parent2: {
        top: hp('20%'),
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        alignSelf: 'auto',
        alignItems: 'center',
        flex: 11,
        backgroundColor: 'white',
    },

    groupimage: {
        height: hp('25.5%'),
        width: wp('55.3%'),
    }
});

export default SubContractorEmptyScreen;
