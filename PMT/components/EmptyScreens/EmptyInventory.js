import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView
} from 'react-native';
import React, { useState, useMemo } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { SearchBar } from 'react-native-elements';
import EmptyScreenHeader from './EmptyScreenHeader';


const EmptyInventory = (props) => {

    const onPressAddMaterial = (params) => {
        props.navigation.navigate('addInventory')
    };

    return (
        <View style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            < FastImage
                style={styles.groupimage}
                source={require('../../assets/icons/inventory.png')
                }
                resizeMode={FastImage.resizeMode.contain}
            />
            <Text style={styles.textMessage}>There are no materials added to the inventory.</Text>
            <Text style={styles.thumbnailText}>Please add new material.</Text>
            <TouchableOpacity
                onPress={() => { onPressAddMaterial() }}
                style={styles.buttonContainer}>
                <Text style={{
                    color: 'white',
                    fontSize: hp('3%'),
                    textAlign: 'center',
                    fontFamily: 'SourceSansPro-Regular',
                }}>+Add Material</Text>
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    textMessage: {
        width: wp('80%'),
        height: hp('8%'),
        textAlign: 'center',
        fontSize: hp('2.3%'),
        color: 'rgb(143,143,143)'
    },

    thumbnailText: {
        width: wp('90%'),
        height: hp('4%'),
        fontSize: hp('1.8%'),
        textAlign: 'center',
        color: 'rgb(190,198,214)'
    },

    groupimage: {
        height: hp('40.5%'),
        width: wp('50.3%'),
    },

    buttonContainer: {
        width: wp('80%'),
        height: hp('8%'),
        backgroundColor: 'rgb(61, 135, 241)',
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 30
    }
});

export default EmptyInventory;
