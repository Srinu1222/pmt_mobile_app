import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
} from "react-native";
import React, { useState, useMemo } from "react";
import LottieView from "lottie-react-native";
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import FastImage from "react-native-fast-image";
import { SearchBar } from "react-native-elements";
import Icons from "react-native-vector-icons/Ionicons";


const AllProjects = (props) => {
    const [search, setSearch] = useState("");
    const updateSearch = (search) => {
        setSearch(search);
    };
    const newProject = (params) => { 
        props.navigation.navigate('NewProjectStack')
    };
    return (
        <SafeAreaView>
            {/* <ScrollView> */}

            <KeyboardAvoidingView>
                    <View style={styles.container}>
                        <View style={styles.parent1}>
                            <View style={styles.container_1_1st_box}>
                                <View>
                                    <TouchableOpacity
                                        onPress={() => { newProject }}
                                    >
                                        <Text style={styles.horizontalbar}></Text>
                                        <Text style={styles.horizontalbar}></Text>
                                        <Text style={styles.horizontalbar}></Text>
                                    </TouchableOpacity>
                                </View>

                                <Text style={{ top: hp('2%'), left: wp('7%'), fontSize: hp('2.8%'), height: hp('10%'), fontFamily: 'SourceSansPro-Regular', 'color': 'white' }}>All Projects</Text>

                                <TouchableOpacity
                                    onPress={() => { newProject }}
                                    style={styles.images}
                                >
                                    <FastImage
                                        style={styles.image_logo}
                                        source={require("../../assets/icons/newproject.png")}
                                        resizeMode={FastImage.resizeMode.contain}
                                    />
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => { newProject }}
                                    style={styles.images}
                                >
                                    <FastImage
                                        style={styles.image_logo}
                                        source={require("../../assets/icons/tasklist.png")}
                                        resizeMode={FastImage.resizeMode.contain}
                                    />
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => { newProject }}
                                    style={styles.images}>
                                    <FastImage
                                        style={styles.image_logo}
                                        source={require("../../assets/icons/notification.png")}
                                        resizeMode={FastImage.resizeMode.contain}
                                    />
                                </TouchableOpacity>
                            </View>
                        </View>

                        {/* <ScrollView> */}

                        <View style={styles.parent2}>
                            < FastImage
                                style={styles.groupimage}
                                source={require('../../assets/icons/group.png')
                                }
                                resizeMode={FastImage.resizeMode.contain}
                            />

                            <Text style={{ width: wp('60%'), height: hp('5%'), textAlign: 'center', fontSize: hp('2.5%'), color: 'rgb(143,143,143)' }}>No projects to display</Text>
                            <Text style={{ width: wp('80%'), height: hp('4%'), fontSize: hp('2%'), textAlign: 'center', color: 'rgb(190,198,214)' }}>Please start a new project</Text>

                            <TouchableOpacity
                                onPress={() => { newProject }}
                                style={{
                                    top: hp('45%'),
                                    position: 'absolute',
                                    width: wp('80%'),
                                    height: hp('8%'),
                                    backgroundColor: 'dodgerblue',
                                    borderRadius: 5,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}>
                                <Text style={{
                                    color: 'white',
                                    fontSize: hp('3%'),
                                    textAlign: 'center',
                                    fontFamily: 'SourceSansPro-Regular',
                                }}>Start New Project</Text>

                            </TouchableOpacity>
                        </View>
                        {/* </ScrollView> */}

                    </View>
                    {/* --------------------Content Section---------------------- */}
                    <View style={styles.projectsDisplaySection}>
                        <FastImage
                            style={styles.noProjectImg}
                            source={require("../../assets/icons/group.png")}
                            resizeMode={FastImage.resizeMode.contain}
                        />
                        <Text style={[styles.noProjectText]}>No projects to display</Text>
                        <Text style={[styles.startNewText]}>
                            Please start a new project
                        </Text>
                        <TouchableOpacity
                            onPress={() => {
                                newProject;
                            }}
                            style={[styles.btnStartNewProject]}
                        >
                            <Text style={[styles.btnText]}>Start New Project</Text>
                        </TouchableOpacity>
                    </View>
            </KeyboardAvoidingView>
            {/* </ScrollView> */}
        </SafeAreaView>
    );
};
const styles = StyleSheet.create({
    mainContainer: {
        width: Dimensions.get("window").width,
        height: Dimensions.get("window").height,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "white",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
    customHeaderAndSearchBarContainer: {
        width: Dimensions.get("window").width,
        height: Dimensions.get("window").height,
        flex: 1,
        alignSelf: 'auto',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#3d87f1",
    },
    customHeaderContainer: {
        display: "flex",
        flexDirection: "row",
        // justifyContent: "space-between",
        alignItems: "center",
        bottom: hp("3%"),
        width: wp("100%"),
        paddingVertical: hp("1%"),
    },
    pageTitle: {
        fontSize: 17,
        fontSize: hp("3.06%"),
        fontFamily: "SourceSansPro-Regular",
        color: "white",
    },
    customHeaderRightIconsContainer: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        width: wp("42%"),
        marginLeft: wp("12%"),
        marginRight: wp("5%"),
    },
    image_logo: {
        width: wp("5.75%"),
        height: hp("2.85%"),
        borderRadius: hp("0.35%"), // Equivalent to borderRadius: 2.
    },
    images: {
        backgroundColor: "#4F92F2",
        maxWidth: wp("11%"),
        height: wp("11%"), //
        borderRadius: 4,
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    searchBoxContainer: {
        display: "flex",
        flexDirection: "row",
        width: wp("79.4%"),
        backgroundColor: "#5596F3",
        borderRadius: 5,
        height: hp("8%"),
    },
    search_logo: {
        width: wp("10%"),
        height: hp("3%"),
        left: wp("3%"),
        top: hp("2.5%"),
        marginRight: wp("1%"),
    },
    searchtextinput: {
        left: wp("1%"),
        fontFamily: "SourceSansPro-Regular",
        fontSize: hp("2.5%"),
        width: wp("68%"),
    },
    projectsDisplaySection: {
        marginTop: hp("5%"),
        marginBottom: hp("2%"),
        width: Dimensions.get("window").width,
        height: Dimensions.get("window").height,
        flex: 3,
        alignItems: "center",
        backgroundColor: "#ffffff",
    },

    container_1_1st_box: {
        display: 'flex',
        flexDirection: 'row',
        bottom: hp('2.9%'),
        width: wp('100%'),
        backgroundColor: '#3d87f1',
        height: hp('9%')
    },
    noProjectText: {
        textAlign: "center",
        fontSize: hp("3.25%"),
        lineHeight: 25,
        color: "#8F8F8F",
        marginTop: hp("10%"),
        fontFamily: "SourceSansPro-Regular",
    },
    startNewText: {
        fontSize: hp("2.5%"),
        textAlign: "center",
        color: "rgb(190,198,214)",
        fontFamily: "SourceSansPro-Regular",
    },
    btnStartNewProject: {
        marginTop: hp("5%"),
        width: wp("75%"),
        height: hp("8%"),
        backgroundColor: "#3d87f1",
        borderRadius: 5,
        alignItems: "center",
        justifyContent: "center",
    },
    btnText: {
        color: "white",
        fontSize: hp("2.6%"),
        lineHeight: 20,
        textAlign: "center",
        fontFamily: "SourceSansPro-Regular",
    },
});
export default AllProjects;