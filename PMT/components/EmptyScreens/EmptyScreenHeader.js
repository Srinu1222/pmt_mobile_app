import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    TouchableWithoutFeedback,
    ScrollView
} from 'react-native';
import React, { useState, useMemo } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { Avatar, Badge, Icon, withBadge } from 'react-native-elements';
import Icons from "react-native-vector-icons/Ionicons";
import ElevatedView from '../ElevatedView';
import config from '../../config';

const EmptyScreenHeader = (props, { navigation }) => {

    return (
        <ElevatedView elevation={4} style={styles.customHeaderContainer}>
            <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                <TouchableWithoutFeedback onPress={() => {
                    props.navigation.openDrawer();
                }}>
                    <Icons.Button
                        name="ios-menu"
                        size={25}
                        backgroundColor="#3d87f1"
                    />
                </TouchableWithoutFeedback>
                <Text style={[styles.pageTitle]}>{props.title}</Text>
            </View>
            <View style={[styles.customHeaderRightIconsContainer]}>
                {/* <TouchableOpacity
                    onPress={() => {
                        props.navigation.navigate('NewProjectStack')
                    }}
                    style={[styles.images]}
                >
                    <FastImage
                        style={styles.image_logo}
                        source={require("../../assets/icons/newproject.png")}
                        resizeMode={FastImage.resizeMode.contain}
                    />
                </TouchableOpacity> */}
                <TouchableOpacity
                    onPress={() => props.navigation.navigate('taskslist')}
                    style={[styles.images]}
                >
                    <FastImage
                        style={styles.image_logo}
                        source={require("../../assets/icons/tasklist.png")}
                        resizeMode={FastImage.resizeMode.contain}
                    />
                </TouchableOpacity>

                <View
                    style={[styles.images]}
                >
                    <Icon
                        color={'white'}
                        type='feather'
                        name='bell'
                        size={22}
                        onPress={() =>  
                            {
                                props.navigation.navigate('notification')
                            }
                        }
                    />

                    <Badge
                        status="error"
                        value={config.notificationCount}
                        containerStyle={{ position: 'absolute', top: -4, right: -4 }}
                    />
                </View>
            </View>
        </ElevatedView>
    )
}


const styles = StyleSheet.create({

    customHeaderContainer: {
        width: Dimensions.get('window').width,
        height: hp('8%'),
        alignItems: 'center',
        backgroundColor: "#3d87f1",
        paddingHorizontal: '3%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        elevation: 8,
    },

    customHeaderRightIconsContainer: {
        flexDirection: "row",
        justifyContent: "space-between",
        width: wp("25%"),
        marginLeft: wp("2%"),
    },

    pageTitle: {
        fontSize: 17,
        fontSize: hp("3.06%"),
        fontFamily: "SourceSansPro-Regular",
        color: "white",
    },

    image_logo: {
        width: wp("5.75%"),
        height: hp("2.85%"),
        borderRadius: hp("0.35%"), // Equivalent to borderRadius: 2.
    },
    images: {
        backgroundColor: "#4F92F2",
        maxWidth: wp("11%"),
        height: wp("11%"), //
        borderRadius: 4,
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },

});

export default EmptyScreenHeader;
