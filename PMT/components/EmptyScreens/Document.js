import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView
} from 'react-native';
import React, { useState, useMemo } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { SearchBar } from 'react-native-elements';
import EmptyScreenHeader from './EmptyScreenHeader'

const Document = (props) => {
    const [search, setSearch] = useState('');

    const updateSearch = (search) => {
        setSearch(search);
    };

    const newProject = (params) => {
    };

    return (
        <SafeAreaView>
            <KeyboardAvoidingView>
                <View style={styles.container}>
          

                    <EmptyScreenHeader {...props} title='Documents' />
                    <View style={styles.parent2}>
                        < FastImage
                            style={styles.groupimage}
                            source={require('../../assets/icons/group.png')
                            }
                            resizeMode={FastImage.resizeMode.contain}
                        />
                        <Text style={styles.textMessage}>No documents to display.</Text>
                        <Text style={styles.thumbnailText}>Please come back when new project is added.</Text>
                    </View>
                </View>
            </KeyboardAvoidingView>
        </SafeAreaView >
    );
};

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        display: 'flex',
        backgroundColor: "white",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    parent2: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        flex: 11,
        backgroundColor: 'white',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },

    textMessage: {
        width: wp('80%'),
        height: hp('5%'),
        textAlign: 'center',
        fontSize: hp('2.3%'),
        color: 'rgb(143,143,143)'
    },

    thumbnailText: {
        width: wp('90%'),
        height: hp('4%'),
        fontSize: hp('1.8%'),
        textAlign: 'center',
        color: 'rgb(190,198,214)'
    },

    groupimage: {
        height: hp('30.5%'),
        width: wp('30.3%'),
    }
});

export default Document;
