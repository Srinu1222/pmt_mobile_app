import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView
} from 'react-native';
import React, { useState, useMemo } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { SearchBar } from 'react-native-elements';
import EmptyScreenHeader from './EmptyScreenHeader'

const EmptyPhotos = (props) => {
    return (
        <View style={{ display: 'flex', alignItems: 'center' }}>
            < FastImage
                style={styles.groupimage}
                source={require('../../assets/icons/noPhotos.png')
                }
                resizeMode={FastImage.resizeMode.contain}
            />
            <Text style={styles.textMessage}>No photos have been added.</Text>
            <Text style={styles.thumbnailText}>Please add new photos.</Text>
            <TouchableOpacity
                onPress={() => { props.onAdd() }}
                style={styles.buttonContainer}>
                <Text style={{
                    color: 'white',
                    fontSize: hp('3%'),
                    textAlign: 'center',
                    fontFamily: 'SourceSansPro-Regular',
                }}>+Add Photo</Text>
            </TouchableOpacity>
            <Text style={styles.goBack}>Go back to "All Project Albums"</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        display: 'flex',
        backgroundColor: "white",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    textMessage: {
        width: wp('80%'),
        height: hp('5%'),
        textAlign: 'center',
        fontSize: hp('2.3%'),
        color: 'rgb(143,143,143)'
    },

    goBack: {
        width: wp('80%'),
        height: hp('5%'),
        textAlign: 'center',
        fontSize: hp('2.3%'),
        color: '#3d87f1',
        marginTop: 30,
        textDecorationLine: 'underline'
    },

    thumbnailText: {
        width: wp('90%'),
        height: hp('4%'),
        fontSize: hp('1.8%'),
        textAlign: 'center',
        color: 'rgb(190,198,214)'
    },

    groupimage: {
        height: hp('40.5%'),
        width: wp('80.3%'),
    },

    buttonContainer: {
        width: wp('80%'),
        height: hp('8%'),
        backgroundColor: 'rgb(61, 135, 241)',
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 30
    }
});

export default EmptyPhotos;
