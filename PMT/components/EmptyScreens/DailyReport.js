import React from 'react';
import {
  View,
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Dimensions,
  Image,
  Text,
  TouchableOpacity,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import LottieView from 'lottie-react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const EmptyDailyReport = (props) => {
  return (
    <View style={{ backgroundColor: '#ffffff' }}>
      <FastImage
        style={{ width: wp('36.5%'), height: hp('50%'), alignSelf: 'center' }}
        source={require('../../assets/icons/dailyreport.png')}
        resizeMode={FastImage.resizeMode.contain}
      />
      <Text
        style={{
          height: hp('5%'),
          textAlign: 'center',
          fontSize: hp('2.5%'),
          color: 'rgb(143,143,143)',
        }}>
        Can't find data
      </Text>

      <Text
        style={{
          height: hp('4%'),
          fontSize: hp('2%'),
          textAlign: 'center',
          color: 'rgb(190,198,214)',
        }}>
        Let's explore more content around you
      </Text>

      <TouchableOpacity
        onPress={() => {
          props.navigation.navigate('dashboard')
        }}
        style={{
          marginTop: hp('5%'),
          height: hp('8%'),
          backgroundColor: '#3d87f1',
          borderRadius: 5,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Text
          style={{
            color: 'white',
            fontSize: hp('3%'),
            textAlign: 'center',
            fontFamily: 'SourceSansPro-Regular',
          }}>
          Back to Dashboard
            </Text>
      </TouchableOpacity>

    </View>
    // <SafeAreaView>
    //   <View style={styles.container}>
    //     <View style={styles.parent1}>
    //       <Text style={styles.text}>Daily Report</Text>
    //       <TouchableOpacity
    //         onPress={() => {
    //           newProject();
    //         }}
    //         style={styles.images}>
    //         <FastImage
    //           style={styles.image_logo}
    //           source={require('../../assets/icons/crossbar.png')}
    //           resizeMode={FastImage.resizeMode.contain}
    //         />
    //       </TouchableOpacity>
    //     </View>
    //     <View style={styles.parent2}>


    // <Text
    //   style={{
    //     width: wp('60%'),
    //     height: hp('5%'),
    //     textAlign: 'center',
    //     fontSize: hp('2.5%'),
    //     color: 'rgb(143,143,143)',
    //   }}>
    //   Can't find data
    // </Text>

    // <Text
    //   style={{
    //     width: wp('80%'),
    //     height: hp('4%'),
    //     fontSize: hp('2%'),
    //     textAlign: 'center',
    //     color: 'rgb(190,198,214)',
    //   }}>
    //   Let's explore more content around you
    // </Text>

    // <TouchableOpacity
    //   onPress={() => {
    //     newProject();
    //   }}
    //   style={{
    //     marginTop: hp('5%'),
    //     width: wp('80%'),
    //     height: hp('8%'),
    //     backgroundColor: 'dodgerblue',
    //     borderRadius: 5,
    //     alignItems: 'center',
    //     justifyContent: 'center',
    //   }}>
    //   <Text
    //     style={{
    //       color: 'white',
    //       fontSize: hp('3%'),
    //       textAlign: 'center',
    //       fontFamily: 'SourceSansPro-Regular',
    //     }}>
    //     Back to Dashboard
    //   </Text>
    // </TouchableOpacity>

    //     </View>
    //   </View>
    // </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
  parent1: {
    // width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    flex: 1,
    alignSelf: 'auto',
    flexDirection: 'row',
    borderBottomWidth: 3,
    borderBottomColor: 'rgb(232,232,232)',
    width: wp('90%'),
    justifyContent: 'space-between',
  },

  parent2: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    alignSelf: 'auto',
    alignItems: 'center',
    flex: 11,
    backgroundColor: 'white',
  },

  text: {
    fontSize: hp('3.15%'),
    lineHeight: 25,
    alignSelf: 'center',
    color: 'rgb(68, 67, 67)',
    fontFamily: 'SourceSansPro-SemiBold',
  },

  image_logo: {
    width: wp('4%'),
    height: hp('2%'),
    alignSelf: 'center'
  },

  images: {
    marginTop: hp('3%'),
  },
});

export default EmptyDailyReport;
