import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView
} from 'react-native';
import React, { useState, useMemo } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import Icons from "react-native-vector-icons/Ionicons";
import ProgressCircle from 'react-native-progress-circle'


const NewProjectHeader = (props) => {

    return (
        <View style={styles.customHeaderContainer}>
            <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
            <ProgressCircle
            percent={props.percents}
            radius={30}
            borderWidth={5}
            color="#fff"
            shadowColor="#88b8fc"
            bgColor="#3d87f1"
        >
            <Text style={{ fontSize: 14,color:'#fff', fontFamily:'SourceSansPro-SemiBold' }}>{props.no}{' of 6'}</Text>
        </ProgressCircle>
        <Text style={[styles.pageTitle]}>{props.title}</Text>

            </View>
            
            <View style={[styles.customHeaderRightIconsContainer]}>
               <Text style={[styles.pageTitle]}>|</Text>
               <Text onPress={() => props.onCancel()} style={[styles.pageTitle1]}>Cancel</Text>
            </View>
        </View>
    )
}


const styles = StyleSheet.create({

    customHeaderContainer: {
        width: Dimensions.get('window').width,
        height: hp('12%'),
        alignItems: 'center',
        backgroundColor: "#3d87f1",
        paddingHorizontal: '3%',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },

    customHeaderRightIconsContainer: {
        flexDirection: "row",
        width: wp("38%"),
        marginRight:-20,
        
    },

    pageTitle: {
        fontSize: 17,
        fontSize: hp("2.6%"),
        marginLeft:20,
        fontFamily: "SourceSansPro-SemiBold",
        color: "white",
    },
    pageTitle1: {
        fontSize: 17,
        fontSize: hp("2.6%"),
        marginLeft:20,
        fontFamily: "SourceSansPro-SemiBold",
        color: "white",
        textDecorationLine:'underline',
    },

    image_logo: {
        width: wp("5.75%"),
        height: hp("2.85%"),
        borderRadius: hp("0.35%"), // Equivalent to borderRadius: 2.
    },
    images: {
        backgroundColor: "#4F92F2",
        maxWidth: wp("11%"),
        height: wp("11%"), //
        borderRadius: 4,
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },

});

export default NewProjectHeader;
