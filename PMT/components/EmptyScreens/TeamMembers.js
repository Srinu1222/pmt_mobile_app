import React from 'react';
import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TouchableOpacity
} from 'react-native';
import FastImage from 'react-native-fast-image';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import AddTeam from '../teams/AddTeam'
import { FAB, Icon } from 'react-native-elements';
import { Avatar, Button, Checkbox, Card, Divider, IconButton } from 'react-native-paper';


const TeamMembers = (props) => {

    const [addTeam, setaddTeam] = React.useState(false);

    const onClickOnAddButton = (params) => {
        setaddTeam(true)
    }

    if (addTeam) {
        return (<AddTeam />);
    } else {
        return (
            <View style={styles.container}>
                <View style={{ height: hp('9%'), justifyContent: 'space-between', padding: '5%', flexDirection: 'row' }}>
                    <Text style={styles.text}>Team Members(0)</Text>
                    <Icon
                        iconStyle={{ fontSize: hp('4.5%') }}
                        name='close-outline'
                        type='ionicon'
                        color='rgb(68, 67, 67)'
                    />
                </View>

                < Divider style={{ marginHorizontal: '5%', backgroundColor: 'rgb(232,232,232)', marginTop: '0%' }} />

                <View style={{alignItems: 'center'}}>
                    < FastImage
                        style={{ width: wp('79.5%'), height: hp('50%')}}
                        source={require('../../assets/icons/teammember.png')
                        }
                        resizeMode={FastImage.resizeMode.contain}
                    />
                    <Text style={{ width: wp('90%'), textAlign: 'center', fontSize: hp('2.5%'), color: 'rgb(143,143,143)' }}>No team members added to the list.</Text>
                    <Text style={{ width: wp('79%'), fontSize: hp('2%'), textAlign: 'center', color: 'rgb(190,198,214)' }}>Please add a new team member.</Text>
                    <TouchableOpacity
                        onPress={() => { onClickOnAddButton() }}
                        style={{
                            width: wp('80%'),
                            height: hp('8%'),
                            marginTop: hp('5%'),
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: 'dodgerblue',
                            borderRadius: 5,
                        }}>
                        <Text style={{
                            color: 'white',
                            fontSize: hp('3.15%'),
                            textAlign: 'center',
                            fontFamily: 'SourceSansPro-Regular',
                        }}>+Add</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

};

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: '#fff',
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    parent1: {
        // width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        flex: 1,
        alignSelf: 'auto',
        flexDirection: 'row',
        borderBottomWidth: 3,
        borderBottomColor: 'rgb(232,232,232)',
        width: wp('90%'),
        justifyContent: 'space-between'
    },

    parent2: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        alignSelf: 'auto',
        alignItems: 'center',
        flex: 11,
        backgroundColor: 'white',
    },

    text: {
        fontSize: hp('3.15%'),
        lineHeight: 25,
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center',
        fontFamily: 'SourceSansPro-SemiBold',
    },

    image_logo: {
        width: wp('4%'),
        height: hp('2%'),
    },

    images: {
        top: hp('3%'),
    },
});

export default TeamMembers;
