import React, {useRef,useEffect} from 'react';
import {
  View,
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Dimensions,
  Image,
  Text,
  TextInput,
  PermissionsAndroid,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
} from 'react-native'; import { Searchbar, Button } from 'react-native-paper';
import RNFetchBlob from 'rn-fetch-blob';
import notifyMessage from '../ToastMessages';


const checkPermission = async (Image_URI) => {

    const downloadImage = (URI) => {
        // Main function to download the image
    
        // To add the time suffix in filename
        let date = new Date();
        // Image URL which we want to download
        let image_URL = URI;
        // Getting the extention of the file
        let ext = getExtention(image_URL);
        ext = '.' + ext[0];
        // Get config and fs from RNFetchBlob
        // config: To pass the downloading related options
        // fs: Directory path where we want our image to download
        const { config, fs } = RNFetchBlob;
        let PictureDir = fs.dirs.PictureDir;
        let options = {
            fileCache: true,
            addAndroidDownloads: {
                // Related to the Android only
                useDownloadManager: true,
                notification: true,
                path:
                    PictureDir +
                    'file_' +
                    Math.floor(date.getTime() + date.getSeconds() / 2) +
                    ext,
                description: 'Image',
            },
        };
        config(options)
            .fetch('GET', image_URL)
            .then(res => {
                // Showing alert after successful downloading
                notifyMessage('File Downloaded Successfully.')
            });
    };
    
    const getExtention = filename => {
        // To get the file extension
        return /[.]/.exec(filename) ?
            /[^.]+$/.exec(filename) : undefined;
    };

    // Function to check the platform
    // If iOS then start downloading
    // If Android then ask for permission

    if (Platform.OS === 'ios') {
        downloadImage();
    } else {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                {
                    title: 'Storage Permission Required',
                    message:
                        'App needs access to your storage to download Photos',
                }
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                // Once user grant the permission start downloading
                //'Storage Permission Granted.');
                downloadImage(Image_URI);
            } else {
                // If permission denied then show alert
                alert('Storage Permission Not Granted');
            }
        } catch (err) {
            // To handle permission related exception
            console.warn(err);
        }
    }
};

export default checkPermission;


