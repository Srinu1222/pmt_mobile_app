import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    Animated,
    Button,
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image';
import { useDispatch, useSelector } from 'react-redux';
import * as actionCreator from '../../actions/team-action/index';
import { AuthContext } from '../../context';
import { SearchBar } from 'react-native-elements';
import { FlatList } from 'react-native-gesture-handler';
import Pagination from '../Pagination';
import {
    useCollapsibleSubHeader,
    CollapsibleSubHeaderAnimator,
} from 'react-navigation-collapsible';
import { FAB, Icon, BottomSheet, ButtonGroup } from 'react-native-elements';
import { Divider, Card, Avatar } from 'react-native-paper';
import EmptyTaskList from '../EmptyScreens/EmptyTaskList';
import { BackHandler } from "react-native";
// import { NavigationActions } from 'react-navigation';


const GetTaskList = props => {

    const {
        onScroll /* Event handler */,
        containerPaddingTop /* number */,
        scrollIndicatorInsetTop /* number */,
        translateY,
    } = useCollapsibleSubHeader();

    const data = props.data
    const title = "TaskList (" + data.length.toString() + ')'
    const Header = () => (
        <Card.Title
            style={{ backgroundColor: '#3d87f1' }}
            title={<Text style={{color: '#fff'}}>{title}</Text>}
            left={() => <Icon color={'white'} onPress={() => props.navigation.goBack()} type='feather' name='chevron-left' />
            }
        />
    );

    // Viewed Projects
    const [ClickedTasksList, setClickedTasksList] = React.useState([]);
    const onPressTasksList = (index) => {
        setClickedTasksList([...ClickedTasksList, index])
    }

    const [selectedTaskIndex, setselectedTaskIndex] = useState('');
    const [isExpanded, setisExpanded] = useState(false);

    const TaskItem = (params) => {
        const { item, index } = params
        return (
            <>
                <TouchableOpacity
                    onPress={() => {
                        setselectedTaskIndex(index)
                        setisExpanded(!isExpanded)
                        onPressTasksList(index)
                    }}
                >
                    <View style={{ elevation: 10 }}>
                        <View style={{ paddingHorizontal: '7%', flexDirection: 'row', width: wp('100%'), height: hp('10%'), justifyContent: 'space-between', alignItems: 'center' }}>
                            <View>
                                <Text style={styles.date}>{item.date}</Text>
                                <Text style={styles.title}>{item.title}</Text>
                            </View>
                            <View style={{ backgroundColor: '#3d87f1', borderRadius: 2 }}>
                                <Icon
                                    color={'white'}
                                    type='feather'
                                    name={(selectedTaskIndex == index && isExpanded) ? 'chevron-up' : 'chevron-down'}
                                    size={30}
                                />
                            </View>
                        </View>
                        {
                            (selectedTaskIndex != index || isExpanded == false) &&
                            <Divider style={{ backgroundColor: 'rgba(179, 179, 179, 0.5)' }} />
                        }
                    </View>
                </TouchableOpacity>
                {
                    selectedTaskIndex == index && isExpanded &&
                    item.tasks.map(
                        (i, index) => {
                            let temp = {
                                is_sub_contractor: '',
                                project_id: item.id,
                                event_id: i.event_id,
                                event_name: i.event_title,
                            }
                            return (
                                <TouchableOpacity
                                    onPress={() => {
                                        props.navigation.navigate('DetailedEventPortraitView', temp);
                                    }}
                                    key={index}
                                >
                                    <View style={styles.tasksStyle}>
                                        <Text style={[styles.title, { width: wp('60%'), color: 'rgb(68, 67, 67)' }]}>
                                            {i.stage} {'->'} {i.event_title} {'->'} {i.title}
                                        </Text>
                                        <View style={styles.customBtnBG}>
                                            <Text style={styles.customBtnText}>View</Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            );
                        }
                    )
                }
            </>

        );
    }


    return (
        <View style={styles.container}>
            <Header />
            <Animated.FlatList
                ListEmptyComponent={<EmptyTaskList />}
                keyExtractor={(item, index) => index}
                data={data}
                renderItem={({ item, index }) => <TaskItem item={item} index={index} />}
                onScroll={onScroll}
                contentContainerStyle={{
                    paddingBottom: hp('1%'),
                }}
                scrollIndicatorInsets={{ top: scrollIndicatorInsetTop }}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: 'white',
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    tasksStyle: {
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        paddingVertical: hp('2%'),
        paddingHorizontal: wp('7%'),
    },

    itemStyle: {
        display: 'flex',
        padding: wp('6%'),
        borderBottomWidth: 1,
        borderBottomColor: 'rgb(232, 232, 232)'
    },

    title: {
        fontSize: hp('2.2%'),
        paddingVertical: hp('0.5%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(143, 143, 143)',
        textTransform: 'capitalize'
    },



    customBtnBG: {
        borderWidth: 1,
        width: wp('17.8%'),
        height: hp('4%'),
        justifyContent: 'center',
        borderRadius: 4,
        borderColor: 'rgb(68, 67,67)',
    },

    customBtnText: {
        alignSelf: 'center',
    },

    date: {
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        textTransform: 'capitalize'
    },
});

export default GetTaskList;


{/* <View>
                    <Text style={styles.date}>{item.date}</Text>
                    <View style={[styles.itemStyle, { backgroundColor: ClickedTasksList.includes(index) ? 'white' : 'rgb(236, 243, 254)' }]}>
                        <Text style={[styles.title, { color: '#3d87f1', fontSize: hp('2.5%') }]}>{'\u2022'}{'  '}{item.title}{'  '}</Text>
                        {
                            item.tasks.map(
                                (i, index) => {
                                    return (
                                        <View key={index} style={styles.tasksStyle}>
                                            <Text style={[styles.title, { color: 'rgb(68, 67, 67)' }]}>{i.title}</Text>
                                            <TouchableOpacity
                                                style={styles.customBtnBG}
                                                onPress={() => { }}
                                            >
                                                <Text style={styles.customBtnText}>View</Text>
                                            </TouchableOpacity>
                                        </View>
                                    );
                                }
                            )
                        }
                    </View>
                </View> */}