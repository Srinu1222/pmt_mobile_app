import { View, Text } from 'react-native'
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { parseSync } from '@babel/core';
import { heightPercentageToDP } from 'react-native-responsive-screen';
import EmptyLoader from '../EmptyLoader';
import { useIsFocused } from "@react-navigation/native";
import * as actionCreators from '../../actions/task-list-action/index';
import notifyMessage from '../../ToastMessages';
import GetTasksList from './GetTasksList'


const TasksList = (props) => {

    const { route, navigation } = props;

    const dispatch = useDispatch();

    const token = route.params.token

    useEffect(() => {
        dispatch(actionCreators.getTaskList(token));
    }, []);

    const taskListReducer = useSelector(state => {
        return state.taskListReducers.taskList.get
    })

    const renderTasksList = () => {
        if (taskListReducer.success.ok) {
            return <GetTasksList navigation={navigation} data={taskListReducer.success.data.project_list} />;
        } else if (taskListReducer.failure.error) {
            return notifyMessage(taskListReducer.failure.message);
        } else {
            return (< EmptyLoader />);
        }
    };
    return renderTasksList();
};

export default TasksList;
