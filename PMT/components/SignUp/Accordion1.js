import React, { useState } from "react";
import {
  View,
  Text,
  LayoutAnimation,
  StyleSheet,
  UIManager,
  Platform
} from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import Icon from 'react-native-vector-icons/Ionicons';
import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {Button, Menu, Divider, Provider } from 'react-native-paper';

if (
  Platform.OS === "android" &&
  UIManager.setLayoutAnimationEnabledExperimental
) {
  UIManager.setLayoutAnimationEnabledExperimental(true);
}

const Accordion = ({ item, children , navigation }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [visible, setVisible] = React.useState(false);

  const openMenu = () => setVisible(true);

  const closeMenu = () => setVisible(false);
  const toggleOpen = () => {
    setIsOpen(value => !value);
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
  }

  return (
    <View style={styles.item_card}>
      <TouchableOpacity onPress={toggleOpen} style={styles.heading} activeOpacity={0.6}>
        <View>

        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <View>
            <Text style={styles.name_text}>{item.name}</Text>
            <Text style={styles.department_text}>{item.department}</Text>
          </View>
          <View>

          <Button style={{ width: wp('12%'), height: hp('4%') }} icon={require('../../assets/icons/More.png')} />

            
          </View>

        </View>
        <Text style={{ height: hp('0.2%'), marginTop: 8, marginBottom: 8, backgroundColor: 'rgb(232, 232, 232)' }}></Text>
        <View style={{ flexDirection: 'row' }}>
          <Text style={styles.textSign}>MORE DETAILS</Text>
          <Icon style={{marginLeft:10}} name={isOpen ? "chevron-up-outline" : "chevron-down-outline"} size={18} color='#3A80E3' />
        </View>

      </TouchableOpacity>
      <View style={[styles.list, !isOpen ? styles.hidden : undefined]}>
        <View style={{ display: 'flex', marginBottom: 10, flexDirection: 'row', justifyContent: 'space-between' }}>
          <View>
            <Text style={styles.email_text}>Designation</Text>
            <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>{item.designation}</Text>
          </View>
          <View>
            <Text style={styles.email_text}>Phone</Text>
            <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>{item.phone_number}</Text>
          </View>
        </View>
        <View>
        <Text style={styles.email_text}>Email</Text>
            <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>{item.email}</Text>
        </View>
      </View>
    </View>
  );
};

const Accordion1 = ({navigation,teamMembers}) => {
  
  return (
    <SafeAreaProvider>
    <SafeAreaView style={styles.safeArea}>
      <View style={styles.container}>
      {teamMembers.map((item)=>
      
      <Accordion item={item} navigation={navigation} />
        
      
      
      )
      }
        
       
        
      </View>
    </SafeAreaView>
    </SafeAreaProvider>
  );
};

export default Accordion1;

const styles = StyleSheet.create({
    item_card: {
        shadowColor: 'rgba(0,0,0,0.2)',
        shadowOffset: {
            width: 0,
            height: 4,
        },
        // height: hp('30%'),
        width: wp('80%'),
        marginRight: 10,
        shadowOpacity: 0.2,
        shadowRadius: 4.46,
        elevation: 2,
        borderWidth: 0,
        borderRadius: 4,
        borderLeftColor: 'rgb(61, 135, 241)',
        borderLeftWidth: wp('1.3%'),
        marginTop: 16.5,
        borderTopLeftRadius: 4,
        borderBottomLeftRadius: 4,
        padding: '5%',
        paddingRight: '4%'
    },
    name_text: {
        fontSize: 16,
        fontFamily: 'SourceSansPro-SemiBold',
        color: 'rgb(68, 67, 67)',
    },

    email_text: {
        fontSize: 12,
        fontFamily: 'Source Sans Pro-Regular',
        color: 'rgb(68, 67, 67)',
    },

    department_text: {
        fontSize: 12,
        fontFamily: 'SourceSansPro-SemiBold',
        color: 'rgb(194, 194, 194)',
    },
  container: {
    paddingHorizontal: 15,
    paddingVertical: 30,
  },
  text: {
    fontSize: 18,
    marginBottom: 20,
  },
  safeArea: {
    flex: 1,
  },
  heading: {
    // alignItems: "center",
    // flexDirection: "row",
    // justifyContent: "space-between",
    paddingVertical: 10
  },
  hidden: {
    height: 0,
  },
  list: {
    overflow: 'hidden'
  },
  sectionTitle: {
    fontSize: 16,
    height: 30,
    marginLeft: '5%',
  },
  sectionDescription: {
    fontSize: 12,
    height: 30,
    marginLeft: '5%',
  },
  divider: {
    borderBottomColor: 'grey',
    borderBottomWidth: StyleSheet.hairlineWidth,
    width: '100%',
},
textSign: {
    fontFamily:'SourceSansPro-SemiBold',
    left:1,
    right:15,
    // top: 15,
    fontSize: 14,
    lineHeight: 20,
    color: '#3A80E3',
},
});

