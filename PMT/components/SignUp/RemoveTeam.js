import {
    View,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    KeyboardAvoidingView,
    ScrollView,
    Keyboard,
    LayoutAnimation,
    UIManager,
    Platform
} from 'react-native';
import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context';
import LottieView from 'lottie-react-native';
import React, { useEffect, useState, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import Feather from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/Ionicons';
import TextBox from 'react-native-password-eye';
import * as Animatable from 'react-native-animatable'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { SearchBar } from 'react-native-elements';
import {Button, Menu, Divider, Provider } from 'react-native-paper';
import Accordion from './Accordion';
import { FlatList ,TouchableOpacity} from 'react-native-gesture-handler';
import Pagination from '../Pagination';
import ActionSheet from 'react-native-actionsheet';
import FastImage from 'react-native-fast-image';
import RNFetchBlob from 'rn-fetch-blob';
import * as actionCreators from "../../actions/team-action/index";
import notifyMessage from '../../ToastMessages'


const RemoveTeam = ({ navigation,route }) => {
    const{teamMembers} = route.params;

    const dispatch = useDispatch();
    
    const [removeteam, setremoveteam]= React.useState('');
    const [searchQuery, setSearchQuery] = React.useState('');
    const [visible, setVisible] = React.useState(false);
    const openMenu = () => setVisible(true);
  
    const closeMenu = () => setVisible(false);
        const onChangeSearch = query => setSearchQuery(query);
    const [activeButton, setactiveButton] = React.useState(1);
    let actionSheet = useRef();
    let optionArray = [
        <View style={{ flexDirection: 'row', marginLeft: -280 }}>
            <FastImage
                style={{ width: wp('9%'), height: hp('3%'),left:5 }}
                source={require('../../assets/icons/ActionsRemove.png')}
                resizeMode={FastImage.resizeMode.contain}
            />
            <Text style={{ color: '#000', fontFamily: 'SourceSansPro-SemiBold', fontSize: hp('2.5%'),left:10 }}>Remove</Text></View>,

    ]

    const projectDocsReducer = useSelector((state) => {
        return state.projectDocsReducer.projectDocs.get;
    });
    // //(project_id)
    if (
        Platform.OS === "android" &&
        UIManager.setLayoutAnimationEnabledExperimental
      ) {
        UIManager.setLayoutAnimationEnabledExperimental(true);
      }
    useEffect(() => {
        // dispatch(actionCreator.getProjectDocs(project_id));
    }, []);
    const showActionSheet = () => {
        actionSheet.current.show();
    }

    const removecheck =(item) =>{
        setremoveteam(item)
    }

    const Removeaction=()=>{

        if(removeteam!=''){
            
                dispatch(actionCreators.removeTeamMembers([removeteam.id]))
              .then((res) => {
                if (res?.status === 200) {
                  dispatch(actionCreators.getTeams());
                  notifyMessage("Member removed successfully!");
                }
              });
        }else{
            notifyMessage("Please Select Teammember!");

        }
        
    }

    return (

        <SafeAreaView>
            <KeyboardAvoidingView>
                <View style={styles.container}>
                    <View style={styles.parent2}>
                        <View style={[styles.CompanyDetails, { flexDirection: 'row', justifyContent: 'space-between', }]}>
                            <View>
                                <Text style={{ color: 'rgb(68,67,67)', fontSize: 18,right:10 }}>0 Selected</Text>

                            </View>
                            <View>
                                <TouchableOpacity
                                    style={{ width: 70, height: 40,top:5 }}
                                >
                                  <Text style={styles.textSign}>Cancel</Text>

                                </TouchableOpacity>

                            </View>


                        </View>
                        <Text style={{ height: hp('0.2%'), marginTop: 8, marginBottom: 8, backgroundColor: 'rgb(232, 232, 232)' }}></Text>

                        <View style={{ display: 'flex', marginTop: 24, flexDirection: 'row', justifyContent: 'space-between' }}>
                            <SearchBar
                                showLoading={false}
                                platform={Platform.OS}
                                containerStyle={{ borderWidth: 1, borderRadius: 4, borderColor: 'rgb(232, 232, 232)', backgroundColor: 'white', justifyContent: 'center', width: wp('73.3%'), height: hp('5.3%') }}
                                clearIcon={true}
                                onChangeText={onChangeSearch}
                                onClearText={() => setSearchQuery('')}
                                placeholder=''
                                cancelButtonTitle='Cancel'
                            />
                            <TouchableOpacity
                                onPress={() => {Removeaction()}}
                                style={{}}
                            >
                                < FastImage
                                    style={{ width: wp('12%'), height: hp('4%') }}
                                    source={require('../../assets/icons/ActionsRemove.png')}
                                    resizeMode={FastImage.resizeMode.contain}
                                />
                                <Text style={styles.option_text}>Remove</Text>
                            </TouchableOpacity>
                            <ActionSheet
                                ref={actionSheet}
                                options={optionArray}
                                destructiveButtonIndex={1}
                                onPress={(index) => { }}
                            />
                        </View>
                        <Pagination activeButton={activeButton} setactiveButton={setactiveButton} reducerLength={1} />

                        <>
                            <ScrollView>
                               
                                <Accordion teamMembers={teamMembers} removecheckfun={removecheck}/>
                               
                            </ScrollView></>
                       
                    </View>

                </View>
            </KeyboardAvoidingView>
        </SafeAreaView >
    );
};

const styles = StyleSheet.create({
    name_text: {
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-SemiBold',
        color: 'rgb(68, 67, 67)',
    },

    email_text: {
        fontSize: hp('1.8%'),
        fontFamily: 'Source Sans Pro-Regular',
        color: 'rgb(68, 67, 67)',
    },

    department_text: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-SemiBold',
        color: 'rgb(194, 194, 194)',
    },

    item_card: {
        shadowColor: 'rgba(0,0,0,0.2)',
        shadowOffset: {
            width: 0,
            height: 4,
        },
        height: hp('30%'),
        width: wp('90%'),
        marginRight: 10,
        shadowOpacity: 0.2,
        shadowRadius: 4.46,
        elevation: 2,
        borderWidth: 0,
        borderRadius: 4,
        borderLeftColor: 'rgb(61, 135, 241)',
        borderLeftWidth: wp('1.3%'),
        marginTop: 16.5,
        borderTopLeftRadius: 4,
        borderBottomLeftRadius: 4,
        padding: '5%',
        paddingRight: '4%'
    },
    item_card1: {
        shadowColor: 'rgba(0,0,0,0.2)',
        shadowOffset: {
            width: 0,
            height: 4,
        },
        height: hp('28%'),
        width: wp('55%'),
        marginRight: 10,
        shadowOpacity: 0.2,
        shadowRadius: 4.46,
        elevation: 2,
        borderWidth: 0,
        borderRadius: 4,
        borderLeftColor: 'rgb(61, 135, 241)',
        borderLeftWidth: wp('1.3%'),
        marginTop: 16.5,
        borderTopLeftRadius: 4,
        borderBottomLeftRadius: 4,
        padding: '5%',
        paddingRight: '4%',
        marginTop: -1
    },

    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },


    parent2: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        flex: 11,
        backgroundColor: 'white',
        padding: '5%'

    },
    textSign: {
        // left:10,
        right:15,
        // top: 15,
        fontSize: 16,
        lineHeight: 20,
        color: '#3A80E3',
    },
    buy_now_text: {
        fontSize: hp('2.3%'),
        fontFamily: 'Source Sans Pro-Regular',
        color: 'rgb(61, 135, 241)'
    },

    subscription_text: {
        fontSize: hp('1.8%'),
        fontFamily: 'Source Sans Pro-Regular',
        color: 'rgb(194, 194, 194)'
    },

    option_text: {
        fontSize: hp('1.2%'),
        fontFamily: 'Source Sans Pro-Regular',
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center'

    },
    CompanyDetails: {

        width: Dimensions.get('window').width,
        height: 30,
        // marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        backgroundColor: '#fff',
    },

});

export default RemoveTeam;
