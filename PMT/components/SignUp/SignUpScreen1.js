import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    Keyboard
} from 'react-native';
import LottieView from 'lottie-react-native';
import React, { useEffect, useState, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import Feather from 'react-native-vector-icons/Feather';
import TextBox from 'react-native-password-eye';
import * as Animatable from 'react-native-animatable'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';


const SignUpScreen1 = ({navigation}) => {

    const [data, setData] = React.useState({
        email: '',
        password: '',
        token: '',
        check_textInputChange: false,
        secureTextEntry: true,
        isValidUser: true,
        isValidPassword: true,
        foundUser: false,
        first_run_on_use_effect: false,
        update_radio_button_for_yes: false,
        update_radio_button_for_no: false,
        companyDetailsBackgroundColor: { backgroundColor: "rgba(120,160,241,0.7)" },
        radioButtonBackgroundColorForYes: { backgroundColor: 'rgb(255, 255, 255)' },
        radioButtonBackgroundColorForYes: { backgroundColor: 'rgb(255, 255, 255)' },
    });

    const dispatch = useDispatch()

    const textInputChange = (val) => {
        if (val.trim().length >= 4) {
            setData({
                ...data,
                email: val,
                check_textInputChange: true,
                isValidUser: true
            });
        } else {
            setData({
                ...data,
                email: val,
                check_textInputChange: false,
                isValidUser: false
            });
        }
    }

    const handlePasswordChange = (val) => {

        if (val.trim().length >= 8) {
            setData({
                ...data,
                password: val,
                isValidPassword: true
            });
        } else {
            setData({
                ...data,
                password: val,
                isValidPassword: false
            });
        }
    }

    const updateSecureTextEntry = () => {
        setData({
            ...data,
            secureTextEntry: !data.secureTextEntry
        });
    }

    const updateRadioButtonForYes = () => {
        setData({
            ...data,
            update_radio_button_for_yes: !data.update_radio_button_for_yes,
            radioButtonBackgroundColorForYes: { backgroundColor: '#3A80E3' },
            radioButtonBackgroundColorForNo: { backgroundColor: 'rgb(255, 255, 255)' },
            companyDetailsBackgroundColor: { backgroundColor: 'rgb(255, 255, 255)' }
        });
    }

    const updateRadioButtonForNo = () => {
        setData({
            ...data,
            update_radio_button_for_no: !data.update_radio_button_for_no,
            radioButtonBackgroundColorForNo: { backgroundColor: '#3A80E3' },
            radioButtonBackgroundColorForYes: { backgroundColor: 'rgb(255, 255, 255)' },
            companyDetailsBackgroundColor: { backgroundColor: "rgba(120,160,241,0.7)" }
        });
    }

    const handleValidUser = (val) => {
        const isValid = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(val)
        if (isValid) {
            setData({
                ...data,
                isValidUser: true
            });
        } else {
            setData({
                ...data,
                isValidUser: false
            });
        }
    }

    const companyDetails = (params) => {
        navigation.navigate('Signup2');

    }

    return (

        <SafeAreaView>
            <KeyboardAvoidingView>
                <ScrollView>
                    <View style={styles.container}>
                        <View style={styles.parent1}>
                            <View style={styles.parent1FirstView}>
                                <TouchableOpacity
                                    onPress={() => { }}
                                    style={styles.leftArrow}
                                >
                                    <Image
                                        style={{ height: 24, width: 24 }}
                                        source={require('../../assets/icons/left_arrow.png')}
                                    />
                                </TouchableOpacity>
                                <Text style={{ left: 130, top: 24, color: '#7a6a4f' }}><Text style={{ fontWeight: 'bold', fontSize: 18 }}>01</Text>/03</Text>
                            </View>
                            <View style={{ top: 50, display: 'flex', flexDirection: 'row', left: 37 }}>
                            <Text style={{ width: 95, height: 4, backgroundColor: '#3A80E3' }}></Text>
                                <Text style={{ width: 185, height: 4, backgroundColor: "rgba(232,232,232, 0.7)" }}></Text>
                            </View>
                            <Text style={{ left: 37, top: 55, color: '#7a6a4f' }}>Basic Info</Text>
                            <Text style={{ left: 37, top: 70, height: 25, fontSize: 19, color: '#7a6a4f' }}>Do you use</Text>
                            <Text style={{ left: 37, top: 70, height: 25, fontSize: 19, color: '#7a6a4f' }}>SafEarth Project Marketplace?</Text>
                            <View style={{ top: 90, left: 37, display: 'flex', flexDirection: 'row' }}>
                                <View style={{ display: 'flex', flexDirection: 'row' }}>
                                    <TouchableOpacity
                                        onPress={updateRadioButtonForYes}
                                        style={[
                                            styles.radioButton,
                                            data.radioButtonBackgroundColorForYes
                                        ]}>
                                    </TouchableOpacity>

                                    <Text style={{ left: 20, width: 25, height: 20, color: '#7a6a4f' }}>Yes</Text>
                                </View>
                                <View style={{ left: 90, display: 'flex', flexDirection: 'row' }}>
                                    <TouchableOpacity onPress={updateRadioButtonForNo} style={[
                                            styles.radioButton,
                                            data.radioButtonBackgroundColorForNo
                                        ]}></TouchableOpacity>
                                    <Text style={{ left: 18, width: 25, height: 20, color: '#7a6a4f' }}>No</Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.parent2}>
                        <Text style={{ left: 30, top: 30, height: 22, fontSize: 19, color: 'rgb(255,255,255)' }}>CATEGORY 1</Text>

                            <TextInput
                                placeholder="Company Name"
                                placeholderTextColor="white"
                                style={styles.textInput}
                                autoCapitalize="none"
                                onChangeText={(val) => textInputChange(val)}
                                onEndEditing={(e) => handleValidUser(e.nativeEvent.text)}
                            />
                            
                            <TextInput
                                placeholder="Website"
                                placeholderTextColor="white"
                                style={styles.textInput}
                                autoCapitalize="none"
                                onChangeText={(val) => textInputChange(val)}
                                onEndEditing={(e) => handleValidUser(e.nativeEvent.text)}
                            />

                            <TextInput
                                placeholder="Head Office"
                                placeholderTextColor="white"
                                style={styles.textInput}
                                autoCapitalize="none"
                                onChangeText={(val) => textInputChange(val)}
                                onEndEditing={(e) => handleValidUser(e.nativeEvent.text)}
                            />


                            <TextInput
                                placeholder="Regional Office"
                                placeholderTextColor="white"
                                style={styles.textInput}
                                autoCapitalize="none"
                                onChangeText={(val) => textInputChange(val)}
                                onEndEditing={(e) => handleValidUser(e.nativeEvent.text)}
                            />
                            <TextInput
                                placeholder="Company Brief"
                                placeholderTextColor="white"
                                style={styles.textInput}
                                autoCapitalize="none"
                                onChangeText={(val) => textInputChange(val)}
                                onEndEditing={(e) => handleValidUser(e.nativeEvent.text)}
                            />
                             <Text style={{ left: 30, top: 50, height: 22, fontSize: 19, color: 'rgb(255,255,255)' }}>CATEGORY 2</Text>
                                <View style={{marginTop:10}}>
                                <TextInput
                                    placeholder="Annual Turnover"
                                    placeholderTextColor="white"
                                    style={styles.textInput}
                                    autoCapitalize="none"
                                    onChangeText={(val) => textInputChange(val)}
                                    onEndEditing={(e) => handleValidUser(e.nativeEvent.text)}
                                />

                                <TextInput
                                    placeholder="Employer Strength"
                                    placeholderTextColor="white"
                                    style={styles.textInput}
                                    autoCapitalize="none"
                                    onChangeText={(val) => textInputChange(val)}
                                    onEndEditing={(e) => handleValidUser(e.nativeEvent.text)}
                                />
                               
                                <TouchableOpacity
                                style={[styles.textInput,{borderColor:'#fff',borderWidth:1}]}

                                >
<Text style={{textAlign: 'center',top:15,fontSize:16,fontFamily:'SourceSansPro-Regula',color:'#fff'}}>Upload Brochure</Text>
                                </TouchableOpacity>
                                </View>
                              <Text style={{ left: 30, top: 50, height: 22, fontSize: 19, color: 'rgb(255,255,255)' }}>CATEGORY 3</Text>
                              <View style={{marginTop:10}}>
                            <TextInput
                                placeholder="Installed Capacity (MW)"
                                placeholderTextColor="white"
                                style={styles.textInput}
                                autoCapitalize="none"
                                onChangeText={(val) => textInputChange(val)}
                                onEndEditing={(e) => handleValidUser(e.nativeEvent.text)}
                            />

                            <TextInput
                                placeholder="Minimum Project Size (KW)"
                                placeholderTextColor="white"
                                style={styles.textInput}
                                autoCapitalize="none"
                                onChangeText={(val) => textInputChange(val)}
                                onEndEditing={(e) => handleValidUser(e.nativeEvent.text)}
                            />

                            <TextInput
                                placeholder="Largest Project (MW)"
                                placeholderTextColor="white"
                                style={styles.textInput}
                                autoCapitalize="none"
                                onChangeText={(val) => textInputChange(val)}
                                onEndEditing={(e) => handleValidUser(e.nativeEvent.text)}
                            />


                            <TextInput
                                placeholder="Business Model"
                                placeholderTextColor="white"
                                style={styles.textInput}
                                autoCapitalize="none"
                                onChangeText={(val) => textInputChange(val)}
                                onEndEditing={(e) => handleValidUser(e.nativeEvent.text)}
                            />
                            <TextInput
                                placeholder="States Active"
                                placeholderTextColor="white"
                                style={styles.textInput}
                                autoCapitalize="none"
                                onChangeText={(val) => textInputChange(val)}
                                onEndEditing={(e) => handleValidUser(e.nativeEvent.text)}
                            />
                            <TextInput
                                placeholder="Flagship Projects"
                                placeholderTextColor="white"
                                style={styles.textInput}
                                autoCapitalize="none"
                                onChangeText={(val) => textInputChange(val)}
                                onEndEditing={(e) => handleValidUser(e.nativeEvent.text)}
                            />
                            </View>

                            <TouchableOpacity
                                style={styles.CompanyDetails}
                                onPress={companyDetails}
                            >
                                <Text style={[styles.textSign, {
                                    color: '#3A80E3'
                                }]}>Next</Text>

                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView >
        </SafeAreaView >
    );
};

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        // height: Dimensions.get('window').height,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
    parent1: {
        width: Dimensions.get('window').width,
        height: hp('40%'),
        flex: 1,
        backgroundColor: "white"
    },

    parent2: {
        width: Dimensions.get('window').width,
        height: 2*Dimensions.get('window').height,
        alignSelf: 'auto',
        flex: 2,
        backgroundColor: '#3A80E3',
    },

    parent1FirstView: {
        display: 'flex',
        flexDirection: 'row',
    },

    leftArrow: {
        left: 16,
        top: 25,
    },

    radioButton: {
        top: 4,
        borderWidth: 2,
        borderColor: '#3A80E3',
        alignItems: 'center',
        justifyContent: 'center',
        width: 16,
        height: 16,
        borderRadius: 50,
    },

    textInput: {
        width: '80%',
        left: 30,
        top: 60,
        height: 56,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: 'white',
        marginBottom: 30,
        backgroundColor: "rgba(100,160,241,0.5)",
        borderRadius: 5
    },

    textSign: {
        textAlign: 'center',
        top: '30%',
        fontSize: 16,
        lineHeight: 20,
    },

    errorMsg: {
        color: '#FF0000',
        fontSize: 14,
        marginTop: 30,
        left: 30
    },

    errorMsgForPassword: {
        color: '#FF0000',
        fontSize: 14,
        marginTop: 5,
        left: 30
    },

    eyeSymbol: {
        padding: 5,
        left: 280,
        bottom: 10
    },

    CompanyDetails: {
        width: Dimensions.get('window').width,
        height: 56,
       marginTop:hp('15.5%'),
        // marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        backgroundColor:'#fff',
        borderRadius: 5
    },
});

export default SignUpScreen1;
