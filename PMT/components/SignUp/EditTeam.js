import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    Keyboard
} from 'react-native';
import LottieView from 'lottie-react-native';
import React, { useEffect, useState, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import Feather from 'react-native-vector-icons/Feather';
import TextBox from 'react-native-password-eye';
import * as Animatable from 'react-native-animatable'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image';


const EditTeam = ({ navigation }) => {

    const [data, setData] = React.useState({
        email: '',
        password: '',
        token: '',
        check_textInputChange: false,
        secureTextEntry: true,
        isValidUser: true,
        isValidPassword: true,
        foundUser: false,
        first_run_on_use_effect: false,
        update_radio_button_for_yes: false,
        update_radio_button_for_no: false,
        companyDetailsBackgroundColor: { backgroundColor: "rgba(120,160,241,0.7)" },
        radioButtonBackgroundColorForYes: { backgroundColor: 'rgb(255, 255, 255)' },
        radioButtonBackgroundColorForYes: { backgroundColor: 'rgb(255, 255, 255)' },
    });

    const dispatch = useDispatch()

    const textInputChange = (val) => {
        if (val.trim().length >= 4) {
            setData({
                ...data,
                email: val,
                check_textInputChange: true,
                isValidUser: true
            });
        } else {
            setData({
                ...data,
                email: val,
                check_textInputChange: false,
                isValidUser: false
            });
        }
    }

    const handlePasswordChange = (val) => {

        if (val.trim().length >= 8) {
            setData({
                ...data,
                password: val,
                isValidPassword: true
            });
        } else {
            setData({
                ...data,
                password: val,
                isValidPassword: false
            });
        }
    }

    const updateSecureTextEntry = () => {
        setData({
            ...data,
            secureTextEntry: !data.secureTextEntry
        });
    }

    const updateRadioButtonForYes = () => {
        setData({
            ...data,
            update_radio_button_for_yes: !data.update_radio_button_for_yes,
            radioButtonBackgroundColorForYes: { backgroundColor: '#3A80E3' },
            radioButtonBackgroundColorForNo: { backgroundColor: 'rgb(255, 255, 255)' },
            companyDetailsBackgroundColor: { backgroundColor: 'rgb(255, 255, 255)' }
        });
    }

    const updateRadioButtonForNo = () => {
        setData({
            ...data,
            update_radio_button_for_no: !data.update_radio_button_for_no,
            radioButtonBackgroundColorForNo: { backgroundColor: '#3A80E3' },
            radioButtonBackgroundColorForYes: { backgroundColor: 'rgb(255, 255, 255)' },
            companyDetailsBackgroundColor: { backgroundColor: "rgba(120,160,241,0.7)" }
        });
    }

    const handleValidUser = (val) => {
        const isValid = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(val)
        if (isValid) {
            setData({
                ...data,
                isValidUser: true
            });
        } else {
            setData({
                ...data,
                isValidUser: false
            });
        }
    }

    const companyDetails = (params) => {
        navigation.navigate('Signup4');

    }

    return (

        <SafeAreaView>
            <KeyboardAvoidingView>
                <ScrollView>
                    <View style={styles.container}>
                        
                        <View style={styles.parent2}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between',top:20 }}>
                            <View>
                                <Text style={{ color: 'rgb(68,67,67)', fontSize: 18, left: 10 }}>Edit</Text>

                            </View>
                            <View>
                                <TouchableOpacity
                                    style={{ width: 70, height: 40, top: 5 }}
                                >
                                    <FastImage
                                        style={{ width: wp('9%'), height: hp('3%') }}
                                        source={require('../../assets/icons/crossbar.png')}
                                        resizeMode={FastImage.resizeMode.contain}
                                    />
                                </TouchableOpacity>

                            </View>


                        </View>
                        <Text style={{ height: hp('0.2%'), width:wp('80%'), marginTop: 25,left:30, marginBottom: 55, backgroundColor: 'rgb(232, 232, 232)' }}></Text>

                            <TextInput
                                placeholder="Name"
                                placeholderTextColor="rgb(179,179,179)"
                                style={styles.textInput}
                                autoCapitalize="none"
                                onChangeText={(val) => textInputChange(val)}
                                onEndEditing={(e) => handleValidUser(e.nativeEvent.text)}
                            />

                            <TextInput
                                placeholder="Department"
                                placeholderTextColor="rgb(179,179,179)"
                                style={styles.textInput}
                                autoCapitalize="none"
                                onChangeText={(val) => textInputChange(val)}
                                onEndEditing={(e) => handleValidUser(e.nativeEvent.text)}
                            />

                            <TextInput
                                placeholder="Email ID"
                                placeholderTextColor="rgb(179,179,179)"
                                style={styles.textInput}
                                autoCapitalize="none"
                                onChangeText={(val) => textInputChange(val)}
                                onEndEditing={(e) => handleValidUser(e.nativeEvent.text)}
                            />


                            <TextInput
                                placeholder="Phone"
                                placeholderTextColor="rgb(179,179,179)"
                                style={styles.textInput}
                                autoCapitalize="none"
                                onChangeText={(val) => textInputChange(val)}
                                onEndEditing={(e) => handleValidUser(e.nativeEvent.text)}
                            />
                            <TextInput
                                placeholder="Designation"
                                placeholderTextColor="rgb(179,179,179)"
                                style={styles.textInput}
                                autoCapitalize="none"
                                onChangeText={(val) => textInputChange(val)}
                                onEndEditing={(e) => handleValidUser(e.nativeEvent.text)}
                            />
                            <TextInput
                                placeholder="Designation"
                                placeholderTextColor="rgb(179,179,179)"
                                style={styles.textInput}
                                autoCapitalize="none"
                                onChangeText={(val) => textInputChange(val)}
                                onEndEditing={(e) => handleValidUser(e.nativeEvent.text)}
                            />


                            <TouchableOpacity
                                style={styles.textInput1}
                            >
                                <Text style={{ textAlign: 'center', top: 15, fontSize: 16, fontFamily: 'SourceSansPro-Regula', color: '#fff' }}>Save</Text>

                            </TouchableOpacity>




                        </View>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView >
        </SafeAreaView >
    );
};

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        // height: Dimensions.get('window').height,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
    parent1: {
        width: Dimensions.get('window').width,
        height: hp('25%'),
        flex: 1,
        backgroundColor: "white"
    },

    parent2: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height*1.1,
        alignSelf: 'auto',
        flex: 2,
        backgroundColor: '#fff',
    },

    parent1FirstView: {
        display: 'flex',
        flexDirection: 'row',
    },

    leftArrow: {
        left: 16,
        top: 25,
    },

    radioButton: {
        top: 4,
        borderWidth: 2,
        borderColor: '#3A80E3',
        alignItems: 'center',
        justifyContent: 'center',
        width: 16,
        height: 16,
        borderRadius: 50,
    },

    textInput: {
        width: '80%',
        left: 30,
        top: 60,
        height: 56,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: 'white',
        marginBottom: 30,
        backgroundColor: "rgba(179,179,179,0.1)",
        borderRadius: 5
    },
    textInput1: {
        width: '80%',
        left: 30,
        top: 60,
        height: 56,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: 'white',
        marginBottom: 30,
        backgroundColor: "'rgb(61,135,241)'",
        borderRadius: 5
    },

    textSign: {
        left: 10,
        right: 15,
        top: 15,
        fontSize: 16,
        lineHeight: 20,
        color: '#3A80E3',
    },

    errorMsg: {
        color: '#FF0000',
        fontSize: 14,
        marginTop: 30,
        left: 30
    },

    errorMsgForPassword: {
        color: '#FF0000',
        fontSize: 14,
        marginTop: 5,
        left: 30
    },

    eyeSymbol: {
        padding: 5,
        left: 280,
        bottom: 10
    },

    CompanyDetails: {

        width: Dimensions.get('window').width,
        height: 56,
        marginTop: hp('21%'),
        // marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        backgroundColor: '#fff',
        borderRadius: 5
    },
});

export default EditTeam;
