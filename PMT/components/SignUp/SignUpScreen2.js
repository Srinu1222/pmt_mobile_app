import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    Keyboard
} from 'react-native';
import LottieView from 'lottie-react-native';
import FastImage from 'react-native-fast-image';
import React, { useEffect, useState, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import Feather from 'react-native-vector-icons/Feather';
import TextBox from 'react-native-password-eye';
import * as Animatable from 'react-native-animatable'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import * as actionCreators from "../../actions/auth-action/index";
import notifyMessage from '../../ToastMessages';
import { RadioButton } from 'react-native-paper';


const SignUpScreen2 = ({ navigation,route }) => {
    const { companyName,website,headOffice,regionalOffice,aboutCompany,annualTurnover,empStrength,fileList,installedCapacity,minProjectSize,largestProject,businessModel,activeStates,flagshipProjects,checked,email1 } = route.params;

    const dispatch = useDispatch();


    const [contactPersonName, setContactPersonName] = useState("");
    const [password1, setPassword1] = useState("");
    const [department, setDepartment] = useState("");
    const [email, setEmail] = useState(email1);
    const [phoneNo, setPhoneNo] = useState("");
    const [password, setPassword] = useState("");
    const [otp, setOtp] = useState("");
    const [getotp, settGetotp] = useState("No");
    const [numProjectsPerYear, setnumProjectsPerYear]= useState(27);


    const handleEmailConfirmation = () => {
        dispatch(actionCreators.getEmailConfirmation(email));
        settGetotp("Yes");
      };

      const handleVerifyOtp = (value) => {
        let form = new FormData();
    
        form.append("active_states", activeStates);
        form.append("password1", password);
        form.append("email", email);
        form.append("otp", otp);
        form.append("company", companyName);
        form.append("website", website);
        form.append("head_office", headOffice);
        form.append("regional_office", regionalOffice);
        form.append("description", aboutCompany);
        form.append("turnover", annualTurnover);
        form.append("employee_strength", empStrength);
        form.append("maximum_size_project", largestProject);
        form.append("capacity_installed", installedCapacity);
        form.append("business_model", businessModel);
        form.append("flagship_projects", flagshipProjects);
        form.append("minimum_size_project", minProjectSize);
        form.append("phone_number", phoneNo);
        form.append("contact_person", contactPersonName);
        form.append("num_projects_per_year", numProjectsPerYear)
        fileList.forEach((file) => form.append("brochure", file));
        form.append("department", department);
    
        Promise.resolve(
          dispatch(actionCreators.verifyOtp(form, email, password))
        ).then(async (res) => {
            //('res',res)
          if (res?.status === 200) {
            // let form = new FormData();
            // form.append("username", email);
            // form.append("password", password);
            // await dispatch(actionCreators.loginUser(form));
            await notifyMessage("Verification successful!");
            navigation.navigate('Signup3');
          }
        });
    
      };



    return (

        <SafeAreaView>
            <KeyboardAvoidingView>
                <ScrollView>
                    <View style={styles.container}>
                        <View style={styles.parent1}>
                            <View style={styles.parent1FirstView}>
                                <TouchableOpacity
                                    onPress={() => { }}
                                    style={styles.leftArrow}
                                >
                                    <FastImage
                                        style={{ height: 24, width: 24 }}
                                        source={require('../../assets/icons/left_arrow.png')}
                                    />
                                </TouchableOpacity>
                                <Text style={{ left: 130, top: 24, color: '#7a6a4f' }}><Text style={{ fontWeight: 'bold', fontSize: 18 }}>02</Text>/03</Text>
                            </View>
                            <View style={{ top: 50, display: 'flex', flexDirection: 'row', left: 37 }}>
                                <Text style={{ width: wp('50%'), height: 4, backgroundColor: '#3A80E3' }}></Text>
                                <Text style={{ width: wp('25%'), height: 4, backgroundColor: "rgba(232,232,232, 0.7)" }}></Text>
                            </View>
                            <Text style={{ top: 55, color: '#7a6a4f', textAlign: 'center' }}>Master id</Text>
                            <Text style={{ left: 37, top: 70, height: 25, fontSize: 19, color: '#7a6a4f' }}>Do you use</Text>
                            <Text style={{ left: 37, top: 70, height: 25, fontSize: 19, color: '#7a6a4f' }}>SafEarth Project Marketplace?</Text>
                            <View style={{ top: 90, left: 37, display: 'flex', flexDirection: 'row' }}>
                                <View style={{ display: 'flex', flexDirection: 'row' }}>
                                <RadioButton
                                        value="Yes"
                                        color="#3A80E3"
                                        status={checked === 'Yes' ? 'checked' : 'unchecked'}
                                    />

                                    <Text style={{ left: 20, width: 30, height: 20, top: 8, color: '#7a6a4f' }}>Yes</Text>
                                </View>
                                <View style={{ left: 90, display: 'flex', flexDirection: 'row' }}>
                                <RadioButton
                                        value="No"
                                        color="#3A80E3"
                                        status={checked === 'No' ? 'checked' : 'unchecked'}
                                    />
                                    <Text style={{ left: 18, width: 25, height: 20, top: 8, color: '#7a6a4f' }}>No</Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.parent2}>

                            <TextInput
                                placeholder="Name"
                                placeholderTextColor="white"
                                style={styles.textInput}
                                autoCapitalize="none"
                                value={contactPersonName}
                                onChangeText={(val) => setContactPersonName(val)}
                            />

                            <TextInput
                                placeholder="Department"
                                placeholderTextColor="white"
                                style={styles.textInput}
                                autoCapitalize="none"
                                value={department}
                                onChangeText={(val) => setDepartment(val)}
                            />

                            <TextInput
                                placeholder="Email ID"
                                placeholderTextColor="white"
                                style={styles.textInput}
                                autoCapitalize="none"
                                value={email}
                                onChangeText={(val) => setEmail(val)}
                            />


                            <TextInput
                                placeholder="Phone"
                                placeholderTextColor="white"
                                style={styles.textInput}
                                autoCapitalize="none"
                                value={phoneNo}
                                onChangeText={(val) => setPhoneNo(val)}
                            />
                            <TextInput
                                placeholder="Password"
                                placeholderTextColor="white"
                                style={styles.textInput}
                                autoCapitalize="none"
                                value={password}
                                onChangeText={(val) => setPassword(val)}
                            />
                            <TextInput
                                placeholder="Comfirm Password"
                                placeholderTextColor="white"
                                style={styles.textInput}
                                autoCapitalize="none"
                                value={password1}
                                onChangeText={(val) => setPassword1(val)}
                            />


                            <View style={{ flexDirection: 'row' }}>
                                <TextInput
                                    placeholder="Enter OTP"
                                    placeholderTextColor="white"
                                    style={styles.textInput1}
                                    autoCapitalize="none"
                                    onChangeText={(val) => setOtp(val)}
                                />
                                <TouchableOpacity
                                    style={[styles.textInput1, { borderColor: '#fff', borderWidth: 1, marginLeft: 13 }]}
                                    onPress={() => handleEmailConfirmation()}

                                >
                                    <View style={{ textAlign: 'center', flexDirection: 'row' }}>
                                        <Text style={{ top: 15, fontSize: 16, fontFamily: 'SourceSansPro-Regula', color: '#fff', marginLeft: 15 }}>Get OTP</Text>
                                        <FastImage
                                            style={{ top: 15, height: 24, width: 24 }}
                                            source={require('../../assets/icons/Arrows.png')}
                                        />
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <Text style={{ textAlign: 'center', top: 50, fontSize: 16, fontFamily: 'SourceSansPro-Regula', color: '#fff' }}>OTP sent to email!</Text>

                            <TouchableOpacity
                                style={[styles.CompanyDetails,{backgroundColor:getotp=='No'?'rgba(100,160,241,0.5)' :'rgb(255,255,255)'}]}
                                onPress={() => handleVerifyOtp()}
                            >
                                <Text style={[styles.textSign, {
                                    color: '#3A80E3'
                                }]}>Verify OTP</Text>


                            </TouchableOpacity>

                        </View>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView >
        </SafeAreaView >
    );
};

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        // height: Dimensions.get('window').height,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
    parent1: {
        width: Dimensions.get('window').width,
        height: hp('40%'),
        flex: 1,
        backgroundColor: "white"
    },

    parent2: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        alignSelf: 'auto',
        flex: 2,
        backgroundColor: '#3A80E3',
    },

    parent1FirstView: {
        display: 'flex',
        flexDirection: 'row',
    },

    leftArrow: {
        left: 16,
        top: 25,
    },

    radioButton: {
        top: 4,
        borderWidth: 2,
        borderColor: '#3A80E3',
        alignItems: 'center',
        justifyContent: 'center',
        width: 16,
        height: 16,
        borderRadius: 50,
    },

    textInput: {
        width: '80%',
        left: 30,
        top: 60,
        height: 56,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: 'white',
        marginBottom: 30,
        backgroundColor: "rgba(100,160,241,0.5)",
        borderRadius: 5
    },
    textInput1: {
        width: '38%',
        left: 30,
        top: 60,
        height: 56,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: 'white',
        marginBottom: 30,
        backgroundColor: "rgba(100,160,241,0.5)",
        borderRadius: 5
    },
    textSign: {
        textAlign: 'center',
        top: '30%',
        fontSize: 16,
        lineHeight: 20,
    },

    errorMsg: {
        color: '#FF0000',
        fontSize: 14,
        marginTop: 30,
        left: 30
    },

    errorMsgForPassword: {
        color: '#FF0000',
        fontSize: 14,
        marginTop: 5,
        left: 30
    },

    eyeSymbol: {
        padding: 5,
        left: 280,
        bottom: 10
    },

    CompanyDetails: {
        width: Dimensions.get('window').width,
        height: 56,
        marginTop: hp('13%'),
        // marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        
        borderRadius: 5
    },
});

export default SignUpScreen2;
