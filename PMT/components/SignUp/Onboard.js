import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    Keyboard
} from 'react-native';
import LottieView from 'lottie-react-native';
import React, { useEffect, useState, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import Feather from 'react-native-vector-icons/Feather';
import TextBox from 'react-native-password-eye';
import * as Animatable from 'react-native-animatable'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { RadioButton } from 'react-native-paper';
import notifyMessage from '../../ToastMessages';


const Onboard = ({ navigation }) => {
    const dispatch = useDispatch()



    const companyDetails = (params) => {
        navigation.navigate('Login');
    }
    const companyDetails1 = (params) => {

        navigation.navigate('Signup2');


    }


    return (

        <SafeAreaView>

            <View style={styles.parent2}>
                <View style={{marginTop:hp('40%') }}>
             
                        <Text style={{ fontFamily: 'SourceSansPro-Regular', fontSize: 24, color: '#fff' }}>Welcome on board!</Text>

              
                   

                </View>
              
                <View style={{marginTop:hp('40%')}}>
                        <TouchableOpacity
                            style={styles.CompanyDetails}
                            onPress={companyDetails}
                        >
                            <Text style={styles.textSign}>Let's get started</Text>

                        </TouchableOpacity>
                    </View>
            
                    </View>
        </SafeAreaView >
    );
};

const styles = StyleSheet.create({



    parent2: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        alignItems:'center',
        
        backgroundColor: '#3A80E3',
    },

    leftArrow: {
        left: 16,
        top: 25,
    },

    radioButton: {
        top: 4,
        borderWidth: 2,
        borderColor: '#3A80E3',
        alignItems: 'center',
        justifyContent: 'center',
        width: 16,
        height: 16,
        borderRadius: 50,
    },

    textInput: {
        width: '80%',
        left: 30,
        top: 60,
        height: 56,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: 'white',
        marginBottom: 30,
        backgroundColor: "rgba(100,160,241,0.5)",
        borderRadius: 5
    },

    textSign: {
        textAlign: 'center',
        top: '30%',
        fontSize: 16,
        lineHeight: 20,
        fontFamily:'SourceSansPro-Regular',
        color:'rgb(61,135,241)'
    },

    errorMsg: {
        color: '#FF0000',
        fontSize: 14,
        marginTop: 30,
        left: 30
    },

    errorMsgForPassword: {
        color: '#FF0000',
        fontSize: 14,
        marginTop: 5,
        left: 30
    },

    eyeSymbol: {
        padding: 5,
        left: 280,
        bottom: 10
    },

    CompanyDetails: {
      
        width: 286,
        height: 56,
        color: '#000',
        backgroundColor: '#fff',
        borderRadius: 5
    },
    CompanyDetails1: {
        width: Dimensions.get('window').width,
        height: 56,
        marginTop: hp('15.5%'),
        // marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        backgroundColor: '#fff',
        borderRadius: 5
    },
});

export default Onboard;
