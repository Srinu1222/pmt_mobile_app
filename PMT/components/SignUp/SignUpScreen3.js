import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    Keyboard
} from 'react-native';
import LottieView from 'lottie-react-native';
import React, { useEffect, useState, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import Feather from 'react-native-vector-icons/Feather';
import TextBox from 'react-native-password-eye';
import * as Animatable from 'react-native-animatable'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image';
import * as actionCreators from "../../actions/team-action/index";
import notifyMessage from '../../ToastMessages'

const SignUpScreen3 = ({ navigation }) => {

    const [name, setName] = useState("");
    const [department, setDepartment] = useState("");
    const [email, setEmail] = useState("");
    const [phone, setPhone] = useState("");
    const [designation, setDesignation] = useState("");
    const [uploadedFile, setUploadedFile] = useState("");
    const [tableData, setTableData] = useState([]);
    const [isUpload, setIsUpload] = useState(false);
    const [isConfirm, setIsConfirm] = useState(false);

    const dispatch = useDispatch();


    const onFinishFailed = () => { };

    const handleAdd = () => {
        const teamMember = {
            name: name,
            number: phone,
            email: email,
            designation: designation,
            location: "",
            department: department,
        };
        //("teamMember", teamMember)
        Promise.resolve(dispatch(actionCreators.addTeam(teamMember))).then(
            (res) => {
                if (res?.status === 200) {
                    dispatch(actionCreators.getTeams());
                    notifyMessage("Team member added successfully!");
                }
            }
        );

        setName("");
        setDepartment("");
        setEmail("");
        setPhone("");
        setDesignation("");
        setUploadedFile("");
    };

    const teamReducer = useSelector((state) => {
        return state.teamReducers.teams.get;
    });

    //("teamReducer", teamReducer?.success?.data?.team_members_list)
    useEffect(() => {
        dispatch(actionCreators.getTeams());
    }, []);



    let index = true;
    const onTeamUploadHandler = (info) => {
        if (info.fileList[0] && index) {
            if (info.fileList[0].name.split(".")[1] !== "xlsx") {
                index = false;
                return message.info("Please upload an excel file!");
            }
            if (info.fileList[0] && index) {
                setUploadedFile(info.fileList[0].originFileObj);
                const formData = new FormData();
                formData.append("add_members", info.fileList[0].originFileObj);
                Promise.resolve(dispatch(actionCreators.addGroupTeam(formData))).then(
                    (res) => {
                        if (res?.status === 200) {
                            dispatch(actionCreators.getTeams());
                            message.success("Team added successfully!");
                            setIsUpload(false);
                        }
                    }
                );
                index = false;
            }
        }
    };

    const companyDetails = (params) => {
        navigation.navigate('Signup4', { teamMembers: teamReducer?.success?.data?.team_members_list });

    }

    const handleconfirm = () => {
        navigation.navigate('Onboard');

    }


    return (

        <SafeAreaView>
            <KeyboardAvoidingView>
                <ScrollView>
                    <View style={styles.container}>
                        <View style={styles.parent1}>
                            <View style={styles.parent1FirstView}>
                                <TouchableOpacity
                                    onPress={() => { }}
                                    style={styles.leftArrow}
                                >
                                    <Image
                                        style={{ height: 24, width: 24 }}
                                        source={require('../../assets/icons/left_arrow.png')}
                                    />
                                </TouchableOpacity>
                                <Text style={{ left: 130, top: 24, color: '#7a6a4f' }}><Text style={{ fontWeight: 'bold', fontSize: 18 }}>03</Text>/03</Text>
                            </View>
                            <View style={{ top: 50, display: 'flex', flexDirection: 'row', left: 37 }}>
                                <Text style={{ width: wp('75%'), height: 4, backgroundColor: '#3A80E3' }}></Text>
                                {/* <Text style={{ width: 286, height: 4, backgroundColor: "rgba(232,232,232, 0.7)" }}></Text> */}
                            </View>
                            <Text style={{ textAlign: 'right', right: wp('15%'), top: 55, color: '#7a6a4f', fontSize: 16, color: 'rgb(68,67,67)' }}>Team Builder</Text>

                        </View>
                        <View style={styles.parent2}>
                            <TextInput
                                placeholder="Name"
                                placeholderTextColor="white"
                                style={styles.textInput}
                                autoCapitalize="none"
                                onChangeText={(val) => setName(val)}
                            />

                            <TextInput
                                placeholder="Department"
                                placeholderTextColor="white"
                                style={styles.textInput}
                                autoCapitalize="none"
                                onChangeText={(val) => setDepartment(val)}
                            />

                            <TextInput
                                placeholder="Email ID"
                                placeholderTextColor="white"
                                style={styles.textInput}
                                autoCapitalize="none"
                                onChangeText={(val) => setEmail(val)}
                            />


                            <TextInput
                                placeholder="Phone"
                                placeholderTextColor="white"
                                style={styles.textInput}
                                autoCapitalize="none"
                                onChangeText={(val) => setPhone(val)}
                            />
                            <TextInput
                                placeholder="Designation"
                                placeholderTextColor="white"
                                style={styles.textInput}
                                autoCapitalize="none"
                                onChangeText={(val) => setDesignation(val)}
                            />

                            <View style={{ flexDirection: 'row' }}>
                                <TouchableOpacity
                                    style={[styles.textInput1, { backgroundColor: "rgb(255,255,255)" }]}
                                    onPress={() => handleAdd()}
                                >
                                    <Text style={{ textAlign: 'center', top: 15, fontSize: 16, fontFamily: 'SourceSansPro-Regula', color: '#3A80E3' }}>+Add</Text>

                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={[styles.textInput1, { borderColor: '#fff', borderWidth: 1, marginLeft: 13 }]}
                                    onPress={() => handleconfirm()}
                                >
                                    <Text style={{ textAlign: 'center', top: 15, fontSize: 16, fontFamily: 'SourceSansPro-Regula', color: '#fff' }}>Confirm</Text>

                                </TouchableOpacity>
                            </View>

                            <View style={[styles.CompanyDetails, { flexDirection: 'row', justifyContent: 'space-between', }]}>
                                <View>
                                    <Text style={{ color: 'rgb(68,67,67)', top: 15, fontSize: 16 }}>Team Members <Text style={{ color: 'rgb(143,143,143)', top: 15 }}>({teamReducer?.success?.data?.team_members_list.length})</Text></Text>

                                </View>
                                <View>
                                    <TouchableOpacity
                                        style={{ width: 70, height: 40 }}
                                        onPress={companyDetails}
                                    >
                                        <Text style={styles.textSign}>View</Text>
                                    </TouchableOpacity>

                                </View>


                            </View>


                        </View>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView >
        </SafeAreaView >
    );
};

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        // height: Dimensions.get('window').height,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
    parent1: {
        width: Dimensions.get('window').width,
        height: hp('25%'),
        flex: 1,
        backgroundColor: "white"
    },

    parent2: {
        width: Dimensions.get('window').width,
        height: 0.95*Dimensions.get('window').height,
        alignSelf: 'auto',
        flex: 2,
        backgroundColor: '#3A80E3',
    },

    parent1FirstView: {
        display: 'flex',
        flexDirection: 'row',
    },

    leftArrow: {
        left: 16,
        top: 25,
    },

    radioButton: {
        top: 4,
        borderWidth: 2,
        borderColor: '#3A80E3',
        alignItems: 'center',
        justifyContent: 'center',
        width: 16,
        height: 16,
        borderRadius: 50,
    },

    textInput: {
        width: '80%',
        left: 30,
        top: 60,
        height: 56,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: 'white',
        marginBottom: 30,
        backgroundColor: "rgba(100,160,241,0.5)",
        borderRadius: 5
    },
    textInput1: {
        width: '38%',
        left: 30,
        top: 60,
        height: 56,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: 'white',
        marginBottom: 30,
        backgroundColor: "rgba(100,160,241,0.5)",
        borderRadius: 5
    },

    textSign: {
        left: 10,
        right: 15,
        top: 15,
        fontSize: 16,
        lineHeight: 20,
        color: '#3A80E3',
    },

    errorMsg: {
        color: '#FF0000',
        fontSize: 14,
        marginTop: 30,
        left: 30
    },

    errorMsgForPassword: {
        color: '#FF0000',
        fontSize: 14,
        marginTop: 5,
        left: 30
    },

    eyeSymbol: {
        padding: 5,
        left: 280,
        bottom: 10
    },

    CompanyDetails: {

        width: Dimensions.get('window').width,
        height: 56,
        marginTop: hp('22%'),
        // marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        backgroundColor: '#fff',
        borderRadius: 5
    },
});

export default SignUpScreen3;
