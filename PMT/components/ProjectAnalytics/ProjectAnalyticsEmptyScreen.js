import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    Linking
} from 'react-native';
import React, { useState, useMemo } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { SearchBar } from 'react-native-elements';
import EmptyScreenHeader from '../EmptyScreens/EmptyScreenHeader'

const ProjectAnalyticsEmptyScreen = (props) => {

    return (
        <>
            <KeyboardAvoidingView>
                <View style={styles.container}>
                    <EmptyScreenHeader {...props} title='Project An...' />
                    <View style={styles.parent2}>
                        <View style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                            < FastImage
                                style={styles.groupimage}
                                source={require('../../assets/icons/projectanalytics.png')
                                }
                                resizeMode={FastImage.resizeMode.contain}
                            />
                            <Text style={styles.projectAnalyticsText}>Project Analytics</Text>
                        </View>

                        <Text style={styles.comingSoonText}>Coming Soon!</Text>
                    </View>
                </View>
            </KeyboardAvoidingView>
        </ >
    );
};

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        display: 'flex',
        backgroundColor: "white",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    parent2: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        flex: 11,
        backgroundColor: 'white',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around',
    },

    projectAnalyticsText: {
        width: wp('98%'),
        height: hp('4%'),
        fontSize: hp('2%'),
        textAlign: 'center',
        color: '#66a0f4'
    },

    comingSoonText: {
        width: wp('98%'),
        height: hp('8%'),
        fontSize: hp('4%'),
        textAlign: 'center',
        color: '#3d87f1',
        fontFamily: 'SourceSansPro-Semibold'
    },

    groupimage: {
        height: hp('40.4%'),
        width: wp('80.3%'),
    }
});

export default ProjectAnalyticsEmptyScreen;
