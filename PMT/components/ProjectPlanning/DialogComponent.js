import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TouchableOpacity,
    TextInput,
    KeyboardAvoidingView,
    ScrollView,
    Animated,
    Pressable
} from 'react-native';
import FastImage from 'react-native-fast-image';
import LottieView from 'lottie-react-native';
import { useDispatch, useSelector } from "react-redux";
import React, { useState, useRef, useMemo, useEffect } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { FAB, Icon, BottomSheet, ButtonGroup, Input } from 'react-native-elements';
import { Card as PaperCard, RadioButton, Button, Portal, Dialog } from 'react-native-paper';


const DialogComponent = (params) => {

    const { visible, onClickCancel, generateGantt } = params

    return (
        <Portal>
            <Dialog visible={visible} onDismiss={() => params.updateState(false)}>
                <View style={{ width: wp('91.1%'), alignSelf: 'center', borderRadius: 4, backgroundColor: 'white', height: hp('18%') }}>
                    <Text style={{ alignSelf: 'center', paddingVertical: hp('3%'), color: 'rgb(68, 67, 67)', fontSize: hp('2.3%'), fontFamily: 'SourceSansPro-Regular' }}>
                        Do you want to generate a new template?
                    </Text>
                    <View
                        style={{ flexDirection: 'row', justifyContent: 'space-around' }}
                    >
                        <Button
                            style={{ backgroundColor: '#3d87f1', width: wp('38.1%'), height: hp('5.3%'), justifyContent: 'center' }}
                            onPress={() => generateGantt()}
                            labelStyle={{ color: 'white' }}
                        >
                            yes
                </Button>
                        <Button
                            style={{ backgroundColor: 'white', borderColor: '#3d87f1', borderWidth: 1.5, width: wp('38.1%'), height: hp('5.3%'), justifyContent: 'center' }}
                            onPress={() => onClickCancel()}
                            labelStyle={{ color: '#3d87f1' }}
                        >
                            No
                </Button>
                    </View>
                </View>
            </Dialog>
        </Portal>
    );
}

export default DialogComponent;