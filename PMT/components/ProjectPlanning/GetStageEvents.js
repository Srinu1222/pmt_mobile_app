import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    Button
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { useDispatch, useSelector } from 'react-redux';
import * as actionCreator from '../../actions/team-action/index';
import { AuthContext } from '../../context';
import { SearchBar } from 'react-native-elements';
import { Divider } from 'react-native-elements';
import { FlatList } from 'react-native-gesture-handler';
import Pagination from '../Pagination'
import EventsCard from './EventCard'

const GetStageEvents = (props) => {
    const {
        loading,
        project_id,
        navigation,
        token,
        stageEvents,
        getSpoc,
        stage,
        backend_key_for_stage,
        subcontractormode
    } = props
    const stages = ['Pre-Requisites', 'Approvals', 'Engineering', 'Procurement', 'Material Handeling', 'Construction', 'Site Handover']
    const [activeButton, setactiveButton] = React.useState(1);

    const newProject = (params) => {
    };

    const nextStage = stages[stages.indexOf(props.stage) + 1]
    const onPressClosebtn = (params) => {
        props.navigation.navigate('project_planning1', { project_id: props.project_id, nextStage: nextStage, is_sub_contractor: props.subcontractormode })
    }

    const isSubContractorEvent = (event) => {
        return event.is_sub_contractor_event == true
    }

    const EventsFilterData = () => {
        if (subcontractormode) {
            return stageEvents.filter(isSubContractorEvent)
        } else if (!subcontractormode) {
            return stageEvents
        }
    }

    return (
        <SafeAreaView style={{ backgroundColor: '#fff' }}>
            <KeyboardAvoidingView keyboardVerticalOffset={0} behavior='position'>
                <View style={styles.container}>
                    <View style={styles.header}>
                        <Text style={styles.text}>{props.stage}</Text>
                        <TouchableOpacity
                            onPress={() => { onPressClosebtn() }}
                            style={{ width: wp('40%') }}
                        >
                            <Text style={styles.backtoText}>Close</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ padding: '4%' }}>
                        <Pagination activeButton={activeButton} setactiveButton={setactiveButton} reducerLength={stageEvents.length} />
                    </View>

                    <Divider style={{ backgroundColor: 'rgba(179, 179, 179, 0.3)', marginHorizontal: wp('4%'), height: hp('0.2%'), marginBottom: hp('2%') }} />

                    <View
                        style={{ marginTop: hp('3%'), height: hp('70%'), borderWidth: 0 }}
                    >
                        <FlatList
                            data={EventsFilterData()}
                            keyExtractor={(item, index) => index}
                            renderItem={
                                ({ item, index }) => (
                                    <EventsCard
                                        data={item}
                                        loading={loading}
                                        token={token}
                                        spocData={getSpoc}
                                        backend_key_for_stage={backend_key_for_stage}
                                        subcontractormode={subcontractormode}
                                        navigation={navigation}
                                    />
                                )
                            }
                        />
                    </View>

                </View>
            </KeyboardAvoidingView>
        </SafeAreaView >

    );
};

const styles = StyleSheet.create({

    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        display: 'flex',
        backgroundColor: '#fff'
        // padding: '4%',
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    buy_now_text: {
        fontSize: hp('2.3%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(61, 135, 241)'
    },

    subscription_text: {
        fontSize: hp('1.8%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(194, 194, 194)'
    },

    option_text: {
        fontSize: hp('1.2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center'

    },

    header: {
        marginTop: hp('1.5%'),
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: '4%'
    },

    text: {
        fontSize: hp('2.8%'),
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center',
        fontFamily: 'SourceSansPro-SemiBold',
    },

    backtoText: {
        alignSelf: 'flex-end',
        textDecorationLine: 'underline',
        color: 'rgb(61, 135, 241)',
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular'
    },

});

export default GetStageEvents;
