import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView
} from 'react-native';
import React, { useState, useEffect, useMemo } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { useDispatch, useSelector } from 'react-redux';
import * as ganttActionCreators from '../../actions/gantt-stage-action/index';
import * as getStageEventActionCreators from '../../actions/project-prerequisits-action/index';
import GetStageEvents from '../ProjectPlanning/GetStageEvents'
import EmptyLoader from '../../components/EmptyLoader'
import notifyMessage from '../../ToastMessages'


const ProjectStage = ({ route, navigation }) => {

    const { stage, project_id, is_sub_contractor, project_completion, token } = route.params

    // //('ps', route.params, stage, project_id, is_sub_contractor)

    const stage_values = {
        'Pre-Requisites': ['PRE-REQUISITE', 'PRE-REQUISITES'],
        'Approvals': ['APPROVALS', 'APPROVALS'],
        'Engineering': ['ENGINEERING', 'ENGINEERING'],
        'Procurement': ['PROCUREMENT', 'PROCUREMENT'],
        'Construction': ['CONSTRUCTION', 'CONSTRUCTION'],
        'Material Handeling': ['MATERIAL HANDLING', 'MATERIAL HANDLING'],
        'Site Handover': ['HANDOVER', 'SITE HAND OVER']
    }

    const backend_key_for_stage = stage_values[stage][1]

    const [loading, setloading] = useState(false);

    const dispatch = useDispatch();

    var api_send_data;

    if (is_sub_contractor) {
        api_send_data = {
            stage: stage_values[stage][0],
            project_id: project_id,
            project_planning: project_completion,
            is_sub_contractor: is_sub_contractor
        }
    } else {
        api_send_data = {
            stage: stage_values[stage][0],
            project_id: project_id,
            project_planning: project_completion,
        }
    }

    const Reload = () => {
        setloading(!loading)
    }

    useEffect(() => {
        dispatch(getStageEventActionCreators.getPreRequisits(token, project_id, stage_values[stage][1]))
        dispatch(ganttActionCreators.getSpoc(token, api_send_data))
    }, [loading]);

    const spocReducer = useSelector(state => {
        return state.spocReducers.spoc.get;
    })

    const stageEventsReducer = useSelector(state => {
        return state.preRequsiteReducers.prerequisite.get;
    })

    const renderProjectStageEvents = () => {
        if (stageEventsReducer.success.ok && spocReducer.success.ok) {
            return <GetStageEvents
                loading={Reload}
                project_id={project_id}
                navigation={navigation}
                token={token}
                stageEvents={stageEventsReducer.success.data.stage_events}
                getSpoc={spocReducer.success.data.members}
                stage={stage}
                backend_key_for_stage={backend_key_for_stage}
                subcontractormode={is_sub_contractor}
            />;
        } else if (stageEventsReducer.failure.error) {
            return notifyMessage(stageEventsReducer.failure.message);
        } else {
            return (< EmptyLoader />)
        }
    };

    return renderProjectStageEvents();
};

export default ProjectStage;
