import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    Button,
    Keyboard
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import Icons from "react-native-vector-icons/Ionicons";
import { white } from 'react-native-paper/lib/typescript/styles/colors';
import FastImageComponent from '../FastImageComponent'
import DropDownPicker from 'react-native-dropdown-picker';
import { Icon, Input } from 'react-native-elements';
import { Divider, Card } from 'react-native-paper';
import DatePicker from 'react-native-datepicker';
import * as actionCreators from '../../actions/update-event-action/index';
import { useDispatch, useSelector } from 'react-redux';
import ElevatedView from '../ElevatedView';
import MyDatePicker from './DatePicker';
import ModalDropdown from 'react-native-modal-dropdown';


const EventCard = (props) => {

    const { access_provided, days_needed, id, is_sub_contractor_event, default_spoc, name, start_time } = props.data
    const { spocData, loading, token, backend_key_for_stage, subcontractormode, navigation } = props

    const [spocList, setSpocList] = useState([])
    const [IconPosition, setIconPosition] = useState(false);

    const [daysNeeded, setDaysNeeded] = useState(days_needed.toString())
    const [spoc, setSpoc] = useState(default_spoc.id.toString())
    const [date, setDate] = useState(start_time)

    const [isConfirmbtnId, setisConfirmbtnId] = useState(0)

    useEffect(() => {
        let spocNamesList = []
        spocData.map((item, index) => spocNamesList.push(item.name))
        setSpocList(spocNamesList)
    }, [])

    const onSelectDate = (date) => {
        setDate(date)
        setisConfirmbtnId(id)
    }

    const dispatch = useDispatch()

    const onPressConfirmBtn = () => {

        const formData = new FormData();
        formData.append('days_needed', daysNeeded);
        formData.append('team_member_id', spoc);
        formData.append('start_date', date);
        formData.append('stage', backend_key_for_stage);
        formData.append('is_sub_contractor', is_sub_contractor_event)
        formData.append('sub_contractor_mode', subcontractormode)
        dispatch(actionCreators.updateEvent(token, formData, id));
        loading()
        setisConfirmbtnId(0)
    };

    const onPressRemoveBtn = () => {
        dispatch(actionCreators.deleteEvent(token, id));
        loading()
    }

    return (

        <View
            style={{
                marginHorizontal: wp('5%'),
                marginBottom: hp('2%')
            }}
        >
            <Card
                elevation={4}
                style={{
                    width: wp('90%'),
                    backgroundColor: '#fff',
                    padding: '4%',
                }}
            >
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={[styles.email_text, { fontSize: hp('2.3%') }]}>{name}</Text>
                    <Icon
                        color={'#3d87f1'}
                        // onPress={() => {
                        //     setvisibleStatusDialog(true)
                        // }}
                        type={'feather'}
                        name={'square'}
                    />
                </View>
                <Divider style={{ backgroundColor: 'rgba(179, 179, 179, 0.3)', height: hp('0.2%'), marginVertical: hp('1.5%') }} />
                <ModalDropdown
                    options={spocList}
                    textStyle={{ fontSize: hp('2%'), width: wp('65%'), color: 'rgb(68, 67, 67)' }}
                    dropdownStyle={{ width: '80%', height: hp('30%'), marginLeft: -9, elevation: 8, marginTop: -20 }}
                    dropdownTextStyle={{ fontSize: hp('2%'), color: 'rgb(68, 67, 67)' }}
                    onSelect={(index, value) => {
                        setisConfirmbtnId(id)
                        setSpoc(spocData[index].id)
                    }
                    }
                    defaultValue={default_spoc.name}
                    defaultTextStyle={'rgb(68, 67, 67)'}
                    dropdownTextHighlightStyle={{ backgroundColor: 'rgb(236, 243, 254)' }}
                    showsVerticalScrollIndicator={true}
                    style={{ width: wp('80%'), alignSelf: 'center', borderWidth: 1, borderColor: 'rgb(232, 232, 232)', backgroundColor: 'rgba(179, 179, 179, .1)', borderRadius: 4, padding: 8, marginBottom: hp('1%') }}
                    renderRightComponent={() =>
                        <View style={{ marginLeft: wp('0%'), borderWidth: 0 }}>
                            <Icon color={'#9B9B9B'} type='feather' name={IconPosition ? 'chevron-up' : 'chevron-down'} />
                        </View>
                    }
                    onDropdownWillShow={() => setIconPosition(true)}
                    onDropdownWillHide={() => setIconPosition(false)}
                />
                <View style={{ flexDirection: 'row', width: wp('80%'), alignSelf: 'center', justifyContent: 'space-between', }}>
                    <MyDatePicker
                        onSelectField={onSelectDate}
                        styling={styles.dateInputForLOD}
                        value={start_time}
                    />
                    <Input
                        placeholder={'Target Price'}
                        onChangeText={text => {
                            setDaysNeeded(text)
                            setisConfirmbtnId(id.toString())
                        }}
                        inputStyle={{ fontSize: hp('2.3%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                        value={daysNeeded}
                        keyboardType={'numeric'}
                        inputContainerStyle={{ borderBottomWidth: 0 }}
                        containerStyle={{ width: wp('38%'), borderWidth: 1, borderColor: 'rgb(232, 232, 232)', backgroundColor: 'rgba(179, 179, 179, .1)', height: hp('6%'), borderRadius: 4 }}
                    />
                </View>
                <Divider style={{ backgroundColor: 'rgba(179, 179, 179, 0.3)', height: hp('0.2%'), marginVertical: hp('1.5%') }} />
                <View style={{ display: 'flex', flexDirection: 'row', width: wp('40%'), alignSelf: 'center', justifyContent: isConfirmbtnId == id ? 'space-between' : 'center' }}>
                    <TouchableOpacity
                        onPress={() => { onPressRemoveBtn() }}
                    >
                        <Text style={{ textDecorationLine: 'underline', fontFamily: 'SourceSansPro-Regular', color: '#f38989' }}>REMOVE</Text>
                    </TouchableOpacity>

                    {
                        isConfirmbtnId == id &&
                        <TouchableOpacity
                            onPress={() => onPressConfirmBtn() }
                        >
                            <Text style={{ textDecorationLine: 'underline', fontFamily: 'SourceSansPro-Regular', color: '#6ca5f4' }}>CONFIRM</Text>
                        </TouchableOpacity>
                    }
                </View>
            </Card>
        </View>
    );
}


const styles = StyleSheet.create({

    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        display: 'flex',
        backgroundColor: "white",
    },

    item_card: {
        borderRadius: 2,
        borderWidth: 0,
        padding: '5%',
    },

    dateInputForLOD: {
        paddingHorizontal: wp('3%'),
        width: wp('38%'),
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        backgroundColor: 'rgba(179, 179, 179, .1)',
        alignSelf: 'center',
        borderRadius: 4,
    },

    projectimage: {
        width: wp('10%'),
        height: hp('10%')
    },
    name_text: {
        fontSize: hp('2.8%'),
        fontFamily: 'SourceSansPro-SemiBold',
        color: 'rgb(68, 67, 67)',
    },

    email_text: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Bold',
        color: 'rgb(68, 67, 67)',
    },

    DaysText: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(194, 194, 194)',
    },

    department_text: {
        fontSize: hp('1.8%'),
        fontFamily: 'SourceSansPro-SemiBold',
        color: 'rgb(143, 143, 143)',
    },


    datePickerStyle: {
        width: 200,
        marginTop: 20,
    },

});

export default EventCard;
