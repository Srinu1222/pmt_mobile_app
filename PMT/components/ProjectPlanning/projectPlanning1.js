import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import React, { useState, useEffect, useMemo } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { Avatar, Card, IconButton } from 'react-native-paper';
import { Icon, } from 'react-native-elements';
import DialogComponent from './DialogComponent';
import * as actionCreators from '../../actions/project-action/index';

const ProjectPlanning1 = ({ route, navigation }) => {

    const { project_id, is_sub_contractor, project_completion, token } = route.params

    const stages = ['Pre-Requisites', 'Approvals', 'Engineering', 'Procurement', 'Material Handeling', 'Construction', 'Site Handover']

    var [enableStages, setenableStages] = useState([]);
    var [enableNextStages, setenableNextStages] = useState(['Pre-Requisites']);

    const onClickStage = (stage) => {
        if (!enableStages.includes(stage)) {
            setenableStages(enableStages => [...enableStages, stage]);
        }
        setenableNextStages(enableNextStages => [...enableNextStages, stages[stages.indexOf(stage) + 1]]);
        navigation.navigate('project_stage', {
            stage: stage,
            project_id: project_id,
            is_sub_contractor: is_sub_contractor,
            project_completion: project_completion
        })
    };

    const [isVisible, setIsVisible] = useState(false);
    const dispatch = useDispatch();

    const onClickCancel = () => {
        // const formData = new FormData();
        // formData.append('project_id', projectId);
        // formData.append('project_completion', 'True');
        // formData.append('generate_template', false);

        // if (!is_sub_contractor) {
        // dispatch(actionCreators.changeProjectStatus(formData, token));
        // } else {
        //     dispatch(actionCreators.changeSubContractorProjectStatus())
        // }
        setIsVisible(false)
    }

    const generateGantt = () => {
        let changed = false
        const formData = new FormData();
        formData.append('project_id', project_id);
        formData.append('project_completion', 'True');
        formData.append('generate_template', true);

        if (changed === false && !is_sub_contractor) {
            dispatch(actionCreators.changeProjectStatus(formData, token));
            navigation.navigate('DetailedEventPortraitView', { project_id: project_id, is_sub_contractor: is_sub_contractor })
        } else if (is_sub_contractor) {
            dispatch(actionCreators.changeSubContractorProjectStatus())
        }
        setIsVisible(false)
    }

    const Header = () => (
        <Card.Title
            style={{ backgroundColor: 'white', elevation: 4, paddingRight: wp('3%') }}
            title={'Project Planning'}
            titleStyle={styles.text}
            right={() => <TouchableOpacity onPress={() => navigation.goBack()}>
                <Text style={styles.backtoText}>{'<'}Back to </Text>
                <Text style={styles.backtoText}>Project Specification</Text>
            </TouchableOpacity>
            }
        />
    );


    return (

        <View style={styles.container}>
            <Header />
            <View style={{ marginVertical: hp('3%') }}>
                {stages.map((i, index) => {
                    let is_active_stage = enableStages.includes(i)
                    let is_active_stage_for_shadow = enableNextStages.includes(i)
                    return (
                        <Card
                            elevation={4}
                            key={index}
                            style={{
                                backgroundColor: '#fff',
                                marginVertical: hp('.5%'),
                                marginHorizontal: wp('5%'),
                                justifyContent: 'center',
                                shadowColor: is_active_stage_for_shadow ? 'black' : 'white'
                            }}
                            onPress={() => { onClickStage(i) }}
                        >
                            <View
                                style={{ borderWidth: 0, flexDirection: 'row', width: wp('90%'), paddingHorizontal: wp('3%'), height: hp('7%'), alignItems: 'center', justifyContent: 'space-between', }}
                            >
                                {
                                    is_active_stage &&
                                    <Icon color={'green'} size={30} type='ionicon' name='checkmark-circle' />
                                }
                                <Text style={{
                                    width: wp('60%'),
                                    fontSize: hp('2%'),
                                    // color: is_active_stage ? 'rgb(68, 67, 67)' : 'rgb(143, 143, 143)',
                                    fontFamily: 'SourceSansPro-Regular',
                                }}>{i}</Text>
                                <Icon color={'rgb(143, 143, 143)'} type='feather' name='chevron-down' />
                            </View>
                        </Card>
                    );
                })}
            </View>
            <TouchableOpacity
                onPress={() => setIsVisible(true)}
                style={styles.generateganttbtnView}>
                <Text style={styles.generateganttbtn}>Generate Gantt</Text>
            </TouchableOpacity>

            <DialogComponent
                visible={isVisible}
                generateGantt={generateGantt}
                onClickCancel={onClickCancel}
                setIsVisible={setIsVisible}
            />

            <TouchableOpacity
                onPress={() => navigation.navigate('dashboard')}
                style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', marginTop: hp('2%') }}
            >
                <Text style={{ textDecorationLine: 'underline', fontSize: hp('2.3%'), fontFamily: 'SourceSansPro-Regular', color: 'rgb(61, 135, 247)' }}>
                    Save & Exit
                </Text>
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        display: 'flex',
        backgroundColor: "white",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },


    header: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: hp('9%'),
        alignItems: 'center',
        marginHorizontal: '5%',
        paddingVertical: '2%',
        borderBottomWidth: 2,
        borderBottomColor: 'rgb(232,232,232)',
        marginBottom: hp('2%')
    },

    cardContainer: {
        display: 'flex',
        justifyContent: 'space-between',
        height: hp('8%'),
        alignItems: 'center',
        flexDirection: 'row',
        borderRadius: 0,
        marginHorizontal: wp('5%'),
        paddingHorizontal: '4%',
        borderWidth: 0,
        marginTop: hp('2%')
    },

    text: {
        fontSize: hp('2.5%'),
        color: 'rgb(68, 67, 67)',
        fontFamily: 'SourceSansPro-SemiBold',
    },

    stagetext: {
        fontSize: hp('2%'),
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center',
        fontFamily: 'SourceSansPro-Regular',
    },

    backtoText: {
        alignSelf: 'flex-end',
        textDecorationLine: 'underline',
        color: 'rgb(61, 135, 241)',
        fontFamily: 'SourceSansPro-Regular',
    },

    image_logo: {
        width: wp('4%'),
        height: hp('2%'),
    },

    images: {
        alignSelf: 'center'
    },


    generateganttbtnView: {
        height: hp('7.3%'),
        marginHorizontal: wp('10%'),
        marginTop: hp('2%'),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'dodgerblue',
        borderRadius: 5,
    },
    generateganttbtn: {
        color: 'white',
        fontSize: hp('2.5%'),
        textAlign: 'center',
        fontFamily: 'SourceSansPro-Regular',
    }


});

export default ProjectPlanning1;