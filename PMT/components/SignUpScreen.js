import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    Keyboard
} from 'react-native';
import LottieView from 'lottie-react-native';
import React, { useEffect, useState, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import Feather from 'react-native-vector-icons/Feather';
import TextBox from 'react-native-password-eye';
import * as Animatable from 'react-native-animatable'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { RadioButton } from 'react-native-paper';
import notifyMessage from '../ToastMessages';
import DropDownPicker from 'react-native-dropdown-picker';
import DocumentPicker from 'react-native-document-picker';
import * as actionCreators from "../actions/auth-action/index";


const SignUpScreen = ({ navigation }) => {
    const dispatch = useDispatch()
    const [checked, setChecked] = React.useState('');
    const [imported, setImported] = React.useState('No');

    const [email, setemail] = useState("");
    const [password, setpassword] = useState("");

    const isMounted = useRef(false);

    const [department, setDepartment] = useState("");

    const [companyName, setCompanyName] = useState("");
    const [website, setWebsite] = useState("");
    const [headOffice, setHeadOffice] = useState("");
    const [regionalOffice, setRegionalOffice] = useState("");
    const [aboutCompany, setAboutCompany] = useState("");


    const [empStrength, setEmpStrength] = useState("");
    const [fileList, setFileList] = useState(['']);


    const [installedCapacity, setInstalledCapacity] = useState("");
    const [minProjectSize, setMinProjectSize] = useState("");
    const [largestProject, setLargestProject] = useState("");
    const [businessModel, setBusinessModel] = useState("");
    const [activeStates, setActiveStates] = useState("");
    const [flagshipProjects, setFlagshipProjects] = useState("");

    const [open, setOpen] = useState(false);
    const [value, setValue] = useState([]);
    const [turnoverList, setturnoverList] = useState([
        { label: 'Rs. <10 cr', value: 'Rs. <10 cr' },
        { label: 'Rs. 10-50 cr', value: 'Rs. 10-50 cr' },
        { label: 'Rs. 50-100 cr', value: 'Rs. 50-100 cr' },
        { label: 'Rs. 100-250 cr', value: 'Rs. 100-250 cr' },
        { label: 'Rs. 250 cr+', value: 'Rs. 250 cr+' }

    ]);


    const [open1, setOpen1] = useState(false);
    const [value1, setValue1] = useState([]);
    const [employeeStrengthList, setemployeeStrengthList] = useState([
        { label: '1-10', value: '1-10' },
        { label: '11-25', value: '11-25' },
        { label: '25-50', value: '25-50' },
        { label: '50-100', value: '50-100' },
        { label: '100+', value: '100+' }

    ]);


    const [open2, setOpen2] = useState(false);
    const [value2, setValue2] = useState([]);
    const [projectModelList, setprojectModelList] = useState([
        { label: 'CAPEX', value: 'CAPEX' },
        { label: 'OPEX', value: 'OPEX' }

    ]);
    const [open3, setOpen3] = useState(false);
    const [value3, setValue3] = useState([]);
    const [statesAndUTsList, setstatesAndUTsList] = useState([
        { label: 'Andhra Pradesh', value: 'Andhra Pradesh' },
        { label: 'Arunachal Pradesh', value: 'Arunachal Pradesh' },
        { label: 'Assam', value: 'Assam' },
        { label: 'Bihar', value: 'Bihar' },
        { label: 'Chhattisgarh', value: 'Chhattisgarh' },
        { label: 'Delhi', value: 'Delhi' },
        { label: 'Goa', value: 'Goa' },
        { label: 'Gujarat', value: 'Gujarat' },
        { label: 'Haryana', value: 'Haryana' },
        { label: 'Himachal Pradesh', value: 'Himachal Pradesh' },
        { label: 'Jammu and Kashmir', value: 'Jammu and Kashmir' },
        { label: 'Jharkhand', value: 'Jharkhand' },
        { label: 'Karnataka', value: 'Karnataka' },
        { label: 'Kerala', value: 'Kerala' },
        { label: 'Madhya Pradesh', value: 'Madhya Pradesh' },
        { label: 'Maharashtra', value: 'Maharashtra' },
        { label: 'Manipur', value: 'Manipur' },
        { label: 'Meghalaya', value: 'Meghalaya' },
        { label: 'Mizoram', value: 'Mizoram' },
        { label: 'Nagaland', value: 'Nagaland' },
        { label: 'Odisha', value: 'Odisha' },
        { label: 'Punjab', value: 'Punjab' },
        { label: 'Rajasthan', value: 'Rajasthan' },
        { label: 'Sikkim', value: 'Sikkim' },
        { label: 'Tamil Nadu', value: 'Tamil Nadu' },
        { label: 'Telengana', value: 'Telengana' },
        { label: 'Tripura', value: 'Tripura' },
        { label: 'Uttrakhand', value: 'Uttrakhand' },
        { label: 'Uttar Pradesh', value: 'Uttar Pradesh' },
        { label: 'West Bengal', value: 'West Bengal' },
        { label: 'Andaman and Nicobar Islands', value: 'Andaman and Nicobar Islands' },
        { label: 'Ladakh', value: 'Ladakh' },
        { label: 'Lakshadweep', value: 'Lakshadweep' },
        { label: 'Puducherry', value: 'Puducherry' },

    ]);





    const onBrochure = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.allFiles],
            });
            setFileList(res.uri)
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
            } else {
                throw err;
            }
        }
    }

    const companyDetails = (params) => {
        navigation.navigate('Signup3');
    }
    const companyDetails1 = (params) => {



        if (companyName != '' && website != '' && headOffice != '' && regionalOffice != '' && aboutCompany != '' && value != '' && value1 != '' && installedCapacity != '' && minProjectSize != '' && largestProject != '' && value2 != '' && value3 != '' && flagshipProjects != '') {

            navigation.navigate('Signup2', {
                companyName: companyName,
                website: website,
                headOffice: headOffice,
                regionalOffice: regionalOffice,
                aboutCompany: aboutCompany,

                annualTurnover: value,
                empStrength: value1,
                fileList: fileList,
                installedCapacity: installedCapacity,
                minProjectSize: minProjectSize,

                largestProject: largestProject,
                businessModel: value2,
                activeStates: value3,
                flagshipProjects: flagshipProjects,

                checked:checked,
                email1:email
            });
        } else {
            notifyMessage("Please fill all the fields");
        }
    }
    const renderOption = (title) => (
        <SelectItem title={title} />
    );

    
    const handleImportCompanyDetails = () => {
        dispatch(actionCreators.getCompanyDetails(email, password));
    };

    const getCompanyDetails = useSelector((state) => {
        return state.signUpReducers.getCompanyDetails.get;
    });

    useEffect(() => {
        if (isMounted.current && getCompanyDetails.failure.error) {
            notifyMessage("Invalid credentials!");
            // form.resetFields();
        } else {
            isMounted.current = true;
        }
    }, [getCompanyDetails.failure.error]);

    useEffect(() => {
        if (getCompanyDetails.success.ok) {
            let companyDetails = getCompanyDetails.success.data;
            setCompanyName(companyDetails?.company);
            setWebsite(companyDetails?.website);
            setHeadOffice(companyDetails?.head_office);
            setRegionalOffice(companyDetails?.regional_office);
            setAboutCompany(companyDetails?.description);
            setValue(companyDetails?.turnover);
            setValue1(companyDetails?.employee_strength);
            //   setFileList(companyDetails?.brochure);
            setInstalledCapacity(companyDetails?.capacity_installed);
            setMinProjectSize(companyDetails?.minimum_size_project);
            setLargestProject(companyDetails?.maximum_size_project);
            setValue2(companyDetails.business_model);
            setValue3(companyDetails?.active_states);
            setFlagshipProjects(companyDetails?.flagship_projects?.join(", "));
            setImported('Yes')
            //   setemail(companyDetails?.name);
            // setFileList([...fileList, {"uid":companyDetails.brochure}])
        }
    }, [getCompanyDetails.success.data]);


    const handleCheckno = () => {
        setChecked('No');
        setImported('No');
        setCompanyName('');
        setWebsite('');
        setHeadOffice('');
        setRegionalOffice('');
        setAboutCompany('');
        setValue([]);
        setValue1([]);
        //   setFileList(companyDetails?.brochure);
        setInstalledCapacity('');
        setMinProjectSize('');
        setLargestProject('');
        setValue2([]);
        setValue3([]);
        setFlagshipProjects('');
    }

    return (

        <SafeAreaView>
            <KeyboardAvoidingView>
                <ScrollView>
                    <View style={styles.container}>
                        <View style={styles.parent1}>
                            <View style={styles.parent1FirstView}>
                                <TouchableOpacity
                                    onPress={() => { }}
                                    style={styles.leftArrow}
                                >
                                    <Image
                                        style={{ height: 24, width: 24 }}
                                        source={require('../assets/icons/left_arrow.png')}
                                    />
                                </TouchableOpacity>
                                <Text style={{ left: 130, top: 24, color: '#7a6a4f' }}><Text style={{ fontWeight: 'bold', fontSize: 18 }}>01</Text>/03</Text>
                            </View>
                            <View style={{ top: 50, display: 'flex', flexDirection: 'row', left: 37 }}>
                                <Text style={{ width: wp('25%'), height: 4, backgroundColor: '#3A80E3' }}></Text>
                                <Text style={{ width: wp('50%'), height: 4, backgroundColor: "rgba(232,232,232, 0.7)" }}></Text>
                            </View>
                            <Text style={{ left: 37, top: 55, color: '#7a6a4f' }}>Basic Info</Text>
                            <Text style={{ left: 37, top: 70, height: 25, fontSize: 19, color: '#7a6a4f' }}>Do you use</Text>
                            <Text style={{ left: 37, top: 70, height: 25, fontSize: 19, color: '#7a6a4f' }}>SafEarth Project Marketplace?</Text>
                            <View style={{ top: 90, left: 37, display: 'flex', flexDirection: 'row' }}>
                                <View style={{ display: 'flex', flexDirection: 'row' }}>
                                    <RadioButton
                                        value="Yes"
                                        color="#3A80E3"
                                        status={checked === 'Yes' ? 'checked' : 'unchecked'}
                                        onPress={() => setChecked('Yes')}
                                    />


                                    <Text style={{ left: 5, width: 30, height: 20, top: 8, color: '#7a6a4f' }}>Yes</Text>
                                </View>
                                <View style={{ left: 90, display: 'flex', flexDirection: 'row' }}>
                                    <RadioButton
                                        value="No"
                                        color="#3A80E3"
                                        status={checked === 'No' ? 'checked' : 'unchecked'}
                                        onPress={() => handleCheckno()}
                                    />
                                    <Text style={{ left: 5, width: 25, height: 20, top: 8, color: '#7a6a4f' }}>No</Text>
                                </View>
                            </View>
                        </View>
                        <>
                            {checked == 'No' ?
                            <>
                            
                                    <View style={styles.parent3}>
                                        <Text style={{ left: 30, top: 30, height: 22, fontSize: 19, color: 'rgb(255,255,255)' }}>CATEGORY 1</Text>

                                        <TextInput
                                            placeholder="Company Name"
                                            placeholderTextColor="white"
                                            style={styles.textInput}
                                            autoCapitalize="none"
                                            value={companyName}
                                            onChangeText={(val) => setCompanyName(val)}
                                        />

                                        <TextInput
                                            placeholder="Website"
                                            placeholderTextColor="white"
                                            style={styles.textInput}
                                            autoCapitalize="none"
                                            value={website}
                                            onChangeText={(val) => setWebsite(val)}
                                        />

                                        <TextInput
                                            placeholder="Head Office"
                                            placeholderTextColor="white"
                                            style={styles.textInput}
                                            autoCapitalize="none"
                                            value={headOffice}
                                            onChangeText={(val) => setHeadOffice(val)}
                                        />


                                        <TextInput
                                            placeholder="Regional Office"
                                            placeholderTextColor="white"
                                            style={styles.textInput}
                                            autoCapitalize="none"
                                            value={regionalOffice}
                                            onChangeText={(val) => setRegionalOffice(val)}
                                        />
                                        <TextInput
                                            placeholder="Company Brief"
                                            placeholderTextColor="white"
                                            style={styles.textInput}
                                            autoCapitalize="none"
                                            value={aboutCompany}
                                            onChangeText={(val) => setAboutCompany(val)}
                                        />
                                        <Text style={{ left: 30, top: 50, height: 22, fontSize: 19, color: 'rgb(255,255,255)' }}>CATEGORY 2</Text>
                                        <View style={{ marginTop: 10 }}>

                                            <DropDownPicker
                                                style={styles.textInput1}
                                                containerStyle={{
                                                    width: wp('72%'),
                                                    marginLeft: 30,
                                                }}
                                                dropDownContainerStyle={{
                                                    borderColor: "white",
                                                    marginBottom: -30
                                                }}
                                                labelStyle={{ color: 'white' }}
                                                dropDownDirection="TOP"
                                                placeholderStyle={{
                                                    color: "white",
                                                }}

                                                open={open}
                                                value={value}
                                                items={turnoverList}
                                                setOpen={setOpen}
                                                setValue={setValue}
                                                setItems={setturnoverList}
                                            />

                                            <DropDownPicker
                                                style={styles.textInput1}
                                                containerStyle={{
                                                    width: wp('72%'),
                                                    marginLeft: 30,
                                                }}
                                                dropDownContainerStyle={{
                                                    borderColor: "white"
                                                }}
                                                labelStyle={{ color: 'white' }}
                                                placeholderStyle={{
                                                    color: "white",
                                                }}
                                                open={open1}
                                                value={value1}
                                                items={employeeStrengthList}
                                                setOpen={setOpen1}
                                                setValue={setValue1}
                                                setItems={setemployeeStrengthList}
                                            />
                                            <TouchableOpacity
                                                style={[styles.textInput, { borderColor: '#fff', borderWidth: 1 }]}
                                                onPress={() => { onBrochure() }}
                                            >


                                                <Text style={{ textAlign: 'center', top: 15, fontSize: 16, fontFamily: 'SourceSansPro-Regula', color: '#fff' }}>Upload Brochure</Text>
                                            </TouchableOpacity>
                                        </View>
                                        <Text style={{ left: 30, top: 50, height: 22, fontSize: 19, color: 'rgb(255,255,255)' }}>CATEGORY 3</Text>
                                        <View style={{ marginTop: 10 }}>
                                            <TextInput
                                                placeholder="Installed Capacity (MW)"
                                                placeholderTextColor="white"
                                                style={styles.textInput}
                                                autoCapitalize="none"
                                                value={installedCapacity}
                                                onChangeText={(val) => setInstalledCapacity(val)}
                                            />

                                            <TextInput
                                                placeholder="Minimum Project Size (KW)"
                                                placeholderTextColor="white"
                                                style={styles.textInput}
                                                autoCapitalize="none"
                                                value={minProjectSize}
                                                onChangeText={(val) => setMinProjectSize(val)}
                                            />

                                            <TextInput
                                                placeholder="Largest Project (MW)"
                                                placeholderTextColor="white"
                                                style={styles.textInput}
                                                autoCapitalize="none"
                                                value={largestProject}
                                                onChangeText={(val) => setLargestProject(val)}
                                            />


                                            <DropDownPicker
                                                style={styles.textInput1}
                                                containerStyle={{
                                                    width: wp('72%'),
                                                    marginLeft: 30,
                                                }}
                                                dropDownContainerStyle={{
                                                    borderColor: "white",
                                                    marginBottom: -30
                                                }}
                                                placeholderStyle={{
                                                    color: "white",
                                                }}
                                                labelStyle={{ color: 'white' }}
                                                dropDownDirection="TOP"
                                                open={open2}
                                                value={value2}
                                                items={projectModelList}
                                                setOpen={setOpen2}
                                                setValue={setValue2}
                                                setItems={setprojectModelList}
                                            />

                                            <DropDownPicker
                                                style={styles.textInput1}
                                                containerStyle={{
                                                    width: wp('72%'),
                                                    marginLeft: 30,
                                                }}
                                                dropDownContainerStyle={{
                                                    borderColor: "white"
                                                }}
                                                dropDownStyle={{ backgroundColor: 'rgba(100,160,241,0.5)', height: 120 }}
                                                maxHeight={300}
                                                scrollViewProps={{
                                                    persistentScrollbar: true
                                                }}
                                                placeholderStyle={{
                                                    color: "white",
                                                }}
                                                listMode={"SCROLLVIEW"}
                                                labelStyle={{ color: 'white' }}
                                                multiple={true}
                                                open={open3}
                                                value={value3}
                                                items={statesAndUTsList}
                                                setOpen={setOpen3}
                                                setValue={setValue3}
                                                setItems={setstatesAndUTsList}
                                            />
                                            <TextInput
                                                placeholder="Flagship Projects"
                                                placeholderTextColor="white"
                                                style={styles.textInput}
                                                autoCapitalize="none"
                                                value={flagshipProjects}
                                                onChangeText={(val) => setFlagshipProjects(val)}
                                            />
                                            <TouchableOpacity
                                                style={styles.CompanyDetails1}
                                                onPress={companyDetails1}
                                            >
                                                <Text style={[styles.textSign, {
                                                    color: '#3A80E3'
                                                }]}>Next</Text>

                                            </TouchableOpacity>
                                        </View>


                                    </View>
                                </>
                            
                            :
                                <>
                                    {imported == 'No' ?
                                        <>
                                            <View style={styles.parent2}>


                                                <TextInput
                                                    placeholder="Email Address"
                                                    placeholderTextColor="white"
                                                    style={styles.textInput}
                                                    autoCapitalize="none"
                                                    value={email}
                                                    onChangeText={(val) => setemail(val)}
                                                />

                                                <TextInput
                                                    placeholder="Password"
                                                    placeholderTextColor="white"
                                                    style={styles.textInput}
                                                    autoCapitalize="none"
                                                    value={password}
                                                    onChangeText={(val) => setpassword(val)}
                                                />

                                                <TouchableOpacity
                                                    style={styles.eyeSymbol}
                                                >

                                                </TouchableOpacity>

                                                {checked==''?
                                                <TouchableOpacity
                                                style={[styles.CompanyDetails,{backgroundColor:'rgba(100,160,241,0.5)'}]}
                                           >
                                               <Text style={[styles.textSign, {
                                                   color: '#000'
                                               }]}>Import Company Details</Text>

                                           </TouchableOpacity>
                                           :
                                           <TouchableOpacity
                                                     style={[styles.CompanyDetails,{backgroundColor:'rgb(255,255,255)'}]}
                                                    onPress={handleImportCompanyDetails}
                                                >
                                                    <Text style={[styles.textSign, {
                                                        color: '#000'
                                                    }]}>Import Company Details</Text>

                                                </TouchableOpacity>

                                                }
                                               
                                            </View>

                                        </> :
                                        <>
                                            <View style={styles.parent3}>
                                                <Text style={{ left: 30, top: 30, height: 22, fontSize: 19, color: 'rgb(255,255,255)' }}>CATEGORY 1</Text>

                                                <TextInput
                                                    placeholder="Company Name"
                                                    placeholderTextColor="white"
                                                    style={styles.textInput}
                                                    autoCapitalize="none"
                                                    value={companyName}
                                                    onChangeText={(val) => setCompanyName(val)}
                                                />

                                                <TextInput
                                                    placeholder="Website"
                                                    placeholderTextColor="white"
                                                    style={styles.textInput}
                                                    autoCapitalize="none"
                                                    value={website}
                                                    onChangeText={(val) => setWebsite(val)}
                                                />

                                                <TextInput
                                                    placeholder="Head Office"
                                                    placeholderTextColor="white"
                                                    style={styles.textInput}
                                                    autoCapitalize="none"
                                                    value={headOffice}
                                                    onChangeText={(val) => setHeadOffice(val)}
                                                />


                                                <TextInput
                                                    placeholder="Regional Office"
                                                    placeholderTextColor="white"
                                                    style={styles.textInput}
                                                    autoCapitalize="none"
                                                    value={regionalOffice}
                                                    onChangeText={(val) => setRegionalOffice(val)}
                                                />
                                                <TextInput
                                                    placeholder="Company Brief"
                                                    placeholderTextColor="white"
                                                    style={styles.textInput}
                                                    autoCapitalize="none"
                                                    value={aboutCompany}
                                                    onChangeText={(val) => setAboutCompany(val)}
                                                />
                                                <Text style={{ left: 30, top: 50, height: 22, fontSize: 19, color: 'rgb(255,255,255)' }}>CATEGORY 2</Text>
                                                <View style={{ marginTop: 10 }}>

                                                    <DropDownPicker
                                                        style={styles.textInput1}
                                                        containerStyle={{
                                                            width: wp('72%'),
                                                            marginLeft: 30,
                                                        }}
                                                        dropDownContainerStyle={{
                                                            borderColor: "white",
                                                            marginBottom: -30
                                                        }}
                                                        labelStyle={{ color: 'white' }}
                                                        dropDownDirection="TOP"
                                                        placeholderStyle={{
                                                            color: "white",
                                                        }}

                                                        open={open}
                                                        value={value}
                                                        items={turnoverList}
                                                        setOpen={setOpen}
                                                        setValue={setValue}
                                                        setItems={setturnoverList}
                                                    />

                                                    <DropDownPicker
                                                        style={styles.textInput1}
                                                        containerStyle={{
                                                            width: wp('72%'),
                                                            marginLeft: 30,
                                                        }}
                                                        dropDownContainerStyle={{
                                                            borderColor: "white"
                                                        }}
                                                        labelStyle={{ color: 'white' }}
                                                        placeholderStyle={{
                                                            color: "white",
                                                        }}
                                                        open={open1}
                                                        value={value1}
                                                        items={employeeStrengthList}
                                                        setOpen={setOpen1}
                                                        setValue={setValue1}
                                                        setItems={setemployeeStrengthList}
                                                    />
                                                    <TouchableOpacity
                                                        style={[styles.textInput, { borderColor: '#fff', borderWidth: 1 }]}
                                                        onPress={() => { onBrochure() }}
                                                    >


                                                        <Text style={{ textAlign: 'center', top: 15, fontSize: 16, fontFamily: 'SourceSansPro-Regula', color: '#fff' }}>Upload Brochure</Text>
                                                    </TouchableOpacity>
                                                </View>
                                                <Text style={{ left: 30, top: 50, height: 22, fontSize: 19, color: 'rgb(255,255,255)' }}>CATEGORY 3</Text>
                                                <View style={{ marginTop: 10 }}>
                                                    <TextInput
                                                        placeholder="Installed Capacity (MW)"
                                                        placeholderTextColor="white"
                                                        style={styles.textInput}
                                                        autoCapitalize="none"
                                                        value={installedCapacity}
                                                        onChangeText={(val) => setInstalledCapacity(val)}
                                                    />

                                                    <TextInput
                                                        placeholder="Minimum Project Size (KW)"
                                                        placeholderTextColor="white"
                                                        style={styles.textInput}
                                                        autoCapitalize="none"
                                                        value={minProjectSize}
                                                        onChangeText={(val) => setMinProjectSize(val)}
                                                    />

                                                    <TextInput
                                                        placeholder="Largest Project (MW)"
                                                        placeholderTextColor="white"
                                                        style={styles.textInput}
                                                        autoCapitalize="none"
                                                        value={largestProject}
                                                        onChangeText={(val) => setLargestProject(val)}
                                                    />


                                                    <DropDownPicker
                                                        style={styles.textInput1}
                                                        containerStyle={{
                                                            width: wp('72%'),
                                                            marginLeft: 30,
                                                        }}
                                                        dropDownContainerStyle={{
                                                            borderColor: "white",
                                                            marginBottom: -30
                                                        }}
                                                        placeholderStyle={{
                                                            color: "white",
                                                        }}
                                                        labelStyle={{ color: 'white' }}
                                                        dropDownDirection="TOP"
                                                        open={open2}
                                                        value={value2}
                                                        items={projectModelList}
                                                        setOpen={setOpen2}
                                                        setValue={setValue2}
                                                        setItems={setprojectModelList}
                                                    />

                                                    <DropDownPicker
                                                        style={styles.textInput1}
                                                        containerStyle={{
                                                            width: wp('72%'),
                                                            marginLeft: 30,
                                                        }}
                                                        dropDownContainerStyle={{
                                                            borderColor: "white"
                                                        }}
                                                        dropDownStyle={{ backgroundColor: 'rgba(100,160,241,0.5)', height: 120 }}
                                                        maxHeight={300}
                                                        scrollViewProps={{
                                                            persistentScrollbar: true
                                                        }}
                                                        placeholderStyle={{
                                                            color: "white",
                                                        }}
                                                        listMode={"SCROLLVIEW"}
                                                        labelStyle={{ color: 'white' }}
                                                        multiple={true}
                                                        open={open3}
                                                        value={value3}
                                                        items={statesAndUTsList}
                                                        setOpen={setOpen3}
                                                        setValue={setValue3}
                                                        setItems={setstatesAndUTsList}
                                                    />
                                                    <TextInput
                                                        placeholder="Flagship Projects"
                                                        placeholderTextColor="white"
                                                        style={styles.textInput}
                                                        autoCapitalize="none"
                                                        value={flagshipProjects}
                                                        onChangeText={(val) => setFlagshipProjects(val)}
                                                    />
                                                    <TouchableOpacity
                                                        style={styles.CompanyDetails1}
                                                        onPress={companyDetails1}
                                                    >
                                                        <Text style={[styles.textSign, {
                                                            color: '#3A80E3'
                                                        }]}>Next</Text>

                                                    </TouchableOpacity>
                                                </View>


                                            </View>
                                        </>}

                                </>
                                
                                
                            }

                        </>

                    </View>
                </ScrollView>
            </KeyboardAvoidingView >
        </SafeAreaView >
    );
};

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        // height: Dimensions.get('window').height,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
    parent1: {
        width: Dimensions.get('window').width,
        height: hp('40%'),
        flex: 1,
        backgroundColor: "white"
    },

    parent2: {
        width: Dimensions.get('window').width,
        height: 0.75 * Dimensions.get('window').height,
        alignSelf: 'auto',
        flex: 2,
        backgroundColor: '#3A80E3',
    },
    parent3: {
        width: Dimensions.get('window').width,
        height: 1.80 * Dimensions.get('window').height,
        alignSelf: 'auto',
        flex: 2,
        backgroundColor: '#3A80E3',
    },

    parent1FirstView: {
        display: 'flex',
        flexDirection: 'row',
    },

    leftArrow: {
        left: 16,
        top: 25,
    },

    radioButton: {
        top: 4,
        borderWidth: 2,
        borderColor: '#3A80E3',
        alignItems: 'center',
        justifyContent: 'center',
        width: 16,
        height: 16,
        borderRadius: 50,
    },

    textInput: {
        width: wp('80%'),
        left: 30,
        top: 60,
        height: 56,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: 'white',
        marginBottom: 30,
        backgroundColor: "rgba(100,160,241,0.5)",
        borderRadius: 5
    },
    textInput1: {
        width: wp('80%'),
        top: 60,
        // marginTop:40,
        height: 56,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: 'white',
        marginBottom: 30,
        borderColor: "rgba(100,160,241,0.5)",
        backgroundColor: "rgba(100,160,241,0.5)",
        borderRadius: 5
    },

    textSign: {
        textAlign: 'center',
        top: '30%',
        fontSize: 16,
        lineHeight: 20,
    },

    errorMsg: {
        color: '#FF0000',
        fontSize: 14,
        marginTop: 30,
        left: 30
    },

    errorMsgForPassword: {
        color: '#FF0000',
        fontSize: 14,
        marginTop: 5,
        left: 30
    },

    eyeSymbol: {
        padding: 5,
        left: 280,
        bottom: 10
    },

    CompanyDetails: {
        width: wp('80%'),
        left: 30,
        top: hp('35%'),
        height: 56,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: 'white',
        marginBottom: 30,
        borderRadius: 5


    },
    CompanyDetails1: {
        width: Dimensions.get('window').width,
        height: 56,
        marginTop: hp('7.5%'),
        // marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        backgroundColor: '#fff',
        borderRadius: 5
    },
});

export default SignUpScreen;
