import React, { useEffect, useState } from 'react';
import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    PermissionsAndroid,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    Picker
} from 'react-native';
import { Searchbar, Button, Switch } from 'react-native-paper';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import * as actionCreators from '../../actions/sub-contractor-action/index';
import { useDispatch, useSelector } from 'react-redux';
import { FlatList } from 'react-native-gesture-handler';
import FastImage from 'react-native-fast-image'
import NewProjectHeader from '../EmptyScreens/NewProjectHeader';
import { SearchBar, Badge } from 'react-native-elements';
import { Checkbox, RadioButton } from 'react-native-paper';
import Icon from "react-native-vector-icons/MaterialIcons";
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import { IndexPath, Layout, Select, SelectGroup, SelectItem } from '@ui-kitten/components';
import Toast from 'react-native-toast-message';




const SubContractorDetails = ({ props, route, navigation }) => {
    const { project, image, customer, team, subContractorReducer, token } = route.params;
    const dispatch = useDispatch();

    const onChangeSearch = query => setSearchQuery(query);
    const [needed, setNeeded] = React.useState(true);

    const [Checkedteam, setChecked] = useState([]);
    const [Checkedrights, setCheckedrights] = useState([]);

    const [subContractor, setsubContractor] = useState(subContractorReducer);
    const [selectectedItems, setselectectedItems] = useState([]);
    const [isShownPicker, setisShownPicker] = useState(false);


    const [onStockButtonPress, setonStockButtonPress] = React.useState(true);
    const [onConsumedButtonPress, setonConsumedButtonPress] = React.useState(false);
   
    const data =  [
        "Pre-Requisite",
        "Engineering",
        "Approvals",
        "Procurement",
        "Material Handling",
        "Construction",
        "Hand-Over"
    ];

    const [rights, setrights] = React.useState([]);
    const groupDisplayValues=[];
    // const groupDisplayValues = rights.map(index => {
    //     return data[index.row];
    // });
    const renderOption = (title) => (
        <SelectItem title={title}/>
      );
    const onPressStock = (params) => {
        setonStockButtonPress(true)
        setonConsumedButtonPress(false)
    }

    const onPressConsumed = (params) => {
        setonConsumedButtonPress(true)
        setonStockButtonPress(false)
    }



    const onPressChecked = (items, i, rights) => {
        if (rights != '') {

          const rightsValues = rights.map(index => {
        return data[index.row];
    });
    let idrights={
        id:items.company_id,
        userName:items.user_name,
        companyName:items.company_name,
        stages:rightsValues
    }
        setChecked([...Checkedteam, items])
        setCheckedrights([...Checkedrights, idrights])
        subContractor.splice(i, 1);
        setrights([])

    } else {
        Toast.show({
            type: 'success',
            text1: 'rights',
            text2: 'Please assign a rights to the Sub-Contractor before selecting.'
          });
    }
        // Checkedteam.sort(function(a, b) {
        //     return a.id > b.id;
        //   });
    }

    const onPressUnchecked = (items, i) => {
        setsubContractor([...subContractor, items])
        Checkedteam.splice(i, 1);
        Checkedrights.splice(i, 1);

        // Team.sort(function(a, b) {
        //     return a.id > b.id;
        //   });

    }

    const onPressNeeded = () => {
       
        setsubContractor([])
        setNeeded(false)
        navigation.navigate('Databank', {
            project: project,
            TeamDetails: team,
            is_sub_contractor:needed,
            subContractorReducer: false,
            image: image,
            customer: customer
        })
    }

    const onPressNoneeded = () => {
        setsubContractor(subContractorReducer)
        setNeeded(true)

    }
    const handleNext = () => {
        navigation.navigate('Databank', {
            project: project,
            TeamDetails: team,
            is_sub_contractor:needed,
            subContractorReducer: Checkedrights,
            image: image,
            customer: customer
        })
    }

    return (
        <SafeAreaView>
            <View style={styles.container}>
                <NewProjectHeader {...props} title='Sub-Contractor Details' no='4' percents={66.66} />
                <ScrollView>
                    <View style={styles.parent2}>
                        <View style={styles.textInput1}>
                            {needed ?
                                <TouchableOpacity
                                    onPress={() => { onPressNeeded() }}>
                                    <FastImage
                                        style={styles.image_logo}
                                        source={require('../../assets/icons/Uncheck.png')
                                        }
                                        resizeMode={FastImage.resizeMode.contain}
                                    />
                                </TouchableOpacity>
                                :
                                <TouchableOpacity
                                    onPress={() => { onPressNoneeded() }}>
                                    <FastImage
                                        style={styles.image_logo}
                                        source={require('../../assets/icons/Check.png')
                                        }
                                        resizeMode={FastImage.resizeMode.contain}
                                    />
                                </TouchableOpacity>}

                            <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>No Sub-Contractor info needed</Text>
                        </View>
                        <SearchBar
                            showLoading={false}
                            platform={Platform.OS}
                            containerStyle={{ borderWidth: 1, borderRadius: 4, borderColor: 'rgb(232, 232, 232)', backgroundColor: 'white', justifyContent: 'center', width: wp('80%'), height: hp('6%') }}
                            clearIcon={true}
                            onChangeText={onChangeSearch}
                            onClearText={() => null}
                            placeholder=''
                            cancelButtonTitle='Cancel'
                        />
                        <View style={{ marginTop: 20, marginBottom: 10, height: hp('6%'), display: 'flex', flexDirection: 'row', justifyContent: 'center', alignSelf: 'center' }}>

                            <TouchableOpacity
                                onPress={() => { onPressStock() }}
                                style={[styles.btn, { backgroundColor: onStockButtonPress ? '#3d87f1' : 'white' }]}
                            >
                                <Text style={[styles.btnText, { color: onStockButtonPress ? 'white' : 'rgb(143, 143, 143)' }]}>All Sub-Contractors</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={() => { onPressConsumed() }}

                                style={[styles.btn, { backgroundColor: onConsumedButtonPress ? '#3d87f1' : 'white', marginLeft: -5 }]}
                            >
                                <Text style={[styles.btnText, { color: onConsumedButtonPress ? 'white' : 'rgb(143, 143, 143)' }]}>Selected</Text>
                            </TouchableOpacity>
                            <Badge
                                status="primary"
                                value={Checkedteam.length}
                                containerStyle={{ position: 'absolute', top: -4, right: -4 }}
                            />
                        </View>
                        {onStockButtonPress ?
                            <>
                                <FlatList
                                    data={subContractor}
                                    renderItem={
                                        ({ item, i }) => (
                                            <View style={styles.card}>
                                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                    <View>
                                                        <Text style={styles.name_text}>{item.company_name}</Text>
                                                         <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>{item.company_name}</Text> 
                                                    </View>
                                                    <View>
                                                        <TouchableOpacity
                                                            onPress={() => { onPressChecked(item, i, rights) }}>
                                                            <FastImage
                                                                style={styles.image_logo}
                                                                source={require('../../assets/icons/Uncheck.png')
                                                                }
                                                                resizeMode={FastImage.resizeMode.contain}
                                                            />
                                                        </TouchableOpacity>
                                                    </View>
                                                </View>

                                                <Text style={{ height: hp('0.2%'), marginTop: 25, marginBottom: 8, backgroundColor: 'rgb(232, 232, 232)' }}></Text>
                                                <View style={{ display: 'flex', marginBottom: 10, flexDirection: 'row', alignItems: 'center' }}>
                                                    <View style={{ backgroundColor: 'rgb(232,232,232)' }}>
                                                        <View style={{ width: 150, height: 40, color: 'rgb(143,143,143)', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', fontSize: 12 }}>
                                                            <Select
                                                                key={i + item.id}
                                                                style={styles.select}
                                                                multiSelect={true}
                                                                placeholder="Select Rights"
                                                                value="Select Rights"
                                                                selectedIndex={rights}
                                                                onSelect={index => setrights(index)}>
                                                                {data.map(renderOption)}
                                                            </Select>
                                                            </View>
                                                           

                                                    </View>
                                                    <View style={{ backgroundColor: 'rgb(232,232,232)', marginLeft: 15, width: wp('11%'), height: hp('5%'), padding: 4 }}>
                                                        < FastImage
                                                            style={{ width: wp('8%'), height: hp('3%'), marginTop: 3 }}
                                                            source={require('../../assets/icons/info.png')}
                                                            resizeMode={FastImage.resizeMode.contain}
                                                        />
                                                    </View>
                                                </View>

                                            </View>
                                        )
                                    }
                                />

                            </> :
                            <>
                                {Checkedteam != '' ?
                                    <>
                                        <FlatList
                                            data={Checkedteam}
                                            renderItem={
                                                ({ item, i }) => (
                                                    <View style={styles.card}>
                                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                            <View>
                                                                <Text style={styles.name_text}>{item.company_name}</Text>
                                                                <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>{item.department}</Text>
                                                            </View>
                                                            <View>
                                                                <TouchableOpacity
                                                                    onPress={() => { onPressUnchecked(item, i) }}>
                                                                    <FastImage
                                                                        style={styles.image_logo}
                                                                        source={require('../../assets/icons/Check.png')
                                                                        }
                                                                        resizeMode={FastImage.resizeMode.contain}
                                                                    />
                                                                </TouchableOpacity>
                                                            </View>
                                                        </View>
                                                        <Text style={{ height: hp('0.2%'), marginTop: 25, marginBottom: 8, backgroundColor: 'rgb(232, 232, 232)' }}></Text>
                                                        <View style={{ display: 'flex', marginBottom: 10, flexDirection: 'row', alignItems: 'center' }}>
                                                            <View>
                                                                <Text style={{ color: 'rgb(68, 67, 67)', fontFamily: 'SourceSansPro-Regular', fontSize: 12 }}>Email</Text>
                                                                <Text style={{ color: 'rgb(143, 143, 143)', fontFamily: 'SourceSansPro-Regular', fontSize: 12 }}>{item.user_name}</Text>
                                                            </View>
                                                        </View>
                                                    </View>
                                                )
                                            }
                                        />
                                    </>
                                    :
                                    <>
                                        <View style={{ width: wp('80%'), height: hp('50%'), marginTop: 3, alignItems: 'center' }}>
                                            <View >
                                                < FastImage
                                                    style={{ width: wp('80%'), height: hp('50%'), marginTop: 3 }}
                                                    source={require('../../assets/icons/NoTeam.png')}
                                                    resizeMode={FastImage.resizeMode.contain}
                                                />
                                            </View>
                                            <View>
                                                <Text style={{ fontFamily: 'SourceSansPro-Regular', fontSize: 20, color: 'rgb(143,143,143)' }}>No Sub-Contractor Selected!</Text>
                                            </View>
                                            <View style={{ padding: 20, alignItems: 'center' }}>
                                                <Text style={{ fontFamily: 'SourceSansPro-Regular', fontSize: 14, color: 'rgb(190,198,214)' }}>Please select a new Sub-Contractor

                                                </Text><Text style={{ fontFamily: 'SourceSansPro-Regular', fontSize: 14, color: 'rgb(190,198,214)' }}>with designated rights </Text>
                                            </View>
                                        </View>
                                    </>

                                }
                            </>

                        }

                    </View>
                </ScrollView>
                <View style={[styles.shadowContainerStyle1, {
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: hp('15%'),
                    width: wp('150%'),
                }]}>
                    <TouchableOpacity
                        onPress={() => navigation.goBack()}>
                        <View style={{ borderColor: 'rgb(61,135,241)', justifyContent: 'center', alignItems: 'center', borderWidth: 1, marginTop: -20, borderRadius: 5, height: hp('7'), width: wp('30%') }}>
                            <Text style={{ color: 'rgb(61,135,241)' }}>Previous</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() =>handleNext() }
                    >
                        <View style={{ backgroundColor: 'rgb(61,135,241)', marginLeft: 10, justifyContent: 'center', alignItems: 'center', marginTop: 1 - 20, borderRadius: 5, height: hp('7'), width: wp('30%') }}>
                            <Text style={{ color: '#fff' }}>Next</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Text style={{ textDecorationLine: 'underline', color: 'rgb(61,135,241)', marginLeft: 10, marginTop: 1 - 20, fontFamily: 'SourceSansPro-Regular' }}>Save for later</Text>
                    </TouchableOpacity>

                </View>
            </View>

        </SafeAreaView>

    )
};
const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
    shadowContainerStyle1: {   //<--- Style with elevation

        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 510 },
        shadowOpacity: 20,
        shadowRadius: 300,
        elevation: 3,
    },
    card: {   //<--- Style with elevation
        borderLeftColor: 'rgb(61,135,241)',
        borderLeftWidth: 5,
        borderRadius: 5,
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        elevation: 3,
        width: wp('90%'),
        margin: 10,
        height: 160,
        padding: 20
        //  justifyContent: 'center',
        //  alignItems:'center'
    },
    select: {
        // color: 'rgb(232,232,232)',
        margin: 2,
        width:160,
      },
    image_logo: {
        width: wp('7%'),
        height: hp('3.5%'),
    },
    parent2: {
        // height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
        flexDirection: 'row',
        marginTop: hp('3%'),
        // justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        backgroundColor: 'white',
        flexDirection: 'column'
    },
    option_text: {
        fontSize: hp('1.2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center'
    },
    signIn: {
        width: wp('74%'),
        height: hp('8%'),
        left: 25,
        top: 80,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        color: '#05375a',
        backgroundColor: '#3A80E3',
        borderRadius: 5,
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: 'SourceSansPro-Regular',
    },
    name_text: {
        fontSize: 16,
        fontWeight: 'bold',
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68,67,67)'
    },
    temail_textxt: {
        fontSize: 14,
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(143,143,143)'
    },
    txt: {
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(143,143,143)'
    },
    txtunder: {
        fontSize: hp('2.5%'),
        textDecorationLine: 'underline',
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(61,135,241)'
    },
    textblue: {
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(61,135,241)'
    },
    textInput1: {
        // marginTop: hp('3%'),
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        width: wp('79.4%'),
        height: hp('7.4%'),
        borderRadius: 4,
        fontSize: 16,
        fontFamily: 'SourceSansPro-Regular',
        color: 'black',
        textAlign: 'left',
        paddingHorizontal: 10,
        marginBottom: Platform.OS === 'ios' ? 6 : 8
    },
    textInput: {
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        width: wp('79.4%'),
        height: hp('7.4%'),
        borderRadius: 4,
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'black',
        textAlign: 'left',
        paddingHorizontal: 10,
        marginBottom: Platform.OS === 'ios' ? 6 : 8
    },
    btnText: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular'
    },

    btn: {
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        width: wp('40%'),
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center'
    },

});
export default SubContractorDetails