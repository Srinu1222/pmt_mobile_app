import React, { useEffect, useState } from 'react';
import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    PermissionsAndroid,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    Picker,
    NativeComponent
} from 'react-native';
import { Searchbar, Button, Switch } from 'react-native-paper';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import * as actionCreator from '../../actions/gantt-stage-action/index';
import * as projectactionCreators from '../../actions/project-action/index';
import { useDispatch, useSelector } from 'react-redux';
import { FlatList } from 'react-native-gesture-handler';
import FastImage from 'react-native-fast-image'
import NewProjectHeader from '../EmptyScreens/NewProjectHeader';
import { SearchBar, Divider } from 'react-native-elements';
import { RadioButton } from 'react-native-paper';
import { CheckBox } from '@ui-kitten/components';

import CheckboxList from 'rn-checkbox-list';
import { types_data, drawings_data, components_data, accessories_data, area_data } from './json1';
const ProjectSpecification1 = ({ props, route, navigation }) => {
    const { project, image, customer, TeamDetails, subContractorReducer, is_sub_contractor, purchase_order_file, escalation_matrix_file, electricity_bill_file, bom_file, site_checklist_file, autocad_file, electrical_SLD_file,token } = route.params;
    const dispatch = useDispatch();
    const onChangeSearch = query => setSearchQuery(query);
    // const [checked, setChecked] = React.useState(false);
    const [gantt, setGantt] = React.useState(true);
    const [saveproject, setsaveproject] = React.useState(false);

    const [selected_project_types, setselected_project_types] = React.useState([]);
    const [selected_drawings, setselected_drawings] = React.useState([]);
    const [selected_components, setselected_components] = React.useState([]);
    const [selected_accessories, setselected_accessories] = React.useState([]);
    const [selected_area_of_installation, setselected_area_of_installation] = React.useState([]);

    const [selected_termination, setselected_termination] = React.useState([]);
    const [selected_inverter_to_ACDB, setselected_inverter_to_ACDB] = React.useState([]);
    const [selected_ACDB_to_Transformer, setselected_ACDB_to_Transformer] = React.useState([]);
    const [selected_Transformer_to_HTPanel, setselected_Transformer_to_HTPanel] = React.useState([]);

    const [typesactive, settypesactive] = React.useState(true);
    const [drawingsactive, setdrawingsactive] = React.useState(true);
    const [componentsactive, setcomponentsactive] = React.useState(true);
    const [accessoriesactive, setaccessoriesactive] = React.useState(true);
    const [areaactive, setareaactive] = React.useState(true);

    const [terminationactive, setterminationactive] = React.useState(true);
    const [terminationdetailsactive, setterminationdetailsactive] = React.useState(true);

    const [remark, setremark] = React.useState(true);

    const [onStockButtonPress, setonStockButtonPress] = React.useState(true);
    const [onConsumedButtonPress, setonConsumedButtonPress] = React.useState(false);
    const [checked, setChecked] = React.useState([]);




    const [Rooftop, setRooftop] = useState(false);
    const [Open_Access, setOpen_Access] = useState(false);
    const [Ground_Mounted, setGround_Mounted] = useState(false);
    const [Industrial, setIndustrial] = useState(false);
    const [Commercial, setCommercial] = useState(false);
    const [Residential, setResidential] = useState(false);


    const [Array_Layout, setArray_Layout] = useState(false);
    const [Civil_Layout, setCivil_Layout] = useState(false);
    const [PV_Syst, setPV_Syst] = useState(false);
    const [Communication, setCommunication] = useState(false);
    const [Equipment_Layout, setEquipment_Layout] = useState(false);
    const [Walkways, setWalkways] = useState(false);
    const [Earthing_LA, setEarthing_LA] = useState(false);
    const [Safety_Line, setSafety_Line] = useState(false);
    const [Cable_Layout, setCable_Layout] = useState(false);
    const [Safety_Rails, setSafety_Rails] = useState(false);
    const [Structure_Drawings_RCC, setStructure_Drawings_RCC] = useState(false);
    const [Project_SLD, setProject_SLD] = useState(false);
    const [Structure_Drawings_Metal, setStructure_Drawings_Metal] = useState(false);
    const [Final_BOQ, setFinal_BOQ] = useState(false);
    const [Structure_Drawings_Ground, setStructure_Drawings_Ground] = useState(false);
    const [Detailed_Project_Report, setDetailed_Project_Report] = useState(false);




    const [Solar_PV_Module, setSolar_PV_Module] = useState(false);
    const [Module_Metal_Sheet, setModule_Metal_Sheet] = useState(false);
    const [Solar_Inverter, setSolar_Inverter] = useState(false);
    const [Module_Ground_Mount, setModule_Ground_Mount] = useState(false);
    const [Module_RCC, setModule_RCC] = useState(false);
    const [Structure_Foundation_Pedestal, setStructure_Foundation_Pedestal] = useState(false);
    const [Cabling_Accessories, setCabling_Accessories] = useState(false);
    const [Earthing_System, setEarthing_System] = useState(false);
    const [Lighting_Arrestor, setLighting_Arrestor] = useState(false);
    const [LT_Panel, setLT_Panel] = useState(false);
    const [HT_Panel, setHT_Panel] = useState(false);
    const [AC_Distribution, setAC_Distribution] = useState(false);
    const [Transformer, setTransformer] = useState(false);
    const [LT_From_inverters, setLT_From_inverters] = useState(false);
    const [LT_From_ACDB, setLT_From_ACDB] = useState(false);
    const [LT_From_metering, setLT_From_metering] = useState(false);
    const [Earthing_Cable, setEarthing_Cable] = useState(false);
    const [Module_Earthing_Cable, setModule_Earthing_Cable] = useState(false);
    const [DC_Junction_Box, setDC_Junction_Box] = useState(false);
    const [GI_Strip, setGI_Strip] = useState(false);
    const [Metering_Panel, setMetering_Panel] = useState(false);
    const [MDB_Breaker, setMDB_Breaker] = useState(false);
    const [Cable_trays, setCable_trays] = useState(false);
    const [HT_Breaker, setHT_Breaker] = useState(false);
    const [Bus_bar, setBus_bar] = useState(false);
    const [InC_Contractor, setInC_Contractor] = useState(false);




    const [Reverse_Protection_Relay, setReverse_Protection_Relay] = useState(false);
    const [Net_Metering, setNet_Metering] = useState(false);
    const [DG_Synchronization, setDG_Synchronization] = useState(false);
    const [Communication_Cable, setCommunication_Cable] = useState(false);
    const [ASafety_Rails, setASafety_Rails] = useState(false);
    const [AWalkways, setAWalkways] = useState(false);
    const [Safety_Lines, setSafety_Lines] = useState(false);
    const [SCADA_System, setSCADA_System] = useState(false);
    const [Pyro_Meter, setPyro_Meter] = useState(false);
    const [Weather_Monitoring_Unit, setWeather_Monitoring_Unit] = useState(false);
    const [Ambient_Temperature_Sensors, setAmbient_Temperature_Sensors] = useState(false);
    const [UPS, setUPS] = useState(false);
    const [Module_Temperature_Sensors, setModule_Temperature_Sensors] = useState(false);
    const [Wire_mesh_for_protection, setWire_mesh_for_protection] = useState(false);
    const [Cleaning_System, setCleaning_System] = useState(false);
    const [Danger_Board_and_Signs, setDanger_Board_and_Signs] = useState(false);
    const [Fire_Extingusiher, setFire_Extingusiher] = useState(false);
    const [Generation_Meter, setGeneration_Meter] = useState(false);
    const [Zero_Export_Device, setZero_Export_Device] = useState(false);
    const [Other_Sensors, setOther_Sensors] = useState(false);
    const [Control_Room, setControl_Room] = useState(false);


    const [Ground_Mount, setGround_Mount] = useState(false);
    const [RCC, setRCC] = useState(false);
    const [Metal_Sheet, setMetal_Sheet] = useState(false);

    const [termination, setTermination] = useState('LT');
    const [InvertertoACDB, setInvertertoACDB] = useState('Cable Tray');
    const [ACDBtoLTPanel, setACDBtoLTPanel] = useState('Cable Tray');
    const [TransformertoHTPanel, setTransformertoHTPanel] = useState('Cable Tray');


    const onChecked = (id) => {
        setChecked(id)
        dispatch(actionCreator.getSampleGanttSpecifications(id, token))

    }

    const postProjectReducers = useSelector(state => {
        return state.projectReducers.projects.post;
    });


    const ganttDetails = useSelector((state) => {
        return state.getSampleGanttReducers.samplegantts?.get?.success?.data?.gantt_list;
    });
    const sampleGanttDetailsReducer = useSelector(state => {
        return state.sampleGanttDetailsReducers.sampleganttdetails.get;
    });

    const ganttspac = sampleGanttDetailsReducer.success?.data?.project_specification
    const onGanttSelectHandler = () => {
        //selected_project_types
        { ganttspac?.selected_project_types.includes("Rooftop") ? setRooftop(true) : setRooftop(false) }
        { ganttspac?.selected_project_types.includes("Open Access") ? setOpen_Access(true) : setOpen_Access(false) }
        { ganttspac?.selected_project_types.includes("Ground Mounted") ? setGround_Mounted(true) : setGround_Mounted(false) }
        { ganttspac?.selected_project_types.includes("Industrial") ? setIndustrial(true) : setIndustrial(false) }
        { ganttspac?.selected_project_types.includes("Commercial") ? setCommercial(true) : setCommercial(false) }
        { ganttspac?.selected_project_types.includes("Residential") ? setResidential(true) : setResidential(false) }

        //selected_drawings

        { ganttspac?.selected_drawings.includes("Array Layout") ? setArray_Layout(true) : setArray_Layout(false) }
        { ganttspac?.selected_drawings.includes("Civil Layout") ? setCivil_Layout(true) : setCivil_Layout(false) }
        { ganttspac?.selected_drawings.includes("PV-Syst") ? setPV_Syst(true) : setPV_Syst(false) }
        { ganttspac?.selected_drawings.includes("Communication System Layout") ? setCommunication(true) : setCommunication(false) }
        { ganttspac?.selected_drawings.includes("Equipment Layout") ? setEquipment_Layout(true) : setEquipment_Layout(false) }
        { ganttspac?.selected_drawings.includes("Walkways") ? setWalkways(true) : setWalkways(false) }

        { ganttspac?.selected_drawings.includes("Earthing and LA Layout") ? setEarthing_LA(true) : setEarthing_LA(false) }
        { ganttspac?.selected_drawings.includes("Safety Line") ? setSafety_Line(true) : setSafety_Line(false) }
        { ganttspac?.selected_drawings.includes("Cable Layout") ? setCable_Layout(true) : setCable_Layout(false) }
        { ganttspac?.selected_drawings.includes("Safety Rails") ? setSafety_Rails(true) : setSafety_Rails(false) }
        { ganttspac?.selected_drawings.includes("Structure Drawings_RCC") ? setStructure_Drawings_RCC(true) : setStructure_Drawings_RCC(false) }

        { ganttspac?.selected_drawings.includes("Project SLD") ? setProject_SLD(true) : setProject_SLD(false) }
        { ganttspac?.selected_drawings.includes("Structure Drawings_Metal Sheet") ? setStructure_Drawings_Metal(true) : setStructure_Drawings_Metal(false) }
        { ganttspac?.selected_drawings.includes("Final BOQ") ? setFinal_BOQ(true) : setFinal_BOQ(false) }
        { ganttspac?.selected_drawings.includes("Structure Drawings_Ground Mount") ? setStructure_Drawings_Ground(true) : setStructure_Drawings_Ground(false) }
        { ganttspac?.selected_drawings.includes("Detailed Project Report") ? setDetailed_Project_Report(true) : setDetailed_Project_Report(false) }

        //selected_components
        { ganttspac?.selected_components.includes("Solar PV Module") ? setSolar_PV_Module(true) : setSolar_PV_Module(false) }
        { ganttspac?.selected_components.includes("Module Mounting Structures with Accessories _ Metal Sheet") ? setModule_Metal_Sheet(true) : setModule_Metal_Sheet(false) }
        { ganttspac?.selected_components.includes("Solar Inverter") ? setSolar_Inverter(true) : setSolar_Inverter(false) }
        { ganttspac?.selected_components.includes("Module Mounting Structures with Accessories _ Ground Mount") ? setModule_Ground_Mount(true) : setModule_Ground_Mount(false) }
        { ganttspac?.selected_components.includes("Module Mounting Structures with Accessories _ RCC") ? setModule_RCC(true) : setModule_RCC(false) }
        { ganttspac?.selected_components.includes("Structure Foundation Pedestal") ? setStructure_Foundation_Pedestal(true) : setStructure_Foundation_Pedestal(false) }

        { ganttspac?.selected_components.includes("Cabling Accessories") ? setCabling_Accessories(true) : setCabling_Accessories(false) }
        { ganttspac?.selected_components.includes("Earthing System") ? setEarthing_System(true) : setEarthing_System(false) }
        { ganttspac?.selected_components.includes("Lighting Arrestor") ? setLighting_Arrestor(true) : setLighting_Arrestor(false) }
        { ganttspac?.selected_components.includes("LT Panel") ? setLT_Panel(true) : setLT_Panel(false) }
        { ganttspac?.selected_components.includes("HT Panel") ? setHT_Panel(true) : setHT_Panel(false) }

        { ganttspac?.selected_components.includes("AC Distribution (Combiner) Panel Board") ? setAC_Distribution(true) : setAC_Distribution(false) }
        { ganttspac?.selected_components.includes("Transformer") ? setTransformer(true) : setTransformer(false) }
        { ganttspac?.selected_components.includes("LT Power Cable (From inverters to ACDB)") ? setLT_From_inverters(true) : setLT_From_inverters(false) }
        { ganttspac?.selected_components.includes("LT Power Cable (From ACDB to Customer LT Panel)") ? setLT_From_ACDB(true) : setLT_From_ACDB(false) }
        { ganttspac?.selected_components.includes("LT Power Cable (From metering cubical to MDB panel)") ? setLT_From_metering(true) : setLT_From_metering(false) }

        { ganttspac?.selected_components.includes("Earthing Cable") ? setEarthing_Cable(true) : setEarthing_Cable(false) }
        { ganttspac?.selected_components.includes("Module Earthing Cable") ? setModule_Earthing_Cable(true) : setModule_Earthing_Cable(false) }
        { ganttspac?.selected_components.includes("DC Junction Box (SCB/SMB)") ? setDC_Junction_Box(true) : setDC_Junction_Box(false) }
        { ganttspac?.selected_components.includes("GI Strip") ? setGI_Strip(true) : setGI_Strip(false) }
        { ganttspac?.selected_components.includes("Metering Panel") ? setMetering_Panel(true) : setMetering_Panel(false) }

        { ganttspac?.selected_components.includes("MDB Breaker") ? setMDB_Breaker(true) : setMDB_Breaker(false) }
        { ganttspac?.selected_components.includes("Cable trays") ? setCable_trays(true) : setCable_trays(false) }
        { ganttspac?.selected_components.includes("HT Breaker") ? setHT_Breaker(true) : setHT_Breaker(false) }
        { ganttspac?.selected_components.includes("Bus bar") ? setBus_bar(true) : setBus_bar(false) }
        { ganttspac?.selected_components.includes("InC Contractor") ? setInC_Contractor(true) : setInC_Contractor(false) }


        //selected_accessories

        { ganttspac?.selected_accessories.includes("Reverse Protection Relay") ? setReverse_Protection_Relay(true) : setReverse_Protection_Relay(false) }
        { ganttspac?.selected_accessories.includes("Net-Metering") ? setNet_Metering(true) : setNet_Metering(false) }
        { ganttspac?.selected_accessories.includes("DG Synchronization") ? setDG_Synchronization(true) : setDG_Synchronization(false) }
        { ganttspac?.selected_accessories.includes("Communication Cable") ? setCommunication_Cable(true) : setCommunication_Cable(false) }
        { ganttspac?.selected_accessories.includes("Safety Rails") ? setASafety_Rails(true) : setASafety_Rails(false) }
        { ganttspac?.selected_accessories.includes("Walkways") ? setAWalkways(true) : setAWalkways(false) }

        { ganttspac?.selected_accessories.includes("Safety Lines") ? setSafety_Lines(true) : setSafety_Lines(false) }
        { ganttspac?.selected_accessories.includes("SCADA System") ? setSCADA_System(true) : setSCADA_System(false) }
        { ganttspac?.selected_accessories.includes("Pyro Meter") ? setPyro_Meter(true) : setPyro_Meter(false) }
        { ganttspac?.selected_accessories.includes("Weather Monitoring Unit") ? setWeather_Monitoring_Unit(true) : setWeather_Monitoring_Unit(false) }
        { ganttspac?.selected_accessories.includes("Ambient Temperature Sensors") ? setAmbient_Temperature_Sensors(true) : setAmbient_Temperature_Sensors(false) }

        { ganttspac?.selected_accessories.includes("UPS") ? setUPS(true) : setUPS(false) }
        { ganttspac?.selected_accessories.includes("Module Temperature Sensors") ? setModule_Temperature_Sensors(true) : setModule_Temperature_Sensors(false) }
        { ganttspac?.selected_accessories.includes("Wire mesh for protection of Skylights") ? setWire_mesh_for_protection(true) : setWire_mesh_for_protection(false) }
        { ganttspac?.selected_accessories.includes("Cleaning System") ? setCleaning_System(true) : setCleaning_System(false) }
        { ganttspac?.selected_accessories.includes("Danger Board and Signs") ? setDanger_Board_and_Signs(true) : setDanger_Board_and_Signs(false) }

        { ganttspac?.selected_accessories.includes("Fire Extingusiher") ? setFire_Extingusiher(true) : setFire_Extingusiher(false) }
        { ganttspac?.selected_accessories.includes("Generation Meter") ? setGeneration_Meter(true) : setGeneration_Meter(false) }
        { ganttspac?.selected_accessories.includes("Zero Export Device") ? setZero_Export_Device(true) : setZero_Export_Device(false) }
        { ganttspac?.selected_accessories.includes("Other Sensors") ? setOther_Sensors(true) : setOther_Sensors(false) }
        { ganttspac?.selected_accessories.includes("Control Room") ? setControl_Room(true) : setControl_Room(false) }

        //ganttspac?.selected_area_of_installation

        { ganttspac?.selected_area_of_installation.includes("Ground Mount") ? setGround_Mount(true) : setGround_Mount(false) }
        { ganttspac?.selected_area_of_installation.includes("RCC") ? setRCC(true) : setRCC(false) }
        { ganttspac?.selected_area_of_installation.includes("Metal Sheet") ? setMetal_Sheet(true) : setMetal_Sheet(false) }



    }

    useEffect(() => {
        dispatch(actionCreator.getSampleGantts(token));
    }, []);

    const onsubmit = () => {
        Rooftop ? selected_project_types.push("Rooftop") : null;
        Open_Access ? selected_project_types.push("Open Access") : null;
        Ground_Mounted ? selected_project_types.push("Ground Mounted") : null;
        Industrial ? selected_project_types.push("Industrial") : null;
        Commercial ? selected_project_types.push("Commercial") : null;
        Residential ? selected_project_types.push("Residential") : null;
        //drawings
        Array_Layout ? selected_drawings.push("Array Layout") : null;
        Civil_Layout ? selected_drawings.push("Civil Layout") : null;
        PV_Syst ? selected_drawings.push("PV-Syst") : null;
        Communication ? selected_drawings.push("Communication System Layout") : null;
        Equipment_Layout ? selected_drawings.push("Equipment Layout") : null;
        Walkways ? selected_drawings.push("Walkways") : null;

        Earthing_LA ? selected_drawings.push("Earthing and LA Layout") : null;
        Safety_Line ? selected_drawings.push("Safety Line") : null;
        Cable_Layout ? selected_drawings.push("Cable Layout") : null;
        Safety_Rails ? selected_drawings.push("Safety Rails") : null;
        Structure_Drawings_RCC ? selected_drawings.push("Structure Drawings_RCC") : null;

        Project_SLD ? selected_drawings.push("Project SLD") : null;
        Structure_Drawings_Metal ? selected_drawings.push("Structure Drawings_Metal Sheet") : null;
        Final_BOQ ? selected_drawings.push("Final BOQ") : null;
        Structure_Drawings_Ground ? selected_drawings.push("Structure Drawings_Ground Mount") : null;
        Detailed_Project_Report ? selected_drawings.push("Detailed Project Report") : null;

        //components

        Solar_PV_Module ? selected_components.push("Solar PV Module") : null;
        Module_Metal_Sheet ? selected_components.push("Module Mounting Structures with Accessories _ Metal Sheet") : null;
        Solar_Inverter ? selected_components.push("Solar Inverter") : null;
        Module_Ground_Mount ? selected_components.push("Module Mounting Structures with Accessories _ Ground Mount") : null;
        Module_RCC ? selected_components.push("Module Mounting Structures with Accessories _ RCC") : null;
        Structure_Foundation_Pedestal ? selected_components.push("Structure Foundation Pedestal") : null;

        Cabling_Accessories ? selected_components.push("Cabling Accessories") : null;
        Earthing_System ? selected_components.push("Earthing System") : null;
        Lighting_Arrestor ? selected_components.push("Lighting Arrestor") : null;
        LT_Panel ? selected_components.push("LT Panel") : null;
        HT_Panel ? selected_components.push("HT Panel") : null;

        AC_Distribution ? selected_components.push("AC Distribution (Combiner) Panel Board") : null;
        Transformer ? selected_components.push("Transformer") : null;
        LT_From_inverters ? selected_components.push("LT Power Cable (From inverters to ACDB)") : null;
        LT_From_ACDB ? selected_components.push("LT Power Cable (From ACDB to Customer LT Panel)") : null;
        LT_From_metering ? selected_components.push("LT Power Cable (From metering cubical to MDB panel)") : null;


        Earthing_Cable ? selected_components.push("Earthing Cable") : null;
        Module_Earthing_Cable ? selected_components.push("Module Earthing Cable") : null;
        DC_Junction_Box ? selected_components.push("DC Junction Box (SCB/SMB)") : null;
        GI_Strip ? selected_components.push("GI Strip") : null;
        Metering_Panel ? selected_components.push("Metering Panel") : null;

        MDB_Breaker ? selected_components.push("MDB Breaker") : null;
        Cable_trays ? selected_components.push("Cable trays") : null;
        HT_Breaker ? selected_components.push("HT Breaker") : null;
        Bus_bar ? selected_components.push("Bus bar") : null;
        InC_Contractor ? selected_components.push("InC Contractor") : null;


        //accessories
        Reverse_Protection_Relay ? selected_accessories.push("Reverse Protection Relay") : null;
        Net_Metering ? selected_accessories.push("Net-Metering") : null;
        DG_Synchronization ? selected_accessories.push("DG Synchronization") : null;
        Communication_Cable ? selected_accessories.push("Communication Cable") : null;
        ASafety_Rails ? selected_accessories.push("Safety Rails") : null;
        AWalkways ? selected_accessories.push("Walkways") : null;

        Safety_Lines ? selected_accessories.push("Safety Lines") : null;
        SCADA_System ? selected_accessories.push("SCADA System") : null;
        Pyro_Meter ? selected_accessories.push("Pyro Meter") : null;
        Weather_Monitoring_Unit ? selected_accessories.push("Weather Monitoring Unit") : null;
        Ambient_Temperature_Sensors ? selected_accessories.push("Ambient Temperature Sensors") : null;

        UPS ? selected_accessories.push("UPS") : null;
        Module_Temperature_Sensors ? selected_accessories.push("Module Temperature Sensors") : null;
        Wire_mesh_for_protection ? selected_accessories.push("Wire mesh for protection of Skylights") : null;
        Cleaning_System ? selected_accessories.push("Cleaning System") : null;
        Danger_Board_and_Signs ? selected_accessories.push("Danger Board and Signs") : null;


        Fire_Extingusiher ? selected_accessories.push("Fire Extingusiher") : null;
        Generation_Meter ? selected_accessories.push("Generation Meter") : null;
        Zero_Export_Device ? selected_accessories.push("Zero Export Device") : null;
        Other_Sensors ? selected_accessories.push("Other Sensors") : null;
        Control_Room ? selected_accessories.push("Control Room") : null;

        //area
        Ground_Mount ? selected_area_of_installation.push("Ground Mount") : null;
        RCC ? selected_area_of_installation.push("RCC") : null;
        Metal_Sheet ? selected_area_of_installation.push("Metal Sheet") : null;

        termination ? selected_termination.push(termination) : null;
        InvertertoACDB ? selected_inverter_to_ACDB.push(InvertertoACDB) : null;
        ACDBtoLTPanel ? selected_ACDB_to_Transformer.push(ACDBtoLTPanel) : null;
        TransformertoHTPanel ? selected_Transformer_to_HTPanel.push(TransformertoHTPanel) : null;


        let projectSpecification = {
            selected_project_types: selected_project_types,
            selected_drawings: selected_drawings,
            selected_accessories: selected_accessories,
            selected_area_of_installation: selected_area_of_installation,
            selected_termination: selected_termination,
            selected_components: selected_components,
            selected_inverter_to_ACDB: selected_inverter_to_ACDB,
            selected_ACDB_to_Transformer: selected_ACDB_to_Transformer,
            selected_Transformer_to_HT_Panel: selected_Transformer_to_HTPanel
        }

        const formData = new FormData();
        formData.append('customer', JSON.stringify(customer));
        formData.append('purchase_order_file', purchase_order_file);
        formData.append('electricity_bill_file', electricity_bill_file);
        formData.append('autocad_file', autocad_file);
        formData.append('electrical_SLD_file', electrical_SLD_file);
        formData.append('bom_file', bom_file);
        formData.append('site_checklist_file', site_checklist_file);
        formData.append('escalation_matrix_file', escalation_matrix_file);
        formData.append('project_image', image);
        formData.append('project', JSON.stringify(project));
        formData.append('team', JSON.stringify(TeamDetails));
        formData.append('sub_contractor_details', JSON.stringify(subContractorReducer))
        formData.append('project_specification', JSON.stringify(projectSpecification));


     
        dispatch(projectactionCreators.postProjects(formData,token));
        setsaveproject(true)
    }
    const postProject = () => {
        if (postProjectReducers.success.ok) {
            //'projectId', postProjectReducers.success.data.project_id);
            navigation.navigate('project_planning1', { project_id: postProjectReducers.success.data.project_id , is_sub_contractor: is_sub_contractor })
                                                                 
        } else if (postProjectReducers.failure.error) {
            //postProjectReducers.failure.error);
        }
    }


    const onPressStock = (params) => {
        setonStockButtonPress(true)
        setonConsumedButtonPress(false)
    }

    const onPressConsumed = (params) => {
        setonConsumedButtonPress(true)
        setonStockButtonPress(false)
    }

    const onPressOpen = () => {
        setGantt(false)
    }
    const onPressClose = () => {
        setGantt(true)
    }


    // let type1=JSON.stringify(ganttspac.selected_drawings)

    // //"ganttspac.selected_project_types",types)
    // //"drawings///",drawings)


    return (
        <SafeAreaView>
            <View style={styles.container}>
                <NewProjectHeader {...props} title='Project Specification' no='6' percents={100} />
                <ScrollView>

                    <View style={styles.parent2}>
                        <View style={styles.textInput1}>
                            <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>Use your Exisiting Gantts</Text>
                            <Divider orientation="horizontal" height={20} width={1} />
                            {gantt ?
                                <TouchableOpacity onPress={() => { onPressOpen() }}>
                                    <Text style={{
                                        fontSize: hp('2.5%'),
                                        textDecorationLine: 'underline',
                                        fontFamily: 'SourceSansPro-Regular',
                                        color: 'rgb(61,135,241)'
                                    }}>OPEN</Text>
                                </TouchableOpacity> :
                                <TouchableOpacity onPress={() => { onPressClose() }}>
                                    <Text style={{
                                        fontSize: hp('2.5%'),
                                        textDecorationLine: 'underline',
                                        fontFamily: 'SourceSansPro-Regular',
                                        color: 'rgb(61,135,241)'
                                    }}>CLOSE</Text>
                                </TouchableOpacity>}
                        </View>
                        {gantt ?
                            <>
                            </> :
                            <View style={{ height: hp('69%') }}>
                                <FlatList
                                    horizontal
                                    data={ganttDetails}
                                    showsHorizontalScrollIndicator={false}
                                    renderItem={
                                        ({ item, index }) => (
                                            <View style={{ marginLeft: 10, marginRight: 10 }}>
                                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                                                    <RadioButton
                                                        value={checked}
                                                        status={checked === item.gantt_id ? 'checked' : 'unchecked'}
                                                        onPress={() => onChecked(item.gantt_id)}
                                                        color={'rgb(61,135,241)'}
                                                        uncheckedColor={'rgb(61,135,241)'}

                                                    />
                                                    <Text style={{ color: 'rgb(68,67,67)', fontSize: 16 }}>Pre-Uploaded Gantt</Text>
                                                </View>
                                                <View style={styles.gantts_card}>
                                                    < FastImage
                                                        style={styles.image_gentt_logo}
                                                        source={require('../../assets/icons/Gantt.png')
                                                        }
                                                        resizeMode={FastImage.resizeMode.contain}
                                                    />
                                                    <View style={{ alignItems: 'center' }}>

                                                        <Text style={{ color: 'rgb(68,67,67)', fontFamily: 'SourceSansPro-Semibold', fontSize: 14, marginTop: 10 }}>{item.gantt_name}</Text>
                                                        <Text style={{ height: hp('0.2%'), width: wp('30%'), marginTop: 15, marginBottom: 8, backgroundColor: 'rgb(232, 232, 232)' }}></Text>
                                                        <View style={{ height: hp('10%'), alignItems: 'center' }}>
                                                            {remark ?
                                                                <>
                                                                    <Text style={{ color: 'rgb(68,67,67)', fontFamily: 'SourceSansPro-Regular', fontSize: 12, marginLeft: 5 }}>Created By</Text>
                                                                    <Text style={{ color: 'rgb(143,143,143)', fontFamily: 'SourceSansPro-Regular', fontSize: 12, marginLeft: 5 }}>{item.created_by}</Text>
                                                                    <Text style={{ color: 'rgb(68,67,67)', fontFamily: 'SourceSansPro-Regular', fontSize: 12, marginLeft: 5, marginTop: 15 }}>Date</Text>
                                                                    <Text style={{ color: 'rgb(143,143,143)', fontFamily: 'SourceSansPro-Regular', fontSize: 12, marginLeft: 5 }}>{item.date}</Text>
                                                                </>
                                                                :
                                                                <>
                                                                    <Text style={{ color: 'rgb(68,67,67)', fontFamily: 'SourceSansPro-Regular', fontSize: 12, marginLeft: 5 }}>{item.remarks}</Text>

                                                                </>
                                                            }
                                                        </View>

                                                        <TouchableOpacity>
                                                            <View style={{ borderColor: 'rgb(61,135,241)', justifyContent: 'center', alignItems: 'center', borderWidth: 1, marginTop: 20, borderRadius: 5, height: hp('5%'), width: wp('25%') }}>
                                                                <Text style={{ color: 'rgb(61,135,241)' }}>View</Text>
                                                            </View>
                                                        </TouchableOpacity>
                                                        {remark ?
                                                            <>
                                                                <TouchableOpacity onPress={() => { setremark(!remark) }}>
                                                                    <Text style={{
                                                                        fontSize: hp('2.5%'),
                                                                        fontFamily: 'SourceSansPro-Regular',
                                                                        color: 'rgb(61,135,241)',
                                                                        marginTop: 15
                                                                    }}>Remarks</Text>
                                                                </TouchableOpacity>
                                                            </>
                                                            :
                                                            <>
                                                                <TouchableOpacity onPress={() => { setremark(!remark) }}>
                                                                    <Text style={{
                                                                        fontSize: hp('2.5%'),
                                                                        fontFamily: 'SourceSansPro-Regular',
                                                                        color: 'rgb(61,135,241)',
                                                                        marginTop: 15
                                                                    }}>Close Remarks</Text>
                                                                </TouchableOpacity>
                                                            </>}

                                                        {
                                                            checked === item.gantt_id ?
                                                                <TouchableOpacity onPress={() => onGanttSelectHandler(item.gantt_id)}>
                                                                    <View style={{ borderColor: 'rgb(61,135,241)', justifyContent: 'center', alignItems: 'center', borderWidth: 1, marginTop: 20, borderRadius: 5, height: hp('5%'), width: wp('35%') }}>
                                                                        <Text style={{ color: 'rgb(61,135,241)' }}>Select Template</Text>
                                                                    </View>
                                                                </TouchableOpacity>

                                                                : null
                                                        }
                                                    </View>
                                                </View>

                                            </View>
                                        )
                                    }
                                />



                            </View>
                        }
                        <View>
                            <View>
                                {typesactive ?
                                    <View style={styles.textInput1}>
                                        <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>Type of Project</Text>
                                        <TouchableOpacity onPress={() => { settypesactive(!typesactive) }}>
                                            <FastImage
                                                style={styles.image_logo}
                                                source={require('../../assets/icons/down.png')
                                                }
                                                resizeMode={FastImage.resizeMode.contain}
                                            />
                                        </TouchableOpacity>
                                    </View>
                                    :
                                    <>
                                        <View style={styles.textInput1}>
                                            <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>Type of Project</Text>
                                            <TouchableOpacity onPress={() => { settypesactive(!typesactive) }}>
                                                < FastImage
                                                    style={styles.image_logo}
                                                    source={require('../../assets/icons/Up.png')
                                                    }
                                                    resizeMode={FastImage.resizeMode.contain}
                                                />
                                            </TouchableOpacity>

                                        </View>
                                        <View style={styles.card}>
                                            {/* {
                                                updatedDrawing.map(i => {
                                                    return (
                                                        <CheckBox
                                                            style={{ marginTop: 10 }}
                                                            checked={i.check}
                                                            onChange={nextChecked => setChecked(nextChecked)}>
                                                        </CheckBox>
                                                    )
                                                })
                                            } */}
                                            {checked == '' ?
                                                <>
                                                    <CheckBox
                                                        style={{ marginTop: 10 }}
                                                        checked={Rooftop}
                                                        onChange={nextChecked => setRooftop(nextChecked)}>
                                                        Rooftop
                                                    </CheckBox>
                                                    <CheckBox
                                                        style={{ marginTop: 10 }}
                                                        checked={Open_Access}
                                                        onChange={nextChecked => setOpen_Access(nextChecked)}>
                                                        Open Access
                                                    </CheckBox>
                                                    <CheckBox
                                                        style={{ marginTop: 10 }}
                                                        checked={Ground_Mounted}
                                                        onChange={nextChecked => setGround_Mounted(nextChecked)}>
                                                        Ground Mounted
                                                    </CheckBox>
                                                    <CheckBox
                                                        style={{ marginTop: 10 }}
                                                        checked={Industrial}
                                                        onChange={nextChecked => setIndustrial(nextChecked)}>
                                                        Industrial
                                                    </CheckBox>
                                                    <CheckBox
                                                        style={{ marginTop: 10 }}
                                                        checked={Commercial}
                                                        onChange={nextChecked => setCommercial(nextChecked)}>
                                                        Commercial
                                                    </CheckBox>
                                                    <CheckBox
                                                        style={{ marginTop: 10 }}
                                                        checked={Residential}
                                                        onChange={nextChecked => setResidential(nextChecked)}>
                                                        Residential
                                                    </CheckBox>
                                                </> :
                                                <>
                                                    <CheckBox
                                                        style={{ marginTop: 10 }}
                                                        checked={Rooftop}
                                                    // onChange={nextChecked => setRooftop(nextChecked)}>
                                                    >Rooftop
                                                    </CheckBox>
                                                    <CheckBox
                                                        style={{ marginTop: 10 }}
                                                        checked={Open_Access}
                                                    // onChange={nextChecked => setOpen_Access(nextChecked)}>
                                                    >Open Access
                                                    </CheckBox>
                                                    <CheckBox
                                                        style={{ marginTop: 10 }}
                                                        checked={Ground_Mounted}
                                                    // onChange={nextChecked => setGround_Mounted(nextChecked)}>
                                                    >  Ground Mounted
                                                    </CheckBox>
                                                    <CheckBox
                                                        style={{ marginTop: 10 }}
                                                        checked={Industrial}
                                                    // onChange={nextChecked => setIndustrial(nextChecked)}>
                                                    > Industrial
                                                    </CheckBox>
                                                    <CheckBox
                                                        style={{ marginTop: 10 }}
                                                        checked={Commercial}
                                                    // onChange={nextChecked => setCommercial(nextChecked)}>
                                                    > Commercial
                                                    </CheckBox>
                                                    <CheckBox
                                                        style={{ marginTop: 10 }}
                                                        checked={Residential}
                                                    // onChange={nextChecked => setResidential(nextChecked)}>
                                                    >Residential
                                                    </CheckBox>
                                                </>

                                            }





                                        </View>
                                    </>
                                }
                            </View>


                            <View>
                                {drawingsactive ?
                                    <View style={styles.textInput1}>
                                        <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>Drawings Needed</Text>
                                        <TouchableOpacity onPress={() => { setdrawingsactive(!drawingsactive) }}>
                                            <FastImage
                                                style={styles.image_logo}
                                                source={require('../../assets/icons/down.png')
                                                }
                                                resizeMode={FastImage.resizeMode.contain}
                                            />
                                        </TouchableOpacity>
                                    </View>
                                    :
                                    <>
                                        <View style={styles.textInput1}>
                                            <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>Drawings Needed</Text>
                                            <TouchableOpacity onPress={() => { setdrawingsactive(!drawingsactive) }}>
                                                < FastImage
                                                    style={styles.image_logo}
                                                    source={require('../../assets/icons/Up.png')
                                                    }
                                                    resizeMode={FastImage.resizeMode.contain}
                                                />
                                            </TouchableOpacity>

                                        </View>
                                        <View style={styles.card}>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Array_Layout}
                                                onChange={nextChecked => setArray_Layout(nextChecked)}>
                                                Array Layout
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Civil_Layout}
                                                onChange={nextChecked => setCivil_Layout(nextChecked)}>
                                                Civil Layout
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={PV_Syst}
                                                onChange={nextChecked => setPV_Syst(nextChecked)}>
                                                PV-Syst
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Communication}
                                                onChange={nextChecked => setCommunication(nextChecked)}>
                                                Communication System Layout
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Equipment_Layout}
                                                onChange={nextChecked => setEquipment_Layout(nextChecked)}>
                                                Equipment Layout
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Walkways}
                                                onChange={nextChecked => setWalkways(nextChecked)}>
                                                Walkways
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Earthing_LA}
                                                onChange={nextChecked => setEarthing_LA(nextChecked)}>
                                                Earthing and LA Layout
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Safety_Line}
                                                onChange={nextChecked => setSafety_Line(nextChecked)}>
                                                Safety Line
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Cable_Layout}
                                                onChange={nextChecked => setCable_Layout(nextChecked)}>
                                                Cable Layout
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Safety_Rails}
                                                onChange={nextChecked => setSafety_Rails(nextChecked)}>
                                                Safety Rails
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Structure_Drawings_RCC}
                                                onChange={nextChecked => setStructure_Drawings_RCC(nextChecked)}>
                                                Structure Drawings_RCC
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Project_SLD}
                                                onChange={nextChecked => setProject_SLD(nextChecked)}>
                                                Project SLD
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Structure_Drawings_Metal}
                                                onChange={nextChecked => setStructure_Drawings_Metal(nextChecked)}>
                                                Structure Drawings_Metal Sheet
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Final_BOQ}
                                                onChange={nextChecked => setFinal_BOQ(nextChecked)}>
                                                Final BOQ
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Structure_Drawings_Ground}
                                                onChange={nextChecked => setStructure_Drawings_Ground(nextChecked)}>
                                                Structure Drawings_Ground Mount
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Detailed_Project_Report}
                                                onChange={nextChecked => setDetailed_Project_Report(nextChecked)}>
                                                Detailed Project Report
                                            </CheckBox>


                                        </View>
                                    </>
                                }
                            </View>



                            <View>
                                {componentsactive ?
                                    <View style={styles.textInput1}>
                                        <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>Components Needed</Text>
                                        <TouchableOpacity onPress={() => { setcomponentsactive(!componentsactive) }}>
                                            <FastImage
                                                style={styles.image_logo}
                                                source={require('../../assets/icons/down.png')
                                                }
                                                resizeMode={FastImage.resizeMode.contain}
                                            />
                                        </TouchableOpacity>
                                    </View>
                                    :
                                    <>
                                        <View style={styles.textInput1}>
                                            <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>Components Needed</Text>
                                            <TouchableOpacity onPress={() => { setcomponentsactive(!componentsactive) }}>
                                                < FastImage
                                                    style={styles.image_logo}
                                                    source={require('../../assets/icons/Up.png')
                                                    }
                                                    resizeMode={FastImage.resizeMode.contain}
                                                />
                                            </TouchableOpacity>

                                        </View>
                                        <View style={styles.card}>

                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Solar_PV_Module}
                                                onChange={nextChecked => setSolar_PV_Module(nextChecked)}>
                                                Solar PV Module
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Module_Metal_Sheet}
                                                onChange={nextChecked => setModule_Metal_Sheet(nextChecked)}>
                                                Module Mounting Structures with Accessories _ Metal Sheet
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Solar_Inverter}
                                                onChange={nextChecked => setSolar_Inverter(nextChecked)}>
                                                Solar Inverter
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Module_Ground_Mount}
                                                onChange={nextChecked => setModule_Ground_Mount(nextChecked)}>
                                                Module Mounting Structures with Accessories _ Ground Mount
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Module_RCC}
                                                onChange={nextChecked => setModule_RCC(nextChecked)}>
                                                Module Mounting Structures with Accessories _ RCC
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Structure_Foundation_Pedestal}
                                                onChange={nextChecked => setStructure_Foundation_Pedestal(nextChecked)}>
                                                Structure Foundation Pedestal
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Cabling_Accessories}
                                                onChange={nextChecked => setCabling_Accessories(nextChecked)}>
                                                Cabling Accessories
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Earthing_System}
                                                onChange={nextChecked => setEarthing_System(nextChecked)}>
                                                Earthing System
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Lighting_Arrestor}
                                                onChange={nextChecked => setLighting_Arrestor(nextChecked)}>
                                                Lighting Arrestor
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={LT_Panel}
                                                onChange={nextChecked => setLT_Panel(nextChecked)}>
                                                LT Panel
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={HT_Panel}
                                                onChange={nextChecked => setHT_Panel(nextChecked)}>
                                                HT Panel
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={AC_Distribution}
                                                onChange={nextChecked => setAC_Distribution(nextChecked)}>
                                                AC Distribution (Combiner) Panel Board
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Transformer}
                                                onChange={nextChecked => setTransformer(nextChecked)}>
                                                Transformer
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={LT_From_inverters}
                                                onChange={nextChecked => setLT_From_inverters(nextChecked)}>
                                                LT Power Cable (From inverters to ACDB)
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={LT_From_ACDB}
                                                onChange={nextChecked => setLT_From_ACDB(nextChecked)}>
                                                LT Power Cable (From ACDB to Customer LT Panel)
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={LT_From_metering}
                                                onChange={nextChecked => setLT_From_metering(nextChecked)}>
                                                LT Power Cable (From metering cubical to MDB panel)
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Earthing_Cable}
                                                onChange={nextChecked => setEarthing_Cable(nextChecked)}>
                                                Earthing Cable
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Module_Earthing_Cable}
                                                onChange={nextChecked => setModule_Earthing_Cable(nextChecked)}>
                                                Module Earthing Cable
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={DC_Junction_Box}
                                                onChange={nextChecked => setDC_Junction_Box(nextChecked)}>
                                                DC Junction Box (SCB/SMB)
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={GI_Strip}
                                                onChange={nextChecked => setGI_Strip(nextChecked)}>
                                                GI Strip
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Metering_Panel}
                                                onChange={nextChecked => setMetering_Panel(nextChecked)}>
                                                Metering Panel
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={MDB_Breaker}
                                                onChange={nextChecked => setMDB_Breaker(nextChecked)}>
                                                MDB Breaker
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Cable_trays}
                                                onChange={nextChecked => setCable_trays(nextChecked)}>
                                                Cable trays
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={HT_Breaker}
                                                onChange={nextChecked => setHT_Breaker(nextChecked)}>
                                                HT Breaker
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Bus_bar}
                                                onChange={nextChecked => setBus_bar(nextChecked)}>
                                                Bus bar
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={InC_Contractor}
                                                onChange={nextChecked => setInC_Contractor(nextChecked)}>
                                                InC Contractor
                                            </CheckBox>
                                        </View>
                                    </>
                                }
                            </View>




                            <View>
                                {accessoriesactive ?
                                    <View style={styles.textInput1}>
                                        <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>Accessories Needed</Text>
                                        <TouchableOpacity onPress={() => { setaccessoriesactive(!accessoriesactive) }}>
                                            <FastImage
                                                style={styles.image_logo}
                                                source={require('../../assets/icons/down.png')
                                                }
                                                resizeMode={FastImage.resizeMode.contain}
                                            />
                                        </TouchableOpacity>
                                    </View>
                                    :
                                    <>
                                        <View style={styles.textInput1}>
                                            <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>Accessories Needed</Text>
                                            <TouchableOpacity onPress={() => { setaccessoriesactive(!accessoriesactive) }}>
                                                < FastImage
                                                    style={styles.image_logo}
                                                    source={require('../../assets/icons/Up.png')
                                                    }
                                                    resizeMode={FastImage.resizeMode.contain}
                                                />
                                            </TouchableOpacity>

                                        </View>
                                        <View style={styles.card}>

                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Reverse_Protection_Relay}
                                                onChange={nextChecked => setReverse_Protection_Relay(nextChecked)}>
                                                Reverse Protection Relay
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Net_Metering}
                                                onChange={nextChecked => setNet_Metering(nextChecked)}>
                                                Net-Metering
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={DG_Synchronization}
                                                onChange={nextChecked => setDG_Synchronization(nextChecked)}>
                                                DG Synchronization
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Communication_Cable}
                                                onChange={nextChecked => setCommunication_Cable(nextChecked)}>
                                                Communication Cable
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={ASafety_Rails}
                                                onChange={nextChecked => setASafety_Rails(nextChecked)}>
                                                Safety Rails
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={AWalkways}
                                                onChange={nextChecked => setAWalkways(nextChecked)}>
                                                Walkways
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Safety_Lines}
                                                onChange={nextChecked => setSafety_Lines(nextChecked)}>
                                                Safety Lines
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={SCADA_System}
                                                onChange={nextChecked => setSCADA_System(nextChecked)}>
                                                SCADA System
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Pyro_Meter}
                                                onChange={nextChecked => setPyro_Meter(nextChecked)}>
                                                Pyro Meter
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Weather_Monitoring_Unit}
                                                onChange={nextChecked => setWeather_Monitoring_Unit(nextChecked)}>
                                                Weather Monitoring Unit
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Ambient_Temperature_Sensors}
                                                onChange={nextChecked => setAmbient_Temperature_Sensors(nextChecked)}>
                                                Ambient Temperature Sensors
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={UPS}
                                                onChange={nextChecked => setUPS(nextChecked)}>
                                                UPS
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Module_Temperature_Sensors}
                                                onChange={nextChecked => setModule_Temperature_Sensors(nextChecked)}>
                                                Module Temperature Sensors
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Wire_mesh_for_protection}
                                                onChange={nextChecked => setWire_mesh_for_protection(nextChecked)}>
                                                Wire mesh for protection of Skylights
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Cleaning_System}
                                                onChange={nextChecked => setCleaning_System(nextChecked)}>
                                                Cleaning System
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Danger_Board_and_Signs}
                                                onChange={nextChecked => setDanger_Board_and_Signs(nextChecked)}>
                                                Danger Board and Signs
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Fire_Extingusiher}
                                                onChange={nextChecked => setFire_Extingusiher(nextChecked)}>
                                                Fire Extingusiher
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Generation_Meter}
                                                onChange={nextChecked => setGeneration_Meter(nextChecked)}>
                                                Generation Meter
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Zero_Export_Device}
                                                onChange={nextChecked => setZero_Export_Device(nextChecked)}>
                                                Zero Export Device
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Other_Sensors}
                                                onChange={nextChecked => setOther_Sensors(nextChecked)}>
                                                Other Sensors
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Control_Room}
                                                onChange={nextChecked => setControl_Room(nextChecked)}>
                                                Control Room
                                            </CheckBox>

                                        </View>
                                    </>
                                }
                            </View>





                            <View>
                                {areaactive ?
                                    <View style={styles.textInput1}>
                                        <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>Area of installation</Text>
                                        <TouchableOpacity onPress={() => { setareaactive(!areaactive) }}>
                                            <FastImage
                                                style={styles.image_logo}
                                                source={require('../../assets/icons/down.png')
                                                }
                                                resizeMode={FastImage.resizeMode.contain}
                                            />
                                        </TouchableOpacity>
                                    </View>
                                    :
                                    <>
                                        <View style={styles.textInput1}>
                                            <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>Area of installation</Text>
                                            <TouchableOpacity onPress={() => { setareaactive(!areaactive) }}>
                                                < FastImage
                                                    style={styles.image_logo}
                                                    source={require('../../assets/icons/Up.png')
                                                    }
                                                    resizeMode={FastImage.resizeMode.contain}
                                                />
                                            </TouchableOpacity>

                                        </View>
                                        <View style={styles.card}>

                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Ground_Mount}
                                                onChange={nextChecked => setGround_Mount(nextChecked)}>
                                                Ground Mount
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={RCC}
                                                onChange={nextChecked => setRCC(nextChecked)}>
                                                RCC
                                            </CheckBox>
                                            <CheckBox
                                                style={{ marginTop: 10 }}
                                                checked={Metal_Sheet}
                                                onChange={nextChecked => setMetal_Sheet(nextChecked)}>
                                                Metal Sheet
                                            </CheckBox>
                                        </View>
                                    </>
                                }
                            </View>

                            <View>
                                {terminationactive ?
                                    <View style={styles.textInput1}>
                                        <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>Termination</Text>
                                        <TouchableOpacity onPress={() => { setterminationactive(!terminationactive) }}>
                                            <FastImage
                                                style={styles.image_logo}
                                                source={require('../../assets/icons/down.png')
                                                }
                                                resizeMode={FastImage.resizeMode.contain}
                                            />
                                        </TouchableOpacity>
                                    </View>
                                    :
                                    <>
                                        <View style={styles.textInput1}>
                                            <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>Termination</Text>
                                            <TouchableOpacity onPress={() => { setterminationactive(!terminationactive) }}>
                                                < FastImage
                                                    style={styles.image_logo}
                                                    source={require('../../assets/icons/Up.png')
                                                    }
                                                    resizeMode={FastImage.resizeMode.contain}
                                                />
                                            </TouchableOpacity>

                                        </View>
                                        <View style={styles.card}>

                                            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                                                <RadioButton
                                                    value="LT"
                                                    color={'rgb(61,135,241)'}
                                                    uncheckedColor={'rgb(61,135,241)'}
                                                    status={termination === 'LT' ? 'checked' : 'unchecked'}
                                                    onPress={() => setTermination('LT')}
                                                />
                                                <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 10 }}>LT</Text>
                                            </View>

                                            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                                                <RadioButton
                                                    value="HT"
                                                    color={'rgb(61,135,241)'}
                                                    uncheckedColor={'rgb(61,135,241)'}
                                                    status={termination === 'HT' ? 'checked' : 'unchecked'}
                                                    onPress={() => setTermination('HT')}
                                                />
                                                <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 10 }}>HT</Text>
                                            </View>


                                        </View>
                                    </>
                                }
                            </View>

                            <View>
                                {terminationdetailsactive ?
                                    <View style={styles.textInput1}>
                                        <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>Termination Details</Text>
                                        <TouchableOpacity onPress={() => { setterminationdetailsactive(!terminationdetailsactive) }}>
                                            <FastImage
                                                style={styles.image_logo}
                                                source={require('../../assets/icons/down.png')
                                                }
                                                resizeMode={FastImage.resizeMode.contain}
                                            />
                                        </TouchableOpacity>
                                    </View>
                                    :
                                    <>
                                        <View style={styles.textInput1}>
                                            <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>Termination Details</Text>
                                            <TouchableOpacity onPress={() => { setterminationdetailsactive(!terminationdetailsactive) }}>
                                                < FastImage
                                                    style={styles.image_logo}
                                                    source={require('../../assets/icons/Up.png')
                                                    }
                                                    resizeMode={FastImage.resizeMode.contain}
                                                />
                                            </TouchableOpacity>

                                        </View>
                                        <View style={styles.card}>
                                            <Text style={{ color: 'rgb(68,67,67)', fontWeight: 'bold', fontSize: 16, marginLeft: 10 }}>Inverter to ACDB</Text>

                                            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                                                <RadioButton
                                                    value="Cable Tray"
                                                    color={'rgb(61,135,241)'}
                                                    uncheckedColor={'rgb(61,135,241)'}
                                                    status={InvertertoACDB === 'Cable Tray' ? 'checked' : 'unchecked'}
                                                    onPress={() => setInvertertoACDB('Cable Tray')}
                                                />
                                                <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 10 }}>Cable Tray</Text>
                                            </View>

                                            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                                                <RadioButton
                                                    value="Cable Trench"
                                                    color={'rgb(61,135,241)'}
                                                    uncheckedColor={'rgb(61,135,241)'}
                                                    status={InvertertoACDB === 'Cable Trench' ? 'checked' : 'unchecked'}
                                                    onPress={() => setInvertertoACDB('Cable Trench')}
                                                />
                                                <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 10 }}>Cable Trench</Text>
                                            </View>
                                            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                                                <RadioButton
                                                    value="HDPE Pipes"
                                                    color={'rgb(61,135,241)'}
                                                    uncheckedColor={'rgb(61,135,241)'}
                                                    status={InvertertoACDB === 'HDPE Pipes' ? 'checked' : 'unchecked'}
                                                    onPress={() => setInvertertoACDB('HDPE Pipes')}
                                                />
                                                <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 10 }}>HDPE Pipes</Text>
                                            </View>


                                            <Text style={{ color: 'rgb(68,67,67)', fontWeight: 'bold', fontSize: 16, marginLeft: 10 }}>ACDB to LT Panel</Text>

                                            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                                                <RadioButton
                                                    value="Cable Tray"
                                                    color={'rgb(61,135,241)'}
                                                    uncheckedColor={'rgb(61,135,241)'}
                                                    status={ACDBtoLTPanel === 'Cable Tray' ? 'checked' : 'unchecked'}
                                                    onPress={() => setACDBtoLTPanel('Cable Tray')}
                                                />
                                                <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 10 }}>Cable Tray</Text>
                                            </View>

                                            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                                                <RadioButton
                                                    value="Cable Trench"
                                                    color={'rgb(61,135,241)'}
                                                    uncheckedColor={'rgb(61,135,241)'}
                                                    status={ACDBtoLTPanel === 'Cable Trench' ? 'checked' : 'unchecked'}
                                                    onPress={() => setACDBtoLTPanel('Cable Trench')}
                                                />
                                                <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 10 }}>Cable Trench</Text>
                                            </View>
                                            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                                                <RadioButton
                                                    value="HDPE Pipes"
                                                    color={'rgb(61,135,241)'}
                                                    uncheckedColor={'rgb(61,135,241)'}
                                                    status={ACDBtoLTPanel === 'HDPE Pipes' ? 'checked' : 'unchecked'}
                                                    onPress={() => setACDBtoLTPanel('HDPE Pipes')}
                                                />
                                                <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 10 }}>HDPE Pipes</Text>
                                            </View>

                                            {termination == 'HT' ?
                                                <>
                                                    <Text style={{ color: 'rgb(68,67,67)', fontWeight: 'bold', fontSize: 16, marginLeft: 10 }}>Transformer to HT Panel</Text>

                                                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                                                        <RadioButton
                                                            value="Cable Tray"
                                                            color={'rgb(61,135,241)'}
                                                            uncheckedColor={'rgb(61,135,241)'}
                                                            status={TransformertoHTPanel === 'Cable Tray' ? 'checked' : 'unchecked'}
                                                            onPress={() => setTransformertoHTPanel('Cable Tray')}
                                                        />
                                                        <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 10 }}>Cable Tray</Text>
                                                    </View>

                                                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                                                        <RadioButton
                                                            value="Cable Trench"
                                                            color={'rgb(61,135,241)'}
                                                            uncheckedColor={'rgb(61,135,241)'}
                                                            status={TransformertoHTPanel === 'Cable Trench' ? 'checked' : 'unchecked'}
                                                            onPress={() => setTransformertoHTPanel('Cable Trench')}
                                                        />
                                                        <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 10 }}>Cable Trench</Text>
                                                    </View>
                                                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                                                        <RadioButton
                                                            value="HDPE Pipes"
                                                            color={'rgb(61,135,241)'}
                                                            uncheckedColor={'rgb(61,135,241)'}
                                                            status={TransformertoHTPanel === 'HDPE Pipes' ? 'checked' : 'unchecked'}
                                                            onPress={() => setTransformertoHTPanel('HDPE Pipes')}
                                                        />
                                                        <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 10 }}>HDPE Pipes</Text>
                                                    </View>
                                                </>
                                                : null
                                            }
                                        </View>
                                    </>
                                }
                            </View>



                        </View>

                    </View>
                </ScrollView>

                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: hp('15%'),
                    width: wp('100%'),
                }}>
                    <TouchableOpacity
                        onPress={() => navigation.goBack()}>
                        <View style={{ borderColor: 'rgb(61,135,241)', justifyContent: 'center', alignItems: 'center', borderWidth: 1, marginTop: -20, borderRadius: 5, height: hp('7'), width: wp('30%') }}>
                            <Text style={{ color: 'rgb(61,135,241)' }}>Previous</Text>
                        </View>
                    </TouchableOpacity>
                    {saveproject?
                        <TouchableOpacity
                        onPress={() => postProject()}
                    >
                        <View style={{ backgroundColor: 'rgb(61,135,241)', marginLeft: 10, justifyContent: 'center', alignItems: 'center', marginTop: 1 - 20, borderRadius: 5, height: hp('7'), width: wp('30%') }}>
                            <Text style={{ color: '#fff' }}>Project Planning</Text>
                        </View>
                    </TouchableOpacity>
                    :
<TouchableOpacity
                        onPress={() => onsubmit()}
                    >
                        <View style={{ backgroundColor: 'rgb(61,135,241)', marginLeft: 10, justifyContent: 'center', alignItems: 'center', marginTop: 1 - 20, borderRadius: 5, height: hp('7'), width: wp('30%') }}>
                            <Text style={{ color: '#fff' }}>Save</Text>
                        </View>
                    </TouchableOpacity>
                    }
                    
                    
                    <TouchableOpacity>
                        <Text style={{ textDecorationLine: 'underline', color: 'rgb(61,135,241)', marginLeft: 10, marginTop: 1 - 20, fontFamily: 'SourceSansPro-Regular' }}>Save for later</Text>
                    </TouchableOpacity>

                </View>
            </View>

        </SafeAreaView>

    )
};
const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
    shadowContainerStyle1: {   //<--- Style with elevation

        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 510 },
        shadowOpacity: 20,
        shadowRadius: 300,
        elevation: 3,
    },
    image_logo: {
        width: wp('8%'),
        height: hp('5%'),
    },
    image_gentt_logo: {
        width: wp('44%'),
        height: hp('18%')

    },
    card: {   //<--- Style with elevation

        borderRadius: 5,
        elevation: 3,
        width: wp('85%'),
        marginTop: -7,
        // height: 150,
        padding: 10
        //  justifyContent: 'center',
        //  alignItems:'center'
    },
    gantts_card: {   //<--- Style with elevation
        borderRadius: 5,
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        elevation: 3,
        width: wp('45%'),
        margin: 10,
        height: hp('60%'),

        //  justifyContent: 'center',
        //  alignItems:'center'
    },
    image_logo: {
        width: wp('8%'),
        height: hp('5%'),
    },
    parent2: {
        // height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
        flexDirection: 'row',
        marginTop: hp('3%'),
        // justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        backgroundColor: 'white',
        flexDirection: 'column'
    },
    option_text: {
        fontSize: hp('1.2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center'
    },
    signIn: {
        width: wp('74%'),
        height: hp('8%'),
        left: 25,
        top: 80,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        color: '#05375a',
        backgroundColor: '#3A80E3',
        borderRadius: 5,
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: 'SourceSansPro-Regular',
    },
    name_text: {
        fontSize: 16,
        fontWeight: 'bold',
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68,67,67)'
    },
    temail_textxt: {
        fontSize: 14,
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(143,143,143)'
    },
    txt: {
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(143,143,143)'
    },
    txtunder: {
        fontSize: hp('2.5%'),
        textDecorationLine: 'underline',
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(61,135,241)'
    },
    textblue: {
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(61,135,241)'
    },
    textInput1: {
        marginTop: hp('3%'),
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        width: wp('85%'),
        height: hp('7.4%'),
        borderRadius: 4,
        fontSize: 16,
        fontFamily: 'SourceSansPro-Regular',
        color: 'black',
        textAlign: 'left',
        paddingHorizontal: 10,
        marginBottom: Platform.OS === 'ios' ? 6 : 8
    },

    btnText: {
        fontSize: 12,
        fontFamily: 'SourceSansPro-Regular'
    },

    btn: {
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        width: wp('40%'),
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center'
    },
    uploadbtnText: {
        fontSize: hp('2%'),
        color: '#fff',
        fontFamily: 'SourceSansPro-Regular'
    },

    uploadbtn: {
        borderWidth: 1,
        backgroundColor: '#3d87f1',
        borderColor: 'rgb(232, 232, 232)',
        width: wp('20%'),
        height: hp('5%'),
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center'
    },

});
export default ProjectSpecification1