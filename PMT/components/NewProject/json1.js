export const types_data = [
    "Rooftop" ,              
    "Open Access" ,
    "Ground Mounted" ,
    "Industrial" , 
    "Commercial" , 
    "Residential" ]
            


export const drawings_data = [
    "Array Layout",
    "Civil Layout",
    "PV-Syst",
    "Communication System Layout",
    "Equipment Layout",
    "Walkways",
    "Earthing and LA Layout",
    "Safety Line",
    "Cable Layout",
    "Safety Rails",
    "Structure Drawings_RCC",
    "Project SLD",
    "Structure Drawings_Metal Sheet",
    "Final BOQ",
    "Structure Drawings_Ground Mount",
    "Detailed Project Report"
]


export const components_data  = [
    "Solar PV Module",
    "Module Mounting Structures with Accessories _ Metal Sheet",
    "Solar Inverter",
    "Module Mounting Structures with Accessories _ Ground Mount",
    "Module Mounting Structures with Accessories _ RCC",
    "Structure Foundation Pedestal",
    "Cabling Accessories",
    "Earthing System",
    "Lighting Arrestor",
    "LT Panel",
    "HT Panel",
    "AC Distribution (Combiner) Panel Board",
    "Transformer",
    "LT Power Cable (From inverters to ACDB)",
    "LT Power Cable (From ACDB to Customer LT Panel)",
    "LT Power Cable (From metering cubical to MDB panel)",
    "Earthing Cable",
    "Module Earthing Cable",
    "DC Junction Box (SCB/SMB)",
    "GI Strip",
    "Metering Panel",
    "MDB Breaker",
    "Cable trays",
    "HT Breaker",
    "Bus bar",
    "InC Contractor"
]


export const accessories_data  = [
    "Reverse Protection Relay",
    "Net-Metering",
    "DG Synchronization",
    "Communication Cable",
    "Safety Rails",
    "Walkways",
    "Safety Lines",
    "SCADA System",
    "Pyro Meter",
    "Weather Monitoring Unit",
    "Ambient Temperature Sensors",
    "UPS",
    "Module Temperature Sensors",
    "Wire mesh for protection of Skylights",
    "Cleaning System",
    "Danger Board and Signs",
    "Fire Extingusiher",
    "Generation Meter",
    "Zero Export Device",
    "Other Sensors",
    "Control Room"
]
           
export const area_data = [
    "Ground Mount",
    "RCC",
    "Metal Sheet"
]
  
const [Rooftop, setRooftop] = useState(false);
const [Open_Access, setOpen_Access] = useState(false);
const [Ground_Mounted, setGround_Mounted] = useState(false);
const [Industrial, setIndustrial] = useState(false);
const [Commercial, setCommercial] = useState(false);
const [Residential, setResidential] = useState(false);




const [Array_Layout, setArray_Layout] = useState(false);
const [Civil_Layout, setCivil_Layout] = useState(false);
const [PV_Syst, setPV_Syst] = useState(false);
const [Communication, setCommunication] = useState(false);
const [Equipment_Layout, setEquipment_Layout] = useState(false);
const [Walkways, setWalkways] = useState(false);
const [Earthing_LA, setEarthing_LA] = useState(false);
const [Safety_Line, setSafety_Line] = useState(false);
const [Cable_Layout, setCable_Layout] = useState(false);
const [Safety_Rails, setSafety_Rails] = useState(false);
const [Structure_Drawings_RCC, setStructure_Drawings_RCC] = useState(false);
const [Project_SLD, setProject_SLD] = useState(false);
const [Structure_Drawings_Metal, setStructure_Drawings_Metal] = useState(false);
const [Final_BOQ, setFinal_BOQ] = useState(false);
const [Structure_Drawings_Ground, setStructure_Drawings_Ground] = useState(false);
const [Detailed_Project_Report, setDetailed_Project_Report] = useState(false);




const [Solar_PV_Module, setSolar_PV_Module] = useState(false);
const [Module_Metal_Sheet, setModule_Metal_Sheet] = useState(false);
const [Solar_Inverter, setSolar_Inverter] = useState(false);
const [Module_Ground_Mount, setModule_Ground_Mount] = useState(false);
const [Module_RCC, setModule_RCC] = useState(false);
const [Structure_Foundation_Pedestal, setStructure_Foundation_Pedestal] = useState(false);
const [Cabling_Accessories, setCabling_Accessories] = useState(false);
const [Earthing_System, setEarthing_System] = useState(false);
const [Lighting_Arrestor, setLighting_Arrestor] = useState(false);
const [LT_Panel, setLT_Panel] = useState(false);
const [HT_Panel, setHT_Panel] = useState(false);
const [AC_Distribution, setAC_Distribution] = useState(false);
const [Transformer, setTransformer] = useState(false);
const [LT_From_inverters, setLT_From_inverters] = useState(false);
const [LT_From_ACDB, setLT_From_ACDB] = useState(false);
const [LT_From_metering, setLT_From_metering] = useState(false);
const [Earthing_Cable, setEarthing_Cable] = useState(false);
const [Module_Earthing_Cable, setModule_Earthing_Cable] = useState(false);
const [DC_Junction_Box, setDC_Junction_Box] = useState(false);
const [GI_Strip, setGI_Strip] = useState(false);
const [Metering_Panel, setMetering_Panel] = useState(false);
const [MDB_Breaker, setMDB_Breaker] = useState(false);
const [Cable_trays, setCable_trays] = useState(false);
const [HT_Breaker, setHT_Breaker] = useState(false);
const [Bus_bar, setBus_bar] = useState(false);
const [InC_Contractor, setInC_Contractor] = useState(false);




const [Reverse_Protection_Relay, setReverse_Protection_Relay] = useState(false);
const [Net_Metering, setNet_Metering] = useState(false);
const [DG_Synchronization, setDG_Synchronization] = useState(false);
const [Communication_Cable, setCommunication_Cable] = useState(false);
const [ASafety_Rails, setASafety_Rails] = useState(false);
const [AWalkways, setAWalkways] = useState(false);

const [Safety_Lines, setSafety_Lines] = useState(false);
const [SCADA_System, setSCADA_System] = useState(false);
const [Pyro_Meter, setPyro_Meter] = useState(false);
const [Weather_Monitoring_Unit, setWeather_Monitoring_Unit] = useState(false);
const [Ambient_Temperature_Sensors, setAmbient_Temperature_Sensors] = useState(false);

const [UPS, setUPS] = useState(false);
const [Module_Temperature_Sensors, setModule_Temperature_Sensors] = useState(false);
const [Wire_mesh_for_protection, setWire_mesh_for_protection] = useState(false);
const [Cleaning_System, setCleaning_System] = useState(false);
const [Danger_Board_and_Signs, setDanger_Board_and_Signs] = useState(false);

const [Fire_Extingusiher, setFire_Extingusiher] = useState(false);
const [Generation_Meter, setGeneration_Meter] = useState(false);
const [Zero_Export_Device, setZero_Export_Device] = useState(false);
const [Other_Sensors, setOther_Sensors] = useState(false);
const [Control_Room, setControl_Room] = useState(false);


const [Ground_Mount, setGround_Mount] = useState(false);
const [RCC, setRCC] = useState(false);
const [Metal_Sheet, setMetal_Sheet] = useState(false);