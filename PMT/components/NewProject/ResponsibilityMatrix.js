import React, { useState, useMemo, useEffect } from 'react';

import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    PermissionsAndroid,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
} from 'react-native';
import { Searchbar, Button, Switch } from 'react-native-paper';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { FlatList } from 'react-native-gesture-handler';
import FastImage from 'react-native-fast-image';


const ResponsibilityMatrix = ({ props, route, navigation }) => {
    return (
        <SafeAreaView>
            <View style={styles.container}>
                <ScrollView>
                    <View style={styles.parent2}>
                        <View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: wp('90%') }}>
                            <Text style={{ fontFamily: 'SourceSansPro-Semibold', fontSize: 20, color: 'rgb(68,67,67)' }}>Responsibility Matrix</Text>
                            <TouchableOpacity onPress={() => setIsVisible(false) } >

                            <FastImage
                                style={styles.image_logo}
                                source={require('../../assets/icons/Remove.png')
                                }
                                resizeMode={FastImage.resizeMode.contain} />
                                </TouchableOpacity>

                        </View>
                        <View>
                        <Text style={{ height: hp('0.2%'), marginTop: 10, marginBottom: 5, backgroundColor: 'rgb(232, 232, 232)' }}></Text>

                        </View>
                        </View>
                        <View>
                            <View style={styles.card}>
                                <Text style={{ fontFamily: 'SourceSansPro-Semibold', fontSize: 16, color: 'rgb(68,67,67)' }}>Project Manager</Text>
                                <Text style={{ height: hp('0.2%'), marginTop: 10, marginBottom: 5, backgroundColor: 'rgb(232, 232, 232)' }}></Text>

                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                    <Text style={styles.txt}>Can View</Text>
                                    <Text style={styles.textblue}>Procurement</Text>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                    <Text style={styles.txt}>Can Edit</Text>
                                    <Text style={styles.textblue}>Array Layout</Text>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                    <Text style={styles.txt}>Can Approve</Text>
                                    <Text style={styles.textblue}>Engineering</Text>
                                </View>
                            </View>
                        </View>

                        <View>
                            <View style={styles.card}>
                                <Text style={{ fontFamily: 'SourceSansPro-Semibold', fontSize: 16, color: 'rgb(68,67,67)' }}>Project Manager</Text>
                                <Text style={{ height: hp('0.2%'), marginTop: 10, marginBottom: 5, backgroundColor: 'rgb(232, 232, 232)' }}></Text>

                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                    <Text style={styles.txt}>Can View</Text>
                                    <Text style={styles.textblue}>Procurement</Text>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                    <Text style={styles.txt}>Can Edit</Text>
                                    <Text style={styles.textblue}>Array Layout</Text>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                    <Text style={styles.txt}>Can Approve</Text>
                                    <Text style={styles.textblue}>Engineering</Text>
                                </View>
                            </View>
                        </View>

                        <View>
                            <View style={styles.card}>
                                <Text style={{ fontFamily: 'SourceSansPro-Semibold', fontSize: 16, color: 'rgb(68,67,67)' }}>Project Manager</Text>
                                <Text style={{ height: hp('0.2%'), marginTop: 10, marginBottom: 5, backgroundColor: 'rgb(232, 232, 232)' }}></Text>

                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                    <Text style={styles.txt}>Can View</Text>
                                    <Text style={styles.textblue}>Procurement</Text>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                    <Text style={styles.txt}>Can Edit</Text>
                                    <Text style={styles.textblue}>Array Layout</Text>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                    <Text style={styles.txt}>Can Approve</Text>
                                    <Text style={styles.textblue}>Engineering</Text>
                                </View>
                            </View>
                        </View>

                        <View>
                            <View style={styles.card}>
                                <Text style={{ fontFamily: 'SourceSansPro-Semibold', fontSize: 16, color: 'rgb(68,67,67)' }}>Project Manager</Text>
                                <Text style={{ height: hp('0.2%'), marginTop: 10, marginBottom: 5, backgroundColor: 'rgb(232, 232, 232)' }}></Text>

                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                    <Text style={styles.txt}>Can View</Text>
                                    <Text style={styles.textblue}>Procurement</Text>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                    <Text style={styles.txt}>Can Edit</Text>
                                    <Text style={styles.textblue}>Array Layout</Text>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                    <Text style={styles.txt}>Can Approve</Text>
                                    <Text style={styles.textblue}>Engineering</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{height:30}}>

                        </View>

                    </View>

                </ScrollView>
            </View>
        </SafeAreaView>
    )
};

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
    shadowContainerStyle1: {   //<--- Style with elevation

        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 510 },
        shadowOpacity: 20,
        shadowRadius: 300,
        elevation: 3,
    },
    card: {   //<--- Style with elevation

        borderRadius: 5,
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        elevation: 3,
        width: wp('90%'),
        margin: 10,
        height: 160,
        padding: 20
        //  justifyContent: 'center',
        //  alignItems:'center'
    },

    image_logo: {
        width: wp('7%'),
        height: hp('3.5%'),
    },
    parent2: {
        // height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
        flexDirection: 'row',
        marginTop: hp('3%'),
        // justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        backgroundColor: 'white',
        flexDirection: 'column'
    },
    option_text: {
        fontSize: hp('1.2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center'
    },
    signIn: {
        width: wp('74%'),
        height: hp('8%'),
        left: 25,
        top: 80,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        color: '#05375a',
        backgroundColor: '#3A80E3',
        borderRadius: 5,
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: 'SourceSansPro-Regular',
    },
    name_text: {
        fontSize: 16,
        fontWeight: 'bold',
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68,67,67)'
    },
    temail_textxt: {
        fontSize: 14,
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(143,143,143)'
    },
    txt: {
        fontSize: 14,
        fontFamily: 'SourceSansPro-Semibold',
        color: 'rgb(rgb(68,67,67))'
    },
    txtunder: {
        fontSize: hp('2.5%'),
        textDecorationLine: 'underline',
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(61,135,241)'
    },
    textblue: {
        fontSize: 14,
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(61,135,241)'
    },
    textInput1: {
        marginTop: hp('30%'),
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        width: wp('79.4%'),
        height: hp('7.4%'),
        borderRadius: 4,
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'black',
        textAlign: 'left',
        paddingHorizontal: 10,
        marginBottom: Platform.OS === 'ios' ? 6 : 8
    },
    textInput: {
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        width: wp('79.4%'),
        height: hp('7.4%'),
        borderRadius: 4,
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'black',
        textAlign: 'left',
        paddingHorizontal: 10,
        marginBottom: Platform.OS === 'ios' ? 6 : 8
    },
    btnText: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular'
    },

    btn: {
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        width: wp('40%'),
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center'
    },

});
export default ResponsibilityMatrix

