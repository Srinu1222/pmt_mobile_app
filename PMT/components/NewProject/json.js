export const types_data = [{ id: "Rooftop", name: "Rooftop" }, 
              { id: "Open Access", name: "Open Access" }, 
              { id: "Ground Mounted", name: "Ground Mounted" }, 
              { id: "Industrial", name: "Industrial" }, 
              { id: "Commercial", name: "Commercial" },
              { id: "Residential", name: "Residential" }
            ]

export const drawings_data = [{ id: "Array Layout", name: "Array Layout" }, 
              { id: "Communication System Layout", name: "Communication System Layout" }, 
              { id: "Earthing and LA Layout" , name: "Earthing and LA Layout" }, 
              { id: "Safety Rails", name: "Safety Rails" }, 
              { id: "Structure Drawings (Metal Sheet)", name: "Structure Drawings (Metal Sheet)" },
              { id: "Detailed Project Report", name: "Detailed Project Report" },
              { id: "Civil Layout", name: "Civil Layout" }, 
              { id: "Equipment Layout", name: "Equipment Layout" }, 
              { id: "Safety Line", name: "Safety Line" }, 
              { id: "Structure Drawings (RCC)", name: "Structure Drawings (RCC)" }, 
              { id: "Final BOQ", name: "Final BOQ" },
              { id: "PV-Syst", name: "PV-Syst" }, 
              { id: "Walkways", name: "Walkways" }, 
              { id: "Cable Layout", name: "Cable Layout" }, 
              { id: "Project SLD", name: "Project SLD" }, 
              { id: "Structure Drawings (Ground Mount)", name: "Structure Drawings (Ground Mount)" },
            ]



export const components_data  = [{ id: "Solar PV Module" , name: "Solar PV Module" }, 
            { id: "Solar Inverter", name: "Solar Inverter" }, 
            { id: "Module Mounting Structures with Accessories ( RCC)", name: "Module Mounting Structures with Accessories ( RCC)" }, 
            { id: "Cabling Accessories", name: "Cabling Accessories" }, 
            { id: "Lighting Arrestor", name: "Lighting Arrestor" },
            { id: "HT Panel", name: "HT Panel" },
            { id: "Transformer", name: "Transformer" }, 
            { id: "LT Power Cable (From ACDB to Customer LT Panel)", name: "LT Power Cable (From ACDB to Customer LT Panel)" }, 
            { id: "Earthing Cable", name: "Earthing Cable" }, 
            { id: "DC Junction Box (SCB/SMB)", name: "DC Junction Box (SCB/SMB)" }, 
            { id: "Metering Panel", name: "Metering Panel" },
            { id: "Cable trays", name: "Cable trays" }, 
            { id: "Bus bar", name: "Bus bar" }, 
            { id: "Module Mounting Structures with Accessories ( Metal Sheet)", name: "Module Mounting Structures with Accessories ( Metal Sheet)" }, 
            { id: "Module Mounting Structures with Accessories ( Ground Mount)" , name: "Module Mounting Structures with Accessories ( Ground Mount)" }, 
            { id: "Structure Foundation Pedestal", name: "Structure Foundation Pedestal" },
            { id: "Earthing System", name: "Earthing System" },
            { id: "LT Panel", name: "LT Panel" }, 
            { id: "AC Distribution (Combiner) Panel Board", name: "AC Distribution (Combiner) Panel Board" }, 
            { id: "LT Power Cable (From inverters to ACDB)", name: "LT Power Cable (From inverters to ACDB)" }, 
            { id: "LT Power Cable (From metering cubical to MDB panel)", name: "LT Power Cable (From metering cubical to MDB panel)" }, 
            { id: "Module Earthing Cable", name: "Module Earthing Cable" },
            { id: "GI Strip", name: "GI Strip" }, 
            { id: "MDB Breaker", name: "MDB Breaker" }, 
            { id: "HT Breaker", name: "HT Breaker" }, 
            { id: "InC Contractor", name: "InC Contractor" }
          ]


export const accessories_data  = [{ id: "Reverse Protection Relay", name: "Reverse Protection Relay" }, 
            { id: "DG Synchronization", name: "DG Synchronization" }, 
            { id: "Safety Rails", name: "Safety Rails" }, 
            { id: "Safety Lines", name: "Safety Lines" }, 
            { id: "Pyro Meter", name: "Pyro Meter" },
            { id: "Ambient Temperature Sensors", name: "Ambient Temperature Sensors" },
            { id: "Module Temperature Sensors", name: "Module Temperature Sensors" }, 
            { id: "Cleaning System", name: "Cleaning System" }, 
            { id: "Fire Extingusiher", name: "Fire Extingusiher" }, 
            { id: "Zero Export Device", name: "Zero Export Device" }, 
            { id: "Control Room", name: "Control Room" },
            { id: "Net-Metering", name: "Net-Metering" }, 
            { id: "Communication Cable", name: "Communication Cable" }, 
            { id: "Walkways", name: "Walkways" }, 
            { id: "SCADA System", name: "SCADA System" }, 
            { id: "Weather Monitoring Unit", name: "Weather Monitoring Unit" },
            { id: "UPS", name: "UPS" },
            { id: "Wire mesh for protection of Skylights", name: "Wire mesh for protection of Skylights" }, 
            { id: "Danger Board and Signs", name: "Danger Board and Signs" }, 
            { id: "Generation Meter", name: "Generation Meter" }, 
            { id: "Other Sensors", name: "Other Sensors" }
        ]
           
export const area_data = [{ id: "Ground Mount", name: "Ground Mount" }, 
              { id: "Metal Sheet", name: "Metal Sheet" }, 
              { id: "RCC", name: "RCC" }
            ]
















const [Rooftop, setRooftop] = useState(false);
const [Open_Access, setOpen_Access] = useState(false);
const [Ground_Mounted, setGround_Mounted] = useState(false);
const [Industrial, setIndustrial] = useState(false);
const [Commercial, setCommercial] = useState(false);
const [Residential, setResidential] = useState(false);




const [Array_Layout, setArray_Layout] = useState(false);
const [Civil_Layout, setCivil_Layout] = useState(false);
const [PV_Syst, setPV_Syst] = useState(false);
const [Communication, setCommunication] = useState(false);
const [Equipment_Layout, setEquipment_Layout] = useState(false);
const [Walkways, setWalkways] = useState(false);
const [Earthing_LA, setEarthing_LA] = useState(false);
const [Safety_Line, setSafety_Line] = useState(false);
const [Cable_Layout, setCable_Layout] = useState(false);
const [Safety_Rails, setSafety_Rails] = useState(false);
const [Structure_Drawings_RCC, setStructure_Drawings_RCC] = useState(false);
const [Project_SLD, setProject_SLD] = useState(false);
const [Structure_Drawings_Metal, setStructure_Drawings_Metal] = useState(false);
const [Final_BOQ, setFinal_BOQ] = useState(false);
const [Structure_Drawings_Ground, setStructure_Drawings_Ground] = useState(false);
const [Detailed_Project_Report, setDetailed_Project_Report] = useState(false);




const [Solar_PV_Module, setSolar_PV_Module] = useState(false);
const [Module_Metal_Sheet, setModule_Metal_Sheet] = useState(false);
const [Solar_Inverter, setSolar_Inverter] = useState(false);
const [Module_Ground_Mount, setModule_Ground_Mount] = useState(false);
const [Module_RCC, setModule_RCC] = useState(false);
const [Structure_Foundation_Pedestal, setStructure_Foundation_Pedestal] = useState(false);
const [Cabling_Accessories, setCabling_Accessories] = useState(false);
const [Earthing_System, setEarthing_System] = useState(false);
const [Lighting_Arrestor, setLighting_Arrestor] = useState(false);
const [LT_Panel, setLT_Panel] = useState(false);
const [HT_Panel, setHT_Panel] = useState(false);
const [AC_Distribution, setAC_Distribution] = useState(false);
const [Transformer, setTransformer] = useState(false);
const [LT_From_inverters, setLT_From_inverters] = useState(false);
const [LT_From_ACDB, setLT_From_ACDB] = useState(false);
const [LT_From_metering, setLT_From_metering] = useState(false);
const [Earthing_Cable, setEarthing_Cable] = useState(false);
const [Module_Earthing_Cable, setModule_Earthing_Cable] = useState(false);
const [DC_Junction_Box, setDC_Junction_Box] = useState(false);
const [GI_Strip, setGI_Strip] = useState(false);
const [Metering_Panel, setMetering_Panel] = useState(false);
const [MDB_Breaker, setMDB_Breaker] = useState(false);
const [Cable_trays, setCable_trays] = useState(false);
const [HT_Breaker, setHT_Breaker] = useState(false);
const [Bus_bar, setBus_bar] = useState(false);
const [InC_Contractor, setInC_Contractor] = useState(false);




const [Reverse_Protection_Relay, setReverse_Protection_Relay] = useState(false);
const [Net_Metering, setNet_Metering] = useState(false);
const [DG_Synchronization, setDG_Synchronization] = useState(false);
const [Communication_Cable, setCommunication_Cable] = useState(false);
const [ASafety_Rails, setASafety_Rails] = useState(false);
const [AWalkways, setAWalkways] = useState(false);
const [Safety_Lines, setSafety_Lines] = useState(false);
const [SCADA_System, setSCADA_System] = useState(false);
const [Pyro_Meter, setPyro_Meter] = useState(false);
const [Weather_Monitoring_Unit, setWeather_Monitoring_Unit] = useState(false);
const [Ambient_Temperature_Sensors, setAmbient_Temperature_Sensors] = useState(false);
const [UPS, setUPS] = useState(false);
const [Module_Temperature_Sensors, setModule_Temperature_Sensors] = useState(false);
const [Wire_mesh_for_protection, setWire_mesh_for_protection] = useState(false);
const [Cleaning_System, setCleaning_System] = useState(false);
const [Danger_Board_and_Signs, setDanger_Board_and_Signs] = useState(false);
const [Fire_Extingusiher, setFire_Extingusiher] = useState(false);
const [Generation_Meter, setGeneration_Meter] = useState(false);
const [Zero_Export_Device, setZero_Export_Device] = useState(false);
const [Other_Sensors, setOther_Sensors] = useState(false);
const [Control_Room, setControl_Room] = useState(false);


const [Ground_Mount, setGround_Mount] = useState(false);
const [RCC, setRCC] = useState(false);
const [Metal_Sheet, setMetal_Sheet] = useState(false);


