import React, { useEffect, useState } from 'react';
import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    PermissionsAndroid,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    Picker,
    ListItem

} from 'react-native';
import { Searchbar, Button, Switch } from 'react-native-paper';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import * as actionCreator from '../../actions/team-action/index';
import { useDispatch, useSelector } from "react-redux";
import GetTeams from '../../components/teams/GetTeams';
import { FlatList } from 'react-native-gesture-handler';
import FastImage from 'react-native-fast-image'
import NewProjectHeader from '../EmptyScreens/NewProjectHeader';
import { SearchBar, Badge, BottomSheet } from 'react-native-elements';
import { Checkbox, RadioButton } from 'react-native-paper';
import ResponsibilityMatrix from './ResponsibilityMatrix';
import notifyMessage from '../../ToastMessages';
import { IndexPath, Layout, Select, SelectGroup, SelectItem } from '@ui-kitten/components';
import Toast from 'react-native-toast-message';


const TeamDetails = ({ props, route, navigation }) => {
    const { project, image, customer, TeamDetails, subContractorReducer, token } = route.params;
    const dispatch = useDispatch();

    const onChangeSearch = query => setSearchQuery(query);
    const [Checkedteam, setChecked] = useState([]);
    const [Checkedrole, setCheckedrole] = useState([]);
    const [Team, setTeam] = useState(TeamDetails);
    const [role, setrole] = useState([]);
    const [onStockButtonPress, setonStockButtonPress] = useState(true);
    const [onConsumedButtonPress, setonConsumedButtonPress] = useState(false);
    const [isVisible, setIsVisible] = useState(false);

  

    
    const data =  [
        "Customer",
        "Project Manager",
        "Sales",
        "Liasoning Manager",
        "Liasoning Officer",
        "Lead Designer",
        "Procurement Manager",
        "Procurement Executive",
        "Logistics Manager",
        "Logistics Executive",
        "Safety Manager",
        "Site In-Charge",
        "Site Executive",
        "O&M Manager",
        "O&M Executive",
    ];
    const renderOption = (title) => (
        <SelectItem title={title}/>
      );;


    const onPressStock = (params) => {
        setonStockButtonPress(true)
        setonConsumedButtonPress(false)
    }

    const onPressConsumed = (params) => {
        setonConsumedButtonPress(true)
        setonStockButtonPress(false)
    }
   


    const onPressChecked = (items, j, role1) => {
        if (role1 != '') {

            const rightsValues = role1.map(index => {
          return data[index.row];
      });
    
      
            let idrole={
                id:items.id,
                roles:rightsValues
            }
            setChecked([...Checkedteam, items])
            setCheckedrole([...Checkedrole, idrole])

            Team.splice(j, 1);
            setrole([])
        } else {
            Toast.show({
                type: 'success',
                text1: 'rights',
                text2: 'Please assign a role to the team member before selecting.'
              });
        }

  
    }

    const onPressUnchecked = (items, i) => {
        setTeam([...Team, items])
        Checkedteam.splice(i, 1);
        Checkedrole.splice(i, 1);
   

    }


    const handleNext = () => {
        if (Checkedteam != '') {

        //"Checkedteam",Checkedteam)

        // //("team Checkedrole",{team:Checkedrole})
        navigation.navigate('SubContractorDetails', {
            project: project,
            team:  {"team_members":Checkedrole},
            subContractorReducer: subContractorReducer,
            image: image,
            customer: customer
        })
    } else {
        notifyMessage("Please select a team member!");
    }
    }
    const Responsibility = () => {

        return (
            <SafeAreaView>
                            <View style={styles.container1}>

                    <ScrollView>
                        <View style={styles.parent2}>
                            <View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: wp('90%') }}>
                                <Text style={{ fontFamily: 'SourceSansPro-Semibold', fontSize: 20, color: 'rgb(68,67,67)' }}>Responsibility Matrix</Text>
                                <TouchableOpacity onPress={() => setIsVisible(false) } >
    
                                <FastImage
                                    style={styles.image_logo}
                                    source={require('../../assets/icons/Remove.png')
                                    }
                                    resizeMode={FastImage.resizeMode.contain} />
                                    </TouchableOpacity>
    
                            </View>
                            <View>
                            <Text style={{ height: hp('0.2%'), marginTop: 10, marginBottom: 5, backgroundColor: 'rgb(232, 232, 232)' }}></Text>
    
                            </View>
                            </View>
                            <View>
                                <View style={styles.card1}>
                                    <Text style={{ fontFamily: 'SourceSansPro-Semibold', fontSize: 16, color: 'rgb(68,67,67)' }}>Project Manager</Text>
                                    <Text style={{ height: hp('0.2%'), marginTop: 10, marginBottom: 5, backgroundColor: 'rgb(232, 232, 232)' }}></Text>
    
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                        <Text style={styles.txt}>Can View</Text>
                                        <Text style={styles.textblue}>Procurement</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                        <Text style={styles.txt}>Can Edit</Text>
                                        <Text style={styles.textblue}>Array Layout</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                        <Text style={styles.txt}>Can Approve</Text>
                                        <Text style={styles.textblue}>Engineering</Text>
                                    </View>
                                </View>
                            </View>
    
                            <View>
                                <View style={styles.card1}>
                                    <Text style={{ fontFamily: 'SourceSansPro-Semibold', fontSize: 16, color: 'rgb(68,67,67)' }}>Design Head</Text>
                                    <Text style={{ height: hp('0.2%'), marginTop: 10, marginBottom: 5, backgroundColor: 'rgb(232, 232, 232)' }}></Text>
    
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                        <Text style={styles.txt}>Can View</Text>
                                        <Text style={styles.textblue}>Procurement</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                        <Text style={styles.txt}>Can Edit</Text>
                                        <Text style={styles.textblue}>Array Layout</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                        <Text style={styles.txt}>Can Approve</Text>
                                        <Text style={styles.textblue}>Engineering</Text>
                                    </View>
                                </View>
                            </View>
    
                            <View>
                                <View style={styles.card1}>
                                    <Text style={{ fontFamily: 'SourceSansPro-Semibold', fontSize: 16, color: 'rgb(68,67,67)' }}>Project Manager</Text>
                                    <Text style={{ height: hp('0.2%'), marginTop: 10, marginBottom: 5, backgroundColor: 'rgb(232, 232, 232)' }}></Text>
    
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                        <Text style={styles.txt}>Can View</Text>
                                        <Text style={styles.textblue}>Procurement</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                        <Text style={styles.txt}>Can Edit</Text>
                                        <Text style={styles.textblue}>Array Layout</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                        <Text style={styles.txt}>Can Approve</Text>
                                        <Text style={styles.textblue}>Engineering</Text>
                                    </View>
                                </View>
                            </View>
    
                            <View>
                                <View style={styles.card1}>
                                    <Text style={{ fontFamily: 'SourceSansPro-Semibold', fontSize: 16, color: 'rgb(68,67,67)' }}>Design Head</Text>
                                    <Text style={{ height: hp('0.2%'), marginTop: 10, marginBottom: 5, backgroundColor: 'rgb(232, 232, 232)' }}></Text>
    
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                        <Text style={styles.txt}>Can View</Text>
                                        <Text style={styles.textblue}>Procurement</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                        <Text style={styles.txt}>Can Edit</Text>
                                        <Text style={styles.textblue}>Array Layout</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                                        <Text style={styles.txt}>Can Approve</Text>
                                        <Text style={styles.textblue}>Engineering</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={{height:30}}>
    
                            </View>
    
                        </View>
    
                    </ScrollView>
                    </View>
            </SafeAreaView>
        )
    }

    return (
        <SafeAreaView>
            <View style={styles.container}>
                <NewProjectHeader {...props} title='Team Details' no='3' percents={50} />
                <ScrollView>
                    <View style={styles.parent2}>

                        <SearchBar
                            showLoading={false}
                            platform={Platform.OS}
                            containerStyle={{ borderWidth: 1, borderRadius: 4, borderColor: 'rgb(232, 232, 232)', backgroundColor: 'white', justifyContent: 'center', width: wp('80%'), height: hp('6%') }}
                            clearIcon={true}
                            onChangeText={onChangeSearch}
                            placeholder=''
                            cancelButtonTitle='Cancel'
                        />
                        <View style={{ marginTop: 20, marginBottom: 10, height: hp('6%'), display: 'flex', flexDirection: 'row', justifyContent: 'center', alignSelf: 'center' }}>
                            <TouchableOpacity
                                onPress={() => { onPressStock() }}
                                style={[styles.btn, { backgroundColor: onStockButtonPress ? '#3d87f1' : 'white' }]}
                            >
                                <Text style={[styles.btnText, { color: onStockButtonPress ? 'white' : 'rgb(143, 143, 143)' }]}>Full Team</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={() => { onPressConsumed() }}
                                style={[styles.btn, { backgroundColor: onConsumedButtonPress ? '#3d87f1' : 'white', marginLeft: -5 }]}
                            >
                                <Text style={[styles.btnText, { color: onConsumedButtonPress ? 'white' : 'rgb(143, 143, 143)' }]}>Selected</Text>
                            </TouchableOpacity>
                            <Badge
                                status="primary"
                                value={Checkedteam.length}
                                containerStyle={{ position: 'absolute', top: -4, right: -4 }}
                            />


                        </View>
                        {onStockButtonPress ?
                            <>
                                <FlatList
                                    data={Team}
                                    renderItem={
                                        ({ item, index }) => (
                                            <View style={styles.card}>
                                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                    <View>
                                                        <Text style={styles.name_text}>{item.name}{item.id}</Text>
                                                        <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>{item.department}</Text>
                                                    </View>
                                                    <View>
                                                        <TouchableOpacity
                                                            onPress={() => { onPressChecked(item, index, role) }}>
                                                            <FastImage
                                                                style={styles.image_logo}
                                                                source={require('../../assets/icons/Uncheck.png')
                                                                }
                                                                resizeMode={FastImage.resizeMode.contain}
                                                            />
                                                        </TouchableOpacity>
                                                    </View>
                                                </View>
                                                <Text style={{ height: hp('0.2%'), marginTop: 25, marginBottom: 8, backgroundColor: 'rgb(232, 232, 232)' }}></Text>
                                                <View style={{ display: 'flex', marginBottom: 10, flexDirection: 'row', alignItems: 'center' }}>
                                                    <View style={{ backgroundColor: 'rgb(232,232,232)' }}>
                                                    <View style={{ width: 150, height: 40, color: 'rgb(143,143,143)', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', fontSize: 12 }}>
                                                            <Select
                                                                key={index + item.id}
                                                                style={styles.select}
                                                                multiSelect={true}
                                                                placeholder="Select Rights"
                                                                value="Select Rights"
                                                                selectedIndex={role}
                                                                onSelect={index => setrole(index)}>
                                                                {data.map(renderOption)}
                                                            </Select>
                                                            </View>
                                                    </View>
                                                    <View style={{ backgroundColor: 'rgb(232,232,232)', marginLeft: 15, width: wp('11%'), height: hp('5%'), padding: 4 }}>
                                                        <TouchableOpacity onPress={() => setIsVisible(true) } >
                                                            < FastImage
                                                                style={{ width: wp('8%'), height: hp('3%'), marginTop: 3 }}
                                                                source={require('../../assets/icons/info.png')}
                                                                resizeMode={FastImage.resizeMode.contain}
                                                            />
                                                        </TouchableOpacity>
                                                        <BottomSheet
                                                            isVisible={isVisible}
                                                            containerStyle={{ backgroundColor: 'rgba(0.5, 0.25, 0, 0.2)' }}
                                                        >
                                                            <Responsibility/>
                                                        </BottomSheet>
                                                    </View>
                                                </View>
                                            </View>
                                        )
                                    }
                                />
                            </> :
                            <>
                                {Checkedteam != '' ?
                                    <>
                                        <FlatList
                                            data={Checkedteam}
                                            renderItem={
                                                ({ item, index }) => (
                                                    <View style={styles.card}>
                                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                            <View>
                                                                <Text style={styles.name_text}>{item.name}</Text>
                                                                <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>{item.department}</Text>
                                                            </View>
                                                            <View>
                                                                <TouchableOpacity
                                                                    onPress={() => { onPressUnchecked(item, index) }}>
                                                                    <FastImage
                                                                        style={styles.image_logo}
                                                                        source={require('../../assets/icons/Check.png')
                                                                        }
                                                                        resizeMode={FastImage.resizeMode.contain}
                                                                    />
                                                                </TouchableOpacity>
                                                            </View>
                                                        </View>
                                                        <Text style={{ height: hp('0.2%'), marginTop: 25, marginBottom: 8, backgroundColor: 'rgb(232, 232, 232)' }}></Text>
                                                        <View style={{ display: 'flex', marginBottom: 10, flexDirection: 'row', alignItems: 'center' }}>
                                                            <View>
                                                                <Text style={{ color: 'rgb(68, 67, 67)', fontFamily: 'SourceSansPro-Regular', fontSize: 12 }}>Email</Text>
                                                                <Text style={{ color: 'rgb(143, 143, 143)', fontFamily: 'SourceSansPro-Regular', fontSize: 12 }}>{item.email}</Text>
                                                            </View>
                                                        </View>
                                                    </View>
                                                )
                                            }
                                        />
                                    </>
                                    :
                                    <>
                                        <View style={{ width: wp('80%'), height: hp('50%'), marginTop: 3, alignItems: 'center' }}>
                                            <View >
                                                < FastImage
                                                    style={{ width: wp('80%'), height: hp('50%'), marginTop: 3 }}
                                                    source={require('../../assets/icons/NoTeam.png')}
                                                    resizeMode={FastImage.resizeMode.contain}
                                                />
                                            </View>
                                            <View>
                                                <Text style={{ fontFamily: 'SourceSansPro-Regular', fontSize: 20, color: 'rgb(143,143,143)' }}>No Team Members Selected!</Text>
                                            </View>
                                            <View style={{ padding: 20, alignItems: 'center' }}>
                                                <Text style={{ fontFamily: 'SourceSansPro-Regular', fontSize: 14, color: 'rgb(190,198,214)' }}>Please select a new Team Member
                                                </Text><Text style={{ fontFamily: 'SourceSansPro-Regular', fontSize: 14, color: 'rgb(190,198,214)' }}> with designated roles</Text>
                                            </View>
                                        </View>
                                    </>

                                }
                            </>

                        }

                    </View>
                </ScrollView>
                <View style={[styles.shadowContainerStyle1, {
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: hp('15%'),
                    width: wp('150%'),
                }]}>
                    <TouchableOpacity
                        onPress={() => navigation.goBack()}>
                        <View style={{ borderColor: 'rgb(61,135,241)', justifyContent: 'center', alignItems: 'center', borderWidth: 1, marginTop: -20, borderRadius: 5, height: hp('7'), width: wp('30%') }}>
                            <Text style={{ color: 'rgb(61,135,241)' }}>Previous</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => handleNext()}

                    >
                        <View style={{ backgroundColor: 'rgb(61,135,241)', marginLeft: 10, justifyContent: 'center', alignItems: 'center', marginTop: 1 - 20, borderRadius: 5, height: hp('7'), width: wp('30%') }}>
                            <Text style={{ color: '#fff' }}>Next</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Text style={{ textDecorationLine: 'underline', color: 'rgb(61,135,241)', marginLeft: 10, marginTop: 1 - 20, fontFamily: 'SourceSansPro-Regular' }}>Save for later</Text>
                    </TouchableOpacity>

                </View>
            </View>

        </SafeAreaView>

    )
};
const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
    container1: {
        width: Dimensions.get('window').width,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
    shadowContainerStyle1: {   //<--- Style with elevation

        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 510 },
        shadowOpacity: 20,
        shadowRadius: 300,
        elevation: 3,
    },
    card: {   //<--- Style with elevation
        borderLeftColor: 'rgb(61,135,241)',
        borderLeftWidth: 5,
        borderRadius: 5,
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        elevation: 3,
        width: wp('90%'),
        margin: 10,
        height: 160,
        padding: 20
        //  justifyContent: 'center',
        //  alignItems:'center'
    },
    select: {
        // color: 'rgb(232,232,232)',
        margin: 2,
        width:160,
      },
    card1: {   //<--- Style with elevation
        
        borderRadius: 5,
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        elevation: 3,
        width: wp('90%'),
        margin: 10,
        height: 170,
        padding: 20
        //  justifyContent: 'center',
        //  alignItems:'center'
    },

    image_logo: {
        width: wp('7%'),
        height: hp('3.5%'),
    },
    parent2: {
        // height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
        flexDirection: 'row',
        marginTop: hp('3%'),
        // justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        backgroundColor: 'white',
        flexDirection: 'column'
    },
    option_text: {
        fontSize: hp('1.2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center'
    },
    signIn: {
        width: wp('74%'),
        height: hp('8%'),
        left: 25,
        top: 80,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        color: '#05375a',
        backgroundColor: '#3A80E3',
        borderRadius: 5,
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: 'SourceSansPro-Regular',
    },
    name_text: {
        fontSize: 16,
        fontWeight: 'bold',
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68,67,67)'
    },
    temail_textxt: {
        fontSize: 14,
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(143,143,143)'
    },
    txt: {
        fontSize: 14,
      fontFamily: 'SourceSansPro-Semibold',
        color: 'rgb(68,67,67)'
    },
    txtunder: {
        fontSize: hp('2.5%'),
        textDecorationLine: 'underline',
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(61,135,241)'
    },
    textblue: {
        fontSize: 14,
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(61,135,241)'
    },
    textInput1: {
        marginTop: hp('30%'),
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        width: wp('79.4%'),
        height: hp('7.4%'),
        borderRadius: 4,
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'black',
        textAlign: 'left',
        paddingHorizontal: 10,
        marginBottom: Platform.OS === 'ios' ? 6 : 8
    },
    textInput: {
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        width: wp('79.4%'),
        height: hp('7.4%'),
        borderRadius: 4,
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'black',
        textAlign: 'left',
        paddingHorizontal: 10,
        marginBottom: Platform.OS === 'ios' ? 6 : 8
    },
    btnText: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular'
    },

    btn: {
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        width: wp('40%'),
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center'
    },

});
export default TeamDetails