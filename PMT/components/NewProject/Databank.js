import React, { useEffect, useState } from 'react';
import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    PermissionsAndroid,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    Picker

} from 'react-native';
import { Searchbar, Button, Switch } from 'react-native-paper';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import * as actionCreator from '../../actions/docs-action/index';
import { useDispatch, useSelector } from 'react-redux';
import { FlatList } from 'react-native-gesture-handler';
import FastImage from 'react-native-fast-image'
import NewProjectHeader from '../EmptyScreens/NewProjectHeader';
import { SearchBar,Badge } from 'react-native-elements';
import { Checkbox, RadioButton } from 'react-native-paper';
import DocumentPicker from 'react-native-document-picker';
import notifyMessage from '../../ToastMessages';


const Databank = ({ props, route, navigation }) => {
    const { project, image, customer, TeamDetails, subContractorReducer,is_sub_contractor, token } = route.params;
    const onChangeSearch = query => setSearchQuery(query);
    const [checked, setChecked] = React.useState(false);
    const [role, setrole] = React.useState('');
    const [Databank, setDatabank] = useState(1);

    const [purchase, setPurchase] = useState('');
    const [electricity, setElectricity] = useState('');
    const [autocad, setAutocad] = useState('');
    const [SLD, setSld] = useState('');
    const [BOM, setBom] = useState('');
    const [site, setSite] = useState('');
    const [escalation, setEscalation] = useState('');
    const [onStockButtonPress, setonStockButtonPress] = React.useState(true);
    const [onConsumedButtonPress, setonConsumedButtonPress] = React.useState(false);
    const rolelist = [{ id: 1, rolename: 'owner' }, { id: 2, rolename: 'owner1' }];
    const [count, setcount]= useState(0);
    const onPressStock = (params) => {
        setonStockButtonPress(true)
        setonConsumedButtonPress(false)
    }

    const onPressConsumed = (params) => {
        setonConsumedButtonPress(true)
        setonStockButtonPress(false)
    }

    const onPurchase = async()=>{
        try {
            const res = await DocumentPicker.pick({
              type: [DocumentPicker.types.allFiles],
            });
            setcount(count+1)
            setPurchase(res)
          } catch (err) {
            if (DocumentPicker.isCancel(err)) {
            } else {
              throw err;
            }
          }
    }
    
    const onElectricity = async()=>{
        try {
            const res = await DocumentPicker.pick({
              type: [DocumentPicker.types.allFiles],
            });
            setcount(count+1)
            setElectricity(res)
          } catch (err) {
            if (DocumentPicker.isCancel(err)) {
            } else {
              throw err;
            }
          }
    }
    const onAutocad = async()=>{
        try {
            const res = await DocumentPicker.pick({
              type: [DocumentPicker.types.allFiles],
            });
            setcount(count+1)
            setAutocad(res)
          } catch (err) {
            if (DocumentPicker.isCancel(err)) {
            } else {
              throw err;
            }
          }
    }
    const onSld = async()=>{
        try {
            const res = await DocumentPicker.pick({
              type: [DocumentPicker.types.allFiles],
            });
            setcount(count+1)
            setSld(res)
          } catch (err) {
            if (DocumentPicker.isCancel(err)) {
            } else {
              throw err;
            }
          }
    }
    const onBom = async()=>{
        try {
            const res = await DocumentPicker.pick({
              type: [DocumentPicker.types.allFiles],
            });
            setcount(count+1)
            setBom(res)
          } catch (err) {
            if (DocumentPicker.isCancel(err)) {
            } else {
              throw err;
            }
          }
    }
    const onSite = async()=>{
        try {
            const res = await DocumentPicker.pick({
              type: [DocumentPicker.types.allFiles],
            });
            setcount(count+1)
            setSite(res)
          } catch (err) {
            if (DocumentPicker.isCancel(err)) {
            } else {
              throw err;
            }
          }
    }
    const onEscalation = async()=>{
        try {
            const res = await DocumentPicker.pick({
              type: [DocumentPicker.types.allFiles],
            });
            setcount(count+1)
            setEscalation(res)
          } catch (err) {
            if (DocumentPicker.isCancel(err)) {
            } else {
              throw err;
            }
          }
    }

    const handleNext = () => {
        if (purchase != '' || escalation != '' || electricity != '' || BOM != '' || site != '' || autocad != ''|| SLD != '') {

        navigation.navigate('ProjectSpecification1', {
            project: project,
            TeamDetails: TeamDetails,
            is_sub_contractor:is_sub_contractor,
            subContractorReducer: subContractorReducer,
            image: image,
            customer: customer,
            purchase_order_file:purchase,
            escalation_matrix_file:escalation,
            electricity_bill_file:electricity,
            bom_file:BOM,
            site_checklist_file:site,
            autocad_file:autocad,
            electrical_SLD_file:SLD,
        })
    } else {
        notifyMessage("Please select a document!");
    }
    }
    return (
        <SafeAreaView>
            <View style={styles.container}>
                <NewProjectHeader {...props} title='Data Bank' no='5' percents={83.3} />
                <ScrollView>
                    <View style={styles.parent2}>
                        <>
                        {onStockButtonPress ?
                        <View>
                            <Text style={{ fontFamily: 'SourceSansPro-Regular', fontSize: 16, color: 'rgb(68,67,67)' }}><Text style={{ color: 'rgb(247,90,90)' }}>*</Text>User can add up to 3 electricity bills</Text>
                        </View>
                        :
                        <View>
                            
                        </View>
                        }
                        <View style={{ marginTop: 20, marginBottom: 10, height: hp('6%'), display: 'flex', flexDirection: 'row', justifyContent: 'center', alignSelf: 'center' }}>
                            <TouchableOpacity
                                onPress={() => { onPressStock() }}
                                style={[styles.btn, { backgroundColor: onStockButtonPress ? '#3d87f1' : 'white' }]}
                            >
                                <Text style={[styles.btnText, { color: onStockButtonPress ? 'white' : 'rgb(143, 143, 143)' }]}>Document List</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={() => { onPressConsumed() }}
                                style={[styles.btn, { backgroundColor: onConsumedButtonPress ? '#3d87f1' : 'white', marginLeft: -5 }]}
                            >
                                <Text style={[styles.btnText, { color: onConsumedButtonPress ? 'white' : 'rgb(143, 143, 143)' }]}>Upload Files</Text>
                            </TouchableOpacity>
                            <Badge
                                status="primary"
                                value={count}
                                containerStyle={{ position: 'absolute', top: -4, right: -4 }}
                            />
                        </View>
                        </>
                        {onStockButtonPress ?
                            <>
                            {purchase?  
                                <View style={styles.textInput1}>
                                    <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>Purchase Order</Text>
                                    <TouchableOpacity
                                     style={styles.uploadbtn1}>
                                        <Text style={styles.uploadbtnText1}>UPLOAD</Text>
                                    </TouchableOpacity>
                                </View>
:
                                <View style={styles.textInput1}>
                                    <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>Purchase Order</Text>
                                    <TouchableOpacity
                                    onPress={()=>{onPurchase()}} style={styles.uploadbtn}>
                                        <Text style={styles.uploadbtnText}>UPLOAD</Text>
                                    </TouchableOpacity>
                                </View>
}
{electricity?  
                                <View style={styles.textInput1}>
                                    <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>Electricity Bill<Text style={{ color: 'rgb(247,90,90)' }}>*</Text></Text>
                                    <TouchableOpacity
                                     style={styles.uploadbtn1}>
                                        <Text style={styles.uploadbtnText1}>UPLOAD</Text>
                                    </TouchableOpacity>
                                </View>
:
                                <View style={styles.textInput1}>
                                    <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>Electricity Bill<Text style={{ color: 'rgb(247,90,90)' }}>*</Text></Text>
                                    <TouchableOpacity
                                    onPress={()=>{onElectricity()}} style={styles.uploadbtn}>
                                        <Text style={styles.uploadbtnText}>UPLOAD</Text>
                                    </TouchableOpacity>
                                </View>
}
{autocad?  
                                <View style={styles.textInput1}>
                                    <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>Auto CAD Files</Text>
                                    <TouchableOpacity
                                     style={styles.uploadbtn1}>
                                        <Text style={styles.uploadbtnText1}>UPLOAD</Text>
                                    </TouchableOpacity>
                                </View>
:
                                <View style={styles.textInput1}>
                                    <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>Auto CAD Files</Text>
                                    <TouchableOpacity
                                    onPress={()=>{onAutocad()}} style={styles.uploadbtn}>
                                        <Text style={styles.uploadbtnText}>UPLOAD</Text>
                                    </TouchableOpacity>
                                </View>
}

{SLD?  
                                <View style={styles.textInput1}>
                                    <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>Electrical SLD</Text>
                                    <TouchableOpacity
                                     style={styles.uploadbtn1}>
                                        <Text style={styles.uploadbtnText1}>UPLOAD</Text>
                                    </TouchableOpacity>
                                </View>
:
                                <View style={styles.textInput1}>
                                    <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>Electrical SLD</Text>
                                    <TouchableOpacity
                                    onPress={()=>{onSld()}} style={styles.uploadbtn}>
                                        <Text style={styles.uploadbtnText}>UPLOAD</Text>
                                    </TouchableOpacity>
                                </View>
}

{BOM?  
                                <View style={styles.textInput1}>
                                    <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>BOM</Text>
                                    <TouchableOpacity
                                     style={styles.uploadbtn1}>
                                        <Text style={styles.uploadbtnText1}>UPLOAD</Text>
                                    </TouchableOpacity>
                                </View>
:
                                <View style={styles.textInput1}>
                                    <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>BOM</Text>
                                    <TouchableOpacity
                                    onPress={()=>{onBom()}} style={styles.uploadbtn}>
                                        <Text style={styles.uploadbtnText}>UPLOAD</Text>
                                    </TouchableOpacity>
                                </View>
}

{site?  
                                <View style={styles.textInput1}>
                                    <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>Site Checklist</Text>
                                    <TouchableOpacity
                                     style={styles.uploadbtn1}>
                                        <Text style={styles.uploadbtnText1}>UPLOAD</Text>
                                    </TouchableOpacity>
                                </View>
:
                                <View style={styles.textInput1}>
                                    <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>Site Checklist</Text>
                                    <TouchableOpacity
                                    onPress={()=>{onSite()}} style={styles.uploadbtn}>
                                        <Text style={styles.uploadbtnText}>UPLOAD</Text>
                                    </TouchableOpacity>
                                </View>
}

{escalation?  
                                <View style={styles.textInput1}>
                                    <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>Escalation Matrix</Text>
                                    <TouchableOpacity
                                     style={styles.uploadbtn1}>
                                        <Text style={styles.uploadbtnText1}>UPLOAD</Text>
                                    </TouchableOpacity>
                                </View>
:
                                <View style={styles.textInput1}>
                                    <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>Escalation Matrix</Text>
                                    <TouchableOpacity
                                    onPress={()=>{onEscalation()}} style={styles.uploadbtn}>
                                        <Text style={styles.uploadbtnText}>UPLOAD</Text>
                                    </TouchableOpacity>
                                </View>
}
                           


                            </> :
                            <>
                                {purchase == '' && electricity == '' && autocad == '' && SLD == '' && BOM == '' && site == '' && escalation == ''?
                                    <View style={{ width: wp('80%'), height: hp('50%'), marginTop: 3, alignItems: 'center' }}>
                                        <View >
                                            < FastImage
                                                style={{ width: wp('80%'), height: hp('50%'), marginTop: 3 }}
                                                source={require('../../assets/icons/NoFiles.png')}
                                                resizeMode={FastImage.resizeMode.contain}
                                            />
                                        </View>
                                        <View>
                                            <Text style={{ fontFamily: 'SourceSansPro-Regular', fontSize: 20, color: 'rgb(143,143,143)' }}>No files have been attached.</Text>
                                        </View>
                                        <View style={{ padding: 20, alignItems: 'center' }}>
                                            <Text style={{ fontFamily: 'SourceSansPro-Regular', fontSize: 14, color: 'rgb(190,198,214)' }}>Please attach files to complete the process

                                           </Text>
                                        </View>
                                    </View>
                                    : <>
{purchase?

                                    
                                        <View>
                                            <Text style={{ color: 'rgb(68,67,67)', fontFamily:'SourceSansPro-Regular', fontSize: 16, marginLeft: 5, marginTop: 15 }}>Purchase Order</Text>
                                            <View style={[styles.textInput1, { marginTop: 5 }]}>
                                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                    <TouchableOpacity style={styles.pdfbtn1}>
                                                        <Text style={styles.uploadbtnText1}>PDF</Text>
                                                    </TouchableOpacity>
                                                    <View>
                                                    <Text style={{ color: 'rgb(68,67,67)', fontFamily:'SourceSansPro-Semibold', fontSize: 14, marginLeft: 10 }}>purchase_order.pdf</Text>
                                                    <Text style={{ color: 'rgb(143, 143, 143)', fontFamily:'SourceSansPro-Regular', fontSize: 12, marginLeft: 10 }}>1.2 KB</Text>
                                                    </View>
                                                </View>
                                                <View>
                                                    <TouchableOpacity onPress={()=>{setPurchase(''), setcount(count-1)}} >
                                                    <FastImage
                                                        style={styles.image_logo}
                                                        source={require('../../assets/icons/Remove.png')
                                                        }
                                                        resizeMode={FastImage.resizeMode.contain}
                                                    />
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>
:
<>
</>}
{electricity?
                                        <View>
                                            <Text style={{ color: 'rgb(68,67,67)', fontFamily:'SourceSansPro-Regular', fontSize: 16, marginLeft: 5, marginTop: 15 }}>Electricity Bill(3 Files)</Text>
                                            <View style={[styles.textInput1, { marginTop: 5 }]}>
                                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                    <TouchableOpacity style={styles.pdfbtn1}>
                                                        <Text style={styles.uploadbtnText1}>PDF</Text>
                                                    </TouchableOpacity>
                                                    <View>
                                                    <Text style={{ color: 'rgb(68,67,67)', fontFamily:'SourceSansPro-Semibold', fontSize: 14, marginLeft: 10 }}>ebill1_order.pdf</Text>
                                                    <Text style={{ color: 'rgb(143, 143, 143)', fontFamily:'SourceSansPro-Regular', fontSize: 12, marginLeft: 10 }}>1.2 KB</Text>
                                                    </View>
                                                </View>
                                                <View>
                                                <TouchableOpacity onPress={()=>{setElectricity(''), setcount(count-1)}} >
                                                    <FastImage
                                                        style={styles.image_logo}
                                                        source={require('../../assets/icons/Remove.png')
                                                        }
                                                        resizeMode={FastImage.resizeMode.contain}
                                                    />
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>
:
<>
</>}
{autocad?
                                        <View>
                                            <Text style={{ color: 'rgb(68,67,67)', fontFamily:'SourceSansPro-Regular', fontSize: 16, marginLeft: 5, marginTop: 15 }}>Auto CAD Files</Text>
                                            <View style={[styles.textInput1, { marginTop: 5 }]}>
                                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                    <TouchableOpacity style={styles.pdfbtn1}>
                                                        <Text style={styles.uploadbtnText1}>PDF</Text>
                                                    </TouchableOpacity>
                                                    <View>
                                                    <Text style={{ color: 'rgb(68,67,67)', fontFamily:'SourceSansPro-Semibold', fontSize: 14, marginLeft: 10 }}>CAD.dwg</Text>
                                                    <Text style={{ color: 'rgb(143, 143, 143)', fontFamily:'SourceSansPro-Regular', fontSize: 12, marginLeft: 10 }}>1.2 KB</Text>
                                                    </View>
                                                </View>
                                                <View>
                                                <TouchableOpacity onPress={()=>{setAutocad(''), setcount(count-1)}} >
                                                    <FastImage
                                                        style={styles.image_logo}
                                                        source={require('../../assets/icons/Remove.png')
                                                        }
                                                        resizeMode={FastImage.resizeMode.contain}
                                                    />
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>
:
<>
</>}
{SLD?
                                        <View>
                                            <Text style={{ color: 'rgb(68,67,67)', fontFamily:'SourceSansPro-Regular', fontSize: 16, marginLeft: 5, marginTop: 15 }}>Electrical SLD</Text>
                                            <View style={[styles.textInput1, { marginTop: 5 }]}>
                                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                    <TouchableOpacity style={styles.pdfbtn1}>
                                                        <Text style={styles.uploadbtnText1}>PDF</Text>
                                                    </TouchableOpacity>
                                                    <View>
                                                    <Text style={{ color: 'rgb(68,67,67)', fontFamily:'SourceSansPro-Semibold', fontSize: 14, marginLeft: 10 }}>SLD.pdf</Text>
                                                    <Text style={{ color: 'rgb(143, 143, 143)', fontFamily:'SourceSansPro-Regular', fontSize: 12, marginLeft: 10 }}>1.2 KB</Text>
                                                    </View>
                                                </View>
                                                <View>
                                                <TouchableOpacity onPress={()=>{setSld(''), setcount(count-1)}} >
                                                    <FastImage
                                                        style={styles.image_logo}
                                                        source={require('../../assets/icons/Remove.png')
                                                        }
                                                        resizeMode={FastImage.resizeMode.contain}
                                                    />
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>
:
<>
</>}
{BOM?
                                        <View>
                                            <Text style={{ color: 'rgb(68,67,67)', fontFamily:'SourceSansPro-Regular', fontSize: 16, marginLeft: 5, marginTop: 15 }}>BOM</Text>
                                            <View style={[styles.textInput1, { marginTop: 5 }]}>
                                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                    <TouchableOpacity style={styles.pdfbtn1}>
                                                        <Text style={styles.uploadbtnText1}>PDF</Text>
                                                    </TouchableOpacity>
                                                    <View>
                                                    <Text style={{ color: 'rgb(68,67,67)', fontFamily:'SourceSansPro-Semibold', fontSize: 14, marginLeft: 10 }}>BOM.pdf</Text>
                                                    <Text style={{ color: 'rgb(143, 143, 143)', fontFamily:'SourceSansPro-Regular', fontSize: 12, marginLeft: 10 }}>1.2 KB</Text>
                                                    </View>
                                                </View>
                                                <View>
                                                <TouchableOpacity onPress={()=>{setBom(''), setcount(count-1)}} >
                                                    <FastImage
                                                        style={styles.image_logo}
                                                        source={require('../../assets/icons/Remove.png')
                                                        }
                                                        resizeMode={FastImage.resizeMode.contain}
                                                    />
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>
:
<>
</>}
{site?
                                        <View>
                                            <Text style={{ color: 'rgb(68,67,67)', fontFamily:'SourceSansPro-Regular', fontSize: 16, marginLeft: 5, marginTop: 15 }}>Site Checklist</Text>
                                            <View style={[styles.textInput1, { marginTop: 5 }]}>
                                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                    <TouchableOpacity style={styles.pdfbtn1}>
                                                        <Text style={styles.uploadbtnText1}>PDF</Text>
                                                    </TouchableOpacity>
                                                    <View>
                                                    <Text style={{ color: 'rgb(68,67,67)', fontFamily:'SourceSansPro-Semibold', fontSize: 14, marginLeft: 10 }}>checklist.pdf</Text>
                                                    <Text style={{ color: 'rgb(143, 143, 143)', fontFamily:'SourceSansPro-Regular', fontSize: 12, marginLeft: 10 }}>1.2 KB</Text>
                                                    </View>
                                                </View>
                                                <View>
                                                <TouchableOpacity onPress={()=>{setSite(''), setcount(count-1)}} >
                                                    <FastImage
                                                        style={styles.image_logo}
                                                        source={require('../../assets/icons/Remove.png')
                                                        }
                                                        resizeMode={FastImage.resizeMode.contain}
                                                    />
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>
:
<>
</>}
{escalation?
                                        <View>
                                            <Text style={{ color: 'rgb(68,67,67)', fontFamily:'SourceSansPro-Regular', fontSize: 16, marginLeft: 5, marginTop: 15 }}>Escalation Matrix</Text>
                                            <View style={[styles.textInput1, { marginTop: 5 }]}>
                                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                    <TouchableOpacity style={styles.pdfbtn1}>
                                                        <Text style={styles.uploadbtnText1}>PDF</Text>
                                                    </TouchableOpacity>
                                                    <View>
                                                    <Text style={{ color: 'rgb(68,67,67)', fontFamily:'SourceSansPro-Semibold', fontSize: 14, marginLeft: 10 }}>Escalation Matrix.pdf</Text>
                                                    <Text style={{ color: 'rgb(143, 143, 143)', fontFamily:'SourceSansPro-Regular', fontSize: 12, marginLeft: 10 }}>1.2 KB</Text>
                                                    </View>
                                                </View>
                                                <View>
                                                <TouchableOpacity onPress={()=>{setEscalation(''), setcount(count-1)}} >
                                                    <FastImage
                                                        style={styles.image_logo}
                                                        source={require('../../assets/icons/Remove.png')
                                                        }
                                                        resizeMode={FastImage.resizeMode.contain}
                                                    />
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>

:
<>
</>}
                                   
                                       

                                    </>}
                            </>

                        }

                    </View>
                </ScrollView>
                <View style={[styles.shadowContainerStyle1, {
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: hp('15%'),
                    width: wp('150%'),
                }]}>
                    <TouchableOpacity
                        onPress={() => props.navigation.goBack()}>
                        <View style={{ borderColor: 'rgb(61,135,241)', justifyContent: 'center', alignItems: 'center', borderWidth: 1, marginTop: -20, borderRadius: 5, height: hp('7'), width: wp('30%') }}>
                            <Text style={{ color: 'rgb(61,135,241)' }}>Previous</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() =>handleNext() }
                    >
                        <View style={{ backgroundColor: 'rgb(61,135,241)', marginLeft: 10, justifyContent: 'center', alignItems: 'center', marginTop: 1 - 20, borderRadius: 5, height: hp('7'), width: wp('30%') }}>
                            <Text style={{ color: '#fff' }}>Next</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Text style={{ textDecorationLine: 'underline', color: 'rgb(61,135,241)', marginLeft: 10, marginTop: 1 - 20, fontFamily: 'SourceSansPro-Regular' }}>Save for later</Text>
                    </TouchableOpacity>

                </View>
            </View>

        </SafeAreaView>

    )
};
const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
    shadowContainerStyle1: {   //<--- Style with elevation

        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 510 },
        shadowOpacity: 20,
        shadowRadius: 300,
        elevation: 3,
    },
    card: {   //<--- Style with elevation
        borderLeftColor: 'rgb(61,135,241)',
        borderLeftWidth: 5,
        borderRadius: 5,
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        elevation: 3,
        width: wp('90%'),
        margin: 10,
        height: 160,
        padding: 20
        //  justifyContent: 'center',
        //  alignItems:'center'
    },

    image_logo: {
        width: wp('8%'),
        height: hp('5%'),
    },
    parent2: {
        // height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
        flexDirection: 'row',
        marginTop: hp('3%'),
        // justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        backgroundColor: 'white',
        flexDirection: 'column'
    },
    option_text: {
        fontSize: hp('1.2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center'
    },
    signIn: {
        width: wp('74%'),
        height: hp('8%'),
        left: 25,
        top: 80,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        color: '#05375a',
        backgroundColor: '#3A80E3',
        borderRadius: 5,
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: 'SourceSansPro-Regular',
    },
    name_text: {
        fontSize: 16,
        fontWeight: 'bold',
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68,67,67)'
    },
    temail_textxt: {
        fontSize: 14,
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(143,143,143)'
    },
    txt: {
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(143,143,143)'
    },
    txtunder: {
        fontSize: hp('2.5%'),
        textDecorationLine: 'underline',
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(61,135,241)'
    },
    textblue: {
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(61,135,241)'
    },
    textInput1: {
        // marginTop: hp('3%'),
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        width: wp('79.4%'),
        height: hp('7.4%'),
        borderRadius: 4,
        fontSize: 16,
        fontFamily: 'SourceSansPro-Regular',
        color: 'black',
        textAlign: 'left',
        paddingHorizontal: 10,
        marginBottom: Platform.OS === 'ios' ? 6 : 8
    },

    btnText: {
        fontSize: 12,
        fontFamily: 'SourceSansPro-Regular'
    },

    btn: {
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        width: wp('40%'),
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center'
    },
    uploadbtnText: {
        fontSize: hp('2%'),
        color: '#fff',
        fontFamily: 'SourceSansPro-Regular'
    },

    uploadbtn: {
        borderWidth: 1,
        backgroundColor: '#3d87f1',
        borderColor: 'rgb(232, 232, 232)',
        width: wp('20%'),
        height: hp('5%'),
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center'
    },

    uploadbtnText1: {
        fontSize: hp('2%'),
        color: 'rgb(143, 143, 143)',
        fontFamily: 'SourceSansPro-Regular'
    },

    uploadbtn1: {
        borderWidth: 1,
        backgroundColor: 'rgb(194, 194, 194)',
        borderColor: 'rgb(194, 194, 194)',
        width: wp('20%'),
        height: hp('5%'),
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center'
    },
    pdfbtn1: {
        borderWidth: 1,
        backgroundColor: 'rgb(236, 243, 254)',
        borderColor: 'rgb(143, 143, 143)',
        width: wp('12%'),
        height: hp('6%'),
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center'
    },

});
export default Databank