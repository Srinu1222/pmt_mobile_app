import React, { useEffect } from 'react';
import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    PermissionsAndroid,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    Picker

} from 'react-native';
import { Searchbar, Button, Switch } from 'react-native-paper';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import * as actionCreator from '../../actions/docs-action/index';
import { useDispatch, useSelector } from 'react-redux';
import { FlatList } from 'react-native-gesture-handler';
import FastImage from 'react-native-fast-image'
import NewProjectHeader from '../EmptyScreens/NewProjectHeader';
import { SearchBar,Divider } from 'react-native-elements';
import { Checkbox, RadioButton } from 'react-native-paper';


const ProjectPlaning = (props) => {
    const onChangeSearch = query => setSearchQuery(query);
    const [checked, setChecked] = React.useState(false);
    const [role, setrole] = React.useState('');

    const [onStockButtonPress, setonStockButtonPress] = React.useState(true);
    const [onConsumedButtonPress, setonConsumedButtonPress] = React.useState(false);
    const rolelist = [{ id: 1, rolename: 'owner' }, { id: 2, rolename: 'owner1' }]

    const onPressStock = (params) => {
        setonStockButtonPress(true)
        setonConsumedButtonPress(false)
    }

    const onPressConsumed = (params) => {
        setonConsumedButtonPress(true)
        setonStockButtonPress(false)
    }


    return (
        <SafeAreaView>
            <View style={styles.container}>
                {/* <NewProjectHeader {...props} title='Data Bank'no='5' percents='83.3' /> */}
                <View style={{ flexDirection: 'row',justifyContent:'space-between', alignItems: 'center',width:wp('80%'),height:hp('10%') }}>
                    <View>
                    <Text style={{
                        fontSize: hp('3%'),
                        fontFamily: 'SourceSansPro-Regular',
                    }}>Project Planning</Text>
                    </View>
                    <TouchableOpacity onPress={() => props.navigation.goBack()} style={{ alignItems:'flex-end' }}>
                        <Text style={[styles.txtunder,{fontSize:13}]}>Back to </Text>
                        <Text style={[styles.txtunder,{fontSize:13,marginTop:5}]}>Project Specification</Text>
                    </TouchableOpacity>

                </View>
                <Divider orientation="horizontal" height={1} width={wp('100%')} />

                <ScrollView>

                    <View style={styles.parent2}>
                        <View style={styles.textInput1}>
                            <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>Pre-Requisites</Text>
                            <TouchableOpacity >
                                < FastImage
                                    style={styles.image_logo}
                                    source={require('../../assets/icons/down.png')
                                    }
                                    resizeMode={FastImage.resizeMode.contain}
                                />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.textInput1}>
                            <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>Approvals</Text>
                            <TouchableOpacity >
                                < FastImage
                                    style={styles.image_logo}
                                    source={require('../../assets/icons/down.png')
                                    }
                                    resizeMode={FastImage.resizeMode.contain}
                                />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.textInput1}>
                            <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>Engineering</Text>
                            <TouchableOpacity >
                                < FastImage
                                    style={styles.image_logo}
                                    source={require('../../assets/icons/down.png')
                                    }
                                    resizeMode={FastImage.resizeMode.contain}
                                />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.textInput1}>
                            <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>Procurement</Text>
                            <TouchableOpacity >
                                < FastImage
                                    style={styles.image_logo}
                                    source={require('../../assets/icons/down.png')
                                    }
                                    resizeMode={FastImage.resizeMode.contain}
                                />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.textInput1}>
                            <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>Material Handeling</Text>
                            <TouchableOpacity >
                                < FastImage
                                    style={styles.image_logo}
                                    source={require('../../assets/icons/down.png')
                                    }
                                    resizeMode={FastImage.resizeMode.contain}
                                />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.textInput1}>
                            <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>Construction</Text>
                            <TouchableOpacity >
                                < FastImage
                                    style={styles.image_logo}
                                    source={require('../../assets/icons/down.png')
                                    }
                                    resizeMode={FastImage.resizeMode.contain}
                                />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.textInput1}>
                            <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>Site Handover</Text>
                            <TouchableOpacity >
                                < FastImage
                                    style={styles.image_logo}
                                    source={require('../../assets/icons/down.png')
                                    }
                                    resizeMode={FastImage.resizeMode.contain}
                                />
                            </TouchableOpacity>
                        </View>

                    </View>
                </ScrollView>

                <View style={{
                    flexDirection: 'column',
                    // justifyContent: 'center',
                    // justifyContent:'space-around',
                    alignItems: 'center',
                    height: hp('20%'),
                    width: wp('100%'),
                }}>

                    <TouchableOpacity
                        onPress={() => props.navigation.navigate('ProjectPlaning')}
                    >
                        <View style={{ backgroundColor: 'rgb(61,135,241)', marginLeft: 10, justifyContent: 'center', alignItems: 'center', marginTop: 10, borderRadius: 5, height: hp('7'), width: wp('80%') }}>
                            <Text style={{ color: '#fff' }}>Generate Gantt</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Text style={{ textDecorationLine: 'underline', color: 'rgb(61,135,241)', marginLeft: 10, marginTop: 20, fontFamily: 'SourceSansPro-Regular' }}>Save for later</Text>
                    </TouchableOpacity>

                </View>
            </View>

        </SafeAreaView>

    )
};
const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
    shadowContainerStyle1: {   //<--- Style with elevation

        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 510 },
        shadowOpacity: 20,
        shadowRadius: 300,
        elevation: 3,
    },
    card: {   //<--- Style with elevation
        borderLeftColor: 'rgb(61,135,241)',
        borderLeftWidth: 5,
        borderRadius: 5,
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        elevation: 3,
        width: wp('90%'),
        margin: 10,
        height: 160,
        padding: 20
        //  justifyContent: 'center',
        //  alignItems:'center'
    },

    image_logo: {
        width: wp('8%'),
        height: hp('5%'),
    },
    parent2: {
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
        flexDirection: 'row',
        marginTop: hp('5%'),
        // justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        backgroundColor: 'white',
        flexDirection: 'column'
    },
    option_text: {
        fontSize: hp('1.2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center'
    },
    signIn: {
        width: wp('74%'),
        height: hp('8%'),
        left: 25,
        top: 80,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        color: '#05375a',
        backgroundColor: '#3A80E3',
        borderRadius: 5,
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: 'SourceSansPro-Regular',
    },
    name_text: {
        fontSize: 16,
        fontWeight: 'bold',
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68,67,67)'
    },
    temail_textxt: {
        fontSize: 14,
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(143,143,143)'
    },
    txt: {
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(143,143,143)'
    },
    txtunder: {
        fontSize: hp('2.5%'),
        textDecorationLine: 'underline',
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(61,135,241)'
    },
    textblue: {
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(61,135,241)'
    },
    textInput1: {
        marginTop: hp('2%'),
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        width: wp('79.4%'),
        height: hp('7.4%'),
        borderRadius: 4,
        fontSize: 16,
        fontFamily: 'SourceSansPro-Regular',
        color: 'black',
        textAlign: 'left',
        paddingHorizontal: 10,
        marginBottom: Platform.OS === 'ios' ? 6 : 8
    },

    btnText: {
        fontSize: 12,
        fontFamily: 'SourceSansPro-Regular'
    },

    btn: {
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        width: wp('40%'),
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center'
    },
    uploadbtnText: {
        fontSize: hp('2%'),
        color: '#fff',
        fontFamily: 'SourceSansPro-Regular'
    },

    uploadbtn: {
        borderWidth: 1,
        backgroundColor: '#3d87f1',
        borderColor: 'rgb(232, 232, 232)',
        width: wp('20%'),
        height: hp('5%'),
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center'
    },

});
export default ProjectPlaning