import React, { useState, useMemo, useEffect } from 'react';
import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    PermissionsAndroid,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,

} from 'react-native';
import { Searchbar, Button, Switch } from 'react-native-paper';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import * as actionCreator from '../../actions/docs-action/index';
import { useDispatch, useSelector } from 'react-redux';
import { FlatList } from 'react-native-gesture-handler';
import FastImage from 'react-native-fast-image'
import NewProjectHeader from '../EmptyScreens/NewProjectHeader';
import * as Animatable from 'react-native-animatable';
import notifyMessage from '../../ToastMessages';
import { Radio, RadioGroup } from '@ui-kitten/components';
import ElevatedView from '../ElevatedView';


const CustomerDetails = ({ props, route, navigation }) => {
    const { project, image, TeamDetails, subContractorReducer } = route.params;
    const [needed, setNeeded] = React.useState(true);
    const [checked, setChecked] = React.useState('Commercial');
    const [selectedIndex, setSelectedIndex] = React.useState(0)
    const [isCustomerInfo, setIsCustomerInfo] = useState(true);
    const [projectStatus, setProjectStatus] = useState('');

    const [customerName, setCustomerName] = useState('');
    const [customerCompanyName, setCustomerCompanyName] = useState('');
    const [email, setEmail] = useState([]);
    const [phoneNo, setPhoneNo] = useState('');
    const [secondaryEmail, setSecondaryEmail] = useState();

    const [isValidcustomerName, setisValidCustomerName] = useState(true);
    const [isValidcustomerCompanyName, setisValidCustomerCompanyName] = useState(true);
    const [isValidemail, setisValidEmail] = useState(true);
    const [isValidphoneNo, setisValidPhoneNo] = useState(true);
    const [isValidsecondaryEmail, setisValidSecondaryEmail] = useState(true);

    const handleNext = () => {
        if (customerName != '' && customerCompanyName != '' && email != '' && phoneNo != '') {

            navigation.navigate('TeamDetails', {
                project: project,
                TeamDetails: TeamDetails,
                subContractorReducer: subContractorReducer,
                image: image, customer: {
                    is_customer_info:isCustomerInfo,
                    property_type:projectStatus,
                    customer_name:customerName,
                    company_name:customerCompanyName,
                    primary_email:email,
                    phone_number:phoneNo,
                    emails:secondaryEmail
                }
            })
        } else {
            notifyMessage("Please fill all the fields");
        }
    }
    const onPressNeeded = () => {

        setIsCustomerInfo(false)
        navigation.navigate('TeamDetails', {
            project: project,
            TeamDetails: TeamDetails,
            subContractorReducer: subContractorReducer,
            image: image, customer: {
                is_customer_info:false,
                    property_type:projectStatus,
                    customer_name:customerName,
                    company_name:customerCompanyName,
                    primary_email:email,
                    phone_number:phoneNo,
                    emails:secondaryEmail
            },
        })
    }

    const onPressNoneeded = () => {
        setIsCustomerInfo(true)

    }

    const handleCustomerName = val => {
        if (val.trim().length >= 4) {
            setCustomerName(val)
            setisValidCustomerName(true)

        } else {
            setisValidCustomerName(false)
        }

    }
    const handleCustomerCompanyName = val => {
        if (val.trim().length >= 4) {
            setCustomerCompanyName(val)
            setisValidCustomerCompanyName(true)

        } else {
            setisValidCustomerCompanyName(false)
        }

    }
    const handleEmail = val => {
        const isValid = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
            val,
        );
        if (isValid) {
            setEmail(val)
            setisValidEmail(true)

        } else {
            setisValidEmail(false)
        }

    }
    const handlePhoneNo = val => {
        const isValid1 = /^[98765]\d{9}$/.test(
            val,
        );
        if (isValid1) {
            setPhoneNo(val)
            setisValidPhoneNo(true)

        } else {
            setisValidPhoneNo(false)
        }

    }
    const handleSecondaryEmail = val => {
        const isValid = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
            val,
        );
        if (isValid) {
            setSecondaryEmail(val)
            setisValidSecondaryEmail(true)

        } else {
            setisValidSecondaryEmail(false)
        }
    }

    const project_type = ['Commercial', 'Industrial', 'Residential']
    return (
        <>
            <View style={styles.container}>
                <NewProjectHeader {...props} title='Customer Details' no='2' percents={32.32} />
                <ScrollView>
                    <KeyboardAvoidingView behavior='position'>
                    <View style={styles.parent2}>
                        <View style={styles.textInput1}>
                            {isCustomerInfo ?
                                <TouchableOpacity
                                    onPress={() => { onPressNeeded() }}>
                                    <FastImage
                                        style={styles.image_logo}
                                        source={require('../../assets/icons/Uncheck.png')
                                        }
                                        resizeMode={FastImage.resizeMode.contain}
                                    />
                                </TouchableOpacity>
                                :
                                <TouchableOpacity
                                    onPress={() => { onPressNoneeded() }}>
                                    <FastImage
                                        style={styles.image_logo}
                                        source={require('../../assets/icons/Check.png')
                                        }
                                        resizeMode={FastImage.resizeMode.contain}
                                    />
                                </TouchableOpacity>}
                            <Text style={{ color: 'rgb(68,67,67)', fontSize: 16, marginLeft: 5 }}>No Customer info needed</Text>
                        </View>
                        {isCustomerInfo ?
                            <>
                                <View style={styles.textInput2}>
                                   
                                    <RadioGroup
        selectedIndex={selectedIndex}
        onChange={index => {
            setSelectedIndex(index)
            setChecked(project_type[index])
        }
        }>
        <Radio style={{margin: 2}}>Commercial</Radio>
        <Radio style={{margin: 2}}>Industrial</Radio>
        <Radio style={{margin: 2}}>Residential</Radio>
      </RadioGroup>
                                </View>
                                <TextInput
                                    placeholder="Customer Name"
                                    placeholderTextColor="rgb(143, 143, 143)"
                                    style={styles.textInput}
                                    autoCapitalize="none"
                                    onChangeText={val => handleCustomerName(val)}


                                />
                                {isValidcustomerName ? null : (
                                    <Animatable.View animation="fadeInLeft" duration={500}>
                                        <Text style={styles.errorMsg}>Enter a Valid Customer Name.</Text>
                                    </Animatable.View>
                                )}
                                <TextInput
                                    placeholder="Customer Company's Name"
                                    placeholderTextColor="rgb(143, 143, 143)"
                                    style={styles.textInput}
                                    autoCapitalize="none"
                                    onChangeText={val => handleCustomerCompanyName(val)}


                                />
                                {isValidcustomerCompanyName ? null : (
                                    <Animatable.View animation="fadeInLeft" duration={500}>
                                        <Text style={styles.errorMsg}>Enter a Valid Customer Company Name.</Text>
                                    </Animatable.View>
                                )}
                                <TextInput
                                    placeholder="Email"
                                    placeholderTextColor="rgb(143, 143, 143)"
                                    style={styles.textInput}
                                    autoCapitalize="none"
                                    onChangeText={val => handleEmail(val)}


                                />
                                {isValidemail ? null : (
                                    <Animatable.View animation="fadeInLeft" duration={500}>
                                        <Text style={styles.errorMsg}>Enter a Valid Email.</Text>
                                    </Animatable.View>
                                )}
                                <TextInput
                                    placeholder="Phone No."
                                    placeholderTextColor="rgb(143, 143, 143)"
                                    style={styles.textInput}
                                    keyboardType='numeric'
                                    autoCapitalize="none"
                                    onChangeText={val => handlePhoneNo(val)}
                                />
                                {isValidphoneNo ? null : (
                                    <Animatable.View animation="fadeInLeft" duration={500}>
                                        <Text style={styles.errorMsg}>Enter a Valid PhoneNo.</Text>
                                    </Animatable.View>
                                )}
                                <View style={{ marginLeft: -75, marginTop: 20, marginBottom: 10 }}>
                                    <Text style={{ fontFamily: 'SourceSansPro-Regular', fontSize: 16, color: 'rgb(68,67,67)' }}><Text style={{ fontFamily: 'SourceSansPro-Regular', fontSize: 16, color: 'rgb(247,90,90)' }}>*</Text>User can add multiple emails</Text>
                                </View>

                                <TextInput
                                    placeholder="Secondary Email Id*"
                                    placeholderTextColor="rgb(143, 143, 143)"
                                    style={styles.textInput}
                                    autoCapitalize="none"
                                    onChangeText={val => handleSecondaryEmail(val)}
                                />
                                {isValidsecondaryEmail ? null : (
                                    <Animatable.View animation="fadeInLeft" duration={500}>
                                        <Text style={styles.errorMsg}>Enter a Valid Secondary Email.</Text>
                                    </Animatable.View>
                                )}
                                {/* <View style={{height:hp('10%')}}>
                               
</View> */}
                            </> :
                            <>
                            </>
                        }
                        
                    </View>
                    </KeyboardAvoidingView>
                </ScrollView>
                <View style={[styles.shadowContainerStyle, {
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: hp('15%'),
                    width: wp('150%'),
                }]}>
                    <TouchableOpacity
                        onPress={() => navigation.goBack()}>
                        <View style={{ borderColor: 'rgb(61,135,241)', justifyContent: 'center', alignItems: 'center', borderWidth: 1, marginTop: -20, borderRadius: 5, height: hp('7'), width: wp('30%') }}>
                            <Text style={{ color: 'rgb(61,135,241)' }}>Previous</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => handleNext()}
                    >
                        <View style={{ backgroundColor: 'rgb(61,135,241)', marginLeft: 10, justifyContent: 'center', alignItems: 'center', marginTop: 1 - 20, borderRadius: 5, height: hp('7'), width: wp('30%') }}>
                            <Text style={{ color: '#fff' }}>Next</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Text style={{ textDecorationLine: 'underline', color: 'rgb(61,135,241)', marginLeft: 10, marginTop: 1 - 20, fontFamily: 'SourceSansPro-Regular' }}>Save for later</Text>
                    </TouchableOpacity>

                </View>
            </View>

        </>

    )
};
const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#fff",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
    shadowContainerStyle: {   //<--- Style with elevation
        shadowOpacity: 0.0015 * 8 + 0.18,
        shadowRadius: 0.54 * 8,
        shadowOffset: {
          height: 0.6 * 8,
        },
    },
    image_logo: {
        width: wp('7%'),
        height: hp('3.5%'),
    },
    parent2: {
        width: Dimensions.get('window').width,
        marginTop: hp('3%'),
        alignItems: 'center',
        flex: 1,
        backgroundColor: 'white',
        flexDirection: 'column',
        backgroundColor: '#fff'


    },
    errorMsg: {
      
        color: '#FF0000',
        fontSize: 14,
        marginBottom: hp('1%'),
        fontFamily: 'SourceSansPro-Regular',
    },
    option_text: {
        fontSize: hp('1.2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center'
    },
    signIn: {
        width: wp('74%'),
        height: hp('8%'),
        left: 25,
        top: 80,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        color: '#05375a',
        backgroundColor: '#3A80E3',
        borderRadius: 5,
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: 'SourceSansPro-Regular',
    },
    txt: {
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(143,143,143)'
    },
    txtunder: {
        fontSize: hp('2.5%'),
        textDecorationLine: 'underline',
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(61,135,241)'
    },
    textblue: {
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(61,135,241)'
    },
    textInput2: {
        marginTop: hp('1%'),
        flexDirection: 'column',
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        width: wp('79.4%'),
        padding: '1%',
        borderRadius: 4,
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'black',
        textAlign: 'left',
        paddingHorizontal: 10,
        marginBottom: Platform.OS === 'ios' ? 6 : 8
    },
    textInput1: {
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        width: wp('79.4%'),
        height: hp('7.4%'),
        borderRadius: 4,
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'black',
        textAlign: 'left',
        paddingHorizontal: 10,
        marginBottom: Platform.OS === 'ios' ? 6 : 8
    },
    textInput: {
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        width: wp('79.4%'),
        height: hp('7.4%'),
        borderRadius: 4,
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'black',
        textAlign: 'left',
        paddingHorizontal: 10,
        marginBottom: Platform.OS === 'ios' ? 6 : 8
    },
    btn: {
        height: hp('10%'),
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        width: wp('80%'),
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnText: {
        fontSize: hp('3%'),
        fontFamily: 'SourceSansPro-Regular'
    }

});
export default CustomerDetails