import { View, Text } from 'react-native'
import React, { useEffect, useState } from "react";
import * as actionCreators from "../actions/all-projects-action/index";
import { useDispatch, useSelector } from "react-redux";
import GetAllProjects from '../components/AllProjects/GetAllProjects'
import notifyMessage from '../ToastMessages';
import { parseSync } from '@babel/core';
import { heightPercentageToDP } from 'react-native-responsive-screen';
import EmptyLoader from '../components/EmptyLoader';
import { useIsFocused } from "@react-navigation/native";


const AllProjects = (props) => {

    const { route, navigation } = props;

    const dispatch = useDispatch();

    const token = route.params.token

    useEffect(() => {
        dispatch(actionCreators.getAllProjects(token));
        // setInterval(() => setLoading(false), 300)
    }, []);

    const allProjectsReducer = useSelector((state) => {
        return state.allProjectReducers.projects.get;
    });

    const renderAllProjects = () => {
        if (allProjectsReducer.success.ok) {
            // //(allProjectsReducer.success.data.projects)
            return (<GetAllProjects token={token} navigation={navigation} data={allProjectsReducer.success.data.projects}/>);
        } else if (allProjectsReducer.failure.error) {
            return notifyMessage(allProjectsReducer.failure.message);
        } else {
            return (<EmptyLoader />);
        }
    }
    return renderAllProjects();
};

export default AllProjects;
