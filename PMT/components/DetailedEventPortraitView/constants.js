export const component_list = [
    "AC Distribution (Combiner) Panel Board",
    "Ambient Temperature Sensors",
    "Bus bar",
    "Cable trays",
    "Cabling Accessories",
    "Cleaning System",
    "Communication Cable",
    "DC Junction Box (SCB/SMB)",
    "DG Syncronization",
    "Danger Board and Signs",
    "Earthing Cable",
    "Easrthing Protection System",
    "Fire Extingusiher",
    "GI Strip",
    "Generation Meter",
    "HT Breaker",
    "HT Panel",
    "InC Contractor",
    "LT Panel",
    "LT Power Cable (From ACDB to Customer LT Panel)",
    "LT Power Cable (From inverters to ACDB)",
    "LT Power Cable (From metering cubical to MDB panel)",
    "Lighting Arrestor",
    "MC4 Connectors",
    "MDB Breaker",
    "Metering Panel",
    "Module Earthing Cable",
    "Module Mounting Structures with Accessories _ Ground Mount",
    "Module Mounting Structures with Accessories _ Metal Sheet",
    "Module Mounting Structures with Accessories _ RCC",
    "Module Temperature Sensors",
    "Net-Metering",
    "Other Sensors",
    "Pyro Meter",
    "Reverse Protection Relay",
    "SCADA System",
    "Safety Lines",
    "Safety Rails",
    "Solar DC Cables",
    "Solar Inverter",
    "Solar PV Module",
    "Structure Foundation Pedestal",
    "Transformer",
    "UPS",
    "Walkways",
    "Weather Monitoring Unit",
    "Wire mesh for protection of Skylights",
    "Zero Export Device",
];

export const drawings = [
    "Array Layout",
    "Civil Layout",
    "PV-Syst",
    "Communication System Layout",
    "Equipment Layout",
    "Walkways",
    "Earthing and LA Layout",
    "Safety Line",
    "Cable Layout",
    "Safety Rails",
    "Structure Drawings_RCC",
    "Project SLD",
    "Structure Drawings_Metal Sheet",
    "Final BOQ",
    "Structure Drawings_Ground Mount",
    "Detailed Project Report",
];

var drawings_list = []
drawings.map((i) =>
    drawings_list.push(
        {
            'label': i,
            'value': i
        }
    )
)
export var drawings_list;

export const primaryHeadingDict = {
    'List of All Drawings from Checklist': 'Engineering Plan',
    'SPOC for each drawing': 'Spoc For Each Drawing',
    'Type of Cable': 'Cable Calculations',
    'Specifications of Revisions that are needed – Required: A way to specify the drawings that need revisions and specifying the revisions needed': 'Internal Design Review',
    'Recommended Changes if Any': 'Internal Design Review',
    'Procurement Plan': 'Procurement Plan',
    'Procurement Specifications': 'Procurement Specifications',
    'Final Specifications': 'Final Specification',
    'Inspection Agency Details': 'PDI Details',
}

export const addFieldInitialObjects = {
    'List of All Drawings from Checklist': { 'drawing': 'Please select...', 'spoc': { 'name': 'Please select...', 'id': null }, 'start_date': '' },
    'SPOC for each drawing': { 'drawing': 'Please select...', 'spoc': { 'name': 'Please select...', 'id': null }, 'start_date': '' },
    'Type of Cable': { 'cable_type': '', 'cable_specification': '', 'cable_length': '' },
    'Specifications of Revisions that are needed – Required: A way to specify the drawings that need revisions and specifying the revisions needed': 'Internal Design Review',
    'Recommended Changes if Any': 'Internal Design Review',
    'Procurement Plan': { 'component_name': 'Please select...', 'spoc': { 'name': 'Please select...', 'id': null }, 'start_date': '' },
    'Procurement Specifications': {
        'stock_quantity': '',
        'target_price': '',
        'target_delivery_date': '',
        'preferred_brands': '',
        'specifications': ''
    },

    'Final Specifications': {
        'stock_quantity': '',
        'final_price': '',
        'target_delivery_date': '',
        'final_brands': '',
        'specifications': '',
        'po_file': ''
    },

    'Inspection Agency Details: To Enter Name, Contact person, Number, Email': {
        'component_name': 'Component Name',
        'pdi_agency_name': 'PDI Agency Name',
        'pdi_date': 'PDI Date',
        'contact_person_name': 'Contact Person Name',
        'contact_person_email': 'Contact Person Email',
        'contact_phone_number': 'Phone Number'
    },
}

export const SpecificationList = [
    'List of All Drawings from Checklist',
    'SPOC for each drawing',
    'Type of Cable',
    'Specifications of Revisions that are needed – Required: A way to specify the drawings that need revisions and specifying the revisions needed',
    'Recommended Changes if Any',
    'Procurement Plan',
    'Procurement Specifications',
    'Final Specifications',
    'Inspection Agency Details'
]


export const singleObjectCheckLists = [
    'Procurement Specifications',
    'Final Specifications',
    'Inspection Agency Details: To Enter Name, Contact person, Number, Email'
]

export const fontTypes = [
    {
        'name': 'bold',
        'style': { fontWeight: 'bold' }
    },
    {
        'name': 'italic',
        'style': { fontStyle: 'italic' }
    },
    {
        'name': 'underline',
        'style': { textDecorationLine: 'underline' }
    },
    {
        'name': 'paperclip',
        'style': {}
    }
]


export const EventTabs = ['Pre-Requisites', 'Checklist', 'Tickets', 'Documents', 'Approval']

export const statusColor = {
    'PENDING': '#9EC3F8',
    'IN-PROGRESS': 'rgb(241, 167, 61)',
    'FINISHED': 'green'
}