import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    useWindowDimensions,
    Animated
} from 'react-native';
import React, { useState, useEffect, useMemo } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { useDispatch, useSelector } from 'react-redux';
import * as getStageEventActionCreators from '../../actions/gantt-stage-action/index';
import EmptyLoader from '../../components/EmptyLoader'
import notifyMessage from '../../ToastMessages'
import { Avatar, Card, Button, List, Divider, IconButton } from 'react-native-paper';
import color from 'color';
import { FlatList } from 'react-native';
import {
    useCollapsibleSubHeader,
    CollapsibleSubHeaderAnimator,
} from 'react-navigation-collapsible';
import { Icon } from 'react-native-elements'


const PreRequisite = (props) => {

    const {
        onScroll /* Event handler */,
        containerPaddingTop /* number */,
        scrollIndicatorInsetTop /* number */,
        translateY,
    } = useCollapsibleSubHeader();

    const data = props.data


    const Item = (item) => {

        return (
            <Card style={styles.item_card}>
                <View style={{ flexDirection: 'row', }}>
                    <Text style={{ width: wp('4%'), marginTop: hp('0.5%'), color: 'rgb(68,67,67)', borderRadius: 2, height: hp('2%'), borderWidth: 1, }}></Text>
                    <View style={{ borderWidth: 0, paddingLeft: '5%' }}>
                        <Text style={styles.titleTxt}>{item.title}</Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center', borderWidth: 0, marginTop: '2%' }}>
                            <Text style={styles.CommentsTxt}>Comments</Text>
                            <View style={styles.CircleShapeView}>
                                <Text style={{ textAlign: 'center', fontSize: hp('1.5%'), color: 'white' }}>{item.comments}</Text>
                            </View>
                        </View>

                        <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.dueDateTxt}>DUE DATE</Text>
                            <Text style={[styles.dueDateTxt, { paddingLeft: '2%' }]}>{item.due_on}</Text>
                        </View>
                    </View>
                </View>

                <Divider style={{ marginVertical: hp('2%') }} />
                <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={[styles.titleTxt, { fontSize: hp('1.8%') }]}>SPOC</Text>
                    <Text style={styles.CommentsTxt}>{item.spoc.name}</Text>
                </View>
                <Divider style={{ marginVertical: hp('2%') }} />

                <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={[styles.titleTxt, { fontSize: hp('1.8%') }]}>Sub-Contractor</Text>
                    <Text style={styles.CommentsTxt}>{item.spoc.name}</Text>
                </View>

                <TouchableOpacity
                    style={{ width: wp('26.3%'), height: hp('3.2%'), alignItems: 'center', marginTop: '6%', justifyContent: 'center', borderColor: '#3d87f1', borderRadius: 2, borderWidth: 1.2 }}
                >
                    <Text
                        style={{ fontSize: hp('1.5%'), color: '#3d87f1', textAlign: 'center' }}
                    >
                        FOLLOW UP
                    </Text>
                </TouchableOpacity>
            </Card>
        );
    }

    return (
        <View style={{ width: wp('95%'), marginTop: hp('2%') }}>
            <Animated.FlatList
                backgroundColor='#ffffff'
                data={data}
                // ListEmptyComponent={<EmptyDashBoard />}
                showsVerticalScrollIndicator
                keyExtractor={(item, index) => index}
                renderItem={({ item, index }) => <Item {...item} />}
                onScroll={onScroll}
                contentContainerStyle={{ paddingTop: containerPaddingTop, paddingLeft: hp('2%'), paddingRight: hp('2%'), paddingBottom: hp('1%') }}
                scrollIndicatorInsets={{ top: scrollIndicatorInsetTop }}
            />
        </View>
    );
};

export default PreRequisite;

const styles = StyleSheet.create({

    item_card: {
        marginBottom: hp('2%'),
        borderWidth: 0,
        backgroundColor: '#fff',
        shadowColor: 'rgba(0,0,0,0.4)',
        elevation: 5,
        borderRadius: 4,
        padding: '5%',
        paddingRight: '4%'
    },

    titleTxt: {
        color: 'rgb(68, 67, 67)',
        fontSize: hp('2.2%'),
        fontFamily: 'SourceSansPro-Regular',
    },

    CommentsTxt: {
        color: 'rgb(143, 143, 143)',
        fontSize: hp('1.8%'),
        fontFamily: 'SourceSansPro-Regular',
    },

    CircleShapeView: {
        width: 16,
        height: 16,
        borderRadius: 8,
        backgroundColor: '#3d87f1',
        marginBottom: hp('2%'),
    },

    dueDateTxt: {
        fontFamily: 'SourceSansPro-Regular',
        fontSize: hp('2%'),
        color: 'rgb(179, 179, 179)',
        borderRightWidth: 1,
        borderRightColor: 'rgb(179, 179, 179)',
        paddingRight: '2%'
    },

});
