import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView
} from 'react-native';
import React, { useState, useEffect, useMemo } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { useDispatch, useSelector } from 'react-redux';
import * as getEventActionCreators from '../../actions/gantt-stage-action/index';
import EventScreen1 from '../DetailedEventPortraitView/EventScreen1'
import EmptyLoader from '../../components/EmptyLoader'
import notifyMessage from '../../ToastMessages'


const EventScreen = (props) => {

    const { eventId, eventName, is_sub_contractor, token, navigation, open, selectedTabIndex, DrawerSet } = props

    useEffect(()=>{
        if (selectedTabIndex==2){
            setactiveIndex(selectedTabIndex)
        }
    }, [selectedTabIndex])

    const [reload, setReload] = useState(false);
    const [activeIndex, setactiveIndex] = useState(0);

    const changeActiveIndex = (index) => {
        setactiveIndex(index)
    }
    
    const changeReload = () => {
      setReload(!reload)
    }

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getEventActionCreators.getEventData(token, eventId, 'false', is_sub_contractor))
    }, [eventId, reload]);

    const ganttEventReducer = useSelector(state => {
        return state.eventDataReducers.eventdata.get;
    })

    const renderEventScreen = () => {
        if (ganttEventReducer.success.ok) {
            return (
                <EventScreen1
                    event_id={eventId}
                    // event_name={eventName}
                    open={open}
                    navigation={navigation}
                    token={token}
                    data={ganttEventReducer.success.data}
                    DrawerSet={DrawerSet}
                    changeReload={changeReload}
                    changeActiveIndex={changeActiveIndex}
                    activeIndex={activeIndex}
                    is_sub_contractor={is_sub_contractor}
                />
            );
        } else if (ganttEventReducer.failure.error) {
            return notifyMessage(ganttEventReducer.failure.message);
        } else {
            return (< EmptyLoader />)
        }
    };

    return renderEventScreen();
};

export default EventScreen;
