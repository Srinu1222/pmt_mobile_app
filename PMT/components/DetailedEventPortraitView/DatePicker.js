import React, { Component, useState } from 'react';
import DatePicker from 'react-native-datepicker';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    useWindowDimensions,
    Animated,
    Picker
} from 'react-native';
import { FAB, Icon, BottomSheet, Input } from 'react-native-elements';



const MyDatePicker = (params) => {

    const { onSelectDate, styling, defaultValue } = params
    const [date, setDate] = useState(defaultValue)

    return (
        <DatePicker
            style={styling}
            date={date}
            mode="date"
            placeholder="Start date"
            format="DD/MM/YYYY"
            // minDate="2016-05-01"
            // maxDate="2016-06-01"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
                dateIcon: {
                    marginLeft: 0,
                    paddingRight: wp('3%'),
                },
                dateInput: {
                    borderWidth: 0,
                    alignItems: 'flex-start',
                },
                placeholderText: {
                    color: 'rgb(143, 143, 143)',
                    fontSize: hp('2.2%'),
                    fontFamily: 'SourceSansPro-Regular',
                }
            }}
            onDateChange={(date) => {
                onSelectDate(date)
                setDate(date)
            }}
            iconComponent={
                <Icon color={'#9B9B9B'} type='feather' name={'calendar'} />
            }
        />
    )
}

export default MyDatePicker;


// import React from 'react';
// import { StyleSheet } from 'react-native';
// import { Datepicker, Icon } from '@ui-kitten/components';
// import {
//     widthPercentageToDP as wp,
//     heightPercentageToDP as hp,
// } from 'react-native-responsive-screen';


// const DatePicker = (props) => {
//     const { specReducerState, dictPosition, setspecReducerState } = props

//     const DateIcon = (style) => (
//         <Icon {...style} name='calendar' />
//     )

//     //(specReducerState[dictPosition]['start_date'])

//     return (
//         <Datepicker
//             size={'large'}
//             // date={specReducerState[dictPosition]['start_date']}
//             onSelect={d => {
//                 let newArr = [...specReducerState];
//                 newArr[dictPosition] = {
//                     ...newArr[dictPosition],
//                     start_date: d,
//                 };
//                 setspecReducerState([...newArr]);
//             }}
//             placeholder={'Select Date'}
//             accessoryRight={DateIcon}
//         />
//     );
// };

// const styles = StyleSheet.create({
//     container: {
//         height: 128,
//     },
// });

// export default DatePicker;
