import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    useWindowDimensions,
    Animated
} from 'react-native';
import React, { useState, useEffect, useMemo } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { useDispatch, useSelector } from 'react-redux';
import * as getStageEventActionCreators from '../../actions/gantt-stage-action/index';
import EmptyLoader from '../../components/EmptyLoader'
import notifyMessage from '../../ToastMessages'
import { Avatar, Card, Button, List, Divider, IconButton } from 'react-native-paper';
import color from 'color';
import { FlatList } from 'react-native';
import {
    useCollapsibleSubHeader,
    CollapsibleSubHeaderAnimator,
} from 'react-navigation-collapsible';
import { FAB, Icon, BottomSheet, Input } from 'react-native-elements';
import DocumentPicker from 'react-native-document-picker';
import * as actionCreators from '../../actions/gantt-stage-action/index';
import Toast from "react-native-toast-message";
import downloadImage from './DownloadFile'

const DocsNeeded = (props) => {
    const {
        onScroll /* Event handler */,
        containerPaddingTop /* number */,
        scrollIndicatorInsetTop /* number */,
        translateY,
    } = useCollapsibleSubHeader();

    const data = props.data
    const { token, event_id, navigation, changeReload, isSubContractor } = props.eventDetails

    const Item = (item) => {
        return (
            <Card style={styles.item_card}>
                <TouchableOpacity
                    onPress={() => { downloadImage(item.file) }}
                >
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ width: wp('11.1%'), marginRight: wp('3%'), justifyContent: 'center', alignItems: 'center', height: hp('5.8%'), backgroundColor: 'rgb(236, 236, 236)', borderRadius: 4 }}>
                                <Text style={{ color: 'rgb(143, 143, 143)', }}>{item.file.split(".").pop(1)}</Text>
                            </View>
                            <Text style={styles.valueTxt1}>{item.name}</Text>
                        </View>
                        <Icon
                            color={'#3d87f1'}
                            type='feather'
                            name='download'
                        />
                    </View>
                </TouchableOpacity>
            </Card >
        );
    }

    const [visible, setVisible] = useState(false);
    const [selectedFile, setSelectedFile] = useState('');
    const [selectedFileName, setSelectedFileName] = useState('');
    const [selectedFileType, setSelectedFileType] = useState('');

    const [fileName, setFileName] = useState('');

    const onPressUploadDocument = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.allFiles],
            });
            setSelectedFile(res.uri)
            setSelectedFileName(res.name)
            setSelectedFileType(res.type)
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
            } else {
                throw err;
            }
        }
    }

    const dispatch = useDispatch();

    const onFilePostHandler = () => {
        const formData = new FormData();
        formData.append('event_id', event_id);
        formData.append("files_data", JSON.stringify([{ 'title': fileName, 'description': '' }]));
        formData.append('files', {
            uri: selectedFile,
            type: selectedFileType,
            name: selectedFileName,
        });

        Promise.resolve(dispatch(actionCreators.postDocs(formData, token, isSubContractor)))
            .then(result => {
                if (result?.status === 200) {
                    Toast.show({
                        text1: 'New Document got Added!',
                        type: 'success',
                        autoHide: true,
                        position: 'bottom',
                        visibilityTime: 1000
                    })
                    setVisible(false)
                    changeReload()
                }
            })
    };

    return (
        <View style={{ width: wp('95%'), marginTop: hp('2%') }}>
            <View style={{ height: hp('50%') }}>
                <Text style={styles.valueTxt1}>Uploaded Files</Text>
                <Animated.FlatList
                    backgroundColor='#ffffff'
                    data={data}
                    ListEmptyComponent={
                        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                            < FastImage
                                style={styles.groupimage}
                                source={require('../../assets/icons/group.png')}
                                resizeMode={FastImage.resizeMode.contain}
                            />
                            <Text style={[styles.valueTxt1, { color: 'rgb(179, 179, 179)', marginVertical: hp('3%') }]}>No files, Please uploaded!</Text>
                        </View>
                    }
                    showsVerticalScrollIndicator
                    keyExtractor={(item, index) => index}
                    renderItem={({ item, index }) => <Item {...item} />}
                    onScroll={onScroll}
                    contentContainerStyle={{ paddingTop: containerPaddingTop, paddingLeft: hp('2%'), paddingRight: hp('3%'), paddingBottom: hp('0%') }}
                    scrollIndicatorInsets={{ top: scrollIndicatorInsetTop }}
                />
            </View>

            <TouchableOpacity
                onPress={() => setVisible(true)}
                style={{ borderWidth: 0, marginLeft: wp('5%'), alignItems: 'center', justifyContent: 'center', backgroundColor: '#3d87f1', width: wp('80%'), borderRadius: 4, height: hp('7.4%') }}
            >
                <Text style={styles.labelStyle}>Upload New</Text>
            </TouchableOpacity>

            <BottomSheet
                isVisible={visible}
                containerStyle={{ backgroundColor: 'rgba(0.5, 0.25, 0, 0.2)' }}
            >
                <View
                    style={{ height: Dimensions.get('window').height }}
                >
                    <TouchableOpacity
                        onPress={()=>setVisible(false)}
                    >
                        <View style={{ height: Dimensions.get('window').height * 0.3 }}></View>
                    </TouchableOpacity>
                    <View style={{ backgroundColor: 'white', padding: '5%', height: Dimensions.get('window').height * 0.7 }}>
                        <TouchableOpacity
                            onPress={() => setVisible(false)}
                        >
                            <Divider style={{ alignSelf: 'center', marginBottom: hp('2%'), backgroundColor: 'rgb(143,143,143)', width: wp('18.6%'), height: hp('0.5%') }} />
                        </TouchableOpacity>

                        <View style={{ alignItems: 'center' }}>
                            <Text style={[styles.valueTxt1, {}]}>Enter Doc. Name</Text>
                            <Input
                                onChangeText={(text) => setFileName(text)}
                                placeholder={'Doc Name'}
                                placeholderTextColor={'rgb(143, 143, 143)'}
                                inputStyle={{ fontSize: hp('2.3%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                value={fileName}
                                inputContainerStyle={{ borderBottomWidth: 0 }}
                                containerStyle={{ width: wp('60%'), marginVertical: hp('2%'), borderWidth: 1, height: hp('6%'), borderColor: 'rgb(143, 143, 143)', borderRadius: 4 }}
                            />
                            {
                                selectedFile != '' &&
                                <View style={{ flexDirection: 'row', height: hp('7%'), marginVertical: hp('1%'), alignItems: 'center', paddingHorizontal: '4%', borderRadius: 4, backgroundColor: 'rgb(236, 243, 254)', width: wp('60%'), justifyContent: 'space-between' }}>
                                    <Text style={styles.titleText}>{selectedFileName}</Text>
                                    <Icon
                                        color={'#F1696A'}
                                        type='feather'
                                        name='x'
                                        onPress={() => {
                                            setSelectedFile('')
                                            setFileName('')
                                        }}
                                    />
                                </View>
                            }
                            <TouchableOpacity
                                onPress={() => onPressUploadDocument()}
                                style={{ borderWidth: 0, alignItems: 'center', justifyContent: 'center', backgroundColor: '#3d87f1', width: wp('80%'), borderRadius: 4, height: hp('6%') }}
                            >
                                <Text style={styles.labelStyle}>Upload</Text>
                            </TouchableOpacity>

                            <View style={{ flexDirection: 'row', marginVertical: hp('10%'), justifyContent: 'space-between', alignItems: 'center' }}>
                                <TouchableOpacity
                                    onPress={() => onFilePostHandler()}
                                    style={{ borderWidth: 0, alignItems: 'center', marginHorizontal: wp('2%'), justifyContent: 'center', backgroundColor: '#3d87f1', width: wp('40%'), borderRadius: 4, height: hp('6%') }}
                                >
                                    <Text style={styles.labelStyle}>OK</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => setVisible(false)}
                                    style={{ borderWidth: 0, alignItems: 'center', justifyContent: 'center', backgroundColor: '#3d87f1', width: wp('40%'), borderRadius: 4, height: hp('6%') }}
                                >
                                    <Text style={styles.labelStyle}>Cancel</Text>
                                </TouchableOpacity>

                            </View>

                        </View>
                    </View>

                </View>

            </BottomSheet>


        </View>
    );
};

export default DocsNeeded;

const styles = StyleSheet.create({
    item_card: {
        marginBottom: hp('2%'),
        borderWidth: 0,
        height: hp('8%'),
        backgroundColor: '#fff',
        shadowColor: 'rgba(0,0,0,0.4)',
        elevation: 5,
        borderRadius: 4,
        paddingVertical: '3%',
        paddingHorizontal: '4%'
    },

    groupimage: {
        height: hp('30.5%'),
        width: wp('30.3%'),
    },


    valueTxt: {
        color: 'rgb(68, 67, 67)',
        fontSize: hp('2.1%'),
        fontFamily: 'SourceSansPro-Regular',
        paddingTop: '1%'
    },

    valueTxt1: {
        color: 'rgb(68, 67, 67)',
        fontSize: hp('2.4%'),
        fontFamily: 'SourceSansPro-Semibold',
        alignSelf: 'center'
    },

    labelStyle: {
        color: 'white',
        fontSize: hp('2.2%'),
        fontFamily: 'SourceSansPro-Regular',
        textTransform: 'capitalize'
    },

    titleTXT: {
        color: 'rgb(143, 143, 143)',
        fontSize: hp('1.9%'),
        fontFamily: 'SourceSansPro-Regular',
    },

    CircleShapeView: {
        width: 16,
        height: 16,
        borderRadius: 8,
        backgroundColor: '#3d87f1',
        marginBottom: hp('2%'),
    },
});
