import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    Animated
} from 'react-native';
import React, { useState, useRef, useEffect, useMemo } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { useDispatch, useSelector } from 'react-redux';
import * as getStageEventActionCreators from '../../actions/gantt-stage-action/index';
import EmptyLoader from '../../components/EmptyLoader'
import notifyMessage from '../../ToastMessages'
import { Avatar, Card, Button, List, Divider, IconButton } from 'react-native-paper';
import color from 'color';
import { FlatList } from 'react-native';
import {
    useCollapsibleSubHeader,
    CollapsibleSubHeaderAnimator,
} from 'react-navigation-collapsible';
import ProgressCircle from 'react-native-progress-circle'
import { ListItem, Icon } from 'react-native-elements';
import MenuDrawer from 'react-native-side-drawer'
import EventScreen from './EventScreen'
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import SideEmptyLoader from '../SideEmptyLoader';

const StageSlider = (props) => {

    const { project_id, is_sub_contractor, event_id, event_name, selectedTabIndex, token, navigation, data } = props

    const [selectedStage, setselectedStage] = React.useState('');
    const [selectedEventId, setselectedEventId] = React.useState('');
    const [selectedEventName, setselectedEventName] = React.useState('');
    const [expanded, setExpanded] = React.useState(false);
    const [open, setopen] = useState(false);
    const [loading, setLoading] = useState(false);

    const dispatch = useDispatch();

    useEffect(() => {
        setselectedEventId(event_id)
        setselectedEventName(event_name)
    }, [event_id])

    const handlePress = (stage, backend_key_for_stage) => {
        setExpanded(!expanded)
        dispatch(getStageEventActionCreators.getCustomEventMenuDetails(token, project_id, backend_key_for_stage))
        setselectedStage(stage)
        setLoading(true)
        setTimeout(() => setLoading(false), 1000)
    }

    const ganttEventReducer = useSelector(state => {
        return state.ganttEventMenuReducers.gantteventsmenu.get;
    })

    const { approvals, construction, engineering, material_handling, pre_requisites, procurement, site_handover } = data;

    const stagesData = [
        {
            id: 1,
            backend_stage: pre_requisites['stage'],
            stage: 'Pre-Requisite',
            percentage: pre_requisites['percentage']
        },

        {
            id: 2,
            stage: 'Approvals',
            percentage: approvals['percentage'],
            backend_stage: approvals['stage'],
        },

        {
            id: 3,
            stage: 'Engineering',
            percentage: engineering['percentage'],
            backend_stage: engineering['stage'],
        },

        {
            id: 4,
            stage: 'Procurement',
            percentage: procurement['percentage'],
            backend_stage: procurement['stage'],
        },

        {
            id: 5,
            stage: 'Material Handeling',
            percentage: material_handling['percentage'],
            backend_stage: material_handling['stage'],
        },

        {
            id: 6,
            stage: 'Construction',
            percentage: construction['percentage'],
            backend_stage: construction['stage'],
        },

        {
            id: 7,
            stage: 'Handover',
            percentage: site_handover['percentage'],
            backend_stage: site_handover['stage'],
        },
    ]

    const DrawerSet = () => {
        if (open == true) {
            setopen(false)
        } else {
            setopen(true)
        }
    }

    const onPressEvent = (eventId, eventName) => {
        DrawerSet()
        setselectedEventId(eventId)
        setselectedEventName(eventName)
    }

    const isSubContractorEvent = (event) => {
        return event.is_sub_contractor_event == true
    }

    const EventsFilterData = (stageEventsList) => {
        if (is_sub_contractor) {
            return stageEventsList.filter(isSubContractorEvent)
        } else if (!is_sub_contractor) {
            return stageEventsList
        }
    }

    const drawerContent = () => {
        return (
            <View style={{ marginTop: hp('16.3%'), flexDirection: 'row', backgroundColor: 'white', elevation: 4, height: hp('90%'), justifyContent: 'space-between' }}>
                <View style={{ width: wp('70%') }}>
                    <FlatList
                        backgroundColor='#ffffff'
                        data={stagesData}
                        // showsVerticalScrollIndicator
                        keyExtractor={(item, index) => index}
                        renderItem={({ item, index }) =>
                            <>
                                <TouchableWithoutFeedback
                                    onPress={() => { handlePress(item.stage, item.backend_stage) }}
                                >
                                    <Card.Title
                                        title={item.stage}
                                        titleStyle={styles.stagetitleTxt}
                                        left={
                                            () => <ProgressCircle
                                                percent={item.percentage}
                                                radius={25}
                                                borderWidth={3}
                                                color='#3D87F1'
                                                shadowColor="#fff"
                                                bgColor="#fff"
                                            >
                                                <Text style={{ fontSize: hp('2%') }}>{parseInt(item.percentage).toString() + '%'}</Text>
                                            </ProgressCircle>
                                        }
                                        right={() => <Icon color={'rgb(68, 67, 67)'} type='feather' name={expanded ? 'chevron-up' : 'chevron-right'} />}
                                    />
                                </TouchableWithoutFeedback>
                                {
                                    item.stage == selectedStage && expanded && ganttEventReducer?.success?.ok &&
                                    <View style={{ width: wp('60%'), alignSelf: 'flex-end' }}>
                                        {!loading ? (
                                            <FlatList
                                                backgroundColor='#ffffff'
                                                data={EventsFilterData(ganttEventReducer?.success?.data?.gantt_view_stage_events)}
                                                ListEmptyComponent={<Text>No Events</Text>}
                                                // showsVerticalScrollIndicator
                                                keyExtractor={(item, index) => index}
                                                renderItem={({ item, index }) =>
                                                    <TouchableWithoutFeedback
                                                        onPress={() => { onPressEvent(item.id, item.name) }}
                                                    >
                                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: wp('50%'), alignSelf: 'flex-end', paddingVertical: hp('1%') }}>
                                                            {
                                                                item.id === selectedEventId ?
                                                                    <Text style={{ alignSelf: 'center', fontSize: hp('1.5%'), color: '#3078f1' }}>{'\u2B24'}</Text>
                                                                    :
                                                                    <Text style={{ alignSelf: 'center', fontSize: hp('1.5%'), color: '#888' }}>{'\u2B24'}</Text>
                                                            }
                                                            <Text style={[styles.valueTxt, { width: wp('30%'), fontSize: hp('2%') }]}>{item.name}</Text>
                                                            <Icon color={'rgb(68, 67, 67)'} style={{ alignSelf: 'center', width: wp('6%'), height: hp('3%') }} type='feather' name='chevron-right' />
                                                        </View>
                                                    </TouchableWithoutFeedback>
                                                }
                                            />)
                                            :
                                            <SideEmptyLoader portrait={true} />
                                        }
                                    </View>

                                }
                            </>
                        }
                    />
                </View>
                <View style={{ alignSelf: 'center' }}>
                <TouchableOpacity
                        onPress={() => DrawerSet()}
                        style={{ width: wp('5%'), height: hp('10%'), borderTopLeftRadius: 4, borderBottomLeftRadius: 4, alignItems: 'center', justifyContent: 'center', backgroundColor: '#3d87f1' }}
                    >
                        <Icon color={'#fff'} size={14} type='ionicon' name={open?'caret-back-outline':'caret-forward'} />
                    </TouchableOpacity>
                </View>
            </View>
        );
    };

    return (
        <View style={styles.container}>
            <MenuDrawer
                open={open}
                drawerContent={drawerContent()}
                drawerPercentage={85}
                animationTime={250}
                overlay={true}
                opacity={0.8}
            >
                {
                    selectedEventName ?
                        <EventScreen
                            selectedTabIndex={selectedTabIndex}
                            eventId={selectedEventId}
                            eventName={selectedEventName}
                            token={token}
                            is_sub_contractor={is_sub_contractor}
                            navigation={navigation}
                            DrawerSet={DrawerSet}
                            open={open}
                        />
                        :
                        <EmptyLoader />
                }
            </MenuDrawer>
        </View>
    );
}


const styles = StyleSheet.create({
    container: {
        backgroundColor: "#fff",
        zIndex: 0,
        height: hp('100%')
    },
    ganttTxt: {
        fontFamily: 'SourceSansPro-Regular',
        fontSize: hp('3%'),
        color: 'rgb(68, 67, 67)'
    },

    labelStyle: {
        color: 'white',
        fontSize: hp('1.8%'),
        fontFamily: 'SourceSansPro-Regular',
    },

    titleTxt: {
        color: 'rgb(143,143,143)',
        fontSize: hp('1.5%'),
        fontFamily: 'SourceSansPro-Regular',
    },

    stagetitleTxt: {
        color: '#000000',
        fontSize: hp('2.2%'),
        fontFamily: 'SourceSansPro-Semibold'
    },

    valueTxt: {
        color: 'rgb(68, 67, 67)',
        fontSize: hp('1.8%'),
        fontFamily: 'SourceSansPro-Regular',
    },
})

export default StageSlider;



    // // Default EVENT SELECTION
    // const dispatch = useDispatch();
    // useEffect(() => {
    //     dispatch(getStageEventActionCreators.getCustomEventMenuDetails(token, project_id, selectedStage))
    // }, [])

    // useEffect(() => {
    //     if (selectedEventName == '') {
    //         let default_event = ganttEventReducer?.success?.data?.gantt_view_stage_events[0]
    //         setselectedEventId(default_event.id)
    //         setselectedEventName(default_event.name)
    //     }
    // }, [ganttEventReducer.success.ok])

