import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    useWindowDimensions,
    Animated
} from 'react-native';
import React, { useState, useEffect, useMemo } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { useDispatch, useSelector } from 'react-redux';
import * as getStageEventActionCreators from '../../actions/gantt-stage-action/index';
import EmptyLoader from '../../components/EmptyLoader'
import notifyMessage from '../../ToastMessages'
import { Avatar, Card, Button, List, Divider, IconButton } from 'react-native-paper';
import color from 'color';
import { FlatList } from 'react-native';
import {
    useCollapsibleSubHeader,
    CollapsibleSubHeaderAnimator,
} from 'react-navigation-collapsible';
import { FAB, Icon, BottomSheet, Input } from 'react-native-elements';
import config from '../../config'
import Toast from "react-native-toast-message";
import * as ganttActionCreators from "../../actions/gantt-stage-action/index";
import MyModalDropdown from './ModalDropDown';
import MyDatePicker from './DatePicker'
import * as actionCreators from '../../actions/gantt-stage-action/index';
import ModalDropdown from 'react-native-modal-dropdown';


const Tickets = (props) => {
    const {
        onScroll /* Event handler */,
        containerPaddingTop /* number */,
        scrollIndicatorInsetTop /* number */,
        translateY,
    } = useCollapsibleSubHeader();

    const data = props.data
    const { token, event_id, navigation, changeReload, projectId } = props.eventDetails
    const [visible, setVisible] = useState(false);
    const [spocList, setSpocList] = useState([]);
    const [spocObjectsList, setspocObjectsList] = useState([]);
    const [IconPosition, setIconPosition] = useState(false);

    const [spoc, setSpoc] = useState('Assigned To');
    const [date, setDate] = useState('');
    const [subject, setSubject] = useState('');
    const [comment, setComment] = useState('');

    // //(spocList)

    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(ganttActionCreators.getSpoc(token, { project_id: projectId }));
    }, []);

    const spocReducer = useSelector((state) => {
        return state.spocReducers.spoc.get;
    });

    useEffect(() => {
        if (spocReducer.success.ok) {
            let temp = spocReducer.success.data.members
            let spocNamesList = []
            temp.map((item, index) => spocNamesList.push(item.name))
            setSpocList(spocNamesList)
            setspocObjectsList(temp)
        }
    }, [spocReducer.success.ok]);

    const onChangeStatus = (value, id) => {
        if (value == 'Close') {
            const formData = new FormData();

            formData.append('event_id', event_id);
            formData.append('ticket_id', id);

            dispatch(actionCreators.changeTicketStatus(formData, token));
            changeReload()
        }
    }

    const [selectedTicketIndex, setselectedTicketIndex] = useState(null);
    const [isTicketExpanded, setisTicketExpanded] = useState(false);

    const onPressTickets = (index) => {
        setselectedTicketIndex(index)
        setisTicketExpanded(!isTicketExpanded)
    }

    const Item = (item, index) => {
        // console.log(item)
        return (
            <View elevation={8} style={[styles.cardContainer, { padding: hp('1%') }]}>
                <View style={{ flexDirection: 'row', paddingVertical: hp('0.2%'), }}>
                    <Text style={styles.raisedBy}>{item.raised_by}</Text>
                    <Text style={styles.raised}> raised a ticket</Text>
                </View>
                <Text style={[styles.description, { paddingVertical: hp('1%'), }]}>A Problem in the {item.stage} ...</Text>
                <Divider style={{ height: hp('0.2%'), backgroundColor: 'rgba(0,0,0,0.1)' }} />
                <View style={{ flexDirection: 'row', paddingTop: '2%', justifyContent: 'space-between' }}>
                    <Text style={styles.assigned}>Project Name</Text>
                    <Text style={styles.assigned}>Assigned To</Text>
                </View>
                <View style={{ flexDirection: 'row', paddingVertical: hp('1%'), justifyContent: 'space-between' }}>
                    <Text style={[styles.raised, { fontSize: hp('2%') }]}>{item.project_name}</Text>
                    <Text style={[styles.raised, { fontSize: hp('2%') }]}>{item.assigned_to}</Text>
                </View>
                <Divider style={{ height: hp('0.2%'), backgroundColor: 'rgba(0,0,0,0.1)' }} />

                <View style={{ flexDirection: 'row', paddingVertical: hp('1%'), justifyContent: 'space-between'}}>
                    <Text style={styles.assigned}>STATUS</Text>
                    <MyModalDropdown id={item.id} status={item.status} onChangeStatus={onChangeStatus} />
                </View>
                <Divider style={{ height: hp('0.2%'), backgroundColor: 'rgba(0,0,0,0.1)' }} />
                <TouchableOpacity
                    onPress={() => { onPressTickets(index) }}
                >
                    <View style={{ flexDirection: 'row', paddingVertical: hp('1%') }}>
                        <Text style={[styles.description, {width: wp('28%')}]}>Ticket Details{'   '}</Text>
                        <Icon color={'#3d87f1'} type='feather' size={22} style={{ borderWidth: 0 }} name={isTicketExpanded ? 'chevron-up' : 'chevron-down'} />
                    </View>
                </TouchableOpacity>
                {
                    (selectedTicketIndex == index && isTicketExpanded) &&
                    <View style={{ padding: '3%', alignItems: 'center' }}>
                        <View style={{ width: wp('65%'), padding: '3%', borderWidth: 1.2, borderColor: 'rgb(234, 234, 234)', borderRadius: 4, borderStyle: 'dashed' }}>
                            <Text style={[styles.title, { fontFamily: 'SourceSansPro-Semibold', color: 'rgb(68, 67, 67)' }]}>Raised By: {item.raised_by}</Text>
                            <Text style={[styles.title, { color: 'rgb(68, 67, 67)' }]}>A problem in the {item.stage} stage</Text>
                            <Text style={[styles.raised]}>{item.subject}</Text>
                            <Text style={[styles.title, { paddingTop: '5%', alignSelf: 'flex-start', color: 'rgb(194, 194, 194)' }]}>{item.date}{' '}{'\u2022'}{' '}{item.time}</Text>
                        </View>
                    </View>
                }

            </View>
        );
    }

    const onSelectDate = (date) => {
        setDate(date)
    }

    const onPressRaiseTicket = () => {

        const formData = new FormData();

        formData.append('event_id', event_id);
        formData.append('subject', subject);
        formData.append('comment', comment);
        formData.append('assigned_to_id', spoc);

        // //(event_id, spoc, comment, token, subject)

        Promise.resolve(dispatch(actionCreators.postTickets(formData, token)))
            .then(result => {
                if (result?.status === 200) {
                    Toast.show({
                        text1: 'New Ticket is Raised',
                        type: 'info',
                        autoHide: true,
                        position: 'bottom',
                        visibilityTime: 1000
                    })
                    setVisible(false)
                    changeReload()
                }
            })
    }

    return (
        <View style={{ width: wp('95%'), marginTop: hp('2%') }}>
            <View style={{ height: hp('55%') }}>
                <Animated.FlatList
                    backgroundColor='#ffffff'
                    data={data}
                    ListEmptyComponent={
                        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                            < FastImage
                                style={styles.groupimage}
                                source={require('../../assets/icons/group.png')}
                                resizeMode={FastImage.resizeMode.contain}
                            />
                            <Text style={[styles.valueTxt1, { color: 'rgb(179, 179, 179)', marginVertical: hp('3%') }]}>No Tickets Raised!</Text>
                        </View>
                    }
                    showsVerticalScrollIndicator
                    keyExtractor={(item, index) => index}
                    renderItem={({ item, index }) => <Item {...item} index={index} />}
                    onScroll={onScroll}
                    contentContainerStyle={{ paddingTop: containerPaddingTop, paddingLeft: hp('2%'), paddingRight: hp('3%'), paddingBottom: hp('0%') }}
                    scrollIndicatorInsets={{ top: scrollIndicatorInsetTop }}
                />
            </View>

            <TouchableOpacity
                onPress={() => setVisible(true)}
                style={{ borderWidth: 0, marginLeft: wp('5%'), alignItems: 'center', justifyContent: 'center', backgroundColor: '#3d87f1', width: wp('80%'), borderRadius: 4, height: hp('7.4%') }}
            >
                <Text style={styles.labelStyle}>+ New Ticket</Text>
            </TouchableOpacity>

            <BottomSheet
                isVisible={visible}
                containerStyle={{ backgroundColor: 'rgba(0.5, 0.25, 0, 0.2)' }}
            >
                {
                    spocReducer.success.ok &&
                    <View style={{ backgroundColor: 'white', padding: '5%', height: Dimensions.get('window').height * 0.7 }}>

                        <TouchableOpacity
                            onPress={() => setVisible(false)}
                        >
                            <Divider style={{ alignSelf: 'center', marginBottom: hp('2%'), backgroundColor: 'rgb(143,143,143)', width: wp('18.6%'), height: hp('0.5%') }} />
                        </TouchableOpacity>

                        <FlatList
                            data={['one']}
                            keyExtractor={(item, index) => index}
                            renderItem={
                                ({ item, index }) => {
                                    return <View>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                            <Text style={styles.titleTxt}>SPOC</Text>
                                            <ModalDropdown
                                                options={spocList}
                                                textStyle={{ fontSize: hp('2%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                                dropdownStyle={{ width: '60%', height: hp('40%'), marginLeft: -9, elevation: 8, marginTop: hp('.8%') }}
                                                dropdownTextStyle={{ fontSize: hp('2%'), color: 'rgb(143, 143, 143)' }}
                                                onSelect={(index, value) => setSpoc(spocObjectsList[index]['id'])}
                                                defaultValue={'Assigned To'}
                                                dropdownTextHighlightStyle={{ backgroundColor: 'rgb(236, 243, 254)' }}
                                                showsVerticalScrollIndicator={true}
                                                style={{ width: wp('60%'), borderWidth: 1, borderColor: 'rgb(143, 143, 143)', borderRadius: 4, padding: 8, marginVertical: hp('1%') }}
                                                renderRightComponent={() => <Icon color={'rgb(68, 67, 67)'} type='feather' name={IconPosition ? 'chevron-up' : 'chevron-down'} />}
                                                defaultTextStyle={{ color: 'rgb(68, 67, 67)' }}
                                                onDropdownWillShow={() => setIconPosition(true)}
                                                onDropdownWillHide={() => setIconPosition(false)}
                                            />
                                        </View>

                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                            <Text style={styles.titleTxt}>DATE</Text>
                                            <MyDatePicker
                                                onSelectDate={onSelectDate}
                                                styling={styles.dateStyle}
                                                defaultValue={date}
                                            />
                                        </View>

                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                            <Text style={styles.titleTxt}>SUBJECT</Text>
                                            <Input
                                                onChangeText={(text) => setSubject(text)}
                                                placeholder={'Subject'}
                                                placeholderTextColor={'rgb(143, 143, 143)'}
                                                inputStyle={{ fontSize: hp('2.3%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                                value={subject}
                                                inputContainerStyle={{ borderBottomWidth: 0 }}
                                                containerStyle={{ width: wp('60%'), marginVertical: hp('2%'), borderWidth: 1, height: hp('6%'), borderColor: 'rgb(143, 143, 143)', borderRadius: 4 }}
                                            />
                                        </View>

                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                            <Text style={styles.titleTxt}>COMMENT</Text>
                                            <Input
                                                onChangeText={(text) => setComment(text)}
                                                placeholder={'Comment'}
                                                placeholderTextColor={'rgb(143, 143, 143)'}
                                                inputStyle={{ fontSize: hp('2.3%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                                value={comment}
                                                multiline={true}
                                                inputContainerStyle={{ borderBottomWidth: 0 }}
                                                containerStyle={{ width: wp('60%'), borderWidth: 1, borderColor: 'rgb(143, 143, 143)', borderRadius: 4 }}
                                            />
                                        </View>

                                        <TouchableOpacity
                                            onPress={() => onPressRaiseTicket()}
                                            style={{ borderWidth: 0, marginTop: hp('10%'), marginLeft: wp('5%'), alignItems: 'center', justifyContent: 'center', backgroundColor: '#3d87f1', width: wp('80%'), borderRadius: 4, height: hp('7.4%') }}
                                        >
                                            <Text style={styles.labelStyle}>Raise Ticket</Text>
                                        </TouchableOpacity>
                                    </View>
                                }
                            }
                        />


                    </View>
                }
            </BottomSheet>
        </View>
    );
};

export default Tickets;

const styles = StyleSheet.create({
    item_card: {
        marginBottom: hp('2%'),
        borderWidth: 0,
        // height: hp('10%'),
        backgroundColor: '#fff',
        shadowColor: 'rgba(0,0,0,0.4)',
        elevation: 2,
        borderRadius: 4,
        paddingVertical: '3%',
        paddingHorizontal: '4%'
    },

    groupimage: {
        height: hp('30.5%'),
        width: wp('30.3%'),
    },

    titleText: {
        fontFamily: 'SourceSansPro-Regular',
        fontSize: hp('1.6%'),
        color: 'rgb(179, 179, 179)',
    },

    titleTxt: {
        color: 'rgb(143, 143, 143)',
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
        width: wp('30%')
    },

    cardContainer: {
        borderRadius: 4,
        borderLeftWidth: 3,
        borderLeftColor: "#3d87f1",
        backgroundColor: "#fff",
        paddingHorizontal: hp("3%"),
        margin: hp("1.5%"),
    },

    valueTxt: {
        color: 'rgb(68, 67, 67)',
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
        paddingTop: '5%'
    },

    dateStyle: {
        width: wp('60%'),
        borderWidth: 1,
        borderColor: 'rgb(143, 143, 143)',
        borderRadius: 4,
        paddingLeft: wp('3%'),
        paddingRight: wp('5%'),
        marginVertical: hp('1%')
    },

    labelStyle: {
        color: 'white',
        fontSize: hp('2.2%'),
        fontFamily: 'SourceSansPro-Regular',
    },

    CommentsTxt: {
        color: 'rgb(143, 143, 143)',
        fontSize: hp('1.8%'),
        fontFamily: 'SourceSansPro-Regular',
    },

    CircleShapeView: {
        width: 16,
        height: 16,
        borderRadius: 8,
        backgroundColor: '#3d87f1',
        marginBottom: hp('2%'),
    },

    assigned: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        textTransform: 'capitalize'
    },

    description: {
        fontSize: hp('2.2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: '#3d87f1',
        textTransform: 'capitalize',
        width: wp('80%')
    },

    raised: {
        fontSize: hp('2.2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(143, 143, 143)',
    },

    raisedBy: {
        fontSize: hp('2.2%'),
        fontFamily: 'SourceSansPro-SemiBold',
        color: 'rgb(68, 67, 67)',
    },



});
