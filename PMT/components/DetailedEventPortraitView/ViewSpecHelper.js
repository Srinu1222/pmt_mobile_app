import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    useWindowDimensions,
    Animated
} from 'react-native';
import React, { useState, useEffect, useMemo } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { useDispatch, useSelector } from 'react-redux';
// import * as getStageEventActionCreators from '../../actions/gantt-stage-action/index';
// import EmptyLoader from '../../components/EmptyLoader'
// import notifyMessage from '../../ToastMessages'
import { Avatar, Card, Button, List, Divider, IconButton } from 'react-native-paper';
import color from 'color';
import { FlatList } from 'react-native';
import {
    useCollapsibleSubHeader,
    CollapsibleSubHeaderAnimator,
} from 'react-navigation-collapsible';
import { FAB, Icon, BottomSheet } from 'react-native-elements';


const ViewSpecHelper = (params) => {

    switch (params.selectedChecklistTitle) {
        case 'List of All Drawings from Checklist' || 'SPOC for each drawing':
            var { drawing, spoc, start_date } = params
            return (
                <View>
                    <Text style={[styles.titleTxt, { fontSize: hp('2.2%'), marginVertical: hp('1%') }]}>{drawing}</Text>
                    <View style={{ flexDirection: 'row', marginVertical: hp('1%'), justifyContent: 'space-between', }}>
                        <Text style={styles.titleTxt}>SPOC</Text>
                        <Text style={styles.titleTxt}>Start Date</Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginVertical: hp('1%'), justifyContent: 'space-between', }}>
                        <Text style={[styles.titleTxt, { color: 'rgb(143, 143, 143)' }]}>{spoc.name}</Text>
                        <Text style={[styles.titleTxt, { color: 'rgb(143, 143, 143)' }]}>{start_date}</Text>
                    </View>
                    <Divider style={{ backgroundColor: 'rgb(232,232,232)', marginVertical: hp('2%'), height: hp('0.3%') }} />
                </View>
            );
            break;

        case 'Type of Cable':
            var { cable_type, cable_specification, cable_length } = params
            return (
                <View>
                    <View style={{ flexDirection: 'row', marginVertical: hp('1%'), justifyContent: 'space-between', }}>
                        <Text style={styles.titleTxt}>Cable Type</Text>
                        <Text style={[styles.titleTxt, { color: 'rgb(143, 143, 143)' }]}>{cable_type}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginVertical: hp('1%'), justifyContent: 'space-between', }}>
                        <Text style={styles.titleTxt}>Cable Specification</Text>
                        <Text style={[styles.titleTxt, { color: 'rgb(143, 143, 143)' }]}>{cable_specification}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginVertical: hp('1%'), justifyContent: 'space-between', }}>
                        <Text style={styles.titleTxt}>Cable Length</Text>
                        <Text style={[styles.titleTxt, { color: 'rgb(143, 143, 143)' }]}>{cable_length}</Text>
                    </View>
                    <Divider style={{ backgroundColor: 'rgb(232,232,232)', marginVertical: hp('2%'), height: hp('0.3%') }} />
                </View>
            );
            break;

        case 'Specifications of Revisions that are needed – Required: A way to specify the drawings that need revisions and specifying the revisions needed' || 'Recommended Changes if Any':
            var { drawing_name, change } = params
            return (
                <View>
                    <View style={{ flexDirection: 'row', marginVertical: hp('1%'), justifyContent: 'space-between', }}>
                        <Text style={styles.titleTxt}>Drawing Name</Text>
                        <Text style={[styles.titleTxt, { color: 'rgb(143, 143, 143)' }]}>{drawing_name}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginVertical: hp('1%'), justifyContent: 'space-between', }}>
                        <Text style={styles.titleTxt}>Change</Text>
                        <Text style={[styles.titleTxt, { color: 'rgb(143, 143, 143)' }]}>{change}</Text>
                    </View>
                    <Divider style={{ backgroundColor: 'rgb(232,232,232)', marginVertical: hp('2%'), height: hp('0.3%') }} />
                </View>
            );
            break;

        case 'Procurement Plan':
            var { component_name, spoc, start_date } = params
            return (
                <View>
                    <Text style={[styles.titleTxt, { fontSize: hp('2.2%'), marginVertical: hp('1%') }]}>{component_name}</Text>
                    <View style={{ flexDirection: 'row', marginVertical: hp('1%'), justifyContent: 'space-between', }}>
                        <Text style={styles.titleTxt}>SPOC</Text>
                        <Text style={styles.titleTxt}>Start Date</Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginVertical: hp('1%'), justifyContent: 'space-between', }}>
                        <Text style={[styles.titleTxt, { color: 'rgb(143, 143, 143)' }]}>{spoc.name}</Text>
                        <Text style={[styles.titleTxt, { color: 'rgb(143, 143, 143)' }]}>{start_date}</Text>
                    </View>
                    <Divider style={{ backgroundColor: 'rgb(232,232,232)', marginVertical: hp('2%'), height: hp('0.3%') }} />
                </View>
            );
            break;

        case 'Procurement Specifications':
            var { material_name, target_price, stock_quantity, preferred_brands, target_delivery_date, specifications } = params
            var dataTitles = [
                'Component Name',
                'Target Price',
                'Quantity',
                'Preferred Brands',
                'Delivery Date',
                'Specifications'
            ]
            var dataValues = [
                material_name, target_price, stock_quantity, preferred_brands, target_delivery_date, specifications
            ]

            return (
                <View>
                    {
                        dataTitles.map(
                            (item, index) => {
                                return (
                                    <View key={index} style={{ flexDirection: 'row', marginVertical: hp('1%'), justifyContent: 'space-between', }}>
                                        <Text style={styles.titleTxt}>{item}</Text>
                                        <Text style={[styles.titleTxt, {width: wp('50%'), color: 'rgb(143, 143, 143)' }]}>{dataValues[index]}</Text>
                                    </View>
                                );
                            }
                        )
                    }
                </View>
            );

            break;

        case 'Final Specifications':

            var { material_name, final_price, stock_quantity, final_brands, target_delivery_date, specifications } = params
            var dataTitles = [
                'Component Name',
                'Target Price',
                'Quantity',
                'Preferred Brands',
                'Delivery Date',
                'Specifications'
            ]
            var dataValues = [
                material_name, final_price, stock_quantity, final_brands, target_delivery_date, specifications
            ]

            return (
                <View>
                    {
                        dataTitles.map(
                            (item, index) => {
                                return (
                                    <View key={index} style={{ flexDirection: 'row', marginVertical: hp('1%'), justifyContent: 'space-between', }}>
                                        <Text style={styles.titleTxt}>{item}</Text>
                                        <Text style={[styles.titleTxt, { width: wp('50%'), color: 'rgb(143, 143, 143)' }]}>{dataValues[index]}</Text>
                                    </View>
                                );
                            }
                        )
                    }
                </View>
            );
            break;

        case 'Inspection Agency Details: To Enter Name, Contact person, Number, Email':
            var { component_name, pdi_date, contact_person_name, pdi_agency_name, contact_person_email, contact_phone_number } = params
            var dataTitles = [
                'Component Name',
                'PDI Date',
                'Contact Person Details',
                'PDI Agency Name',
                'Contact Person Email',
                'Contact Person Number',
            ]
            var dataValues = [
                component_name, pdi_date, contact_person_name, pdi_agency_name, contact_person_email, contact_phone_number
            ]

            return (
                <View>
                    {
                        dataTitles.map(
                            (item, index) => {
                                return (
                                    <View key={index} style={{ flexDirection: 'row', marginVertical: hp('1%'), justifyContent: 'space-between', }}>
                                        <Text style={styles.titleTxt}>{item}</Text>
                                        <Text style={[styles.titleTxt, { color: 'rgb(143, 143, 143)' }]}>{dataValues[index]}</Text>
                                    </View>
                                );
                            }
                        )
                    }
                    <Divider style={{ backgroundColor: 'rgb(232,232,232)', marginVertical: hp('2%'), height: hp('0.3%') }} />
                </View>
            );
            break;

        default:
            var { date, percentage, comment } = params
            var dataTitles = [
                'Date',
                'Percentage',
                'Comment'
            ]
            var dataValues = [
                date, percentage, comment
            ]
            return (
                <View>
                    {
                        dataTitles.map(
                            (item, index) => {
                                return (
                                    <View key={index} style={{ flexDirection: 'row', marginVertical: hp('1%'), justifyContent: 'space-between', }}>
                                        <Text style={styles.titleTxt}>{item}</Text>
                                        <Text style={[styles.titleTxt, { color: 'rgb(143, 143, 143)' }]}>{dataValues[index]}</Text>
                                    </View>
                                );
                            }
                        )
                    }
                    <Divider style={{ backgroundColor: 'rgb(232,232,232)', marginVertical: hp('2%'), height: hp('0.3%') }} />
                </View>
            );
            break;
    }
}

export default ViewSpecHelper;


const styles = StyleSheet.create({

    titleTxt: {
        color: 'rgb(68, 67, 67)',
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
        // width: wp('50%')
    },

    CommentsTxt: {
        color: 'rgb(143, 143, 143)',
        fontSize: hp('1.8%'),
        fontFamily: 'SourceSansPro-Regular',
    },

    CircleShapeView: {
        width: 16,
        height: 16,
        borderRadius: 8,
        backgroundColor: '#3d87f1',
        marginBottom: hp('2%'),
    },

    dueDateTxt: {
        fontFamily: 'SourceSansPro-Regular',
        fontSize: hp('2%'),
        color: 'rgb(179, 179, 179)',
        borderRightWidth: 1,
        borderRightColor: 'rgb(179, 179, 179)',
        paddingRight: '2%'
    },

});
