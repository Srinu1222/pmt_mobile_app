import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    useWindowDimensions,
    Animated,
    Picker
} from 'react-native';
import React, { useState, useEffect, useMemo } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { useDispatch, useSelector } from 'react-redux';
// import * as getStageEventActionCreators from '../../actions/gantt-stage-action/index';
// import EmptyLoader from '../../components/EmptyLoader'
// import notifyMessage from '../../ToastMessages'
import { Avatar, Card, Button, TextInput, List, Divider, IconButton } from 'react-native-paper';
import color from 'color';
import { FlatList } from 'react-native';
import {
    useCollapsibleSubHeader,
    CollapsibleSubHeaderAnimator,
} from 'react-navigation-collapsible';
import { FAB, Icon, BottomSheet, Input } from 'react-native-elements';
import { IndexPath, Layout, Select, SelectItem } from '@ui-kitten/components';
import { drawings, component_list, drawings_list } from './constants'
import DropDownPicker from 'react-native-dropdown-picker';
import ModalDropdown from 'react-native-modal-dropdown';
import MyDatePicker from './DatePicker'
import UploadPo from './UploadPo'

const EditSpecHelper = (params) => {
    var { inventory } = params
    switch (params.selectedChecklistTitle) {
        case 'SPOC for each drawing':
        case 'List of All Drawings from Checklist':
            var { spoc_list, specReducerState, dictPosition, setspecReducerState } = params
            // To display Spoc Names
            var spocNamesList = []
            spoc_list.map((item, index) => spocNamesList.push(item.name))

            var onSelectValue = (selectedIndex, value, index) => {
                if (index == 0) {
                    let newArr = [...specReducerState];
                    newArr[dictPosition] = {
                        ...newArr[dictPosition],
                        drawing: value,
                    };
                    setspecReducerState([...newArr]);
                } else {
                    let i = { 'name': value, 'id': spoc_list[selectedIndex].id }
                    let newArr = [...specReducerState];
                    newArr[dictPosition] = {
                        ...newArr[dictPosition],
                        spoc: i,
                    };
                    setspecReducerState([...newArr]);
                }
            }

            var onSelectDate = (value) => {
                let newArr = [...specReducerState];
                newArr[dictPosition] = {
                    ...newArr[dictPosition],
                    start_date: value,
                };
                setspecReducerState([...newArr]);
            }

            var dropdownItems = [
                {
                    'title': 'DRAWING',
                    'data': drawings,
                    'default_value': specReducerState[dictPosition]['drawing']
                },
                {
                    'title': 'SPOC',
                    'data': spocNamesList,
                    'default_value': specReducerState[dictPosition]['spoc'].name
                },
            ]

            var [IconPosition, setIconPosition] = useState(false);

            return (
                <View>
                    {
                        dropdownItems.map(
                            (item, i) => {
                                return (
                                    <View key={i} style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                        <Text style={styles.titleTxt}>{item.title}</Text>
                                        <ModalDropdown
                                            options={item.data}
                                            textStyle={{ fontSize: hp('2%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                            dropdownStyle={{ width: '60%', height: hp('40%'), marginLeft: -9, elevation: 8, marginTop: hp('.8%') }}
                                            dropdownTextStyle={{ fontSize: hp('2%'), color: 'rgb(143, 143, 143)' }}
                                            onSelect={(index, value) => onSelectValue(index, value, i)}
                                            defaultValue={item.default_value}
                                            dropdownTextHighlightStyle={{ backgroundColor: 'rgb(236, 243, 254)' }}
                                            showsVerticalScrollIndicator={true}
                                            style={{ width: wp('60%'), borderWidth: 1, borderColor: 'rgb(143, 143, 143)', borderRadius: 4, padding: 8, marginVertical: hp('1%') }}
                                            renderRightComponent={() => <Icon color={'rgb(68, 67, 67)'} type='feather' name={IconPosition ? 'chevron-up' : 'chevron-down'} />}
                                            defaultTextStyle={{ color: 'rgb(68, 67, 67)' }}
                                            onDropdownWillShow={() => setIconPosition(true)}
                                            onDropdownWillHide={() => setIconPosition(false)}
                                        />
                                    </View>
                                );
                            }
                        )
                    }
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={styles.titleTxt}>Start date</Text>
                        <MyDatePicker
                            onSelectDate={onSelectDate}
                            styling={styles.dateStyle}
                            defaultValue={specReducerState[dictPosition]['start_date']}
                        />
                    </View>
                    <Divider style={{ backgroundColor: 'rgb(232,232,232)', marginVertical: hp('2%'), height: hp('0.3%') }} />
                </View>
            );
            break;

        case 'Type of Cable':
            var { spoc_list, specReducerState, dictPosition, setspecReducerState } = params
            var [IconPosition, setIconPosition] = useState(false);
            return (
                <View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={styles.titleTxt}>Cable Type</Text>
                        <ModalDropdown
                            options={["Ac cable", "Dc cable"]}
                            textStyle={{ fontSize: hp('2%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                            dropdownStyle={{ width: '60%', height: hp('12%'), marginLeft: -9, elevation: 8, marginTop: hp('.8%') }}
                            dropdownTextStyle={{ fontSize: hp('2%'), color: 'rgb(143, 143, 143)' }}
                            onSelect={(index, value) => {
                                let newArr = [...specReducerState];
                                newArr[dictPosition] = {
                                    ...newArr[dictPosition],
                                    cable_type: value,
                                };
                                setspecReducerState([...newArr]);
                            }}
                            defaultValue={specReducerState[dictPosition]['cable_type']}
                            dropdownTextHighlightStyle={{ backgroundColor: 'rgb(236, 243, 254)' }}
                            showsVerticalScrollIndicator={true}
                            style={{ width: wp('60%'), borderWidth: 1, borderColor: 'rgb(143, 143, 143)', borderRadius: 4, padding: 8, marginVertical: hp('1%') }}
                            renderRightComponent={() => <Icon color={'rgb(68, 67, 67)'} type='feather' name={IconPosition ? 'chevron-up' : 'chevron-down'} />}
                            defaultTextStyle={{ color: 'rgb(143, 143, 143)' }}
                            onDropdownWillShow={() => setIconPosition(true)}
                            onDropdownWillHide={() => setIconPosition(false)}
                        />
                    </View>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={styles.titleTxt}>Specification</Text>
                        <Input
                            placeholder={'Cable Specification'}
                            onChangeText={text => {
                                let newArr = [...specReducerState];
                                newArr[dictPosition] = {
                                    ...newArr[dictPosition],
                                    cable_specification: text,
                                };
                                setspecReducerState([...newArr]);
                            }}
                            inputStyle={{ fontSize: hp('2%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                            value={specReducerState[dictPosition]['cable_specification']}
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                            containerStyle={{ width: wp('60%'), borderWidth: 1, height: hp('6%'), borderColor: 'rgb(143, 143, 143)', borderRadius: 4 }}
                        />
                    </View>

                    <View style={{ flexDirection: 'row', marginVertical: hp('1%'), justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={styles.titleTxt}>Cable Length</Text>
                        <Input
                            placeholder={'Cable Length'}
                            onChangeText={text => {
                                let newArr = [...specReducerState];
                                newArr[dictPosition] = {
                                    ...newArr[dictPosition],
                                    cable_length: text,
                                };
                                setspecReducerState([...newArr]);
                            }}
                            inputStyle={{ fontSize: hp('2%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                            value={specReducerState[dictPosition]['cable_length']}
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                            containerStyle={{ width: wp('60%'), borderWidth: 1, height: hp('6%'), borderColor: 'rgb(143, 143, 143)', borderRadius: 4 }}
                        />
                    </View>
                    <Divider style={{ backgroundColor: 'rgb(232,232,232)', marginVertical: hp('2%'), height: hp('0.3%') }} />
                </View >
            );
            break;

        case 'Specifications of Revisions that are needed – Required: A way to specify the drawings that need revisions and specifying the revisions needed':
        case 'Recommended Changes if Any':
            // var { drawing_name, change } = params
            var { specReducerState, dictPosition, setspecReducerState } = params
            var [IconPosition, setIconPosition] = useState(false);
            return (
                <View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={styles.titleTxt}>Drawing</Text>
                        <ModalDropdown
                            options={drawings}
                            textStyle={{ fontSize: hp('2%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                            dropdownStyle={{ width: '60%', height: hp('12%'), marginLeft: -9, elevation: 8, marginTop: hp('.8%') }}
                            dropdownTextStyle={{ fontSize: hp('2%'), color: 'rgb(143, 143, 143)' }}
                            onSelect={(index, value) => {
                                let newArr = [...specReducerState];
                                newArr[dictPosition] = {
                                    ...newArr[dictPosition],
                                    drawing_name: value,
                                };
                                setspecReducerState([...newArr]);
                            }}
                            defaultValue={specReducerState[dictPosition]['drawing_name']}
                            dropdownTextHighlightStyle={{ backgroundColor: 'rgb(236, 243, 254)' }}
                            showsVerticalScrollIndicator={true}
                            style={{ width: wp('60%'), borderWidth: 1, borderColor: 'rgb(143, 143, 143)', borderRadius: 4, padding: 8, marginVertical: hp('1%') }}
                            renderRightComponent={() => <Icon color={'rgb(68, 67, 67)'} type='feather' name={IconPosition ? 'chevron-up' : 'chevron-down'} />}
                            defaultTextStyle={{ color: 'rgb(143, 143, 143)' }}
                            onDropdownWillShow={() => setIconPosition(true)}
                            onDropdownWillHide={() => setIconPosition(false)}
                        />
                    </View>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={styles.titleTxt}>Change</Text>
                        <Input
                            placeholder={'Change'}
                            onChangeText={text => {
                                let newArr = [...specReducerState];
                                newArr[dictPosition] = {
                                    ...newArr[dictPosition],
                                    change: text,
                                };
                                setspecReducerState([...newArr]);
                            }}
                            inputStyle={{ fontSize: hp('2%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                            value={specReducerState[dictPosition]['change']}
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                            containerStyle={{ width: wp('60%'), borderWidth: 1, height: hp('6%'), borderColor: 'rgb(143, 143, 143)', borderRadius: 4 }}
                        />
                    </View>
                    <Divider style={{ backgroundColor: 'rgb(232,232,232)', marginVertical: hp('2%'), height: hp('0.3%') }} />
                </View >
            );
            break;

        case 'Procurement Plan':
            var { spoc_list, specReducerState, dictPosition, setspecReducerState } = params
            var { component_name, spoc, start_date } = params

            // To display Spoc Names
            var spocNamesList = []
            spoc_list.map((item, index) => spocNamesList.push(item.name))

            var onSelectValue = (selectedIndex, value, index) => {
                if (index == 0) {
                    let newArr = [...specReducerState];
                    newArr[dictPosition] = {
                        ...newArr[dictPosition],
                        component_name: value,
                    };
                    setspecReducerState([...newArr]);
                } else {
                    let i = { 'name': value, 'id': spoc_list[selectedIndex].id }
                    let newArr = [...specReducerState];
                    newArr[dictPosition] = {
                        ...newArr[dictPosition],
                        spoc: i,
                    };
                    setspecReducerState([...newArr]);
                }
            }

            var onSelectDate = (value) => {
                let newArr = [...specReducerState];
                newArr[dictPosition] = {
                    ...newArr[dictPosition],
                    start_date: value,
                };
                setspecReducerState([...newArr]);
            }

            var dropdownItems = [
                {
                    'title': 'COMPONENT NAME',
                    'data': component_list,
                    'default_value': specReducerState[dictPosition]['component_name']
                },
                {
                    'title': 'SPOC',
                    'data': spocNamesList,
                    'default_value': specReducerState[dictPosition]['spoc'].name
                },
            ]

            var [IconPosition, setIconPosition] = useState(false);

            return (
                <View>
                    {
                        dropdownItems.map(
                            (item, i) => {
                                return (
                                    <View key={i} style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                        <Text style={styles.titleTxt}>{item.title}</Text>
                                        <ModalDropdown
                                            options={item.data}
                                            textStyle={{ fontSize: hp('2%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                            dropdownStyle={{ width: '60%', height: hp('40%'), marginLeft: -9, elevation: 8, marginTop: hp('.8%') }}
                                            dropdownTextStyle={{ fontSize: hp('2%'), color: 'rgb(143, 143, 143)' }}
                                            onSelect={(index, value) => onSelectValue(index, value, i)}
                                            defaultValue={item.default_value}
                                            dropdownTextHighlightStyle={{ backgroundColor: 'rgb(236, 243, 254)' }}
                                            showsVerticalScrollIndicator={true}
                                            style={{ width: wp('60%'), borderWidth: 1, borderColor: 'rgb(143, 143, 143)', borderRadius: 4, padding: 8, marginVertical: hp('1%') }}
                                            renderRightComponent={() => <Icon color={'rgb(68, 67, 67)'} type='feather' name={IconPosition ? 'chevron-up' : 'chevron-down'} />}
                                            defaultTextStyle={{ color: 'rgb(68, 67, 67)' }}
                                            onDropdownWillShow={() => setIconPosition(true)}
                                            onDropdownWillHide={() => setIconPosition(false)}
                                        />
                                    </View>
                                );
                            }
                        )
                    }
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={styles.titleTxt}>Start date</Text>
                        <MyDatePicker
                            onSelectDate={onSelectDate}
                            styling={styles.dateStyle}
                            defaultValue={specReducerState[dictPosition]['start_date']}
                        />
                    </View>

                    <Divider style={{ backgroundColor: 'rgb(232,232,232)', marginVertical: hp('2%'), height: hp('0.3%') }} />
                </View>
            );
            break;

        case 'Procurement Specifications':
            var [IconPosition, setIconPosition] = useState(false);
            var { specReducerState, dictPosition, setspecReducerState } = params
            var valuesDict = specReducerState[dictPosition]

            var data = [
                ['stock_quantity', "Quantity (No.of pieces)"],
                ['target_price', 'Target Price'],
                ['target_delivery_date', 'Delivery Date'],
                ['preferred_brands', 'Preferred Brands'],
                ['specifications', 'Specifications']
            ]

            var onSelectDate = (value) => {
                let newArr = [...specReducerState];
                newArr[dictPosition] = {
                    ...newArr[dictPosition],
                    target_delivery_date: value,
                };
                setspecReducerState([...newArr]);
            }

            return (
                <View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={styles.titleTxt}>Component Name</Text>
                        <Input
                            placeholder={'Component Name'}
                            editable={false}
                            inputStyle={{ fontSize: hp('2.3%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                            value={inventory}
                            multiline={true}
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                            containerStyle={{ width: wp('60%'), borderWidth: 1, height: hp('6%'), borderColor: 'rgb(143, 143, 143)', borderRadius: 4 }}
                        />
                    </View>
                    {
                        data.map(
                            (item, index) => {
                                var dynamic_props = index == 4 ? {} : { height: hp('6%') }
                                return (
                                    <View key={index} style={{ flexDirection: 'row', marginVertical: hp('1%'), justifyContent: 'space-between', alignItems: 'center' }}>
                                        <Text style={styles.titleTxt}>{item[1]}</Text>

                                        {
                                            index != 2 ?
                                                <Input
                                                    placeholder={item[1]}
                                                    onChangeText={text => {
                                                        let newArr = [...specReducerState];
                                                        newArr[dictPosition] = {
                                                            ...newArr[dictPosition],
                                                            [item[0]]: text,
                                                        };
                                                        setspecReducerState([...newArr]);
                                                    }}
                                                    inputStyle={{ fontSize: hp('2.3%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                                    value={specReducerState[0][item[0]]}
                                                    multiline={true}
                                                    inputContainerStyle={{ borderBottomWidth: 0 }}
                                                    containerStyle={{ width: wp('60%'), borderWidth: 1, ...dynamic_props, borderColor: 'rgb(143, 143, 143)', borderRadius: 4 }}
                                                />
                                                :
                                                <MyDatePicker
                                                    onSelectDate={onSelectDate}
                                                    styling={styles.dateStyle}
                                                    defaultValue={specReducerState[0][item[0]]}
                                                />
                                        }
                                    </View>
                                );
                            }
                        )
                    }
                </View >
            );
            break;

        case 'Final Specifications':
            var [IconPosition, setIconPosition] = useState(false);
            var { specReducerState, dictPosition, setspecReducerState } = params
            var valuesDict = specReducerState[dictPosition]
            var data = [
                ['final_price', 'Target Price'],
                ['stock_quantity', "Quantity (No.of pieces)"],
                ['final_brands', 'Preferred Brands'],
                ['target_delivery_date', 'Delivery Date'],
                ['specifications', 'Specifications']
            ]

            var onSelectDate = (value) => {
                let newArr = [...specReducerState];
                newArr[dictPosition] = {
                    ...newArr[dictPosition],
                    target_delivery_date: value,
                };
                setspecReducerState([...newArr]);
            }

            return (
                <View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={styles.titleTxt}>Component Name</Text>
                        <Input
                            placeholder={'Component Name'}
                            editable={false}
                            inputStyle={{ fontSize: hp('2.3%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                            value={inventory}
                            multiline={true}
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                            containerStyle={{ width: wp('60%'), borderWidth: 1, height: hp('6%'), borderColor: 'rgb(143, 143, 143)', borderRadius: 4 }}
                        />
                    </View>
                    {
                        data.map(
                            (item, index) => {
                                var dynamic_props = index == 4 ? {} : { height: hp('6%') }
                                return (
                                    <View key={index} style={{ flexDirection: 'row', marginVertical: hp('1%'), justifyContent: 'space-between', alignItems: 'center' }}>
                                        <Text style={styles.titleTxt}>{item[1]}</Text>
                                        {
                                            index != 3 ?
                                                <Input
                                                    placeholder={item[1]}
                                                    onChangeText={text => {
                                                        let newArr = [...specReducerState];
                                                        newArr[dictPosition] = {
                                                            ...newArr[dictPosition],
                                                            [item[0]]: text,
                                                        };
                                                        setspecReducerState([...newArr]);
                                                    }}
                                                    inputStyle={{ fontSize: hp('2.3%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                                    value={specReducerState[0][item[0]]}
                                                    multiline={true}
                                                    inputContainerStyle={{ borderBottomWidth: 0 }}
                                                    containerStyle={{ width: wp('60%'), borderWidth: 1, ...dynamic_props, borderColor: 'rgb(143, 143, 143)', borderRadius: 4 }}
                                                />
                                                :
                                                <MyDatePicker
                                                    onSelectDate={onSelectDate}
                                                    styling={styles.dateStyle}
                                                    defaultValue={specReducerState[0][item[0]]}
                                                />
                                        }
                                    </View>
                                );
                            }
                        )
                    }
                    <UploadPo
                        specReducerState={specReducerState}
                        setspecReducerState={setspecReducerState}
                    />
                </View >
            );
            break;

        case 'Inspection Agency Details: To Enter Name, Contact person, Number, Email':
            var [IconPosition, setIconPosition] = useState(false);
            var { specReducerState, dictPosition, setspecReducerState } = params
            var valuesDict = specReducerState[dictPosition]

            var data = [
                ['component_name', 'Component Name'],
                ['pdi_agency_name', 'PDI Agency Name'],
                ['pdi_date', 'PDI Date'],
                ['contact_person_name', 'Contact Person Name'],
                ['contact_person_email', 'Contact Person Email'],
                ['contact_phone_number', 'Contact Phone Number'],
            ]

            var onSelectDate = (value) => {
                let newArr = [...specReducerState];
                newArr[dictPosition] = {
                    ...newArr[dictPosition],
                    pdi_date: value,
                };
                setspecReducerState([...newArr]);
            }

            return (
                <View>
                    {
                        data.map((item, index) => {
                            return (
                                <View key={index} style={{ flexDirection: 'row', marginVertical: hp('1%'), justifyContent: 'space-between', alignItems: 'center' }}>
                                    <Text style={styles.titleTxt}>{item[1]}</Text>
                                    {
                                        index != 2 ?
                                            <Input
                                                placeholder={item[1]}
                                                onChangeText={text => {
                                                    let newArr = [...specReducerState];
                                                    newArr[dictPosition] = {
                                                        ...newArr[dictPosition],
                                                        [item[0]]: text,
                                                    };
                                                    setspecReducerState([...newArr]);
                                                }}
                                                inputStyle={{ fontSize: hp('2.3%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                                value={specReducerState[0][item[0]]}
                                                inputContainerStyle={{ borderBottomWidth: 0 }}
                                                containerStyle={{ width: wp('60%'), borderWidth: 1, height: hp('6%'), borderColor: 'rgb(143, 143, 143)', borderRadius: 4 }}
                                            />
                                            :
                                            <MyDatePicker
                                                onSelectDate={onSelectDate}
                                                styling={styles.dateStyle}
                                                defaultValue={specReducerState[0][item[0]]}
                                            />
                                    }
                                </View>
                            );
                        })
                    }
                </View >
            );
            break;

        default:
            var { specReducerState, dictPosition, setspecReducerState } = params
            var onSelectDate = (value) => {
                let newArr = [...specReducerState];
                newArr[dictPosition] = {
                    ...newArr[dictPosition],
                    date: value,
                };
                setspecReducerState([...newArr]);
            }
            return (
                <View>
                    <View style={{ flexDirection: 'row', marginVertical: hp('1%'), justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={styles.titleTxt}>date</Text>
                        <MyDatePicker
                            onSelectDate={onSelectDate}
                            styling={styles.dateStyle}
                        />
                    </View>

                    <View style={{ flexDirection: 'row', marginVertical: hp('1%'), justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={styles.titleTxt}>Percentage</Text>
                        <Input
                            placeholder='Quantity(Kg)'
                            onChangeText={text => {
                                let newArr = [...specReducerState];
                                newArr[dictPosition] = {
                                    ...newArr[dictPosition],
                                    percentage: text,
                                };
                                setspecReducerState([...newArr]);
                            }}
                            inputStyle={{ fontSize: hp('2%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                            value={specReducerState[dictPosition]['percentage']}
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                            containerStyle={{ width: wp('60%'), borderWidth: 1, height: hp('6%'), borderColor: 'rgb(143, 143, 143)', borderRadius: 4 }}
                        />
                    </View>

                    <View style={{ flexDirection: 'row', marginVertical: hp('1%'), justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={styles.titleTxt}>Comment</Text>
                        <Input
                            placeholder={'Comment'}
                            onChangeText={text => {
                                let newArr = [...specReducerState];
                                newArr[dictPosition] = {
                                    ...newArr[dictPosition],
                                    comment: text,
                                };
                                setspecReducerState([...newArr]);
                            }}
                            inputStyle={{ fontSize: hp('2.3%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                            value={specReducerState[dictPosition]['comment']}
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                            multiline={true}
                            containerStyle={{ width: wp('60%'), borderWidth: 1, borderColor: 'rgb(143, 143, 143)', borderRadius: 4 }}
                        />
                    </View>
                </View >
            );
            break;

    }
}


export default EditSpecHelper;


const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 40,
        alignItems: "center"
    },

    titleTxt: {
        color: 'rgb(143, 143, 143)',
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
        width: wp('30%')
    },

    CommentsTxt: {
        color: 'rgb(143, 143, 143)',
        fontSize: hp('1.8%'),
        fontFamily: 'SourceSansPro-Regular',
    },

    dateStyle: {
        width: wp('60%'),
        borderWidth: 1,
        borderColor: 'rgb(143, 143, 143)',
        borderRadius: 4,
        paddingLeft: wp('3%'),
        paddingRight: wp('5%'),
        marginVertical: hp('1%')
    },

    CircleShapeView: {
        width: 16,
        height: 16,
        borderRadius: 8,
        backgroundColor: '#3d87f1',
        marginBottom: hp('2%'),
    },

    dueDateTxt: {
        fontFamily: 'SourceSansPro-Regular',
        fontSize: hp('2%'),
        color: 'rgb(179, 179, 179)',
        borderRightWidth: 1,
        borderRightColor: 'rgb(179, 179, 179)',
        paddingRight: '2%'
    },
});
