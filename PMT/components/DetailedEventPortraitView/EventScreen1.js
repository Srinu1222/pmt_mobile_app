import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    useWindowDimensions,
    Animated
} from 'react-native';
import React, { useState, useEffect, useMemo } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { useDispatch, useSelector } from 'react-redux';
import * as getStageEventActionCreators from '../../actions/gantt-stage-action/index';
import * as actionCreators from "../../actions/gantt-stage-action/index";
import EmptyLoader from '../../components/EmptyLoader'
import notifyMessage from '../../ToastMessages'
import { Avatar, Card, Button, List, Divider, IconButton } from 'react-native-paper';
import { ListItem, Icon } from 'react-native-elements';
import color from 'color';
import { FlatList } from 'react-native';
import {
    useCollapsibleSubHeader,
    CollapsibleSubHeaderAnimator,
} from 'react-navigation-collapsible';
import StageSlider from './StageSlider'
import Dots from 'react-native-dots-pagination';
import {
    ScalingDot,
    SlidingBorder,
    ExpandingDot,
    SlidingDot,
} from 'react-native-animated-pagination-dots';
import PreRequisite from './EventPreRequisites'
import Checklist from './EventChecklist'
import Tickets from './EventTickets'
import DocsNeeded from './EventDocsNeeded'
import Approval from './EventApproval';
import Photos from './EventPhotos';
import { statusColor } from './constants';
import ModalDropdown from 'react-native-modal-dropdown';
import Toast from "react-native-toast-message";


const EventScreen1 = (props) => {


    // Project Details
    const [projectId, setProjectId] = useState('');
    const [isSubContractor, setisSubContractor] = useState('');
    const [projectName, setProjectName] = useState('');
    const drawerReducer = useSelector((state) => {
        return state.getProjectReducers.selected_project.data;
    });
    useEffect(() => {
        setProjectId(drawerReducer?.id)
        setProjectName(drawerReducer.name)
        setisSubContractor(drawerReducer?.is_sub_contractor)
    }, [drawerReducer])

    const {
        start_date,
        end_date,
        status,
        customer_approval_required,
        is_sub_contractor_event,
        event_name,
        approval_status,
        sub_contractor_approver,
        event_approval_date,
        is_requested,
        can_work,
        can_approve,
        approval
    } = props.data.event

    // const [activeIndex, setactiveIndex] = useState(0);
    const { event_id, token, navigation, is_sub_contractor, changeReload, open, changeActiveIndex, activeIndex } = props

    const EventTabs = ['Pre-Requisites', 'Checklist', 'Tickets', 'Documents', 'Photos', 'Approval']
    const eventDetails = {
        'projectId': projectId,
        'projectName': projectName,
        'event_id': event_id,
        'event_name': event_name,
        'is_sub_contractor_event': is_sub_contractor_event,
        'sub_contractor_approver': sub_contractor_approver,
        'is_requested': is_requested,
        'event_approval_date': event_approval_date,
        'isSubContractor': isSubContractor,
        'can_work': can_work,
        'approval': approval,
        'can_approve': can_approve,
        'approval_status': approval_status,
        'customer_approval_required': customer_approval_required,
        'token': token,
        'navigation': navigation,
        'changeReload': changeReload,
        'activeIndex': activeIndex,
        'stage': props.data.event?.stage,
        'inventory': props.data.event.inventory
    }

    const tabScreenData = [
        props.data.event.pre_requisites,
        props.data.event.checklist,
        props.data.event.tickets,
        props.data.event.documents,
        props.data.event.photos,
        {
            'sub_contractor_spoc': props.data.event.sub_contractor_spoc,
            'spoc': props.data.event.spoc,
        }
    ]

    const tabScreenComponents = [
        <Checklist data={tabScreenData[activeIndex]} eventDetails={eventDetails} />,
        <Checklist data={tabScreenData[activeIndex]} eventDetails={eventDetails} />,
        <Tickets data={tabScreenData[activeIndex]} eventDetails={eventDetails} />,
        <DocsNeeded data={tabScreenData[activeIndex]} eventDetails={eventDetails} />,
        <Photos data={tabScreenData[activeIndex]} eventDetails={eventDetails} />,
        <Approval data={tabScreenData[activeIndex]} eventDetails={eventDetails} />,
    ]

    const { width } = useWindowDimensions();
    const scrollX = React.useRef(new Animated.Value(0)).current;
    const keyExtractor = React.useCallback((item, index) => index, []);

    const onPressBtn = (item, index) => {
        changeActiveIndex(index)
    }

    const onPressSliderBtn = (params) => {
        props.DrawerSet()
    }

    const HeaderData = [
        { id: 1, title: 'Status', value: status },
        { id: 2, title: 'Start Date', value: start_date },
        { id: 3, title: 'End Date', value: end_date },
    ]


    const dispatch = useDispatch();
    const handleMenuClick = (value) => {
        const formData = new FormData();
        formData.append("event_id", event_id);
        formData.append("status", value);

        Promise.resolve(dispatch(actionCreators.changeStatus(formData, token, isSubContractor))).then(
            (result) => {
                if (result.status === 200) {
                    Toast.show({
                        text1: event_name + ' ' + value,
                        type: 'success',
                        autoHide: true,
                        position: 'bottom',
                        visibilityTime: 1000
                    })
                    changeReload()
                }
            }
        );
    }

    const HeaderPart = (params) => {
        const [IconPosition, setIconPosition] = useState(false);

        return (
            <Card
                // elevation={5}
                style={{ shadowColor: 'black', backgroundColor: 'white' }}
            >
                <View style={{ display: 'flex', padding: '4%', width: wp('50%'), justifyContent: 'space-around', flexDirection: 'row' }}>
                    <Icon color={'rgb(68, 67, 67)'} type='feather' onPress={() => navigation.goBack()} name={'chevron-left'} />
                    <Text style={styles.ganttTxt}>{event_name.length < 12 ? event_name : event_name.substring(0, 12) + '...'}</Text>
                </View>
                <Divider style={{ backgroundColor: 'rgb(232, 232, 232)', height: hp('0.2%') }} />
                <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', padding: '4%', }}>
                    <Animated.FlatList
                        backgroundColor='#ffffff'
                        horizontal
                        data={HeaderData}
                        keyExtractor={(item, index) => item.id}
                        renderItem={({ item, index }) =>
                            <View style={{ borderLeftWidth: 1, borderColor: 'rgb(232,232,232)' }}>
                                <View style={{ marginHorizontal: wp('2%') }}>
                                    <Text style={styles.titleTxt}>{item.title}</Text>
                                    <Text style={styles.valueTxt}>{item.value}</Text>
                                </View>
                            </View>}
                        contentContainerStyle={{ paddingLeft: '2%' }}
                    />
                    <ModalDropdown
                        options={['PENDING', 'IN-PROGRESS', 'FINISHED']}
                        textStyle={{ fontSize: hp('1.5%'), color: 'white' }}
                        dropdownStyle={{ width: '25%', height: hp('15%'), marginRight: -12, elevation: 8, marginTop: -30 }}
                        dropdownTextStyle={{ fontSize: hp('1.5%'), color: 'rgb(143, 143, 143)' }}
                        onSelect={(index, value) => handleMenuClick(value)}
                        defaultValue={status}
                        dropdownTextHighlightStyle={{ backgroundColor: 'rgb(236, 243, 254)' }}
                        showsVerticalScrollIndicator={true}
                        style={{ backgroundColor: statusColor[status], justifyContent: 'center', alignItems: 'center', width: wp('25%'), borderColor: 'rgb(143, 143, 143)', borderRadius: 4 }}
                        renderRightComponent={() => <Icon color={'white'} size={20} type='feather' name={IconPosition ? 'chevron-up' : 'chevron-down'} />}
                        defaultTextStyle={{ color: 'white' }}
                        onDropdownWillShow={() => setIconPosition(true)}
                        onDropdownWillHide={() => setIconPosition(false)}
                    />
                </View>
            </Card >
        );
    }

    return (
        <View>
            <HeaderPart/>
            <View style={{ borderWidth: 0, backgroundColor: '#3d87f1', height: hp('16%'), display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                <FlatList
                    data={EventTabs}
                    keyExtractor={keyExtractor}
                    showsHorizontalScrollIndicator={true}
                    onScroll={Animated.event(
                        [{ nativeEvent: { contentOffset: { x: scrollX } } }],
                        {
                            useNativeDriver: false,
                        }
                    )}
                    style={styles.flatList}
                    // pagingEnabled
                    horizontal
                    decelerationRate={'normal'}
                    scrollEventThrottle={16}
                    renderItem={({ item, index }) =>
                        <TouchableOpacity
                            onPress={() => onPressBtn(item, index)}
                            style={{ backgroundColor: activeIndex == index ? 'white' : '#8ab7f6', borderRadius: 4, marginRight: wp('2%'), width: wp('28.3%'), alignSelf: 'center', justifyContent: 'center', height: hp('5%') }}
                        >
                            <Text
                                style={{ color: activeIndex == index ? '#3d87f1' : 'white', textAlign: 'center', fontFamily: 'SourceSansPro-Regular', fontSize: hp('1.8%') }}
                            >
                                {item}
                            </Text>
                        </TouchableOpacity>
                    }
                    contentContainerStyle={{ paddingLeft: '3%', paddingRight: '3%', }}
                />
                
            </View>
            <View style={{ borderWidth: 0, height: hp('68%'), flexDirection: 'row', display: 'flex', justifyContent: 'space-between' }}>
                <View style={{ alignSelf: 'center' }}>
                <TouchableOpacity
                        onPress={() => onPressSliderBtn()}
                        style={{width: wp('5%'), height: hp('10%'), borderTopRightRadius: 4, borderBottomRightRadius: 4, alignItems: 'center', justifyContent: 'center', backgroundColor: '#3d87f1'}}
                    >
                        <Icon color={'#fff'} size={14} type='ionicon' name={open?'caret-back-outline':'caret-forward'} />
                    </TouchableOpacity>
                </View>
                {tabScreenComponents[activeIndex]}
            </View>
        </View>
    );
};

export default EventScreen1;

const styles = StyleSheet.create({

    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        display: 'flex',
        flex: 1,
        backgroundColor: "white",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    ganttTxt: {
        fontFamily: 'SourceSansPro-Regular',
        fontSize: hp('3%'),
        color: 'rgb(68, 67, 67)'
    },

    labelStyle: {
        color: 'white',
        fontSize: hp('1.5%'),
        fontFamily: 'SourceSansPro-Regular',
    },

    titleTxt: {
        color: 'rgb(143,143,143)',
        fontSize: hp('1.5%'),
        fontFamily: 'SourceSansPro-Regular',
    },

    valueTxt: {
        color: 'rgb(68, 67, 67)',
        fontSize: hp('1.8%'),
        fontFamily: 'SourceSansPro-Regular',
    },

});
