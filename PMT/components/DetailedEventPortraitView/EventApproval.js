import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    useWindowDimensions,
    Animated
} from 'react-native';
import React, { useState, useEffect, useMemo } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { useDispatch, useSelector } from 'react-redux';
import * as getStageEventActionCreators from '../../actions/gantt-stage-action/index';
import EmptyLoader from '../../components/EmptyLoader'
import notifyMessage from '../../ToastMessages'
import { Avatar, Card, Button, List, Divider, IconButton } from 'react-native-paper';
import color from 'color';
import { FlatList } from 'react-native';
import {
    useCollapsibleSubHeader,
    CollapsibleSubHeaderAnimator,
} from 'react-navigation-collapsible';
import { Icon } from 'react-native-elements';
import config from '../../config';
import * as actionCreators from "../../actions/gantt-stage-action/index";
import DialogComponent from './DialogComponent';
import AsyncStorage from '@react-native-async-storage/async-storage';


const Approval = (props) => {

    const data = props.data
    const { token, event_id, navigation, changeReload, customer_approval_required,
        is_sub_contractor_event, approval_status, is_requested, sub_contractor_approver,
        event_approval_date, can_work, can_approve, isSubContractor, approval
    } = props.eventDetails

    const [visibleSubContractorApproval, setVisibleSubContractorApproval] = useState(false);
    const [visibleMyApproval, setVisibleMyApproval] = useState(false)
    const [visibleCustomerApproval, setVisibleCustomerApproval] = useState(false)

    const dispatch = useDispatch();

    const onRequestApproval = () => {
        const formData = new FormData();
        formData.append("event_id", event_id);
        dispatch(actionCreators.requestApproval(formData, token));
        changeReload()
    };

    const onApproval = () => {
        const formData = new FormData();
        formData.append("event_id", event_id);
        dispatch(actionCreators.postApproval(formData, token, isSubContractor));
        setVisibleSubContractorApproval(false)
        setVisibleMyApproval(false)
        setVisibleCustomerApproval(false)
        changeReload()
    };

    const onCancelPopover = () => {
        setVisibleSubContractorApproval(false)
        setVisibleMyApproval(false)
        setVisibleCustomerApproval(false)
    };

    const onClickPopover = (approvalType) => {
        if (approvalType === "sub_contractor_approval") {
            setVisibleSubContractorApproval(true);
        } else if (approvalType === "my_approval") {
            setVisibleMyApproval(true)
        } else if (approvalType === "customer_approval") {
            setVisibleCustomerApproval(true)
        }
    };

    const customerApprovalRedquiredHandler = () => { };

    const [isConsumer, setisConsumer] = useState('');

    useEffect(
        async () => {
            try {
                const userDetails = await AsyncStorage.getItem('userDetails');
                const value = JSON.parse(userDetails);
                setisConsumer(value['user_type']['is_consumer'])
            } catch (e) {
                //(e)
            }
        }, []
    )


    return (
        <View style={{ width: wp('95%'), paddingLeft: '5%', marginTop: hp('2%') }}>

            <View opacity={is_sub_contractor_event ? 1 : 0.5}>
                <Text style={styles.valueTxt}>Sub-Contractor Level Approval</Text>

                {
                    approval_status === true ?
                        (<Text style={styles.labelStyle}>Approved by :{" "} {sub_contractor_approver != '' ? sub_contractor_approver : 'NA'}</Text>)
                        :
                        is_requested === true ?
                            <Text style={styles.labelStyle}>Requested for approval:{" "} {sub_contractor_approver != '' ? sub_contractor_approver : 'NA'}</Text>
                            :
                            <Text style={styles.labelStyle}>To be approved by:{" "} {sub_contractor_approver != '' ? sub_contractor_approver : 'NA'}</Text>
                }

                {
                    approval_status === true && <Text style={styles.labelStyle}>Approved on :{" "} {event_approval_date != '' ? event_approval_date : 'NA'}</Text>
                }

                <DialogComponent
                    visible={visibleSubContractorApproval}
                    onApproval={onApproval}
                    onCancelPopover={onCancelPopover}
                />

                {
                    can_approve && (
                        <TouchableOpacity
                            onPress={() => onClickPopover("sub_contractor_approval")}
                            disabled={approval_status || isSubContractor === false ? true : false}
                            style={styles.touchableBtnTxt}
                        >
                            <Text style={[styles.btnTxt, { alignSelf: 'center' }]}>{approval_status === false ? "Approve" : "Approved"}</Text>
                        </TouchableOpacity>
                    )
                }

                {
                    can_work && !can_approve && (
                        <TouchableOpacity
                            onPress={() => onRequestApproval()}
                            disabled={is_requested || isSubContractor === false ? true : false}
                            style={styles.touchableBtnTxt}
                        >
                            <Text style={[styles.btnTxt, { alignSelf: 'center' }]}>{is_requested === false ? "Request" : "Requested"}</Text>
                        </TouchableOpacity>
                    )
                }
            </View>

            <Divider style={{marginVertical: hp('1%') ,backgroundColor: 'rgba(179, 179, 179, 0.3)' }} />

            <View style={{ }}>
                <Text style={styles.valueTxt}>My Approval</Text>
                {
                    approval_status === true ?
                        (<Text style={styles.labelStyle}>Approved by :{" "} {approval != '' ? approval : 'NA'}</Text>)
                        :
                        is_requested === true ?
                            <Text style={styles.labelStyle}>Requested for approval:{" "} {approval != '' ? approval : 'NA'}</Text>
                            :
                            <Text style={styles.labelStyle}>To be approved by:{" "} {approval != '' ? approval : 'NA'}</Text>
                }

                {
                    approval_status === true && <Text style={styles.labelStyle}>Approved on :{" "} {event_approval_date != '' ? event_approval_date : 'NA'}</Text>
                }

                <DialogComponent
                    visible={visibleMyApproval}
                    onApproval={onApproval}
                    onCancelPopover={onCancelPopover}
                />

                {
                    can_approve && (
                        <TouchableOpacity
                            onPress={() => onClickPopover("my_approval")}
                            disabled={approval_status}
                            style={styles.touchableBtnTxt}
                        >
                            <Text style={[styles.btnTxt, { alignSelf: 'center' }]}>{approval_status === false ? "Approve" : "Approved"}</Text>
                        </TouchableOpacity>
                    )
                }

                {
                    can_work && !can_approve && (
                        <TouchableOpacity
                            onPress={() => onRequestApproval()}
                            disabled={is_requested}
                            style={styles.touchableBtnTxt}
                        >
                            <Text style={[styles.btnTxt, { alignSelf: 'center' }]}>{is_requested === false ? "Request" : "Requested"}</Text>
                        </TouchableOpacity>
                    )
                }
            </View>

            <Divider style={{marginVertical: hp('2%') ,backgroundColor: 'rgba(179, 179, 179, 0.3)' }} />

            <View style={{}}>
                {
                    can_approve === true && (
                        <View style={{ display: 'flex', flexDirection: 'row', }}>
                            <Icon
                                color={'#3d87f1'}
                                type='feather'
                                name={customer_approval_required ? 'check-square' : 'square'}
                                onPress={() => { customerApprovalRedquiredHandler() }}
                            />
                            <Text style={[styles.valueTxt, { paddingLeft: '5%' }]}>Customer Approval Required</Text>
                        </View>
                    )
                }
            </View>

            <Divider style={{marginVertical: hp('1%') ,backgroundColor: 'rgba(179, 179, 179, 0.3)' }} />

            <View opacity={isConsumer ? 1 : 0.5}>
                <Text style={styles.valueTxt}>Customer Approval</Text>
                {
                    approval_status === true ?
                        (<Text style={styles.labelStyle}>Approved by :{" "} {approval != '' ? approval : 'NA'}</Text>)
                        :
                        is_requested === true ?
                            <Text style={styles.labelStyle}>Requested for approval:{" "} {approval != '' ? approval : 'NA'}</Text>
                            :
                            <Text style={styles.labelStyle}>To be approved by:{" "} {approval != '' ? approval : 'NA'}</Text>
                }

                {
                    approval_status === true && <Text style={styles.labelStyle}>Approved on :{" "} {event_approval_date != '' ? event_approval_date : 'NA'}</Text>
                }

                <DialogComponent
                    visible={visibleCustomerApproval}
                    onApproval={onApproval}
                    onCancelPopover={onCancelPopover}
                />

                {
                    can_approve && (
                        <TouchableOpacity
                            onPress={() => onClickPopover("customer_approval")}
                            disabled={!isConsumer}
                            style={styles.touchableBtnTxt}
                        >
                            <Text style={[styles.btnTxt, { alignSelf: 'center' }]}>{approval_status === false ? "Approve" : "Approved"}</Text>
                        </TouchableOpacity>
                    )
                }
            </View>

        </View >
    );
};

export default Approval;

const styles = StyleSheet.create({

    valueTxt: {
        color: 'rgb(68, 67, 67)',
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-Regular',
    },

    labelStyle: {
        color: 'rgb(179, 179, 179)',
        fontSize: hp('1.8%'),
        paddingTop: '1%',
        fontFamily: 'SourceSansPro-Regular',
        textTransform: 'uppercase'
    },

    touchableBtnTxt: {
        backgroundColor: 'white',
        borderColor: '#3d87f1',
        borderRadius: 5,
        justifyContent: 'center',
        borderWidth: 1.5,
        marginTop: '2%',
        width: wp('38.1%'),
        height: hp('5.3%'),
        justifyContent: 'center'
    },

    btnTxt: {
        color: '#3d87f1',
        fontSize: hp('1.9%'),
        fontFamily: 'SourceSansPro-Regular',
        textTransform: 'capitalize'

    },

    CircleShapeView: {
        width: 16,
        height: 16,
        borderRadius: 8,
        backgroundColor: '#3d87f1',
        marginBottom: hp('2%'),
    },
});


// {
//     can_approve === true && (
//         <View style={{ display: 'flex', flexDirection: 'row', paddingTop: '10%' }}>
//             <Icon
//                 color={'#3d87f1'}
//                 type='feather'
//                 name={customer_approval_required ? 'check-square' : 'square'}
//                 onPress={() => { customerApprovalRedquiredHandler() }}
//             />
//             <Text style={[styles.valueTxt, { paddingLeft: '5%' }]}>Customer Approval Required</Text>
//         </View>
//     )
// }









