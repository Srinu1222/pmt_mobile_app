import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    useWindowDimensions,
    Animated
} from 'react-native';
import React, { useState, useEffect, useMemo } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { useDispatch, useSelector } from 'react-redux';
import * as getStageEventActionCreators from '../../actions/gantt-stage-action/index';
import EmptyLoader from '../../components/EmptyLoader'
import notifyMessage from '../../ToastMessages'
import { Avatar, Card, Button, List, Divider, IconButton } from 'react-native-paper';
import color from 'color';
import { FlatList } from 'react-native';
import {
    useCollapsibleSubHeader,
    CollapsibleSubHeaderAnimator,
} from 'react-navigation-collapsible';
import { FAB, Icon, Input, BottomSheet } from 'react-native-elements';
import ViewSpecHelper from './ViewSpecHelper'
import EditSpecHelper from './EditSpecHelper'
import * as actionCreators from "../../actions/gantt-stage-action/index";
import { primaryHeadingDict, SpecificationList, addFieldInitialObjects, fontTypes, singleObjectCheckLists } from './constants'
import * as ganttActionCreators from "../../actions/gantt-stage-action/index";
import config from '../../config'
import Toast from "react-native-toast-message";
import * as commentActionCreators from "../../actions/comments-action/index";
import onPressUploadDocument from './FileUpload';
import DialogComponent from './DialogComponent';

const Checklist = (props) => {

    const {
        onScroll /* Event handler */,
        containerPaddingTop /* number */,
        scrollIndicatorInsetTop /* number */,
        translateY,
    } = useCollapsibleSubHeader();

    const dispatch = useDispatch();
    const data = props.data
    const { event_id, event_name, isSubContractor, is_sub_contractor_event, token, changeReload, activeIndex, projectId, inventory } = props.eventDetails

    // SPEC VARIABLES
    const [isViewSpec, setisViewSpec] = useState(false);
    const [isEditSpec, setisEditSpec] = useState(false);
    const [selectedChecklistTitle, setselectedChecklistTitle] = useState('');
    const [selectedChecklistId, setselectedChecklistId] = useState(null);

    useEffect(() => {
        if (projectId != '') {
            dispatch(ganttActionCreators.getSpoc(token, { project_id: projectId }));
        }
        // dispatch(ganttActionCreators.getDrawings());
    }, [projectId]);

    const spocReducer = useSelector((state) => {
        return state.spocReducers.spoc.get;
    });

    const onPressSpec = (checkListId, checklistName, index) => {
        if (index == 0) {
            setisViewSpec(true)
            setisEditSpec(false)
        } else if (index == 1) {
            setisViewSpec(false)
            setisEditSpec(true)
        }
        setselectedChecklistTitle(checklistName)
        setselectedChecklistId(checkListId)

        if (checklistName.toString() === ("List of All Drawings from Checklist" || "SPOC for each drawing" || "Procurement Plan") && event_id && checkListId) {
            dispatch(actionCreators.viewDrawingSpecifications(event_id, checkListId, is_sub_contractor_event, token));
        } else if (event_id && checkListId) {
            dispatch(actionCreators.viewSpecifications(event_id, checkListId, is_sub_contractor_event, token));
        }
    }

    const viewSpecificationReducer = useSelector((state) => {
        return state.viewSpecificationReducers.viewspecifications.get;
    });

    var [specReducerState, setspecReducerState] = useState([]);

    useEffect(() => {
        if (viewSpecificationReducer.success.ok) {
            let data = viewSpecificationReducer.success.data.specifications
            if (data.length == 0) {
                if (singleObjectCheckLists.includes(selectedChecklistTitle) || selectedChecklistTitle.toString().includes("OEM")) {
                    if (isEditSpec) {
                        if (selectedChecklistTitle.toString().includes("OEM")) {
                            let temp = [{ date: '', percentage: '', comment: '' }]
                            setspecReducerState(temp)
                        } else {
                            let temp = [addFieldInitialObjects[selectedChecklistTitle]]
                            setspecReducerState(temp)
                        }
                    } else {
                        setspecReducerState([])
                    }
                }
            } else {
                setspecReducerState(data)
            }
        }
    }, [viewSpecificationReducer.success.ok]);

    // COMMENTS CODE

    const [selectedCommentCardId, setSelectedCommentCardId] = useState('');
    const [selectedCommentCardType, setSelectedCommentCardType] = useState('');
    const [deletedCommentId, setdeletedCommentId] = useState('');

    const onPressComments = (preRequisiteId, commentBelongsTo) => {
        if (selectedCommentCardId == '') {
            setSelectedCommentCardId(preRequisiteId)
            setSelectedCommentCardType(commentBelongsTo)
            dispatch(commentActionCreators.getComments(event_id, preRequisiteId, commentBelongsTo, token, isSubContractor));
        } else {
            setSelectedCommentCardId('')
            changeReload()
        }
    }

    const onPostCommentHandler = (comment, selectedFile) => {

        if (comment) {
            const formData = new FormData();

            formData.append("event_id", event_id);
            formData.append("comment_belongs_to", selectedCommentCardType);
            formData.append("data", comment);
            if (selectedCommentCardType === "pre_requisites") {
                formData.append("pre_requisite_id", selectedCommentCardId);
            } else {
                formData.append("checklist_id", selectedCommentCardId);
            }
            if (Object.keys(selectedFile).length != 0) {
                formData.append("file", selectedFile);
            }
            dispatch(commentActionCreators.postComments(formData, token, isSubContractor));
        }
    };

    const deleteComment = (commentId) => {
        setdeletedCommentId(commentId)
        const formData = new FormData();

        formData.append("event_id", event_id);
        formData.append("comment_id", commentId);

        if (selectedCommentCardType === "pre_requisites") {
            formData.append("pre_requisite_id", selectedCommentCardId);
        } else if (selectedCommentCardType === "checklist") {
            formData.append("checklist_id", selectedCommentCardId);
        }
        dispatch(commentActionCreators.deleteComment(formData, token, isSubContractor));
    };

    const onPressUpdateTaskStatus = (taskId, type, status, spoc) => {
        if (status == false) {
            const formData = new FormData();
            formData.append('event_id', event_id);
            formData.append('status', !status);
            formData.append('spoc', spoc.id);
            formData.append('is_sub_contractor', isSubContractor);
            if (type == 'pre_requisites') {
                formData.append('pre_requisite_id', taskId);
                dispatch(actionCreators.preRequisiteTask(formData, token));
            } else {
                formData.append('checklist_id', taskId);
                dispatch(actionCreators.checkListTask(formData, token));
            }
        }
        changeReload()
    };

    const followUpHandler = (preRequisiteId) => {
        const formData = new FormData();
        formData.append('event_id', event_id);
        formData.append('pre_requisite_id', preRequisiteId);
        dispatch(actionCreators.followUp(formData, token, isSubContractor));
    };

    const postCommentReducer = useSelector((state) => {
        return state.commentReducers.comments.post;
    });

    const getCommentReducer = useSelector((state) => {
        return state.commentReducers.comments.get;
    });

    useEffect(() => {
        if (event_id && selectedCommentCardId && selectedCommentCardType) {
            dispatch(commentActionCreators.getComments(event_id, selectedCommentCardId, selectedCommentCardType, token, isSubContractor));
        }
    }, [postCommentReducer.success.ok, deletedCommentId]);

    const Item = (item) => {
        const buttons = ['View Spec', 'Edit Spec']

        // COMMENTS CODE
        const [addComment, setAddComment] = useState('');
        const [selectedFontType, setSelectedFontType] = useState({});

        const [selectedFile, setSelectedFile] = useState({});
        const [fileName, setFileName] = useState('');

        const onPressAttachment = () => {
            onPressUploadDocument(onSaveFileDetails)
        }
        const onSaveFileDetails = (file_url, name) => {
            if (file_url != '') {
                setSelectedFile(file_url)
            } else {
                setFileName(name)
            }
        }

        // Task DIALOG
        const [visibleStatusDialog, setvisibleStatusDialog] = useState(false);
        const onApproval = () => {
            setvisibleStatusDialog(false)
            onPressUpdateTaskStatus(item.id, activeIndex == 0 ? "pre_requisites" : "checklist", item.status, item.spoc.id)
        }
        const onCancelPopover = () => {
            setvisibleStatusDialog(false)
        }

        return (
            <Card style={styles.item_card}>
                <DialogComponent
                    visible={visibleStatusDialog}
                    onApproval={onApproval}
                    onCancelPopover={onCancelPopover}
                />
                <View style={{ flexDirection: 'row', }}>
                    <View style={{ borderWidth: 0 }}>
                        <Icon
                            color={'#3d87f1'}
                            onPress={() => {
                                setvisibleStatusDialog(true)
                            }}
                            type={item.status ? 'ionicon' : 'feather'}
                            name={item.status ? 'checkbox' : 'square'}
                        />
                    </View>
                    <View style={{ borderWidth: 0, paddingLeft: '5%' }}>
                        <Text style={styles.titleTxt}>{item.title}</Text>

                        <TouchableOpacity
                            onPress={() => onPressComments(item.id, activeIndex == 0 ? "pre_requisites" : "checklist")}
                        >
                            <View style={{ flexDirection: 'row', alignItems: 'center', borderWidth: 0, marginTop: '2%' }}>
                                <Text style={styles.CommentsTxt}>Comments</Text>
                                {
                                    selectedCommentCardId == '' &&
                                    <Text style={styles.commentCircle}>{item.comments}</Text>
                                }
                            </View>
                        </TouchableOpacity>

                        {
                            getCommentReducer.success.ok && selectedCommentCardId == item.id && selectedCommentCardType == (activeIndex == 0 ? "pre_requisites" : "checklist") &&
                            <Animated.FlatList
                                backgroundColor='#ffffff'
                                data={getCommentReducer.success?.data?.comments}
                                ListEmptyComponent={
                                    <Text style={[styles.titleTxt, { color: 'rgb(179, 179, 179)' }]}>No Comments Added</Text>
                                }
                                showsVerticalScrollIndicator
                                keyExtractor={(item, index) => index}
                                renderItem={({ item, index }) => {
                                    return (
                                        <View>
                                            <View style={{ justifyContent: 'space-between', flexDirection: 'row', width: wp('65%'), alignItems: 'center' }}>
                                                <View style={{ paddingVertical: hp('1%') }}>
                                                    <Text style={styles.commentName}>{item.name}</Text>
                                                    <Text style={[styles.CommentsTxt, { width: wp('60%') }]}>{item.data}</Text>
                                                </View>
                                                <View style={{ borderWidth: 0, alignSelf: 'center', padding: '1%', marginHorizontal: hp('1%') }}>
                                                    <Icon
                                                        color={'rgb(143, 143, 143)'}
                                                        type='feather'
                                                        name='trash'
                                                        size={18}
                                                        onPress={() => { deleteComment(item.id) }}
                                                    />
                                                </View>
                                            </View>
                                            <Divider style={{ backgroundColor: 'rgba(179, 179, 179, 0.3)' }} />
                                        </View>
                                    );
                                }
                                }
                                onScroll={onScroll}
                                contentContainerStyle={{ paddingTop: containerPaddingTop, paddingLeft: hp('2%'), paddingRight: hp('3%'), paddingBottom: hp('0%') }}
                                scrollIndicatorInsets={{ top: scrollIndicatorInsetTop }}
                            />
                        }

                        {
                            selectedCommentCardId == item.id && selectedCommentCardType == (activeIndex == 0 ? "pre_requisites" : "checklist") &&
                            <>
                                <View style={{ marginVertical: hp('2%'), borderWidth: 1, borderColor: 'rgb(143, 143, 143)', borderRadius: 4, paddingBottom: hp('1%') }}>
                                    <Input
                                        placeholder={'Type Something...'}
                                        onChangeText={text => setAddComment(text)}
                                        multiline={true}
                                        inputStyle={{ fontSize: hp('2%'), ...selectedFontType, color: 'rgb(68, 67, 67)' }}
                                        value={addComment}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        containerStyle={{ width: wp('70%') }}
                                    />
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingLeft: wp('2%') }}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                                            {
                                                fontTypes.map((i, index) => {
                                                    return (
                                                        <View
                                                            key={index}
                                                            style={{ borderWidth: 0, alignSelf: 'center', padding: '1%', marginHorizontal: hp('1%') }}
                                                        >
                                                            <Icon
                                                                color={'rgb(68, 67, 67)'}
                                                                type='feather'
                                                                size={15}
                                                                name={i.name}
                                                                onPress={() => {
                                                                    if (i.name == 'paperclip') {
                                                                        onPressAttachment()
                                                                    }
                                                                    else {
                                                                        setSelectedFontType(i['style'])
                                                                    }
                                                                }}
                                                            />
                                                        </View>
                                                    );
                                                })
                                            }
                                        </View>
                                        <View style={{ paddingRight: '5%', alignSelf: 'center', borderWidth: 0 }}>
                                            <Icon
                                                color={'#3d87f1'}
                                                onPress={() => {
                                                    onPostCommentHandler(addComment, selectedFile)
                                                    setAddComment('')
                                                    setSelectedFile({})
                                                }}
                                                type='ionicon'
                                                name='send-outline'
                                            />
                                        </View>

                                    </View>
                                </View>
                            </>
                        }

                        <View style={{ flexDirection: 'row', marginVertical: hp('1%') }}>
                            <Text style={styles.dueDateTxt}>DUE DATE</Text>
                            <Text style={[styles.dueDateTxt, { paddingLeft: '2%' }]}>{item.due_on}</Text>
                        </View>
                    </View>
                </View>

                <Divider style={{ backgroundColor: 'rgb(232,232,232)', marginVertical: hp('2%'), height: hp('0.2%') }} />

                <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={[styles.titleTxt, { fontSize: hp('1.8%') }]}>SPOC</Text>
                    <Text style={styles.CommentsTxt}>{item.spoc.name}</Text>
                </View>

                <Divider style={{ backgroundColor: 'rgb(232,232,232)', marginVertical: hp('2%'), height: hp('0.2%') }} />

                <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={[styles.titleTxt, { fontSize: hp('1.8%') }]}>Sub-Contractor</Text>
                    <Text style={styles.CommentsTxt}>{item.spoc.name}</Text>
                </View>

                <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                    {
                        activeIndex == 0 &&
                        <TouchableOpacity
                            style={styles.btnStyle}
                            onPress={() => { followUpHandler(item.id) }}
                        >
                            <Text style={{ fontSize: hp('1.5%'), color: '#3d87f1', textAlign: 'center' }}>Follow Up</Text>
                        </TouchableOpacity>
                    }

                    {
                        buttons.map(
                            (title, index) => {
                                return (
                                    (SpecificationList.includes(item.title) || item.title.toString().includes('OEM')) &&
                                    <TouchableOpacity
                                        key={index}
                                        style={styles.btnStyle}
                                        onPress={() => onPressSpec(item.id, item.title, index)}
                                    >
                                        <Text style={{ fontSize: hp('1.5%'), color: '#3d87f1', textAlign: 'center' }}>{title}</Text>
                                    </TouchableOpacity>
                                );
                            }

                        )
                    }
                </View>
            </Card>
        );
    }

    const onPressSubmit = (params) => {
        setisEditSpec(false)
        const formData = new FormData();
        formData.append("event_id", event_id);
        formData.append("checklist_id", selectedChecklistId);
        formData.append("is_mobile", true);
        switch (selectedChecklistTitle) {
            case 'SPOC for each drawing':
            case 'List of All Drawings from Checklist':
                formData.append("list_of_drawings", JSON.stringify(specReducerState));
                dispatch(actionCreators.editTaskListOfDrawings(formData, token));
                break;
            case 'Type of Cable':
                formData.append("cable_calculations", JSON.stringify(specReducerState));
                dispatch(actionCreators.editTaskListOfCable(formData, token));
                break;
            case 'Specifications of Revisions that are needed – Required: A way to specify the drawings that need revisions and specifying the revisions needed' || 'Recommended Changes if Any':
                formData.append("internal_review_design_change", JSON.stringify(specReducerState));
                dispatch(actionCreators.editTaskListOfInternalApproval(formData, token));
                break;
            case 'Procurement Plan':
                formData.append("procurement_plan", JSON.stringify(specReducerState));
                dispatch(actionCreators.editTaskListOfProcurementPlan(formData, token));
                break;
            case 'Procurement Specifications':
                var { stock_quantity, target_price, target_delivery_date, preferred_brands, specifications } = specReducerState[0]
                formData.append("target_price", target_price);
                formData.append("preferred_brand", preferred_brands);
                formData.append("stock_quantity", stock_quantity);
                formData.append("delivery_date", target_delivery_date);
                formData.append("specifications", specifications);
                dispatch(actionCreators.editTaskListOfProcurementSpecification(formData, token));
                break;
            case 'Final Specifications':
                var { final_price, final_brands, stock_quantity, target_delivery_date, specifications, po_file } = specReducerState[0]

                if (final_price && final_brands && stock_quantity && target_delivery_date && specifications) {
                    formData.append("final_price", final_price);
                    formData.append("stock_quantity", stock_quantity);
                    formData.append("final_brand", final_brands);
                    formData.append("delivery_date", target_delivery_date);
                    formData.append("specifications", specifications);

                    dispatch(actionCreators.editTaskListOfFinalSpecification(formData, token));
                } else {
                    Toast.show({
                        text1: 'Please fill all the fields',
                        type: 'error',
                        autoHide: true,
                        position: 'bottom',
                        visibilityTime: 1000,
                        topOffset: 40,
                    })
                }
                break;
            case 'Inspection Agency Details: To Enter Name, Contact person, Number, Email':
                var { component_name, pdi_agency_name, pdi_date, contact_person_name, contact_person_email, contact_phone_number } = specReducerState[0]
                formData.append("component_name", component_name);
                formData.append("pdi_agency_name", pdi_agency_name);
                formData.append("pdi_date", pdi_date);
                formData.append("contact_person_name", contact_person_name);
                formData.append("contact_person_email", contact_person_email);
                formData.append("contact_phone_number", contact_phone_number);
                dispatch(actionCreators.editPreDispatchInspection(formData, token));
                break;
            default:
                var { date, percentage, comment } = specReducerState[0]
                formData.append("date", date);
                formData.append("percentage", percentage);
                formData.append("comment", comment);
                dispatch(actionCreators.editTaskListOfOEMGuideLines(formData, token));
                break;
        }
    }

    return (
        <View style={{ width: wp('95%'), marginTop: hp('2%') }}>
            <BottomSheet
                isVisible={isViewSpec}
                containerStyle={{ backgroundColor: 'rgba(0.5, 0.25, 0, 0.2)' }}
            >
                {
                    viewSpecificationReducer.success.ok &&
                    <View style={{ backgroundColor: 'white', height: Dimensions.get('window').height * 0.7 }}>
                        <TouchableOpacity
                            onPress={() => setisViewSpec(false)}
                        >
                            <Divider style={{ alignSelf: 'center', marginVertical: hp('2%'), backgroundColor: 'rgb(143,143,143)', width: wp('18.6%'), height: hp('0.5%') }} />
                        </TouchableOpacity>

                        <View style={{ paddingRight: wp('10%'), paddingLeft: wp('5%'), }}>
                            <Text style={[styles.titleTxt, { fontSize: hp('3%'), marginVertical: hp('1%') }]}>
                                {
                                    selectedChecklistTitle.includes('OEM')?inventory:primaryHeadingDict[selectedChecklistTitle]
                                }
                            </Text>
                            <Divider style={{ backgroundColor: 'rgb(232,232,232)', height: hp('0.3%') }} />
                        </View>

                        <View style={{ marginVertical: hp('2%'), marginHorizontal: wp('3%'), height: Dimensions.get('window').height * 0.41 }}>
                            <Animated.FlatList
                                backgroundColor='#ffffff'
                                data={specReducerState}
                                ListEmptyComponent={
                                    <View style={{ justifyContent: 'center', height: hp('40%'), alignItems: 'center' }}>
                                        <Text
                                            style={{ fontSize: hp('3%'), color: 'rgb(179, 179, 179)' }}
                                        >No data found</Text>
                                    </View>
                                }
                                showsVerticalScrollIndicator
                                keyExtractor={(item, index) => index}
                                renderItem={({ item, index }) => <ViewSpecHelper {...item} selectedChecklistTitle={selectedChecklistTitle} />}
                                onScroll={onScroll}
                                contentContainerStyle={{ paddingTop: containerPaddingTop, paddingHorizontal: wp('3%'), paddingBottom: hp('2%') }}
                                scrollIndicatorInsets={{ top: scrollIndicatorInsetTop }}
                            />
                        </View>

                        <TouchableOpacity
                            style={{ width: wp('78.3%'), marginHorizontal: hp('10%'), justifyContent: 'center', borderRadius: 4, alignSelf: 'center', height: hp('7%'), borderWidth: 2, borderColor: '#3d87f1', backgroundColor: 'white' }}
                            onPress={() => onPressSpec(selectedChecklistId, selectedChecklistTitle, 1)}
                        >
                            <Text style={{ color: '#3d87f1', alignSelf: 'center', textTransform: 'capitalize', fontSize: hp('2.5%') }}>Edit spec</Text>
                        </TouchableOpacity>

                    </View>
                }
            </BottomSheet>

            <BottomSheet
                isVisible={isEditSpec}
                containerStyle={{ backgroundColor: 'rgba(0.5, 0.25, 0, 0.2)' }}
            >
                {
                    (viewSpecificationReducer.success.ok && spocReducer.success.ok) &&
                    <View style={{ backgroundColor: 'white', height: Dimensions.get('window').height * 0.7 }}>
                        <TouchableOpacity
                            onPress={() => setisEditSpec(false)}
                        >
                            <Divider style={{ alignSelf: 'center', marginVertical: hp('2%'), backgroundColor: 'rgb(143,143,143)', width: wp('18.6%'), height: hp('0.5%') }} />
                        </TouchableOpacity>

                        <View style={{ paddingRight: wp('10%'), paddingLeft: wp('5%'), }}>
                            <Text style={[styles.titleTxt, { fontSize: hp('3%'), marginVertical: hp('1%') }]}>
                                {
                                    selectedChecklistTitle.includes('OEM')?inventory:primaryHeadingDict[selectedChecklistTitle]
                                }
                            </Text>
                            <Divider style={{ backgroundColor: 'rgb(232,232,232)', height: hp('0.3%') }} />
                        </View>

                        <Animated.FlatList
                            backgroundColor='#ffffff'
                            data={specReducerState}
                            ListEmptyComponent={
                                <View style={{ justifyContent: 'center', height: hp('40%'), alignItems: 'center' }}>
                                    <Text
                                        style={{ fontSize: hp('3%'), color: 'rgb(179, 179, 179)' }}
                                    >No data found</Text>
                                </View>
                            }
                            showsVerticalScrollIndicator
                            keyExtractor={(item, index) => index}
                            renderItem={({ item, index }) => <EditSpecHelper
                                {...item}
                                dictPosition={index}
                                specReducerState={specReducerState}
                                setspecReducerState={setspecReducerState}
                                spoc_list={spocReducer.success.data.members}
                                selectedChecklistTitle={selectedChecklistTitle}
                                inventory={inventory}
                            />
                            }
                            onScroll={onScroll}
                            contentContainerStyle={{ paddingTop: hp('2%'), paddingRight: wp('10%'), paddingLeft: wp('5%'), paddingBottom: hp('2%') }}
                            scrollIndicatorInsets={{ top: scrollIndicatorInsetTop }}
                        />

                        {
                            !(singleObjectCheckLists.includes(selectedChecklistTitle)||selectedChecklistTitle.includes('OEM')) &&
                            <TouchableOpacity
                                style={styles.addFieldWrapper}
                                onPress={() => {
                                    let tempDict = addFieldInitialObjects[selectedChecklistTitle]
                                    setspecReducerState([
                                        ...specReducerState,
                                        tempDict
                                    ])
                                }}
                            >
                                <Text style={styles.addFieldText}>+Add field</Text>
                            </TouchableOpacity>
                        }

                        <TouchableOpacity
                            style={styles.submitFieldWrapper}
                            onPress={() => onPressSubmit()}
                        >
                            <Text style={styles.submitText}>Submit</Text>
                        </TouchableOpacity>

                    </View>
                }
            </BottomSheet>

            <Animated.FlatList
                backgroundColor='#ffffff'
                data={data}
                showsVerticalScrollIndicator
                keyExtractor={(item, index) => index}
                renderItem={({ item, index }) => <Item {...item} />}
                onScroll={onScroll}
                contentContainerStyle={{ paddingTop: containerPaddingTop, paddingLeft: hp('2%'), paddingRight: hp('2%'), paddingBottom: hp('2%') }}
                scrollIndicatorInsets={{ top: scrollIndicatorInsetTop }}
            />
        </View>
    );
};

export default Checklist;

const styles = StyleSheet.create({
    item_card: {
        marginBottom: hp('2%'),
        borderWidth: 0,
        backgroundColor: '#fff',
        shadowColor: 'rgba(0,0,0,0.4)',
        elevation: 5,
        borderRadius: 4,
        padding: '5%',
        paddingRight: '4%'
    },

    touchableBtnTxt: {
        backgroundColor: '#3d87f1',
        borderColor: '#3d87f1',
        borderRadius: 5,
        justifyContent: 'center',
        borderWidth: 1.5,
        marginTop: '2%',
        width: wp('25%'),
        height: hp('5%'),
    },

    titleTxt: {
        color: 'rgb(68, 67, 67)',
        fontSize: hp('2.2%'),
        fontFamily: 'SourceSansPro-Regular',
    },

    CommentsTxt: {
        color: 'rgb(143, 143, 143)',
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
    },

    commentName: {
        color: '#3d87f1',
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
    },

    commentCircle: {
        textAlign: 'center',
        fontSize: hp('1.5%'),
        color: 'white',
        height: 20,
        width: 20,
        backgroundColor: '#3d87f1',
        borderRadius: 100,
        padding: '1%',
        marginLeft: '2%'
    },

    btnStyle: {
        width: wp('26.3%'),
        height: hp('3.2%'),
        alignItems: 'center',
        marginTop: '6%',
        justifyContent: 'center',
        borderColor: '#3d87f1',
        borderRadius: 2,
        borderWidth: 1.2
    },

    dueDateTxt: {
        fontFamily: 'SourceSansPro-Regular',
        fontSize: hp('2%'),
        color: 'rgb(179, 179, 179)',
        borderRightWidth: 1,
        borderRightColor: 'rgb(179, 179, 179)',
        paddingRight: '2%'
    },

    addFieldWrapper: {
        width: wp('78.3%'),
        marginVertical: hp('2%'),
        justifyContent: 'center',
        borderRadius: 4,
        alignSelf: 'center',
        height: hp('6%'),
        borderWidth: 2,
        borderColor: '#3d87f1',
        backgroundColor: 'white'
    },

    addFieldText: {
        color: '#3d87f1',
        alignSelf: 'center',
        textTransform: 'capitalize',
        fontSize: hp('2.5%')
    },

    submitFieldWrapper: {
        width: wp('40%'),
        backgroundColor: '#3d87f1',
        justifyContent: 'center',
        borderRadius: 4,
        alignSelf: 'center',
        height: hp('6%'),
        borderWidth: 2,
        borderColor: '#3d87f1'
    },

    submitText: {
        color: 'white',
        alignSelf: 'center',
        textTransform: 'capitalize',
        fontSize: hp('2.5%')
    }

});
