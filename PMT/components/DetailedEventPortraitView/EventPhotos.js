import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    useWindowDimensions,
    Animated
} from 'react-native';
import React, { useState, useEffect, useMemo } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { useDispatch, useSelector } from 'react-redux';
import * as getStageEventActionCreators from '../../actions/gantt-stage-action/index';
import EmptyLoader from '../EmptyLoader'
import notifyMessage from '../../ToastMessages'
import { Avatar, Card, Button, List, Divider, IconButton } from 'react-native-paper';
import color from 'color';
import { FlatList } from 'react-native';
import {
    useCollapsibleSubHeader,
    CollapsibleSubHeaderAnimator,
} from 'react-navigation-collapsible';
import { FAB, Icon, BottomSheet, Input } from 'react-native-elements';
import config from '../../config'
import Toast from "react-native-toast-message";
import * as ganttActionCreators from "../../actions/gantt-stage-action/index";
import MyModalDropdown from './ModalDropDown';
import MyDatePicker from './DatePicker'
import ModalDropdown from 'react-native-modal-dropdown';
import TouchableScale from 'react-native-touchable-scale';
import * as actionCreators from '../../actions/project-docs-action/index';
import AddEventPhotos from './AddEventPhotos';


const Photos = (props) => {
    const {
        onScroll /* Event handler */,
        containerPaddingTop /* number */,
        scrollIndicatorInsetTop /* number */,
        translateY,
    } = useCollapsibleSubHeader();

    const data = props.data
    const { token, event_id, event_name, projectName, stage, navigation, isSubContractor, changeReload, projectId } = props.eventDetails

    const [EventPhotosData, setEventPhotosData] = useState(data);

    const [isViewPhoto, setisViewPhoto] = useState(false);
    const [isAddPhoto, setisAddPhoto] = useState(false);
    const [isClickEditPhoto, setisClickEditPhoto] = useState(false);
    const [selectedPhoto, setselectedPhoto] = useState({});
    const [selectedPhotoIndex, setselectedPhotoIndex] = useState(null);
    const [photoName, setPhotoName] = useState('');
    const [photoDescription, setPhotoDescription] = useState('')

    const handleAddPhoto = () => {
        setisAddPhoto(true)
    }

    const EmptyPhotos = (params) => {
        return (
            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                < FastImage
                    style={styles.groupimage}
                    source={require('../../assets/icons/noPhotos.png')}
                    resizeMode={FastImage.resizeMode.contain}
                />
                <Text style={[styles.titleTxt]}>No photos have been added</Text>
                <Text style={[styles.titleText]}>Please add new photos</Text>
                <TouchableOpacity
                    style={styles.buttonContainer}
                    onPress={() => handleAddPhoto()}
                >
                    <Text style={{
                        color: 'white',
                        fontSize: hp('3%'),
                        textAlign: 'center',
                        fontFamily: 'SourceSansPro-Regular',
                    }}>+Add Photos</Text>
                </TouchableOpacity>
            </View>
        );
    }

    const handleEventPhoto = (i, index) => {
        setisViewPhoto(true)
        setselectedPhoto({ ...i })
        setselectedPhotoIndex(index)
    }

    const handleEditPhoto = (item) => {
        setisClickEditPhoto(true)
    }

    const handleSave = () => {
        const form = new FormData();
        form.append("title", EventPhotosData[selectedPhotoIndex].name);
        form.append("description", EventPhotosData[selectedPhotoIndex].description);
        form.append("doc_id", selectedPhoto.doc_id);
        actionCreators.postImageTitleAndDescription(form, token)
        setisClickEditPhoto(false)
    }

    const Item = ({ i, index }) => {
        return (
            <TouchableScale activeScale={0.99} friction={10} tension={0}
                onPress={() => handleEventPhoto(i, index)}
            >
                <View style={{ width: wp('25%'), margin: '3%', borderWidth: 0, marginRight: '0%', }}>
                    <FastImage
                        style={{ width: wp('25%'), height: hp('12%'), borderRadius: 3 }}
                        source={{ uri: i.file }}
                        resizeMode={FastImage.resizeMode.cover}
                    />
                </View>
            </TouchableScale>
        );
    }

    return (
        <View style={{ width: wp('95%'), marginTop: hp('2%') }}>

            <View style={{}}>
                <View style={{ justifyContent: 'space-between', paddingHorizontal: wp('5%'), paddingTop: hp('1%'), flexDirection: 'row' }}>
                    <Text style={[styles.option_text]}>Photos({data.length})</Text>
                    <TouchableOpacity onPress={() => handleAddPhoto()}>
                        <Text style={styles.addphoto}>+Add Photos</Text>
                    </TouchableOpacity>
                </View>
                <Divider style={{ backgroundColor: 'rgba(0,0,0,0.1)', marginVertical: hp('2%'), marginHorizontal: wp('5%'), height: hp('0.2%') }} />
                <Animated.FlatList
                    backgroundColor='#ffffff'
                    data={EventPhotosData}
                    ListEmptyComponent={EmptyPhotos}
                    showsVerticalScrollIndicator
                    keyExtractor={(item, index) => index}
                    renderItem={({ item, index }) => <Item i={item} index={index} />}
                    onScroll={onScroll}
                    numColumns={3}
                    contentContainerStyle={{ paddingTop: containerPaddingTop, paddingLeft: hp('2%'), paddingRight: hp('3%'), paddingBottom: hp('0%') }}
                    scrollIndicatorInsets={{ top: scrollIndicatorInsetTop }}
                />
                <BottomSheet
                    isVisible={isViewPhoto}
                    containerStyle={{ backgroundColor: 'rgba(0.5, 0.25, 0, 0.2)' }}
                >
                    <SafeAreaView style={{ backgroundColor: '#fff', height: Dimensions.get('window').height }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', padding: '5%', }}>
                            <Icon color={'rgb(68, 67, 67)'} onPress={() => {
                                setisViewPhoto(false)
                            }} type='feather' name={'chevron-left'} />
                            {
                                isClickEditPhoto &&
                                <TouchableOpacity
                                    onPress={() => setisClickEditPhoto(false)}
                                >
                                    <Text style={styles.addphoto}>Cancel</Text>
                                </TouchableOpacity>
                            }
                        </View>
                        <ScrollView>
                            <KeyboardAvoidingView keyboardVerticalOffset={5} behavior='position'>
                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <FastImage
                                        style={{ width: wp('90%'), height: hp('40%'), borderRadius: 3 }}
                                        source={{ uri: selectedPhoto.file }}
                                        resizeMode={FastImage.resizeMode.cover}
                                    />

                                    {
                                        isClickEditPhoto ?
                                            <View>
                                                <Input
                                                    placeholder={'Image Name'}
                                                    onChangeText={text => {
                                                        let tempList = [...EventPhotosData]
                                                        tempList[selectedPhotoIndex] = { ...tempList[selectedPhotoIndex], name: text }
                                                        setEventPhotosData([...tempList])
                                                    }}
                                                    inputStyle={{ fontSize: hp('2%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                                    value={EventPhotosData[selectedPhotoIndex].name}
                                                    inputContainerStyle={{ borderBottomWidth: 0 }}
                                                    containerStyle={{ width: wp('80%'), margin: '2%', height: hp('6%'), backgroundColor: 'rgba(179, 179, 179, 0.1)', borderColor: 'rgb(143, 143, 143)', borderRadius: 4 }}
                                                />
                                                <Input
                                                    placeholder={'Description'}
                                                    onChangeText={text => {
                                                        let tempList = [...EventPhotosData]
                                                        tempList[selectedPhotoIndex] = { ...tempList[selectedPhotoIndex], description: text }
                                                        setEventPhotosData([...tempList])
                                                    }}
                                                    inputStyle={{ fontSize: hp('2%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                                    value={EventPhotosData[selectedPhotoIndex].description}
                                                    multiline={true}
                                                    inputContainerStyle={{ borderBottomWidth: 0 }}
                                                    containerStyle={{ width: wp('80%'), height: hp('20%'), margin: '2%', backgroundColor: 'rgba(179, 179, 179, 0.1)', borderColor: 'rgb(143, 143, 143)', borderRadius: 4 }}
                                                />
                                            </View>
                                            :
                                            <>
                                                <Text style={[styles.allAlbums, { alignSelf: 'flex-start', padding: '5%', }]}>{EventPhotosData[selectedPhotoIndex]?.name}</Text>
                                                <ScrollView style={{ height: hp('20%'), marginBottom: hp('2%'), width: wp('90%') }}>
                                                    <Text style={[styles.textMessage]}>{EventPhotosData[selectedPhotoIndex]?.description}</Text>
                                                </ScrollView>
                                            </>
                                    }

                                    {
                                        isClickEditPhoto ?
                                            <TouchableOpacity
                                                style={styles.buttonContainer}
                                                onPress={() => handleSave()}
                                            >
                                                <Text style={{
                                                    color: 'white',
                                                    fontSize: hp('3%'),
                                                    textAlign: 'center',
                                                    fontFamily: 'SourceSansPro-Regular',
                                                }}>Save</Text>
                                            </TouchableOpacity>
                                            :
                                            <TouchableOpacity
                                                style={styles.buttonContainer}
                                                onPress={() => handleEditPhoto(selectedPhoto)}
                                            >
                                                <Text style={{
                                                    color: 'white',
                                                    fontSize: hp('3%'),
                                                    textAlign: 'center',
                                                    fontFamily: 'SourceSansPro-Regular',
                                                }}>
                                                    <Icon color={'rgb(68, 67, 67)'} color='white' size={18} type='feather' name={'edit'} />{'  '}Edit
                                                </Text>
                                            </TouchableOpacity>
                                    }
                                </View>
                            </KeyboardAvoidingView >
                        </ScrollView>
                    </SafeAreaView >
                </BottomSheet>

                <BottomSheet
                    isVisible={isAddPhoto}
                    containerStyle={{ backgroundColor: 'rgba(0.5, 0.25, 0, 0.2)' }}
                >
                    <SafeAreaView style={{ backgroundColor: '#fff', height: Dimensions.get('window').height }}>
                        <AddEventPhotos
                            {...props.eventDetails}
                            reload={changeReload}
                            setisAddPhoto={setisAddPhoto}
                        />
                    </SafeAreaView>
                </BottomSheet>

            </View>
        </View>

    );
};

export default Photos;

const styles = StyleSheet.create({
    item_card: {
        marginBottom: hp('2%'),
        borderWidth: 0,
        // height: hp('10%'),
        backgroundColor: '#fff',
        shadowColor: 'rgba(0,0,0,0.4)',
        elevation: 2,
        borderRadius: 4,
        paddingVertical: '3%',
        paddingHorizontal: '4%'
    },

    buttonContainer: {
        marginTop: hp('4%'),
        width: wp('80%'),
        height: hp('7%'),
        backgroundColor: 'rgb(61, 135, 241)',
        borderRadius: 5,
        alignSelf: 'center',
        justifyContent: 'center',
        // marginTop: hp('20%')
    },

    allAlbums: {
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-Bold',
        color: 'rgb(68, 67, 67)',
    },

    textMessage: {
        fontSize: hp('2.2%'),
        color: 'rgb(143,143,143)',
        fontFamily: 'SourceSansPro-Regular',
        textTransform: 'capitalize',
        paddingVertical: '5%'
    },

    buttonContainer1: {
        width: wp('80%'),
        height: hp('7%'),
        backgroundColor: 'rgb(61, 135, 241)',
        borderRadius: 5,
        alignSelf: 'center',
        justifyContent: 'center',
    },

    groupimage: {
        height: hp('33.5%'),
        width: wp('80.3%'),
    },

    option_text: {
        fontSize: hp('2.2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
    },
    addphoto: {
        fontSize: hp('2.2%'),
        color: '#3d87f1',
        fontFamily: 'SourceSansPro-Regular',
    },

    titleText: {
        fontFamily: 'SourceSansPro-Regular',
        fontSize: hp('2%'),
        color: 'rgb(179, 179, 179)',
    },

    titleTxt: {
        color: 'rgb(143, 143, 143)',
        fontSize: hp('3%'),
        marginVertical: hp('1%'),
        fontFamily: 'SourceSansPro-Regular',
    },

    valueTxt: {
        color: 'rgb(68, 67, 67)',
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
        paddingTop: '5%'
    },

    dateStyle: {
        width: wp('60%'),
        borderWidth: 1,
        borderColor: 'rgb(143, 143, 143)',
        borderRadius: 4,
        paddingLeft: wp('3%'),
        paddingRight: wp('5%'),
        marginVertical: hp('1%')
    },

    labelStyle: {
        color: 'white',
        fontSize: hp('2.2%'),
        fontFamily: 'SourceSansPro-Regular',
    },

    CommentsTxt: {
        color: 'rgb(143, 143, 143)',
        fontSize: hp('1.8%'),
        fontFamily: 'SourceSansPro-Regular',
    },

    CircleShapeView: {
        width: 16,
        height: 16,
        borderRadius: 8,
        backgroundColor: '#3d87f1',
        marginBottom: hp('2%'),
    },



});
