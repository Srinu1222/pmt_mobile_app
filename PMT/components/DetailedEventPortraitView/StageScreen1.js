import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView
} from 'react-native';
import React, { useState, useEffect, useMemo } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { useDispatch, useSelector } from 'react-redux';
import * as getStageEventActionCreators from '../../actions/gantt-stage-action/index';
import EmptyLoader from '../../components/EmptyLoader'
import notifyMessage from '../../ToastMessages'
import StageSlider from './StageSlider';
import Orientation from 'react-native-orientation';



const StageScreen1 = (props) => {
    const { route, navigation } = props;

    // Project Details
    // const [project_id, setProjectId] = useState('');
    // const [isSubContractor, setisSubContractor] = useState('');
    // const drawerReducer = useSelector((state) => {
    //     return state.getProjectReducers.selected_project.data;
    // });
    // useEffect(() => {
    //     setProjectId(drawerReducer?.id)
    //     setisSubContractor(drawerReducer?.is_sub_contractor)
    // }, [drawerReducer])

    const { event_id, project_id, is_sub_contractor, selectedTabIndex, event_name, token, fromLandScape } = route.params
    
    useEffect(()=>{
        Orientation.lockToPortrait()
        StatusBar.setHidden(false);
    }, [route.params])

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getStageEventActionCreators.getGanttEvents(token, project_id))
    }, [project_id]);

    const ganttEventReducer = useSelector(state => {
        return state.ganttEventReducers.ganttevents.get;
    })

    const renderStageScreen = () => {
        if (ganttEventReducer.success.ok) {
            let data = ganttEventReducer.success.data
            let default_event_id = data.default_event_id
            let default_event_name = data.default_event_name
            return <StageSlider project_id={project_id} is_sub_contractor={is_sub_contractor} selectedTabIndex={selectedTabIndex} event_id={event_id ? event_id : default_event_id} event_name={event_name ? event_name : default_event_name} navigation={navigation} token={token} data={ganttEventReducer.success.data} />;
        } else if (ganttEventReducer.failure.error) {
            return notifyMessage(ganttEventReducer.failure.message);
        } else {
            return (<EmptyLoader />)
        }
    };

    return renderStageScreen();
};

export default StageScreen1;
