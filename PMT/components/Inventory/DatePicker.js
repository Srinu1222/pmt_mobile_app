import React from 'react';
import { StyleSheet } from 'react-native';
import { Datepicker, Icon } from '@ui-kitten/components';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';


const DatePicker = (props) => {
    const { placeHolder, selectedDate } = props

    const DateIcon = (style) => (
        <Icon {...style} name='calendar'/>
      )

    return (
        <Datepicker
            size={'large'}
            date={selectedDate}
            onSelect={d => props.setMethod(d)}
            placeholder={placeHolder}
            // icon={DateIcon}
            accessoryRight={DateIcon}
            // title={d=>//d)}
        />
    );
};

const styles = StyleSheet.create({
    container: {
        height: 128,
    },
});

export default DatePicker;
