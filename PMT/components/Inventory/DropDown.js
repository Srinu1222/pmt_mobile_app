import React from 'react';
import { StyleSheet } from 'react-native';
import { IndexPath, Layout, Select, SelectItem } from '@ui-kitten/components';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';


const DropDown = (props) => {

    const {dataItems, placeHolder} = props
    
    const [selectedIndex, setSelectedIndex] = React.useState(null);

    const onPressSelect = (index) => {
        setSelectedIndex(index)
        props.setMethod(dataItems[index.row].id)
    }

    if (selectedIndex != null) {
        var dynamic_props = { value: dataItems[selectedIndex.row].value }
    } else {
        var dynamic_props = {}
    }

    return (
        <Select
            selectedIndex={selectedIndex}
            size={'large'}
            {...dynamic_props}
            placeholder={placeHolder}
            // style={{ borderWidth: 0, height: hp('7.4%'), backgroundColor: 'red', borderColor: 'rgba(179, 179, 179, 0.1)' }}
            
            onSelect={index => { onPressSelect(index) }}>
            {
                dataItems.map(
                    (item, index) =>
                        <SelectItem
                            key={index} 
                            title={item.value} 
                            style={{borderWidth: 0,}}
                        />
                )
            }

        </Select>
    );
};

const styles = StyleSheet.create({
    container: {
        height: 128,
    },
});
export default DropDown;