import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    Animated,
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image';
import { useDispatch, useSelector } from 'react-redux';
import { SearchBar } from 'react-native-elements';
import { FlatList } from 'react-native-gesture-handler';
import EmptyScreenHeader from '../EmptyScreens/EmptyScreenHeader';
import Pagination from '../Pagination';
import InventoryItem from './InventoryItem';
import {
    useCollapsibleSubHeader,
    CollapsibleSubHeaderAnimator,
} from 'react-navigation-collapsible';
import { FAB, Icon, BottomSheet } from 'react-native-elements';
import EmptyLoader from '../EmptyLoader'
import { Button, Divider } from 'react-native-paper'
import config from '../../config';
import { ButtonGroup, CheckBox } from 'react-native-elements';
import * as actionCreators from '../../actions/inventory-action/index';
import { useSafeArea } from 'react-native-safe-area-context';
import AddInventory from './AddInventory';



const GetInventory = props => {

    const {
        onScroll /* Event handler */,
        containerPaddingTop /* number */,
        scrollIndicatorInsetTop /* number */,
        translateY,
    } = useCollapsibleSubHeader();

    const { inventory: data, token, changeReload } = props;
    console.log("inventory props",props.inventory)


    const dispatch = useDispatch();

    const buttons = ['Stock', 'Consumed', 'Wastage']
    const [selectedIndex, setselectedIndex] = React.useState(0);
    const [selectedBtnData, setselectedBtnData] = React.useState(data['stock']);

    const onPressBtn = (index) => {
        setselectedIndex(index)
        switch (index) {
            case 0:
                setselectedBtnData(data['stock'])
                break;
            case 1:
                setselectedBtnData(data['consumed'])
                break;
            case 2:
                setselectedBtnData([])
                break;
            default:
                break
        }
    }

    const onPressOptions = params => {
        setIsVisible(true)
    };

    // Bottom Component For Add Material
    const [isAddMaterialClick, setisAddMaterialClick] = useState(false);
    const changeIsVisible = (value) => {
        setisAddMaterialClick(value)
    }

    // Bottom Components
    const [isDelete, setisDelete] = useState(false)
    const [isVisible, setIsVisible] = useState(false);
    const [isSortByVisible, setisSortByVisible] = useState(false);

    const bottomList = ['Sort By', 'Filter', 'Delete']

    const onPressFilter = (params) => {
        setIsVisible(false)
        props.navigation.navigate(
            'inventory_filter',
            {
                'projectList': data.project_list,
                'categoryList': data.category_list,
                'locationList': data.location_list
            }
        )
    }

    const onPressRemove = (params) => {
        setIsVisible(false)
        setisDelete(true)
    }

    const onPressSort = (params) => {
        setIsVisible(false)
        setisSortByVisible(true)
    }

    const SortByBottomComponent = (params) => {
        const Types = ['A-Z', 'Z-A']
        let [checkedItem, setcheckedItem] = useState(null)

        const onPressCheckBox = (item) => {
            setcheckedItem(item)
            setisSortByVisible(false)
            props.navigation.navigate('inventory', { 'doFilter': true, 'isSort': true, 'value': [checkedItem] })
        }

        return (
            <View
                style={{ height: Dimensions.get('window').height }}
            >
                <TouchableOpacity
                    onPress={() => setIsVisible(false)}
                >
                    <View style={{ height: hp('75%') }}></View>
                </TouchableOpacity>
                <View style={{ backgroundColor: 'white', height: hp('25%'), borderTopStartRadius: 10, borderTopRightRadius: 10, }}>
                    <View style={{ height: hp('7%'), justifyContent: 'space-between', paddingHorizontal: wp('5%'), paddingTop: hp('2%'), alignItems: 'center', borderWidth: 0, flexDirection: 'row' }}>
                        <Text style={{ fontSize: hp('2.7%'), fontFamily: 'SourceSansPro-Semibold', color: 'rgb(68, 67, 67)' }}>Sort By</Text>
                        <Icon
                            iconStyle={{ fontSize: hp('4%') }}
                            name='close-outline'
                            type='ionicon'
                            color='rgb(68, 67, 67)'
                            onPress={() => setisSortByVisible(false)}
                        />
                    </View>
                    {Types.map((item, index) =>
                        <View
                            key={index}
                        >
                            < CheckBox
                                containerStyle={{ borderColor: '#fff', backgroundColor: 'white' }}
                                left
                                title={item}
                                textStyle={{ color: 'rgb(68, 67, 67)', fontSize: hp('2%'), fontWeight: 'normal' }}
                                checkedIcon='dot-circle-o'
                                uncheckedIcon='circle-o'
                                checked={item == checkedItem}
                                onPress={() => onPressCheckBox(index)}
                                uncheckedColor={'#3d87f1'}
                                checkedColor={'#3d87f1'}
                            />
                        </View>
                    )}
                </View>
            </View>
        );
    }

    const BottomComponent = (params) => {
        return (
            <View
                style={{ height: Dimensions.get('window').height }}
            >
                <TouchableOpacity
                    onPress={() => setIsVisible(false)}
                >
                    <View style={{ height: hp('75%') }}></View>
                </TouchableOpacity>
                <View style={{ backgroundColor: 'white', padding: '4%', height: hp('25%'), borderTopStartRadius: 10, borderTopRightRadius: 10, }}>
                    {
                        bottomList.map(
                            (item, index) => {
                                const iconsList = ["swap-vertical-outline", "funnel-outline", "trash-outline"]
                                return (
                                    <View key={index} >
                                        <TouchableOpacity
                                            onPress={() => {
                                                if (index == 0) {
                                                    onPressSort()
                                                } else if (index == 1) {
                                                    onPressFilter()
                                                } else {
                                                    onPressRemove()
                                                }
                                            }}
                                        >
                                            <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: hp('1.5%') }}>
                                                <Icon
                                                    iconStyle={{ fontSize: hp('3%') }}
                                                    name={iconsList[index]}
                                                    type='ionicon'
                                                    color='rgb(68, 67, 67)'
                                                />
                                                <Text style={styles.filterText}>{item}</Text>
                                            </View>

                                        </TouchableOpacity>
                                        {index != bottomList.length - 1 && <Divider style={{ backgroundColor: 'rgb(232, 232, 232)' }} />}
                                    </View>
                                );
                            }
                        )
                    }
                </View>
            </View>

        );
    }

    const [selectedInventoryCheckBoxList, setselectedInventoryCheckBoxList] = React.useState([]);
    const onPressDeleteIcon = (params) => {
        dispatch(actionCreators.removeInventory(props.token, selectedInventoryCheckBoxList));
    }

    const [activeButton, setactiveButton] = React.useState(1);

    const [searchQuery, setSearchQuery] = React.useState('');

    const [isClickmanageTeam, setisClickmanageTeam] = useState(false);

    const onChangeSearch = query => setSearchQuery(query);

    const newProject = params => { };

    const onPressReManageTeam = () => {
        setisClickmanageTeam(true)
    };

    const filterItems = (items, filter) => {
        return items.filter(item => item.category.toLowerCase().includes(filter.toLowerCase()))
    }

    const Footer = () => (

        <View style={{
            flex: 1,
            justifyContent: 'center',
        }}>
            <View style={{
                backgroundColor: '#3d87f1',
                paddingHorizontal: wp('8%'),
                justifyContent: 'space-between',
                flexDirection: 'row',
                height: hp('7.4%'),
                alignItems: 'center',
            }}>
                <Button
                    style={{ width: wp('40%'), height: hp('5.5%'), borderWidth: 1, borderColor: 'white', backgroundColor: '#3d87f1' }}
                    labelStyle={{ color: 'white', textTransform: 'capitalize', fontSize: hp('1.6%') }}
                    mode="contained" onPress={() => changeIsVisible(true)}>
                    +Add Material
                </Button>
                <Button
                    style={{ width: wp('40%'), height: hp('5.5%'), borderWidth: 1, borderColor: 'white', backgroundColor: 'white' }}
                    labelStyle={{ color: '#3d87f1', textTransform: 'capitalize', fontSize: hp('1.6%') }}
                    mode="contained"
                    onPress={() => onPressReManageTeam()}>
                    Report Wastage
                </Button>
            </View>
        </View>
    );

    const EmptyMaterial = (params) => {
        return (
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                < FastImage
                    style={styles.groupimage}
                    source={require('../../assets/icons/inventory.png')
                    }
                    resizeMode={FastImage.resizeMode.contain}
                />
                <Text style={styles.textMessage}>There are no materials added to the inventory.</Text>
                <Text style={styles.thumbnailText}>Please add new material.</Text>
            </View>
        );
    }

    const DeleteHeader = (params) => {
        return (
            <View style={{ height: hp('9%'), justifyContent: 'space-between', alignItems: 'center', padding: '5%', flexDirection: 'row' }}>
                <Text style={styles.deleteHeaderTitle}>{selectedInventoryCheckBoxList.length} Selected</Text>
                <TouchableOpacity
                    onPress={
                        () => setisDelete(false)
                    }
                >
                    <Text style={styles.cancelText}>Cancel</Text>
                </TouchableOpacity>
            </View>
        );
    }

    return (
        <View style={styles.container}>

            <BottomSheet
                isVisible={isVisible}
                onPress={() => null}
                containerStyle={{ backgroundColor: 'rgba(0.5, 0.25, 0, 0.2)' }}
            >
                {BottomComponent()}
            </BottomSheet>

            <BottomSheet
                isVisible={isSortByVisible}
                containerStyle={{ backgroundColor: 'rgba(0.5, 0.25, 0, 0.2)' }}
            >
                {SortByBottomComponent()}
            </BottomSheet>

            {
                isDelete ?
                    <DeleteHeader />
                    :
                    <EmptyScreenHeader title='Inventory' navigation={props.navigation} />
            }

            <BottomSheet
                isVisible={isAddMaterialClick}
                containerStyle={{ backgroundColor: 'rgba(0.5, 0.25, 0, 0.2)' }}
            >
                <AddInventory
                    changeIsVisible={changeIsVisible}
                    token={token}
                    changeReload={changeReload}
                />
            </BottomSheet>

            <View style={{ flex: 10 }}>
                <View
                    style={{
                        // elevation: 8,
                        paddingTop: 15,
                        paddingLeft: wp('5%'),
                        paddingRight: wp('5%'),
                        width: '100%',
                        height: 'auto',
                        backgroundColor: '#fff',
                    }}>
                    <View
                        style={{
                            display: 'flex',
                            marginTop: hp('1%'),
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                        }}>
                        <SearchBar
                            showLoading={false}
                            platform={Platform.OS}
                            containerStyle={{
                                borderWidth: 1,
                                borderRadius: 4,
                                borderColor: 'rgb(232, 232, 232)',
                                backgroundColor: 'white',
                                justifyContent: 'center',
                                width: wp('73.3%'),
                                height: hp('5.3%'),
                            }}
                            clearIcon={true}
                            onChangeText={onChangeSearch}
                            onClearText={() => null}
                            placeholder=""
                            cancelButtonTitle="Cancel"
                        />

                        {
                            isDelete ?
                                <Icon
                                    iconStyle={{ fontSize: hp('4%') }}
                                    name={'trash-outline'}
                                    type='ionicon'
                                    color='rgb(68, 67, 67)'
                                    onPress={() => onPressDeleteIcon()}
                                />
                                :
                                <TouchableOpacity
                                    onPress={() => {
                                        onPressOptions();
                                    }}
                                    style={{}}>
                                    <FastImage
                                        style={{ width: wp('12%'), height: hp('4%') }}
                                        source={require('../../assets/icons/rectangle.png')}
                                        resizeMode={FastImage.resizeMode.contain}
                                    />
                                    <Text style={styles.option_text}>Options</Text>
                                </TouchableOpacity>
                        }

                    </View>
                    <View style={{ width: wp('100%'), marginVertical: hp('2%'), alignSelf: 'center' }}>
                        <ButtonGroup
                            onPress={(index) => { onPressBtn(index) }}
                            buttons={buttons}
                            containerStyle={{ height: hp('5.6%'), left: 0, }}
                            selectedIndex={selectedIndex}
                            buttonContainerStyle={{ backgroundColor: 'white' }}
                            textStyle={{
                                fontSize: hp('1.8%'),
                                fontFamily: 'SourceSansPro-Regular',
                                color: 'rgb(143, 143, 143)'
                            }}
                            selectedButtonStyle={{ backgroundColor: '#3d87f1', }}
                        />
                    </View>
                </View >

                {
                    selectedBtnData.length != 0 &&
                    <View style={{ marginBottom: hp('1%'), paddingHorizontal: wp('5%') }}>
                        <Pagination
                            activeButton={activeButton}
                            setactiveButton={setactiveButton}
                            reducerLength={selectedBtnData.length}
                            style={{ paddingBottom: hp('5%') }}
                        />
                    </View>
                }

                <Animated.FlatList
                    ListEmptyComponent={<EmptyMaterial />}
                    keyExtractor={(item, index) => index}
                    data={filterItems(selectedBtnData.slice(activeButton * 10 - 10, activeButton * 10), searchQuery)}
                    renderItem={({ item, index }) => <InventoryItem item={item} isDelete={isDelete} navigation={props.navigation} selectedInventoryCheckBoxList={selectedInventoryCheckBoxList} setselectedInventoryCheckBoxList={setselectedInventoryCheckBoxList} />}
                    onScroll={onScroll}
                    contentContainerStyle={{
                        paddingLeft: hp('3%'),
                        paddingRight: hp('3%'),
                        paddingBottom: hp('1%'),
                    }}
                    scrollIndicatorInsets={{ top: scrollIndicatorInsetTop }}
                />

            </View>
            {
                !isDelete && <Footer />
            }
        </View >
    );
};

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        flex: 1,
        backgroundColor: '#fff',
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    option_text: {
        fontSize: hp('1.2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center',
    },

    deleteHeaderTitle: {
        fontSize: hp('2.8%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center',
        fontWeight: 'bold'
    },

    cancelText: {
        fontFamily: 'SourceSansPro-Regular',
        fontSize: hp('2.5%'),
        color: '#3d87f1'
    },

    textMessage: {
        width: wp('80%'),
        height: hp('8%'),
        textAlign: 'center',
        fontSize: hp('2.3%'),
        color: 'rgb(143,143,143)'
    },

    thumbnailText: {
        width: wp('90%'),
        height: hp('4%'),
        fontSize: hp('1.8%'),
        textAlign: 'center',
        color: 'rgb(190,198,214)'
    },

    filterText: {
        fontSize: hp('2.6%'),
        fontFamily: 'SourceSansPro-Semibold',
        color: 'rgb(68, 67, 67)',
        paddingLeft: wp('5%')
    },

    groupimage: {
        height: hp('40.5%'),
        width: wp('50.3%'),
    },
});

export default GetInventory;
