import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    Animated,
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image';
import { useDispatch, useSelector } from 'react-redux';
import { SearchBar } from 'react-native-elements';
import { FlatList } from 'react-native-gesture-handler';
import EmptyScreenHeader from '../EmptyScreens/EmptyScreenHeader';
import Pagination from '../Pagination';
import {
    useCollapsibleSubHeader,
    CollapsibleSubHeaderAnimator,
} from 'react-navigation-collapsible';
import { FAB, Icon, BottomSheet } from 'react-native-elements';
import EmptyLoader from '../EmptyLoader'
import { Button, Divider, Card } from 'react-native-paper'
import { ButtonGroup, CheckBox } from 'react-native-elements';
import DropDown from './DropDown'
import DatePicker from './DatePicker';



const InventoryAssign = (props) => {

    const { inventory_id, product, category, location, quantity, rate, value, project_name } = props.route.params.data

    const [selectedProjectId, setselectedProjectId] = useState(null);
    const projectDataItems = [
        {
            'id': 30,
            'value': 'pmt'
        },
        {
            'id': 32,
            'value': 'pmtDevelopment'
        },
    ]

    const [selectedTeamMemberId, setselectedTeamMemberId] = useState(null);
    const SpocDataItems = [
        {
            'id': 2,
            'value': 'hari'
        },
        {
            'id': 5,
            'value': 'ramana'
        },
    ]

    const [selectedDate, setselectedDate] = useState('')
    //selectedDate)

    return (
        <View style={[{ padding: '5%', backgroundColor: '#fff', flex: 1, paddingTop: '0%' }]}>
            <View style={{ height: hp('9%'), justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row' }}>
                <Text style={styles.deleteHeaderTitle}>Assign Material</Text>
                <Icon
                    iconStyle={{ fontSize: hp('4.5%') }}
                    name='close-outline'
                    type='ionicon'
                    color='rgb(68, 67, 67)'
                    onPress={() => props.navigation.navigate('inventory')}
                />
            </View>

            <Divider style={{ backgroundColor: 'rgb(232,232,232)', height: hp('0.3%') }} />

            <View style={styles.deleteItemCard}>
                <View>
                    <Text style={styles.name_text}>{product}</Text>
                    <Text style={styles.department_text}>Category</Text>
                </View>

                <Text style={{ height: hp('0.2%'), marginVertical: '4%', backgroundColor: 'rgb(232, 232, 232)' }}></Text>
                <View style={{ display: 'flex', marginBottom: 20, width: wp('70%'), flexDirection: 'row', justifyContent: 'space-between' }}>
                    <View>
                        <Text style={styles.email_text}>{quantity}</Text>
                        <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>Quantity</Text>
                    </View>
                    <View>
                        <Text style={styles.email_text}>{rate}</Text>
                        <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>Rate</Text>
                    </View>
                    <View>
                        <Text style={styles.email_text}>{value}</Text>
                        <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>Value</Text>
                    </View>
                </View>

                <View style={{}}>
                    <Text style={styles.email_text}>{location}</Text>
                    <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>Project Location</Text>
                </View>
            </View>

            <View style={{ marginTop: hp('5%'), marginBottom: hp('1.5%') }}>
                <Card.Title
                    style={{ height: hp('7.4%'), backgroundColor: 'rgba(179, 179, 179, 0.1)' }}
                    title={project_name}
                    titleStyle={[styles.email_text, { fontSize: hp('2.5%') }]}
                />
            </View>

            <View style={{ marginBottom: hp('1.5%') }}>
                < DropDown
                    dataItems={projectDataItems}
                    placeHolder='New Project'
                    setMethod={setselectedProjectId}
                />
            </View>

            <View style={{ marginBottom: hp('1.5%') }}>
                < DropDown
                    dataItems={SpocDataItems}
                    placeHolder='Add SPOC'
                    setMethod={setselectedTeamMemberId}
                />
            </View>

            <View style={{ marginBottom: hp('5%') }}>
                <DatePicker
                    selectedDate={selectedDate}
                    setMethod={setselectedDate}
                    placeHolder='Set Target Date'
                />
            </View>

            <View style={{
                justifyContent: 'space-between',
                flexDirection: 'row',
                height: hp('8%'),
                alignItems: 'center',
            }}>
                <Button
                    style={{ width: wp('42%'), height: hp('5.5%'), borderWidth: 1, borderColor: '#3d87f1', backgroundColor: 'white' }}
                    labelStyle={{ color: '#3d87f1', textTransform: 'capitalize', fontSize: hp('2%') }}
                    mode="contained"
                    onPress={() => props.navigation.navigate('inventory')}>
                    Cancel
                </Button>
                <Button
                    style={{ width: wp('42%'), height: hp('5.5%'), backgroundColor: '#3d87f1' }}
                    labelStyle={{ color: 'white', textTransform: 'capitalize', fontSize: hp('2%') }}
                    mode="contained" onPress={() => onPressReCalculate(selectedIndex)}>
                    Assign
                </Button>
            </View>

        </View>
    );
}

const styles = StyleSheet.create({
    name_text: {
        fontSize: hp('2.8%'),
        fontFamily: 'SourceSansPro-SemiBold',
        color: 'rgb(68, 67, 67)',
        textTransform: 'capitalize'
    },

    email_text: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        textTransform: 'capitalize'
    },

    department_text: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-SemiBold',
        color: 'rgb(194, 194, 194)',
    },



    deleteItemCard: {
        elevation: 8,
        shadowColor: 'rgba(0,0,0,0.4)',
        backgroundColor: 'white',
        marginTop: 16.5,
        padding: '6%',
        paddingRight: '4%'
    },



    deleteHeaderTitle: {
        fontSize: hp('2.8%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center',
        fontWeight: 'bold'
    },


});


export default InventoryAssign;