import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    Button
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import Icons from "react-native-vector-icons/Ionicons";
import { white } from 'react-native-paper/lib/typescript/styles/colors';
import { Card, Checkbox } from 'react-native-paper'


const TotalWidth = (Dimensions.get('window').width) * 0.3


function EllipsesCard(props) {
    const downloadPO = (params) => {

    }

    const duplicate = (params) => {

    }

    const reAssign = (params) => {

    }

    const assign = (params) => {
        props.navigation.navigate('inventory_assign', {data: props.item})
    }

    return (
        <View style={styles.ellipsesCard}>
            <TouchableOpacity
                onPress={() => { downloadPO() }}
                style={styles.ellipsesbtn}
            >
                <Text style={styles.ellipsestxt}>Download PO</Text>
            </TouchableOpacity>


            <TouchableOpacity
                onPress={() => { duplicate() }}
                style={styles.ellipsesbtn}
            >
                <Text style={styles.ellipsestxt}>Duplicate</Text>
            </TouchableOpacity>

            <TouchableOpacity
                onPress={() => { reAssign() }}
                style={styles.ellipsesbtn}
            >
                <Text style={styles.ellipsestxt}>ReAssign</Text>
            </TouchableOpacity>

            <TouchableOpacity
                onPress={() => { assign() }}
                style={styles.ellipsesbtn}
            >
                <Text style={styles.ellipsestxt}>Assign</Text>
            </TouchableOpacity>
        </View>
    );
}

function Item(props) {

    const { inventory_id, product, category, quantity, rate, value, project_name } = props.item;

    console.log("Inventory props.item.status",props.item.status)

    const [selectedId, setselectedId] = React.useState(null);
    const [isClickOnEllipses, setisClickOnEllipses] = React.useState(false);

    const OnPressEllipses = (params) => {
        setisClickOnEllipses(!isClickOnEllipses)
        setselectedId(params.inventory_id)
    }

    
    const onPressInventoryCheckBox = (id) => {
        if (props.selectedInventoryCheckBoxList.includes(id)) {
            props.selectedInventoryCheckBoxList.splice(props.selectedInventoryCheckBoxList.indexOf(id), 1);
            props.setselectedInventoryCheckBoxList([...props.selectedInventoryCheckBoxList])
        } else {
            props.setselectedInventoryCheckBoxList([...props.selectedInventoryCheckBoxList, id])
        }
    }

    return (
        <View style={[
            props.isDelete ? styles.deleteItemCard : styles.item_card,
            props.isDelete && { flexDirection: 'row' }
        ]}>
            {
                props.isDelete &&
                <View style={{ paddingRight: '2%', paddingTop: '3%', left: 0 }}>
                    <Checkbox
                        color={'#3d87f1'}
                        uncheckedColor={'rgb(232,232,232)'}
                        status={props.selectedInventoryCheckBoxList.includes(inventory_id) ? 'checked' : 'unchecked'}
                        onPress={() => {
                            onPressInventoryCheckBox(inventory_id)
                        }}
                    />
                </View>
            }

            <View>
                <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>

                    <View>
                        <Text style={styles.name_text}>{product}</Text>
                        <Text style={styles.department_text}>{category}</Text>
                    </View>

                    {selectedId == inventory_id && isClickOnEllipses ? <EllipsesCard item={props.item} navigation={props.navigation}/> : null}

                    <TouchableOpacity>
                        <Icons.Button
                            name="ellipsis-vertical"
                            size={25}
                            color='rgb(143, 143, 143)'
                            backgroundColor='white'
                            onPress={() => { OnPressEllipses({ inventory_id }) }}
                        />
                    </TouchableOpacity>

                </View>

                <Text style={{ height: hp('0.2%'), marginVertical: '4%', backgroundColor: 'rgb(232, 232, 232)' }}></Text>
                <View style={{ display: 'flex', marginBottom: 20, width: wp('70%'), flexDirection: 'row', justifyContent: 'space-between' }}>
                    <View>
                        <Text style={styles.email_text}>{quantity}</Text>
                        <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>Quantity</Text>
                    </View>
                    <View>
                        <Text style={styles.email_text}>{rate}</Text>
                        <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>Rate</Text>
                    </View>
                    <View>
                        <Text style={styles.email_text}>{value}</Text>
                        <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>Value</Text>
                    </View>
                </View>

                <View style={{ display: 'flex', marginBottom: 15, flexDirection: 'row', width: wp('65%'), justifyContent: 'space-between' }}>
                    <View style={{}}>
                        <Text style={styles.email_text}>{props.item.status.replace(/_/g, " ")}</Text>
                        <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>Status</Text>
                    </View>
                    <View style={{ alignSelf: 'center' }}>
                        <Text style={styles.email_text}>{project_name}</Text>
                        <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>Project Name</Text>
                    </View>

                </View>
            </View>


        </View>
    )
}


const styles = StyleSheet.create({
    name_text: {
        fontSize: hp('2.8%'),
        fontFamily: 'SourceSansPro-SemiBold',
        color: 'rgb(68, 67, 67)',
    },

    email_text: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
    },

    department_text: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-SemiBold',
        color: 'rgb(194, 194, 194)',
    },

    item_card: {
        elevation: 8,
        shadowColor: 'rgba(0,0,0,0.4)',
        backgroundColor: 'white',
        borderLeftColor: 'rgb(61, 135, 241)',
        borderLeftWidth: wp('1.3%'),
        marginTop: 16.5,
        borderTopLeftRadius: 4,
        borderBottomLeftRadius: 4,
        padding: '5%',
        paddingRight: '4%'
    },

    deleteItemCard: {
        elevation: 8,
        shadowColor: 'rgba(0,0,0,0.4)',
        backgroundColor: 'white',
        marginTop: 16.5,
        padding: '5%',
        paddingRight: '4%'
    },

    ellipsesCard: {
        position: 'absolute',
        marginLeft: TotalWidth,
        marginTop: hp('5%'),
        zIndex: 10,
        backgroundColor: 'white',
        width: wp('42%'),
        height: hp('20%'),
        borderWidth: 0,
        shadowColor: 'rgba(0,0,0,0.4)',
        elevation: 8,
        padding: '5%',
        borderRadius: 4,
    },

    ellipsesbtn: {
        marginVertical: 5,
    },

    ellipsestxt: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(143, 143, 143)',
    }

});


export default Item;