import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    Animated,
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image';
import { useDispatch, useSelector } from 'react-redux';
import { SearchBar } from 'react-native-elements';
import { FlatList } from 'react-native-gesture-handler';
import EmptyScreenHeader from '../EmptyScreens/EmptyScreenHeader';
import { ButtonGroup } from 'react-native-elements';
import {
    useCollapsibleSubHeader,
    CollapsibleSubHeaderAnimator,
} from 'react-navigation-collapsible';
import { FAB, Icon } from 'react-native-elements';
import { Avatar, Button, Checkbox, Card, Divider, IconButton } from 'react-native-paper';
import Icons from "react-native-vector-icons/Ionicons";


const InventoryFilter = props => {

    const [selectedCardIndex, setselectedCardIndex] = React.useState(null);
    const [isSelectedCard, setisSelectedCard] = React.useState(false);

    const buttons = ['Project Name', 'Category', 'Location']
    const [selectedIndex, setselectedIndex] = React.useState(0);

    const {
        onScroll /* Event handler */,
        containerPaddingTop /* number */,
        scrollIndicatorInsetTop /* number */,
        translateY,
    } = useCollapsibleSubHeader();

    const [selectedProjectCheckBoxList, setselectedProjectCheckBoxList] = React.useState([]);
    const onPressProjectCheckBox = (item) => {
        if (selectedProjectCheckBoxList.includes(item)) {
            selectedProjectCheckBoxList.splice(selectedProjectCheckBoxList.indexOf(item), 1);
            setselectedProjectCheckBoxList([...selectedProjectCheckBoxList])
        } else {
            setselectedProjectCheckBoxList([...selectedProjectCheckBoxList, item])
        }
    }

    const [selectedCategoriesCheckBoxList, setselectedCategoriesCheckBoxList] = React.useState([]);
    const onPressCategoriesCheckBox = (item) => {
        if (selectedCategoriesCheckBoxList.includes(item)) {
            selectedCategoriesCheckBoxList.splice(selectedCategoriesCheckBoxList.indexOf(item), 1);
            setselectedCategoriesCheckBoxList([...selectedCategoriesCheckBoxList])
        } else {
            setselectedCategoriesCheckBoxList([...selectedCategoriesCheckBoxList, item])
        }
    }

    const [selectedLocationCheckBoxList, setselectedLocationCheckBoxList] = React.useState([]);
    const onPressLocationCheckBox = (item) => {
        if (selectedLocationCheckBoxList.includes(item)) {
            selectedLocationCheckBoxList.splice(selectedLocationCheckBoxList.indexOf(item), 1);
            setselectedLocationCheckBoxList([...selectedLocationCheckBoxList])
        } else {
            setselectedLocationCheckBoxList([...selectedLocationCheckBoxList, item])
        }
    }

    const FlatlistItem = (props) => {
        const { item, index } = props
        const statusList = [
            selectedProjectCheckBoxList.includes(item),
            selectedCategoriesCheckBoxList.includes(item),
            selectedLocationCheckBoxList.includes(item)
        ]
        const onPressCheckBox = (item) => {
          if(selectedIndex==0) {
            onPressProjectCheckBox(item)
          } else if(selectedIndex == 1) {
              onPressCategoriesCheckBox(item)
          } else {
              onPressLocationCheckBox(item)
          }
        }
        
        return (
            <View>
                <TouchableOpacity
                    onPress={() => {onPressCheckBox(item)}}
                >
                    <View
                        style={{ flexDirection: 'row', paddingHorizontal: wp('8%'), paddingVertical: hp('1.5%'), justifyContent: 'flex-start' }}
                        key={props.index}
                    >
                        <Checkbox
                            color={'#3d87f1'}
                            uncheckedColor={'#3d87f1'}
                            status={statusList[selectedIndex]?'checked' : 'unchecked'}
                        />
                        <Text style={styles.text}>{item}</Text>
                    </View>
                </TouchableOpacity>

                <Divider style={{ backgroundColor: 'rgb(232,232,232)' }} />
            </View>
        );
    }

    const data = [
        {
            'title': 'Filter By Project Name',
            'list_data': props.route.params.projectList
        },
        {
            'title': 'Filter By Category',
            'list_data': props.route.params.categoryList
        },
        {
            'title': 'Filter By location',
            'list_data': props.route.params.locationList
        }
    ]

    return (
        <View style={styles.container}>
            <View style={{ height: hp('9%'), justifyContent: 'space-between', padding: '5%', flexDirection: 'row' }}>
                <Text style={styles.option_text}>{data[selectedIndex]['title']}</Text>
                <Icon
                    iconStyle={{ fontSize: hp('4.5%') }}
                    name='close-outline'
                    type='ionicon'
                    color='rgb(68, 67, 67)'
                    onPress={() => 
                        props.navigation.navigate(
                            'inventory', 
                            { 
                                'doFilter': true, 
                                'isSort': false,
                                'value': [
                                    selectedProjectCheckBoxList,
                                    selectedCategoriesCheckBoxList,
                                    selectedLocationCheckBoxList
                                ]
                            }
                        )
                    }
                />
            </View>

            < Divider style={{ marginHorizontal: '5%', backgroundColor: 'rgb(232, 232, 232)', marginTop: '0%' }} />

            <View>
                <ButtonGroup
                    onPress={(index) => setselectedIndex(index)}
                    buttons={buttons}
                    containerStyle={{ height: hp('5.3%'), marginTop: hp('3%') }}
                    selectedIndex={selectedIndex}
                    buttonContainerStyle={{ backgroundColor: 'white' }}
                    selectedButtonStyle={{ backgroundColor: '#3d87f1', }}
                    textStyle={{
                        fontSize: hp('2%'),
                        fontFamily: 'SourceSansPro-Regular',
                        color: 'rgb(143, 143, 143)'
                    }}
                />
            </View>

            <View>
                <Animated.FlatList
                    // ListEmptyComponent={<EmptyMaterial />}
                    keyExtractor={(item, index) => index}
                    data={data[selectedIndex]['list_data']}
                    renderItem={
                        ({ item, index }) => <FlatlistItem index={index} item={item} />}
                    onScroll={onScroll}
                    contentContainerStyle={{
                        // paddingLeft: hp('3%'),
                        // paddingRight: hp('3%'),
                        paddingBottom: hp('2%'),
                    }}
                    scrollIndicatorInsets={{ top: scrollIndicatorInsetTop }}
                />
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: '#fff',
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    option_text: {
        fontSize: hp('3%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center',
    },

    text: {
        fontSize: hp('2.3%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center',
        textTransform: 'capitalize',
        paddingLeft: wp('5%')
    }
});

export default InventoryFilter;
