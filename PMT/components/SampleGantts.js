import React from 'react'
import { View, Text } from 'react-native'
import SampleGanttEmptyScreen from '../components/SampleGantts/SampleGanttEmptyScreen'

const SampleGantts = ({route, navigation}) => {

    return (
        < SampleGanttEmptyScreen navigation={navigation}/>
    )
}

export default SampleGantts
