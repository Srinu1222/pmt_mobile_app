import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    Linking
} from 'react-native';
import React, { useState, useMemo } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { SearchBar } from 'react-native-elements';
import EmptyScreenHeader from '../EmptyScreens/EmptyScreenHeader'

const SampleGanttEmptyScreen = (props) => {

    return (
        <>
            <KeyboardAvoidingView>
                <View style={styles.container}>
                    <EmptyScreenHeader {...props} title='Sample Gantts' />
                    <View style={styles.parent2}>
                        < FastImage
                            style={styles.groupimage}
                            source={require('../../assets/icons/samplegantt.png')
                            }
                            resizeMode={FastImage.resizeMode.contain}
                        />
                        <Text style={styles.thumbnailText}>Sample gantts can only be accessed on</Text>
                        <Text style={styles.thumbnailText}>
                            our website. Please visit <Text style={{ color: '#74a9f5' }}
                                onPress={() => Linking.openURL('https://pmt.safearth.in/')}>
                                https://pmt.safearth.in/
                             </Text>
                        </Text>

                    </View>
                </View>
            </KeyboardAvoidingView>
        </ >
    );
};

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        display: 'flex',
        backgroundColor: "white",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    parent2: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        flex: 11,
        backgroundColor: 'white',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },

    thumbnailText: {
        width: wp('98%'),
        height: hp('4%'),
        fontSize: hp('2%'),
        textAlign: 'center',
        color: '#b1b1b1'
    },

    groupimage: {
        height: hp('50.4%'),
        width: wp('80.3%'),
    }
});

export default SampleGanttEmptyScreen;
