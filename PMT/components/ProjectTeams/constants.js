export const roleOptions = [
    'Customer', 'Project Manager', 'Sales', 'Liasoning Manager', 'Liasoning Officer',
    'Designer', 'Lead Designer', 'Procurement Manager', 'Procurement Executive', 'Logistics Manager',
    'Logistics Executive', 'Safety Manager', 'Site In-Charge', 
    'Site Executive', 'O&M Manager', 'O&M Executive'
]

const roleOptionsMakingDict = () => {
    let x = []
    roleOptions.map((i, index) => x.push({'id': index, 'name': i}))
    return x;
}

export const roleOptionsDict = roleOptionsMakingDict()
