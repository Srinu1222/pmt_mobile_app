import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    Animated,
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image';
import { useDispatch, useSelector } from 'react-redux';
import * as actionCreator from '../../actions/team-action/index';
import { AuthContext } from '../../context';
import { SearchBar } from 'react-native-elements';
import { FlatList } from 'react-native-gesture-handler';
import EmptyScreenHeader from '../EmptyScreens/EmptyScreenHeader';
import Pagination from '../Pagination';
import ManageTeamItem from './ManageTeamItem';
import TeamMembers from '../EmptyScreens/TeamMembers';
import {
    useCollapsibleSubHeader,
    CollapsibleSubHeaderAnimator,
} from 'react-navigation-collapsible';
import { FAB, Icon } from 'react-native-elements';
import EmptyLoader from '../EmptyLoader'
import { Button, Divider } from 'react-native-paper'


const ManageTeam = props => {

    const {
        onScroll /* Event handler */,
        containerPaddingTop /* number */,
        scrollIndicatorInsetTop /* number */,
        translateY,
    } = useCollapsibleSubHeader();

    const data = props.data

    const onPressCancel = params => {
        props.setisClickmanageTeam(false)
    };

    const Footer = () => (

        <View style={{
            flex: 1,
            justifyContent: 'center'
        }}>
            <View style={{
                backgroundColor: '#3d87f1',
                paddingHorizontal: wp('8%'),
                justifyContent: 'space-between',
                flexDirection: 'row',
                height: hp('7.4%'),
                alignItems: 'center',
            }}>
                <Button
                    style={{ width: wp('40%'), height: hp('5.5%'), borderWidth: 1, borderColor: 'white', backgroundColor: '#3d87f1' }}
                    labelStyle={{ color: 'white', textTransform: 'capitalize', fontSize: hp('1.6%') }}
                    mode="contained" onPress={() => onPressCancel()}>
                    Cancel
                </Button>
                <Button
                    style={{ width: wp('40%'), height: hp('5.5%'), borderWidth: 1, borderColor: 'white', backgroundColor: 'white' }}
                    labelStyle={{ color: '#3d87f1', textTransform: 'capitalize', fontSize: hp('1.6%') }}
                    mode="contained"
                    onPress={() => onPressSave()}>
                    Save
                </Button>
            </View>
        </View>
    );

    const Header = (params) => {
        return (
            <View style={{ flex: 1, flexDirection: 'row', paddingTop: '1%', paddingHorizontal: '5%', alignItems: 'center', justifyContent: 'space-between' }}>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={{fontFamily: 'SourceSansPro-Semibold', fontWeight: 'bold', fontSize: hp('2.4%'), color: 'rgb(68, 67, 67)'}}>Manage Team </Text>
                    <Text style={{fontFamily: 'SourceSansPro-Regular',fontSize: hp('2.2%'), marginTop: hp('0.3%'), color: 'rgb(143, 143, 143)'}}>(Roles & Rights)</Text>
                </View>
                <Icon
                    iconStyle={{fontSize: hp('4.5%')}}
                    name='close-outline'
                    type='ionicon'
                    color='rgb(68, 67, 67)'
                />
            </View>
        );
    }


    return (
        <View style={styles.container}>
            <Header />
            <View style={{ flex: 10, paddingHorizontal: '5%', }}>
                <Divider />
                <Animated.FlatList
                    keyExtractor={(item, index) => index}
                    data={data}
                    renderItem={({ item, index }) => <ManageTeamItem item={item} />}
                    onScroll={onScroll}
                    contentContainerStyle={{
                        paddingBottom: hp('1%'),
                    }}
                    scrollIndicatorInsets={{ top: scrollIndicatorInsetTop }}
                />
            </View>
            <Footer />
        </View>
    );
};


const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        flex: 1,
        backgroundColor: '#fff',
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    option_text: {
        fontSize: hp('1.2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center',
    },
});

export default ManageTeam;
