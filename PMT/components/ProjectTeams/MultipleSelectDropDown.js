// Multiple Select / Dropdown / Picker Example in React Native
// https://aboutreact.com/multiple-select-dropdown-picker-example-in-react-native/

// import React in our code
import React, { useState, useEffect } from 'react';

// import all the components we are going to use
import { SafeAreaView, StyleSheet, Text, View } from 'react-native';

// import MultiSelect library
import MultiSelect from 'react-native-multiple-select';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';


const App = (props) => {

    const { roles, setRoles, items } = props

    // Data Source for the SearchableDropdown
    const [selectedItems, setSelectedItems] = useState([]);

    const onSelectedItemsChange = (selectedItems) => {
        // Set Selected Items
        setRoles(selectedItems.map((i)=>items[i].name))
        setSelectedItems(selectedItems);
    };

    return (
        <View style={styles.container}>
            <MultiSelect
                hideTags
                items={items}
                uniqueKey="id"
                onSelectedItemsChange={onSelectedItemsChange}
                selectedItems={selectedItems}
                selectText="Select Roles"
                textColor={selectedItems.length > 0 ? 'rgb(68, 67, 67)' : 'rgb(143, 143, 143)'}
                //   styleMainWrapper={{borderWidth: 1, paddingLeft: wp('2%'), borderColor: 'rgb(143, 143, 143)', borderRadius: 4, }}
                searchInputPlaceholderText="Search Items..."
                onChangeInput={(text) => {}}
                tagRemoveIconColor="#CCC"
                tagBorderColor="#CCC"
                tagTextColor="#CCC"
                selectedItemTextColor="#3d87f1"
                selectedItemIconColor="#3d87f1"
                itemTextColor="rgb(143, 143, 143)"
                displayKey="name"
                searchInputStyle={{ color: '#CCC' }}
                submitButtonColor="#48d22b"
                submitButtonText="Submit"
                hideSubmitButton
            />
        </View>
    );
};

export default App;

const styles = StyleSheet.create({
    container: {
        width: wp('80%'),
        alignSelf: 'center',
        marginVertical: hp('1%'),
        paddingLeft: 10,
        backgroundColor: 'white',
    },
    titleText: {
        padding: 8,
        fontSize: 16,
        textAlign: 'center',
        fontWeight: 'bold',
    },
    headingText: {
        padding: 8,
    },
});