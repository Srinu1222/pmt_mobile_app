import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    Animated,
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Divider } from 'react-native-paper';
import { roleOptions, roleOptionsDict } from './constants';
import ModalDropdown from 'react-native-modal-dropdown';
import { FAB, Icon, Input, BottomSheet } from 'react-native-elements';
import * as actionCreators from "../../actions/project-level-team-action/index";
import Toast from "react-native-toast-message";
import { IndexPath, Layout, Select, SelectGroup, SelectItem } from '@ui-kitten/components';
import App from './MultipleSelectDropDown';

const AddTeam = (params) => {

    const { changeIsVisible, token, project_id, changeReload } = params

    var [IconPosition, setIconPosition] = useState(false);

    const [id, setId] = useState("");
    const [roles, setRoles] = useState([]);

    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(actionCreators.getTeamMembersName(token))
    }, [])

    const projectLevelTeamReducer = useSelector(state => {
        return state.projectLevelTeamReducer.teamMembersName.get;
    });

    const [teamMembersListNames, setteamMembersListNames] = useState([]);
    const [teamMembersListIds, setteamMembersListIds] = useState([]);

    const renderTeamMembersName = () => {
        if (projectLevelTeamReducer.loading) {
            return null
        } else if (projectLevelTeamReducer.success.ok) {
            let teamMembersListObjects = projectLevelTeamReducer.success.data
            let teamMembersListNames = []
            let teamMembersListIds = []
            teamMembersListObjects.map((i, index) => {
                teamMembersListNames.push(i.name)
                teamMembersListIds.push(i.id)
            })
            setteamMembersListNames(teamMembersListNames)
            setteamMembersListIds(teamMembersListIds)
            return teamMembersListObjects
        } else if (projectLevelTeamReducer.failure.error) {
            return message.error(projectLevelTeamReducer.failure.message)
        }
    }

    useEffect(() => {
        renderTeamMembersName()
    }, [projectLevelTeamReducer.success.ok])

    const handleAddClick = () => {
        if (id && roles.length > 0) {
            dispatch(actionCreators.postTeamMemberDetails(token, id, roles, project_id))
            setTimeout(() => changeReload(), 500)
            changeIsVisible(false)
        } else {
            Toast.show({
                text1: 'Please select both the fields!',
                type: 'info',
                autoHide: true,
                position: 'bottom',
                visibilityTime: 1000
            })
        }
    };

    return (
        <View style={{ backgroundColor: 'white', alignItems: 'center', }}>
            <TouchableOpacity
                onPress={() => changeIsVisible(false)}
            >
                <Divider style={{ marginVertical: hp('2%'), backgroundColor: 'rgb(143,143,143)', width: wp('18.6%'), height: hp('0.5%') }} />
            </TouchableOpacity>
            <Text style={styles.name_text}>Add Team</Text>
            <ModalDropdown
                options={teamMembersListNames}
                textStyle={{ fontSize: hp('2%'), width: wp('65%'), color: 'rgb(68, 67, 67)' }}
                dropdownStyle={{ width: '80%', height: hp('42%'), marginLeft: -9, elevation: 8, marginTop: hp('.8%') }}
                dropdownTextStyle={{ fontSize: hp('2%'), color: 'rgb(143, 143, 143)' }}
                onSelect={(index, value) => { setId(teamMembersListIds[index]) }}
                defaultValue={'Name'}
                dropdownTextHighlightStyle={{ backgroundColor: 'rgb(236, 243, 254)' }}
                showsVerticalScrollIndicator={true}
                style={{ width: wp('80%'), borderWidth: 1, borderColor: 'rgb(143, 143, 143)', borderRadius: 4, padding: 8, marginVertical: hp('1%') }}
                renderRightComponent={() => <Icon color={'rgb(68, 67, 67)'} type='feather' name={IconPosition ? 'chevron-up' : 'chevron-down'} />}
                defaultTextStyle={{ color: 'rgb(143, 143, 143)' }}
                onDropdownWillShow={() => setIconPosition(true)}
                onDropdownWillHide={() => setIconPosition(false)}
            />

            <App 
                roles={roles}
                setRoles={setRoles}
                items={roleOptionsDict}
            />
            {/* <View style={{ width: wp('80%'), color: 'rgb(143,143,143)', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', fontSize: 12 }}>
                <Select
                    style={{ width: wp('80%'), }}
                    multiSelect={true}
                    placeholder="Select Rights"
                    value="Select Rights"
                    selectedIndex={0}
                    onSelect={index => setRoles(
                        [...roles, roleOptions[index]]
                    )}
                >
                    {
                        roleOptions.map((i, index) =>
                            <SelectItem key={index} title={i} />
                        )
                    }
                </Select>
            </View> */}
            {/* <ModalDropdown
                options={roleOptions}
                textStyle={{ fontSize: hp('2%'), width: wp('65%'), color: 'rgb(68, 67, 67)' }}
                dropdownStyle={{ width: '80%', height: hp('42%'), marginLeft: -9, elevation: 8, marginTop: hp('.8%') }}
                dropdownTextStyle={{ fontSize: hp('2%'), color: 'rgb(143, 143, 143)' }}
                onSelect={(index, value) => setRoles(
                    [...roles, roleOptionsDict[value]]
                )}
                defaultValue={'Roles'}
                multipleSelect={true}
                dropdownTextHighlightStyle={{ backgroundColor: 'rgb(236, 243, 254)' }}
                showsVerticalScrollIndicator={true}
                style={{ width: wp('80%'), borderWidth: 1, borderColor: 'rgb(143, 143, 143)', borderRadius: 4, padding: 8, marginVertical: hp('1%') }}
                renderRightComponent={() => <Icon color={'rgb(68, 67, 67)'} type='feather' name={IconPosition ? 'chevron-up' : 'chevron-down'} />}
                defaultTextStyle={{ color: 'rgb(143, 143, 143)' }}
                onDropdownWillShow={() => setIconPosition(true)}
                onDropdownWillHide={() => setIconPosition(false)}
            /> */}
            <TouchableOpacity
                style={{ justifyContent: 'center', marginVertical: hp('2%'), borderRadius: 4, backgroundColor: '#3d87f1', alignItems: 'center', width: wp('80%'), height: hp('6%') }}
                onPress={() => handleAddClick()}
            >
                <Text style={styles.add_text}>Add</Text>
            </TouchableOpacity>

        </View >
    );
}

const styles = StyleSheet.create({
    name_text: {
        fontSize: hp('2.8%'),
        fontFamily: 'SourceSansPro-SemiBold',
        color: 'rgb(68, 67, 67)',
        textAlign: 'center'
    },

    select: {
        // color: 'rgb(232,232,232)',
        margin: 2,
        width: 160,
    },

    add_text: {
        fontSize: hp('2.8%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'white',
        textAlign: 'center'
    },

    email_text: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        textTransform: 'capitalize'
    },

    department_text: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-SemiBold',
        color: 'rgb(194, 194, 194)',
    },

    item_card: {
        shadowColor: 'rgba(0,0,0,0.4)',
        elevation: 8,
        borderLeftColor: '#3d87f1',
        backgroundColor: '#fff',
        borderRadius: 4,
        borderLeftWidth: 3,
        marginTop: hp('2%'),
        padding: '5%',
        paddingRight: '4%',
    },

    ellipsesCard: {
        position: 'absolute',
        marginLeft: (Dimensions.get('window').width) * 0.3,
        marginTop: hp('5%'),
        zIndex: 10,
        backgroundColor: 'white',
        width: wp('42%'),
        height: hp('19%'),
        borderWidth: 0,
        shadowColor: 'rgba(0,0,0,0.4)',
        elevation: 8,
        padding: '5%',
        borderRadius: 4,
    },

    ellipsesbtn: {
        marginVertical: 5,
    },

    ellipsestxt: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(143, 143, 143)',
    }
});

export default AddTeam;
