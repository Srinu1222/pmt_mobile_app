import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    Keyboard,
    ScrollView,
    Button,
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image';
import { useDispatch, useSelector } from 'react-redux';
import { SearchBar } from 'react-native-elements';
import { FlatList } from 'react-native-gesture-handler';
import EmptyScreenHeader from '../EmptyScreens/EmptyScreenHeader';
import Pagination from '../Pagination';
import { Icon } from 'react-native-elements'
import { Divider } from 'react-native-paper'
import DropDownPicker from 'react-native-dropdown-picker';


function TeamItem(props) {

    const item = props.item;

    let roleOptions = [
        {
            label: 'Sales',
            value: 'Sales'
        },
        {
            label: 'Project Owner',
            value: 'Project Owner'
        },
        {
            label: 'Project Manager',
            value: 'Project Manager'
        },
        {
            label: 'Customer',
            value: 'Customer'
        },
        {
            label: 'Liasoning Manager',
            value: 'Liasoning Manager'
        },

        {
            label: 'Liasoning Agent',
            value: 'Liasoning Agent'
        },

        {
            label: 'Engineering Head',
            value: 'Engineering Head'
        },

        {
            label: 'Designer',
            value: 'Designer'
        },

        {
            label: 'Lead Designer',
            value: 'Lead Designer'
        },

        {
            label: 'Procurement Manager',
            value: 'Procurement Manager',
        },
        {
            label: 'Procurement Executive',
            value: 'Procurement Executive',
        },
        {
            label: 'Logistics Manager',
            value: 'Logistics Manager',
        },
        {
            label: 'Logistics Executive',
            value: 'Logistics Executive',
        },
        {
            label: 'Construction manager',
            value: 'Construction manager',
        },

        {
            label: 'Safety Manager',
            value: 'Safety Manager',
        },

        {
            label: 'Site In-Charge',
            value: 'Site In-Charge',
        },

        {
            label: 'Site Executive',
            value: 'Site Executive',
        },

        {
            label: 'O&M Manager',
            value: 'O&M Manager',
        },

        {
            label: '"O&M Executive',
            value: '"O&M Executive',
        },
    ];
   
    const [value, setValue] = useState(item.role);
    const [open, setOpen] = useState(false);
    const [items, setItems] = useState(roleOptions)

    return (
        <View style={styles.item_card}>
            <Text style={styles.name_text}>{item.name}</Text>
            <Divider style={{ marginVertical: hp('1%') }} />
            <View style={{ marginVertical: hp('1%') }}>
                <Text style={styles.email_text}>Role</Text>
                <DropDownPicker
                    onOpen={() => { Keyboard.dismiss() }}
                    open={open}
                    value={value}
                    items={items}
                    setValue={setValue}
                    setItems={setItems}
                    setOpen={setOpen}
                    searchable={false}
                    placeholder="Category"
                    // placeholderTextColor='rgb(179, 179, 179)'
                    style={{
                        borderWidth: 1,
                        borderColor: 'rgb(232, 232, 232)',
                        width: wp('51.3%'),
                        height: hp('6.5%'),
                        borderRadius: 4,
                    }}
                    closeAfterSelecting={true}
                    textStyle={{
                        fontSize: value ? hp('2.1%') : hp('2.5%'),
                        fontFamily: 'SourceSansPro-Regular',
                        color: value ? 'black' : 'rgb(179, 179, 179)',
                    }}
                    containerStyle={{
                        marginBottom: Platform.OS === 'ios' ? 6 : 8,
                    }}

                    dropDownContainerStyle={{
                        backgroundColor: 'rgb(236, 243, 254)',
                        // borderWidth: 1,
                        borderColor: 'white',
                        width: wp('51.3%'),
                        borderRadius: 5,
                

                    }}

                    listItemContainerStyle={{
                        backgroundColor: 'white',
                        height: hp('6%')
                    }}

                    listItemLabelStyle={{
                        color: 'rgb(68, 67, 67)',
                        fontSize: hp('2%'),
                        fontFamily: 'SourceSansPro-Regular',
                    }}
                    itemSeparator={false}
                />
                {/* <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>
                    {item.role}
                </Text> */}
            </View>
            <View>
                <Text style={styles.email_text}>Additional Rights</Text>
                {/* <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>
                    {item.additional_rights}
                </Text> */}
                <DropDownPicker
                    onOpen={() => { Keyboard.dismiss() }}
                    open={open}
                    value={value}
                    items={items}
                    setValue={setValue}
                    setItems={setItems}
                    setOpen={setOpen}
                    searchable={false}
                    placeholder="Category"
                    // placeholderTextColor='rgb(179, 179, 179)'
                    style={{
                        borderWidth: 1,
                        borderColor: 'rgb(232, 232, 232)',
                        width: wp('51.3%'),
                        height: hp('6.5%'),
                        borderRadius: 4,
                    }}
                    closeAfterSelecting={true}
                    textStyle={{
                        fontSize: value ? hp('2.1%') : hp('2.5%'),
                        fontFamily: 'SourceSansPro-Regular',
                        color: value ? 'black' : 'rgb(179, 179, 179)',
                    }}
                    containerStyle={{
                        marginBottom: Platform.OS === 'ios' ? 6 : 8,
                    }}

                    dropDownContainerStyle={{
                        backgroundColor: 'rgb(236, 243, 254)',
                        // borderWidth: 1,
                        borderColor: 'white',
                        width: wp('51.3%'),
                        borderRadius: 5,
                

                    }}

                    listItemContainerStyle={{
                        backgroundColor: 'white',
                        height: hp('6%')
                    }}

                    listItemLabelStyle={{
                        color: 'rgb(68, 67, 67)',
                        fontSize: hp('2%'),
                        fontFamily: 'SourceSansPro-Regular',
                    }}
                    itemSeparator={false}
                />
            </View>
        </View>
    );
}


const styles = StyleSheet.create({
    name_text: {
        fontSize: hp('2.8%'),
        fontFamily: 'SourceSansPro-SemiBold',
        color: 'rgb(68, 67, 67)',
    },

    email_text: {
        fontSize: hp('1.8%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        textTransform: 'capitalize',
        fontWeight: 'bold',
        marginBottom: hp('1%')
    },

    department_text: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-SemiBold',
        color: 'rgb(194, 194, 194)',
    },

    item_card: {
        shadowColor: 'rgba(0,0,0,0.4)',
        elevation: 8,
        borderLeftColor: '#3d87f1',
        backgroundColor: '#fff',
        borderRadius: 4,
        borderLeftWidth: 3,
        marginTop: hp('2%'),
        padding: '5%',
        paddingRight: '4%',
    },
});

export default TeamItem;
