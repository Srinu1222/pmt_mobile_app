import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    Animated,
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Divider } from 'react-native-paper';
import ModalDropdown from 'react-native-modal-dropdown';
import { FAB, Icon, Input, BottomSheet } from 'react-native-elements';
import * as actionCreators from "../../actions/project-level-team-action/index";
import Toast from "react-native-toast-message";


const CreateNewRight = (params) => {

    const { changeNewRightPopUp, token, project_id, changeReload, clickedMemberId } = params

    const rightTypes = ['CAN VIEW', 'CAN WORK', 'CAN APPROVE']
    const rightTypesDict = {
        'CAN VIEW': 'can_view',
        'CAN WORK': 'can_work',
        'CAN APPROVE': 'can_approve'
    }

    const stageTypes = ['PRE-REQUISITES', 'APPROVALS', 'ENGINEERING', 'PROCUREMENT', 'MATERIAL HANDLING', 'CONSTRUCTION', 'SITE HAND OVER']
    const stageTypesDict = {
        'PRE-REQUISITES': 'PRE-REQUISITES',
        'APPROVALS': 'APPROVALS',
        'ENGINEERING': 'ENGINEERING',
        'PROCUREMENT': 'PROCUREMENT',
        'MATERIAL HANDLING': 'MATERIAL HANDLING',
        'CONSTRUCTION': 'CONSTRUCTION',
        'SITE HAND OVER': 'SITE HAND OVER'
    }

    const [rightType, setRightType] = useState("Right Type");
    const [stage, setStage] = useState("Stage");

    var [IconPosition, setIconPosition] = useState(false);
    const dispatch = useDispatch();

    const handleOkClick = () => {
        if (rightType && stage) {
            dispatch(actionCreators.createNewRight(token, rightType, stage, project_id, clickedMemberId))
            setTimeout(() => changeReload(), 500)
            changeNewRightPopUp(false)
        } else {
            Toast.show({
                text1: 'Please select both the fields!',
                type: 'info',
                autoHide: true,
                position: 'bottom',
                visibilityTime: 1000
            })
        }
    };

    return (
        <View style={{ backgroundColor: 'white', alignItems: 'center', height: Dimensions.get('window').height * 0.4 }}>
            <TouchableOpacity
                onPress={() => changeIsVisible(false)}
            >
                <Divider style={{ marginVertical: hp('2%'), backgroundColor: 'rgb(143,143,143)', width: wp('18.6%'), height: hp('0.5%') }} />
            </TouchableOpacity>
            <Text style={styles.name_text}>Create New Right</Text>
            <ModalDropdown
                options={rightTypes}
                textStyle={{ fontSize: hp('2%'), width: wp('65%'), color: 'rgb(68, 67, 67)' }}
                dropdownStyle={{ width: '80%', height: hp('20%'), marginLeft: -9, elevation: 8, marginTop: hp('.8%') }}
                dropdownTextStyle={{ fontSize: hp('2%'), color: 'rgb(143, 143, 143)' }}
                onSelect={(index, value) => {
                    setRightType(rightTypesDict[value])
                }}
                defaultValue={rightType}
                dropdownTextHighlightStyle={{ backgroundColor: 'rgb(236, 243, 254)' }}
                showsVerticalScrollIndicator={true}
                style={{ width: wp('80%'), borderWidth: 1, borderColor: 'rgb(143, 143, 143)', borderRadius: 4, padding: 8, marginVertical: hp('1%') }}
                renderRightComponent={() => <Icon color={'rgb(68, 67, 67)'} type='feather' name={IconPosition ? 'chevron-up' : 'chevron-down'} />}
                defaultTextStyle={{ color: 'rgb(143, 143, 143)' }}
                onDropdownWillShow={() => setIconPosition(true)}
                onDropdownWillHide={() => setIconPosition(false)}
            />

            <ModalDropdown
                options={stageTypes}
                textStyle={{ fontSize: hp('2%'), width: wp('65%'), color: 'rgb(68, 67, 67)' }}
                dropdownStyle={{ width: '80%', height: hp('40%'), marginLeft: -9, elevation: 8, marginTop: hp('0%') }}
                dropdownTextStyle={{ fontSize: hp('2%'), color: 'rgb(143, 143, 143)' }}
                onSelect={(index, value) => {
                    setStage(stageTypesDict[value])
                }}
                defaultValue={stage}
                dropdownTextHighlightStyle={{ backgroundColor: 'rgb(236, 243, 254)' }}
                showsVerticalScrollIndicator={true}
                style={{ width: wp('80%'), borderWidth: 1, borderColor: 'rgb(143, 143, 143)', borderRadius: 4, padding: 8, marginVertical: hp('1%') }}
                renderRightComponent={() => <Icon color={'rgb(68, 67, 67)'} type='feather' name={IconPosition ? 'chevron-up' : 'chevron-down'} />}
                defaultTextStyle={{ color: 'rgb(143, 143, 143)' }}
                onDropdownWillShow={() => setIconPosition(true)}
                onDropdownWillHide={() => setIconPosition(false)}
            />

            <View
                style={{ flexDirection: 'row', marginVertical: hp('1%'), justifyContent: 'space-between', width: wp('80%'), alignItems: 'center', }}
            >
                <TouchableOpacity
                    style={{ borderRadius: 4, backgroundColor: '#3d87f1', width: wp('35%'), justifyContent: 'center', height: hp('6%') }}
                    onPress={() => changeNewRightPopUp(false)}
                >
                    <Text style={styles.add_text}>Cancel</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={{ borderRadius: 4, backgroundColor: '#3d87f1', justifyContent: 'center', width: wp('35%'), height: hp('6%') }}
                    onPress={() => handleOkClick()}
                >
                    <Text style={styles.add_text}>Ok</Text>
                </TouchableOpacity>
            </View>


        </View>
    );
}

const styles = StyleSheet.create({
    name_text: {
        fontSize: hp('2.8%'),
        fontFamily: 'SourceSansPro-SemiBold',
        color: 'rgb(68, 67, 67)',
        textAlign: 'center'
    },

    add_text: {
        fontSize: hp('2.8%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'white',
        textAlign: 'center'
    },

    email_text: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        textTransform: 'capitalize'
    },

    department_text: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-SemiBold',
        color: 'rgb(194, 194, 194)',
    },

    item_card: {
        shadowColor: 'rgba(0,0,0,0.4)',
        elevation: 8,
        borderLeftColor: '#3d87f1',
        backgroundColor: '#fff',
        borderRadius: 4,
        borderLeftWidth: 3,
        marginTop: hp('2%'),
        padding: '5%',
        paddingRight: '4%',
    },

    ellipsesCard: {
        position: 'absolute',
        marginLeft: (Dimensions.get('window').width) * 0.3,
        marginTop: hp('5%'),
        zIndex: 10,
        backgroundColor: 'white',
        width: wp('42%'),
        height: hp('19%'),
        borderWidth: 0,
        shadowColor: 'rgba(0,0,0,0.4)',
        elevation: 8,
        padding: '5%',
        borderRadius: 4,
    },

    ellipsesbtn: {
        marginVertical: 5,
    },

    ellipsestxt: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(143, 143, 143)',
    }
});

export default CreateNewRight;
