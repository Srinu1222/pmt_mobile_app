import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    Button,
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image';
import { useDispatch, useSelector } from 'react-redux';
import { SearchBar } from 'react-native-elements';
import { FlatList } from 'react-native-gesture-handler';
import EmptyScreenHeader from '../EmptyScreens/EmptyScreenHeader';
import Pagination from '../Pagination';
import { FAB, Icon, Input, BottomSheet } from 'react-native-elements';
import CreateNewRight from './CreateNewRight';

function TeamItem(props) {
    const { item, token, project_id, changeReload } = props

    const [selectedId, setselectedId] = React.useState(null);
    const [isExpandable, setisExpandable] = React.useState(false);

    const changeExpandable = id => {
        setisExpandable(!isExpandable);
        setselectedId(id);
    };

    const [newRightPopUp, setNewRightPopUp] = useState(false);
    const changeNewRightPopUp = (value) => {
        setNewRightPopUp(value)
    }

    return (
        <View style={styles.item_card}>
            <Text style={styles.name_text}>{item.name}</Text>
            <Text style={styles.department_text}>Delhi</Text>
            <Text
                style={{
                    height: hp('0.2%'),
                    marginTop: 8,
                    marginBottom: 8,
                    backgroundColor: 'rgb(232, 232, 232)',
                }}></Text>
            <View
                style={{
                    display: 'flex',
                    marginBottom: 10,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                }}>
                <View>
                    <Text style={styles.email_text}>Role</Text>
                    <Text style={[styles.email_text, { width: wp('50%'), color: 'rgb(143, 143, 143)' }]}>
                        {item.role.join(' ,')}
                    </Text>
                </View>
                <View>
                    <Text style={styles.email_text}>Active Projects</Text>
                    <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>
                        {item.active_projects}
                    </Text>
                </View>
            </View>
            <View>
                <Text style={styles.email_text}>Additional Rights</Text>
                {
                    item.additional_rights != null ?
                        item.additional_rights.map((right, index) => (
                            <Text key={index} style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>
                                {right.type.split("_").join(" ")}{" ("}{right.stage}{")"}
                            </Text>
                        ))
                        :
                        <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>
                            Not Assigned
                        </Text>
                }
            </View>
            
            <BottomSheet
                isVisible={newRightPopUp}
                containerStyle={{ backgroundColor: 'rgba(0.5, 0.25, 0, 0.2)' }}
            >
                <CreateNewRight
                    changeNewRightPopUp={changeNewRightPopUp}
                    token={token}
                    project_id={project_id}
                    changeReload={changeReload}
                    clickedMemberId={item.id}
                />
            </BottomSheet>

            <TouchableOpacity
                onPress={() => changeNewRightPopUp(true)}
            >
                <Text style={[{ fontSize: hp('2%'), textDecorationLine: 'underline', color: '#3d87f1' }]}>Create New Right</Text>
            </TouchableOpacity>
            <Text
                style={{
                    height: hp('0.2%'),
                    marginTop: 8,
                    marginBottom: 8,
                    backgroundColor: 'rgb(232, 232, 232)',
                }} 
            />
            <TouchableOpacity
                onPress={() => {
                    changeExpandable(item.id);
                }}>
                <View style={{ display: 'flex', marginTop: hp('1%'), flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={[{ fontSize: hp('2%'), color: '#3d87f1' }]}>WORKING ON</Text>
                    {isExpandable ?
                        <Icon color={'#a1a1a1'} type='feather' name='chevron-up' />
                        :
                        <Icon color={'#a1a1a1'} type='feather' name='chevron-down' />
                    }
                </View>
            </TouchableOpacity>

            {
                isExpandable && item.id == selectedId
                &&
                <FlatList
                    data={item.currently_working_on}
                    keyExtractor={(item, index) => index}
                    ListEmptyComponent={
                        <View style={{ height: 50, justifyContent: 'center' }}>
                            <Text style={styles.email_text}>work not assigned</Text>
                        </View>
                    }
                    renderItem={
                        ({ item, index }) => (
                            <View style={{ paddingVertical: '4%' }}>
                                <Text
                                    style={[
                                        styles.email_text,
                                        { paddingTop: '1%', marginLeft: '3.5%' },
                                    ]}>
                                    {item}
                                </Text>
                            </View>
                        )
                    }
                />
            }
        </View>
    );
}

const styles = StyleSheet.create({
    name_text: {
        fontSize: hp('2.8%'),
        fontFamily: 'SourceSansPro-SemiBold',
        color: 'rgb(68, 67, 67)',
    },

    email_text: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        textTransform: 'capitalize'
    },

    department_text: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-SemiBold',
        color: 'rgb(194, 194, 194)',
    },

    item_card: {
        shadowColor: 'rgba(0,0,0,0.4)',
        elevation: 8,
        borderLeftColor: '#3d87f1',
        backgroundColor: '#fff',
        borderRadius: 4,
        borderLeftWidth: 3,
        marginTop: hp('2%'),
        padding: '5%',
        paddingRight: '4%',
    },
});

export default TeamItem;
