import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    Animated,
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image';
import { useDispatch, useSelector } from 'react-redux';
import * as actionCreator from '../../actions/team-action/index';
import { AuthContext } from '../../context';
import { SearchBar } from 'react-native-elements';
import { FlatList } from 'react-native-gesture-handler';
import EmptyScreenHeader from '../EmptyScreens/EmptyScreenHeader';
import Pagination from '../Pagination';
import TeamItem from './TeamItem';
import TeamMembers from '../EmptyScreens/TeamMembers';
import {
    useCollapsibleSubHeader,
    CollapsibleSubHeaderAnimator,
} from 'react-navigation-collapsible';
import { FAB, Icon, BottomSheet } from 'react-native-elements';
import { Divider } from 'react-native-paper';
import EmptyLoader from '../EmptyLoader'
import { Button } from 'react-native-paper'
import ManageTeam from './ManageTeam'
import Search from '../teams/SearchComponent'
import AddTeam from './AddTeam';


const TeamList = props => {
    const {
        onScroll /* Event handler */,
        containerPaddingTop /* number */,
        scrollIndicatorInsetTop /* number */,
        translateY,
    } = useCollapsibleSubHeader();

    const { token, changeReload, project_id } = props;

    // Bottom Component For Add Material
    const [isVisible, setIsVisible] = useState(false);
    const changeIsVisible = (value) => {
        setIsVisible(value)
    }

    // Search Functionality
    const [search, setSearch] = React.useState('');
    const SetSearch = (query) => {
        setSearch(query)
    }
    const filterItems = (items, filter) => {
        return items.filter(item => item.name.toLowerCase().includes(filter.toLowerCase()))
    }

    const data = props.team;

    const [activeButton, setactiveButton] = React.useState(1);
    const [isVisible1, setIsVisible1] = useState(false);
    const bottomList = ['Filter']
    
    const onPressFilter = (params) => {
        setIsVisible1(false)
        props.navigation.navigate(
          'team_filter',
          {
            'departmentList': props.roleList,
            'team':'project_team'
          }
        )
      }
    
      const onPressRemove = (params) => {
        setIsVisible1(false)
        props.navigation.navigate(
          'team_remove',
          {
            'data': data,
            'sub_contractor_member': false
          }
        )
      }
    
    const onPressOptions = params => {
        setIsVisible1(true)
      };
    const [isClickmanageTeam, setisClickmanageTeam] = useState(false);

    const newProject = params => { };

    const onPressReManageTeam = () => {
        setisClickmanageTeam(true)
    };
    const BottomComponent = (params) => {
        return (
          <View style={{ height: hp('100%') }}>
            <TouchableOpacity onPress={() => setIsVisible1(false)}>
              <View style={{ height: hp('86%') }}></View>
            </TouchableOpacity>
            <View style={{ backgroundColor: 'white', padding: '4%', height: hp('16%'), borderTopStartRadius: 10, borderTopRightRadius: 10, }}>
              {
                bottomList.map(
                  (item, index) => {
                    return (
                      <View key={index} >
                        <TouchableOpacity
                          onPress={() => item == 'Filter' ? onPressFilter() : onPressRemove()}
                        >
                          <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: hp('1%') }}>
                            <Icon
                              iconStyle={{ fontSize: hp('3%') }}
                              name={item == 'Filter' ? "funnel-outline" : "trash-outline"}
                              type='ionicon'
                              color='rgb(68, 67, 67)'
                            />
                            <Text style={styles.filterText}>{item}</Text>
                          </View>
    
                        </TouchableOpacity>
    
                        {index != bottomList.length - 1 && <Divider style={{ backgroundColor: 'rgb(232, 232, 232)' }} />}
                      </View>
                    );
                  }
                )
              }
            </View>
          </View>
        );
      }
    const Footer = () => (

        <View style={{
            flex: 0.8,
            justifyContent: 'center'
        }}>
            <View style={{
                backgroundColor: '#3d87f1',
                paddingHorizontal: wp('8%'),
                justifyContent: 'space-between',
                flexDirection: 'row',
                height: hp('7.4%'),
                alignItems: 'center',
            }}>
                <Button
                    style={{ width: wp('40%'), height: hp('5.5%'), borderWidth: 1, borderColor: 'white', backgroundColor: '#3d87f1' }}
                    labelStyle={{ color: 'white', textTransform: 'capitalize', fontSize: hp('1.6%') }}
                    mode="contained" onPress={() => changeIsVisible(true)}>
                    +Add Team
                </Button>
                <Button
                    style={{ width: wp('40%'), height: hp('5.5%'), borderWidth: 1, borderColor: 'white', backgroundColor: 'white' }}
                    labelStyle={{ color: '#3d87f1', textTransform: 'capitalize', fontSize: hp('1.6%') }}
                    mode="contained"
                    onPress={() => onPressReManageTeam()}>
                    Manage Team
                </Button>
            </View>
        </View>
    );

    const renderTeamList = () => {
        if (!isClickmanageTeam) {
            return (
                <View style={styles.container}>
                    <EmptyScreenHeader title='Team' navigation={props.navigation} />
                    <View style={{ flex: 10 }}>
                        <View
                            style={{
                                elevation: 8,
                                paddingTop: 15,
                                paddingLeft: wp('5%'),
                                paddingRight: wp('5%'),
                                width: '100%',
                                height: 'auto',
                                backgroundColor: '#fff',
                            }}>
                            <View
                                style={{
                                    display: 'flex',
                                    marginTop: hp('1%'),
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                }}>
                                <Search
                                    setSearch={SetSearch}
                                    search={search}
                                />
                                <TouchableOpacity
                                    onPress={() => {
                                        onPressOptions();
                                    }}
                                    style={{ alignSelf: 'center' }}>
                                    <FastImage
                                        style={{ width: wp('12%'), height: hp('4%') }}
                                        source={require('../../assets/icons/rectangle.png')}
                                        resizeMode={FastImage.resizeMode.contain}
                                    />
                                    <Text style={styles.option_text}>Options</Text>
                                </TouchableOpacity>
                            </View>
                            <BottomSheet
            isVisible={isVisible1}
            containerStyle={{ backgroundColor: 'rgba(0.5, 0.25, 0, 0.2)' }}
          >
            {BottomComponent()}
          </BottomSheet>
                            <View style={{ marginBottom: hp('2%'), marginTop: hp('2%') }}>
                                <Pagination
                                    activeButton={activeButton}
                                    setactiveButton={setactiveButton}
                                    reducerLength={data.length}
                                    style={{ paddingBottom: hp('5%') }}
                                />
                            </View>
                        </View>
                        <Animated.FlatList
                            //   ListEmptyComponent={<TeamMembers {...props}/>}
                            keyExtractor={(item, index) => index}
                            data={filterItems(data.slice(activeButton * 10 - 10, activeButton * 10), search)}
                            renderItem={({ item, index }) => <TeamItem
                                item={item}
                                token={token}
                                project_id={project_id}
                                changeReload={changeReload}
                            />}
                            onScroll={onScroll}
                            contentContainerStyle={{
                                paddingLeft: hp('3%'),
                                paddingRight: hp('3%'),
                                paddingBottom: hp('1%'),
                            }}
                            scrollIndicatorInsets={{ top: scrollIndicatorInsetTop }}
                        />
                    </View>
                    <Footer />
                    <BottomSheet
                        isVisible={isVisible}
                        containerStyle={{ backgroundColor: 'rgba(0.5, 0.25, 0, 0.2)' }}
                    >
                        <AddTeam
                            changeIsVisible={changeIsVisible}
                            token={token}
                            project_id={project_id}
                            changeReload={changeReload}
                        />
                    </BottomSheet>
                </View>
            );
        } else {
            return <ManageTeam setisClickmanageTeam={setisClickmanageTeam} data={data} />
        }
    };

    return renderTeamList();
};

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        flex: 1,
        backgroundColor: '#fff',
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
    filterText: {
        fontSize: hp('2.8%'),
        fontFamily: 'SourceSansPro-Semibold',
        color: 'rgb(68, 67, 67)',
        paddingLeft: wp('5%')
      },
    option_text: {
        fontSize: hp('1.2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center',
    },
});

export default TeamList;
