import React, { useEffect, useState } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  Button,
  View,
  processColor,
  ScrollView
} from 'react-native';
import GanttChart from 'react-native-gantt-chart';
import { SearchBar, Tooltip, Badge, Divider, BottomSheet } from 'react-native-elements';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import Tips from 'react-native-root-tips';
import Toast from 'react-native-toast-message'
import SideMenu from 'react-native-side-menu-updated'

const GanttChartView = ({task}) => {

  // const [tasks, settasks] = useState(props.route.params.task);

  useEffect(() => {

  }, []);
  // //"tasks",tasks)
  const Showtipss = (task) => {
    //task.start)
    Toast.show({ position: 'bottom' ,type: 'info', text1: task.name, text2: new Date(task.start).toDateString() + '-' + new Date(task.end).toDateString(), visibilityTime: 500 });
  }
  return (

    <ScrollView
      horizontal={true} showsHorizontalScrollIndicator={false}
    >
      <View style={{ width: wp('270%'), height: hp('98%') }}>


        <GanttChart
          data={task}
          numberOfTicks={12}
          rx={5}
          onPressTask={task => Showtipss(task)}
          colors={{
            barColorPrimary: '#0c2461',
            barColorSecondary: '#4a69bd',
            textColor: '#000',
            backgroundColor: '#fff'
          }}
        />
      </View>

    </ScrollView>

  );

}



export default GanttChartView;
