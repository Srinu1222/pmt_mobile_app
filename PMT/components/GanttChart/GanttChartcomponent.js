import React, { useEffect, useState, useRef } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  Button,
  View,
  Dimensions,
  ScrollView,
  TouchableOpacity,
  Alert,
  StatusBar
} from 'react-native';
import GanttChartView from './GanttChartView'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import * as actionCreators from '../../actions/gantt-stage-action/index';
import * as getStageEventActionCreators from '../../actions/gantt-stage-action/index';

import { useDispatch, useSelector } from 'react-redux';
import Orientation from 'react-native-orientation';
import moment from 'moment';
import GanttHeader from '../EmptyScreens/GanttHeader';
import FastImage from 'react-native-fast-image';
import Icons from "react-native-vector-icons/AntDesign";
import ProgressCircle from 'react-native-progress-circle'
import { Icon } from 'react-native-elements'
import { Avatar, Card, List, Divider, IconButton } from 'react-native-paper';

// import { OrientationLocker, PORTRAIT, LANDSCAPE } from "react-native-orientation-locker";
import { NavigationContainer } from '@react-navigation/native';
import EmptyLoaderPortraitView from '../EmptyLoaderPortraitView';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { Drawer, DrawerGroup, DrawerItem, Layout, IndexPath } from '@ui-kitten/components';
import notifyMessage from '../../ToastMessages';
import Accordion from './Accordion';
import DrawerCard from './DrawerCard';
import { useNavigationState, useNavigation } from '@react-navigation/native'    







const { Navigator, Screen } = createDrawerNavigator();
const Drawer1 = createDrawerNavigator();

const onPressDrawerOpen = (navigation) => {
  navigation.openDrawer();
}

const UsersScreen = (props) => {

  const routes = useNavigationState(state => state.routes)
  const currentRoute = routes[routes.length -1].name;
  //('UsersScreen currentRoute',currentRoute)


  const { approvals, construction, engineering, material_handling, pre_requisites, procurement, site_handover } = props.route.params.data;
  const stagesData1 = [
    {
      id: 0,
      backend_stage: pre_requisites['stage'],
      stage: 'Pre-Requisite',
      percentage: pre_requisites['percentage']
    },

    {
      id: 1,
      stage: 'Approvals',
      percentage: approvals['percentage'],
      backend_stage: approvals['stage'],
    },

    {
      id: 2,
      stage: 'Engineering',
      percentage: engineering['percentage'],
      backend_stage: engineering['stage'],
    },

    {
      id: 3,
      stage: 'Procurement',
      percentage: procurement['percentage'],
      backend_stage: procurement['stage'],
    },

    {
      id: 4,
      stage: 'Material Handeling',
      percentage: material_handling['percentage'],
      backend_stage: material_handling['stage'],
    },

    {
      id: 5,
      stage: 'Construction',
      percentage: construction['percentage'],
      backend_stage: construction['stage'],
    },

    {
      id: 6,
      stage: 'Handover',
      percentage: site_handover['percentage'],
      backend_stage: site_handover['stage'],
    },
  ]
  return (
    <View style={{ flexDirection: 'row' }}>
      <View style={{ flexDirection: 'row', backgroundColor: '#fff' }}>
        <View>
          <ScrollView showsVerticalScrollIndicator={false}>
            {stagesData1.map(
              (item, index) =>
                <View style={{backgroundColor: '#fff', flexDirection: "column", justifyContent: 'center', alignItems: 'center' }}>
                  <ProgressCircle
                    percent={item.percentage}
                    radius={25}
                    borderWidth={3}
                    color="#3d87f1"
                    shadowColor="#fff"
                    bgColor="#fff"
                  >
                    <Text style={{ fontSize: 10, color: '#000' }}>{parseFloat(item.percentage).toFixed(0)}%</Text>
                  </ProgressCircle>
                  <Text style={{ fontFamily: "SourceSansPro-Semibold", fontSize: 14, color: 'rgb(18,18,18)', fontWeight: '800' }}>{item.stage}</Text>
                </View>
            )

            }

          </ScrollView>
        </View>
        <View style={{ width: wp('5%'), justifyContent: 'center', alignItems: 'center' }}>
          <TouchableOpacity onPress={() => onPressDrawerOpen(props.navigation)}>
            <FastImage
              style={styles.image_logo}
              source={require('../../assets/icons/Right_Grey.png')
              }
              resizeMode={FastImage.resizeMode.contain}
            />
          </TouchableOpacity>
        </View>
      </View>
      <GanttChartView task={props.route.params.task} />


    </View>


  )
}



const OrdersScreen = () => (
  <Layout style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
    <Text category='h1'>ORDERS</Text>
  </Layout>
);
const DrawerContent = (props) => {
  const { approvals, construction, engineering, material_handling, pre_requisites, procurement, site_handover } = props.data;
  const token = props.token;
  const { project_id, is_sub_contractor } = props;
  const dispatch = useDispatch();
  const [selectedIndex, setSelectedIndex] = React.useState('');
  const [selectedstage, setSelectedstage] = React.useState('Pre-Requisite');

  const stagesData = [
    {
      id: 0,
      backend_stage: pre_requisites['stage'],
      stage: 'Pre-Requisite',
      percentage: pre_requisites['percentage']
    },

    {
      id: 1,
      stage: 'Approvals',
      percentage: approvals['percentage'],
      backend_stage: approvals['stage'],
    },

    {
      id: 2,
      stage: 'Engineering',
      percentage: engineering['percentage'],
      backend_stage: engineering['stage'],
    },

    {
      id: 3,
      stage: 'Procurement',
      percentage: procurement['percentage'],
      backend_stage: procurement['stage'],
    },

    {
      id: 4,
      stage: 'Material Handeling',
      percentage: material_handling['percentage'],
      backend_stage: material_handling['stage'],
    },

    {
      id: 5,
      stage: 'Construction',
      percentage: construction['percentage'],
      backend_stage: construction['stage'],
    },

    {
      id: 6,
      stage: 'Handover',
      percentage: site_handover['percentage'],
      backend_stage: site_handover['stage'],
    },
  ]
  const onPressDrawerClose = (navigation) => {
    navigation.closeDrawer();
  }

  const handlePress = (index, token, project_id) => {
    // //('token, project_id, backend_key_for_stage', stagesData[index.row].stage)
    dispatch(getStageEventActionCreators.getCustomEventMenuDetails(token, project_id, stagesData[index.row].backend_stage))
    setSelectedIndex(index.row)
    setSelectedstage(stagesData[index.row].stage)
    // setExpanded(!expanded)
  }

  const ganttEventReducer = useSelector(state => {
    return state.ganttEventMenuReducers.gantteventsmenu.get;
  })
  // //("ganttEventReducer data", selectedstage)

  return (
    <View style={{ marginTop: -40, marginLeft: -150, flexDirection: 'row', width: wp('100%'), height: wp('100%'), backgroundColor: '#fff' }}>
      <View style={{ borderWidth: 0 }}>
        <DrawerCard stages={stagesData} navigation={props.navigation} token={token} is_sub_contractor={is_sub_contractor} project_id={project_id} />
      </View>
      <View style={{width: wp('5%')
      , justifyContent:'center' }}>
        <TouchableOpacity onPress={() => onPressDrawerClose(props.navigation)}>
          <FastImage
            style={styles.image_logo1}
            source={require('../../assets/icons/Left_Grey.png')
            }
            resizeMode={FastImage.resizeMode.contain}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const GanttChartcomponent = ({ props, route, navigation }) => {
 
  const { token, project_id, is_sub_contractor } = route.params
  // const [tasks, settasks] = useState();
  const [check, setcheck] = useState(true);
  const [selectedIndex, setSelectedIndex] = React.useState(null);

  const dispatch = useDispatch();

  const [expanded, setExpanded] = React.useState(false);
  // const [preSelectedEventId, setpreSelectedEventId] = useState(0);
  const preSelectedEventId = useRef(0);

  // const ganttEventReducer = useSelector(state => {
  //     return state.ganttEventMenuReducers.gantteventsmenu.get;
  // })

  useEffect(() => {
    dispatch(actionCreators.ganttChartData(token, project_id));
    // Orientation.lockToPortrait();
    
    const unsubscribe = navigation.addListener('focus', () => {
      Orientation.lockToLandscape();
      StatusBar.setHidden(true);
    });
    return unsubscribe;


  }, []);

  useEffect(() => {
    dispatch(getStageEventActionCreators.getGanttEvents(token, project_id))
  }, [project_id]);

  const ganttEventReducer = useSelector(state => {

    return state.ganttEventReducers.ganttevents.get;
  })


  const ganttDataReducer = useSelector(state => {
    return state.ganttDataReducers.ganttdata.get;
  });

  const data = ganttDataReducer?.success?.data?.project_events_list.filter(index => index.start_time != "Not yet started").map((i) => {

    return (
      {
        id: `${i.status} ${i.stage}`,
        name: i.name,
        status: i.status,
        stage: i.stage,
        start: new Date(moment(i.start_time, 'DD-MM-YYYY').format('YYYY-MM-DD')),
        end: new Date(moment(i.completed_time, 'DD-MM-YYYY').format('YYYY-MM-DD'))
      }
    )

  })
  // console.log("GanttChartcomponent data",data)

  if (ganttDataReducer?.success?.ok && ganttEventReducer?.success?.ok) {
    return (
      <>
        <GanttHeader {...props} title='Team Details' no='3' percents={50} navigation={navigation} />

        <Navigator drawerContent={(props) =>
          <DrawerContent
            navigation={navigation}
            data={ganttEventReducer.success.data}
            token={token}
            is_sub_contractor={is_sub_contractor}
            project_id={project_id}
            {...props}
          />}
        >
          <Screen name='Users' initialParams={{ task: data, data: ganttEventReducer.success.data }} data={ganttEventReducer.success.data} component={UsersScreen} />
        </Navigator>




        {/* <GanttChartView task={data} />  */}


      </>


    );
  }
  else {
    return <EmptyLoaderPortraitView />;
  }
}

const styles = StyleSheet.create({


  image_logo1: {
    // color:'#000',
    width: wp("6.75%"),
    height: hp("6.75%"),
    borderRadius: hp("0.35%"), // Equivalent to borderRadius: 2.
  },
  image_logo: {
    // color:'#000',
    width: wp("6.75%"),
    height: hp("6.75%"),
    borderRadius: hp("0.35%"), // Equivalent to borderRadius: 2.
  },
  container: {
    backgroundColor: '#fff',
    paddingLeft: '2%',
    paddingTop: '2%',
    shadowColor: 'rgba(0,0,0,0.4)',
    elevation: 8,
    width: Dimensions.get('window').width * 0.3,
    borderWidth: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 4.3
  },

  ganttTxt: {
    fontFamily: 'SourceSansPro-Regular',
    fontSize: hp('3%'),
    color: 'rgb(68, 67, 67)'
  },

  labelStyle: {
    color: 'white',
    fontSize: hp('1.8%'),
    fontFamily: 'SourceSansPro-Regular',
  },

  titleTxt: {
    color: 'rgb(143,143,143)',
    fontSize: hp('1.5%'),
    fontFamily: 'SourceSansPro-Regular',
  },

  stagetitleTxt: {
    color: 'rgb(18, 18, 18)',
    fontSize: hp('2%'),
    fontFamily: 'SourceSansPro-Semibold'
  },

  valueTxt: {
    color: 'rgb(68, 67, 67)',
    fontSize: hp('1.8%'),
    fontFamily: 'SourceSansPro-Regular',
  },


});

export default GanttChartcomponent;

