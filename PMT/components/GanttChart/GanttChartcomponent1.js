import React, { useEffect, useState } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  Button,
  View,
  processColor,
  ScrollView,
  TouchableOpacity
} from 'react-native';
import GanttChart from 'react-native-gantt-chart';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import * as actionCreators from '../../actions/gantt-stage-action/index';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import GanttChartView from './GanttChartView'
import GanttHeader from '../EmptyScreens/GanttHeader';
import Orientation from 'react-native-orientation';
import ProgressCircle from 'react-native-progress-circle'
import FastImage from 'react-native-fast-image';
import Icons from "react-native-vector-icons/AntDesign";


// import { OrientationLocker, PORTRAIT, LANDSCAPE } from "react-native-orientation-locker";


const GanttChartcomponent1 = ({ props, route, navigation }) => {
  const token = route.params.token
  const [tasks, settasks] = useState();
  const [check, setcheck] = useState(true);



  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(actionCreators.ganttChartData(token));
    // Orientation.lockToPortrait();
    const unsubscribe = navigation.addListener('focus', () => {
      Orientation.lockToLandscape();


    });
    return unsubscribe;


  }, []);

  const ganttDataReducer = useSelector(state => {
    return state.ganttDataReducers.ganttdata.get;
  });

  let data = ganttDataReducer?.success?.data?.project_events_list.filter(index => index.start_time != "Not yet started").map((i) => {

    // //("date",i,"no ",i.start_time);
    return (
      {
        id: `${i.status} ${i.stage}`,
        name: i.name,
        status: i.status,
        stage: i.stage,
        start: new Date(moment(i.start_time, 'DD-MM-YYYY').format('YYYY-MM-DD')),
        end: new Date(moment(i.completed_time, 'DD-MM-YYYY').format('YYYY-MM-DD'))
      }
    )

  })
console.log("GanttChartcomponent1 data",data)


  return (

    <View>
      <GanttHeader {...props} title='Team Details' no='3' percents={50} />
      {/* <OrientationLocker
         orientation={LANDSCAPE}
         onChange={orientation => //('onChange', LANDSCAPE)}
         onDeviceChange={orientation => //('onDeviceChange', orientation)}
       /> */}
      <View style={{ flexDirection: "row" }}>
        {check ?
          <View style={{ width: hp('39%'), height: hp('45%'), flexDirection: "row" }}>
            <View style={{ width: hp('36%'), height: hp('45%'), flexDirection: "column" }}>
              <ScrollView>
                <View style={{ marginLeft: 5, width: hp('40%'), height: wp('15%'), backgroundColor: '#fff', flexDirection: "row", alignItems: 'center' }}>
                  <ProgressCircle
                    percent={70}
                    radius={20}
                    borderWidth={3}
                    color="#3d87f1"
                    shadowColor="#fff"
                    bgColor="#fff"
                  >
                    <Text style={{ fontSize: 10, color: '#000' }}>70%</Text>
                  </ProgressCircle>
                  <View style={{ width: hp('25.5%'), height: wp('15%'), marginLeft: 20, flexDirection: "row", alignItems: 'center', justifyContent: 'space-between' }}>
                    <Text style={{ fontFamily: "SourceSansPro-Semibold", fontSize: 14, color: 'rgb(18,18,18)', fontWeight: 'bold' }}>Pre-Requisite</Text>

                    <FastImage
                      style={styles.image_logo}
                      source={require('../../assets/icons/down.png')
                      }
                      resizeMode={FastImage.resizeMode.contain} />
                  </View>

                </View>

                <View style={{ marginLeft: 5, width: hp('40%'), height: wp('15%'), backgroundColor: '#fff', flexDirection: "row", alignItems: 'center' }}>
                  <ProgressCircle
                    percent={70}
                    radius={20}
                    borderWidth={3}
                    color="#3d87f1"
                    shadowColor="#fff"
                    bgColor="#fff"
                  >
                    <Text style={{ fontSize: 10, color: '#000' }}>70%</Text>
                  </ProgressCircle>
                  <View style={{ width: hp('25.5%'), height: wp('15%'), marginLeft: 20, flexDirection: "row", alignItems: 'center', justifyContent: 'space-between' }}>
                    <Text style={{ fontFamily: "SourceSansPro-Semibold", fontSize: 14, color: 'rgb(18,18,18)', fontWeight: 'bold' }}>Approvals</Text>

                    <FastImage
                      style={styles.image_logo}
                      source={require('../../assets/icons/down.png')
                      }
                      resizeMode={FastImage.resizeMode.contain} />
                  </View>

                </View>

                <View style={{ marginLeft: 5, width: hp('40%'), height: wp('15%'), backgroundColor: '#fff', flexDirection: "row", alignItems: 'center' }}>
                  <ProgressCircle
                    percent={70}
                    radius={20}
                    borderWidth={3}
                    color="#3d87f1"
                    shadowColor="#fff"
                    bgColor="#fff"
                  >
                    <Text style={{ fontSize: 10, color: '#000' }}>70%</Text>
                  </ProgressCircle>
                  <View style={{ width: hp('25.5%'), height: wp('15%'), marginLeft: 20, flexDirection: "row", alignItems: 'center', justifyContent: 'space-between' }}>
                    <Text style={{ fontFamily: "SourceSansPro-Semibold", fontSize: 14, color: 'rgb(18,18,18)', fontWeight: 'bold' }}>Engineering</Text>

                    <FastImage
                      style={styles.image_logo}
                      source={require('../../assets/icons/down.png')
                      }
                      resizeMode={FastImage.resizeMode.contain} />
                  </View>

                </View>


                <View style={{ marginLeft: 5, width: hp('40%'), height: wp('15%'), backgroundColor: '#fff', flexDirection: "row", alignItems: 'center' }}>
                  <ProgressCircle
                    percent={70}
                    radius={20}
                    borderWidth={3}
                    color="#3d87f1"
                    shadowColor="#fff"
                    bgColor="#fff"
                  >
                    <Text style={{ fontSize: 10, color: '#000' }}>70%</Text>
                  </ProgressCircle>
                  <View style={{ width: hp('25.5%'), height: wp('15%'), marginLeft: 20, flexDirection: "row", alignItems: 'center', justifyContent: 'space-between' }}>
                    <Text style={{ fontFamily: "SourceSansPro-Semibold", fontSize: 14, color: 'rgb(18,18,18)', fontWeight: 'bold' }}>Procurement</Text>

                    <FastImage
                      style={styles.image_logo}
                      source={require('../../assets/icons/down.png')
                      }
                      resizeMode={FastImage.resizeMode.contain} />
                  </View>

                </View>

                <View style={{ marginLeft: 5, width: hp('40%'), height: wp('15%'), backgroundColor: '#fff', flexDirection: "row", alignItems: 'center' }}>
                  <ProgressCircle
                    percent={70}
                    radius={20}
                    borderWidth={3}
                    color="#3d87f1"
                    shadowColor="#fff"
                    bgColor="#fff"
                  >
                    <Text style={{ fontSize: 10, color: '#000' }}>70%</Text>
                  </ProgressCircle>
                  <View style={{ width: hp('25.5%'), height: wp('15%'), marginLeft: 20, flexDirection: "row", alignItems: 'center', justifyContent: 'space-between' }}>
                    <Text style={{ fontFamily: "SourceSansPro-Semibold", fontSize: 14, color: 'rgb(18,18,18)', fontWeight: 'bold' }}>Material Handeling</Text>

                    <FastImage
                      style={styles.image_logo}
                      source={require('../../assets/icons/down.png')
                      }
                      resizeMode={FastImage.resizeMode.contain} />
                  </View>

                </View>

                <View style={{ marginLeft: 5, width: hp('40%'), height: wp('15%'), backgroundColor: '#fff', flexDirection: "row", alignItems: 'center' }}>
                  <ProgressCircle
                    percent={70}
                    radius={20}
                    borderWidth={3}
                    color="#3d87f1"
                    shadowColor="#fff"
                    bgColor="#fff"
                  >
                    <Text style={{ fontSize: 10, color: '#000' }}>70%</Text>
                  </ProgressCircle>
                  <View style={{ width: hp('25.5%'), height: wp('15%'), marginLeft: 20, flexDirection: "row", alignItems: 'center', justifyContent: 'space-between' }}>
                    <Text style={{ fontFamily: "SourceSansPro-Semibold", fontSize: 14, color: 'rgb(18,18,18)', fontWeight: 'bold' }}>Construction</Text>

                    <FastImage
                      style={styles.image_logo}
                      source={require('../../assets/icons/down.png')
                      }
                      resizeMode={FastImage.resizeMode.contain} />
                  </View>

                </View>


                <View style={{ marginLeft: 5, width: hp('40%'), height: wp('15%'), backgroundColor: '#fff', flexDirection: "row", alignItems: 'center' }}>
                  <ProgressCircle
                    percent={70}
                    radius={20}
                    borderWidth={3}
                    color="#3d87f1"
                    shadowColor="#fff"
                    bgColor="#fff"
                  >
                    <Text style={{ fontSize: 10, color: '#000' }}>70%</Text>
                  </ProgressCircle>
                  <View style={{ width: hp('25.5%'), height: wp('15%'), marginLeft: 20, flexDirection: "row", alignItems: 'center', justifyContent: 'space-between' }}>
                    <Text style={{ fontFamily: "SourceSansPro-Semibold", fontSize: 14, color: 'rgb(18,18,18)', fontWeight: 'bold' }}>Handover</Text>

                    <FastImage
                      style={styles.image_logo}
                      source={require('../../assets/icons/down.png')
                      }
                      resizeMode={FastImage.resizeMode.contain} />
                  </View>

                </View>


              </ScrollView>
            </View>
            <View style={{ width: hp('3%'), justifyContent: 'center', backgroundColor: '#fff' }}>
              <TouchableOpacity onPress={() => { setcheck(false) }}>

<View style={{backgroundColor:'rgb(194,194,194)',height:50,alignItems:'center',justifyContent:'center'}}>
<Icons
                  name="caretleft"
                  size={15}
                  color="rgb(143,143,143)"

                />
</View>
                
              </TouchableOpacity>

            </View>
          </View>
          :
          <View style={{ width: hp('18%'), height: hp('45%'), flexDirection: "row" }}>
            <View style={{ width: hp('15%'), height: hp('45%'), flexDirection: "column" }}>
              <ScrollView>
                <View style={{ marginLeft: 5, width: hp('15%'), height: wp('15%'), backgroundColor: '#fff', flexDirection: "column", justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={{ fontFamily: "SourceSansPro-Semibold", fontSize: 14, color: '#3d87f1', fontWeight: '800' }}>70%</Text>
                  <Text style={{ fontFamily: "SourceSansPro-Semibold", fontSize: 14, color: 'rgb(18,18,18)', fontWeight: '800' }}>Pre-Req...</Text>
                </View>
                <View style={{ marginLeft: 5, width: hp('15%'), height: wp('15%'), backgroundColor: '#fff', flexDirection: "column", justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={{ fontFamily: "SourceSansPro-Semibold", fontSize: 14, color: '#3d87f1', fontWeight: '800' }}>70%</Text>
                  <Text style={{ fontFamily: "SourceSansPro-Semibold", fontSize: 14, color: 'rgb(18,18,18)', fontWeight: '800' }}>Approvals</Text>
                </View>
                <View style={{ marginLeft: 5, width: hp('15%'), height: wp('15%'), backgroundColor: '#fff', flexDirection: "column", justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={{ fontFamily: "SourceSansPro-Semibold", fontSize: 14, color: '#3d87f1', fontWeight: '800' }}>70%</Text>
                  <Text style={{ fontFamily: "SourceSansPro-Semibold", fontSize: 14, color: 'rgb(18,18,18)', fontWeight: '800' }}>Engineer..</Text>
                </View>
                <View style={{ marginLeft: 5, width: hp('15%'), height: wp('15%'), backgroundColor: '#fff', flexDirection: "column", justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={{ fontFamily: "SourceSansPro-Semibold", fontSize: 14, color: '#3d87f1', fontWeight: '800' }}>70%</Text>
                  <Text style={{ fontFamily: "SourceSansPro-Semibold", fontSize: 14, color: 'rgb(18,18,18)', fontWeight: '800' }}>Procere...</Text>
                </View>
                <View style={{ marginLeft: 5, width: hp('15%'), height: wp('15%'), backgroundColor: '#fff', flexDirection: "column", justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={{ fontFamily: "SourceSansPro-Semibold", fontSize: 14, color: '#3d87f1', fontWeight: '800' }}>70%</Text>
                  <Text style={{ fontFamily: "SourceSansPro-Semibold", fontSize: 14, color: 'rgb(18,18,18)', fontWeight: '800' }}>Material...</Text>
                </View>
                <View style={{ marginLeft: 5, width: hp('15%'), height: wp('15%'), backgroundColor: '#fff', flexDirection: "column", justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={{ fontFamily: "SourceSansPro-Semibold", fontSize: 14, color: '#3d87f1', fontWeight: '800' }}>70%</Text>
                  <Text style={{ fontFamily: "SourceSansPro-Semibold", fontSize: 14, color: 'rgb(18,18,18)', fontWeight: '800' }}>Construc...</Text>
                </View>
                <View style={{ marginLeft: 5, width: hp('15%'), height: wp('15%'), backgroundColor: '#fff', flexDirection: "column", justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={{ fontFamily: "SourceSansPro-Semibold", fontSize: 14, color: '#3d87f1', fontWeight: '800' }}>70%</Text>
                  <Text style={{ fontFamily: "SourceSansPro-Semibold", fontSize: 14, color: 'rgb(18,18,18)', fontWeight: '800' }}>Handover</Text>
                </View>

              </ScrollView>
            </View>
            <View style={{ justifyContent: 'center', backgroundColor: '#fff' }}>
              <TouchableOpacity onPress={() => { setcheck(true) }}>


              <View style={{backgroundColor:'rgb(194,194,194)',height:50,alignItems:'center',justifyContent:'center'}}>
<Icons
                  name="caretright"
                  size={15}
                  color="rgb(143,143,143)"

                />
</View>
                

              </TouchableOpacity>
            </View>
          </View>
        }

        <GanttChartView task={data} />
      </View>
    </View>

  );

}

const styles = StyleSheet.create({



  image_logo: {
    // color:'#000',
    width: 35,
    height: 35,
    borderRadius: hp("0.35%"), // Equivalent to borderRadius: 2.
  },


});

export default GanttChartcomponent1;
