import React, { useEffect, useState, useRef } from 'react';
import {
  Switch,
  ScrollView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TouchableWithoutFeedback,
  AsyncStorage,
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import Collapsible from 'react-native-collapsible';
import Accordion from 'react-native-collapsible/Accordion';
import * as getStageEventActionCreators from '../../actions/gantt-stage-action/index';
import { useDispatch, useSelector } from 'react-redux';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import ProgressCircle from 'react-native-progress-circle';
import FastImage from 'react-native-fast-image';
import { ButtonGroup, FAB, Icon } from 'react-native-elements';
import SideEmptyLoader from '../SideEmptyLoader';



const DrawerCard = ({ stages, is_sub_contractor, navigation, project_id, token }) => {
  const dispatch = useDispatch();


  const [activeSections, setactiveSections] = useState([]);
  const [collapsed, setcollapsed] = useState(true);
  const [activeevent, setactiveevent] = useState();

  useEffect( async() => {
    if (activeSections != '') {
      dispatch(getStageEventActionCreators.getCustomEventMenuDetails(token, project_id, stages[activeSections].backend_stage))

    }

    userId = await AsyncStorage.getItem('activeevent')
    setactiveevent(userId)
  }, [activeSections]);

  const ganttEventReducer = useSelector(state => {
    return state.ganttEventMenuReducers.gantteventsmenu.get;
  })

  const setSections = (sections) => {
    setactiveSections(sections.includes(undefined) ? [] : sections)
  };

  renderHeader = (section, _, isActive) => {
    return (

      <View
        style={{ height: wp('15%'), marginLeft: 160, backgroundColor: '#fff', flexDirection: "row" }}
      >
        <ProgressCircle
          percent={section.percentage}
          radius={21}
          borderWidth={3}
          color="#3d87f1"
          shadowColor="#fff"
          bgColor="#fff"
        >
          <Text style={{ fontSize: 8, color: '#000' }}>{parseFloat(section.percentage).toFixed(0)}%</Text>
        </ProgressCircle>
        <View style={{ width: wp('45%'), height: wp('15%'), marginLeft: 15, flexDirection: "row", justifyContent: 'space-between' }}>
          <Text style={styles.headerText}>{section.stage}</Text>

          <FastImage
            style={styles.image_logo}
            source={isActive ? require('../../assets/icons/Up.png') : require('../../assets/icons/down.png')
            }
            resizeMode={FastImage.resizeMode.contain} />
        </View>
      </View>


      // <View

      //   style={[styles.header, isActive ? styles.active : styles.inactive]}
      //   transition="backgroundColor"
      // >
      //   <Text style={styles.headerText}>{section.stage}</Text>
      // </View> 

    );
  };

  renderContent = (section, _, isActive) => {



    //   //("isActive",isActive)
    // //('ganttEventReducer?.success?.ok', ganttEventReducer?.success?.ok)
    // //('ganttEventReducer?.success?.data?.stage_events_list', ganttEventReducer?.success?.data?.gantt_view_stage_events)

    const EventAction = async(project_id, item) => {
      AsyncStorage.setItem('activeevent', item.id.toString());
      navigation.navigate('DetailedEventPortraitView', {
        project_id: project_id,
        event_id: item.id,
        event_name: item.name,
        fromLandScape: true
      })
    };


    return (
      <View
        style={[styles.content, isActive ? styles.active : styles.inactive]}
        transition="backgroundColor"
      >
        {/* <Text animation={isActive ? 'bounceIn' : undefined}>
        {section.stage}
        </Text> */}

        {ganttEventReducer?.success?.ok ?
          ganttEventReducer.success.data.gantt_view_stage_events.map(
            (item, index) =>
              <View key={index} style={{ width: wp('45%'), marginLeft: wp('45%'), borderWidth: 0 }}>
                <TouchableWithoutFeedback
                  onPress={() => EventAction(project_id, item)
                    // setactiveevent(item.name),

                  }
                >
                  <View style={{ flexDirection: "row", justifyContent: 'space-between' }}>
                    {item.id==activeevent?

                <View style={{
                  marginTop:5,
                  width: 10,
                  height: 10,
                  borderRadius: 5,
                  backgroundColor: '#0db501',
                }}>
                  </View>
                  :
                  <View style={{
                    marginTop:5,
                    width: 10,
                    height: 10,
                    borderRadius: 5,
                    backgroundColor: '#e0e0e0',
                  }}>
                    </View>
                    }
                    
                    <Text>
                      {item.name.length > 18 ? item.name.substring(0, 18) + '...' : item.name}
                    </Text>
                    <Icon type='feather' name='chevron-right' color='#888' />

                  </View>


                </TouchableWithoutFeedback>
              </View>
          ) :
          <View style={{
            //   marginLeft: wp('42%'),
            //            width: wp('40%'),
            height: wp('40%'),
          }}>
            <SideEmptyLoader />
          </View>
        }
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Accordion
        activeSections={activeSections}
        sections={stages}
        touchableComponent={TouchableWithoutFeedback}
        renderHeader={renderHeader}
        renderContent={renderContent}
        duration={200}
        containerStyle={{ marginTop: '2%' }}
        onChange={setSections}
        renderAsFlatList={true}
      />
    </View>
  );

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: '#F5FCFF',
    paddingTop: 40,
    borderWidth: 0,
    width: wp('105%')
  },
  image_logo: {
    // color:'#000',
    width: wp("6.75%"),
    height: hp("6.75%"),
    borderRadius: hp("0.35%"), // Equivalent to borderRadius: 2.
  },
  title: {
    textAlign: 'center',
    fontSize: 22,
    fontWeight: '300',
    marginBottom: 20,
  },
  header: {
    // backgroundColor: '#F5FCFF',
    padding: 10,
  },
  headerText: {
    textAlign: 'center',
    fontSize: 16,
    fontWeight: '500',
    marginTop: 10
  },
  content: {
    marginLeft: wp('10%'),
    // backgroundColor: '#fff',
  },
  active: {
    // backgroundColor: 'rgba(255,255,255,1)',
  },
  inactive: {
    // backgroundColor: 'rgba(245,252,255,1)',
  },
  selectors: {
    marginBottom: 10,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  selector: {
    // backgroundColor: '#F5FCFF',
    padding: 10,
  },
  activeSelector: {
    fontWeight: 'bold',
  },
  selectTitle: {
    fontSize: 14,
    fontWeight: '500',
    padding: 10,
  },
  multipleToggle: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 30,
    alignItems: 'center',
  },
  multipleToggle__title: {
    fontSize: 16,
    marginRight: 8,
  },
});

export default DrawerCard;