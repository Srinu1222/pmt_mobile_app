import React,{useEffect, useState} from 'react'
import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    PermissionsAndroid,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    Picker,
    ListItem,
    AsyncStorage
} from 'react-native';
import { SearchBar,Tooltip, Badge,Divider, BottomSheet } from 'react-native-elements';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
// import { OrientationLocker, Orientation ,PORTRAIT, LANDSCAPE } from "react-native-orientation-locker";
import EmptyScreenHeader from './EmptyScreens/EmptyScreenHeader';
// import { SearchBar, Divider } from 'react-native-elements';
import Orientation from 'react-native-orientation';
import { useDispatch, useSelector } from 'react-redux';
import EmptyLoader from './EmptyLoader';
import * as getStageEventActionCreators from '../actions/gantt-stage-action/index';


const ProjectGanttChart = ({navigation,route}) => {

    const dispatch = useDispatch();
    const {project_id, is_sub_contractor} = route.params
    const [orientation, setOrientation] = useState("PORTRAIT");
  

    useEffect(() => {
        dispatch(getStageEventActionCreators.getGanttEvents(route.params.token, project_id))
        AsyncStorage.setItem('activeevent', "Document Collection");
    }, [ project_id]);

    const ganttEventReducer = useSelector(state => {
        return state.ganttEventReducers.ganttevents.get;
    })

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      //'PORTRAIT Refreshed!');
      StatusBar.setHidden(false);
      Orientation.lockToPortrait();
    });
    return unsubscribe;
  }, [navigation]);

//   useEffect(() => {
    
//     const initial = Orientation.getInitialOrientation();
//     if (initial === 'PORTRAIT') {
//       } else {
//       }
//       Orientation.addOrientationListener(_orientationDidChange);

//   }, []);

//   const _orientationDidChange = (orientation) => {
    // if (orientation === 'LANDSCAPE') {
    //   // do something with landscape layout
    // } else {
    //     console.log("/////////////////////////////////////LANDSCAPE")

    //   // do something with portrait layout
    // }
//   }
    const onsubmit=()=>{
        navigation.navigate('GanttChartcomponent', {project_id: project_id, is_sub_contractor: is_sub_contractor, data:ganttEventReducer.success.data})
    }

    return (
        <View>

            <EmptyScreenHeader title='Project Gantt Chart' navigation={navigation}/>
           {        ganttEventReducer.success.ok ?

                <SafeAreaView>
                <View style={styles.container1}>

                    <ScrollView>
                        <View style={styles.parent2}>
                          <View style={{alignItems:'center'}}>
                          <Divider color="#000" orientation="vertical" height={3} width={70} />

                          </View>

                        <FastImage
                                    style={{width: wp('30%'), height: hp('30%'),marginTop:30}}
                                    source={require('../assets/icons/Rotate.png')
                                    }
                                    resizeMode={FastImage.resizeMode.contain} /> 
                                    <View  style={{  width: wp('70%') }}>
                                    <Text style={{ fontFamily: 'SourceSansPro-Regular', fontSize: 20, color: 'rgb(194,194,194)' }}>Please rotate your device for 
                                    a better view of the gantt page.</Text>
                                    </View>
                                  
                                    <View  style={{  width: wp('70%'),alignItems:'center',marginTop:40 }}>
                                        <TouchableOpacity onPress={() => {onsubmit()}}>
                                    <Text style={{ fontFamily: 'SourceSansPro-Regular', fontSize: 20, textDecorationLine: 'underline', color: 'rgb(68,67,67)' }}>ROTATE</Text>
                                    </TouchableOpacity>
                                    </View>
                                    

                        </View>

                    </ScrollView>
                </View>
            </SafeAreaView>
            :
            <EmptyLoader/>}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
    container1: {
        width: Dimensions.get('window').width,
        height: hp('80%'),
        display: 'flex',
        marginTop: hp('20%'),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
        borderRadius:30
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
    shadowContainerStyle1: {   //<--- Style with elevation

        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 510 },
        shadowOpacity: 20,
        shadowRadius: 300,
        elevation: 3,
    },
    card: {   //<--- Style with elevation
        borderLeftColor: 'rgb(61,135,241)',
        borderLeftWidth: 5,
        borderRadius: 5,
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        elevation: 3,
        width: wp('90%'),
        margin: 10,
        height: 160,
        padding: 20
        //  justifyContent: 'center',
        //  alignItems:'center'
    },
    card1: {   //<--- Style with elevation

        borderRadius: 5,
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
        elevation: 3,
        width: wp('90%'),
        margin: 10,
        height: 170,
        padding: 20
        //  justifyContent: 'center',
        //  alignItems:'center'
    },

    image_logo: {
        width: wp('7%'),
        height: hp('3.5%'),
    },
    parent2: {
        // height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
        flexDirection: 'row',
        marginTop: hp('3%'),
        // justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        backgroundColor: 'white',
        flexDirection: 'column'
    },
    option_text: {
        fontSize: hp('1.2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center'
    },
    signIn: {
        width: wp('74%'),
        height: hp('8%'),
        left: 25,
        top: 80,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        color: '#05375a',
        backgroundColor: '#3A80E3',
        borderRadius: 5,
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: 'SourceSansPro-Regular',
    },
    name_text: {
        fontSize: 16,
        fontWeight: 'bold',
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68,67,67)'
    },
    temail_textxt: {
        fontSize: 14,
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(143,143,143)'
    },
    txt: {
        fontSize: 14,
        fontFamily: 'SourceSansPro-Semibold',
        color: 'rgb(68,67,67)'
    },
    txtunder: {
        fontSize: hp('2.5%'),
        textDecorationLine: 'underline',
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(61,135,241)'
    },
    textblue: {
        fontSize: 14,
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(61,135,241)'
    },
    textInput1: {
        marginTop: hp('30%'),
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        width: wp('79.4%'),
        height: hp('7.4%'),
        borderRadius: 4,
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'black',
        textAlign: 'left',
        paddingHorizontal: 10,
        marginBottom: Platform.OS === 'ios' ? 6 : 8
    },
    textInput: {
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        width: wp('79.4%'),
        height: hp('7.4%'),
        borderRadius: 4,
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'black',
        textAlign: 'left',
        paddingHorizontal: 10,
        marginBottom: Platform.OS === 'ios' ? 6 : 8
    },
    btnText: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular'
    },

    btn: {
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        width: wp('40%'),
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center'
    },

});

export default ProjectGanttChart
