import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TouchableOpacity,
    TextInput,
    KeyboardAvoidingView,
    ScrollView,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import { useDispatch, useSelector } from "react-redux";
import React, { useState, useMemo, useEffect } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import * as actionCreators from "../actions/daily-updates-action/index";


const Card = (props) => {

    const dispatch = useDispatch();


    const Total_height = Dimensions.get('window').height

    const onPressYes = (params) => {
        props.setisDisplayCard(false)
        if (props.type == 'dailyreport1') {
            props.navigation.navigate('dailyReports')
        } else if (props.type == 'dailyreport4') {
            const dailyUpdate = {
                projectId: props.data.project_id,
                todayTasks: props.data.todayTasks,
                tomorrowTasks: props.data.tomorrowTasks,
                imageAns: '',
            };
            const token = props.token
            dispatch(actionCreators.postDailyUpdate(token, dailyUpdate))
            props.navigation.navigate('dailyReportSubmit')
        }
    }

    const onPressNo = (params) => {
        props.setisDisplayCard(false)
    }

    return (
        <View style={{ position: 'absolute', zIndex: 100, backgroundColor: 'white', borderWidth: 0, borderRadius: 5, display: 'flex', justifyContent: 'center', alignItems: 'center', marginTop: Total_height * 0.8 / 2, width: wp('91.1%'), height: hp('19%') }}>
            <Text style={{ fontSize: hp('2.2%'), fontFamily: 'SourceSansPro-Regular', width: wp('60%'), color: 'rgb(68, 67, 67)' }}>{props.msg}</Text>
            <View style={{ display: 'flex', flexDirection: 'row' }}>

                <View>
                    <TouchableOpacity
                        onPress={() => onPressYes()}
                        style={[styles.btnStartNewProject]}
                    >
                        <Text style={[styles.btnText, {}]}>Yes</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity
                        onPress={() => onPressNo()}
                        style={[styles.btnStartNewProject, { borderColor: '#3d87f1', borderWidth: 1, backgroundColor: "white", }]}
                    >
                        <Text style={[styles.btnText, { color: '#3d87f1' }]}>No</Text>
                    </TouchableOpacity>
                </View>


            </View>
        </View>
    );
}


export default Card;

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        display: 'flex',
        alignItems: 'center',
        backgroundColor: "white",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
    containerHeader: {
        width: Dimensions.get('window').width * 0.9,
        height: Dimensions.get('window').height,
        flex: 0.2,
        flexDirection: 'row',
        borderBottomWidth: 3,
        borderBottomColor: 'rgb(232,232,232)',
        justifyContent: 'space-between',

    },

    titleText: {
        fontSize: hp('3.15%'),
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center',
        fontFamily: 'SourceSansPro-SemiBold',
    },

    cancelText: {
        fontSize: hp('2.4%'),
        color: '#3d87f1',
        fontFamily: 'SourceSansPro-Regular',
    },

    images: {
        alignSelf: 'center',
    },

    containerBody: {
        flex: 2,
        display: 'flex',
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        justifyContent: 'flex-end',
        // paddingHorizontal: '5%'
    },

    btnStartNewProject: {
        marginTop: hp("2%"),
        width: wp("35%"),
        height: hp("6%"),
        backgroundColor: "#3d87f1",
        borderRadius: 4,
        marginRight: wp('5%'),
        alignItems: "center",
        justifyContent: "center",
    },
    btnText: {
        color: "white",
        fontSize: hp("2.2%"),
        textAlign: "center",
        fontFamily: "SourceSansPro-Regular",
    },

});