import { View, Text } from 'react-native'
import React, { useEffect, useState } from "react";
import * as actionCreators from '../actions/project-level-team-action/index';
import { useDispatch, useSelector } from "react-redux";
import notifyMessage from '../ToastMessages';
import { parseSync } from '@babel/core';
import { heightPercentageToDP } from 'react-native-responsive-screen';
import EmptyLoader from '../components/EmptyLoader'
import config from '../config'
import TeamList from '../components/ProjectTeams/TeamList'

const ProjectTeam = (props) => {

    const dispatch = useDispatch();

    const {token, project_id} = props.route.params

    const [reload, setReload] = useState(false);
    const changeReload = () => {
      setReload(!reload)
    }
    
    useEffect(() => {
        dispatch(actionCreators.getProjectLevelTeam(token, project_id));
    }, [reload]);

    const projectLevelTeamReducer = useSelector(state => {
        return state.projectLevelTeamReducer.team.get;
    });

    const renderTeam = () => {
        if (projectLevelTeamReducer.success.ok) {

            let data = projectLevelTeamReducer.success.data

            // FILTER DATA TO DISPLAY DEPARTMENT
            const roleList = []
            data.map(
                (item, index) => {
                    if (!roleList.includes(item.role)) {
                        roleList.push(item.role[0])
                    }
                }
            )
            const Rolefilter = (item) => {
                return props.route.params?.selectedRoleList.includes(item.role[0]);
            }

            if (props.route.params?.isFilter && props.route.params?.selectedRoleList.length > 0) {
                data = data.filter(Rolefilter)
            }
            return <TeamList
                style={{ backgroundColor: '#ffffff' }}
                navigation={props.navigation}
                token={token}
                project_id={project_id}
                changeReload={changeReload}
                team={data}
                roleList={roleList}
            />;
        } else if (projectLevelTeamReducer.failure.error) {
            return notifyMessage('Something went wrong');
        } else {
            return <EmptyLoader />
        }
    };
    return renderTeam();
};

export default ProjectTeam;
