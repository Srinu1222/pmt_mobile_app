import { View, Text } from 'react-native'
import React, { useEffect, useState } from "react";
import * as actionCreators from '../actions/sub-contractor-action/index';
import { useDispatch, useSelector } from "react-redux";
import GetSubContractor from '../components/SubContractor/GetSubContractor'
import notifyMessage from '../ToastMessages';
import { parseSync } from '@babel/core';
import { heightPercentageToDP } from 'react-native-responsive-screen';
import EmptyLoader from './EmptyLoader';


const SubContractor = (props) => {

    const dispatch = useDispatch();

    // const isFocused = useIsFocused();

    const { route, navigation } = props

    const { token } = route.params

    const [reload, setReload] = useState(false);
    const changeReload = (params) => {
        setReload(!reload)
    }

    // useEffect(() => {
    //     dispatch(actionCreators.getSubContractors(token));
    // }, [reload]);

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            dispatch(actionCreators.getSubContractors(token));
        });
        return unsubscribe;
    }, [navigation, props]);

    const subContractorReducer = useSelector((state) => {
        return state.subContractorReducers.subContractor.get;
    });

    const renderSubContractor = () => {
        if (subContractorReducer.success.ok) {
            return <GetSubContractor changeReload={changeReload} token={token} navigation={navigation} subContractor={subContractorReducer.success.data.sub_contractors_list} />;
        } else if (subContractorReducer.failure.error) {
            return notifyMessage(subContractorReducer.failure.message);
        } else {
            return (<EmptyLoader />)
        }
    };
    return renderSubContractor();
};

export default SubContractor;
