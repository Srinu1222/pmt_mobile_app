import React, { useState, useMemo, useEffect } from 'react';
import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    PermissionsAndroid,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
} from 'react-native';
import { Searchbar, Button, Switch } from 'react-native-paper';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import * as actionCreator from '../actions/team-action/index';
import * as actionCreators from '../actions/sub-contractor-action/index';
import { useDispatch, useSelector } from 'react-redux';
import { FlatList } from 'react-native-gesture-handler';
import FastImage from 'react-native-fast-image'
import NewProjectHeader from './EmptyScreens/NewProjectHeader';
import DocumentPicker from 'react-native-document-picker';
import * as Animatable from 'react-native-animatable';
import notifyMessage from '../ToastMessages';
import ElevatedView from './ElevatedView';

const NewProject = ({ props, route, navigation }) => {
    const token = route.params.token
    const dispatch = useDispatch();
    const [Image, setImage] = useState("");
    const [Projectname, setProjectname] = useState("");
    const [Size, setSize] = useState("");
    const [Location, setLocation] = useState("");

    const [isValidName, setisValidName] = useState(true);
    const [isValidSize, setisValidSize] = useState(true);
    const [isValidLocation, setisValidLocation] = useState(true);


    

    useEffect(() => {
        dispatch(actionCreator.getTeams(token, true));
        dispatch(actionCreators.getProjectSubContractors(token));
    }, []);

    const TeamDetails = useSelector((state) => {
        return state.teamReducers.teams.get?.success?.data?.team_members_list;
    });
    const subContractorReducer = useSelector((state) => {
        return state.subContractorProjectReducers.subContractorProject.get?.success?.data?.sub_contractors_list;
    });
    
    const handleNext = () => {
         if (Image != '' && Projectname != '' && Size != '' && Location != '') {
            // if (true) {

        navigation.navigate('CustomerDetails', {
            project: {
                name:Projectname,
                size:Size,
                location:Location,
                score: 10,
                date:"06/09/2021"
            },
            image: Image,
            TeamDetails: TeamDetails,
            subContractorReducer: subContractorReducer
        })
    } else {
        notifyMessage("Please fill all the fields");
    }
    }
    const handlePickImage = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.allFiles],
                //   readContent:true,
                //   mode:"import"
            });
            setImage(res)
            // //res.uri);
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }

    }

    const handleName = val =>{
        if(val.trim().length >= 4){
            setProjectname(val)
            setisValidName(true)

        }else{
            setisValidName(false)
        }
    }
    const handleSize = val =>{
        if(val.trim().length != 0){
        setSize(val)
        setisValidSize(true)

        }else{
        setisValidSize(false)
        }
    }
    const handleLocation = val =>{
        if(val.trim().length != 0){
            setLocation(val)
            setisValidLocation(true)
    
            }else{
            setisValidLocation(false)
            }
    }
    return (
        <>
            <View style={styles.container}>
                <NewProjectHeader onCancel={navigation.goBack} {...props} title='Project Details' no='1' percents={16.66} />
                <ScrollView style={styles.parent2}>
                    <KeyboardAvoidingView behavior='position'>
                    <TouchableOpacity onPress={() => handlePickImage()}>
                        <View style={styles.textInput1}>
                            < FastImage
                                style={styles.image_logo}
                                source={require('../assets/icons/Image.png')
                                }
                                resizeMode={FastImage.resizeMode.contain}
                            />
                            <Text style={{ color: 'rgb(143,143,143)', fontSize: 16, marginLeft: 60 }}>Upload Image</Text>
                        </View>
                    </TouchableOpacity>
                    {Image =='' ? null : (
                        <Animatable.View animation="fadeInLeft" duration={500}>
                            <Text style={styles.errorMsg1}>{Image.name}.</Text>
                        </Animatable.View>
                    )}
                    <TextInput
                        placeholder="Project Name"
                        placeholderTextColor="rgb(143, 143, 143)"
                        style={styles.textInput}
                        onChangeText={val => handleName(val)}
                    />
                    {isValidName ? null : (
                        <Animatable.View animation="fadeInLeft" duration={500}>
                            <Text style={styles.errorMsg}>Enter a Valid Project Name.</Text>
                        </Animatable.View>
                    )}
                    <TextInput
                        placeholder="Size"
                        placeholderTextColor="rgb(143, 143, 143)"
                        style={styles.textInput}
                        keyboardType='numeric'
                        onChangeText={val => handleSize(val)}
                    />
                    {isValidSize ? null : (
                        <Animatable.View animation="fadeInLeft" duration={500}>
                            <Text style={styles.errorMsg}>Enter a Valid Size.</Text>
                        </Animatable.View>
                    )}
                    <TextInput
                        placeholder="Location"
                        placeholderTextColor="rgb(143, 143, 143)"
                        style={styles.textInput}
                        autoCapitalize="none"
                        onChangeText={val => handleLocation(val)}
                    />
                    {isValidLocation ? null : (
                        <Animatable.View animation="fadeInLeft" duration={500}>
                            <Text style={styles.errorMsg}>Enter a Valid Location.</Text>
                        </Animatable.View>
                    )}
                    </KeyboardAvoidingView>
                </ScrollView>
                <ElevatedView style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: hp('10%'),
                    backgroundColor: '#fff',
                    width: wp('100%'),
                }} elevation={8}>
                    <TouchableOpacity>
                        <View style={{ borderColor: '#abbafc', justifyContent: 'center', alignItems: 'center', borderWidth: 1, borderRadius: 5, height: hp('5'), width: wp('30%') }}>
                            <Text style={{ color: '#abbafc', fontFamily: 'SourceSansPro-Regular', fontSize: 18 }}>Previous</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => handleNext()}
                    >
                        <View style={{ backgroundColor: 'rgb(61,135,241)', marginLeft: 10, justifyContent: 'center', alignItems: 'center', borderRadius: 5, height: hp('5'), width: wp('30%') }}>
                            <Text style={{ color: '#fff', fontFamily: 'SourceSansPro-Regular', fontSize: 18 }}>Next</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Text style={{ textDecorationLine: 'underline', color: 'rgb(61,135,241)', marginLeft: 10, fontFamily: 'SourceSansPro-Regular', fontSize: 18 }}>Save for later</Text>
                    </TouchableOpacity>

                </ElevatedView>
            </View>

        </>

    )
};
const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
    },
    shadowContainerStyle: {   //<--- Style with elevation

        
    },
    image_logo: {
        width: wp('8%'),
        height: hp('5%'),
    },
    parent2: {
        flex: 1,
        backgroundColor: 'white',
        flexDirection: 'column'
    },
    errorMsg: {
        color: '#FF0000',
        fontSize: 14,
        marginBottom: hp('1%'),
        fontFamily: 'SourceSansPro-Regular',
    },
    errorMsg1: {
        color: '#000000',
        fontSize: 14,
        marginBottom: hp('1%'),
        fontFamily: 'SourceSansPro-Regular',
    },
    option_text: {
        fontSize: hp('1.2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center'
    },
    signIn: {
        width: wp('74%'),
        height: hp('8%'),
        left: 25,
        top: 80,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        color: '#05375a',
        backgroundColor: '#3A80E3',
        borderRadius: 5,
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: 'SourceSansPro-Regular',
    },
    txt: {
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(143,143,143)'
    },
    txtunder: {
        fontSize: hp('2.5%'),
        textDecorationLine: 'underline',
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(61,135,241)'
    },
    textblue: {
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(61,135,241)'
    },
    textInput1: {
        marginTop: hp('10%'),
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        width: wp('79.4%'),
        height: hp('7.4%'),
        borderRadius: 4,
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'black',
        textAlign: 'left',
        paddingHorizontal: 10,
        marginBottom: Platform.OS === 'ios' ? 6 : 8
    },
    textInput: {
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        width: wp('79.4%'),
        height: hp('7.4%'),
        borderRadius: 4,
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'black',
        textAlign: 'left',
        paddingHorizontal: 10,
        marginBottom: Platform.OS === 'ios' ? 6 : 8
    },
    btn: {
        height: hp('10%'),
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        width: wp('80%'),
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnText: {
        fontSize: hp('3%'),
        fontFamily: 'SourceSansPro-Regular'
    }

});
export default NewProject