import {
  View,
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Dimensions,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  Button,
} from 'react-native';
import React, { useState, useMemo, useRef, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image';
import { useDispatch, useSelector } from 'react-redux';
import { SearchBar, Icon } from 'react-native-elements';
import { FlatList } from 'react-native-gesture-handler';
import EmptyScreenHeader from '../EmptyScreens/EmptyScreenHeader';
import Pagination from '../Pagination';
import BottomSheet from "react-native-gesture-bottom-sheet";
// import Icon from "react-native-vector-icons/Ionicons";
import ElevatedView from '../ElevatedView';

function TeamItem(props) {
  const item = props.item;
  const bottomSheet = useRef();
  const bottomSheet1 = useRef();

  const [selectedId, setselectedId] = React.useState(null);
  const [isExpandable, setisExpandable] = React.useState(false);

  const changeExpandable = id => {
    setisExpandable(!isExpandable);
    setselectedId(id);
  };

  return (
    <ElevatedView key={item.id} elevation={8} style={styles.item_card}>
      <Text style={styles.name_text}>{item.name}</Text>
      <Text style={styles.department_text}>{item.department}</Text>
      <Text
        style={{
          height: hp('0.2%'),
          marginTop: 8,
          marginBottom: 8,
          backgroundColor: 'rgb(232, 232, 232)',
        }}></Text>
      <View
        style={{
          display: 'flex',
          marginBottom: 10,
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <View>
          <Text style={styles.email_text}>Email</Text>
          <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>
            {item.email}
          </Text>
        </View>
        <View>
          <Text style={styles.email_text}>Phone Number</Text>
          <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>
            {item.phone_number}
          </Text>
        </View>
      </View>
      <View>
        <Text style={styles.email_text}>Designation</Text>
        <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>
          {item.designation}
        </Text>
      </View>
      <Text
        style={{
          height: hp('0.2%'),
          marginTop: 8,
          marginBottom: 5,
          backgroundColor: 'rgb(232, 232, 232)',
        }}></Text>

      <View
        style={{
          display: 'flex',
          flexDirection: 'row',
          marginBottom: 10,
          justifyContent: 'space-between',
        }}>
        <TouchableOpacity onPress={() => { bottomSheet.current.show() }}>
          <View
            style={{
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <FastImage
              style={{ width: wp('5%'), height: hp('4%') }}
              source={require('../../assets/icons/info.png')}
              resizeMode={FastImage.resizeMode.contain}
            />
            <Text
              style={[
                styles.email_text,
                { marginLeft: 5, color: 'rgb(143, 143, 143)' },
              ]}>
              No.of Projects-{item.number_of_projects}
            </Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => { bottomSheet1.current.show() }}>

          <View
            style={{
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <FastImage
              style={{ width: wp('5%'), height: hp('4%') }}
              source={require('../../assets/icons/info.png')}
              resizeMode={FastImage.resizeMode.contain}
            />
            <Text
              style={[
                styles.email_text,
                { marginLeft: 5, color: 'rgb(143, 143, 143)' },
              ]}>
              Total Projects-{item.Total_projects}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
      <BottomSheet ref={bottomSheet} sheetBackgroundColor={'#ffffff'}>
        <View style={{height: 500}}>
          <FlatList
           scrollEnabled={true}
          data={item.total_project_list}
          ListHeaderComponent={ <View style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            margin: 30
          }}>
            <Text style={{ color: 'rgb(68,67,67)', fontFamily: 'SourceSansPro-Semibold', fontSize: 20 }}>No.of Projects</Text>
            <TouchableOpacity onPress={() => { bottomSheet.current.close() }}>
              <FastImage
                style={{ width: wp('4%'), height: hp('4%') }}
                source={require('../../assets/icons/crossbar.png')}
                resizeMode={FastImage.resizeMode.contain}
              />
            </TouchableOpacity>
            </View>}
            renderItem={(item) => <Text style={{fontSize: 64}}>232</Text>}
            keyExtractor={(item, index) => index}

            />
            </View>
      </BottomSheet>

      <BottomSheet ref={bottomSheet1} height={550} sheetBackgroundColor={'#ffffff'}>
        <View style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          margin: 30
        }}>
          <Text style={{ color: 'rgb(68,67,67)', fontFamily: 'SourceSansPro-Semibold', fontSize: 20 }}>Total Projects</Text>
          <TouchableOpacity onPress={() => { bottomSheet1.current.close() }}>
            <FastImage
              style={{ width: wp('4%'), height: hp('4%') }}
              source={require('../../assets/icons/crossbar.png')}
              resizeMode={FastImage.resizeMode.contain}
            />
          </TouchableOpacity>
        </View>
        <Text
          style={{
            height: hp('0.2%'),
            marginHorizontal:30,
            backgroundColor: 'rgb(232, 232, 232)',
          }}></Text>
          
        <SafeAreaView style={{marginTop:20}}>
        <ScrollView>
          {

            item.total_project_list.map((index) => (

              <Text style={{ color: 'rgb(68,67,67)', marginLeft: 30,marginVertical:15, fontFamily: 'SourceSansPro-Regular', fontSize: 16 }}>{index}</Text>

            ))
          }

       
</ScrollView>
</SafeAreaView>
      </BottomSheet>

      <TouchableOpacity
        onPress={() => {
          changeExpandable(item.id);
        }}>
        <View
          style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
          <Text style={[styles.buy_now_text, { fontSize: hp('1.8%') }]}>
            WORKING ON
          </Text>

          {isExpandable ?
            <Icon color={'#a1a1a1'} type='feather' name='chevron-up' />
            :
            <Icon color={'#a1a1a1'} type='feather' name='chevron-down' />
          }
        </View>
      </TouchableOpacity>

      {isExpandable && item.id == selectedId
        ? item.currently_working_on.map((i, index) => {
          return (
            <View style={{ paddingVertical: '4%' }} key={index}>
              <View style={{ display: 'flex', flexDirection: 'row' }}>
                <Text
                  style={{
                    width: 6,
                    height: 6,
                    alignSelf: 'center',
                    borderRadius: 6,
                    backgroundColor: '#3D87F1',
                  }}></Text>
                <Text
                  style={[
                    styles.name_text,
                    { fontSize: hp('2.5%'), marginLeft: wp('1.5%') },
                  ]}>
                  {i.project_name}
                </Text>
              </View>
              <Text
                style={[
                  styles.email_text,
                  { paddingTop: '1%', marginLeft: '3.5%' },
                ]}>
                {i.stage_name}
              </Text>
              <Text
                style={[
                  styles.email_text,
                  { paddingTop: '1%', marginLeft: '3.5%' },
                ]}>
                {i.event_name}
              </Text>
            </View>
          );
        })
        : null}
    </ElevatedView>
  );
}

const styles = StyleSheet.create({
  name_text: {
    fontSize: hp('2.8%'),
    fontFamily: 'SourceSansPro-SemiBold',
    color: 'rgb(68, 67, 67)',
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  email_text: {
    fontSize: hp('2%'),
    fontFamily: 'SourceSansPro-Regular',
    color: 'rgb(68, 67, 67)',
  },

  department_text: {
    fontSize: hp('2%'),
    fontFamily: 'SourceSansPro-SemiBold',
    color: 'rgb(194, 194, 194)',
  },

  item_card: {
    borderLeftColor: '#3d87f1',
    backgroundColor: '#fff',
    borderRadius: 4,
    borderLeftWidth: 3,
    marginTop: hp('2%'),
    padding: '5%',
    paddingRight: '4%',
  },
});

export default TeamItem;
