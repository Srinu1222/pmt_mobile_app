import {
  View,
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Dimensions,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  Animated,
  Button,
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image';
import { useDispatch, useSelector } from 'react-redux';
import * as actionCreator from '../../actions/team-action/index';
import { AuthContext } from '../../context';
import { FlatList } from 'react-native-gesture-handler';
import Pagination from '../Pagination';
import TeamItem from './TeamsItem';
import TeamMembers from '../EmptyScreens/TeamMembers';
import {
  useCollapsibleSubHeader,
  CollapsibleSubHeaderAnimator,
} from 'react-navigation-collapsible';
import { FAB, Icon, BottomSheet } from 'react-native-elements';
import { Divider } from 'react-native-paper';
import EmptyDashBoard from '../EmptyScreens/EmptyDashBoard';
import EmptyLoader from '../EmptyLoader'
import EmptyScreenHeader from '../EmptyScreens/EmptyScreenHeader'
import ElevatedView from '../ElevatedView';
import Search from './SearchComponent';


const GetTeams = props => {
  const {
    onScroll /* Event handler */,
    containerPaddingTop /* number */,
    scrollIndicatorInsetTop /* number */,
    translateY,
  } = useCollapsibleSubHeader();

  const { team: data, changeReload } = props;

  const [activeButton, setactiveButton] = React.useState(1);
  const [isVisible, setIsVisible] = useState(false);

  const onPressOptions = params => {
    setIsVisible(true)
  };

  // Bottom Components
  const bottomList = ['Filter', 'Remove']

  const onPressFilter = (params) => {
    setIsVisible(false)
    props.navigation.navigate(
      'team_filter',
      {
        'departmentList': props.departmentList,
        'team':'company_team'
      }
    )
  }

  const onPressRemove = (params) => {
    setIsVisible(false)
    props.navigation.navigate(
      'team_remove',
      {
        'data': data,
        'sub_contractor_member': false
      }
    )
  }

  const BottomComponent = (params) => {
    return (
      <View style={{ height: hp('100%') }}>
        <TouchableOpacity onPress={() => setIsVisible(false)}>
          <View style={{ height: hp('86%') }}></View>
        </TouchableOpacity>
        <View style={{ backgroundColor: 'white', padding: '4%', height: hp('16%'), borderTopStartRadius: 10, borderTopRightRadius: 10, }}>
          {
            bottomList.map(
              (item, index) => {
                return (
                  <View key={index} >
                    <TouchableOpacity
                      onPress={() => item == 'Filter' ? onPressFilter() : onPressRemove()}
                    >
                      <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: hp('1%') }}>
                        <Icon
                          iconStyle={{ fontSize: hp('3%') }}
                          name={item == 'Filter' ? "funnel-outline" : "trash-outline"}
                          type='ionicon'
                          color='rgb(68, 67, 67)'
                        />
                        <Text style={styles.filterText}>{item}</Text>
                      </View>

                    </TouchableOpacity>

                    {index != bottomList.length - 1 && <Divider style={{ backgroundColor: 'rgb(232, 232, 232)' }} />}
                  </View>
                );
              }
            )
          }
        </View>
      </View>
    );
  }

  // Search Functionality
  const [search, setSearch] = React.useState('');
  const SetSearch = (query) => {
    setSearch(query)
  }
  const filterItems = (items, filter) => {
    return items.filter(item => item.name.toLowerCase().includes(filter.toLowerCase()))
  }

  const renderGetTeams = (params) => {
    if (data.length == 0) {
      return <TeamMembers {...props} />
    } else {
      return (
        <SafeAreaView style={{ backgroundColor: '#fff', height: '100%' }}>
          <Animated.FlatList
            // ListEmptyComponent={<TeamMembers {...props} />}
            keyExtractor={(item, index) => index}
            data={filterItems(data.slice(activeButton * 10 - 10, activeButton * 10), search)}
            renderItem={({ item, index }) => <TeamItem item={item} />}
            onScroll={onScroll}
            contentContainerStyle={{
              paddingTop: containerPaddingTop,
              paddingLeft: hp('2%'),
              paddingRight: hp('2%'),
              paddingBottom: hp('8%'),
            }}
            scrollIndicatorInsets={{ top: scrollIndicatorInsetTop }}
          />
          <CollapsibleSubHeaderAnimator translateY={translateY}>
            <ElevatedView elevation={4}
              style={{
                paddingTop: 15, justifyContent: 'center', width: '100%', height: 'auto',
                backgroundColor: '#fff',
              }}
            >
              <View
                style={{
                  display: 'flex',
                  marginTop: hp('1%'),
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                }}
              >
                <Search
                  setSearch={SetSearch}
                  search={search}
                />
                <TouchableOpacity
                  onPress={() => {
                    onPressOptions();
                  }}
                  style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <FastImage
                    style={{ width: wp('6%'), height: wp('6%') }}
                    source={require('../../assets/icons/rectangle.png')}
                    resizeMode={FastImage.resizeMode.contain}
                  />
                  <Text style={styles.option_text}>Options</Text>
                </TouchableOpacity>
              </View>
              <View style={{ marginBottom: hp('2%'), marginTop: hp('2%'), margin: '3%' }}>
                <Pagination
                  activeButton={activeButton}
                  setactiveButton={setactiveButton}
                  reducerLength={data.length}
                  style={{ paddingBottom: hp('5%') }}
                /></View>
            </ElevatedView >
          </CollapsibleSubHeaderAnimator>
          <FAB
            size="large"
            placement="right"
            buttonStyle={{ backgroundColor: '#000000' }}
            icon={<Icon name="user-plus" color="white" type="feather" />}
            onPress={() => props.navigation.navigate('addTeam')}
          />
          <BottomSheet
            isVisible={isVisible}
            containerStyle={{ backgroundColor: 'rgba(0.5, 0.25, 0, 0.2)' }}
          >
            {BottomComponent()}
          </BottomSheet>
        </SafeAreaView >
      );


      // <View style={{ backgroundColor: '#fff' }}>
      //   <EmptyScreenHeader title={'Team'}/>
      //   <Animated.FlatList
      //     // ListEmptyComponent={<TeamMembers {...props} />}
      //     keyExtractor={(item, index) => index}
      //     data={data.slice(activeButton * 10 - 10, activeButton * 10)}
      //     renderItem={({ item, index }) => <TeamItem item={item} />}
      //     onScroll={onScroll}
      //     contentContainerStyle={{
      //       paddingTop: containerPaddingTop,
      //       paddingLeft: hp('3%'),
      //       paddingRight: hp('3%'),
      //       paddingBottom: hp('1%'),
      //     }}
      //     scrollIndicatorInsets={{ top: scrollIndicatorInsetTop }}
      //   />
      //   {/* <CollapsibleSubHeaderAnimator translateY={translateY}>
      //     <Header />
      //   </CollapsibleSubHeaderAnimator> */}
      //   <FAB
      //     size="large"
      //     placement="right"
      //     buttonStyle={{ backgroundColor: '#000000' }}
      //     icon={<Icon name="user-plus" color="white" type="feather" />}
      //     onPress={() => props.navigation.navigate('addTeam')}
      //   />
      //   {loading && <EmptyLoader />}
      //   <BottomSheet
      //     isVisible={isVisible}
      //     containerStyle={{ backgroundColor: 'rgba(0.5, 0.25, 0, 0.2)' }}
      //   >
      //     {BottomComponent()}
      //   </BottomSheet>
      // </View>


    }
  }

  return renderGetTeams();
};

const styles = StyleSheet.create({
  container: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },

  parent2: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    flex: 11,
    backgroundColor: 'white',
    padding: '5%',
  },

  filterText: {
    fontSize: hp('2.8%'),
    fontFamily: 'SourceSansPro-Semibold',
    color: 'rgb(68, 67, 67)',
    paddingLeft: wp('5%')
  },

  subscription_text: {
    fontSize: hp('1.8%'),
    fontFamily: 'SourceSansPro-Regular',
    color: 'rgb(194, 194, 194)',
  },

  option_text: {
    fontSize: hp('1.2%'),
    fontFamily: 'SourceSansPro-Regular',
    color: 'rgb(68, 67, 67)',
    alignSelf: 'center',
  },
});

export default GetTeams;
