import * as actionCreators from "../../actions/team-action/index";
import { useDispatch } from "react-redux";
import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TouchableOpacity,
    TextInput,
    KeyboardAvoidingView,
    ScrollView,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import LottieView from 'lottie-react-native';
import React, { useState, useMemo, useEffect } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Toast from "react-native-toast-message";
import * as Animatable from 'react-native-animatable';


const AddTeam = ({ route, navigation }) => {
    const [name, setName] = useState("");
    const [contactNum, setContactNum] = useState("");
    const [email, setEmail] = useState("");
    const [designation, setDesignation] = useState("");
    const [location, setLocation] = useState("");
    const [department, setDepartment] = useState("");

    const dispatch = useDispatch();

    const handleAddClick = () => {
        if (name && contactNum && email && designation && location && department) {
            let teamDetails = {
                name: name,
                number: contactNum,
                email: email,
                designation: designation,
                location: location,
                department: department,
            };
            Promise.resolve(dispatch(actionCreators.addTeam(teamDetails, route.params.token)))
                .then((res) => {
                    if (res?.status === 200) {
                        dispatch(actionCreators.getTeams(route.params.token));
                        Toast.show({
                            text1: 'Team member added successfully!',
                            position: 'bottom',
                            type: 'success'
                        })
                        navigation.goBack()
                    } else {
                        Toast.show({ text1: 'User already exists', type: 'info' })
                    }
                });
        } else {
            Toast.show({ text1: 'Please fill all the fields', type: 'error' })
        }
    };

    // VALIDATIONS
    const [isValidemail, setisValidEmail] = useState(true);
    const [isValidphoneNo, setisValidPhoneNo] = useState(true);
    const [isValidName, setisValidName] = useState(true);

    const handleEmail = val => {
        const isValid = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
            val,
        );
        if (isValid) {
            setEmail(val)
            setisValidEmail(true)
        } else {
            setisValidEmail(false)
        }
    }

    const handlePhoneNo = val => {
        const isValid1 = /^[98765]\d{9}$/.test(
            val,
        );
        if (isValid1) {
            setContactNum(val)
            setisValidPhoneNo(true)

        } else {
            setisValidPhoneNo(false)
        }
    }

    const handleName = val => {
        if (val.trim().length >= 4) {
            setName(val)
            setisValidName(true)
        } else {
            setisValidName(false)
        }
    }

    return (
        <>
            <KeyboardAvoidingView behavior='position' keyboardVerticalOffset={-175}>
                <View style={styles.container}>
                    <View style={styles.containerHeader}>
                        <Text style={styles.titleText}>Add Team</Text>
                        <TouchableOpacity
                            onPress={navigation.goBack}
                            style={styles.images}
                        >
                            < FastImage
                                style={styles.image_logo}
                                source={require('../../assets/icons/crossbar.png')
                                }
                                resizeMode={FastImage.resizeMode.contain}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.containerBody}>
                        <TextInput
                            placeholder="Name"
                            placeholderTextColor="rgb(143, 143, 143)"
                            style={styles.textInput}
                            autoCapitalize="none"
                            onChangeText={val => handleName(val)}
                        />

                        {isValidName ? null : (
                            <Animatable.View animation="fadeInLeft" duration={500}>
                                <Text style={styles.errorMsg}>Enter a Valid Name.</Text>
                            </Animatable.View>
                        )}

                        <TextInput
                            placeholder="Phone Number"
                            placeholderTextColor="rgb(143, 143, 143)"
                            style={styles.textInput}
                            autoCapitalize="none"
                            keyboardType="number-pad"
                            onChangeText={val => handlePhoneNo(val)}
                        />

                        {isValidphoneNo ? null : (
                            <Animatable.View animation="fadeInLeft" duration={500}>
                                <Text style={styles.errorMsg}>Enter a Valid PhoneNo.</Text>
                            </Animatable.View>
                        )}

                        <TextInput
                            placeholder="Email"
                            placeholderTextColor="rgb(143, 143, 143)"
                            style={styles.textInput}
                            autoCapitalize="none"
                            onChangeText={val => handleEmail(val)}
                        />
                        {isValidemail ? null : (
                            <Animatable.View animation="fadeInLeft" duration={500}>
                                <Text style={styles.errorMsg}>Enter a Valid Email.</Text>
                            </Animatable.View>
                        )}

                        <TextInput
                            placeholder="Designation"
                            placeholderTextColor="rgb(143, 143, 143)"
                            style={styles.textInput}
                            autoCapitalize="none"
                            onChangeText={val => setDesignation(val)}
                        />

                        <TextInput
                            placeholder="Location"
                            placeholderTextColor="rgb(143, 143, 143)"
                            style={styles.textInput}
                            autoCapitalize="none"
                            onChangeText={val => setLocation(val)}
                        />

                        <TextInput
                            placeholder="Department"
                            placeholderTextColor="rgb(143, 143, 143)"
                            style={styles.textInput}
                            autoCapitalize="none"
                            onChangeText={val => setDepartment(val)}
                        />

                        <TouchableOpacity
                            onPress={() => { handleAddClick() }}
                            style={styles.addbtn}>
                            <Text style={styles.btnText}>+Add</Text>
                        </TouchableOpacity>
                    </View>
                </View>

            </KeyboardAvoidingView>
        </>
    );
}

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        display: 'flex',
        alignItems: 'center',
        backgroundColor: "white",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
    containerHeader: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        flex: 1,
        flexDirection: 'row',
        borderBottomWidth: 3,
        borderBottomColor: 'rgb(232,232,232)',
        width: wp('90%'),
        justifyContent: 'space-between'
    },

    titleText: {
        fontSize: hp('3.15%'),
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center',
        fontFamily: 'SourceSansPro-SemiBold',
    },

    image_logo: {
        width: wp('4%'),
        height: hp('2%'),
    },

    images: {
        alignSelf: 'center'
    },

    containerBody: {
        flex: 11,
        display: 'flex',
        justifyContent: 'center'
    },

    textInput: {
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        width: wp('79.4%'),
        height: hp('7.4%'),
        borderRadius: 4,
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'black',
        textAlign: 'left',
        paddingHorizontal: 10,
        marginBottom: Platform.OS === 'ios' ? 6 : 8
    },

    addbtn: {
        width: wp('79.4%'),
        height: hp('7.4%'),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#3d87f1',
        borderRadius: 5,
        marginTop: 20,
    },

    btnText: {
        color: 'white',
        fontSize: hp('3%'),
        textAlign: 'center',
        fontFamily: 'SourceSansPro-Regular',
    },

    errorMsg: {

        color: '#FF0000',
        fontSize: 14,
        marginBottom: hp('1%'),
        fontFamily: 'SourceSansPro-Regular',
    },

});


export default AddTeam;
