import * as React from 'react';
import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    Animated,
    Button,
    Keyboard
} from 'react-native';
import { FAB, Icon, Input, BottomSheet } from 'react-native-elements';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';


const Search = (props) => {
    let dynamicProps = {}
    if (props.search != '') {
        dynamicProps = { rightIcon: <Icon color={'white'} type='feather' onPress={() => props.setSearch('')} size={20} name={'x'} /> }
    }
    return (
        <View style={styles.searchBoxContainer}>
            <Icon color={'#9B9B9B'} type='feather' onPress={() => Keyboard.dismiss()} style={{ marginTop: hp('0%') }} size={18} name={'search'} />
            <Input
                placeholder={'Search here...'}
                onChangeText={text => props.setSearch(text)}
                inputStyle={{ fontSize: hp('2.3%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                value={props.search}
                inputContainerStyle={{ borderBottomWidth: 0 }}
                rightIcon={
                    props.search!=''&&
                    <Icon color={'#9B9B9B'} type='feather' onPress={() => props.setSearch('')} size={20} name={'x'} />
                }
                containerStyle={{ width: wp('70%'), borderWidth: 0, height: hp('6.5%'), }}
            />
        </View>
    );
};

export default Search;


const styles = StyleSheet.create({
    searchBoxContainer: {
        paddingHorizontal: wp('2%'),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        backgroundColor: 'white',
        borderRadius: 4
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
});
