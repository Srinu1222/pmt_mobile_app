import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    Animated,
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image';
import { useDispatch, useSelector } from 'react-redux';
import { SearchBar } from 'react-native-elements';
import { FlatList } from 'react-native-gesture-handler';
import EmptyScreenHeader from '../EmptyScreens/EmptyScreenHeader';
import { ButtonGroup } from 'react-native-elements';
import {
    useCollapsibleSubHeader,
    CollapsibleSubHeaderAnimator,
} from 'react-navigation-collapsible';
import { FAB, Icon } from 'react-native-elements';
import { Avatar, Button, Paragraph, Dialog, Portal, Checkbox, Card, Title, Divider, IconButton } from 'react-native-paper';
import Icons from "react-native-vector-icons/Ionicons";
import Pagination from '../Pagination';
import notifyMessage from '../../ToastMessages';
import * as actionCreators from "../../actions/team-action/index";
// import {NavigationActions} from 'react-navigation';




const TeamRemove = props => {

    const [searchQuery, setSearchQuery] = React.useState('');

    const onChangeSearch = query => setSearchQuery(query);

    const [activeButton, setactiveButton] = React.useState(1);

    const { data, sub_contractor_member } = props.route.params

    const [selectedId, setselectedId] = React.useState(null);
    const [isExpandable, setisExpandable] = React.useState(false);

    const changeExpandable = (id, i) => {
        console.log("changeExpandable props", i)
        setisExpandable(!isExpandable);
        setselectedId(id);
    };

    const {
        onScroll /* Event handler */,
        containerPaddingTop /* number */,
        scrollIndicatorInsetTop /* number */,
        translateY,
    } = useCollapsibleSubHeader();

    const [selectedTeamCheckBoxList, setselectedTeamCheckBoxList] = React.useState([]);
    const onPressCheckBox = (item) => {
        if (selectedTeamCheckBoxList.includes(item)) {
            selectedTeamCheckBoxList.splice(selectedTeamCheckBoxList.indexOf(item), 1);
            setselectedTeamCheckBoxList([...selectedTeamCheckBoxList])
        } else {
            setselectedTeamCheckBoxList([...selectedTeamCheckBoxList, item])
        }
    }

    const FlatlistItem = (props) => {
        const { item, index } = props
        return (
            <View
                style={{ marginBottom: hp('2%') }}
            >{sub_contractor_member?

<Card
                    style={{ elevation: 5, backgroundColor: 'white', paddingRight: wp('3%'), shadowColor: 'rgba(0,0,0,0.4)' }}
                >
                    <Card.Title
                        titleStyle={styles.titleStyle}
                        title={item.member_name }
                        subtitle={item.department}
                        subtitleStyle={styles.subtitleStyle}
                        left={(props) =>
                            <Checkbox
                                color={'#3d87f1'}
                                uncheckedColor={'rgb(232, 232, 232)'}
                                status={
                                    selectedTeamCheckBoxList.includes(item.member_id)  ? 'checked' : 'unchecked'
                                }
                                onPress={() => onPressCheckBox(item.member_id)}
                            />
                        }
                        right={(props) =>
                            <Icon color={'rgb(68, 67, 67)'} onPress={() => changeExpandable(item.member_id)} type='feather' name={isExpandable && item.member_id == selectedId ? 'chevron-up' : 'chevron-down'} />
                        }
                    />
                    {isExpandable && item.member_id == selectedId ?
                        <Card.Content>
                            <Text
                                style={{
                                    height: hp('0.2%'),
                                    marginLeft: 55,
                                    marginTop: 8,
                                    marginBottom: 8,
                                    backgroundColor: 'rgb(232, 232, 232)',
                                }}></Text>
                            <View
                                style={{
                                    marginLeft: 55,
                                    marginTop: 10,
                                    display: 'flex',
                                    marginBottom: 10,
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                }}>
                                <View>
                                    <Text style={styles.email_text}>Designation</Text>
                                    <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>
                                        {item.department}
                                    </Text>
                                </View>
                                <View>
                                    <Text style={styles.email_text}>Phone</Text>
                                    <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>
                                        {item.phone_number}
                                    </Text>
                                </View>
                            </View>
                            <View style={{ marginLeft: 55, marginTop: 10 }}>
                                <Text style={styles.email_text}>Email</Text>
                                <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>
                                    {item.email}
                                </Text>
                            </View>
                        </Card.Content> :
                        <>
                        </>

                    }

                </Card>:


        
                <Card
                    style={{ elevation: 5, backgroundColor: 'white', paddingRight: wp('3%'), shadowColor: 'rgba(0,0,0,0.4)' }}
                >
                    <Card.Title
                        titleStyle={styles.titleStyle}
                        title={item.name}
                        subtitle={item.department}
                        subtitleStyle={styles.subtitleStyle}
                        left={(props) =>
                            <Checkbox
                                color={'#3d87f1'}
                                uncheckedColor={'rgb(232, 232, 232)'}
                                status={
                                    selectedTeamCheckBoxList.includes(item.id) ? 'checked' : 'unchecked'
                                }
                                onPress={() => onPressCheckBox(item.id)}
                            />
                        }
                        right={(props) =>
                            <Icon color={'rgb(68, 67, 67)'} onPress={() => changeExpandable(item.id)} type='feather' name={isExpandable && item.id == selectedId ? 'chevron-up' : 'chevron-down'} />
                        }
                    />
                    {isExpandable && item.id == selectedId ?
                        <Card.Content>
                            <Text
                                style={{
                                    height: hp('0.2%'),
                                    marginLeft: 55,
                                    marginTop: 8,
                                    marginBottom: 8,
                                    backgroundColor: 'rgb(232, 232, 232)',
                                }}></Text>
                            <View
                                style={{
                                    marginLeft: 55,
                                    marginTop: 10,
                                    display: 'flex',
                                    marginBottom: 10,
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                }}>
                                <View>
                                    <Text style={styles.email_text}>Designation</Text>
                                    <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>
                                        {item.designation}
                                    </Text>
                                </View>
                                <View>
                                    <Text style={styles.email_text}>Phone</Text>
                                    <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>
                                        {item.phone_number}
                                    </Text>
                                </View>
                            </View>
                            <View style={{ marginLeft: 55, marginTop: 10 }}>
                                <Text style={styles.email_text}>Email</Text>
                                <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>
                                    {item.email}
                                </Text>
                            </View>
                        </Card.Content> :
                        <>
                        </>

                    }

                </Card>
                    }
            </View>
        );
    }

    const Header = (params) => {
        return (
            <View style={{ height: hp('9%'), justifyContent: 'space-between', alignItems: 'center', padding: '5%', flexDirection: 'row' }}>
                <Text style={styles.option_text}>{selectedTeamCheckBoxList.length} Selected</Text>
                <TouchableOpacity
                    onPress={() => props.navigation.navigate(sub_contractor_member ? 'subContractor' : 'company_team')}
                >
                    <Text style={styles.cancelText}>Cancel</Text>
                </TouchableOpacity>
            </View>
        );
    }

    // Dialog Box
    const [visible, setVisible] = React.useState(false);
    const hideDialog = () => setVisible(false);
    const onPressDelete = (params) => {
        if (selectedTeamCheckBoxList.length > 0) {
            setVisible(true)
        } else {
            notifyMessage('Please Select the members to delete')
        }
    }

    const dispatch = useDispatch();
    const token = props.route.params.token
    const onPressYesBtn = (params) => {
        setVisible(false)
        dispatch(actionCreators.removeTeamMembers(token, selectedTeamCheckBoxList, sub_contractor_member))
        if (sub_contractor_member) {
            props.navigation.navigate('subContractor')
        } else {
            props.navigation.navigate('company_team')
        }
    }

    const DialogComponent = (params) => {
        return (
            <View style={{ width: wp('91.1%'), alignSelf: 'center', borderRadius: 4, backgroundColor: 'white', height: hp('20%') }}>
                <Text style={{ alignSelf: 'center', paddingVertical: hp('2.5%'), color: 'rgb(68, 67, 67)', fontSize: hp('2.3%'), fontFamily: 'SourceSansPro-Regular' }}>Are you sure you want to remove{'\n'}
                    all the members from the team?</Text>
                <View
                    style={{ flexDirection: 'row', justifyContent: 'space-around' }}
                >
                    <Button
                        style={{ backgroundColor: '#3d87f1', width: wp('38.1%'), height: hp('5.3%'), justifyContent: 'center' }}
                        onPress={() => onPressYesBtn()}
                        labelStyle={{ color: 'white' }}
                    >
                        yes
                    </Button>
                    <Button
                        style={{ backgroundColor: 'white', borderColor: '#3d87f1', borderWidth: 1.5, width: wp('38.1%'), height: hp('5.3%'), justifyContent: 'center' }}
                        onPress={() => setVisible(false)}
                        labelStyle={{ color: '#3d87f1' }}
                    >
                        No
                    </Button>
                </View>
            </View>
        );
    }

    return (
        <View style={styles.container}>
            {visible && <Portal>
                <Dialog visible={visible} onDismiss={hideDialog}>
                    <DialogComponent />
                </Dialog>
            </Portal>}
            <Header />
            <Divider style={{ marginHorizontal: '5%', color: 'red', marginTop: '0%' }} />
            <View
                style={{ padding: '5%' }}
            >
                <View style={{ flexDirection: 'row', paddingBottom: '2%', alignItems: 'center', justifyContent: 'space-between' }}>
                    <SearchBar
                        showLoading={false}
                        platform={Platform.OS}
                        containerStyle={{
                            borderWidth: 1,
                            borderRadius: 4,
                            borderColor: 'rgb(232, 232, 232)',
                            backgroundColor: 'white',
                            justifyContent: 'center',
                            width: wp('73.3%'),
                            height: hp('5.3%'),
                        }}
                        clearIcon={true}
                        onChangeText={onChangeSearch}
                        onClearText={() => null}
                        placeholder=""
                        cancelButtonTitle="Cancel"
                    />
                    <Icon
                        iconStyle={{ fontSize: hp('4%') }}
                        name="trash-outline"
                        type='ionicon'
                        color='rgb(68, 67, 67)'
                        onPress={() => { onPressDelete() }}
                    />
                </View>

                <Pagination
                    activeButton={activeButton}
                    setactiveButton={setactiveButton}
                    reducerLength={data.length}
                    style={{ paddingBottom: hp('5%') }}
                />

                <View style={{ height: hp('80%'), paddingTop: '5%' }}>
                    <Animated.FlatList
                        keyExtractor={(item, index) => index}
                        data={data}
                        renderItem={
                            ({ item, index }) => <FlatlistItem index={index} item={item} />}
                        onScroll={onScroll}
                        contentContainerStyle={{
                            paddingBottom: hp('2%'),
                        }}
                        scrollIndicatorInsets={{ top: scrollIndicatorInsetTop }}
                    />
                </View>
            </View>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: '#fff',
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    titleStyle: {
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
    },

    subtitleStyle: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(143, 143, 143)',
    },

    option_text: {
        fontSize: hp('3.2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center',
    },

    cancelText: {
        fontFamily: 'SourceSansPro-Regular',
        fontSize: hp('2.5%'),
        color: '#3d87f1'
    },
    email_text: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
    },
});

export default TeamRemove;
