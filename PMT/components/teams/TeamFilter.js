import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    Animated,
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image';
import { useDispatch, useSelector } from 'react-redux';
import { SearchBar } from 'react-native-elements';
import { FlatList } from 'react-native-gesture-handler';
import EmptyScreenHeader from '../EmptyScreens/EmptyScreenHeader';
import { ButtonGroup } from 'react-native-elements';
import {
    useCollapsibleSubHeader,
    CollapsibleSubHeaderAnimator,
} from 'react-navigation-collapsible';
import { FAB, Icon } from 'react-native-elements';
import { Avatar, Button, Checkbox, Card, Divider, IconButton } from 'react-native-paper';
import Icons from "react-native-vector-icons/Ionicons";


const TeamFilter = props => {

const teamtype =props.route.params.team
    const {
        onScroll /* Event handler */,
        containerPaddingTop /* number */,
        scrollIndicatorInsetTop /* number */,
        translateY,
    } = useCollapsibleSubHeader();

    const [selectedDepartmentCheckBoxList, setselectedDepartmentCheckBoxList] = React.useState([]);
    const onPressDepartmentCheckBox = (item) => {
        if (selectedDepartmentCheckBoxList.includes(item)) {
            selectedDepartmentCheckBoxList.splice(selectedDepartmentCheckBoxList.indexOf(item), 1);
            setselectedDepartmentCheckBoxList([...selectedDepartmentCheckBoxList])
        } else {
            setselectedDepartmentCheckBoxList([...selectedDepartmentCheckBoxList, item])
        }
    }

    const FlatlistItem = (props) => {
        const { item, index } = props
        return (
            <View>
                <TouchableOpacity
                    onPress={() => onPressDepartmentCheckBox(item)}
                >
                    <View
                        style={{ flexDirection: 'row', paddingHorizontal: wp('8%'), paddingVertical: hp('1.5%'), justifyContent: 'flex-start' }}
                        key={props.index}
                    >
                        <Checkbox
                            color={'#3d87f1'}
                            uncheckedColor={'#3d87f1'}
                            status={
                                selectedDepartmentCheckBoxList.includes(item) ? 'checked' : 'unchecked'
                            }
                        />
                        <Text style={styles.text}>{item}</Text>
                    </View>
                </TouchableOpacity>
                <Divider style={{backgroundColor: 'rgb(232,232,232)'}}/>
            </View>
        );
    }

    const departmentList = props.route.params.departmentList

    return (
        <View style={styles.container}>
            <View style={{ height: hp('9%'), justifyContent: 'space-between', padding: '5%', flexDirection: 'row' }}>
                <Text style={styles.option_text}>Filter by Department</Text>
                <Icon
                    iconStyle={{ fontSize: hp('4.5%') }}
                    name='close-outline'
                    type='ionicon'
                    color='rgb(68, 67, 67)'
                    onPress={
                        () =>{teamtype=='company_team'? props.navigation.navigate(
                            'team',
                            {
                                'isFilter': true,
                                'selectedDepartmentList': selectedDepartmentCheckBoxList
                            }
                        ):props.navigation.navigate(
                            'projectTeam',
                            {
                                'isFilter': true,
                                'selectedRoleList': selectedDepartmentCheckBoxList
                            }
                        )}
                    }
                />
            </View>

            < Divider style={{ marginHorizontal: '5%', marginTop: '0%' }} />
            <View>
                <Animated.FlatList
                    // ListEmptyComponent={<EmptyMaterial />}
                    keyExtractor={(item, index) => index}
                    data={departmentList}
                    renderItem={
                        ({ item, index }) => <FlatlistItem index={index} item={item} />}
                    onScroll={onScroll}
                    contentContainerStyle={{
                        paddingBottom: hp('2%'),
                    }}
                    scrollIndicatorInsets={{ top: scrollIndicatorInsetTop }}
                />
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: '#fff',
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    option_text: {
        fontSize: hp('3%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center',
    },

    text: {
        fontSize: hp('2.3%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center',
        textTransform: 'capitalize',
        paddingLeft: wp('5%')
    },

    textMessage: {
        fontFamily: 'SourceSansPro-Regular',
        fontSize: hp('2%'),
        color: '#3d87f1'
    },
});

export default TeamFilter;
