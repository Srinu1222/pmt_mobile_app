import React, { useEffect, useState, useRef } from 'react';
import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    Keyboard
} from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import Icon from 'react-native-vector-icons/Feather';

const SignUpBasicInfo = (params) => {

    const initial_states = {
        'companyName': '',
        'website': '',
        'headOffice': '',
        'regionalOffice': '',
        'companyBrief': '',
        'annualTurnOver': 0,
        'employerStrength': 0,
        'installedCapacity': 0,
        'minimumProjectSize': 0,
        'largestProject': 0,
        'businessModel': '',
        'statesActive': '',
        'flagShipProjects': ''
    }

    const [data, setData] = React.useState(initial_states);

    const textInputChange = (key, val) => {
        setData({
            ...data,
            key: val,
        });

    }

    const category1 = {
        'companyName': 'Company Name',
        'website': 'Website',
        'headOffice': 'Head Office',
        'regionalOffice': 'Regional Office',
        'companyBrief': 'Company Brief'
    }

    const category2 = {
        'annualTurnOver': 'Annual Turnover',
        'employerStrength': 'Employer Strength'
    }

    const category3 = {
        'installedCapacity': 'Installed Capacity(MW)',
        'minimumProjectSize': 'Minimum Project Size (KW)',
        'largestProject': 'Largest Project(MW)',
        'businessModel': 'Business Model',
        'statesActive': 'States Active',
        'flagShipProjects': 'FlagShip Projects'
    }

    return (
        <View style={styles.parent2}>
            <Text style={styles.text}>CATEGORY1</Text>
            {
                Object.entries(category1)
                    .map(([key, value]) => <TextInput
                        key={key}
                        placeholder={value}
                        placeholderTextColor="white"
                        style={styles.textInput}
                        autoCapitalize="none"
                        onChangeText={(val) => textInputChange(key, val)}
                    />)
            }

            <Text style={[styles.text, { top: 40 }]}>CATEGORY2</Text>
        
            {
                Object.entries(category2)
                    .map(([key, value]) => <DropDownPicker
                        items={[
                            { label: 'annualTurnOver', value: '10', icon: () => <Icon name="flag" size={18} color="#900" />, hidden: true },
                            { label: 'employerStrength', value: '20', icon: () => <Icon name="flag" size={18} color="#900" /> },
                        ]}
                        key={key}
                        defaultValue='10'
                        containerStyle={{ height: 40 }}
                        style={{ backgroundColor: '#fafafa' }}
                        itemStyle={{
                            justifyContent: 'flex-start'
                        }}
                        dropDownStyle={{ backgroundColor: '#fafafa' }}
                        onChangeItem={item => setState({
                            annualTurnOver: item.value
                        })}
                    />)
            }
        </View>
        
    );
};


const styles = StyleSheet.create({
    parent2: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        alignSelf: 'auto',
        flex: 2,
        backgroundColor: '#3A80E3',
    },

    textInput: {
        width: '80%',
        left: 30,
        top: 60,
        height: 56,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: 'white',
        marginBottom: 30,
        backgroundColor: "rgba(100,160,241,0.5)",
        borderRadius: 5
    },

    text: {
        width: 125,
        height: 25,
        left: 37,
        top: 24,
        fontSize: 20,
        color: 'rgb(255,255,255)'
    }

});

export default SignUpBasicInfo;
