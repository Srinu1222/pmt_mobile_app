import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    Animated,
    Button,
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image';
import { useDispatch, useSelector } from 'react-redux';
import * as actionCreator from '../../actions/team-action/index';
import { AuthContext } from '../../context';
import { SearchBar } from 'react-native-elements';
import { FlatList } from 'react-native-gesture-handler';
import Pagination from '../Pagination';
import {
    useCollapsibleSubHeader,
    CollapsibleSubHeaderAnimator,
} from 'react-navigation-collapsible';
import { FAB, Icon, BottomSheet, ButtonGroup } from 'react-native-elements';
import { Divider, Card, Avatar } from 'react-native-paper';
import EmptyNotification from '../EmptyScreens/EmptyNotification';
import { BackHandler } from "react-native";
import ElevatedView from "../ElevatedView";


const GetNotification = props => {

    const {
        onScroll /* Event handler */,
        containerPaddingTop /* number */,
        scrollIndicatorInsetTop /* number */,
        translateY,
    } = useCollapsibleSubHeader();

    const { navigation } = props

    const Header = () => (
        <Card.Title
            style={{ backgroundColor: '#3d87f1' }}
            title={<Text styles={styles.headerTitle}>Notifications</Text>}
            left={() => <Icon color={'white'} onPress={() => props.navigation.navigate('dashboard')} type='feather' name='chevron-left' />
            }
        />
    );

    // Button Group
    const buttons = ['All', 'Tickets Raised'];
    const [selectedIndex, setSelectedIndex] = React.useState(0);

    const updateIndex = index => {
        setSelectedIndex(index);
    };

    const [ClickedTicketsList, setClickedTicketsList] = React.useState([]);
    const [selectedTicketIndex, setselectedTicketIndex] = useState(null);
    const [isTicketExpanded, setisTicketExpanded] = useState(false);

    const onPressTickets = (index) => {
        setselectedTicketIndex(index)
        setisTicketExpanded(!isTicketExpanded)
        setClickedTicketsList([...ClickedTicketsList, index])
    }

    const TicketsItem = (params) => {
        const { item, index } = params
        return (
            <View elevation={8} style={[styles.cardContainer, { padding: hp('1%') }]}>
                <View style={{ flexDirection: 'row', paddingVertical: hp('0.2%'), }}>
                    <Text style={styles.raisedBy}>{item.raised_by}</Text>
                    <Text style={styles.raised}> raised a ticket</Text>
                </View>
                <Text style={[styles.description, { paddingVertical: hp('1%'), }]}>A Problem in the {item.stage} ...</Text>
                <Divider style={{ height: hp('0.2%'), backgroundColor: 'rgba(0,0,0,0.1)' }} />
                <View style={{ flexDirection: 'row', paddingTop: '2%', justifyContent: 'space-between' }}>
                    <Text style={styles.assigned}>Project Name</Text>
                    <Text style={styles.assigned}>Assigned To</Text>
                </View>
                <View style={{ flexDirection: 'row', paddingVertical: hp('1%'), justifyContent: 'space-between' }}>
                    <Text style={[styles.raised, { fontSize: hp('2%') }]}>{item.project_name}</Text>
                    <Text style={[styles.raised, { fontSize: hp('2%') }]}>{item.assigned_to}</Text>
                </View>
                <Divider style={{ height: hp('0.2%'), backgroundColor: 'rgba(0,0,0,0.1)' }} />
                <TouchableOpacity
                    onPress={() => { onPressTickets(index) }}
                >
                    <View style={{ flexDirection: 'row', paddingVertical: hp('1%') }}>
                        <Text style={styles.description}>Ticket Details{'   '}</Text>
                        <Icon color={'#3d87f1'} type='feather' size={22} style={{ borderWidth: 0 }} name={isTicketExpanded ? 'chevron-up' : 'chevron-down'} />
                    </View>
                </TouchableOpacity>
                {
                    (selectedTicketIndex == index && isTicketExpanded) &&
                    <View style={{ padding: '3%', alignItems: 'center' }}>
                        <View style={{ width: wp('79.4%'), padding: '3%', borderWidth: 1.2, borderColor: 'rgb(234, 234, 234)', borderRadius: 4, borderStyle: 'dashed' }}>
                            <Text style={[styles.title, { fontFamily: 'SourceSansPro-Semibold', color: 'rgb(68, 67, 67)' }]}>Raised By: {item.raised_by}</Text>
                            <Text style={[styles.title, { color: 'rgb(68, 67, 67)' }]}>A problem in the {item.stage} stage</Text>
                            <Text style={[styles.raised]}>{item.subject}</Text>
                            <Text style={[styles.title, { paddingTop: '5%', alignSelf: 'flex-start', color: 'rgb(194, 194, 194)' }]}>{item.date}{' '}{'\u2022'}{' '}{item.time}</Text>
                        </View>
                        <TouchableOpacity
                            onPress={() =>{
                                // console.log('GNot', item.event_id)
                                navigation.navigate(
                                    'DetailedEventPortraitView',
                                    {
                                        'project_id': item.project_id,
                                        'is_sub_contractor': item.is_sub_contractor,
                                        'event_id': item.event_id,
                                        'selectedTabIndex': 2
                                    }
                                )
                            }}
                            style={{ width: wp('80%'), borderRadius: 4, marginTop: '8%', borderWidth: 1.2, justifyContent: 'center', alignItems: 'center', height: hp('4%'), borderColor: '#3d87f1' }}
                        >
                            <Text style={{ color: '#3d87f1' }}>View Event</Text>
                        </TouchableOpacity>
                    </View>
                }

            </View>
        );
    }

    // Viewed Notifications
    const [ClickedNotificationList, setClickedNotificationList] = React.useState([]);

    const onPressNotification = (index, type, eventId, stage, projectId, is_sub_contractor) => {
        setClickedNotificationList([...ClickedNotificationList, index])
        let temp = {
            'project_id': projectId,
            'is_sub_contractor': is_sub_contractor,
            'selectedTabIndex': 0
        }

        if (type === 'team_page') {
            navigation.navigate('projectTeam');
        } else if (type === 'project_gantt') {
            navigation.navigate('DetailedEventPortraitView', temp)
        } else if (type === 'project_dashboard') {
            navigation.navigate('project_dashboard_1')
        } else if (type === 'detailed_event') {
            temp['event_id'] = eventId
            navigation.navigate('DetailedEventPortraitView', temp)
        } else if (type === 'specification_page') {
            navigation.navigate('DetailedEventPortraitView', temp)
        } else if (type === 'gantt_page') {
            navigation.navigate('DetailedEventPortraitView', temp)
        } else if (type === 'ticket_view') {
            temp['selectedTabIndex'] = 2
            navigation.navigate('DetailedEventPortraitView', temp)
        } else if (type === 'gantt_chart') {
            navigation.navigate('DetailedEventPortraitView', temp)
        } else if (type === 'stage_detail') {
            navigation.navigate('DetailedEventPortraitView', temp)
        } else if (type === 'gantt_view') {
            navigation.navigate('DetailedEventPortraitView', temp)
        } else if (type === 'popup_request') {
            return history.push('/');
        } else if (type === 'detailed_stage') {
            navigation.navigate('DetailedEventPortraitView', temp)
        } else if (type === 'non_clickable') {
            navigation.navigate('DetailedEventPortraitView', temp)
        } else if (type === 'particular_event') {
            temp['event_id'] = eventId
            props.navigation.navigate('DetailedEventPortraitView', temp)
        } else if (type === 'event') {
            temp['event_id'] = eventId
            props.navigation.navigate('DetailedEventPortraitView', temp)
        } else if (type === 'stage_page') {
            navigation.navigate('DetailedEventPortraitView', temp)
        }
    };

    const NotificationItem = (params) => {
        const { item, index } = params

        return (
            <View>
                <TouchableOpacity
                    key={index}
                    onPress={() => { onPressNotification(index, item.notification_type, item.event_id, item.stage, item.project_id, item.is_sub_contractor) }}
                >
                    <View style={[styles.itemStyle, { backgroundColor: ClickedNotificationList.includes(index) ? 'white' : 'rgb(236, 243, 254)' }]}>
                        <Text style={[styles.title, { color: '#3d87f1' }]}>{'\u2022'}{'  '}{item.title}{'  '}</Text>
                        <Text style={[styles.title, { color: 'rgb(143, 143, 143)' }]}>{item.project_name}{'  '}</Text>
                        <Text style={[styles.title, { color: 'rgb(68, 67, 67)' }]}>{item.description}{'  '}</Text>
                        <Text style={[styles.title, { color: 'rgb(194, 194, 194)' }]}>{item.date}{'  '}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }

    return (
        <View style={styles.container}>
            <Header />

            <ButtonGroup
                onPress={updateIndex}
                selectedIndex={selectedIndex}
                buttons={buttons}
                containerStyle={{ height: hp('6%'), marginHorizontal: wp('6%'), marginVertical: hp('3%'), elevation: 2 }}
                textStyle={{
                    fontFamily: 'SourceSansPro-Regular',
                    fontSize: hp('2%'),
                }}
                selectedButtonStyle={{ backgroundColor: '#3d87f1' }}
            />

            <Animated.FlatList
                ListEmptyComponent={<EmptyNotification isTicket={selectedIndex == 0 ? 'false' : 'true'} />}
                keyExtractor={(item, index) => index}
                data={selectedIndex == 0 ? props.notificationData : props.ticketsRaisedData}
                renderItem={({ item, index }) => {
                    if (selectedIndex == 0) {
                        return <NotificationItem item={item} index={index} />
                    } else { return <TicketsItem item={item} index={index} /> }
                }
                }
                onScroll={onScroll}
                contentContainerStyle={{
                    paddingBottom: hp('1%'),
                }}
                scrollIndicatorInsets={{ top: scrollIndicatorInsetTop }}
            />
        </View>
    );
};


const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: 'white',
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    cardContainer: {
        borderRadius: 4,
        borderLeftWidth: 3,
        borderLeftColor: "#3d87f1",
        backgroundColor: "#fff",
        paddingHorizontal: hp("3%"),
        margin: hp("1.5%"),
    },

    itemStyle: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        padding: wp('6%'),
        borderBottomWidth: 1,
        borderBottomColor: 'rgb(232, 232, 232)'
    },

    title: {
        fontSize: hp('2%'),
        paddingVertical: hp('0.5%'),
        fontFamily: 'SourceSansPro-Regular',
        color: '#3d87f1',
        textTransform: 'capitalize'
    },

    headerTitle: {
        fontFamily: 'SourceSansPro-Regular',
        color: '#fff',
        textTransform: 'capitalize'
    },

    assigned: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        textTransform: 'capitalize'
    },

    description: {
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-Regular',
        color: '#3d87f1',
    },

    raised: {
        fontSize: hp('2.2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(143, 143, 143)',
    },

    raisedBy: {
        fontSize: hp('2.2%'),
        fontFamily: 'SourceSansPro-SemiBold',
        color: 'rgb(68, 67, 67)',
    },
});

export default GetNotification;
    //Viewed Tickets Raised
    // const ticketsRaisedData = [
    //     {
    //         'raised_by': 'kaustubh',
    //         'subject': "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua",
    //         'project_name': 'PMT',
    //         'project_id': 8,
    //         'event_id': 1,
    //         'date': '13/11/2021',
    //         'time': '3:14PM'
    //     },
    //     {
    //         'raised_by': 'kaustubh',
    //         'subject': "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua",
    //         'project_name': 'PMT',
    //         'project_id': 8,
    //         'event_id': 1,
    //         'date': '13/11/2021',
    //         'time': '3:14PM'
    //     },
    //     {
    //         'raised_by': 'kaustubh',
    //         'subject': "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua",
    //         'project_name': 'PMT',
    //         'project_id': 8,
    //         'event_id': 1,
    //         'date': '13/11/2021',
    //         'time': '3:14PM'
    //     }
    // ]