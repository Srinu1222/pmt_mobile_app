import { View, Text } from 'react-native'
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { parseSync } from '@babel/core';
import { heightPercentageToDP } from 'react-native-responsive-screen';
import EmptyLoader from '../../components/EmptyLoader';
import { useIsFocused } from "@react-navigation/native";
import * as actionCreators from '../../actions/notification-action/index';
import notifyMessage from '../../ToastMessages';
import GetNotification from './GetNotification'

const Notification = (props) => {

    const { route, navigation } = props;

    const dispatch = useDispatch();

    const token = route.params.token

    const [reload, setReload] = useState(false);
    const changeReload = (params) => {
        setReload(!reload)
    }

    // React.useEffect(() => {
    //     dispatch(actionCreators.getNotifications(token));
    // }, []);

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            dispatch(actionCreators.getNotifications(token));
        });
        return unsubscribe;
      }, [navigation]);

    const notifictaionReducer = useSelector(state => {
        return state.notificationReducers.notifications.get;
    });


    const renderNotification = () => {
        if (notifictaionReducer.success.ok) {
            
            const data = notifictaionReducer.success.data
            return <GetNotification 
                        navigation={navigation} 
                        notificationData={data.notification_list} 
                        ticketsRaisedData={data.ticket_raised_list}
                    />;
        } else if (notifictaionReducer.failure.error) {
            return notifyMessage(notifictaionReducer.failure.message);
        } else {
            return (< EmptyLoader />);
        }
    };
    return renderNotification();
};

export default Notification;