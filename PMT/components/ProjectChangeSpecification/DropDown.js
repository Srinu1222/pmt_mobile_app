import React from 'react';
import { StyleSheet, Text } from 'react-native';
import { IndexPath, Layout, Select, SelectItem } from '@ui-kitten/components';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';


const DropDown = (props) => {
    const {dataItems, placeHolder} = props
    const [selectedIndex, setSelectedIndex] = React.useState([]);
    const onPressSelect = (index) => {
        // //index[index.length-1].row)
        // let row = index[index.length-1].row
        setSelectedIndex(index)
        // //index[index.length - 1])
        // props.setMethod(index)
    }

    // //selectedIndex)

    // const onPressVal = (x) => {
    //     //x)
    //     return ['x', 'y']
    // }

    return (
        <Select
            size={'large'}
            placeholder={placeHolder}
            multiSelect={true}
            selectedIndex={selectedIndex}
            // value={onPressVal}
            onSelect={index => { onPressSelect(index) }}>  
            {
                dataItems.map(
                    (item, index) =>
                        <SelectItem
                            key={index} 
                            title={item}
                            style={{borderWidth: 0,}}
                        />
                )
            }
        </Select>
    );
};

const styles = StyleSheet.create({
    container: {
        height: 128,
    },
});

export default DropDown;
