export const drawings = [
    "Array Layout",
    "Civil Layout",
    "PV-Syst",
    "Communication System Layout",
    "Equipment Layout",
    "Walkways",
    "Earthing and LA Layout",
    "Safety Line",
    "Cable Layout",
    "Safety Rails",
    "Structure Drawings_RCC",
    "Project SLD",
    "Structure Drawings_Metal Sheet",
    "Final BOQ",
    "Structure Drawings_Ground Mount",
    "Detailed Project Report"
]

export const projectTypes = [
    "RoofTop",
    "Open Access",
    "Ground Mounted",
    "Industrial",
    "Commercial",
    "Resedential"
]

export const components = [
    "Solar PV Module",
    "Module Mounting Structures with Accessories _ Metal Sheet",
    "Solar Inverter",
    "Module Mounting Structures with Accessories _ Ground Mount",
    "Module Mounting Structures with Accessories _ RCC",
    "Structure Foundation/Pedestal",
    "Cabling Accessories",
    "Earthing System",
    "Lighting Arrestor",
    "LT Panel",
    "HT Panel",
    "AC Distribution (Combiner) Panel Board",
    "Transformer",
    "LT Power Cable (From inverters to ACDB)",
    "LT Power Cable (From ACDB to Customer LT Panel)",
    "LT Power Cable (From metering cubical to MDB panel)",
    "Earthing Cable",
    "Module Earthing Cable",
    "DC Junction Box (SCB/SMB)",
    "GI Strip",
    "Metering Panel",
    "MDB Breaker",
    "Cable trays",
    "HT Breaker",
    "Bus bar",
    "InC Contractor"
]

export const accessories = [
    "Reverse Protection Relay",
    "Net-Metering",
    "DG Synchronization",
    "Communication Cable",
    "Safety Rails",
    "Walkways",
    "Safety Lines",
    "SCADA System",
    "Pyro Meter",
    "Weather Monitoring Unit",
    "Ambient Temperature Sensors",
    "UPS",
    "Module Temperature Sensors",
    "Wire mesh for protection of Skylights",
    "Cleaning System",
    "Danger Board and Signs",
    "Fire Extingusiher",
    "Generation Meter",
    "Zero Export Device",
    "Other Sensors",
    "Control Room"
]

export const areaOfInstallation = [
    "Ground Mount",
    "RCC",
    "Metal Sheet"
]

export const Termination = [
    "LT",
    "HT"
]
