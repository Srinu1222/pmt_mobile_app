import React, { useRef, useEffect } from 'react';
import {
  View,
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Dimensions,
  Image,
  Text,
  TextInput,
  PermissionsAndroid,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
} from 'react-native'; import { Searchbar, Button } from 'react-native-paper';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import EmptyScreenHeader from '../EmptyScreens/EmptyScreenHeader';
import { SearchBar } from 'react-native-elements';
import { useDispatch, useSelector } from 'react-redux';
import { FlatList } from 'react-native-gesture-handler';
import Pagination from '../Pagination';
import ActionSheet from 'react-native-actionsheet';
import FastImage from 'react-native-fast-image';
import RNFetchBlob from 'rn-fetch-blob';
import * as actionCreator from '../../actions/project-docs-action/index';
import Toast from 'react-native-toast-message';


function Item({ id, name, department, stage, file, owner, date }) {

  const [selectedId, setselectedId] = React.useState(null);
  const [isExpandable, setisExpandable] = React.useState(false);

  const changeExpandable = (id) => {
    setisExpandable(!isExpandable)
    setselectedId(id)
  }
  const checkPermission = async (Image_URI) => {

    // Function to check the platform
    // If iOS then start downloading
    // If Android then ask for permission

    if (Platform.OS === 'ios') {
      downloadImage();
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Storage Permission Required',
            message:
              'App needs access to your storage to download Photos',
          }
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          // Once user grant the permission start downloading
          //'Storage Permission Granted.');
          downloadImage(Image_URI);
        } else {
          // If permission denied then show alert
          Toast.show({
            type: 'error',
            text1: 'Storage Permission',
            text2: 'Storage Permission Not Granted'
          });
        }
      } catch (err) {
        // To handle permission related exception
        console.warn(err);
      }
    }
  };

  const downloadImage = (URI) => {
    // Main function to download the image

    // To add the time suffix in filename
    let date = new Date();
    // Image URL which we want to download
    let image_URL = URI;
    // Getting the extention of the file
    let ext = getExtention(image_URL);
    ext = '.' + ext[0];
    // Get config and fs from RNFetchBlob
    // config: To pass the downloading related options
    // fs: Directory path where we want our image to download
    const { config, fs } = RNFetchBlob;
    let PictureDir = fs.dirs.PictureDir;
    let options = {
      fileCache: true,
      addAndroidDownloads: {
        // Related to the Android only
        useDownloadManager: true,
        notification: true,
        path:
          PictureDir +
          '/safearth_files_' +
          Math.floor(date.getTime() + date.getSeconds() / 2) +
          ext,
        description: 'Image',
      },
    };
    config(options)
      .fetch('GET', image_URL)
      .then(res => {
        // Showing alert after successful downloading
        //'res -> ', JSON.stringify(res));
        Toast.show({
          type: 'success',
          text1: 'File',
          text2: 'File has been downloaded Successfully.'
        });
      });
  };
  const getExtention = filename => {
    // To get the file extension
    return /[.]/.exec(filename) ?
      /[^.]+$/.exec(filename) : undefined;
  };
  return (
    <View style={styles.item_card1}>

      <Text style={styles.name_text}>{name}</Text>
      <Text style={styles.department_text}>{department}</Text>



      <Text style={{ height: hp('0.2%'), marginTop: 8, marginBottom: 8, backgroundColor: 'rgb(232, 232, 232)' }}></Text>
      <View style={{ display: 'flex', marginBottom: 10, flexDirection: 'row' }}>
        <View>
          <Text style={styles.email_text}>Stage</Text>
          <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>{stage}</Text>
        </View>
      </View>
      <View style={{ alignItems: 'flex-end', marginTop: 35 }}>
        <TouchableOpacity
          onPress={() => { checkPermission(file) }}
        >
          < FastImage
            style={{ width: wp('12%'), height: hp('4%') }}
            source={require('../../assets/icons/Download.png')}
            resizeMode={FastImage.resizeMode.contain}
          />
        </TouchableOpacity>
      </View>
    </View>
  )
}
const Doclist = ({ route, navigation }) => {
  const { project_id, Docs,AllDocs, token } = route.params;
  const dispatch = useDispatch();
  const [searchQuery, setSearchQuery] = React.useState('');
  const onChangeSearch = query => setSearchQuery(query);
  const [activeButton, setactiveButton] = React.useState(1);

  


  // const projectDocsReducer = useSelector((state) => {
  //   return state.projectDocsReducer.projectDocs.get;
  // });
  // //project_id)
  // useEffect(() => {
  //   dispatch(actionCreator.getProjectDocs(project_id, token));
  // }, []);


  const showActionSheet = () => {
    actionSheet.current.show();
  }


  const checkPermission = async (Image_URI) => {

    // Function to check the platform
    // If iOS then start downloading
    // If Android then ask for permission

    if (Platform.OS === 'ios') {
      downloadImage();
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Storage Permission Required',
            message:
              'App needs access to your storage to download Photos',
          }
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          // Once user grant the permission start downloading
          //'Storage Permission Granted.');
          downloadImage(Image_URI);
        } else {
          // If permission denied then show alert
          Toast.show({
            type: 'error',
            text1: 'Storage Permission',
            text2: 'Storage Permission Not Granted'
          });
        }
      } catch (err) {
        // To handle permission related exception
        console.warn(err);
      }
    }
  };

  const downloadImage = (URI) => {
    // Main function to download the image

    // To add the time suffix in filename
    let date = new Date();
    // Image URL which we want to download
    let image_URL = URI;
    // Getting the extention of the file
    let ext = getExtention(image_URL);
    ext = '.' + ext[0];
    // Get config and fs from RNFetchBlob
    // config: To pass the downloading related options
    // fs: Directory path where we want our image to download
    const { config, fs } = RNFetchBlob;
    let PictureDir = fs.dirs.PictureDir;
    let options = {
      fileCache: true,
      addAndroidDownloads: {
        // Related to the Android only
        useDownloadManager: true,
        notification: true,
        path:
          PictureDir +
          '/image_' +
          Math.floor(date.getTime() + date.getSeconds() / 2) +
          ext,
        description: 'Image',
      },
    };
    config(options)
      .fetch('GET', image_URL)
      .then(res => {
        // Showing alert after successful downloading
        //'res -> ', JSON.stringify(res));
        Toast.show({
          type: 'success',
          text1: 'File',
          text2: 'File has been downloaded Successfully.'
        });
      });
  };

  const getExtention = filename => {
    // To get the file extension
    return /[.]/.exec(filename) ?
      /[^.]+$/.exec(filename) : undefined;
  };


  let actionSheet = useRef();
  let optionArray = [
    <View style={{ flexDirection: 'row', marginLeft: -280 }}>
      <FastImage
        style={{ width: wp('9%'), height: hp('3%') }}
        source={require('../../assets/icons/sort.png')}
        resizeMode={FastImage.resizeMode.contain}
      />
      <Text style={{ color: '#000', fontFamily: 'SourceSansPro-SemiBold', fontSize: hp('2.3%') }}>Sort By</Text></View>,
    <View style={{ flexDirection: 'row', marginLeft: -300 }}>
      < FastImage
        style={{ width: wp('9%'), height: hp('3%') }}
        source={require('../../assets/icons/filter.png')}
        resizeMode={FastImage.resizeMode.contain}
      />
      <Text style={{ color: '#000', fontFamily: 'SourceSansPro-SemiBold', fontSize: hp('2.3%') }}>Filter</Text></View>,
    <View style={{ flexDirection: 'row', marginLeft: -255 }}>
      <Text style={{ color: '#000', fontFamily: 'SourceSansPro-SemiBold', fontSize: hp('2.3%') }}>Cancel</Text></View>,
  ]
  return (
    <SafeAreaView style={{ backgroundColor: "#fff" }}>
      <KeyboardAvoidingView>
        <View style={styles.container}>
          <View style={styles.parent2}>

            <View style={{ display: 'flex', marginTop: 24, flexDirection: 'row', justifyContent: 'space-between' }}>
              <SearchBar
                showLoading={false}
                platform={Platform.OS}
                containerStyle={{ borderWidth: 1, borderRadius: 4, borderColor: 'rgb(232, 232, 232)', backgroundColor: 'white', justifyContent: 'center', width: wp('73.3%'), height: hp('5.3%') }}
                clearIcon={true}
                onChangeText={onChangeSearch}
                onClearText={() => null}
                placeholder=''
                cancelButtonTitle='Cancel'
              />
              <TouchableOpacity
                onPress={() => { showActionSheet() }}
                style={{}}
              >
                < FastImage
                  style={{ width: wp('12%'), height: hp('4%') }}
                  source={require('../../assets/icons/Options.png')}
                  resizeMode={FastImage.resizeMode.contain}
                />
                <Text style={styles.option_text}>Options</Text>
              </TouchableOpacity>
              <ActionSheet
                ref={actionSheet}
                options={optionArray}
                cancelButtonIndex={2}
                destructiveButtonIndex={1}
                onPress={(index) => {}}
              />
            </View>
            {/* <Pagination activeButton={activeButton} setactiveButton={setactiveButton} reducerLength={projectDocsReducer?.success?.data?.project_docs.length} /> */}

            <Text style={{ height: hp('0.2%'), marginTop: 8, marginBottom: 8, backgroundColor: 'rgb(232, 232, 232)' }}></Text>
            <>
              <ScrollView>
                <View style={styles.item_card}>
                  <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <View>
                      <Text style={styles.name_text}>{Docs.document_name}</Text>
                      <Text style={styles.department_text}>{Docs.project_name}</Text>
                    </View>
                    <View>
                      <TouchableOpacity
                        onPress={() => { checkPermission(Docs.file) }}
                      >
                        < FastImage
                          style={{ width: wp('12%'), height: hp('4%') }}
                          source={require('../../assets/icons/Download.png')}
                          resizeMode={FastImage.resizeMode.contain}
                        />
                      </TouchableOpacity>
                    </View>
                  </View>

                  <Text style={{ height: hp('0.2%'), marginTop: 8, marginBottom: 8, backgroundColor: 'rgb(232, 232, 232)' }}></Text>
                  <View style={{ display: 'flex', marginBottom: 10, flexDirection: 'row', justifyContent: 'space-between' }}>
                    <View>
                      <Text style={styles.email_text}>Stage</Text>
                      <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>{Docs.stage}</Text>
                    </View>
                    <View>
                      <Text style={styles.email_text}>Owner</Text>
                      <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>{Docs.owner}</Text>
                    </View>
                  </View>
                  <View>
                    <Text style={[styles.department_text, { marginTop: 30 }]}>{Docs.date}</Text>
                  </View>
                </View>
                <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: hp('5%') }}>
                  <View>
                    <Text style={styles.name_text}>Other documents</Text>
                    <Text style={styles.department_text}>{Docs.project_name}</Text>

                  </View>
                  <View style={{ marginTop: 20 }}>
                    <TouchableOpacity onPress={() => navigation.navigate('Project Name', { project_id: project_id,token: token  })}>
                      <Text style={{ color: '#3d87f1' }}>View All</Text>
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={{ marginTop: hp('2%') }}>
                  <FlatList
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    data={AllDocs.slice((activeButton * 10) - 10, activeButton * 10)}
                    renderItem={
                      ({ item, index }) => (
                        <Item
                          name={item.document_name}
                          department={item.project_name}
                          project_name={item.project_name}
                          stage={item.stage}
                          file={item.file}
                          date={item.date}
                        />
                      )
                    }
                  />
                </View>
              </ScrollView></>
            <View style={{ height: hp('2%') }}>

            </View>
          </View>

        </View>
      </KeyboardAvoidingView>
    </SafeAreaView >
  );
};
const styles = StyleSheet.create({
  name_text: {
    fontSize: hp('2.5%'),
    fontFamily: 'SourceSansPro-SemiBold',
    color: 'rgb(68, 67, 67)',
  },

  email_text: {
    fontSize: hp('1.8%'),
    fontFamily: 'SourceSansPro-Regular',
    color: 'rgb(68, 67, 67)',
  },

  department_text: {
    fontSize: hp('2%'),
    fontFamily: 'SourceSansPro-SemiBold',
    color: 'rgb(194, 194, 194)',
  },

  item_card: {
    shadowColor: 'rgba(0,0,0,0.2)',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    height: hp('30%'),
    width: wp('93%'),
    marginRight: 10,
    shadowOpacity: 0.2,
    shadowRadius: 4.46,
    elevation: 2,
    borderWidth: 0,
    borderRadius: 4,
    borderLeftColor: 'rgb(61, 135, 241)',
    borderLeftWidth: 3,
    marginTop: 16.5,
    borderTopLeftRadius: 4,
    borderBottomLeftRadius: 4,
    padding: '5%',
    paddingRight: '4%'
  },
  item_card1: {
    shadowColor: 'rgba(0,0,0,0.2)',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    height: hp('26.5%'),
    width: wp('55%'),
    marginRight: 10,
    shadowOpacity: 0.2,
    shadowRadius: 4.46,
    elevation: 2,
    borderWidth: 0,
    borderRadius: 4,
    borderLeftColor: 'rgb(61, 135, 241)',
    borderLeftWidth: 3,
    marginTop: 16.5,
    borderTopLeftRadius: 4,
    borderBottomLeftRadius: 4,
    padding: '5%',
    paddingRight: '4%',
    marginTop: -1
  },

  container: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "white",
    // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },


  parent2: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    flex: 11,
    backgroundColor: 'white',
    padding: '5%'

  },

  buy_now_text: {
    fontSize: hp('2.3%'),
    fontFamily: 'SourceSansPro-Regular',
    color: 'rgb(61, 135, 241)'
  },

  subscription_text: {
    fontSize: hp('1.8%'),
    fontFamily: 'SourceSansPro-Regular',
    color: 'rgb(194, 194, 194)'
  },

  option_text: {
    fontSize: hp('1.2%'),
    fontFamily: 'SourceSansPro-Regular',
    color: 'rgb(68, 67, 67)',
    alignSelf: 'center'

  },

});
export default Doclist;
