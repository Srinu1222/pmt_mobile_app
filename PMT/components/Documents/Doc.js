import React from 'react';
import {
  View,
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Dimensions,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView
} from 'react-native';
import { Searchbar, Button } from 'react-native-paper';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Pagination from './Pagination';
import { FlatList } from 'react-native-gesture-handler';
import Search from '../teams/SearchComponent'

function Item({ id, name, total_files_count }) {

  const [selectedId, setselectedId] = React.useState(null);
  const [isExpandable, setisExpandable] = React.useState(false);

  const changeExpandable = (id) => {
    setisExpandable(!isExpandable)
    setselectedId(id)
  }

  return (
    <View style={styles.item_card}>
      {/* <Text style={styles.name_text}>{name}</Text>
          <Text style={styles.department_text}>TOTAL FILES-{total_files_count}</Text>     */}
    </View>
  )
}

const Documents = ({ navigation }) => {
  
  // Search Functionality
  const [search, setSearch] = React.useState('');
  const SetSearch = (query) => {
    setSearch(query)
  }
  const filterItems = (items, filter) => {
    return items.filter(item => item.name.toLowerCase().includes(filter.toLowerCase()))
  }

  const [activeButton, setactiveButton] = React.useState(1);
  const [pbutton, setpbutton] = React.useState(0);

  const onproject = () => {
    setpbutton(0)
  }
  const ondocument = () => {
    setpbutton(1)
  }
 

  return (

    <View style={{ backgroundColor: '#fff' }}>
      <View>
        <Text
          style={{ fontSize: 20, marginLeft: wp('11%'), fontWeight: "bold", fontFamily: "SourceSansPro-Regular", marginTop: hp('4.32') }}
        >Search By</Text>
        <Search
          setSearch={SetSearch}
          search={search}
        />
        {pbutton == 0 ?
          <View style={{ flexDirection: 'row', marginTop: 24, marginLeft: wp('11.84%') }}>
            <Button style={{ width: wp('40%'), height: 50 }} onPress={() => onproject()} uppercase={false} color="#3d87f1" mode="contained">Projects</Button>
            <Button style={{ width: wp('40%'), height: 50 }} onPress={() => ondocument()} uppercase={false} color="#3d87f1" mode="outlined" >Document Name</Button>
          </View> :
          <View style={{ flexDirection: 'row', marginTop: 24, marginLeft: wp('11.84%') }}>
            <Button style={{ width: wp('40%'), height: 50 }} onPress={() => onproject()} uppercase={false} color="#3d87f1" mode="outlined">Projects</Button>
            <Button style={{ width: wp('40%'), height: 50 }} onPress={() => ondocument()} uppercase={false} color="#3d87f1" mode="contained" >Document Name</Button>
          </View>
        }
        <View style={{ marginLeft: wp('8%'), marginRight: wp('8%'), }}>
          <Pagination activeButton={activeButton} setactiveButton={setactiveButton} reducerLength={Projects.length} />
        </View>
        <ScrollView>
          {pbutton == 0 ?
            <View style={{ marginLeft: wp('8%'), marginRight: wp('8%'), }}>
              <FlatList
                data={Projects.slice((activeButton * 10) - 10, activeButton * 10)}
                renderItem={
                  ({ item, index }) => (
                    <TouchableOpacity onPress={() => navigation.navigate('Project Name')} >

                      <View style={styles.item_card}>
                        <Text style={styles.name_text}>{item.project_name}</Text>
                        <Text style={styles.department_text}>TOTAL FILES-{item.total_files_count}</Text>
                      </View>
                    </TouchableOpacity>
                  )
                }
              />
            </View>
            : <View style={{ marginLeft: wp('8%'), marginRight: wp('8%'), }}>
              <FlatList
                data={Projects.slice((activeButton * 10) - 10, activeButton * 10)}
                renderItem={
                  ({ item, index }) => (
                    <TouchableOpacity onPress={() => navigation.navigate('Document Name')} >

                      <View style={styles.item_card1}>

                        <View>
                          <Text style={styles.name_text}>Document Name</Text>
                          <Text style={styles.department_text}>PROJECT NAME</Text>
                        </View>
                        <View>
                          <Image
                            style={{ height: 30, width: 30, marginTop: 10, marginLeft: 100 }}
                            source={require('../assets/icons/Download.png')}
                          />
                        </View>
                      </View>
                    </TouchableOpacity>
                  )
                }
              />
            </View>
          }
        </ScrollView>

      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  name_text: {
    fontSize: hp('2.4%'),
    fontFamily: 'SourceSansPro-SemiBold',
    color: 'rgb(68, 67, 67)',
  },

  email_text: {
    fontSize: hp('2%'),
    fontFamily: 'SourceSansPro-Regular',
    color: 'rgb(68, 67, 67)',
  },

  department_text: {
    fontSize: hp('1.7%'),
    fontFamily: 'SourceSansPro-SemiBold',
    color: 'rgb(61, 135, 241)',
  },
  item_card: {
    shadowColor: 'rgba(0,0,0,0.2)',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    height: wp('19%'),
    shadowOpacity: 0.2,
    shadowRadius: 4.46,
    elevation: 2,
    borderWidth: 0,
    borderLeftColor: 'rgb(61, 135, 241)',
    borderLeftWidth: wp('1.3%'),
    marginTop: 16.5,
    borderTopLeftRadius: 4,
    borderBottomLeftRadius: 4,
    padding: '5%',
    paddingRight: '4%'
  },
  item_card1: {
    shadowColor: 'rgba(0,0,0,0.2)',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    height: wp('19%'),
    shadowOpacity: 0.2,
    shadowRadius: 4.46,
    elevation: 2,
    borderWidth: 0,
    borderLeftColor: 'rgb(61, 135, 241)',
    borderLeftWidth: wp('1.3%'),
    marginTop: 16.5,
    borderTopLeftRadius: 4,
    borderBottomLeftRadius: 4,
    padding: '5%',
    paddingRight: '4%',
    flexDirection: "row"
  },

  container: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "white",
    // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },


  parent2: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    flex: 11,
    backgroundColor: 'white',
    padding: '5%'

  },

  buy_now_text: {
    fontSize: hp('2.3%'),
    fontFamily: 'SourceSansPro-Regular',
    color: 'rgb(61, 135, 241)'
  },

  subscription_text: {
    fontSize: hp('1.8%'),
    fontFamily: 'SourceSansPro-Regular',
    color: 'rgb(194, 194, 194)'
  },

  option_text: {
    fontSize: hp('1.2%'),
    fontFamily: 'SourceSansPro-Regular',
    color: 'rgb(68, 67, 67)',
    alignSelf: 'center'

  },

});

export default Documents;
