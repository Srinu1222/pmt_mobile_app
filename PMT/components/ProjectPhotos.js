import { View, Text } from 'react-native'
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import notifyMessage from '../ToastMessages';
import { parseSync } from '@babel/core';
import { heightPercentageToDP } from 'react-native-responsive-screen';
import EmptyLoader from '../components/EmptyLoader';
import { useIsFocused } from "@react-navigation/native";
import GetPhotos from '../components/Photos/GetPhotos';
import * as actionCreators from '../actions/project-docs-action/index';
import GetProjectPhotos from '../components/ProjectPhotos/GetProjectPhotos';


const ProjectPhotos = (props) => {

    const { route, navigation } = props;

    // console.log('pp', props)

    const dispatch = useDispatch();

    const { token, projectId, projectName, is_sub_contractor } = route.params

    // console.log(token, projectId, projectName)

    const [reload, setReload] = useState(false);
    const changeReload = (params) => {
        setReload(!reload)
    }

    useEffect(() => {
        dispatch(actionCreators.getProjectDocs(projectId, token, true));
    }, [reload, props]);

    const projectImagesReducer = useSelector(state => {
        return state.projectDocsReducer.projectDocs.get;
    });

    const renderProjectPhotos = () => {
        if (projectImagesReducer.success.ok) {
            return <GetProjectPhotos
                token={token}
                changeReload={changeReload}
                is_sub_contractor={is_sub_contractor}
                projectId={projectId}
                navigation={navigation}
                projectName={projectName}
                getProjectPhotosDict={projectImagesReducer.success.data}
            />;
        } else if (projectImagesReducer.failure.error) {
            return notifyMessage(projectImagesReducer.failure.message);
        } else {
            return (
                < EmptyLoader />
            )
        }
    };
    return renderProjectPhotos();
};

export default ProjectPhotos;
