import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    Button
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { useDispatch, useSelector } from 'react-redux';
import {Icon} from 'react-native-elements'
import ElevatedView from './ElevatedView';

const Pagination = (params) => {

    const [initialpage, setinitialpage] = React.useState(0);
    const [RightArrowBackgroundColor, setRightArrowBackgroundColor] = React.useState('rgb(68, 67, 67)')
    const [leftArrowBackgroundColor, setleftArrowBackgroundColor] = React.useState('rgb(179,179,179)')
    const [buttonsListArr, setbuttonsListArr] = React.useState([]);

    const onButtonPress = (index) => {
        params.setactiveButton(index)
    }

    const pages = (params.reducerLength % 10 == 0) ? params.reducerLength / 10 : Math.ceil(params.reducerLength / 10)

    useEffect(() => {

        if (pages == 1) {
            setRightArrowBackgroundColor('rgb(179, 179, 179)')
        }

        let tempArray = []
        for (let i = 0; i <= pages - 1; i++) {
            let title = (i + 1).toString();
            tempArray.push(
                <ElevatedView key={i} elevation={4}>
                <TouchableOpacity
                    key={i}
                    activeOpacity={0.8}
                    onPress={() => onButtonPress(i + 1)}
                    style={{ width: 24,height: 24, backgroundColor: i + 1 == params.activeButton ? 'rgb(61, 135, 241)' : 'white', borderColor: 'rgb(232, 232, 232)', justifyContent: 'center', borderWidth: 0, borderRadius: 4, marginRight: 6}}
                >
                    <Text style={{ color: i + 1 == params.activeButton ? 'white' : 'rgb(143, 143, 143)', alignSelf: 'center', fontFamily:'SourceSansPro-Regular' }}>{title}</Text>
                </TouchableOpacity></ElevatedView>
            );
        }
        setbuttonsListArr([...tempArray])
    }, [params.activeButton, initialpage, pages]);

    var active_number_of_items;
    if (params.reducerLength >= 10) {
        var max_pages_length = params.activeButton * 10;
        if(max_pages_length>params.reducerLength) {
            max_pages_length = params.reducerLength
        } 
        active_number_of_items = (params.activeButton * 10 - 9).toString() + '-' + max_pages_length

    } else {
        active_number_of_items = params.reducerLength
    }

    const onRightArrowPress = () => {
        if (params.activeButton < pages) {
            if (params.activeButton == pages - 1) {
                setRightArrowBackgroundColor('rgb(179, 179, 179)')
            }
            params.setactiveButton(params.activeButton + 1)
            if (initialpage + 3 == params.activeButton || initialpage + 4 == params.activeButton || initialpage + 5 == params.activeButton) {
                setinitialpage(initialpage + 1)
            }
            setleftArrowBackgroundColor('rgb(68, 67, 67)')
        } else {
            setRightArrowBackgroundColor('rgb(179, 179, 179)')
        }
    }

    const onLeftArrowPress = () => {
        if (params.activeButton > 1) {

            if (params.activeButton == 2) {
                setleftArrowBackgroundColor('rgb(179, 179, 179)')
            }
            params.setactiveButton(params.activeButton - 1)
            if (initialpage + 2 == params.activeButton || initialpage + 1 == params.activeButton) {
                if (initialpage >= 1) {
                    setinitialpage(initialpage - 1)
                }
            }
            setRightArrowBackgroundColor('rgb(68, 67, 67)')
        } else {
            setleftArrowBackgroundColor('rgb(179, 179, 179)')
        }
    }

    return (
        <View style={{ display: 'flex', marginTop: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
            <View style={{ display: 'flex', flexDirection: 'row' }}>{buttonsListArr.slice(initialpage, initialpage + 5)}</View>
            <Text style={{ marginLeft: 20, color: 'rgb(143, 143, 143)', fontSize: hp('2%'), fontFamily: 'SourceSansPro-Regular' }}>{active_number_of_items} of {params.reducerLength}</Text>
            <View style={{ display: 'flex', flexDirection: 'row' }}>
                    <Icon color={leftArrowBackgroundColor} onPress={() => onLeftArrowPress()} type='feather' name='chevron-left'/>
                    <Icon color={RightArrowBackgroundColor} onPress={() => onRightArrowPress()} type='feather' name='chevron-right'/>
            </View>
        </View>
    );
}

export default Pagination;
