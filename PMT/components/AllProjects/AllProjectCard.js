import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, Image } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import FastImage from "react-native-fast-image";
import ElevatedView from "../ElevatedView";
const AllProjectCard = (props) => {
  const { project, navigation } = props;


  const onPressProjectCard = (id) => {
    

    // if (props.project_completion == true) {
        props.navigation.navigate("project_dashboard", { 
            screen: 'projectStackScreen',
            params: 
            {
              project_id: id,
              is_sub_contractor: false,
              token: props.token,
            }});
      
}
  return (
    <ElevatedView elevation={8}
      onPress={() => navigation.navigate('project_dashboard_1', { project_id: project.project_id })}
    >
      <View style={[styles.cardContainer]}>
        <View style={[styles.projectNameAndSizeContainer]}>
          <Text style={[styles.projectNameText]}>{project.project_name}</Text>
          <Text style={[styles.sizeText]}>{`${project.size} kW`}</Text>
        </View>
        <View style={[styles.horizontalLine]}></View>
        <View style={[styles.locationStatusScoreContainer]}>
          <View style={[styles.location]}>
            <Text style={[styles.locationHeading]}>Location</Text>
            <Text style={[styles.locationName]}>{project.location}</Text>
          </View>
          <View style={[styles.status]}>
            <Text style={[styles.locationHeading]}>Status</Text>
            <Text
              style={[styles.locationName]}
            >{`Day ${project.Finished_events_days}/${project.total_days_needed}`}</Text>
          </View>
          <View style={[styles.score]}>
            <Text style={[styles.locationHeading]}>Score</Text>
            <Text style={[styles.locationName]}>{project.score}</Text>
          </View>
        </View>
        <View style={[styles.controlsContainer]}>
          <View style={[styles.viewContainer]}>
            <TouchableOpacity
              onPress={() => {
                onPressProjectCard(project.project_id)
              }}
              style={[styles.controlIconContainer]}
            >
              <Image
                style={[styles.controlIcon]}
                source={require("../../assets/images/android/all_projects_page_icons/view.png")}
                // resizeMode={FastImage.resizeMode.contain}
                resizeMode={"contain"}
              />
            </TouchableOpacity>
            <Text style={[styles.iconLabel]}>View</Text>
          </View>
          {/* <View style={[styles.downloadContainer, styles.viewContainer]}>
            <TouchableOpacity
              onPress={() => {
              }}
              style={[styles.controlIconContainer]}
            >
              <FastImage
                style={[styles.controlIcon]}
                source={require("../../assets/images/android/all_projects_page_icons/download.png")}
                resizeMode={FastImage.resizeMode.contain}
              />
            </TouchableOpacity>
            <Text style={[styles.iconLabel]}>Download</Text>
          </View> */}
          <View style={[styles.feedbackContainer, styles.viewContainer]}>
            <TouchableOpacity
              onPress={() => {props.navigation.navigate("Feedback")
              }}
              style={[styles.controlIconContainer]}
            >
              <FastImage
                style={[styles.controlIcon]}
                source={require("../../assets/images/android/all_projects_page_icons/feedback.png")}
                resizeMode={FastImage.resizeMode.contain}
              />
            </TouchableOpacity>
            <Text style={[styles.iconLabel]}>Feedback</Text>
          </View>
          {/* <View style={[styles.auditContainer, styles.viewContainer]}>
            <TouchableOpacity
              onPress={() => {
              }}
              style={[styles.controlIconContainer]}
            >
              <FastImage
                style={[styles.controlIcon]}
                source={require("../../assets/images/android/all_projects_page_icons/audit.png")}
                resizeMode={FastImage.resizeMode.contain}
              />
            </TouchableOpacity>
            <Text style={[styles.iconLabel]}>Audit</Text>
          </View> */}
        </View>
      </View>
    </ElevatedView>

  );
};

const styles = StyleSheet.create({
  cardContainer: {
    height: hp("28%"),
    borderRadius: 4,
    borderLeftWidth: 3,
    borderLeftColor: "#3d87f1",
    backgroundColor: "#fff",
    elevation: 8,
    paddingHorizontal: hp("3%"),
    paddingTop: hp("3%"),
    paddingBottom: hp("1%"),
    margin: hp("1.5%"),
  },
  projectNameAndSizeContainer: {
    marginBottom: hp("1.5%"),
  },
  projectNameText: {
    fontSize: hp("2.5%"),
    lineHeight: 24,
    letterSpacing: 0.12,
    color: "#444343",
    fontFamily: "SourceSansPro-Regular",
    fontWeight: "bold",
    paddingBottom: hp("0.2%"),
  },
  sizeText: {
    fontSize: hp("2.1%"),
    lineHeight: 20,
    letterSpacing: 0.22,
    color: "#C2C2C2",
    fontFamily: "SourceSansPro-Regular",
  },
  horizontalLine: {
    backgroundColor: "#E8E8E8",
    width: wp("75%"),
    height: 1.5,
    marginTop: hp("1%"),
    marginBottom: hp("0.25%"),
  },
  locationStatusScoreContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginTop: hp("-2%"),
  },
  location: {},
  locationHeading: {
    fontSize: hp("2.1%"),
    letterSpacing: 0.12,
    color: "#444343",
    fontFamily: "SourceSansPro-Regular",
    paddingBottom: hp("0.2%"),
  },
  locationName: {
    fontSize: hp("1.8%"),
    lineHeight: 15,
    letterSpacing: 0.22,
    color: "#8F8F8F",
    fontFamily: "SourceSansPro-Regular",
    textTransform: "capitalize",
  },
  status: {},
  score: {},
  controlsContainer: {
    width: wp("68%"),
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  viewContainer: {
    height: wp("15%"),
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  controlIconContainer: {
    // backgroundColor: "#ECF3FE",
    // maxWidth: wp("2%"),
    // maxHeight: wp("2%"),
    borderRadius: 4,
    justifyContent: "center",
    alignItems: "center",
  },
  controlIcon: {
    width: wp("10%"),
    height: wp("10%"),
    borderRadius: hp("0.35%"),
  },
  iconLabel: {
    fontSize: hp("1.6%"),
    lineHeight: 16,
    letterSpacing: 0.33,
    color: "#8F8F8F",
    fontFamily: "SourceSansPro-Regular",
  },
  downloadContainer: {},
  feedbackContainer: {},
  auditContainer: {},
});

export default AllProjectCard;
