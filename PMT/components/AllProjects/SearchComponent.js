import * as React from 'react';
import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    Animated,
    Button,
} from 'react-native';
import { FAB, Icon, Input, BottomSheet } from 'react-native-elements';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';


const Search = (props) => {
    let dynamicProps = {}
    if (props.search != '') {
        dynamicProps = { rightIcon: <Icon color={'white'} type='feather' onPress={() => props.setSearch('')} size={20} name={'x'} /> }
    }
    return (
        <View style={styles.searchBoxContainer}>
            <Icon color={'white'} type='feather' style={{ marginTop: hp('0%') }} size={23} name={'search'} />
            <Input
                placeholder={'Search for active projects..'}
                placeholderTextColor='white'
                onChangeText={text => props.setSearch(text)}
                inputStyle={{ fontSize: hp('2.2%'), width: wp('45%'), color: 'white' }}
                value={props.search}
                inputContainerStyle={{ borderBottomWidth: 0 }}
                {...dynamicProps}
                // rightIcon={<Icon color={'white'} type='feather' onPress={() => props.setSearch('')} size={20} name={'x'} />}
                containerStyle={{ width: wp('70%'), borderWidth: 0, height: hp('6.5%'), }}
            />
        </View>
    );
};

export default Search;

const styles = StyleSheet.create({
    searchBoxContainer: {
        paddingHorizontal: wp('2%'),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderWidth: 0,
        // borderColor: 'rgb(232, 232, 232)',
        backgroundColor: '#5596F3',
        borderRadius: 4
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
});
