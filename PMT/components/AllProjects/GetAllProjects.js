import {
  View,
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Dimensions,
  Animated,
  Text,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
} from "react-native";
import React, { useState, useMemo, useEffect } from "react";
import LottieView from "lottie-react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import FastImage from "react-native-fast-image";
import { SearchBar } from "react-native-elements";
import Icons from "react-native-vector-icons/Ionicons";
import { useSelector, useDispatch } from "react-redux";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { AuthContext } from "../../context";
import AllProjectCard from "./AllProjectCard";
import EmptyLoader from '../EmptyLoader'
import {
  useCollapsibleSubHeader,
  CollapsibleSubHeaderAnimator,
} from 'react-navigation-collapsible';
import Search from './SearchComponent';


const GetAllProjects = (props) => {

  const {
    onScroll /* Event handler */,
    containerPaddingTop /* number */,
    scrollIndicatorInsetTop /* number */,
    translateY,
  } = useCollapsibleSubHeader();

  const [projects, setProjects] = useState([]);

  // Search Functionality
  const [search, setSearch] = React.useState('');
  const SetSearch = (query) => {
    setSearch(query)
  }
  const filterItems = (items, search) => {
    return items.filter(item => item.project_name.toLowerCase().includes(search.toLowerCase()))
  }

  const newProject = (params) => { 
    props.navigation.navigate('NewProjectStack')
  };

  const renderFunction = () => {
    return (
      <>
        <Animated.FlatList
          backgroundColor='#ffffff'
          data={filterItems(props.data, search)}
          ListEmptyComponent={<View style={styles.projectsDisplaySection}>
            <FastImage
              style={styles.noProjectImg}
              source={require("../../assets/icons/group.png")}
              resizeMode={FastImage.resizeMode.contain}
            />

            <Text style={[styles.noProjectText]}>No projects to display</Text>
            <Text style={[styles.startNewText]}>
              Please start a new project
            </Text>

            <TouchableOpacity
              onPress={() => {
                newProject;
              }}
              style={[styles.btnStartNewProject]}
            >
              <Text style={[styles.btnText]}>Start New Project</Text>
            </TouchableOpacity>
          </View>}
          keyExtractor={(item, index) => index}
          renderItem={({ item, index }) => <AllProjectCard
            key={item.project_id}
            {...props}
            project={item}
          />}
          onScroll={onScroll}
          contentContainerStyle={{ paddingTop: containerPaddingTop, paddingLeft: hp('2%'), paddingRight: hp('2%'), paddingBottom: hp('1%') }}
          scrollIndicatorInsets={{ top: scrollIndicatorInsetTop }}
        />
        <CollapsibleSubHeaderAnimator translateY={translateY}>
          <View style={styles.customHeaderAndSearchBarContainer}>
            <View style={styles.searchBoxContainer}>
              <Search
                setSearch={SetSearch}
                search={search}
              />
            </View>
          </View>
        </CollapsibleSubHeaderAnimator>
      </>
    );
  };

  return (renderFunction());
};

const styles = StyleSheet.create({
  mainContainer: {
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
    // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },

  customHeaderAndSearchBarContainer: {
    width: Dimensions.get("window").width,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#3d87f1",
    elevation: 8
  },

  customHeaderContainer: {
    display: "flex",
    flexDirection: "row",
    // justifyContent: "space-between",
    alignItems: "center",
    bottom: hp("3%"),
    width: wp("100%"),
    paddingVertical: hp("1%"),
  },

  pageTitle: {
    fontSize: 17,
    fontSize: hp("3.06%"),
    fontFamily: "SourceSansPro-Regular",
    color: "white",
  },

  customHeaderRightIconsContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    width: wp("42%"),
    marginLeft: wp("12%"),
    marginRight: wp("5%"),
  },

  image_logo: {
    width: wp("5.75%"),
    height: hp("2.85%"),
    borderRadius: hp("0.35%"), // Equivalent to borderRadius: 2.
  },

  images: {
    backgroundColor: "#4F92F2",
    maxWidth: wp("11%"),
    height: wp("11%"), //
    borderRadius: 4,
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },

  searchBoxContainer: {
    display: "flex",
    flexDirection: "row",
    margin: wp('3%'),
    marginTop: hp('4%'),
    marginBottom: hp('3%'),
    backgroundColor: "#5596F3",
    borderRadius: 5,
    height: hp("8%"),
  },

  search_logo: {
    width: wp("10%"),
    height: hp("3%"),
    left: wp("3%"),
    top: hp("2.5%"),
    marginRight: wp("1%"),
  },

  searchtextinput: {
    left: wp("1%"),
    fontFamily: "SourceSansPro-Regular",
    fontSize: hp("2.5%"),
    width: wp("68%"),
  },

  projectsDisplaySection: {
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
    flex: 3,
    alignItems: "center",
    backgroundColor: "#ffffff",
  },

  noProjectImg: {
    height: wp("33%"),
    width: wp("30%"),
    marginVertical: wp("3%"),
  },

  noProjectText: {
    textAlign: "center",
    fontSize: hp("3.25%"),
    lineHeight: 25,
    color: "#8F8F8F",
    marginTop: hp("10%"),
    fontFamily: "SourceSansPro-Regular",
  },

  startNewText: {
    fontSize: hp("2.5%"),
    textAlign: "center",
    color: "rgb(190,198,214)",
    fontFamily: "SourceSansPro-Regular",
  },

  btnStartNewProject: {
    marginTop: hp("5%"),
    width: wp("75%"),
    height: hp("8%"),
    backgroundColor: "#3d87f1",
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
  },

  btnText: {
    color: "white",
    fontSize: hp("2.6%"),
    lineHeight: 20,
    textAlign: "center",
    fontFamily: "SourceSansPro-Regular",
  },
});

export default GetAllProjects;