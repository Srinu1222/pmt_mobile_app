import React from 'react'
import FastImage from "react-native-fast-image";

const FastImageComponent = (props) => (
  <FastImage
    style={props.styles}
    source={props.url? props.url: 
      {
      uri: props.uri,
        // headers: { Authorization: "someAuthToken" },
        priority: FastImage.priority.normal,
    }
  }
    resizeMode={FastImage.resizeMode.contain}
  />
);

export default FastImageComponent
