import { View, Text } from 'react-native'
import React, { useEffect, useState } from "react";
import * as actionCreator from '../actions/daily-updates-action/index';
import { useDispatch, useSelector } from "react-redux";
import GetReport from '../components/ProjectReport/GetReport'
import notifyMessage from '../ToastMessages';
import { parseSync } from '@babel/core';
import { heightPercentageToDP } from 'react-native-responsive-screen';
import EmptyInventory from '../components/EmptyScreens/EmptyInventory'
import EmptyLoader from '../components/EmptyLoader'


const ProjectReports = ({ route, navigation }) => {

    const dispatch = useDispatch();
    const { token, project_id, is_sub_contractor } = route.params
    
    useEffect(() => {
        dispatch(actionCreator.getProjectReports(token, project_id));
    }, [project_id]);

    const getReportsReducer = useSelector((state) => {
        return state.dailyUpdatesReducer.projectReports.get;
    });

    const renderProjectReports = () => {
        if (getReportsReducer.success.ok) {
            return <GetReport style={{ backgroundColor: '#ffffff' }} data={getReportsReducer.success.data} navigation={navigation} />;
        } else if (false) {
            return notifyMessage(getReportsReducer.failure.message);
        } else {
            return (
                < EmptyLoader />
            )
        }
    };
    return renderProjectReports();
};

export default ProjectReports;
