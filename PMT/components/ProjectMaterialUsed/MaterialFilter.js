import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    Animated,
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image';
import { useDispatch, useSelector } from 'react-redux';
import { SearchBar } from 'react-native-elements';
import { FlatList } from 'react-native-gesture-handler';
import EmptyScreenHeader from '../EmptyScreens/EmptyScreenHeader';
import { ButtonGroup } from 'react-native-elements';
import {
    useCollapsibleSubHeader,
    CollapsibleSubHeaderAnimator,
} from 'react-navigation-collapsible';
import { FAB, Icon } from 'react-native-elements';
import { Avatar, Button, Checkbox, Card, Divider, IconButton } from 'react-native-paper';
import Icons from "react-native-vector-icons/Ionicons";


const MaterialFilter = (props) => {

    const {token, project_id} = props.route.params

    const [selectedCardIndex, setselectedCardIndex] = React.useState(null);
    const [isSelectedCard, setisSelectedCard] = React.useState(false);

    const buttons = ['Categories', 'Status']
    const [selectedIndex, setselectedIndex] = React.useState(0);

    const {
        onScroll /* Event handler */,
        containerPaddingTop /* number */,
        scrollIndicatorInsetTop /* number */,
        translateY,
    } = useCollapsibleSubHeader();

    const [selectedCategoriesCheckBoxList, setselectedCategoriesCheckBoxList] = React.useState([]);
    const onPressCategoriesCheckBox = (item) => {
        if (selectedCategoriesCheckBoxList.includes(item)) {
            selectedCategoriesCheckBoxList.splice(selectedCategoriesCheckBoxList.indexOf(item), 1);
            setselectedCategoriesCheckBoxList([...selectedCategoriesCheckBoxList])
        } else {
            setselectedCategoriesCheckBoxList([...selectedCategoriesCheckBoxList, item])
        }
    }

    const [selectedStatusCheckBoxList, setselectedStatusCheckBoxList] = React.useState([]);
    const onPressStatusCheckBox = (item) => {
        if (selectedStatusCheckBoxList.includes(item)) {
            selectedStatusCheckBoxList.splice(selectedStatusCheckBoxList.indexOf(item), 1);
            setselectedStatusCheckBoxList([...selectedStatusCheckBoxList])
        } else {
            setselectedStatusCheckBoxList([...selectedStatusCheckBoxList, item])
        }
    }

    const FlatlistItem = (props) => {
        const { item, index } = props
        return (
            <View>
                <View
                    style={{ flexDirection: 'row', paddingHorizontal: wp('8%'), paddingVertical: hp('1.5%'), justifyContent: 'flex-start' }}
                    key={props.index}
                >
                    <Checkbox
                        color={'#3d87f1'}
                        uncheckedColor={'#3d87f1'}
                        status={
                            selectedIndex == 0 ? selectedCategoriesCheckBoxList.includes(item) ? 'checked' : 'unchecked' : selectedStatusCheckBoxList.includes(item) ? 'checked' : 'unchecked'
                        }
                        onPress={() => selectedIndex == 0 ? onPressCategoriesCheckBox(item) : onPressStatusCheckBox(item)}
                    />
                    <Text style={styles.text}>{item}</Text>
                </View>
                <Divider />
            </View>
        );
    }

    return (

        <View style={styles.container}>
            <View style={{ height: hp('9%'), justifyContent: 'space-between', padding: '5%', flexDirection: 'row' }}>
                <Text style={styles.option_text}>Filter</Text>
                <Icon
                    iconStyle={{ fontSize: hp('4.5%') }}
                    name='close-outline'
                    type='ionicon'
                    color='rgb(68, 67, 67)'
                    onPress={() => props.navigation.navigate(
                        'project_material',
                        {
                            'categoryFilter': selectedCategoriesCheckBoxList,
                            'statusFilter': selectedStatusCheckBoxList,
                            'project_id': project_id,
                            'token': token,
                            'isFilter': true
                        }
                    )
                    }
                />
            </View>

            < Divider style={{ marginHorizontal: '5%', backgroundColor: 'rgb(232, 232, 232)', marginTop: '0%' }} />

            <View style={{ marginHorizontal: wp('8%'), borderWidth: 0 }}>
                <ButtonGroup
                    onPress={(index) => setselectedIndex(index)}
                    buttons={buttons}
                    containerStyle={{ height: hp('5.3%'), marginTop: hp('3%') }}
                    selectedIndex={selectedIndex}
                    buttonContainerStyle={{ backgroundColor: 'white' }}
                    selectedButtonStyle={{ backgroundColor: '#3d87f1', }}
                    textStyle={{
                        fontSize: hp('2%'),
                        fontFamily: 'SourceSansPro-Regular',
                        color: 'rgb(143, 143, 143)'
                    }}
                />
            </View>

            <View>
                <Animated.FlatList
                    // ListEmptyComponent={<EmptyMaterial />}
                    keyExtractor={(item, index) => index}
                    data={selectedIndex == 0 ? props.route.params.categoriesList : props.route.params.statusList}
                    renderItem={
                        ({ item, index }) => <FlatlistItem index={index} item={item} />}
                    onScroll={onScroll}
                    contentContainerStyle={{
                        // paddingLeft: hp('3%'),
                        // paddingRight: hp('3%'),
                        paddingBottom: hp('2%'),
                    }}
                    scrollIndicatorInsets={{ top: scrollIndicatorInsetTop }}
                />
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: '#fff',
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    option_text: {
        fontSize: hp('3%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center',
    },

    text: {
        fontSize: hp('2.3%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center',
        textTransform: 'capitalize',
        paddingLeft: wp('5%')
    },

    textMessage: {
        fontFamily: 'SourceSansPro-Regular',
        fontSize: hp('2%'),
        color: '#3d87f1'
    },
});

export default MaterialFilter;
