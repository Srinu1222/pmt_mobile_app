import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    Button,
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image';
import { useDispatch, useSelector } from 'react-redux';
import { SearchBar } from 'react-native-elements';
import { FlatList } from 'react-native-gesture-handler';
import EmptyScreenHeader from '../EmptyScreens/EmptyScreenHeader';
import Pagination from '../Pagination';
import { Icon } from 'react-native-elements'
import Icons from "react-native-vector-icons/Ionicons";
import downloadImage from './DownloadFile';

function TeamItem(props) {
    const {
        inventory_id,
        project_id,
        project_name,
        category,
        product,
        quantity,
        status,
        rate,
        value,
        view_po,
        location
    } = props.item;

    const [selectedId, setselectedId] = React.useState(null);
    const [isClickOnEllipses, setisClickOnEllipses] = React.useState(false);

    const OnPressEllipses = (params) => {
        setisClickOnEllipses(!isClickOnEllipses)
        setselectedId(params.inventory_id)
    }

    const EllipsesCard = (params) => {
        const downloadPO = (params) => {

        }

        const duplicate = (params) => {

        }

        const reAssign = (params) => {

        }

        const assign = (params) => {

        }

        return (
            <View style={styles.ellipsesCard}>
                <TouchableOpacity
                    onPress={() => { downloadPO() }}
                    style={styles.ellipsesbtn}
                >
                    <Text style={styles.ellipsestxt}>Download PO</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => { duplicate() }}
                    style={styles.ellipsesbtn}
                >
                    <Text style={styles.ellipsestxt}>Duplicate</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => { reAssign() }}
                    style={styles.ellipsesbtn}
                >
                    <Text style={styles.ellipsestxt}>ReAssign</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => { assign() }}
                    style={styles.ellipsesbtn}
                >
                    <Text style={styles.ellipsestxt}>Assign</Text>
                </TouchableOpacity>
            </View>
        );
    }

    return (
        <View style={styles.item_card}>

            <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>

                <View>
                    <Text style={styles.name_text}>{category}</Text>
                    <Text style={styles.department_text}>Category</Text>
                </View>

                {selectedId == inventory_id && isClickOnEllipses ? <EllipsesCard /> : null}

                <TouchableOpacity>
                    <Icons.Button
                        name="ellipsis-vertical"
                        size={25}
                        color='rgb(143, 143, 143)'
                        backgroundColor='white'
                        onPress={() => { OnPressEllipses({ inventory_id }) }}
                    />
                </TouchableOpacity>
            </View>

            <Text style={{ height: hp('0.2%'), marginVertical: '4%', backgroundColor: 'rgb(232, 232, 232)' }}></Text>

            <View style={{ display: 'flex', marginBottom: 20, width: wp('70%'), flexDirection: 'row', justifyContent: 'space-between' }}>
                <View>
                    <Text style={styles.email_text}>{quantity}</Text>
                    <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>Quantity</Text>
                </View>
                <View>
                    <Text style={styles.email_text}>{rate}</Text>
                    <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>Rate</Text>
                </View>
                <View>
                    <Text style={styles.email_text}>{value}</Text>
                    <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>Value</Text>
                </View>
            </View>

            <View>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={[styles.email_text, { borderRightWidth: 2, textTransform: 'capitalize', paddingRight: '3%', borderRightColor: '#f3f3f3' }]}>{status.replace(/_/g, " ")}</Text>

                    <TouchableOpacity
                        onPress={() => downloadImage(view_po)}
                    >
                        <Text style={{ color: '#3d87f1', fontSize: hp('1.8%'), fontFamily: 'SourceSansPro-Regular', paddingLeft: '3%' }}>VIEW</Text>
                    </TouchableOpacity>
                </View>
                <Text style={[styles.email_text, { color: 'rgb(143, 143, 143)' }]}>Status</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    name_text: {
        fontSize: hp('2.8%'),
        fontFamily: 'SourceSansPro-SemiBold',
        color: 'rgb(68, 67, 67)',
    },

    email_text: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        textTransform: 'capitalize'
    },

    department_text: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-SemiBold',
        color: 'rgb(194, 194, 194)',
    },

    item_card: {
        shadowColor: 'rgba(0,0,0,0.4)',
        elevation: 8,
        borderLeftColor: '#3d87f1',
        backgroundColor: '#fff',
        borderRadius: 4,
        borderLeftWidth: 3,
        marginTop: hp('2%'),
        padding: '5%',
        paddingRight: '4%',
    },

    ellipsesCard: {
        position: 'absolute',
        marginLeft: (Dimensions.get('window').width) * 0.3,
        marginTop: hp('5%'),
        zIndex: 10,
        backgroundColor: 'white',
        width: wp('42%'),
        height: hp('19%'),
        borderWidth: 0,
        shadowColor: 'rgba(0,0,0,0.4)',
        elevation: 8,
        padding: '5%',
        borderRadius: 4,
    },

    ellipsesbtn: {
        marginVertical: 5,
    },

    ellipsestxt: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(143, 143, 143)',
    }
});

export default TeamItem;
