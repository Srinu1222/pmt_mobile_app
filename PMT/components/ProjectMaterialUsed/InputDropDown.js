import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    Animated,
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Divider } from 'react-native-paper';
import ModalDropdown from 'react-native-modal-dropdown';
import { FAB, Icon, Input, BottomSheet } from 'react-native-elements';
import Toast from "react-native-toast-message";


const InputDropDown = (params) => {
    const { placeholder, value, setInputFunction, dropDownData,
        setDropDownFunction, dropDownValue } = params

    var [IconPosition, setIconPosition] = useState(false);

    return (
        <View
            style={{
                borderWidth: 0,
                width: wp('80%'),
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginVertical: hp('1%'),
                height: hp('7%'),
                borderColor: 'rgb(143, 143, 143)',
                borderRadius: 4,
                backgroundColor: 'rgba(179, 179, 179, 0.1)'
            }}
        >
            <Input
                placeholder={placeholder}
                onChangeText={text => setInputFunction(text)}
                inputStyle={{ fontSize: hp('2%'), color: 'rgb(68, 67, 67)' }}
                value={value}
                keyboardType={'numeric'}
                inputContainerStyle={{ borderBottomWidth: 0 }}
                containerStyle={{ borderWidth: 0,  width: wp('45%') }}
            />
            <ModalDropdown
                options={dropDownData}
                textStyle={{ fontSize: hp('2.3%'), borderWidth: 0, textAlign: 'justify', width: wp('20%'), color: 'rgb(68, 67, 67)' }}
                dropdownStyle={{ width: '30%', height: hp('15%'), marginLeft: -9, elevation: 8, marginTop: hp('.8%') }}
                dropdownTextStyle={{ fontSize: hp('2%'), color: 'rgb(143, 143, 143)' }}
                onSelect={(index, value) => setDropDownFunction(index, value)}
                defaultValue={dropDownValue}
                dropdownTextHighlightStyle={{ backgroundColor: 'rgb(236, 243, 254)' }}
                showsVerticalScrollIndicator={true}
                style={{ width: wp('30%'), alignSelf: 'center', padding: 3, paddingLeft: 7, borderLeftWidth: 1.5, borderLeftColor: 'rgb(179, 179, 179)', borderColor: 'rgb(143, 143, 143)' }}
                renderRightComponent={() => <Icon color={'rgb(179, 179, 179)'} size={20} type='feather' name={IconPosition ? 'chevron-up' : 'chevron-down'} />}
                defaultTextStyle={{ color: 'rgb(143, 143, 143)', }}
                onDropdownWillShow={() => setIconPosition(true)}
                onDropdownWillHide={() => setIconPosition(false)}
            />
        </View>
    );
}

export default InputDropDown;
