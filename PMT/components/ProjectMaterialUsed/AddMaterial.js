import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    Animated,
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Divider } from 'react-native-paper';
import { category_list, all_components_unit_dict, allFormats, CustomRateUnits } from './constants';
import ModalDropdown from 'react-native-modal-dropdown';
import { FAB, Icon, Input, BottomSheet } from 'react-native-elements';
import UploadPo from './UploadPo';
import * as actionCreators from '../../actions/material-used-action/index';
import Toast from "react-native-toast-message";
import InputDropDown from './InputDropDown';
import { FlatList } from 'react-native-gesture-handler';
import { relativeTimeRounding } from 'moment';


const AddMaterial = (params) => {

    const { changeIsVisible, token, project_id, changeReload } = params

    const [category, setCategory] = useState('Category')
    const [productName, setProductName] = useState('')
    const [quantity, setQuantity] = useState('')
    const [location, setLocation] = useState('')
    const [rate, setRate] = useState('')
    const [value, setValue] = useState('');


    // Unit Module Code
    const [unitFormat, setunitFormat] = useState('');
    const [unitsList, setunitsList] = useState([]);

    useEffect(() => {
        if (category != 'Category') {
            let tempUnitsList = []
            let categoryUnitsDictFormat = all_components_unit_dict[category]
            allFormats.map((i) => {
                let is_format = categoryUnitsDictFormat[i]['unit_format']
                if (is_format) {
                    tempUnitsList.push(is_format)
                }
            })
            setunitsList([...tempUnitsList])
        }
    }, [category])

    const dispatch = useDispatch();
    useEffect(() => {
        if (quantity && rate) {
            let totalValue = parseFloat(quantity) * parseFloat(rate)
            setValue(totalValue.toString())
        }
    }, [quantity, rate])

    const [specification, setSpecifications] = useState('')
    const [uploadPo, setUploadPo] = useState({});

    var [IconPosition, setIconPosition] = useState(false);

    const handleQuantity = (value) => {
        setQuantity(value)
    }

    const handleUnitFormat = (index, value) => {
        setunitFormat(allFormats[index])
    }

    const handleRate = (value) => {
        setRate(value)
    }

    const SetUploadPO = (item) => {
        setUploadPo(item)
    }

    const handleAdd = () => {
        if (
            category &&
            productName &&
            quantity &&
            location &&
            rate &&
            value &&
            specification &&
            unitFormat
        ) {
            let materialDetails = {
                project_id: project_id,
                category: category,
                product_name: productName,
                quantity: quantity,
                location: location,
                rate: rate,
                value: value,
                upload_po_file: uploadPo,
                specification: specification,
                unitFormat: unitFormat
            }
            dispatch(actionCreators.addMaterial(materialDetails, token))
            setTimeout(() => changeReload(), 2000)
            changeIsVisible(false)
        } else {
            Toast.show({
                text1: 'Please fill all the fields!',
                type: 'info',
                autoHide: true,
                position: 'bottom',
                visibilityTime: 1000
            })
        }
    }

    return (
        <View style={{ backgroundColor: 'white', alignItems: 'center', height: Dimensions.get('window').height }}>
            <View style={{ flexDirection: 'row', width: wp('100%'), padding: '5%', justifyContent: 'space-between', }}>
                <Text style={styles.name_text}>Add Material</Text>
                <Icon color={'rgb(68, 67, 67)'} type='feather' onPress={() => changeIsVisible(false)} name={'x'} />
            </View>
            <Divider style={{ height: hp('0.1%'), width: wp('90%'), backgroundColor: 'rgba(143, 143, 143, 0.4)' }} />
            <View style={{ marginTop: hp('6%') }}></View>
            <View style={{ height: hp('80%') }}>
                <FlatList
                    data={['x']}
                    keyExtractor={(item, index) => index}
                    renderItem={
                        ({ item, index }) =>
                            <View style={{ width: wp('100%'), alignItems: 'center' }}>
                                <ModalDropdown
                                    options={category_list}
                                    textStyle={{ fontSize: hp('2%'), width: wp('65%'), color: 'rgb(68, 67, 67)' }}
                                    dropdownStyle={{ width: '80%', height: hp('42%'), marginLeft: -9, elevation: 8, marginTop: hp('.8%') }}
                                    dropdownTextStyle={{ fontSize: hp('2%'), color: 'rgb(143, 143, 143)' }}
                                    onSelect={(index, value) => {
                                        setCategory(value)
                                    }}
                                    defaultValue={category}
                                    dropdownTextHighlightStyle={{ backgroundColor: 'rgb(236, 243, 254)' }}
                                    showsVerticalScrollIndicator={true}
                                    style={styles.dropDownStyle}
                                    renderRightComponent={() => <Icon color={'rgb(68, 67, 67)'} type='feather' name={IconPosition ? 'chevron-up' : 'chevron-down'} />}
                                    defaultTextStyle={{ color: 'rgb(143, 143, 143)' }}
                                    onDropdownWillShow={() => setIconPosition(true)}
                                    onDropdownWillHide={() => setIconPosition(false)}
                                />

                                <Input
                                    placeholder={'Product Name'}
                                    onChangeText={text => setProductName(text)}
                                    inputStyle={{ fontSize: hp('2%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                    value={productName}
                                    inputContainerStyle={{ borderBottomWidth: 0 }}
                                    containerStyle={styles.inputContainerStyle}
                                />

                                <InputDropDown
                                    placeholder={'Quantity'}
                                    value={quantity}
                                    setInputFunction={handleQuantity}
                                    dropDownData={unitsList}
                                    setDropDownFunction={handleUnitFormat}
                                    dropDownValue={unitFormat}
                                />

                                <Input
                                    placeholder={'Location'}
                                    onChangeText={text => setLocation(text)}
                                    inputStyle={{ fontSize: hp('2%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                    value={location}
                                    inputContainerStyle={{ borderBottomWidth: 0 }}
                                    containerStyle={styles.inputContainerStyle}
                                />


                                <View
                                    style={{
                                        width: wp('80%'),
                                        flexDirection: 'row',
                                        justifyContent: 'space-between',
                                        marginVertical: hp('1%'),
                                        borderWidth: 0,
                                        backgroundColor: 'rgba(179, 179, 179, 0.1)',
                                        borderRadius: 4
                                    }}
                                >
                                    <Input
                                        placeholder={'Rate'}
                                        onChangeText={text => setRate(text)}
                                        inputStyle={{ fontSize: hp('2%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                        value={rate}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        containerStyle={{ width: wp('50%'), height: hp('7%') }}
                                    />
                                    <Text style={styles.UnitsText}>
                                        {
                                            unitFormat ? CustomRateUnits[
                                                all_components_unit_dict[category][unitFormat]['price_format']
                                            ] : ''
                                        }
                                    </Text>
                                </View>

                                <View
                                    style={{
                                        width: wp('80%'),
                                        flexDirection: 'row',
                                        justifyContent: 'space-between',
                                        marginVertical: hp('1%'),
                                        borderWidth: 0,
                                        backgroundColor: 'rgba(179, 179, 179, 0.1)',
                                        borderRadius: 4
                                    }}
                                >
                                    <Input
                                        placeholder={'Value'}
                                        onChangeText={text => setValue(text)}
                                        inputStyle={{ fontSize: hp('2%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                        value={value}
                                        editable={false}
                                        inputContainerStyle={{ borderBottomWidth: 0 }}
                                        containerStyle={{ width: wp('50%'), height: hp('7%') }}
                                    />
                                    <Text style={styles.UnitsText}>
                                        {
                                            unitFormat ? CustomRateUnits[
                                                all_components_unit_dict[category][unitFormat]['price_format']
                                            ]+'s' : ''
                                        }
                                    </Text>
                                </View>

                                <Input
                                    placeholder={'Specification'}
                                    onChangeText={text => setSpecifications(text)}
                                    inputStyle={{ fontSize: hp('2%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                    value={specification}
                                    multiline={true}
                                    inputContainerStyle={{ borderBottomWidth: 0 }}
                                    containerStyle={{
                                        width: wp('80%'),
                                        marginVertical: hp('1%'),
                                        borderWidth: 0,
                                        backgroundColor: 'rgba(179, 179, 179, 0.1)',
                                        borderRadius: 4
                                    }}
                                />

                                <UploadPo
                                    SetUploadPO={SetUploadPO}
                                />

                                <TouchableOpacity
                                    style={{ justifyContent: 'center', borderRadius: 4, backgroundColor: '#3d87f1', alignItems: 'center', width: wp('80%'), height: hp('7%') }}
                                    onPress={() => handleAdd()}
                                >
                                    <Text style={styles.add_text}>Add</Text>
                                </TouchableOpacity>
                            </View>
                    }
                />
            </View>

        </View>

    );
}

const styles = StyleSheet.create({
    name_text: {
        fontSize: hp('2.8%'),
        fontFamily: 'SourceSansPro-SemiBold',
        color: 'rgb(68, 67, 67)',
        textAlign: 'center'
    },

    add_text: {
        fontSize: hp('2.8%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'white',
        textAlign: 'center'
    },

    UnitsText: {
        width: wp('30%'),
        height: hp('4%'),
        alignSelf: 'center',
        paddingTop: 5,
        borderLeftWidth: 1.5,
        borderLeftColor: 'rgb(143, 143, 143)',
        fontSize: hp('2.3%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        textAlign: 'center'
    },

    inputContainerStyle: {
        width: wp('80%'),
        marginVertical: hp('1%'),
        borderWidth: 0,
        height: hp('7%'),
        backgroundColor: 'rgba(179, 179, 179, 0.1)',
        borderRadius: 4
    },

    dropDownStyle: {
        width: wp('80%'),
        borderWidth: 0,
        backgroundColor: 'rgba(179, 179, 179, 0.1)',
        height: hp('7%'),
        justifyContent: 'center',
        borderColor: 'rgb(143, 143, 143)',
        borderRadius: 4,
        padding: 8,
        marginVertical: hp('1%')
    },


    email_text: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        textTransform: 'capitalize'
    },

    department_text: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-SemiBold',
        color: 'rgb(194, 194, 194)',
    },

    item_card: {
        shadowColor: 'rgba(0,0,0,0.4)',
        elevation: 8,
        borderLeftColor: '#3d87f1',
        backgroundColor: '#fff',
        borderRadius: 4,
        borderLeftWidth: 3,
        marginTop: hp('2%'),
        padding: '5%',
        paddingRight: '4%',
    },

    ellipsesCard: {
        position: 'absolute',
        marginLeft: (Dimensions.get('window').width) * 0.3,
        marginTop: hp('5%'),
        zIndex: 10,
        backgroundColor: 'white',
        width: wp('42%'),
        height: hp('19%'),
        borderWidth: 0,
        shadowColor: 'rgba(0,0,0,0.4)',
        elevation: 8,
        padding: '5%',
        borderRadius: 4,
    },

    ellipsesbtn: {
        marginVertical: 5,
    },

    ellipsestxt: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(143, 143, 143)',
    }
});

export default AddMaterial;
