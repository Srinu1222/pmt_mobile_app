import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    Animated,
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image';
import { useDispatch, useSelector } from 'react-redux';
import * as actionCreator from '../../actions/team-action/index';
import { AuthContext } from '../../context';
import { SearchBar } from 'react-native-elements';
import { FlatList } from 'react-native-gesture-handler';
import EmptyScreenHeader from '../EmptyScreens/EmptyScreenHeader';
import Pagination from '../Pagination';
import MaterialUsedItem from './MaterialUsedItem';
import TeamMembers from '../EmptyScreens/TeamMembers';
import {
    useCollapsibleSubHeader,
    CollapsibleSubHeaderAnimator,
} from 'react-navigation-collapsible';
import { FAB, Icon, Input, BottomSheet,ButtonGroup } from 'react-native-elements';
import EmptyLoader from '../EmptyLoader'
import { Button, Divider } from 'react-native-paper';
import config from '../../config';
import AddMaterial from './AddMaterial';
import { ScrollView } from 'react-native';

const MaterialUsedList = props => {

    const {
        onScroll /* Event handler */,
        containerPaddingTop /* number */,
        scrollIndicatorInsetTop /* number */,
        translateY,
    } = useCollapsibleSubHeader();

    const { token, data, navigation, changeReload, project_id, categoriesList, statusList } = props

const buttons = ['Stock', 'Consumed', 'Wastage']
const [selectedIndex, setselectedIndex] = React.useState(0);
const [selectedBtnData, setselectedBtnData] = React.useState(data['stock']);
console.log("selectedBtnData",selectedBtnData)
const onPressBtn = (index) => {
    setselectedIndex(index)
    switch (index) {
        case 0:
            setselectedBtnData(data['stock'])
            break;
        case 1:
            setselectedBtnData(data['consumed'])
            break;
        case 2:
            setselectedBtnData(data['wastage'])
            break;
        default:
            break
    }
}
    const [activeButton, setactiveButton] = React.useState(1);
    const [isVisible, setIsVisible] = useState(false);
    const [isClickmanageTeam, setisClickmanageTeam] = useState(false);

    const onPressReManageTeam = () => {
        setisClickmanageTeam(true)
    };

    // Search Functionality
    const [search, setSearch] = React.useState('');
    const SetSearch = (query) => {
        setSearch(query)
    }
    const filterItems = (items, filter) => {
        return items.filter(item => item.category.toLowerCase().includes(filter.toLowerCase()))
    }

    const Footer = () => (
        <View style={{
            flex: 0.8,
            justifyContent: 'center',
        }}>
            <View style={{
                backgroundColor: '#3d87f1',
                paddingHorizontal: wp('8%'),
                justifyContent: 'space-between',
                flexDirection: 'row',
                height: hp('7.4%'),
                alignItems: 'center',
            }}>
                <Button
                    style={{ width: wp('40%'), height: hp('5.5%'), borderWidth: 1, borderColor: 'white', backgroundColor: '#3d87f1' }}
                    labelStyle={{ color: 'white', textTransform: 'capitalize', fontSize: hp('1.6%') }}
                    mode="contained"
                    onPress={() => changeIsVisible(true)}>
                    +Add Material
                </Button>
                <Button
                    style={{ width: wp('40%'), height: hp('5.5%'), borderWidth: 1, borderColor: 'white', backgroundColor: 'white' }}
                    labelStyle={{ color: '#3d87f1', textTransform: 'capitalize', fontSize: hp('1.6%') }}
                    mode="contained"
                    onPress={() => onPressReManageTeam()}>
                    Report Wastage
                </Button>
            </View>
        </View>
    );

    const EmptyMaterial = (params) => {
        return (
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                < FastImage
                    style={styles.groupimage}
                    source={require('../../assets/icons/inventory.png')
                    }
                    resizeMode={FastImage.resizeMode.contain}
                />
                <Text style={styles.textMessage}>There are no materials added to the inventory.</Text>
                <Text style={styles.thumbnailText}>Please add new material.</Text>
            </View>
        );
    }

    // Bottom Component For Add Material
    const changeIsVisible = (value) => {
        setIsVisible(value)
    }

    return (
        <View style={styles.container}>
            <EmptyScreenHeader title='Material Used' navigation={props.navigation} />

            <BottomSheet
                isVisible={isVisible}
                containerStyle={{ backgroundColor: 'rgba(0.5, 0.25, 0, 0.2)' }}
            >
                <AddMaterial
                    changeIsVisible={changeIsVisible}
                    token={token}
                    project_id={project_id}
                    changeReload={changeReload}
                />
            </BottomSheet>

            <View style={{ flex: 10 }}>
                <View
                    style={{
                        elevation: 8,
                        paddingTop: 15,
                        paddingLeft: wp('5%'),
                        paddingRight: wp('5%'),
                        width: '100%',
                        height: 'auto',
                        backgroundColor: '#fff',
                    }}>
                    <View
                        style={{
                            display: 'flex',
                            marginTop: hp('1%'),
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                        }}>
                        <SearchBar
                            showLoading={false}
                            platform={Platform.OS}
                            containerStyle={{
                                borderWidth: 1,
                                borderRadius: 4,
                                borderColor: 'rgb(232, 232, 232)',
                                backgroundColor: 'white',
                                justifyContent: 'center',
                                width: wp('73.3%'),
                                height: hp('5.3%'),
                            }}
                            clearIcon={true}
                            onChangeText={(text) => SetSearch(text)}
                            onClearText={() => null}
                            placeholder=""
                            cancelButtonTitle="Cancel"
                        />
                        <TouchableOpacity
                            onPress={() =>
                                navigation.navigate('material_used_filter',
                                    {
                                        'categoriesList': categoriesList,
                                        'statusList': statusList,
                                        'token': token,
                                        'project_id': project_id
                                    }
                                )
                            }
                            style={{}}>
                            <FastImage
                                style={{ width: wp('12%'), height: hp('4%') }}
                                source={require('../../assets/icons/rectangle.png')}
                                resizeMode={FastImage.resizeMode.contain}
                            />
                            <Text style={styles.option_text}>Options</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ width: wp('100%'), marginVertical: hp('2%'), alignSelf: 'center' }}>
                        <ButtonGroup
                            onPress={(index) => { onPressBtn(index) }}
                            buttons={buttons}
                            containerStyle={{ height: hp('5.6%'), left: 0, }}
                            selectedIndex={selectedIndex}
                            buttonContainerStyle={{ backgroundColor: 'white' }}
                            textStyle={{
                                fontSize: hp('1.8%'),
                                fontFamily: 'SourceSansPro-Regular',
                                color: 'rgb(143, 143, 143)'
                            }}
                            selectedButtonStyle={{ backgroundColor: '#3d87f1', }}
                        />
                    </View>
                    <View style={{ marginBottom: hp('2%'), marginTop: hp('2%') }}>
                        <Pagination
                            activeButton={activeButton}
                            setactiveButton={setactiveButton}
                            reducerLength={selectedBtnData.length}
                            style={{ paddingBottom: hp('5%') }}
                        />
                    </View>
                </View>

                <Animated.FlatList
                    ListEmptyComponent={<EmptyMaterial />}
                    keyExtractor={(item, index) => index}
                    data={filterItems(selectedBtnData.slice(activeButton * 10 - 10, activeButton * 10), search)}
                    renderItem={({ item, index }) => <MaterialUsedItem item={item} />}
                    onScroll={onScroll}
                    contentContainerStyle={{
                        paddingLeft: hp('3%'),
                        paddingRight: hp('3%'),
                        paddingBottom: hp('1%'),
                    }}
                    scrollIndicatorInsets={{ top: scrollIndicatorInsetTop }}
                />
            </View>

            <Footer />

        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        flex: 1,
        backgroundColor: '#fff',
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    option_text: {
        fontSize: hp('1.2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center',
    },

    textMessage: {
        width: wp('80%'),
        height: hp('8%'),
        textAlign: 'center',
        fontSize: hp('2.3%'),
        color: 'rgb(143,143,143)'
    },

    thumbnailText: {
        width: wp('90%'),
        height: hp('4%'),
        fontSize: hp('1.8%'),
        textAlign: 'center',
        color: 'rgb(190,198,214)'
    },

    groupimage: {
        height: hp('40.5%'),
        width: wp('50.3%'),
    },
});

export default MaterialUsedList;
