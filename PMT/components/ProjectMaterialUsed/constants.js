export const all_components_unit_dict = {
    'Solar PV Module': {
        'format1': { 'unit_format': 'kWp', 'price_format': 'Rs. X/Wp', 'total_price': 'X *1000 * kWp' },
        'format2': { 'unit_format': 'Nos', 'price_format': 'Rs.X Per Nos', 'total_price': 'X* Nos' },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'Solar Inverter': {
        'format1': { 'unit_format': 'Nos', 'price_format': 'Rs. X Per Nos', 'total_price': 'X * Nos' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'LT Power Cable (From inverters to ACDB)': {
        'format1': { 'unit_format': 'm', 'price_format': 'Rs. X/m', 'total_price': 'X * no. of metres' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'LT Power Cable (From ACDB to Customer LT Panel)': {
        'format1': { 'unit_format': 'm', 'price_format': 'Rs. X/m', 'total_price': 'X * no. of metres' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'LT Power Cable (From metering cubical to MDB panel)': {
        'format1': { 'unit_format': 'm', 'price_format': 'Rs. X/m', 'total_price': 'X * no. of metres' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'Earthing Cable': {
        'format1': { 'unit_format': 'm', 'price_format': 'Rs. X/m', 'total_price': 'X * no. of metres' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'Module Earthing Cable': {
        'format1': { 'unit_format': 'm', 'price_format': 'Rs. X/m', 'total_price': 'X * no. of metres' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'Solar DC Cables': {
        'format1': { 'unit_format': 'm', 'price_format': 'Rs. X/m', 'total_price': 'X * no. of metres' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'Safety Lines': {
        'format1': { 'unit_format': 'm', 'price_format': 'Rs. X/m', 'total_price': 'X * no. of metres' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'Fire Extingusiher': {
        'format1': { 'unit_format': 'Nos.', 'price_format': 'Rs. X Per Nos', 'total_price': 'X * Nos' },
        'format2': { 'unit_format': 'Set', 'price_format': 'Rs. X Per Set', 'total_price': 'X * Set' },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'Danger Board and Signs': {
        'format1': { 'unit_format': 'Nos.', 'price_format': 'Rs. X Per Nos', 'total_price': 'X * Nos' },
        'format2': { 'unit_format': 'Set', 'price_format': 'Rs. X Per Set', 'total_price': 'X * Set' },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'GI Strip': {
        'format1': { 'unit_format': 'm', 'price_format': 'Rs. X/m', 'total_price': 'X * no. of metres' },
        'format2': { 'unit_format': 'Kg', 'price_format': 'Rs. X per kg', 'total_price': 'X * kgs' },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'Easrthing Protection System': {
        'format1': { 'unit_format': 'Nos.', 'price_format': 'Rs. X Per Nos', 'total_price': 'X * Nos' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'Lighting Arrestor': {
        'format1': { 'unit_format': 'Nos.', 'price_format': 'Rs. X Per Nos', 'total_price': 'X * Nos' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'Cable trays': {
        'format1': { 'unit_format': 'm', 'price_format': 'Rs. X/m', 'total_price': 'X * no. of metres' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'Cabling Accessories': {
        'format1': { 'unit_format': 'Nos', 'price_format': 'Rs. X Per Nos', 'total_price': 'X * Nos' },
        'format2': { 'unit_format': 'Set', 'price_format': 'Rs. X Per Set', 'total_price': 'X * Set' },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'Wire mesh for protection of Skylights': {
        'format1': { 'unit_format': 'Nos.', 'price_format': 'Rs. X Per Nos', 'total_price': 'X * Nos' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'Cleaning System': {
        'format1': { 'unit_format': 'Nos.', 'price_format': 'Rs. X Per Nos', 'total_price': 'X * Nos' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'Communication Cable': {
        'format1': { 'unit_format': 'm', 'price_format': 'Rs. X/m', 'total_price': 'X * no. of metres' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    }, 'Weather Monitoring Unit': {
        'format1': { 'unit_format': 'Nos', 'price_format': 'Rs. X Per Nos', 'total_price': 'X * Nos' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    }, 'Ambient Temperature Sensors': {
        'format1': { 'unit_format': 'Nos', 'price_format': 'Rs. X Per Nos', 'total_price': 'X * Nos' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    }, 'Module Temperature Sensors': {
        'format1': { 'unit_format': 'Nos', 'price_format': 'Rs. X Per Nos', 'total_price': 'X * Nos' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'Other Sensors': {
        'format1': { 'unit_format': 'Nos.', 'price_format': 'Rs. X Per Nos', 'total_price': 'X * Nos' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'Pyro Meter': {
        'format1': { 'unit_format': 'Nos.', 'price_format': 'Rs. X Per Nos', 'total_price': 'X * Nos' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'SCADA System': {
        'format1': { 'unit_format': 'Nos.', 'price_format': 'Rs. X Per Nos', 'total_price': 'X * Nos' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'UPS': {
        'format1': { 'unit_format': 'Nos.', 'price_format': 'Rs. X Per Nos', 'total_price': 'X * Nos' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'Transformer': {
        'format1': { 'unit_format': 'Nos.', 'price_format': 'Rs. X Per Nos', 'total_price': 'X * Nos' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'Walkways': {
        'format1': { 'unit_format': 'Kg', 'price_format': 'Rs. X per kg', 'total_price': 'X * kgs' },
        'format2': { 'unit_format': 'm', 'price_format': 'Rs. X/m', 'total_price': 'X * no. of metres' },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'LT Panel': {
        'format1': { 'unit_format': 'Nos.', 'price_format': 'Rs. X Per Nos', 'total_price': 'X * Nos' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'HT Panel': {
        'format1': { 'unit_format': 'Nos.', 'price_format': 'Rs. X Per Nos', 'total_price': 'X * Nos' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'AC Distribution (Combiner) Panel Board': {
        'format1': { 'unit_format': 'Nos.', 'price_format': 'Rs. X Per Nos', 'total_price': 'X * Nos' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    }, 'DC Junction Box (SCB/SMB)': {
        'format1': { 'unit_format': 'Nos.', 'price_format': 'Rs. X Per Nos', 'total_price': 'X * Nos' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'Metering Panel': {
        'format1': { 'unit_format': 'Nos.', 'price_format': 'Rs. X Per Nos', 'total_price': 'X * Nos' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'MDB Breaker': {
        'format1': { 'unit_format': 'Nos.', 'price_format': 'Rs. X Per Nos', 'total_price': 'X * Nos' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'HT Breaker': {
        'format1': { 'unit_format': 'Nos.', 'price_format': 'Rs. X Per Nos', 'total_price': 'X * Nos' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'Bus bar': {
        'format1': { 'unit_format': 'Nos.', 'price_format': 'Rs. X Per Nos', 'total_price': 'X * Nos' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'Generation Meter': {
        'format1': { 'unit_format': 'Nos.', 'price_format': 'Rs. X Per Nos', 'total_price': 'X * Nos' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'Zero Export Device': {
        'format1': { 'unit_format': 'Nos.', 'price_format': 'Rs. X Per Nos', 'total_price': 'X * Nos' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'Reverse Protection Relay': {
        'format1': { 'unit_format': 'Nos.', 'price_format': 'Rs. X Per Nos', 'total_price': 'X * Nos' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'Net-Metering': {
        'format1': { 'unit_format': 'Nos.', 'price_format': 'Rs. X Per Nos', 'total_price': 'X * Nos' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'DG Syncronization': {
        'format1': { 'unit_format': 'Nos.', 'price_format': 'Rs. X Per Nos', 'total_price': 'X * Nos' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'Structures': {
        'format1': { 'unit_format': 'Nos.', 'price_format': 'Rs. X Per Nos', 'total_price': 'X * Nos' },
        'format2': { 'unit_format': 'kWp', 'price_format': 'Rs. X per Wp', 'total_price': 'X *1000* kWp' },
        'format3': { 'unit_format': 'Kg', 'price_format': 'Rs. X per kg', 'total_price': 'X * kgs' }
    },
    'Structure Foundation Pedestal': {
        'format1': { 'unit_format': 'Nos.', 'price_format': 'Rs. X Per Nos', 'total_price': 'X * Nos' },
        'format2': { 'unit_format': 'Set', 'price_format': 'Rs. X Per Set', 'total_price': 'X * Set' },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'Safety Rails': {
        'format1': { 'unit_format': 'Kg', 'price_format': 'Rs. X per kg', 'total_price': 'X * kgs' },
        'format2': { 'unit_format': 'Set', 'price_format': 'Rs. X Per Set', 'total_price': 'X * Set' },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'Module Mounting Structures with Accessories _ Metal Sheet': {
        'format1': { 'unit_format': 'Nos.', 'price_format': 'Rs. X Per Nos', 'total_price': 'X * Nos' },
        'format2': { 'unit_format': 'kWp', 'price_format': 'Rs. X per Wp', 'total_price': 'X *1000* kWp' },
        'format3': { 'unit_format': 'Kg', 'price_format': 'Rs. X per kg', 'total_price': 'X * kgs' }
    },
    'Module Mounting Structures with Accessories _ Ground Mount': {
        'format1': { 'unit_format': 'Nos.', 'price_format': 'Rs. X Per Nos', 'total_price': 'X * Nos' },
        'format2': { 'unit_format': 'kWp', 'price_format': 'Rs. X per Wp', 'total_price': 'X *1000* kWp' },
        'format3': { 'unit_format': 'Kg', 'price_format': 'Rs. X per kg', 'total_price': 'X * kgs' }
    },
    'Module Mounting Structures with Accessories _ RCC': {
        'format1': { 'unit_format': 'Nos.', 'price_format': 'Rs. X Per Nos', 'total_price': 'X * Nos' },
        'format2': { 'unit_format': 'kWp', 'price_format': 'Rs. X per Wp', 'total_price': 'X *1000* kWp' },
        'format3': { 'unit_format': 'Kg', 'price_format': 'Rs. X per kg', 'total_price': 'X * kgs' }
    },
    'InC Contractor': {
        'format1': { 'unit_format': 'kWp', 'price_format': 'Rs. X per Wp', 'total_price': 'X *1000* kWp' },
        'format2': { 'unit_format': null, 'price_format': null, 'total_price': null },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
    'MC4 Connectors': {
        'format1': { 'unit_format': 'Pairs', 'price_format': 'Rs. X per pairs', 'total_price': 'X * pairs' },
        'format2': { 'unit_format': 'Nos', 'price_format': 'Rs. X per Nos', 'total_price': 'X * Nos' },
        'format3': { 'unit_format': null, 'price_format': null, 'total_price': null }
    },
}

export const allFormats = ['format1', 'format2', 'format3'];

export const CustomRateUnits = {
    'Rs. X per Wp': 'Rs/Wp',
    'Rs. X per kg': 'Rs/Kg',
    'Rs. X Per Set': 'Rs/Set',
    'Rs. X Per Nos': 'Rs/No',
    'Rs. X per pairs': 'Rs/pairs',
    'Rs. X/Wp': 'Rs/Wp',
    'Rs. X per Nos': 'Rs/No',
    'Rs.X Per Nos': 'Rs/No',
    'Rs. X/m': 'Rs/m'
}

export const CustomTotalPriceOperation = {
    'X * kgs': '',
    'X* Nos': '',
    'X * no. of metres': '',
    'X * Set': '',
    'X *1000 * kWp': '',
    'X * Nos': '',
    'X * pairs': '',
    'X *1000* kWp': ''
}


export const category_list = [
    'AC Distribution (Combiner) Panel Board',
    'Ambient Temperature Sensors',
    'Bus bar',
    'Cable trays',
    'Cabling Accessories',
    'Cleaning System',
    'Communication Cable',
    'DC Junction Box (SCB/SMB)',
    'DG Syncronization',
    'Danger Board and Signs',
    'Earthing Cable',
    'Easrthing Protection System',
    'Fire Extingusiher',
    'GI Strip',
    'Generation Meter',
    'HT Breaker',
    'HT Panel',
    'InC Contractor',
    'LT Panel',
    'LT Power Cable (From ACDB to Customer LT Panel)',
    'LT Power Cable (From inverters to ACDB)',
    'LT Power Cable (From metering cubical to MDB panel)',
    'Lighting Arrestor',
    'MC4 Connectors',
    'MDB Breaker',
    'Metering Panel',
    'Module Earthing Cable',
    'Module Mounting Structures with Accessories _ Ground Mount',
    'Module Mounting Structures with Accessories _ Metal Sheet',
    'Module Mounting Structures with Accessories _ RCC',
    'Module Temperature Sensors',
    'Net-Metering',
    'Other Sensors',
    'Pyro Meter',
    'Reverse Protection Relay',
    'SCADA System',
    'Safety Lines',
    'Safety Rails',
    'Solar DC Cables',
    'Solar Inverter',
    'Solar PV Module',
    'Structure Foundation Pedestal',
    'Transformer',
    'UPS',
    'Walkways',
    'Weather Monitoring Unit',
    'Wire mesh for protection of Skylights',
    'Zero Export Device'
]