import { View, Text } from 'react-native'
import React, { useEffect, useState } from "react";
import * as actionCreators from "../actions/material-used-action/index";
import { useDispatch, useSelector } from "react-redux";
import notifyMessage from '../ToastMessages';
import { parseSync } from '@babel/core';
import { heightPercentageToDP } from 'react-native-responsive-screen';
import EmptyLoader from '../components/EmptyLoader'
import config from '../config'
import MaterialUsedList from '../components/ProjectMaterialUsed/MaterialUsedList'


const ProjectMaterialUsed = (props) => {

    const dispatch = useDispatch();

    const { token, project_id, is_sub_contractor, isFilter, categoryFilter, statusFilter } = props.route.params
    const [reload, setReload] = useState(false);
    const changeReload = () => {
      setReload(!reload)
    }

    useEffect(() => {
        //(token, project_id, categoryFilter, statusFilter)
        dispatch(actionCreators.getProjectInventory(token, project_id));
    }, [props, reload]);

    const materialUsedReducer = useSelector((state) => {
        return state.materialUsedReducer.materialUsed.get;
    });

    const renderMaterialUsed = () => {
        if (materialUsedReducer.success.ok) {

            let data = materialUsedReducer.success.data

            
           // FILTER DATA TO DISPLAY
           let categories_list=materialUsedReducer.success.data.category_list
            
            // data.map(
            //     (item, index) => {
            //         if (!categories_list.includes(item.category)) {
            //             categories_list.push(item.category)
            //         }
            //     }
            // )
            let status_list=materialUsedReducer.success.data.status_list
            // data.map(
            //     (item, index) => {
            //         if (!status_list.includes(item.status)) {
            //             status_list.push(item.status)
            //         }
            //     }
            // )

            // FILTERING LOGIC

            const Categoryfilter = (item) => {
                return categoryFilter.includes(item.category);
            }

            if (isFilter && categoryFilter.length > 0) {
                data['stock'] = data['stock'].filter(Categoryfilter)
                data['consumed'] = data['consumed'].filter(Categoryfilter)
                data['wastage'] = data['wastage'].filter(Categoryfilter)
            }

            const StatusFilter = (item) => {
                return statusFilter.includes(item.status);
            }

            if (isFilter && statusFilter.length > 0) {
                data['stock'] = data['stock'].filter(StatusFilter)
                data['consumed'] = data['consumed'].filter(StatusFilter)
                data['wastage'] = data['wastage'].filter(StatusFilter)
            }

            return <MaterialUsedList
                style={{ backgroundColor: '#ffffff' }}
                categoriesList={categories_list}
                statusList={status_list}
                navigation={props.navigation}
                token={token}
                project_id={project_id}
                data={data}
                changeReload={changeReload}
            />;
        } else if (materialUsedReducer.failure.error) {
            return notifyMessage('Something went wrong');
        } else {
            return <EmptyLoader />
        }
    };
    return renderMaterialUsed();
};

export default ProjectMaterialUsed;
