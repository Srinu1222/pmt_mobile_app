import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    PermissionsAndroid,
    KeyboardAvoidingView,
    ScrollView,
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { FlatList } from 'react-native-gesture-handler';
import { ButtonGroup } from 'react-native-elements';
import { Avatar, Card, Divider, Button, IconButton } from 'react-native-paper';
import { Icon } from 'react-native-elements'
import BudgetedScreen1 from './BudgetedScreen1'
import TouchableScale from 'react-native-touchable-scale';
import downloadImage from './DownloadFile';
import onPressUploadDocument from './FileUpload';
import * as actionCreators from "../../actions/project-finance-action/index";
import { useDispatch, useSelector } from 'react-redux';
import RNFetchBlob from 'rn-fetch-blob';
import Toast from 'react-native-toast-message';


const BudgetedScreen = (props) => {

    const btnType = props.type

    const {token, project_id, changeReload} = props

    const [isClickOnArrow, setisClickOnArrow] = React.useState(false);
    const [isSupplySide, setisSupplySide] = React.useState(false);

    const total_budget_service_side_cost = props.total_budget_service_side_cost
    const total_budget_supply_side_cost = props.total_budget_supply_side_cost
    const SupplySideList = props.SupplySideList
    const ServiceSideList = props.ServiceSideList

    const cardData = ['Supply Side', 'Service Side']

    const onArrowPress = (index) => {
        setisClickOnArrow(true)
        if (index == 0) {
            setisSupplySide(true)
        } else {
            setisSupplySide(false)
        }
    }

    const checkPermission = async (Image_URI) => {

        // Function to check the platform
        // If iOS then start downloading
        // If Android then ask for permission
    
        if (Platform.OS === 'ios') {
          downloadImage();
        } else {
          try {
            const granted = await PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
              {
                title: 'Storage Permission Required',
                message:
                  'App needs access to your storage to download Photos',
              }
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
              // Once user grant the permission start downloading
              //'Storage Permission Granted.');
              downloadImage(Image_URI);
            } else {
              // If permission denied then show alert
              Toast.show({
                type: 'error',
                text1: 'Storage Permission',
                text2: 'Storage Permission Not Granted'
              });
            }
          } catch (err) {
            // To handle permission related exception
            console.warn(err);
          }
        }
      };
    
      const downloadImage = (URI) => {
        // Main function to download the image
    
        // To add the time suffix in filename
        let date = new Date();
        // Image URL which we want to download
        let image_URL = URI;
        // Getting the extention of the file
        let ext = getExtention(image_URL);
        ext = '.' + ext[0];
        // Get config and fs from RNFetchBlob
        // config: To pass the downloading related options
        // fs: Directory path where we want our image to download
        const { config, fs } = RNFetchBlob;
        let PictureDir = fs.dirs.PictureDir;
        let options = {
          fileCache: true,
          addAndroidDownloads: {
            // Related to the Android only
            useDownloadManager: true,
            notification: true,
            path:
              PictureDir +
              '/safearth_files_' +
              Math.floor(date.getTime() + date.getSeconds() / 2) +
              ext,
            description: 'File',
          },
        };
        config(options)
          .fetch('GET', image_URL)
          .then(res => {
            // Showing alert after successful downloading
            //'res -> ', JSON.stringify(res));
            Toast.show({
              type: 'success',
              text1: 'File',
              text2: 'File has been downloaded Successfully.'
            });
          });
      };
    
      const getExtention = filename => {
        // To get the file extension
        return /[.]/.exec(filename) ?
          /[^.]+$/.exec(filename) : undefined;
      };

    const [uploadFinanceFile, setuploadFinanceFile] = useState({});
    const [fileName, setFileName] = useState('');

    const dispatch = useDispatch();
    const onSaveFileDetails = (fileObject) => {
        setuploadFinanceFile(fileObject)
        dispatch(actionCreators.uploadFinanceFile(fileObject, token, project_id));
        changeReload()
    }
    
    const DocumentUpload = () => {
        onPressUploadDocument(onSaveFileDetails)
    }

    const renderBudgetedScreen = () => {
        if (isClickOnArrow) {
            return <BudgetedScreen1
                setisClickOnArrow={setisClickOnArrow}
                type={isSupplySide ? 'Supply Side' : 'Service Side'}
                list_of_budgeted_data_based_on_type={isSupplySide ? SupplySideList : ServiceSideList}
                btnType={btnType}
                token={props.token}
            />;
        } else {
            return (
                <View style={{ borderWidth: 0, height: hp('82%') }}>
                    <View style={{ paddingTop: hp('3%') }}>
                        <FlatList
                            backgroundColor='#ffffff'
                            data={cardData}
                            horizontal
                            showsHorizontalScrollIndicator={false}
                            keyExtractor={(item, index) => index}
                            renderItem={({ item, index }) =>
                                <TouchableScale
                                    onPress={() => onArrowPress(index)}
                                    activeScale={0.95}
                                >
                                    <Card
                                        style={{ marginLeft: wp('2.5%'), elevation: 8, backgroundColor: '#fff', shadowColor: 'rgba(0,0,0,0.4)', }}
                                    >
                                        <View style={{ display: 'flex', height: hp('26.7%'), marginLeft: wp('5%'), width: wp('35.3%'), justifyContent: 'space-around', borderWidth: 0, alignItems: 'center' }}>

                                            {
                                                index == 0 ?
                                                    < FastImage
                                                        style={{ width: wp('10%'), height: hp('7%') }}
                                                        source={require('../../assets/icons/supplyside.png')}
                                                        resizeMode={FastImage.resizeMode.contain}
                                                    /> :
                                                    < FastImage
                                                        style={{ width: wp('10%'), height: hp('7%') }}
                                                        source={require('../../assets/icons/serviceside.png')}
                                                        resizeMode={FastImage.resizeMode.contain}
                                                    />
                                            }
                                            <View>
                                                <Text style={styles.Supply_text}>{item}</Text>
                                                <Text style={styles.Supply_text_value}>
                                                    {
                                                        index == 0 ?
                                                            (total_budget_supply_side_cost == 0 ? 'N/A' : '\u20B9' + total_budget_supply_side_cost.toString() + ' / Wp')
                                                            :
                                                            (total_budget_service_side_cost == 0 ? 'N/A' : '\u20B9' + total_budget_service_side_cost.toString() + ' / Wp')
                                                    }
                                                </Text>
                                            </View>

                                            <View style={{ width: 30, height: 30, borderWidth: 0, borderRadius: 30, backgroundColor: 'rgb(232,232,232)' }}>
                                                <Text style={{ alignSelf: 'center', borderWidth: 0, top: 2, fontSize: hp('1%') }}>
                                                    <Icon color={'#a1a1a1'} type='feather' name='chevron-right' />
                                                </Text>
                                            </View>
                                        </View>
                                    </Card>
                                </TouchableScale>

                            }
                            contentContainerStyle={{ paddingLeft: hp('2%'), paddingRight: hp('2%'), paddingBottom: hp('1%') }}
                        />
                    </View>

                    <TouchableOpacity
                        style={{ marginTop: hp('30%'), borderRadius: 4, width: wp('70%'), height: hp('7%'), backgroundColor: '#3d87f1', alignSelf: 'center', justifyContent: 'center' }}
                        onPress={() => DocumentUpload()}
                    >
                        <Text style={styles.labelStyle}>Upload Data</Text>
                    </TouchableOpacity>

                    <View style={{ alignSelf: 'center', justifyContent: 'center', marginTop: '8%' }}>
                        <TouchableOpacity
                            onPress={() => checkPermission(
                                "https://safearth-pmt-static.s3.ap-south-1.amazonaws.com/Sheet_for_Finance_Module%2B(3)+(5)+(3).xlsx"
                            )}
                        >
                            <Text style={styles.view_format}>View Format</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            );
        }
    };
    return renderBudgetedScreen();
}

export default BudgetedScreen;

const styles = StyleSheet.create({


    Supply_text: {
        fontSize: hp('2.2%'),
        fontFamily: 'SourceSansPro-Semibold',
        color: 'rgb(68, 67, 67)',
    },

    Supply_text_value: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(179, 179, 179)',
        alignSelf: 'center'
    },

    labelStyle: {
        color: 'white',
        fontSize: hp('2.2%'),
        fontFamily: 'SourceSansPro-Regular',
        textTransform: 'capitalize',
        textAlign: 'center'
    },

    view_format: {
        fontSize: hp('2.2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: '#3d87f1',
        textDecorationLine: 'underline'
    },

});