export const list_of_budgeted_supply_side_empty = [
    {
        component: "Solar Modules",
        basic_price: 0,
        transport: 0,
        gst: 0,
        total: 0,
    },
    {
        component: "Inverters",
        basic_price: 0,
        transport: 0,
        gst: 0,
        total: 0,
    },
    {
        component: "Structures",
        basic_price: 0,
        transport: 0,
        gst: 0,
        total: 0,
    },
    {
        component: "BoS",
        total: 0,
        components: [
            { component: "Structure Foundation/↵ Pedestal", total: 10 },
            { component: "Solar DC Cables", total: 11 },
            { component: "Cabling Accessories", total: 12 },
            { component: "Easrthing Protection System", total: 13 },
            { component: "Lighting Arrestor", total: 14 },
            { component: "LT Panel", total: 15 },
            { component: "HT Panel", total: 16 },
            { component: "AC Distribution (Combiner) Panel Board", total: 17 },
            { component: "Transformer", total: 18 },
            { component: "LT Power Cable", total: 19 },
            { component: "LT Power Cable↵(From ACDB to Customer LT Panel)", total: 20 },
            { component: "LT Power Cable↵(From metering cubical to MDB panel)", total: 21 },
            { component: "Earthing Cable", total: 22 },
            { component: "Module Earthing Cable", total: 23 },
            { component: "DC Junction Box (SCB/SMB)", total: 24 },
            { component: "GI Strip", total: 25 },
            { component: "Metering Panel", total: 26 },
            { component: "MDB Breaker", total: 27 },
            { component: "Cable trays", total: 28 },
            { component: "HT Breaker", total: 29 },
            { component: "Bus bar", total: 30 },
            { component: "Reverse Protection Relay", total: 31 },
            { component: "Net-Metering", total: 32 },
            { component: "DG Syncronization", total: 33 },
            { component: "Communication Cable", total: 34 },
            { component: "Safety Rails", total: 35 },
            { component: "Walkways", total: 36 },
            { component: "Safety Lines", total: 37 },
            { component: "SCADA System", total: 38 },
            { component: "Pyro Meter", total: 39 },
            { component: "Weather Monitoring Unit", total: 40 },
            { component: "Ambient Temperature Sensors", total: 41 },
            { component: "UPS", total: 42 },
            { component: "Module Temperature Sensors", total: 43 },
            { component: "Wire mesh for protection of Skylights", total: 44 },
            { component: "Cleaning System", total: 45 },
            { component: "Danger Board and Signs", total: 46 },
            { component: "Fire Extingusiher", total: 47 },
            { component: "Generation Meter", total: 48 },
            { component: "Zero Export Device", total: 49 },
            { component: "Other Sensors", total: 50 },
        ]
    }
]


export const list_of_budgeted_service_side_empty = [
    {
        component: "Installation and Commissioning",
        indirect_cost: 0,
        direct_cost: 0,
        taxes: 0,
        total: 0,
    },
    {
        component: "Net Metering",
        indirect_cost: 0,
        direct_cost: 0,
        taxes: 0,
        total: 0,
    },
    {
        component: "Approvals",
        indirect_cost: 0,
        direct_cost: 0,
        taxes: 0,
        total: 0,
    },
    {
        component: "Sales",
        indirect_cost: 0,
        direct_cost: 0,
        taxes: 0,
        total: 0,
    },
]

export const supplySideTextValues = [
    ['Basic', 'basic_price'],
    ['Transport', 'transport'],
    ['GST+Duties', 'gst']
]

export const serviceSideTextValues = [
    ['Direct Cost', 'direct_cost'],
    ['InDirect Cost', 'indirect_cost'],
    ['Tax', 'taxes']
]

export const CATEGORY = {
    'BUDGETED': {
        'Supply Side': 'budget_supply_side',
        'Service Side': 'budget_service_side',
    },
    'ACTUAL': {
        'Supply Side': 'actual_supply_side',
        'Service Side': 'actual_service_side',
    }
}