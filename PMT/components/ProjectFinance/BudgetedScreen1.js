import {
  View,
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Dimensions,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  Animated
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { FlatList } from 'react-native-gesture-handler';
import { ButtonGroup, Input } from 'react-native-elements';
import { Avatar, Card, Divider, Button, IconButton } from 'react-native-paper';
import { Icon } from 'react-native-elements'
import {
  useCollapsibleSubHeader,
  CollapsibleSubHeaderAnimator,
} from 'react-navigation-collapsible';
import TouchableScale from 'react-native-touchable-scale';
import { useDispatch, useSelector } from 'react-redux';
import {
  list_of_budgeted_supply_side_empty,
  list_of_budgeted_service_side_empty,
  supplySideTextValues,
  serviceSideTextValues,
  CATEGORY
} from './constant'
import * as actionCreators from "../../actions/project-finance-action/index";
import config from "../../config";


const BudgetedScreen1 = (props) => {

  const {
    onScroll /* Event handler */,
    containerPaddingTop /* number */,
    scrollIndicatorInsetTop /* number */,
    translateY,
  } = useCollapsibleSubHeader();

  // Project Details
  const [projectId, setProjectId] = useState('');
  const [isSubContractor, setisSubContractor] = useState('');
  const drawerReducer = useSelector((state) => {
      return state.getProjectReducers.selected_project.data;
  });
  useEffect(() => {
      setProjectId(drawerReducer?.id)
      setisSubContractor(drawerReducer?.is_sub_contractor)
  }, [drawerReducer])

  const { list_of_budgeted_data_based_on_type, btnType, type, token } = props

  const [tempData, settempData] = useState(
    type == 'Supply Side' ?
      (
        list_of_budgeted_data_based_on_type.length > 0
          ? [...list_of_budgeted_data_based_on_type]
          : list_of_budgeted_supply_side_empty
      )
      :
      (
        list_of_budgeted_data_based_on_type?.length > 0
          ? [...list_of_budgeted_data_based_on_type]
          : list_of_budgeted_service_side_empty
      )
  )

  const [textValues, settextValues] = useState(
    type == 'Supply Side' ? supplySideTextValues : serviceSideTextValues
  )
  const [category, setcategory] = useState(CATEGORY[btnType][type]);
  const [isExpanded, setisExpanded] = useState(false)
  const [selectedIndex, setselectedIndex] = useState(null)
  const [UpdateBtnVisible, setUpdateBtnVisible] = useState(false);

  const onPressBackArrow = (params) => {
    props.setisClickOnArrow(false)
  }

  const onPressExpandedCard = (index) => {
    setisExpanded(!isExpanded)
    setselectedIndex(index)
  }

  const onChangeInput = (index, text, type) => {
    let value = parseFloat(text)
    if (!isNaN(value)) {
      let newArr = [...tempData];
      newArr[index] = {
        ...tempData[index],
        [type]: value
      };
      settempData([...newArr])
    }
    setUpdateBtnVisible(true)
  }

  const onChangeInputForBos = (position, bosIndex, text) => {
    let value = parseFloat(text)
    if (!isNaN(value)) {
      let newArr = [...tempData];
      newArr[position]['components'][bosIndex] = {
        ...newArr[position]['components'][bosIndex],
        total: value
      }
      settempData([...newArr])
      setUpdateBtnVisible(true)
    }
  }

  const dispatch = useDispatch();
  const handleUpdate = (index) => {
    dispatch(actionCreators.updateBudgetedData(token, category, tempData[index], projectId));
    setUpdateBtnVisible(false);
  };

  return (
    <View style={{ paddingTop: '5%', marginHorizontal: '4%', height: hp('82%') }}>
      <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: hp('2%') }}>
        <Icon color={'#a1a1a1'} onPress={() => onPressBackArrow()} type='feather' name='chevron-left' />
        <Text style={styles.Supply_text}>{props.type}</Text>
      </View>
      <View style={{ marginBottom: hp('2%') }}>
        <FlatList
          backgroundColor='#ffffff'
          data={tempData}
          keyExtractor={(item, index) => index}
          renderItem={({ item, index }) => {
            return (
              <Card style={{ width: wp('86.7%'), alignSelf: 'center', elevation: 5, paddingRight: wp('4%'), backgroundColor: '#fff', shadowColor: 'rgba(0,0,0,0.4)', marginBottom: hp('2%'), }}>
                <TouchableScale
                  onPress={() => onPressExpandedCard(index)}
                  activeScale={1}
                >
                  <View style={{ flexDirection: 'row', height: hp('7.5%'), paddingLeft: wp('3%'), justifyContent: 'space-between', alignItems: 'center' }}>
                    <Text style={[styles.component, { width: UpdateBtnVisible ? wp('25%') : wp('50%') }]}>{item.component}</Text>
                    {
                      UpdateBtnVisible && (isExpanded && index == selectedIndex) &&
                      <TouchableOpacity
                        onPress={() => handleUpdate(index)}
                        style={{ backgroundColor: '#3d87f1', width: wp('20%'), justifyContent: 'center', alignItems: 'center', borderRadius: 4, height: hp('4%') }}
                      >
                        <Text style={{ color: 'white' }}>Update</Text>
                      </TouchableOpacity>
                    }
                    <Text style={[styles.component, { width: wp('20%') }]}>{'\u20B9' + ' ' + item.total}</Text>
                    <Icon color={'#a1a1a1'} type='feather' name={(isExpanded && index == selectedIndex) ? 'chevron-up' : 'chevron-down'} />
                  </View>
                </TouchableScale>

                {
                  (isExpanded && index == selectedIndex) &&
                  <>
                    {
                      textValues.map((i, textValuesindex) =>
                        item.component != 'BoS' ?
                          <KeyboardAvoidingView key={textValuesindex} keyboardVerticalOffset={10} behavior={'position'}>
                            <View
                              style={{ flexDirection: 'row', borderWidth: 0, padding: hp('1.5%'), alignItems: 'center', justifyContent: 'space-between' }}
                            >
                              <Text style={[styles.titleTxt, { width: wp('40%') }]}>{i[0]}</Text>
                              <Text style={styles.costText}>{'\u20B9'+' /' + item['units']+'  '}</Text>
                              <Input
                                onChangeText={text => onChangeInput(index, text, i[1])}
                                inputStyle={{ fontSize: hp('2%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                defaultValue={item[i[1]].toString()}
                                inputContainerStyle={{ borderBottomWidth: 0 }}
                                containerStyle={styles.textInput}
                                keyboardType='numeric'
                              />
                            </View>
                          </KeyboardAvoidingView>
                          :
                          <View key={textValuesindex}>
                            {
                              item.components.map((h, bosIndex) =>
                                <View key={bosIndex}>
                                  <View style={{ flexDirection: 'row', paddingLeft: wp('2%'), height: hp('7.5%'), alignItems: 'center' }}>
                                    <Text style={[styles.titleTxt, { width: wp('45%') }]}>{h['component']}</Text>
                                    <Text style={styles.costText}>{'\u20B9'+' /' + h['units']+'  '}</Text>
                                    <Input
                                      onChangeText={text => onChangeInputForBos(index, bosIndex, text)}
                                      inputStyle={{ fontSize: hp('2%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                      defaultValue={h['total'].toString()}
                                      inputContainerStyle={{ borderBottomWidth: 0 }}
                                      containerStyle={styles.textInput}
                                      keyboardType='numeric'
                                    />
                                  </View>
                                  <Divider style={{ backgroundColor: 'rgb(232,232,232)', marginTop: hp('1%'), height: hp('0.1%') }} />
                                </View>
                              )
                            }
                          </View>
                      )
                    }
                    <View style={[styles.btn, { marginLeft: wp('3%') }]}>
                      <Text style={styles.totalTxt}>Total</Text>
                      <Text style={styles.totalTxt}>{'\u20B9' + ' ' + item.total + '/'+item.units+' '}</Text>
                    </View>
                  </>
                }
              </Card>
            );
          }}
        />
      </View>

    </View>
  );
}

export default BudgetedScreen1;

const styles = StyleSheet.create({

  Supply_text: {
    fontSize: hp('2.8%'),
    fontFamily: 'SourceSansPro-Semibold',
    color: 'rgb(179, 179, 179)',
    borderWidth: 0,
    paddingLeft: '4%'
  },

  component: {
    fontSize: hp('2.3%'),
    fontFamily: 'SourceSansPro-Semibold',
    color: 'rgb(68, 67, 67)'
  },

  titleTxt: {
    fontSize: hp('2%'),
    fontFamily: 'SourceSansPro-Regular',
    color: 'rgb(68, 67, 67)',
  },

  costText: {
    fontSize: hp('1.8%'),
    fontFamily: 'SourceSansPro-Regular',
    color: 'rgb(194, 194, 194)',
  },

  totalTxt: {
    fontSize: hp('2%'),
    fontFamily: 'SourceSansPro-Regular',
    color: '#3d87f1',
  },

  btn: {
    height: hp('6.7%'),
    paddingHorizontal: '4%',
    marginBottom: '5%',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#ecf3fe',
    borderRadius: 4,
    justifyContent: 'space-between'
  },

  textInput: {
    width: wp('25.6%'),
    height: hp('5.8%'),
    borderRadius: 4,
    borderColor: 'rgb(232, 232, 232)',
    borderWidth: 1,
    color: 'black',
    textAlign: 'center'
  }





});
