import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    useWindowDimensions,
    Animated
} from 'react-native';
import React, { useState, useEffect, useMemo } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { useDispatch, useSelector } from 'react-redux';
import notifyMessage from '../../ToastMessages'
import { Avatar, Card, Button, List, Divider, IconButton } from 'react-native-paper';
import color from 'color';
import { FlatList } from 'react-native';
import {
    useCollapsibleSubHeader,
    CollapsibleSubHeaderAnimator,
} from 'react-navigation-collapsible';
import { FAB, Icon, BottomSheet, Input } from 'react-native-elements';
import DocumentPicker from 'react-native-document-picker';
import Toast from "react-native-toast-message";


const onPressUploadDocument = async (props) => {
    //('xxxxx', props)
    try {
        const res = await DocumentPicker.pick({
            type: [DocumentPicker.types.xlsx, DocumentPicker.types.xls ],
        });
        // //(res)
        let temp = {
            uri: res.uri,
            type: res.type,
            name: res.name
        }
       props(temp)
    } catch (err) {
        //(err)
        if (DocumentPicker.isCancel(err)) {
        } else {
            throw err;
        }
    }
}

export default onPressUploadDocument;