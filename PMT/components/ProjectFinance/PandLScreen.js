import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { FlatList } from 'react-native-gesture-handler';
import { ButtonGroup } from 'react-native-elements';
import { Avatar, Card, Divider, Button, IconButton } from 'react-native-paper';
import { Icon } from 'react-native-elements'
import TouchableScale from 'react-native-touchable-scale';
import { CheckBox } from 'react-native-elements'
import * as actionCreators from '../../actions/project-finance-action/index'
import notifyMessage from "../../ToastMessages";
import { useDispatch, useSelector } from 'react-redux';
import { black } from 'react-native-paper/lib/typescript/styles/colors';


const PandLScreen = (props) => {

    const token = props.token

    const [basicPrice, setBasicPrice] = useState("");
    const [gst, setGst] = useState("");
    const [totalPrice, setTotalPrice] = useState("");
    const [size, setSize] = useState("");

    const [displayResult, setDisplayResult] = useState(false);

    const [signingPrice, setSigningPrice] = useState("");
    const [epcPriceTarget, setEpcPriceTarget] = useState("");
    const [opexSize, setopexSize] = useState("");

    const [selectedIndex, setselectedIndex] = useState(0)
    const mode = ['CAPEX', 'OPEX']

    const onPressCheckedBox = (index) => {
        setselectedIndex(index)
        setDisplayResult(false)
    }

    const onChangetxtInput = (placeholder, text) => {
        if (placeholder == 'Basic') {
            setBasicPrice(text)
        } else if (placeholder == 'GST') {
            setGst(text)
        } else if (placeholder == 'Total') {
            setTotalPrice(text)
        } else if (placeholder == 'Size(Kw)') {
            setSize(text)
        } else if (placeholder == 'Total(/Wp)') {
            setEpcPriceTarget(text)
        } else if (placeholder == 'Basic(Kwh)') {
            setSigningPrice(text)
        } else if (placeholder == 'Size(KW)') {
            setopexSize(text)
        }
    }

    const dispatch = useDispatch();

    const handleCalculate = () => {
        setDisplayResult(true);
        if (selectedIndex === 0) {
            if (basicPrice && gst && totalPrice && size) {
                const data = {
                    mode: 'CAPEX',
                    basicPrice: basicPrice,
                    gst: gst,
                    totalPrice: totalPrice,
                    size: size,
                };
                dispatch(actionCreators.calculatePnL(token, data, 9));
            } else {
                notifyMessage("Please fill all the fields!");
            }
        } else {
            if (signingPrice && epcPriceTarget && opexSize) {
                const data = {
                    mode: 'OPEX',
                    signingPrice: signingPrice,
                    epcPriceTarget: epcPriceTarget,
                    size: opexSize,
                };
                dispatch(actionCreators.calculatePnL(token, data, 9));
            } else {
                notifyMessage("Please fill all the fields!");
            }
        }
    };

    const projectFinanceReducer = useSelector((state) => {
        return state.projectFinanceReducer.pnlDetails.get;
    });

    const onPressReCalculate = (params) => {
        setDisplayResult(false)
        if (selectedIndex == 0) {
            setBasicPrice('')
            setGst('')
            setSize('')
            setTotalPrice('')
        } else {
            setSigningPrice('')
            setEpcPriceTarget('')
            setopexSize('')
        }
    }

    const valueDict = {
        'Basic': basicPrice,
        'GST': gst,
        'Total': totalPrice,
        'Size(Kw)': size,
        'Total(/Wp)': epcPriceTarget,
        'Basic(Kwh)': signingPrice,
        'Size(KW)': opexSize
    }

    const txtInputBtn = (placeholder, width, height, backgroundColor) => {
        return (
            <TextInput
                style={{ width: wp(width), color: 'black', height: hp(height), marginBottom: hp('1%'), paddingStart: 15, borderRadius: 4, marginLeft: '2.5%', backgroundColor: backgroundColor }}
                placeholder={placeholder}
                placeholderTextColor={'rgb(179,179,179)'}
                onChangeText={text => onChangetxtInput(placeholder, text)}
                keyboardType='numeric'
                value={valueDict[placeholder]}
            >
            </TextInput>
        );
    }

    const resultData = ['Revenue', 'Margin', 'Margin %', 'Time', 'Revenue per Day']

    return (
        <ScrollView>
            <View style={{ marginHorizontal: wp('7%'), borderWidth: 0, marginTop: hp('3%'), }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                    {
                        mode.map((i, index) =>
                            <CheckBox
                                key={index}
                                containerStyle={{ width: wp('38%'), height: hp('7%'), backgroundColor: 'white' }}
                                left
                                title={i}
                                checkedIcon='dot-circle-o'
                                uncheckedIcon='circle-o'
                                checked={index == selectedIndex}
                                onPress={() => onPressCheckedBox(index)}
                                uncheckedColor={'#3d87f1'}
                                checkedColor={'#3d87f1'}
                            />)
                    }
                </View>

                {
                    selectedIndex == 0 ?
                        <View>
                            <Text style={styles.signing_text}>Signing Price</Text>
                            {txtInputBtn('Basic', '80.4%', '7.3%', '#f7f7f7')}
                            <View style={{ flexDirection: 'row' }}>
                                {txtInputBtn('GST', '60%', '7.3%', '#f7f7f7')}
                                <Text style={styles.gstTxt}>at 8.9%</Text>
                            </View>
                            {txtInputBtn('Total', '80.4%', '7.3%', '#f7f7f7')}
                            <Text style={styles.signing_text}>Size</Text>
                            {txtInputBtn('Size(Kw)', '80.4%', '7.3%', '#f7f7f7')}
                        </View>
                        :
                        <View>
                            <Text style={styles.signing_text}>Signing Price</Text>
                            {txtInputBtn('Basic(Kwh)', '80.4%', '7.3%', '#f7f7f7')}
                            <Text style={styles.signing_text}>EPC Price Target</Text>
                            {txtInputBtn('Total(/Wp)', '80.4%', '7.3%', '#f7f7f7')}
                            <Text style={styles.signing_text}>Size</Text>
                            {txtInputBtn('Size(KW)', '80.4%', '7.3%', '#f7f7f7')}
                        </View>
                }

                <TouchableOpacity
                    style={styles.btnStyle}
                    onPress={() => handleCalculate()}
                >
                    <Text style={styles.btnTxt}>
                        Calculate
                    </Text>
                </TouchableOpacity>
            </View>

            {
                (displayResult && projectFinanceReducer?.success?.ok) &&
                <View style={{ borderWidth: 0, marginVertical: hp('8%') }}>
                    <View style={{ flexDirection: 'row', paddingHorizontal: wp('5%'), justifyContent: 'space-between' }}>
                        <Text style={styles.signing_text}>Actual Price</Text>
                        <Text style={styles.signing_text}>{`₹ ${projectFinanceReducer?.success?.data?.p_and_l?.actual_cost} / Wp`}</Text>
                    </View>
                    {
                        resultData.map((i, index) => {
                            const data = projectFinanceReducer?.success?.data?.p_and_l
                            const valuesList = [
                                `₹ ${data.revenue} / Wp`,
                                `₹ ${data.margin} / Wp`,
                                `${data.margin_percentage}%`,
                                `${data.time} days`,
                                `₹ ${data.revenue_per_day} / day`
                            ]
                            return (
                                <View key={index} style={{ flexDirection: 'row', height: hp('7.3%'), alignItems: 'center', paddingHorizontal: wp('5%'), backgroundColor: index % 2 == 0 ? 'rgba(179, 179, 179, 0.1)' : 'white', justifyContent: 'space-between' }}>
                                    <Text style={styles.signing_text1}>{i}</Text>
                                    <Text style={styles.resultTxt}>{valuesList[index]}</Text>
                                </View>
                            )
                        })
                    }
                    <View style={{ flexDirection: 'row', marginTop: hp('5%'), marginHorizontal: wp('8%'), justifyContent: 'space-between' }}>
                        <Button
                            style={{ width: '40%', backgroundColor: '#3d87f1' }}
                            mode="contained" onPress={() => null}>
                            Back
                        </Button>
                        <Button
                            style={{ width: wp('40%'), borderWidth: 1, borderColor: '#3d87f1', backgroundColor: 'white' }}
                            labelStyle={{ color: '#3d87f1', fontSize: hp('1.6%') }}
                            mode="contained" onPress={() => onPressReCalculate(selectedIndex)}>
                            Re-Calculate
                        </Button>
                    </View>
                </View>
            }

        </ScrollView>
    );
}

export default PandLScreen;

const styles = StyleSheet.create({


    signing_text: {
        fontSize: hp('2.4%'),
        fontFamily: 'SourceSansPro-Semibold',
        color: 'rgb(68, 67, 67)',
        marginVertical: hp('2%'),
        paddingLeft: '2.5%',
        fontWeight: 'bold',
    },

    signing_text1: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(179, 179, 179)',
        paddingLeft: '2.5%',
    },

    resultTxt: {
        color: '#3d87f1',
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
    },

    gstTxt: {
        width: wp('18%'),
        marginLeft: '3%',
        paddingTop: '4%',
        textAlign: 'center',
        borderRadius: 4,
        height: hp('7.3%'),
        color: '#3d87f1',
        backgroundColor: 'rgb(236, 243, 254)'
    },

    btnStyle: {

        backgroundColor: '#3d87f1',
        borderRadius: 4,
        marginTop: '15%',
        marginLeft: '3%',
        width: wp('80%'),
        height: hp('8%'),
        alignItems: 'center',
        justifyContent: 'center'
    },

    btnTxt: {
        color: 'white',
        fontSize: hp('2.2%'),
        fontFamily: 'SourceSansPro-Regular',
    },
});