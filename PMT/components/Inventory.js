import { View, Text } from 'react-native'
import React, { useEffect, useState } from "react";
import * as actionCreators from '../actions/inventory-action/index';
import { useDispatch, useSelector } from "react-redux";
import GetInventory from '../components/Inventory/GetInventory'
import notifyMessage from '../ToastMessages';
import { parseSync } from '@babel/core';
import { heightPercentageToDP } from 'react-native-responsive-screen';
import EmptyInventory from '../components/EmptyScreens/EmptyInventory'
import EmptyLoader from '../components/EmptyLoader';
import { useIsFocused } from "@react-navigation/native";

const Inventory = (props) => {

    const { route, navigation } = props;

    const dispatch = useDispatch();

    const token = route.params.token

    const [reload, setReload] = useState(false);
    const changeReload = () => {
      setReload(!reload)
    }

    React.useEffect(() => {
        if (props.route.params?.doFilter) {
            let value = props.route.params.value
            if (props.route.params?.isSort) {
                dispatch(actionCreators.getInventory(token, isSort = true, isFilter = false, value = value));
            } else {
                dispatch(actionCreators.getInventory(token, isSort = false, isFilter = true, value = value));
            }
        } else {
            dispatch(actionCreators.getInventory(token, isSort = false, isFilter = false, value = []));
        }
    }, [props, reload]);

    const inventoryReducer = useSelector((state) => {
        return state.inventoryReducer.inventory.get;
    });

    const renderInventory = () => {
        if (inventoryReducer.success.ok) {
            return <GetInventory token={token} changeReload={changeReload} navigation={navigation} inventory={inventoryReducer.success.data} />;
        } else if (inventoryReducer.failure.error) {
            return <View></View>
        } else {
            return (
                < EmptyLoader />
            )
        }
    };
    return renderInventory();
};

export default Inventory;
