import React, { useEffect, useContext } from 'react';
import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    PermissionsAndroid,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    Linking 

} from 'react-native';
import { Searchbar, Button,Switch  } from 'react-native-paper';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import * as actionCreator from '../actions/docs-action/index';
import { useDispatch, useSelector } from 'react-redux';
import { FlatList } from 'react-native-gesture-handler';
import FastImage from 'react-native-fast-image'
import EmptyScreenHeader from './EmptyScreens/EmptyScreenHeader';
import { AuthContext } from '../context';

const Settings = (props) => {
    //('Settings props',props)
    const [email, setemail] = React.useState('');

    const [isSwitchOn1, setIsSwitchOn1] = React.useState(false);
    const onToggleSwitch1 = () => setIsSwitchOn1(!isSwitchOn1);

    const [isSwitchOn2, setIsSwitchOn2] = React.useState(false);
    const onToggleSwitch2 = () => setIsSwitchOn2(!isSwitchOn2);

    const [isSwitchOn3, setIsSwitchOn3] = React.useState(false);
    const onToggleSwitch3 = () => setIsSwitchOn3(!isSwitchOn3);

    const [isSwitchOn4, setIsSwitchOn4] = React.useState(false);
    const onToggleSwitch4 = () => setIsSwitchOn4(!isSwitchOn4);

    const [isSwitchOn5, setIsSwitchOn5] = React.useState(false);
    const onToggleSwitch5 = () => setIsSwitchOn5(!isSwitchOn5);

    const authContext = useContext(AuthContext);
    const {signOut} = authContext

    useEffect(async () => {
        const loginDetails = await AsyncStorage.getItem('loginDetails');
        const loginvalue = JSON.parse(loginDetails);
        setemail(loginvalue.email)

      }, []);

    return (
        <>
                <View style={styles.container}>
                    <EmptyScreenHeader {...props} title='Settings' />

                    <View style={styles.parent2}>
                    <ScrollView>

                        <View style={{ flexDirection: 'column', marginLeft: wp('10%'), marginRight: wp('10%') }}>
                            <View style={{ flexDirection: 'row', marginTop: hp('4.5%')}}>
                                <FastImage
                                    style={{ height: 25, width: 25 }}
                                    source={require("../assets/icons/Personal.png")}
                                    resizeMode={FastImage.resizeMode.contain}
                                />
                                <Text style={{ fontSize: 20,marginLeft:wp('2%'), fontFamily: 'SourceSansPro-Semibold', fontWeight: 'bold', color: 'rgb(68, 67, 67)' }}>
                                    Personal Details</Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: hp('4.5%'), justifyContent: 'space-between' }}>
                                <Text style={styles.txt}>Email</Text>
                                <Text style={styles.txt}>{email}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: hp('4.5%'), justifyContent: 'space-between' }}>
                                <Text style={styles.txt}>Password</Text>
                                <Text style={styles.txt}>*********42</Text>
                            </View >
                            <View style={{marginTop: hp('1%')}}>
                            <TouchableOpacity onPress={() => props.navigation.navigate('account',{email:email})}>
                                <Text  style={styles.txtunder}>Change password</Text>
                                </TouchableOpacity>
                            </View>

                        </View>


                        <View style={{ flexDirection: 'column', marginLeft: wp('10%'), marginRight: wp('10%'), marginTop: hp('7%') }}>
                            <View style={{ flexDirection: 'row' }}>
                                <FastImage
                                    style={{ height: 25, width: 25 }}
                                    source={require("../assets/icons/Notifications.png")}
                                    resizeMode={FastImage.resizeMode.contain}
                                />
                                <Text style={{ fontSize: 20, marginLeft:wp('2%'), fontFamily: 'SourceSansPro-Semibold', fontWeight: 'bold', color: 'rgb(68, 67, 67)' }}>
                                    Notifications & Reminders</Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: hp('4.5%'), justifyContent: 'space-between' }}>
                                <Text style={styles.txt}>Daily Reports</Text>
                                <Switch color={'rgb(61,135,241)'} value={isSwitchOn1} onValueChange={onToggleSwitch1} />
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: hp('4.5%'), justifyContent: 'space-between' }}>
                                <Text style={styles.txt}>Task List</Text>
                                <Switch color={'rgb(61,135,241)'}  value={isSwitchOn2} onValueChange={onToggleSwitch2} />
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: hp('4.5%'), justifyContent: 'space-between' }}>
                                <Text style={styles.txt}>Detailed Event</Text>
                                <Switch color={'rgb(61,135,241)'}  value={isSwitchOn3} onValueChange={onToggleSwitch3} />
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: hp('4.5%'), justifyContent: 'space-between' }}>
                                <Text style={styles.txt}>Recent Activities</Text>
                                <Switch color={'rgb(61,135,241)'}  value={isSwitchOn4} onValueChange={onToggleSwitch4} />
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: hp('4.5%'), justifyContent: 'space-between' }}>
                                <Text style={styles.txt}>Current Activities</Text>
                                <Switch color={'rgb(61,135,241)'}  value={isSwitchOn5} onValueChange={onToggleSwitch5} />
                            </View>


                        </View>


                        <View style={{ flexDirection: 'column', marginTop: hp('7%'), marginLeft: wp('10%'), marginRight: wp('10%') }}>
                            <View style={{ flexDirection: 'row' }}>
                                <FastImage
                                    style={{ height: 25, width: 25 }}
                                    source={require("../assets/icons/General.png")}
                                    resizeMode={FastImage.resizeMode.contain}
                                />
                                <Text style={{ fontSize: 20, marginLeft:wp('2%'), fontFamily: 'SourceSansPro-Semibold', fontWeight: 'bold', color: 'rgb(68, 67, 67)' }}>
                                    General</Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: hp('4.5%'), justifyContent: 'space-between' }}>
                                <Text style={styles.txt}>About SafEarth</Text>
                                <TouchableOpacity onPress={() => Linking.openURL('https://safearth.in/')}>
                                <Text style={styles.textblue}>Visit</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: hp('4.5%'), justifyContent: 'space-between' }}>
                                <Text style={styles.txt}>Privacy Policy</Text>
                                <TouchableOpacity onPress={() => Linking.openURL('https://safearth.in/privacy-policy')}>
                                <Text  style={styles.textblue}>Visit</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: hp('4.5%'), justifyContent: 'space-between' }}>
                                <Text style={styles.txt}>Terms & Conditions</Text>
                                <TouchableOpacity onPress={() => Linking.openURL('https://safearth-pmt-static.s3.ap-south-1.amazonaws.com/Safearth+PMT+-+Terms+of+Use+-+Final+Draft.pdf')}>
                                <Text  style={styles.textblue}>Visit</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: hp('4.5%'), justifyContent: 'space-between' }}>
                                <Text style={styles.txt}>Report a Bug</Text>
                                <Text  style={styles.textblue}>Report</Text>
                            </View>
                            <View style={{marginTop:hp('1%')}}>
                            <Text style={{fontSize:hp('1.8%'),fontFamily:'SourceSansPro-Regular', color:'rgb(143,143,143)'}}>We will respond via email to feedback and questions.
                            You may also send feedback to
                            <Text style={{color:'rgb(61,135,241)'  }}> info@safearth.in </Text></Text>
                            </View>
                            
        
                            <TouchableOpacity
                                onPress={() => signOut()}
                                style={[styles.btn, {marginTop:hp('4%'), backgroundColor: '#3d87f1' }]}
                            >
                                <Text style={[styles.btnText, { color: 'white' }]}>Log out</Text>
                            </TouchableOpacity>

                        </View>
                        <View style={{height:hp('8%')}}>
                            </View>
                        </ScrollView>

                    </View>

                </View>
        </>

    )
};
const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    parent2: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        flex: 11,
        backgroundColor: 'white',

    },
    option_text: {
        fontSize: hp('1.2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center'
    },
    signIn: {
        width: wp('74%'),
        height: hp('8%'),
        left: 25,
        top: 80,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        color: '#05375a',
        backgroundColor: '#3A80E3',
        borderRadius: 5,
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: 'SourceSansPro-Regular',
    },
    txt: {
        fontSize:hp('2.5%'),
        fontFamily:'SourceSansPro-Regular', 
        color:'rgb(143,143,143)'
    },
    txtunder: {
        fontSize:hp('2.5%'),
        textDecorationLine:'underline',
        fontFamily:'SourceSansPro-Regular', 
        color:'rgb(61,135,241)'
    },
    textblue: {
        fontSize:hp('2.5%'),
        fontFamily:'SourceSansPro-Regular', 
        color:'rgb(61,135,241)'
    },
    btn: {
        height:hp('10%'),
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        width: wp('80%'),
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnText: {
        fontSize: hp('3%'),
        fontFamily: 'SourceSansPro-Regular'
    }

});
export default Settings