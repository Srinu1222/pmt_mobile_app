import { View, Text } from 'react-native'
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import notifyMessage from '../ToastMessages';
import { parseSync } from '@babel/core';
import { heightPercentageToDP } from 'react-native-responsive-screen';
import EmptyLoader from '../components/EmptyLoader';
import { useIsFocused } from "@react-navigation/native";
import * as actionCreators from '../actions/docs-action/index';
import GetPhotos from '../components/Photos/GetPhotos';

const Photos = (props) => {

    const { route, navigation } = props;

    const dispatch = useDispatch();

    const token = route.params.token

    useEffect(() => {
        dispatch(actionCreators.getDocs(token, true));
    }, []);

    const photosReducers = useSelector(state => {
        return state.docsReducers.docs.get;
    });

    const renderPhotos = () => {
        if (photosReducers.success.ok) {
            return <GetPhotos token={token} navigation={navigation} getPhotos={photosReducers.success.data.basic_project_documents_details} />;
        } else if (photosReducers.failure.error) {
            return notifyMessage(photosReducers.failure.message);
        } else {
            return (
                < EmptyLoader />
            )
        }
    };
    return renderPhotos();
};

export default Photos;
