import React from 'react';
import {
  View,
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Dimensions,
  Image,
  Platform,
} from 'react-native';
import LottieView from 'lottie-react-native';
import CustomStatusBar from './CustomStatusBar'
import FastImage from 'react-native-fast-image';

const SplashScreen = (props) => {

  setTimeout(() => {
    props.changeLoading();
  }, 100);
  return (
    <>
      <StatusBar style={{ height: Platform.OS == 'android' ? StatusBar.currentHeight : 0 }} backgroundColor={'#fff'} barStyle="light-content"
      />
      <View style={styles.container}>
        <View style={{ backgroundColor: '#fff'}}>
          <FastImage
                    resizeMode={FastImage.resizeMode.contain}
            style={{ height: 400, width: 400, alignSelf: 'center' }}
            source={{uri: Image.resolveAssetSource(require('../assets/icons/SolarFlowSplash.png')).uri}}

          />
        </View>

        <View style={{ alignItems: 'center' }}>
          <FastImage
          style={{width:60, height:60, marginBottom: -100}}
          resizeMode={FastImage.resizeMode.contain}
          source={{uri: Image.resolveAssetSource(require('../assets/icons/Powered_by_SafEarth2.png')).uri}}
          />

          <Image
            style={{ height: 150, width: Dimensions.get('window').width, marginBottom: -10 }}
                resizeMode={FastImage.resizeMode.contain}
          source={{uri: Image.resolveAssetSource(require('../assets/icons/Powered_by_SafEarth2.png')).uri}}
            source={require('../assets/icons/Flow2x.png')}
          />
        </View>

        {/* <LottieView source={require('../assets/animations/animation.json')} autoPlay loop={false}  onAnimationFinish={() => props.changeLoading()}/> */}
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',

    // display: 'flex',
    // justifyContent: 'center',
    backgroundColor: '#fff',
    // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },

  text: {
    color: 'red',
    fontSize: 25,
  },
});

export default SplashScreen;
