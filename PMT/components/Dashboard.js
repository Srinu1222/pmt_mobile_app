import { View, Text } from 'react-native'
import React, { useEffect } from "react";
import * as actionCreators from '../actions/project-action/index';
import { useDispatch, useSelector } from "react-redux";
import GetDashboard from '../components/Dashboard/GetDashboard'
import notifyMessage from '../ToastMessages';
import { parseSync } from '@babel/core';
import { heightPercentageToDP } from 'react-native-responsive-screen';
import EmptyInventory from '../components/EmptyScreens/EmptyInventory'
import EmptyLoader from '../components/EmptyLoader'
import EmptyScreenHeader from './EmptyScreens/EmptyScreenHeader';

const Dashboard = ({ route, navigation }) => {

    const dispatch = useDispatch();

    const token = route.params.token


    useEffect(() => {
        dispatch(actionCreators.getProjects(token));
    }, []);

    const dashboardReducer = useSelector((state) => {
        return state.getProjectReducers.projects.get;
    });

    const renderDashboard = () => {
            return (
            <>
            <EmptyScreenHeader  navigation={navigation} title='Dashboard'/>
            {dashboardReducer.success.ok ?

<GetDashboard navigation={navigation} dashboard={dashboardReducer.success.data} token={token}/>
 :            
                < EmptyLoader />
    }
        </>
            )
    };
    return renderDashboard();
};

export default Dashboard;
