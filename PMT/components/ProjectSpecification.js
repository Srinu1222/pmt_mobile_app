import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    Button
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { useDispatch, useSelector } from 'react-redux';
// import * as actionCreator from '../../actions/team-action/index';
import { AuthContext } from '../../context';
import { SearchBar, Icon } from 'react-native-elements';
import { FlatList } from 'react-native-gesture-handler';
// import Pagination from '../Pagination'
import EmptyScreenHeader from '../components/EmptyScreens/EmptyScreenHeader'
import { Avatar, Card, IconButton } from 'react-native-paper';
import config from '../config'
import ProjectSpecification1 from './ProjectSpecification/ProjectSpecification1';
import TouchableScale from 'react-native-touchable-scale';


const ProjectSpecification = (props) => {
    
    const stages = ['Approvals', 'Engineering', 'Procurement', 'Material Handling', 'Construction', 'Site Hand over']

    const onPressArrow = (index) => {
        props.navigation.navigate('project_specification_stage_list', { stage: stages[index], project_id: props.route.params.project_id })
    }

    return (
        <View style={styles.container}>
            <EmptyScreenHeader {...props} title='Specification' />
            <View style={styles.body}>
                <View
                    style={{ flexDirection: 'row', flexWrap: 'wrap', }}
                >
                    {
                        stages.map(
                            (item, index) =>
                                <TouchableScale
                                    key={index}
                                    onPress={() => onPressArrow(index)}
                                    activeScale={0.95}
                                >
                                    <Card
                                        style={{ elevation: 8, shadowColor: 'rgba(0,0,0,0.4)', marginVertical: hp('1%'), marginHorizontal: index % 2 == 0 ? wp('4%') : wp('2%'), width: wp('42%'), backgroundColor: '#fff', }}
                                    >
                                        <View style={{ flexDirection: 'row', height: hp('7.4%'), justifyContent: 'space-between', alignItems: 'center' }}>
                                            <Text style={styles.stageText}>{item}</Text>
                                            <Icon color={'rgb(143, 143, 143)'} type='feather' name='chevron-right' />
                                        </View>
                                    </Card>
                                </TouchableScale>
                        )
                    }
                </View>
            </View>
        </View>
    );

};


const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: "white",
        flex: 1,
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    body: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        flex: 11,
        backgroundColor: 'white',
        paddingTop: hp('2%')
    },

    stageText: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        textTransform: 'capitalize',
        paddingLeft: wp('2%')
    },

});

export default ProjectSpecification;
