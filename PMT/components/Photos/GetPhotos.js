import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    Animated,
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image';
import { useDispatch, useSelector } from 'react-redux';
import * as actionCreator from '../../actions/team-action/index';
import { AuthContext } from '../../context';
import { SearchBar } from 'react-native-elements';
import { FlatList } from 'react-native-gesture-handler';
import EmptyScreenHeader from '../EmptyScreens/EmptyScreenHeader';
import Pagination from '../Pagination';
import {
    useCollapsibleSubHeader,
    CollapsibleSubHeaderAnimator,
} from 'react-navigation-collapsible';
import { FAB, Icon, BottomSheet } from 'react-native-elements';
import EmptyLoader from '../EmptyLoader'
import { Button } from 'react-native-paper'
import ElevatedView from '../ElevatedView';
import TouchableScale from 'react-native-touchable-scale';

// import EmptyPhotos from '../EmptyScreens/EmptyPhotos';

const GetPhotos = props => {

    const {
        onScroll /* Event handler */,
        containerPaddingTop /* number */,
        scrollIndicatorInsetTop /* number */,
        translateY,
    } = useCollapsibleSubHeader();

    const { token, getPhotos, navigation } = props;

    const EmptyPhotos = (params) => {
        return (
            <View style={{ display: 'flex', alignItems: 'center' }}>
                < FastImage
                    style={styles.groupimage}
                    source={require('../../assets/icons/noPhotos.png')
                    }
                    resizeMode={FastImage.resizeMode.contain}
                />
                <Text style={styles.textMessage}>No Albums.</Text>
            </View>
        );
    }

    return (
        <SafeAreaView style={{ backgroundColor: '#fff', height: '100%' }}>
            <Animated.FlatList
                ListEmptyComponent={EmptyPhotos}
                keyExtractor={(item, index) => index}
                data={getPhotos}
                renderItem={({ item, index }) =>
                    <TouchableScale activeScale={0.99} friction={10} tension={0}
                        onPress={() => navigation.navigate('projectPhotos', { projectId: item.project_id, projectName: item.project_name })}
                    >
                        <View
                            style={{ borderWidth: 1, padding: '3%', width: wp('42%'), marginVertical: '3%', marginLeft: wp('3%'), justifyContent: 'space-between', borderColor: 'rgb(234, 234, 234)' }}
                        >
                            <View>
                                <Text style={styles.projectName}>{item.project_name}</Text>
                                <Text style={styles.option_text}>{item.total_files_count} Images</Text>
                            </View>
                            <Text style={styles.count}>Last updated 5h ago</Text>
                        </View>
                    </TouchableScale>
                }
                onScroll={onScroll}
                horizontal={false}
                numColumns={2}
                contentContainerStyle={{
                    paddingTop: containerPaddingTop,
                    paddingLeft: hp('2%'),
                    paddingRight: hp('2%'),
                    paddingBottom: hp('8%'),
                    marginTop: hp('3%'),
                    alignItems: 'center'
                }}
                scrollIndicatorInsets={{ top: scrollIndicatorInsetTop }}
            />
            <CollapsibleSubHeaderAnimator translateY={translateY}>
                <ElevatedView elevation={4}
                    style={{
                        justifyContent: 'center', width: '100%', height: 'auto',
                        backgroundColor: '#fff',
                    }}
                >
                    <Text style={styles.allAlbums}>All Project Albums {'(' + getPhotos.length + ')'}</Text>
                </ElevatedView >
            </CollapsibleSubHeaderAnimator>
            {/* <FAB
                size="large"
                placement="right"
                buttonStyle={{ backgroundColor: '#000000' }}
                icon={<Icon name="camera" color="white" type="feather" />}
                onPress={() => props.navigation.navigate('addPhotos')}
            /> */}
        </SafeAreaView >
    );
};

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        flex: 1,
        backgroundColor: '#fff',
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    allAlbums: {
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-Bold',
        color: 'rgb(68, 67, 67)',
        padding: '5%'
    },

    count: {
        marginTop: hp('5%'),
        fontSize: hp('2.1%'),
        color: 'rgb(143, 143, 143)',
        fontFamily: 'SourceSansPro-Regular',
    },

    projectName: {
        fontSize: hp('2.3%'),
        color: '#3d87f1',
        fontFamily: 'SourceSansPro-Regular',
    },

    textMessage: {
        width: wp('80%'),
        height: hp('5%'),
        textAlign: 'center',
        fontSize: hp('2.3%'),
        color: 'rgb(143,143,143)'
    },
    thumbnailText: {
        width: wp('90%'),
        height: hp('4%'),
        fontSize: hp('1.8%'),
        textAlign: 'center',
        color: 'rgb(190,198,214)'
    },

    groupimage: {
        height: hp('50.5%'),
        width: wp('80.3%'),
    },
    option_text: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
    },
});

export default GetPhotos;
