import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView
} from 'react-native';
import React, { useState, useEffect, useMemo } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { useDispatch, useSelector } from 'react-redux';
import * as actionCreators from '../actions/specific-project-dashboard-action/index';
import ProjectDashboard2 from '../components/ProjectDashBoard/ProjectDashboard2';
import EmptyLoader from '../components/EmptyLoader'
import notifyMessage from '../ToastMessages';
import Orientation from 'react-native-orientation';


const ProjectDashboard = ({route, navigation}) => {
    const { project_id, token } = route.params
    const [loading, setloading] = React.useState(false);

    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(actionCreators.getSpecificProjectDetails(token, project_id))
    }, [project_id]);


   


    const projectReducer = useSelector(state => {
        return state.specificProjectDetailsReducer.specificProjectDetails.get;
    })

    const renderProjectStageEvents = () => {
        if (projectReducer.success.ok) {
            return <ProjectDashboard2 project_id={project_id} navigation={navigation} token={token} data={projectReducer.success.data} />;
        } else if (projectReducer.failure.error) {
            return notifyMessage(projectReducer.failure.message);
        } else {
            return (< EmptyLoader />)
        }
    };

    return renderProjectStageEvents();
};

export default ProjectDashboard;
