import React from 'react';
import PropTypes from 'prop-types';
import { View, Platform } from 'react-native';

export default class ElevatedView extends React.Component {
  static propTypes = {
    elevation: PropTypes.number,
  };
  static defaultProps = {
    elevation: 8
  };

  render() {
    const { elevation, style, ...otherProps } = this.props;

    if (Platform.OS === 'android') {
      return (
        <View elevation={elevation} style={[style ]} {...otherProps}>
          {this.props.children}
        </View>
      );
    }

    if (elevation === 0) {
      return (
        <View style={style} {...otherProps}>
          {this.props.children}
        </View>
      )
    }

    //calculate iosShadows here
    const iosShadowElevation = {
      shadowColor: "black",
shadowOffset: {
	width: 0.4 * elevation,
	height: 0.4 * elevation,
},
shadowOpacity: 0.03 * elevation,
shadowRadius: 1.25 * elevation,
    };

    return (
      <View style={[iosShadowElevation, style]} {...otherProps}>
        {this.props.children}
      </View>
    );
  }
}