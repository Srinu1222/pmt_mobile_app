import {
  View,
  StyleSheet,
  StatusBar,
  Dimensions,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  Button,
  Animated,
  RefreshControl
} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image';
import { useDispatch, useSelector } from 'react-redux';
import { AuthContext } from '../../context';
import { SearchBar } from 'react-native-elements';
import { FlatList } from 'react-native-gesture-handler';
import EmptyScreenHeader from '../EmptyScreens/EmptyScreenHeader';
import Pagination from '../Pagination';
import Item from './DashboardItem';
import EmptyDashBoard from '../EmptyScreens/EmptyDashBoard';
import DailyReport from '../DailyReports/DailyReport';
import { ButtonGroup, FAB, Icon } from 'react-native-elements';
import {
  useCollapsibleSubHeader,
  CollapsibleSubHeaderAnimator,
} from 'react-navigation-collapsible';
import a from '@ant-design/react-native/lib/modal/alert';
import EmptyLoader from '../EmptyLoader';
import ElevatedView from '../ElevatedView';

const GetDashboard = props => {
  const data = props.dashboard;
  const {
    onScroll /* Event handler */,
    containerPaddingTop /* number */,
    scrollIndicatorInsetTop /* number */,
    translateY,
  } = useCollapsibleSubHeader();

  const dailyReport = params => { props.navigation.navigate('dailyReports')};

  const [onActiveButtonPress, setonActiveButtonPress] = React.useState(true);
  const [onClosedButtonPress, setonClosedButtonPress] = React.useState(false);

  const [loading, setLoading] = React.useState(false)

  const [selectedIndex, setSelectedIndex] = React.useState(0);

  const onPressActive = () => {
    setonActiveButtonPress(true);
    setonClosedButtonPress(false);
  };


  const Header = () => (
    <ElevatedView elevation={8} style={{paddingTop: 15, paddingLeft: wp('5%'), paddingRight: wp('5%'), width: '100%', height: 'auto', backgroundColor: '#00' }}>
      <ButtonGroup
        onPress={updateIndex}
        selectedIndex={selectedIndex}
        buttons={buttons}
        containerStyle={{ height: hp('6%'), borderWidth: 0, elevation: 2 }}
        textStyle={{
          fontFamily: 'SourceSansPro-Regular',
          fontSize: hp('2%'),
        }}
        selectedButtonStyle={{ backgroundColor: '#3d87f1' }}
      />
    </ElevatedView>
  )

  const onPressClosed = () => {
    setonActiveButtonPress(false);
    setonClosedButtonPress(true);
  };

  const buttons = ['Active', 'Closed'];

  const updateIndex = index => {
    if (index === 0) {
      onPressActive();
    } else {
      onPressClosed();
    }
    setSelectedIndex(index);
  };

  const [refreshing, setRefreshing] = React.useState(false);
  const onRefresh = React.useCallback(async () => {
    setRefreshing(true);
    setTimeout(()=>setRefreshing(false), 2000)
  }, [refreshing]);

  return (
    <View style={{height: '91%', zIndex:-1}}>
      <Animated.FlatList
        backgroundColor='#ffffff'
        data={
          onActiveButtonPress
            ? data.active_projects
            : data.closed_projects
        }
        ListEmptyComponent={<EmptyDashBoard {...props} type={onActiveButtonPress ? 'active' : 'closed'}/>}
        keyExtractor={(item, index) => index}
        renderItem={({ item, index }) => <Item {...item} navigation={props.navigation} token={props.token} />}
        onScroll={onScroll}
        contentContainerStyle={{ paddingTop: containerPaddingTop, paddingLeft: hp('2%'), paddingRight: hp('2%'), paddingBottom: hp('1%') }}
        scrollIndicatorInsets={{ top: scrollIndicatorInsetTop }}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      />
      <CollapsibleSubHeaderAnimator translateY={translateY}>
        <Header />
      </CollapsibleSubHeaderAnimator>
      <FAB size="large" placement='right'
        buttonStyle={{ backgroundColor: '#000000' }}
        style={{paddingBottom: '5%'}}
        icon={
          <Icon
            name="calendar"
            color="white"
            type='feather'
          />}
        onPress={() => {
            props.navigation.navigate('DailyReports')
        }} />
      {loading && <EmptyLoader />}
    </View>
  )
};

const styles = StyleSheet.create({

});

export default GetDashboard;
