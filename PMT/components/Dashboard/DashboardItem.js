import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    Button
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import Icons from "react-native-vector-icons/Ionicons";
import { white } from 'react-native-paper/lib/typescript/styles/colors';
import { Card, ProgressBar } from 'react-native-paper';
import config from '../../config'
import * as actionCreators from '../../actions/project-action/index';
import { useDispatch, useSelector } from "react-redux";
import TouchableScale from 'react-native-touchable-scale';
import { CommonActions } from '@react-navigation/native';
import { NavigationActions } from 'react-navigation';


function Item(props) {
    const dispatch = useDispatch();


    const { id, is_sub_contractor, project_completion, image, completed_days_needed, total_days_needed, name, stage, current_activity, last_activity } = props;


    const onPressProjectCard = (params) => {
        // To set Project Name in secure storage
        config.projectId = id
        config.isSubContractor = is_sub_contractor

        dispatch(actionCreators.updateSelectedProject(props));

        // if (props.project_completion == true) {
            props.navigation.navigate("project_dashboard", { 
                screen: 'projectStackScreen',
                params: 
                {
                  project_id: id,
                  is_sub_contractor: is_sub_contractor,
                  token: props.token,
                  // project_completion: drawerReducer.project_completion
                }});
            // const routeName = 'project_dashboard'
            // const params = {
            //     project_id: id,
            //     is_sub_contractor: is_sub_contractor
            // }
            // props.navigation.navigate({ name: routeName, params: params });
        // } else {
        //     props.navigation.navigate('project_planning1', {
        //         project_id: id,
        //         is_sub_contractor: is_sub_contractor,
        //         project_completion: project_completion
        //     })
        // }
    }

    return (
        <TouchableScale activeScale={0.99} friction={10} tension={0}
            onPress={() => { onPressProjectCard() }}
        >
            <Card style={styles.item_card}>
                <View>
                    <View style={{ alignItems: 'center', display: 'flex', flexDirection: 'row', }}>
                        <FastImage
                            style={{ width: wp('14%'), height: hp('6%'), borderRadius: 12 }}
                            source={{ uri: image }}
                            resizeMode={FastImage.resizeMode.contain}
                        />

                        <View style={{ height: hp('6%'), display: 'flex', flexDirection: 'column', justifyContent: 'space-evenly', width: wp('60%'), marginLeft: hp('2.5%') }}>
                            <Text style={[styles.email_text, { fontSize: hp('2.3%') }]}>{completed_days_needed}/{total_days_needed} {' Days'}</Text>
                            <ProgressBar style={styles.DaysText} progress={completed_days_needed / total_days_needed} color={'#3d87f1'}></ProgressBar>
                        </View>
                    </View>

                    <Text style={[styles.department_text, { marginTop: 10, fontSize: hp('2.3%'), color: 'rgb(68, 67, 67)' }]}>{name}</Text>

                    <Text style={{ height: hp('0.2%'), marginVertical: '4%', backgroundColor: 'rgb(232, 232, 232)' }}></Text>

                    <View style={{ marginTop: 10, display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View style={{}}>
                            <Text style={[styles.email_text, { fontFamily: 'SourceSansPro-Regular' }]}>{stage}</Text>
                            <Text style={[styles.department_text,]}>Current stage</Text>
                        </View>
                        <View style={{ alignSelf: 'center' }}>
                            <Text style={[styles.email_text, { fontFamily: 'SourceSansPro-Regular' }]}>{last_activity}</Text>
                            <Text style={[styles.department_text]}>Last Activity</Text>
                        </View>
                    </View>

                    <View style={{ marginTop: hp('2%') }}>
                        <Text style={[styles.email_text, { fontFamily: 'SourceSansPro-Regular' }]}>{current_activity}</Text>
                        <Text style={[styles.department_text]}>Current Activity</Text>
                    </View>
                </View>
            </Card>
        </TouchableScale>

    )
}


const styles = StyleSheet.create({

    projectimage: {
        width: wp('10%'),
        height: hp('10%')
    },
    name_text: {
        fontSize: hp('2.8%'),
        fontFamily: 'SourceSansPro-SemiBold',
        color: 'rgb(68, 67, 67)',
    },

    email_text: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Bold',
        color: 'rgb(68, 67, 67)',
    },

    DaysText: {
        fontFamily: 'SourceSansPro-Regular',
        borderRadius: 32
    },

    department_text: {
        fontSize: hp('1.8%'),
        fontFamily: 'SourceSansPro-SemiBold',
        color: 'rgb(143, 143, 143)',
    },

    item_card: {
        backgroundColor: '#fff',
        shadowColor: 'rgba(0,0,0,0.4)',
        elevation: 8,
        borderRadius: 4,
        marginTop: 16.5,
        padding: '5%',
        paddingRight: '4%',
    }

});

export default Item;
