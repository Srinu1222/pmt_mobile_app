import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TouchableOpacity,
    TextInput,
    KeyboardAvoidingView,
    ScrollView,
    Animated,
    Pressable
} from 'react-native';
import FastImage from 'react-native-fast-image';
import LottieView from 'lottie-react-native';
import { useDispatch, useSelector } from "react-redux";
import React, { useState, useRef, useMemo, useEffect } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import notifyMessage from '../../ToastMessages';
import { FlatList } from 'react-native-gesture-handler';
import Item from '../DailyReports/DailyReportItem';
import EmptyDailyReport from '../EmptyScreens/DailyReport'
import * as actionCreators from '../../actions/daily-updates-action/index';
import * as ganttactionCreators from '../../actions/gantt-stage-action/index';
import Card from '../../components/card'
import EmptyLoader from '../EmptyLoader'
import { FAB, Icon, BottomSheet, ButtonGroup, Input } from 'react-native-elements';
import { Card as PaperCard, RadioButton, Button, Portal, Dialog } from 'react-native-paper';
import { Keyboard } from 'react-native';
import DialogComponent from './DialogComponent'
import Header from './Header'
import LinkedToHelper from './LinkedToHelper';
import Toast from "react-native-toast-message";


const LinkStageEvent = ({ route, navigation }) => {

    const { title, project_id, token, dailyUpdate } = route.params
    const [isFilesUploadPage, setisFilesUploadPage] = useState(true);
    const [inputText, setinputText] = useState('');
    const [messagesList, setmessagesList] = useState([]);
    const [key, setKey] = useState(0);

    const flatlistRef = useRef();
    const positionref = useRef();

    useEffect(() => {
        setTimeout(() => (flatlistRef && flatlistRef.current) && flatlistRef.current.scrollToEnd({ animating: true }), 100)
    }, [messagesList]);

    const onPressSend = (params) => {
        setmessagesList([...messagesList, inputText])
        setinputText('')
        Keyboard.dismiss()
    }

    const MessagesListView = (props) => {
        const data = props.data
        return (
            <View style={{ alignItems: 'flex-end', marginHorizontal: wp('5%') }}>
                {
                    data.map(
                        (i, index) => {
                            return (
                                <View key={index} style={{ backgroundColor: '#3d87f1', borderRadius: 5, padding: '2%', marginVertical: hp('1%') }}>
                                    <Text style={{ color: 'white' }}>{i}</Text>
                                </View>
                            );
                        }
                    )
                }
            </View>
        );
    }

    const [uploadDocumentsList, setuploadDocumentsList] = useState([]);
    const [uploadPicturesList, setuploadPicturesList] = useState([]);

    const [DocumentDataList, setDocumentDataList] = useState([]);
    const [PicturesDataList, setPicturesDataList] = useState([]);

    //(DocumentDataList, PicturesDataList)
    
    const dispatch = useDispatch();

    const handleNextButton = () => {
        if (isFilesUploadPage) {
            setisFilesUploadPage(false)
            setKey(key + 2)
        } else {
            dispatch(actionCreators.postDailyUpdate(
                token, dailyUpdate, DocumentDataList, uploadDocumentsList, PicturesDataList, uploadPicturesList
                ));
                navigation.navigate('reportSubmit')
        }
    }

    const Buttons = (params) => {
        return (
            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginLeft: wp('5%') }}>
                <TouchableOpacity
                    onPress={() => setisFilesUploadPage(true)}
                    style={[styles.btnStartNewProject, { backgroundColor: 'white', borderWidth: 1, borderColor: '#3d87f1' }]}
                >
                    <Text style={[styles.btnText, { color: '#3d87f1' }]}>Prev</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => handleNextButton()}
                    style={[styles.btnStartNewProject]}
                >
                    <Text style={[styles.btnText]}>{isFilesUploadPage ? 'Next' : 'Submit'}</Text>
                </TouchableOpacity>

                {
                    isFilesUploadPage &&
                    <TouchableOpacity
                        onPress={() =>
                            isFilesUploadPage ? setisFilesUploadPage(false) : navigation.navigate('reportSubmit')
                        }
                        style={[styles.btnStartNewProject, { backgroundColor: 'white' }]}
                    >
                        <Text style={[styles.btnText, { color: '#3d87f1' }]}>Skip</Text>
                    </TouchableOpacity>
                }

            </View>
        );
    }

    const flatListData = [

        {
            'type': 'MemberTasksCard',
            'data': []
        },
        {
            'type': 'MessagesList',
            'data': messagesList
        },
    ]

    // Dialog Component
    const [visible, setVisible] = useState(false);
    const updateVisibleState = (value) => {
        setVisible(value)
    }

    return (
        <View style={[styles.container]}>
            {
                visible && <DialogComponent
                    visible={visible}
                    updateState={updateVisibleState}
                    navigation={navigation}
                />
            }

            <Header title={title} updateState={updateVisibleState} />

            <KeyboardAvoidingView keyboardVerticalOffset={10} behavior='position'>
                <View style={{ height: Dimensions.get('window').height * 0.75 }}>
                    <FlatList
                        data={flatListData}
                        keyExtractor={(item, index) => index}
                        ref={flatlistRef}
                        renderItem={
                            ({ item, index }) => {
                                if (item.type === 'MemberTasksCard') {
                                    return (<LinkedToHelper
                                    key={key}
                                        isFilesUploadPage={isFilesUploadPage}
                                        navigation={navigation}
                                        project_id={project_id}
                                        token={token}
                                        uploadDocumentsList={uploadDocumentsList}
                                        setuploadDocumentsList={setuploadDocumentsList}
                                        uploadPicturesList={uploadPicturesList}
                                        setuploadPicturesList={setuploadPicturesList}
                                        DocumentDataList={DocumentDataList}
                                        setDocumentDataList={setDocumentDataList}
                                        PicturesDataList={PicturesDataList}
                                        setPicturesDataList={setPicturesDataList}
                                    />);
                                } else if (item.type === 'MessagesList') {
                                    return (<MessagesListView data={item.data} />);
                                }
                            }
                        }
                    />
                </View>

                <Buttons />

                <Input
                    placeholder='Type here...'
                    onChangeText={text => setinputText(text)}
                    inputStyle={{ fontSize: hp('2%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                    value={inputText}
                    inputContainerStyle={{ borderBottomWidth: 0 }}
                    containerStyle={{ borderWidth: 0.2, marginVertical: hp('2%'), height: hp('7%'), borderColor: 'rgb(143, 143, 143)', borderRadius: 2 }}
                    rightIcon={(params) =>
                        <Icon
                            color={'#3d87f1'}
                            onPress={() => onPressSend()}
                            type='ionicon'
                            name='send-outline'
                        />
                    }
                />
            </KeyboardAvoidingView>

        </View >
    )
};

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: "#ffffff",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
    containerHeader: {
        width: Dimensions.get('window').width * 0.9,
        height: Dimensions.get('window').height,
        flex: 0.2,
        flexDirection: 'row',
        borderBottomWidth: 3,
        borderBottomColor: 'rgb(232,232,232)',
        justifyContent: 'space-between',

    },

    titleText: {
        fontSize: hp('3.15%'),
        color: 'rgb(68, 67, 67)',
        fontFamily: 'SourceSansPro-SemiBold',
    },

    cancelText: {
        fontSize: hp('2.4%'),
        color: '#3d87f1',
        fontFamily: 'SourceSansPro-Regular',
    },

    images: {
        alignSelf: 'center',
    },

    containerBody: {
        flex: 2,
        display: 'flex',
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        justifyContent: 'flex-end',
        // paddingHorizontal: '5%'
    },

    btnStartNewProject: {
        marginTop: hp("2%"),
        width: wp("17%"),
        height: hp("4%"),
        backgroundColor: "#3d87f1",
        borderRadius: 4,
        marginHorizontal: wp('1%'),
        alignItems: "center",
        justifyContent: "center",
    },
    btnText: {
        color: "white",
        fontSize: hp("2.2%"),
        textAlign: "center",
        fontFamily: "SourceSansPro-Regular",
    },

    emptyText: {
        fontSize: hp('2.4%'),
        color: 'rgb(179, 179, 179)',
        fontFamily: 'SourceSansPro-Regular',
    },


});

export default LinkStageEvent;

// import {
//     View,
//     SafeAreaView,
//     StyleSheet,
//     StatusBar,
//     Dimensions,
//     Image,
//     Text,
//     TouchableOpacity,
//     TextInput,
//     KeyboardAvoidingView,
//     ScrollView,
//     Animated,
//     Pressable
// } from 'react-native';
// import FastImage from 'react-native-fast-image';
// import LottieView from 'lottie-react-native';
// import { useDispatch, useSelector } from "react-redux";
// import React, { useState, useRef, useMemo, useEffect } from 'react';
// import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
// import notifyMessage from '../../ToastMessages';
// import { FlatList } from 'react-native-gesture-handler';
// import Item from './DailyReportItem';
// import EmptyDailyReport from '../EmptyScreens/DailyReport'
// import * as actionCreators from '../../actions/daily-updates-action/index';
// import Card from '../card'
// import EmptyLoader from '../EmptyLoader'
// import { FAB, Icon, BottomSheet, ButtonGroup, Input } from 'react-native-elements';
// import { Card as PaperCard, RadioButton, Button, Portal, Dialog } from 'react-native-paper';
// import { Keyboard } from 'react-native';
// import Toast from "react-native-toast-message";
// import { specificationsList } from "./constants"
// import SpecificationHelper from './SpecificationHelper'
// import { useSafeArea } from 'react-native-safe-area-context';
// import DocumentBody from './DocumentBody'
// import Header from './Header'
// import LinkedToHelper from './LinkedToHelper';

// const LinkStageEvent = ({ route, navigation }) => {

//     var { title, project_id, token, dailyUpdate } = route.params;

//     const [uploadDocumentsList, setuploadDocumentsList] = useState([]);
//     const [uploadPicturesList, setuploadPicturesList] = useState([]);

//     const [isFilesUploadPage, setisFilesUploadPage] = useState(true);

//     // Send Message
//     const [messagesList, setmessagesList] = useState([]);
//     const [inputText, setinputText] = useState('');
//     const onPressChangeText = (text) => {
//         setinputText(text)
//     }
//     const flatlistRef = useRef();
//     useEffect(() => {
//         setTimeout(() => (flatlistRef && flatlistRef.current) && flatlistRef.current.scrollToEnd({ animating: true }), 100)
//     }, [messagesList, uploadDocumentsList, uploadPicturesList]);

//     const onPressSend = (params) => {
//         setmessagesList([...messagesList, inputText])
//         setinputText('')
//         Keyboard.dismiss()
//     }


//     // Dialog Component
//     const [visible, setVisible] = React.useState(false);
//     const updateVisibleState = (params) => {
//         setVisible(false)
//     }


//     const MessagesListView = (props) => {
//         const data = props.data
//         return (
//             <View style={{ alignItems: 'flex-end', marginHorizontal: wp('5%') }}>
//                 {
//                     data.map(
//                         (i, index) => {
//                             return (
//                                 <View key={index} style={{ backgroundColor: '#3d87f1', borderRadius: 5, padding: '2%', marginVertical: hp('1%') }}>
//                                     <Text style={{ color: 'white' }}>{i}</Text>
//                                 </View>
//                             );
//                         }
//                     )
//                 }
//             </View>
//         );
//     }

//     var flatListData = [
//         {
//             'type': 'Spacer',
//             'data': ''
//         },
//         {
//             'type': 'body',
//             'data': []
//         },
//         {
//             'type': 'MessagesList',
//             'data': messagesList
//         }
//     ]

//     return (
//         <View style={[styles.container]}>
//             {
//                 visible && <DialogComponent
//                     visible={visible}
//                     updateState={updateVisibleState}
//                     navigation={navigation}
//                 />
//             }

//             <Header title={title} updateState={updateVisibleState} />

//             <KeyboardAvoidingView keyboardVerticalOffset={10} behavior='position'>
//                 <View style={{ height: Dimensions.get('window').height * 0.8 }}>
//                     <FlatList
//                         data={flatListData}
//                         keyExtractor={(item, index) => index}
//                         ref={flatlistRef}
//                         renderItem={
//                             ({ item, index }) => {
//                                 if (item.type === 'body') {
//                                     return (<LinkedToHelper
//                                         uploadList={isFilesUploadPage ? uploadDocumentsList : uploadPicturesList}
//                                         uploadSetMethod={isFilesUploadPage ? setuploadDocumentsList : setuploadPicturesList}
//                                         setisFilesUploadPage={setisFilesUploadPage}
//                                         isFilesUploadPage={isFilesUploadPage}
//                                         navigation={navigation}
//                                         project_id={project_id}
//                                         token={token}
//                                         uploadDocumentsList={uploadDocumentsList}
//                                         uploadPicturesList={uploadPicturesList}
//                                     />);
//                                 } else if (item.type === 'MessagesList') {
//                                     return (<MessagesListView data={item.data} />);
//                                 } else if (item.type === 'Spacer') {
//                                     return (<View style={{ height: hp('45%') }}></View>);
//                                 }
//                             }
//                         }
//                     />
//                 </View>

//                 <Input
//                     placeholder='Type here...'
//                     onChangeText={text => onPressChangeText(text)}
//                     inputStyle={{ fontSize: hp('2%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
//                     value={inputText}
//                     inputContainerStyle={{ borderBottomWidth: 0 }}
//                     containerStyle={{ borderWidth: 0.2, marginVertical: hp('2%'), height: hp('7%'), borderColor: 'rgb(143, 143, 143)', borderRadius: 2 }}
//                     rightIcon={(params) =>
//                         <Icon
//                             color={'#3d87f1'}
//                             onPress={() => onPressSend()}
//                             type='ionicon'
//                             name='send-outline'
//                         />
//                     }
//                 />
//             </KeyboardAvoidingView>
//         </View >
//     );
// };

// const styles = StyleSheet.create({
//     container: {
//         width: Dimensions.get('window').width,
//         height: Dimensions.get('window').height,
//         backgroundColor: "#ffffff",
//         // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
//     },
//     containerHeader: {
//         width: Dimensions.get('window').width * 0.9,
//         height: Dimensions.get('window').height,
//         flex: 0.2,
//         flexDirection: 'row',
//         borderBottomWidth: 3,
//         borderBottomColor: 'rgb(232,232,232)',
//         justifyContent: 'space-between',

//     },

//     titleText: {
//         fontSize: hp('3.15%'),
//         color: 'rgb(68, 67, 67)',
//         fontFamily: 'SourceSansPro-SemiBold',
//     },

//     cancelText: {
//         fontSize: hp('2.4%'),
//         color: '#3d87f1',
//         fontFamily: 'SourceSansPro-Regular',
//     },

//     images: {
//         alignSelf: 'center',
//     },

//     containerBody: {
//         flex: 2,
//         display: 'flex',
//         width: Dimensions.get('window').width,
//         height: Dimensions.get('window').height,
//         justifyContent: 'flex-end',
//         // paddingHorizontal: '5%'
//     },

//     btnStartNewProject: {
//         marginTop: hp("2%"),
//         width: wp("17%"),
//         height: hp("4%"),
//         backgroundColor: "#3d87f1",
//         borderRadius: 4,
//         marginHorizontal: wp('1%'),
//         alignItems: "center",
//         justifyContent: "center",
//     },
//     btnText: {
//         color: "white",
//         fontSize: hp("2.2%"),
//         textAlign: "center",
//         fontFamily: "SourceSansPro-Regular",
//     },

// });

// export default LinkStageEvent;

