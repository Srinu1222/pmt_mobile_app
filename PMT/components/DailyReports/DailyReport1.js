import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TouchableOpacity,
    TextInput,
    KeyboardAvoidingView,
    ScrollView,
    Animated,
    Pressable
} from 'react-native';
import FastImage from 'react-native-fast-image';
import LottieView from 'lottie-react-native';
import { useDispatch, useSelector } from "react-redux";
import React, { useState, useRef, useMemo, useEffect } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import notifyMessage from '../../ToastMessages';
import { FlatList } from 'react-native-gesture-handler';
import Item from '../DailyReports/DailyReportItem';
import EmptyDailyReport from '../EmptyScreens/DailyReport'
import * as actionCreators from '../../actions/daily-updates-action/index';
import Card from '../../components/card'
import EmptyLoader from '../EmptyLoader'
import { FAB, Icon, BottomSheet, ButtonGroup, Input } from 'react-native-elements';
import { Card as PaperCard, RadioButton, Button, Portal, Dialog } from 'react-native-paper';
import { Keyboard } from 'react-native';
import DialogComponent from './DialogComponent'
import Header from './Header'


const DailyReport1 = ({ route, navigation }) => {

    const { title, project_id, token } = route.params

    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(actionCreators.getTodayTasks(token, project_id, true));
    }, []);

    const getMemberTasksReducer = useSelector((state) => {
        return state.dailyUpdatesReducer.memberTasks.get;
    });

    const [todayTaskTitles, settodayTaskTitles] = useState([]);
    const [todayTasksList, settodayTasksList] = useState([]);
    const [completedList, setcompletedList] = useState([]);
    const [onGoingList, setonGoingList] = useState([]);
    const [messagesList, setmessagesList] = useState([]);

    useEffect(() => {
        if (getMemberTasksReducer.success.ok) {
            let tasks = getMemberTasksReducer?.success?.data?.tasks;
            settodayTasksList([...tasks])
            let titles = []
            titles = tasks.map((task) => { return task.title });
            settodayTaskTitles([...titles])
        }
    }, [getMemberTasksReducer.success.ok]);

    const PassData = {
        'completedList': completedList,
        'onGoingList': onGoingList,
        'todayTasksList': todayTasksList,
        project_id: project_id,
        title: title
    }

    const onPressCompletedCheckBox = (task, position) => {
        if (completedList.includes(task)) {
            var temp = completedList.filter(item => item !== task)
            setcompletedList([...temp])
        } else {
            setcompletedList([...completedList, task])
        }
    }

    const onPressOnGoingCheckBox = (task, position) => {
        if (onGoingList.includes(task)) {
            var temp = onGoingList.filter(item => item !== task)
            setonGoingList([...temp])
        } else {
            setonGoingList([...onGoingList, task])
        }
    }

    const [inputText, setinputText] = useState('');


    const flatlistRef = useRef();
    const positionref = useRef();

    useEffect(() => {
        setTimeout(() => (positionref && positionref.current) && positionref.current.scrollToIndex({ animating: true, index: 20 }), 100)
    }, [completedList, onGoingList]);

    useEffect(() => {
        setTimeout(() => (flatlistRef && flatlistRef.current) && flatlistRef.current.scrollToEnd({ animating: true }), 100)
    }, [messagesList]);

    const onPressSend = (params) => {
        setmessagesList([...messagesList, inputText])
        setinputText('')
        Keyboard.dismiss()
    }

    const MemberTasksView = (props) => {
        const data = props.data
        return (
            <View>
                <View style={{ marginLeft: wp('5%'), width: wp('55%'), height: hp('6%'), alignItems: 'center', justifyContent: 'center', backgroundColor: '#f1f1f1', borderRadius: 5 }}>
                    <Text style={{ color: 'rgb(143, 143, 143)', fontSize: hp('2%'), fontFamily: 'SourceSansPro-Regular' }}>What did you do today?</Text>
                </View>

                <View style={{ flexDirection: 'row', display: 'flex', marginTop: hp('2%'), justifyContent: 'space-around', marginLeft: wp('5%'), width: wp('90%'), height: hp('6%'), alignItems: 'center', backgroundColor: '#f1f1f1', borderRadius: 5 }}>
                    <Text style={{ color: 'rgb(143, 143, 143)', fontSize: hp('2%'), fontFamily: 'SourceSansPro-Regular' }}>Task Name</Text>
                    <Text style={{ color: 'rgb(143, 143, 143)', fontSize: hp('2%'), fontFamily: 'SourceSansPro-Regular' }}>Completed</Text>
                    <Text style={{ color: 'rgb(143, 143, 143)', fontSize: hp('2%'), fontFamily: 'SourceSansPro-Regular' }}>On-Going</Text>
                </View>

                <View style={{ marginLeft: wp('5%'), width: wp('90%'), borderRadius: 2, borderWidth: 0.5, borderColor: 'rgba(0, 0, 0, 0.16)', height: hp('30%') }}>
                    <FlatList
                        // ref={positionref}
                        backgroundColor='#ffffff'
                        data={data}
                        ListEmptyComponent={
                            <View style={{  height: 150, alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={styles.emptyText}>No tasks to display.</Text>
                            </View>
                        }
                        showsVerticalScrollIndicator
                        keyExtractor={(item, index) => index}
                        getItemLayout={(item, index) => (
                            { length: data.length, offset: index * index, index }
                        )}
                        renderItem={({ item, index }) => {
                            return (
                                <View>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: wp('6%') }}>
                                        <Text style={{ color: 'rgb(143, 143, 143)', width: wp('25%'), fontSize: hp('2%'), fontFamily: 'SourceSansPro-Regular' }}>{item}</Text>
                                        <RadioButton
                                            value={completedList.includes(item)}
                                            status={completedList.includes(item) ? 'checked' : 'unchecked'}
                                            onPress={() => onPressCompletedCheckBox(item, index)}
                                            color={'rgb(61,135,241)'}
                                            uncheckedColor={'rgb(61,135,241)'}
                                        />
                                        <RadioButton
                                            value={onGoingList.includes(item)}
                                            status={onGoingList.includes(item) ? 'checked' : 'unchecked'}
                                            onPress={() => onPressOnGoingCheckBox(item, index)}
                                            color={'rgb(61,135,241)'}
                                            uncheckedColor={'rgb(61,135,241)'}
                                        />
                                    </View>
                                    <View style={{ height: hp('0.1%'), marginVertical: hp('0.5%'), backgroundColor: 'rgba(0,0,0,0.16)' }}></View>
                                </View>
                            );
                        }
                        }
                    />
                </View>

            </View>
        );
    }

    const MessagesListView = (props) => {
        const data = props.data
        return (
            <View style={{ alignItems: 'flex-end', marginHorizontal: wp('5%') }}>
                {
                    data.map(
                        (i, index) => {
                            return (
                                <View key={index} style={{ backgroundColor: '#3d87f1', borderRadius: 5, padding: '2%', marginVertical: hp('1%') }}>
                                    <Text style={{ color: 'white' }}>{i}</Text>
                                </View>
                            );
                        }
                    )
                }
            </View>
        );
    }

    const Buttons = (params) => {
        return (
            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginLeft: wp('5%') }}>
                <TouchableOpacity
                    onPress={() => navigation.goBack()}
                    style={[styles.btnStartNewProject, { backgroundColor: 'white', borderWidth: 1, borderColor: '#3d87f1' }]}
                >
                    <Text style={[styles.btnText, { color: '#3d87f1' }]}>Prev</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => navigation.navigate('dailyReports3', PassData)}
                    style={[styles.btnStartNewProject]}
                >
                    <Text style={[styles.btnText]}>Next</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => navigation.navigate('dailyReports3', PassData)}
                    style={[styles.btnStartNewProject, { backgroundColor: 'white' }]}
                >
                    <Text style={[styles.btnText, { color: '#3d87f1' }]}>Skip</Text>
                </TouchableOpacity>
            </View>
        );
    }

    // Dialog Component
    const [visible, setVisible] = React.useState(false);
    const updateVisibleState = (value) => {
        setVisible(value)
    }

    const renderDailyReport1 = () => {
        if (getMemberTasksReducer.success.ok) {
            let memberTasks = [];
            var todayTaskList = getMemberTasksReducer.success.data.tasks;
            memberTasks = todayTaskList.map((task) => { return task.title; });
            var flatListData = [
                {
                    'type': 'Spacer',
                    'data': ''
                },
                {
                    'type': 'MemberTasksCard',
                    'data': memberTasks
                },
                {
                    'type': 'MessagesList',
                    'data': messagesList
                },
                {
                    'type': 'Buttons',
                    'data': []
                },
            ]
            return (
                <View style={[styles.container]}>

                    {
                        visible && <DialogComponent
                            visible={visible}
                            updateState={updateVisibleState}
                            navigation={navigation}
                        />
                    }

                    <Header title={title} updateState={updateVisibleState} />

                    <KeyboardAvoidingView keyboardVerticalOffset={10} behavior='position'>
                        <View style={{ height: Dimensions.get('window').height * 0.82 }}>
                            <FlatList
                                data={flatListData}
                                keyExtractor={(item, index) => index}
                                ref={flatlistRef}
                                renderItem={
                                    ({ item, index }) => {
                                        if (item.type === 'MemberTasksCard') {
                                            return (<MemberTasksView data={item.data} />);
                                        } else if (item.type === 'MessagesList') {
                                            return (<MessagesListView data={item.data} />);
                                        } else if (item.type === 'Spacer') {
                                            return (<View style={{ height: hp('30%') }}></View>);
                                        } else if (item.type === 'Buttons') {
                                            return (<Buttons />);
                                        }
                                    }
                                }
                            />
                        </View>

                        <Input
                            placeholder='Type here...'
                            onChangeText={text => setinputText(text)}
                            inputStyle={{ fontSize: hp('2%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                            value={inputText}
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                            containerStyle={{ borderWidth: 0.2, marginVertical: hp('2%'), height: hp('7%'), borderColor: 'rgb(143, 143, 143)', borderRadius: 2 }}
                            rightIcon={(params) =>
                                <Icon
                                    color={'#3d87f1'}
                                    onPress={() => onPressSend()}
                                    type='ionicon'
                                    name='send-outline'
                                />
                            }
                        />
                    </KeyboardAvoidingView>

                </View >
            )
        } else if (getMemberTasksReducer.failure.error) {
            return notifyMessage(getMemberTasksReducer.failure.message);
        } else {
            return (<EmptyLoader />)
        }
    };

    return renderDailyReport1();
};

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: "#ffffff",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
    containerHeader: {
        width: Dimensions.get('window').width * 0.9,
        height: Dimensions.get('window').height,
        flex: 0.2,
        flexDirection: 'row',
        borderBottomWidth: 3,
        borderBottomColor: 'rgb(232,232,232)',
        justifyContent: 'space-between',

    },

    titleText: {
        fontSize: hp('3.15%'),
        color: 'rgb(68, 67, 67)',
        fontFamily: 'SourceSansPro-SemiBold',
    },

    cancelText: {
        fontSize: hp('2.4%'),
        color: '#3d87f1',
        fontFamily: 'SourceSansPro-Regular',
    },

    images: {
        alignSelf: 'center',
    },

    containerBody: {
        flex: 2,
        display: 'flex',
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        justifyContent: 'flex-end',
        // paddingHorizontal: '5%'
    },

    btnStartNewProject: {
        marginTop: hp("2%"),
        width: wp("17%"),
        height: hp("4%"),
        backgroundColor: "#3d87f1",
        borderRadius: 4,
        marginHorizontal: wp('1%'),
        alignItems: "center",
        justifyContent: "center",
    },
    btnText: {
        color: "white",
        fontSize: hp("2.2%"),
        textAlign: "center",
        fontFamily: "SourceSansPro-Regular",
    },

    emptyText: {
        fontSize: hp('2.4%'),
        color: 'rgb(179, 179, 179)',
        fontFamily: 'SourceSansPro-Regular',
    },


});

export default DailyReport1;
