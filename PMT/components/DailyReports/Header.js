import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TouchableOpacity,
    TextInput,
    KeyboardAvoidingView,
    ScrollView,
    Animated,
    Pressable
} from 'react-native';
import FastImage from 'react-native-fast-image';
import LottieView from 'lottie-react-native';
import { useDispatch, useSelector } from "react-redux";
import React, { useState, useRef, useMemo, useEffect } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { FAB, Icon, BottomSheet, ButtonGroup, Input } from 'react-native-elements';
import { Card, RadioButton, Button, Portal, Dialog } from 'react-native-paper';



const Header = (params) => {

    const { title, updateState } = params

    return (
        <Card.Title
            style={{ backgroundColor: 'white', elevation: 5, paddingRight: wp('5%') }}
            title={title}
            titleStyle={styles.titleText}
            right={() =>
                <TouchableOpacity
                    onPress={()=>updateState(true)}
                >
                    <Text style={styles.cancelText}>Cancel</Text>
                </TouchableOpacity>
            }
        />
    );
}


const styles = StyleSheet.create({
    titleText: {
        fontSize: hp('3.15%'),
        color: 'rgb(68, 67, 67)',
        fontFamily: 'SourceSansPro-SemiBold',
    },
    cancelText: {
        fontSize: hp('2.5%'),
        color: '#3d87f1',
        fontFamily: 'SourceSansPro-SemiBold',
    }
});

export default Header;