export const component_list = [
    "AC Distribution (Combiner) Panel Board",
    "Ambient Temperature Sensors",
    "Bus bar",
    "Cable trays",
    "Cabling Accessories",
    "Cleaning System",
    "Communication Cable",
    "DC Junction Box (SCB/SMB)",
    "DG Syncronization",
    "Danger Board and Signs",
    "Earthing Cable",
    "Easrthing Protection System",
    "Fire Extingusiher",
    "GI Strip",
    "Generation Meter",
    "HT Breaker",
    "HT Panel",
    "InC Contractor",
    "LT Panel",
    "LT Power Cable (From ACDB to Customer LT Panel)",
    "LT Power Cable (From inverters to ACDB)",
    "LT Power Cable (From metering cubical to MDB panel)",
    "Lighting Arrestor",
    "MC4 Connectors",
    "MDB Breaker",
    "Metering Panel",
    "Module Earthing Cable",
    "Module Mounting Structures with Accessories _ Ground Mount",
    "Module Mounting Structures with Accessories _ Metal Sheet",
    "Module Mounting Structures with Accessories _ RCC",
    "Module Temperature Sensors",
    "Net-Metering",
    "Other Sensors",
    "Pyro Meter",
    "Reverse Protection Relay",
    "SCADA System",
    "Safety Lines",
    "Safety Rails",
    "Solar DC Cables",
    "Solar Inverter",
    "Solar PV Module",
    "Structure Foundation Pedestal",
    "Transformer",
    "UPS",
    "Walkways",
    "Weather Monitoring Unit",
    "Wire mesh for protection of Skylights",
    "Zero Export Device",
];

export const drawings = [
    "Array Layout",
    "Civil Layout",
    "PV-Syst",
    "Communication System Layout",
    "Equipment Layout",
    "Walkways",
    "Earthing and LA Layout",
    "Safety Line",
    "Cable Layout",
    "Safety Rails",
    "Structure Drawings_RCC",
    "Project SLD",
    "Structure Drawings_Metal Sheet",
    "Final BOQ",
    "Structure Drawings_Ground Mount",
    "Detailed Project Report",
  ];

var drawings_list = []
drawings.map((i)=>
    drawings_list.push(
        {
            'label': i,
            'value': i
        }
    )
)
export var drawings_list;

export const primaryHeadingDict = {
    'List of All Drawings from Checklist': 'Engineering Plan',
    'Type of Cable': 'Cable Calculations',
    'Specifications of Revisions that are needed – Required: A way to specify the drawings that need revisions and specifying the revisions needed': 'Internal Design Review',
    'Recommended Changes if Any': 'Internal Design Review',
    'Procurement Plan': 'Procurement Plan',
    'Procurement Specifications': 'Procurement Specifications',
    'Final Specifications': 'Final Specification',
    'Inspection Agency Details': 'PDI Details',
}

export const addFieldInitialObjects = {
    'List of All Drawings from Checklist': {'drawing': 'select drawing', 'spoc': {name: 'select spoc', id: null}, 'start_date': 'select start date'},
    'Type of Cable': {'cable_type': '', 'cable_specification': '', 'cable_length': ''},
    'Specifications of Revisions that are needed – Required: A way to specify the drawings that need revisions and specifying the revisions needed': 'Internal Design Review',
    'Recommended Changes if Any': 'Internal Design Review',
    'Procurement Plan': 'Procurement Plan',
    'Procurement Specifications': 'Procurement Specifications',
    'Final Specifications': 'Final Specification',
    'Inspection Agency Details': 'PDI Details',
}

export const viewSpecifications = {
    'List of All Drawings from Checklist': 'List of Drawings',
    'Type of Cable': 'Cable Calculations',
    'Specifications of Revisions that are needed – Required: A way to specify the drawings that need revisions and specifying the revisions needed': 'Internal Approval',
    'Recommended Changes if Any': 'Client Approval',
    'Procurement Plan': 'Procurement Planning',
    'Procurement Specifications': 'Procurement Planning',
    'Final Specifications': 'Procurement Planning',
    'Inspection Agency Details': 'Pre Dispatch Inspection',
}

export const specificationsList = [
    'List of All Drawings from Checklist',
    'Type of Cable',
    'Specifications of Revisions that are needed – Required: A way to specify the drawings that need revisions and specifying the revisions needed',
    'Recommended Changes if Any',
    'Procurement Plan',
    'Procurement Specifications',
    'Final Specifications',
    'Inspection Agency Details'
]

export const specificationsTitles = {
    'List of All Drawings from Checklist': 'Edit specifications of engineering plan.',
    'Type of Cable': 'Edit specifications of Cable Calculation.',
    'Specifications of Revisions that are needed – Required: A way to specify the drawings that need revisions and specifying the revisions needed': 'Edit specifications of Internal Approval',
    'Recommended Changes if Any': 'Edit specifications of Client Approval',
    'Procurement Plan': 'Edit Procurement Plan specification Details',
    'Procurement Specifications': 'Edit Procurement specification Details',
    'Final Specifications': 'Edit Procurement Final specification Details',
    'Inspection Agency Details: To Enter Name, Contact person, Number, Email': 'Edit Inspection Agency Details'
}
