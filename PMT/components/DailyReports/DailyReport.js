import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TouchableOpacity,
    TextInput,
    KeyboardAvoidingView,
    ScrollView,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import LottieView from 'lottie-react-native';
import { useDispatch, useSelector } from "react-redux";
import React, { useState, useMemo, useEffect } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import notifyMessage from '../../ToastMessages';
import * as actionCreator from '../../actions/daily-updates-action/index'
import GetDailyReport from '../DailyReports/GetDailyReport'
import EmptyLoader from '../EmptyLoader'


const DailyReport = (props) => {

    const dispatch = useDispatch();

    const token = props.route.params.token

    useEffect(() => {
        dispatch(actionCreator.getDailyUpdateProjects(token));
    }, []);

    const dailyUpdateReducer = useSelector((state) => {
        return state.dailyUpdatesReducer.dailyUpdates.get;
    });

    const renderDailyReport = () => {
        if (dailyUpdateReducer.success.ok) {
            return <GetDailyReport {...props} dailyReport={dailyUpdateReducer.success.data.projects} />;
        } else if (dailyUpdateReducer.failure.error) {
            return notifyMessage(dailyUpdateReducer.failure.message);
        } else {
            return (<EmptyLoader />)
        }
    };
    return renderDailyReport();
}

export default DailyReport;
