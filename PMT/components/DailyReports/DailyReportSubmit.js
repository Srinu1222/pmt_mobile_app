import React from 'react';
import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TouchableOpacity
} from 'react-native';
import LottieView from 'lottie-react-native';
import FastImage from 'react-native-fast-image';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';


const DailyReportSubmit = ({ route, navigation }) => {

    const Total_height = Dimensions.get('window').height;

    const onPressFinish = (params) => {
        navigation.reset({
            index: 0,
            routes: [{ name: 'DailyReports' }],
        });
        // navigation.navigate('DailyReports')
    }

    return (
        <SafeAreaView>

            <View style={[styles.container]}>
                {/* < FastImage
                    style={{ width: wp('12%'), height: hp('6%') }}
                    source={require('../../assets/icons/check.png')
                    }
                    resizeMode={FastImage.resizeMode.stretch}
                /> */}
                <LottieView
                    style={{ width: wp('40%'), height: hp('15%') }}
                    source={require('../../done.json')}
                    autoPlay
                    loop={false}
                // onAnimationFinish={this.props.onAnimationFinish}
                />

                <Text style={[styles.text]}>Report Submitted!</Text>

                <View style={{ position: 'absolute', top: Total_height * 0.85 }}>
                    <TouchableOpacity
                        onPress={() => { onPressFinish() }}
                        style={styles.buttonContainer}>
                        <Text style={{
                            color: '#3A80E3',
                            fontSize: hp('2.5%'),
                            textAlign: 'center',
                            fontFamily: 'SourceSansPro-Regular',
                        }}>Finish</Text>
                    </TouchableOpacity>
                </View>

            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgb(57, 127, 226)',
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    text: {
        color: 'white',
        fontSize: hp('3'),
        fontFamily: 'SourceSansPro-Semibold'
    },

    buttonContainer: {
        width: wp('80%'),
        height: hp('7.5%'),
        backgroundColor: 'white',
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
    }
});

export default DailyReportSubmit;
