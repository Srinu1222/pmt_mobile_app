import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    Button
} from 'react-native';
import React, { useState, useMemo, useEffect } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { useDispatch, useSelector } from 'react-redux';
import { SearchBar } from 'react-native-elements';
import { FlatList, TouchableWithoutFeedback } from 'react-native-gesture-handler';
import EmptyScreenHeader from '../EmptyScreens/EmptyScreenHeader';
import Pagination from '../Pagination'


function Item(props) {

    const { id, title, is_pending } = props

    if (!is_pending) {
        return (
            <View style={[styles.item_card, { borderLeftColor: is_pending ? '#3d87f1' : 'rgb(194, 194, 194)' }]}>

                <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                    <View>
                        <Text style={styles.name_text}>{title}</Text>
                        <Text style={styles.department_text}>Project Name</Text>
                    </View>

                    <View style={{ alignSelf: 'center' }}>
                        <Text style={[styles.pending_text, { color: is_pending ? '#6ba4f4' : 'rgb(194, 194, 194)' }]}>
                            {is_pending ? 'PENDING!' : 'UPDATED'}
                        </Text>
                    </View>
                </View>

            </View>
        );
    } else {
        return (
            <View>
                <TouchableWithoutFeedback
                    onPress={() => props.navigation.navigate('dailyReports1', {title: title, project_id: id})}
                >
                    <View style={[styles.item_card, { borderLeftColor: is_pending ? '#3d87f1' : 'rgb(194, 194, 194)' }]}>

                        <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View>
                                <Text style={styles.name_text}>{title}</Text>
                                <Text style={styles.department_text}>Project Name</Text>
                            </View>

                            <View style={{ alignSelf: 'center' }}>
                                <Text style={[styles.pending_text, { color: is_pending ? '#6ba4f4' : 'rgb(194, 194, 194)' }]}>
                                    {is_pending ? 'PENDING!' : 'UPDATED'}
                                </Text>
                            </View>
                        </View>

                    </View>

                </TouchableWithoutFeedback>
            </View>
        );
    }


}

const styles = StyleSheet.create({
    name_text: {
        fontSize: hp('2.6%'),
        fontFamily: 'SourceSansPro-SemiBold',
        color: 'rgb(68, 67, 67)',
    },

    department_text: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(143, 143, 143)',
    },

    pending_text: {
        fontSize: hp('2.2%'),
        fontFamily: 'SourceSansPro-Regular',
    },

    item_card: {
    borderRadius: 4,
    borderLeftWidth: 3,
    backgroundColor: "#fff",
    elevation: 2,
    paddingHorizontal: hp("3%"),
    paddingTop: hp("1%"),
    paddingBottom: hp("1%"),
    margin: hp("1%"),
    }

});

export default Item;
