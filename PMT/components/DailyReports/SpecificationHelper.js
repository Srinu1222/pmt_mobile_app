import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    useWindowDimensions,
    Animated,
    Picker
} from 'react-native';
import React, { useState, useRef, useEffect, useMemo } from 'react';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'
import { useDispatch, useSelector } from 'react-redux';
import * as actionCreators from "../../actions/gantt-stage-action/index";
import { Avatar, Card, Button, TextInput, List, Divider, IconButton } from 'react-native-paper';
import color from 'color';
import { FlatList } from 'react-native';
import {
    useCollapsibleSubHeader,
    CollapsibleSubHeaderAnimator,
} from 'react-navigation-collapsible';
import { FAB, Icon, BottomSheet, Input } from 'react-native-elements';
import { IndexPath, Layout, Select, SelectItem } from '@ui-kitten/components';
import { drawings, specificationsTitles, component_list, drawings_list } from './constants'
import DropDownPicker from 'react-native-dropdown-picker';
import ModalDropdown from 'react-native-modal-dropdown';
import MyDatePicker from './DatePicker'
import DocumentPicker from 'react-native-document-picker';
import UploadPo from './UploadPo'


const SpecificationHelper = (params) => {
    var { selectedChecklistTitle, spoc_list, event_id, token, selectedChecklistId, inventory } = params
    const dispatch = useDispatch();

    const onPressSave = (specReducerState) => {
        const formData = new FormData();
        formData.append("event_id", event_id);
        formData.append("checklist_id", selectedChecklistId);

        switch (selectedChecklistTitle) {
            case 'List of All Drawings from Checklist':
            case 'SPOC for each drawing':
                formData.append("list_of_drawings", JSON.stringify(specReducerState));
                dispatch(actionCreators.editTaskListOfDrawings(formData, token));
                break;
            case 'Type of Cable':
                formData.append("cable_calculations", JSON.stringify(specReducerState));
                dispatch(actionCreators.editTaskListOfCable(formData, token));
                break;
            case 'Specifications of Revisions that are needed – Required: A way to specify the drawings that need revisions and specifying the revisions needed':
            case 'Recommended Changes if Any':
                formData.append("internal_review_design_change", JSON.stringify(specReducerState));
                dispatch(actionCreators.editTaskListOfInternalApproval(formData, token));
                break;
            case 'Procurement Plan':
                formData.append("procurement_plan", JSON.stringify(specReducerState));
                dispatch(actionCreators.editTaskListOfProcurementPlan(formData, token));
                break;
            case 'Procurement Specifications':
                var { target_price, preferred_brand, stock_quantity, delivery_date, specifications } = specReducerState[0]
                formData.append("target_price", target_price);
                formData.append("preferred_brand", preferred_brand);
                formData.append("stock_quantity", stock_quantity);
                formData.append("delivery_date", delivery_date);
                formData.append("specifications", specifications);
                dispatch(actionCreators.editTaskListOfProcurementSpecification(formData, token));
                break;
            case 'Final Specifications':
                var { final_price, final_brand, stock_quantity, delivery_date, specifications } = specReducerState[0]
                formData.append("final_price", final_price);
                formData.append("final_brand", final_brand);
                formData.append("stock_quantity", stock_quantity);
                formData.append("delivery_date", delivery_date);
                formData.append("specifications", specifications);
                dispatch(actionCreators.editTaskListOfFinalSpecification(formData, token));
                break;
            case 'Inspection Agency Details: To Enter Name, Contact person, Number, Email':
                var { component_name, pdi_agency_name, pdi_date, contact_person_name, contact_person_email, contact_phone_number } = specReducerState[0]
                formData.append("component_name", component_name);
                formData.append("pdi_agency_name", pdi_agency_name);
                formData.append("pdi_date", pdi_date);
                formData.append("contact_person_name", contact_person_name);
                formData.append("contact_person_email", contact_person_email);
                formData.append("contact_phone_number", contact_phone_number);
                dispatch(actionCreators.editPreDispatchInspection(formData, token));
                break;
            // case selectedChecklistTitle.toString().includes('OEM'):
            default:
                var { date, percentage, comment } = specReducerState[0]
                formData.append("date", date);
                formData.append("percentage", percentage);
                formData.append("comment", comment);
                dispatch(actionCreators.editTaskListOfOEMGuideLines(formData, token));
                break;
        }
    }

    switch (selectedChecklistTitle) {
        case 'List of All Drawings from Checklist':
        case 'SPOC for each drawing':
            var [specReducerState, setspecReducerState] = useState([]);

            //(specReducerState)

            // To display Spoc Names
            var spocNamesList = []
            spoc_list.map((item, index) => spocNamesList.push(item.name))

            var [IconPosition, setIconPosition] = useState(false);

            const onSelectDropDown = (index, value, item) => {
                let found = true
                for (let i = 0; i < specReducerState.length; i++) {
                    let element = specReducerState[i]
                    if (element.drawing === item) {
                        found = false
                        let newArray = [...specReducerState]
                        newArray[i] = {
                            ...element,
                            'default_spoc': spoc_list[index].id
                        }
                        setspecReducerState([...newArray]);
                        break;
                    }
                }
                if (found) {
                    let drawingObject = { "default_spoc": spoc_list[index].id, "drawing": item, "start_time": '' }
                    setspecReducerState([...specReducerState, drawingObject])
                }
            }

            const onSelectDate = (value, index, item) => {
                let found = true
                for (let i = 0; i < specReducerState.length; i++) {
                    let element = specReducerState[i]
                    if (element.drawing === item) {
                        found = false
                        let newArray = [...specReducerState]
                        newArray[i] = {
                            ...element,
                            'start_time': value
                        }
                        setspecReducerState([...newArray]);
                        break;
                    }
                }
                if (found) {
                    let drawingObject = { "default_spoc": '', "drawing": item, "start_time": value }
                    setspecReducerState([...specReducerState, drawingObject])
                }
            }

            return (
                <View>
                    <View style={styles.titleViewStyle}>
                        <Text style={styles.titleStyle1}>{specificationsTitles[params.selectedChecklistTitle]}</Text>
                    </View>

                    <View style={styles.cardWrapperViewStyle}>
                        <View style={styles.cardHeaderViewStyle}>
                            <TouchableOpacity
                                onPress={() => onPressSave(specReducerState)}
                            >
                                <Text style={styles.saveStyle}>Save</Text>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <Text style={styles.saveStyle}>+Add</Text>
                            </TouchableOpacity>
                        </View>
                        <View>
                            {
                                drawings.map(
                                    (item, i) => {
                                        return (
                                            <View key={i} style={{ padding: '5%', paddingBottom: '0%' }}>
                                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                                    <Text style={styles.titleStyle}>Drawing Name</Text>
                                                    <View
                                                        style={{ width: wp('55%'), borderWidth: 1, borderColor: 'rgb(232, 232, 232)', backgroundColor: 'rgba(179, 179, 179, .1)', borderRadius: 4, padding: 8 }}
                                                    >
                                                        <Text style={styles.Txt}>{item}</Text>
                                                    </View>

                                                </View>
                                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                                    <Text style={styles.titleStyle}>SPOC</Text>
                                                    <ModalDropdown
                                                        options={spocNamesList}
                                                        textStyle={{ fontSize: hp('2%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                                        dropdownStyle={{ width: '55%', height: hp('30%'), marginLeft: -9, elevation: 8, marginTop: hp('.2%') }}
                                                        dropdownTextStyle={{ fontSize: hp('2%'), color: 'rgb(143, 143, 143)' }}
                                                        onSelect={(index, value) => onSelectDropDown(index, value, item, i)}
                                                        defaultValue={'spoc'}
                                                        dropdownTextHighlightStyle={{ backgroundColor: 'rgb(236, 243, 254)' }}
                                                        showsVerticalScrollIndicator={true}
                                                        style={{ width: wp('55%'), borderWidth: 1, borderColor: 'rgb(232, 232, 232)', backgroundColor: 'rgba(179, 179, 179, .1)', borderRadius: 4, padding: 8, marginVertical: hp('1%') }}
                                                        renderRightComponent={() =>
                                                            <View style={{ marginLeft: wp('0%'), borderWidth: 0 }}>
                                                                <Icon color={'#9B9B9B'} type='feather' name={IconPosition ? 'chevron-up' : 'chevron-down'} />
                                                            </View>
                                                        }
                                                        defaultTextStyle={{ color: 'rgb(143, 143, 143)' }}
                                                        onDropdownWillShow={() => setIconPosition(true)}
                                                        onDropdownWillHide={() => setIconPosition(false)}
                                                    />
                                                </View>
                                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: hp('1%'), alignItems: 'center' }}>
                                                    <Text style={styles.titleStyle}>Start Date</Text>
                                                    <MyDatePicker
                                                        onSelectField={onSelectDate}
                                                        type={item}
                                                        index={''}
                                                        styling={styles.dateInputForLOD}
                                                    />
                                                </View>
                                                {
                                                    drawings.length == i + 1 ? null :
                                                        <Divider style={{ backgroundColor: 'rgb(232,232,232)', marginTop: hp('1%'), height: hp('0.3%') }} />
                                                }
                                            </View>
                                        );
                                    }
                                )
                            }
                        </View>
                    </View>
                </View>
            );
            break;

        case 'Type of Cable':
            var [specReducerState, setspecReducerState] = useState([
                { 'cable_type': '', 'cable_specification': '', 'cable_length': '' }
            ]);
            var [IconPosition, setIconPosition] = useState(false);

            var addEmptyObject = () => {
                let emptyObject = { 'cable_type': '', 'cable_specification': '', 'cable_length': '' }
                setspecReducerState([...specReducerState, emptyObject])
            }

            var onSelectField = (value, index, type) => {
                let newArray = [...specReducerState]
                newArray[index] = {
                    ...newArray[index],
                    [type]: value
                }
                setspecReducerState([...newArray]);
            }

            return (
                <View>
                    <View style={[styles.titleViewStyle, { width: wp('83%') }]}>
                        <Text style={styles.titleStyle1}>{specificationsTitles[params.selectedChecklistTitle]}</Text>
                    </View>

                    <View style={styles.cardWrapperViewStyle}>
                        <View style={styles.cardHeaderViewStyle}>
                            <TouchableOpacity
                                onPress={() => onPressSave(specReducerState)}
                            >
                                <Text style={styles.saveStyle}>Save</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => addEmptyObject()}
                            >
                                <Text style={styles.saveStyle}>+Add</Text>
                            </TouchableOpacity>
                        </View>

                        {
                            specReducerState.map((item, index) => {
                                return (
                                    <View key={index}>
                                        <ModalDropdown
                                            options={["Ac cable", "Dc cable"]}
                                            textStyle={{ fontSize: hp('2.3%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                            dropdownStyle={{ width: '80%', height: hp('12%'), marginLeft: -12, elevation: 8, marginTop: -20 }}
                                            dropdownTextStyle={{ fontSize: hp('2%'), color: 'rgb(143, 143, 143)' }}
                                            onSelect={(i, value) => onSelectField(value, index, 'cable_type')}
                                            defaultValue={'Cable Type'}
                                            dropdownTextHighlightStyle={{ backgroundColor: 'rgb(236, 243, 254)' }}
                                            showsVerticalScrollIndicator={true}
                                            style={{ width: wp('80%'), paddingVertical: hp('2%'), paddingHorizontal: wp('3%'), borderWidth: 1, borderColor: 'rgb(232, 232, 232)', backgroundColor: 'rgba(179, 179, 179, .1)', alignSelf: 'center', borderRadius: 4, marginVertical: hp('1%') }}
                                            renderRightComponent={() =>
                                                <View style={{ marginLeft: wp('20%') }}>
                                                    <Icon color={'#9B9B9B'} type='feather' name={IconPosition ? 'chevron-up' : 'chevron-down'} />
                                                </View>
                                            }
                                            defaultTextStyle={{ color: 'rgb(143, 143, 143)' }}
                                            onDropdownWillShow={() => setIconPosition(true)}
                                            onDropdownWillHide={() => setIconPosition(false)}
                                        />
                                        <Input
                                            placeholder={'Specification'}
                                            onChangeText={text => onSelectField(text, index, 'cable_specification')}
                                            inputStyle={{ fontSize: hp('2.3%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                            value={specReducerState[index]['cable_specification']}
                                            inputContainerStyle={{ borderBottomWidth: 0 }}
                                            multiline={true}
                                            containerStyle={{ width: wp('80%'), alignSelf: 'center', borderWidth: 1, borderColor: 'rgb(232, 232, 232)', backgroundColor: 'rgba(179, 179, 179, .1)', borderRadius: 4 }}
                                        />
                                        <View style={{ flexDirection: 'row', marginVertical: hp('1%'), width: wp('80%'), alignSelf: 'center', justifyContent: 'flex-start' }}>
                                            <Input
                                                placeholder={'Cable Length'}
                                                onChangeText={text => onSelectField(text, index, cable_length)}
                                                inputStyle={{ fontSize: hp('2.3%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                                value={specReducerState[index]['cable_length']}
                                                inputContainerStyle={{ borderBottomWidth: 0 }}
                                                containerStyle={{ width: wp('60%'), borderWidth: 1, borderColor: 'rgb(232, 232, 232)', backgroundColor: 'rgba(179, 179, 179, .1)', height: hp('7%'), borderRadius: 4 }}
                                            />
                                            <Text style={[styles.textStyle, { marginLeft: wp('3%'), alignSelf: 'center' }]}>Meter</Text>
                                        </View>
                                        {
                                            specReducerState.length == index + 1 ? null :
                                                <Divider style={{ backgroundColor: 'rgb(232,232,232)', marginHorizontal: wp('5%'), marginVertical: hp('1%'), height: hp('0.3%') }} />
                                        }
                                    </View>
                                );
                            })
                        }

                    </View>
                </View>
            );
            break;

        case 'Specifications of Revisions that are needed – Required: A way to specify the drawings that need revisions and specifying the revisions needed':
        case 'Recommended Changes if Any':
            var [specReducerState, setspecReducerState] = useState([
                { 'drawing_name': '', 'change': '' }
            ]);
            var [IconPosition, setIconPosition] = useState(false);

            var addEmptyObject = () => {
                let emptyObject = { 'drawing_name': '', 'change': '' }
                setspecReducerState([...specReducerState, emptyObject])
            }

            var onSelectField = (value, index, type) => {
                let newArray = [...specReducerState]
                newArray[index] = {
                    ...newArray[index],
                    [type]: value
                }
                setspecReducerState([...newArray]);
            }

            return (
                <View>
                    <View style={[styles.titleViewStyle, { width: wp('83%') }]}>
                        <Text style={styles.titleStyle1}>{specificationsTitles[params.selectedChecklistTitle]}</Text>
                    </View>

                    <View style={styles.cardWrapperViewStyle}>
                        <View style={styles.cardHeaderViewStyle}>
                            <TouchableOpacity
                                onPress={() => onPressSave(specReducerState)}
                            >
                                <Text style={styles.saveStyle}>Save</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => addEmptyObject()}
                            >
                                <Text style={styles.saveStyle}>+Add</Text>
                            </TouchableOpacity>
                        </View>
                        {
                            specReducerState.map((item, index) => {
                                return (
                                    <View key={index}>
                                        <ModalDropdown
                                            options={drawings}
                                            textStyle={{ fontSize: hp('2.3%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                            dropdownStyle={{ width: '80%', height: hp('40%'), marginLeft: -12, elevation: 8, marginTop: -20 }}
                                            dropdownTextStyle={{ fontSize: hp('2%'), color: 'rgb(143, 143, 143)' }}
                                            onSelect={(i, value) => onSelectField(value, index, 'drawing_name')}
                                            defaultValue={'Drawing'}
                                            dropdownTextHighlightStyle={{ backgroundColor: 'rgb(236, 243, 254)' }}
                                            showsVerticalScrollIndicator={true}
                                            style={{ width: wp('80%'), paddingVertical: hp('2%'), paddingHorizontal: wp('3%'), borderWidth: 1, borderColor: 'rgb(232, 232, 232)', backgroundColor: 'rgba(179, 179, 179, .1)', alignSelf: 'center', borderRadius: 4, marginVertical: hp('1%') }}
                                            renderRightComponent={() =>
                                                <View style={{ marginLeft: wp('20%') }}>
                                                    <Icon color={'#9B9B9B'} type='feather' name={IconPosition ? 'chevron-up' : 'chevron-down'} />
                                                </View>
                                            }
                                            defaultTextStyle={{ color: 'rgb(143, 143, 143)' }}
                                            onDropdownWillShow={() => setIconPosition(true)}
                                            onDropdownWillHide={() => setIconPosition(false)}
                                        />
                                        <Input
                                            placeholder={'Change'}
                                            onChangeText={text => onSelectField(text, index, 'change')}
                                            inputStyle={{ fontSize: hp('2.3%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                            value={specReducerState[index]['change']}
                                            inputContainerStyle={{ borderBottomWidth: 0 }}
                                            multiline={true}
                                            containerStyle={{ width: wp('80%'), marginBottom: hp('1%'), alignSelf: 'center', borderWidth: 1, borderColor: 'rgb(232, 232, 232)', backgroundColor: 'rgba(179, 179, 179, .1)', borderRadius: 4 }}
                                        />
                                        {
                                            specReducerState.length == index + 1 ? null :
                                                <Divider style={{ backgroundColor: 'rgb(232,232,232)', marginHorizontal: wp('5%'), marginVertical: hp('1%'), height: hp('0.3%') }} />
                                        }
                                    </View>
                                );
                            })
                        }

                    </View>
                </View>
            );
            break;

        case 'Procurement Plan':

            // To display Spoc Names
            var spocNamesList = []
            spoc_list.map((item, index) => spocNamesList.push(item.name))

            var [specReducerState, setspecReducerState] = useState([
                { 'component_name': '', 'spoc': '', 'start_date': '' }
            ]);

            var [IconPosition, setIconPosition] = useState(false);

            var addEmptyObject = () => {
                let emptyObject = { 'component_name': '', 'spoc': '', 'start_date': '' }
                setspecReducerState([...specReducerState, emptyObject])
            }

            var onSelectField = (value, index, type) => {
                let newArray = [...specReducerState]
                newArray[index] = {
                    ...newArray[index],
                    [type]: value
                }
                setspecReducerState([...newArray]);
            }

            return (
                <View>
                    <View style={[styles.titleViewStyle, { width: wp('83%') }]}>
                        <Text style={styles.titleStyle1}>{specificationsTitles[params.selectedChecklistTitle]}</Text>
                    </View>

                    <View style={styles.cardWrapperViewStyle}>
                        <View style={styles.cardHeaderViewStyle}>
                            <TouchableOpacity
                                onPress={() => onPressSave(specReducerState)}
                            >
                                <Text style={styles.saveStyle}>Save</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => addEmptyObject()}
                            >
                                <Text style={styles.saveStyle}>+Add</Text>
                            </TouchableOpacity>
                        </View>

                        {
                            specReducerState.map((item, index) => {
                                return (
                                    <View key={index}>
                                        <Input
                                            placeholder={'Component Name'}
                                            onChangeText={text => onSelectField(text, index, 'component_name')}
                                            inputStyle={{ fontSize: hp('2.3%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                            value={componentName}
                                            inputContainerStyle={{ borderBottomWidth: 0 }}
                                            containerStyle={{ width: wp('80%'), alignSelf: 'center', marginVertical: hp('1%'), borderWidth: 1, borderColor: 'rgb(232, 232, 232)', backgroundColor: 'rgba(179, 179, 179, .1)', height: hp('7%'), borderRadius: 4 }}
                                        />
                                        <ModalDropdown
                                            options={spocNamesList}
                                            textStyle={{ fontSize: hp('2%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                            dropdownStyle={{ width: '55%', height: hp('30%'), marginLeft: -9, elevation: 8, marginTop: hp('.2%') }}
                                            dropdownTextStyle={{ fontSize: hp('2%'), color: 'rgb(143, 143, 143)' }}
                                            onSelect={(i, value) => onSelectField(value, index, 'spoc')}
                                            defaultValue={'Spoc'}
                                            dropdownTextHighlightStyle={{ backgroundColor: 'rgb(236, 243, 254)' }}
                                            showsVerticalScrollIndicator={true}
                                            style={{ width: wp('80%'), paddingVertical: hp('2%'), paddingHorizontal: wp('3%'), borderWidth: 1, borderColor: 'rgb(232, 232, 232)', backgroundColor: 'rgba(179, 179, 179, .1)', alignSelf: 'center', borderRadius: 4, marginVertical: hp('1%') }}
                                            renderRightComponent={() =>
                                                <View style={{ marginLeft: wp('20%') }}>
                                                    <Icon color={'#9B9B9B'} type='feather' name={IconPosition ? 'chevron-up' : 'chevron-down'} />
                                                </View>
                                            }
                                            defaultTextStyle={{ color: 'rgb(143, 143, 143)' }}
                                            onDropdownWillShow={() => setIconPosition(true)}
                                            onDropdownWillHide={() => setIconPosition(false)}
                                        />

                                        <MyDatePicker
                                            onSelectField={onSelectField}
                                            type={'start_date'}
                                            index={index}
                                            styling={styles.dateInput}
                                        />


                                        {
                                            specReducerState.length == index + 1 ? null :
                                                <Divider style={{ backgroundColor: 'rgb(232,232,232)', marginHorizontal: wp('5%'), marginVertical: hp('1%'), height: hp('0.3%') }} />
                                        }
                                    </View>
                                );
                            })
                        }

                    </View>
                </View>
            );
            break;

        case 'Procurement Specifications':
            var [targetPrice, settargetPrice] = useState('');
            var [Quantity, setQuantity] = useState('');
            var [preferredBrand, setpreferredBrand] = useState('');
            var [deliveryDate, setdeliveryDate] = useState('');
            var [specification, setspecification] = useState('')

            var [IconPosition, setIconPosition] = useState(false);
            var [uploadedFilesList, setuploadedFilesList] = useState('')

            var onPressDate = (value, index, item) => {
                setdeliveryDate(value)
            }

            return (
                <View>
                    <View style={[styles.titleViewStyle, { width: wp('90%') }]}>
                        <Text style={styles.titleStyle1}>{specificationsTitles[params.selectedChecklistTitle]}</Text>
                    </View>

                    <View style={styles.cardWrapperViewStyle}>
                        <View style={styles.cardHeaderViewStyle}>
                            <TouchableOpacity
                                onPress={() => onPressSave(
                                    [
                                        {
                                            'target_price': targetPrice,
                                            'preferred_brand': preferredBrand,
                                            'stock_quantity': Quantity,
                                            'delivery_date': deliveryDate,
                                            'specifications': specifications
                                        }
                                    ]
                                )}
                            >
                                <Text style={styles.saveStyle}>Save</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={{ padding: '5%', alignSelf: 'center' }}>
                            <Input
                                placeholder={'Component Name'}
                                // onChangeText={text => setcomponentName(text)}
                                inputStyle={{ fontSize: hp('2.3%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                value={inventory}
                                editable={false}
                                inputContainerStyle={{ borderBottomWidth: 0 }}
                                containerStyle={{ width: wp('80%'), marginVertical: hp('1%'), borderWidth: 1, borderColor: 'rgb(232, 232, 232)', backgroundColor: 'rgba(179, 179, 179, .1)', height: hp('7%'), borderRadius: 4 }}
                            />

                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Input
                                    placeholder={'Target Price'}
                                    onChangeText={text => settargetPrice(text)}
                                    inputStyle={{ fontSize: hp('2.3%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                    value={targetPrice}
                                    inputContainerStyle={{ borderBottomWidth: 0 }}
                                    containerStyle={{ width: wp('38%'), marginVertical: hp('1%'), borderWidth: 1, borderColor: 'rgb(232, 232, 232)', backgroundColor: 'rgba(179, 179, 179, .1)', height: hp('7%'), borderRadius: 4 }}
                                />
                                <Input
                                    placeholder={'Quantity(No.of Pieces)'}
                                    onChangeText={text => setQuantity(text)}
                                    inputStyle={{ fontSize: Quantity ? hp('2.5%') : hp('1.6%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                    value={Quantity}
                                    inputContainerStyle={{ borderBottomWidth: 0 }}
                                    containerStyle={{ width: wp('38%'), marginVertical: hp('1%'), borderWidth: 1, borderColor: 'rgb(232, 232, 232)', backgroundColor: 'rgba(179, 179, 179, .1)', height: hp('7%'), borderRadius: 4 }}
                                />
                            </View>
                            <MyDatePicker
                                onSelectField={onPressDate}
                                type={''}
                                index={''}
                                styling={styles.dateInput}
                            />
                            <Input
                                placeholder={'Preferred Brand'}
                                onChangeText={text => setpreferredBrand(text)}
                                inputStyle={{ fontSize: hp('2.3%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                value={preferredBrand}
                                inputContainerStyle={{ borderBottomWidth: 0 }}
                                containerStyle={{ width: wp('80%'), marginVertical: hp('1%'), borderWidth: 1, borderColor: 'rgb(232, 232, 232)', backgroundColor: 'rgba(179, 179, 179, .1)', height: hp('7%'), borderRadius: 4 }}
                            />
                            <Input
                                placeholder={'Specification'}
                                onChangeText={text => setspecification(text)}
                                inputStyle={{ fontSize: hp('2.3%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                value={specification}
                                inputContainerStyle={{ borderBottomWidth: 0 }}
                                multiline={true}
                                containerStyle={{ width: wp('80%'), alignSelf: 'center', borderWidth: 1, borderColor: 'rgb(232, 232, 232)', backgroundColor: 'rgba(179, 179, 179, .1)', borderRadius: 4 }}
                            />
                            <UploadPo
                                uploadList={uploadedFilesList}
                                uploadSetMethod={setuploadedFilesList}
                            />
                        </View>
                    </View>
                </View>
            );
            break;

        case 'Final Specifications':

            var [targetPrice, settargetPrice] = useState('');
            var [Quantity, setQuantity] = useState('');
            var [preferredBrand, setpreferredBrand] = useState('');
            var [deliveryDate, setdeliveryDate] = useState('');
            var [specification, setspecification] = useState('')

            var [IconPosition, setIconPosition] = useState(false);

            var onPressDate = (value, index, item) => {
                setdeliveryDate(value)
            }

            return (
                <View>
                    <View style={[styles.titleViewStyle, { width: wp('90%') }]}>
                        <Text style={styles.titleStyle1}>{specificationsTitles[params.selectedChecklistTitle]}</Text>
                    </View>

                    <View style={styles.cardWrapperViewStyle}>
                        <View style={styles.cardHeaderViewStyle}>
                            <TouchableOpacity
                                onPress={() => onPressSave(
                                    [
                                        {
                                            'final_price': targetPrice,
                                            'final_brand': preferredBrand,
                                            'stock_quantity': Quantity,
                                            'delivery_date': '',
                                            'specifications': specifications
                                        }
                                    ]
                                )}
                            >
                                <Text style={styles.saveStyle}>Save</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={{ padding: '5%', alignSelf: 'center' }}>
                            <Input
                                placeholder={'Component Name'}
                                // onChangeText={text => setcomponentName(text)}
                                inputStyle={{ fontSize: hp('2.3%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                value={inventory}
                                editable={false}
                                inputContainerStyle={{ borderBottomWidth: 0 }}
                                containerStyle={{ width: wp('80%'), marginVertical: hp('1%'), borderWidth: 1, borderColor: 'rgb(232, 232, 232)', backgroundColor: 'rgba(179, 179, 179, .1)', height: hp('7%'), borderRadius: 4 }}
                            />

                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Input
                                    placeholder={'Target Price'}
                                    onChangeText={text => settargetPrice(text)}
                                    inputStyle={{ fontSize: hp('2.3%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                    value={targetPrice}
                                    inputContainerStyle={{ borderBottomWidth: 0 }}
                                    containerStyle={{ width: wp('38%'), marginVertical: hp('1%'), borderWidth: 1, borderColor: 'rgb(232, 232, 232)', backgroundColor: 'rgba(179, 179, 179, .1)', height: hp('7%'), borderRadius: 4 }}
                                />
                                <Input
                                    placeholder={'Quantity(No.of Pieces)'}
                                    onChangeText={text => setQuantity(text)}
                                    inputStyle={{ fontSize: Quantity ? hp('2.5%') : hp('1.6%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                    value={Quantity}
                                    inputContainerStyle={{ borderBottomWidth: 0 }}
                                    containerStyle={{ width: wp('38%'), marginVertical: hp('1%'), borderWidth: 1, borderColor: 'rgb(232, 232, 232)', backgroundColor: 'rgba(179, 179, 179, .1)', height: hp('7%'), borderRadius: 4 }}
                                />
                            </View>
                            <MyDatePicker
                                onSelectField={onPressDate}
                                type={''}
                                index={''}
                                styling={styles.dateInput}
                            />
                            <Input
                                placeholder={'Preferred Brand'}
                                onChangeText={text => setpreferredBrand(text)}
                                inputStyle={{ fontSize: hp('2.3%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                value={preferredBrand}
                                inputContainerStyle={{ borderBottomWidth: 0 }}
                                containerStyle={{ width: wp('80%'), marginVertical: hp('1%'), borderWidth: 1, borderColor: 'rgb(232, 232, 232)', backgroundColor: 'rgba(179, 179, 179, .1)', height: hp('7%'), borderRadius: 4 }}
                            />
                            <Input
                                placeholder={'Specification'}
                                onChangeText={text => setspecification(text)}
                                inputStyle={{ fontSize: hp('2.3%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                value={specification}
                                inputContainerStyle={{ borderBottomWidth: 0 }}
                                multiline={true}
                                containerStyle={{ width: wp('80%'), alignSelf: 'center', borderWidth: 1, borderColor: 'rgb(232, 232, 232)', backgroundColor: 'rgba(179, 179, 179, .1)', borderRadius: 4 }}
                            />
                        </View>
                    </View>
                </View>
            );
            break;

        case 'Inspection Agency Details: To Enter Name, Contact person, Number, Email':
            var [componentName, setcomponentName] = useState('');
            var [pdiAgencyName, setpdiAgencyName] = useState('');
            var [pdiDate, setpdiDate] = useState('');
            var [contactPersonName, setcontactPersonName] = useState('');
            var [contactPersonMail, setcontactPersonMail] = useState('');
            var [contactPersonPhoneNumber, setcontactPersonPhoneNumber] = useState('')

            var [IconPosition, setIconPosition] = useState(false);

            var onPressDate = (value, index, item) => {
                setpdiDate(value)
            }

            return (
                <View>
                    <View style={[styles.titleViewStyle, { width: wp('90%') }]}>
                        <Text style={styles.titleStyle1}>{specificationsTitles[params.selectedChecklistTitle]}</Text>
                    </View>

                    <View style={styles.cardWrapperViewStyle}>
                        <View style={styles.cardHeaderViewStyle}>
                            <TouchableOpacity
                                onPress={() => onPressSave(
                                    [
                                        {
                                            'component_name': componentName,
                                            'pdi_agency_name': pdiAgencyName,
                                            'pdi_date': pdiDate,
                                            'contact_person_name': contactPersonName,
                                            'contact_person_email': contactPersonMail,
                                            'contact_phone_number': contactPersonPhoneNumber
                                        }
                                    ]
                                )}
                            >
                                <Text style={styles.saveStyle}>Save</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={{ padding: '5%', alignSelf: 'center' }}>
                            <Input
                                placeholder={'Component Name'}
                                onChangeText={text => setcomponentName(text)}
                                inputStyle={{ fontSize: hp('2.3%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                value={componentName}
                                inputContainerStyle={{ borderBottomWidth: 0 }}
                                containerStyle={{ width: wp('80%'), marginVertical: hp('1%'), borderWidth: 1, borderColor: 'rgb(232, 232, 232)', backgroundColor: 'rgba(179, 179, 179, .1)', height: hp('7%'), borderRadius: 4 }}
                            />
                            <Input
                                placeholder={'PDI Agency Name'}
                                onChangeText={text => setpdiAgencyName(text)}
                                inputStyle={{ fontSize: hp('2.3%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                value={componentName}
                                inputContainerStyle={{ borderBottomWidth: 0 }}
                                containerStyle={{ width: wp('80%'), marginVertical: hp('1%'), borderWidth: 1, borderColor: 'rgb(232, 232, 232)', backgroundColor: 'rgba(179, 179, 179, .1)', height: hp('7%'), borderRadius: 4 }}
                            />
                            <MyDatePicker
                                onSelectField={onPressDate}
                                type={''}
                                index={''}
                                styling={styles.dateInput}
                            />
                            <Input
                                placeholder={'Contact Person Name'}
                                onChangeText={text => setcontactPersonName(text)}
                                inputStyle={{ fontSize: hp('2.3%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                value={componentName}
                                inputContainerStyle={{ borderBottomWidth: 0 }}
                                containerStyle={{ width: wp('80%'), marginVertical: hp('1%'), borderWidth: 1, borderColor: 'rgb(232, 232, 232)', backgroundColor: 'rgba(179, 179, 179, .1)', height: hp('7%'), borderRadius: 4 }}
                            />
                            <Input
                                placeholder={'Contact Person Mail'}
                                onChangeText={text => setcontactPersonMail(text)}
                                inputStyle={{ fontSize: hp('2.3%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                value={componentName}
                                inputContainerStyle={{ borderBottomWidth: 0 }}
                                containerStyle={{ width: wp('80%'), marginVertical: hp('1%'), borderWidth: 1, borderColor: 'rgb(232, 232, 232)', backgroundColor: 'rgba(179, 179, 179, .1)', height: hp('7%'), borderRadius: 4 }}
                            />
                            <Input
                                placeholder={'Phone Number'}
                                onChangeText={text => setcontactPersonPhoneNumber(text)}
                                inputStyle={{ fontSize: hp('2.3%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                value={componentName}
                                inputContainerStyle={{ borderBottomWidth: 0 }}
                                containerStyle={{ width: wp('80%'), marginVertical: hp('1%'), borderWidth: 1, borderColor: 'rgb(232, 232, 232)', backgroundColor: 'rgba(179, 179, 179, .1)', height: hp('7%'), borderRadius: 4 }}
                            />
                        </View>
                    </View>
                </View>
            );
            break;

        default:
            var [date, setdate] = useState('');
            var [percentage, setpercentage] = useState('');
            var [comment, setcomment] = useState('');

            var [IconPosition, setIconPosition] = useState(false);

            var onPressDate = (value, index, item) => {
                setdate(value)
            }

            return (
                <View>
                    <View style={[styles.titleViewStyle, { width: wp('90%') }]}>
                        <Text style={styles.titleStyle1}>{'Edit OEM details'}</Text>
                    </View>

                    <View style={styles.cardWrapperViewStyle}>
                        <View style={styles.cardHeaderViewStyle}>
                            <TouchableOpacity
                                onPress={() => onPressSave(
                                    [
                                        {
                                            'date': date,
                                            'percentage': percentage,
                                            'comment': comment,
                                        }
                                    ]
                                )}
                            >
                                <Text style={styles.saveStyle}>Save</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={{ padding: '5%', alignSelf: 'center' }}>
                            <MyDatePicker
                                onSelectField={onPressDate}
                                type={''}
                                index={''}
                                styling={styles.dateInput}
                            />
                            <Input
                                placeholder={'Percentage'}
                                onChangeText={text => setpercentage(text)}
                                inputStyle={{ fontSize: hp('2.3%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                value={percentage}
                                inputContainerStyle={{ borderBottomWidth: 0 }}
                                containerStyle={{ width: wp('80%'), marginVertical: hp('1%'), borderWidth: 1, borderColor: 'rgb(232, 232, 232)', backgroundColor: 'rgba(179, 179, 179, .1)', height: hp('7%'), borderRadius: 4 }}
                            />
                            <Input
                                placeholder={'Comment'}
                                onChangeText={text => onSelectField(text, index, 'cable_specification')}
                                inputStyle={{ fontSize: hp('2.3%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                value={comment}
                                inputContainerStyle={{ borderBottomWidth: 0 }}
                                multiline={true}
                                containerStyle={{ width: wp('80%'), alignSelf: 'center', borderWidth: 1, borderColor: 'rgb(232, 232, 232)', backgroundColor: 'rgba(179, 179, 179, .1)', borderRadius: 4 }}
                            />
                        </View>
                    </View>
                </View>
            );
            break;
    }
}

export default SpecificationHelper;


const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 40,
        alignItems: "center"
    },

    cardWrapperViewStyle: {
        marginLeft: wp('5%'),
        marginTop: hp('3%'),
        width: wp('90%'),
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        borderRadius: 5
    },

    dateInputForLOD: {
        width: wp('55%'),
        paddingHorizontal: wp('2%'),
        // paddingVertical: hp('.1%'),
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        backgroundColor: 'rgba(179, 179, 179, .1)',
        alignSelf: 'center',
        borderRadius: 4,
    },

    dateInput: {
        width: wp('80%'),
        paddingHorizontal: wp('3.5%'),
        paddingVertical: hp('.6%'),
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        backgroundColor: 'rgba(179, 179, 179, .1)',
        alignSelf: 'center',
        borderRadius: 4,
        marginVertical: hp('1%')
    },

    cardHeaderViewStyle: {
        height: hp('5.3%'),
        alignItems: 'flex-start',
        paddingHorizontal: '5%',
        justifyContent: 'center',
        backgroundColor: 'rgb(241, 241, 241)',
        borderRadius: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },

    titleViewStyle: {
        marginLeft: wp('5%'),
        width: wp('80%'),
        marginTop: hp('5%'),
        height: hp('6%'),
        alignItems: 'flex-start',
        paddingHorizontal: wp('4%'),
        justifyContent: 'center',
        backgroundColor: '#f1f1f1',
        borderRadius: 5
    },

    titleStyle: {
        width: wp('20%'),
        color: 'rgb(143, 143, 143)',
        fontSize: hp('2.2%'),
        fontFamily: 'SourceSansPro-Regular'
    },

    titleStyle1: {
        color: 'rgb(143, 143, 143)',
        fontSize: hp('2.2%'),
        fontFamily: 'SourceSansPro-Regular'
    },

    saveStyle: {
        color: '#3d87f1',
        fontSize: hp('2.2%'),
        fontFamily: 'SourceSansPro-Regular'
    },

    titleTxt: {
        color: 'rgb(68, 67, 67)',
        fontSize: hp('2.2%'),
        fontFamily: 'SourceSansPro-Semibold',
        marginBottom: hp('1%')
    },

    Txt: {
        color: 'rgb(68, 67, 67)',
        fontSize: hp('2.2%'),
        fontFamily: 'SourceSansPro-Semibold',
    },

    CommentsTxt: {
        color: 'rgb(143, 143, 143)',
        fontSize: hp('1.8%'),
        fontFamily: 'SourceSansPro-Regular',
    },

    CircleShapeView: {
        width: 16,
        height: 16,
        borderRadius: 8,
        backgroundColor: '#3d87f1',
        marginBottom: hp('2%'),
    },

    dueDateTxt: {
        fontFamily: 'SourceSansPro-Regular',
        fontSize: hp('2%'),
        color: 'rgb(179, 179, 179)',
        borderRightWidth: 1,
        borderRightColor: 'rgb(179, 179, 179)',
        paddingRight: '2%'
    },

});
