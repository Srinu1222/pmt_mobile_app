import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TouchableOpacity,
    TextInput,
    KeyboardAvoidingView,
    ScrollView,
    Animated,
    Pressable
} from 'react-native';
import FastImage from 'react-native-fast-image';
import LottieView from 'lottie-react-native';
import { useDispatch, useSelector } from "react-redux";
import React, { useState, useRef, useMemo, useEffect } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import notifyMessage from '../../ToastMessages';
import { FlatList } from 'react-native-gesture-handler';
import EmptyLoader from '../EmptyLoader'
import { FAB, Icon, BottomSheet, ButtonGroup, Input } from 'react-native-elements';
import { Card as PaperCard, Divider, RadioButton, Button, Portal, Dialog } from 'react-native-paper';
import { Keyboard } from 'react-native';
import DocumentPicker from 'react-native-document-picker';


const UploadPo = (params) => {
    var [fileName, setfileName] = useState('')

    const onPressUploadDocument = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.allFiles],
            });

            // Update Documents List
            let dict = {
                file_name: fileName,
                file_url: res.uri
            }
            params.uploadSetMethod([...params.uploadList, dict]);
            setfileName('')
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
            } else {
                throw err;
            }
        }
    }

    const UploadButton = () => {
        return (
            <View style={{ flexDirection: 'row' }}>
                <View style={{ borderLeftWidth: 2, borderLeftColor: 'rgb(143, 143, 143)', marginHorizontal: wp('3%') }}></View>
                <TouchableOpacity
                    onPress={() => onPressUploadDocument()}
                    style={{ borderWidth: 1, borderRadius: 4, alignSelf: 'center', borderColor: '#3d87f1', padding: '4%', width: wp('20%') }}
                >
                    <Text style={{ textAlign: 'center', color: '#3d87f1' }}>
                        Upload
              </Text>
                </TouchableOpacity>
            </View>

        );
    }

    return (
        <View>
            <Text style={[styles.textStyle, { marginTop: hp('3%'), fontSize: hp('2.3%'), marginLeft: wp('.5%') }]}>Upload PO</Text>
            <Input
                placeholder={'Enter File Name'}
                onChangeText={text => setfileName(text)}
                inputStyle={{ fontSize: hp('2.3%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                value={fileName}
                inputContainerStyle={{ borderBottomWidth: 0 }}
                rightIcon={UploadButton}
                containerStyle={{ width: wp('80%'), marginVertical: hp('1%'), borderWidth: 1, borderColor: 'rgb(232, 232, 232)', backgroundColor: 'rgba(179, 179, 179, .1)', height: hp('7%'), borderRadius: 4 }}
            />
            <Text style={[styles.textStyle, { marginTop: hp('1%'), fontSize: hp('2.3%'), marginLeft: wp('.5%') }]}>Uploaded files</Text>

            {
                params.uploadList.length === 0 ?
                    <View style={{ width: wp('80%'), height: 100, alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={styles.emptyText}>No files uploaded yet</Text>
                    </View>
                    :
                    params.uploadList.map((item, index) => {
                        return (
                            <View key={index} style={{ flexDirection: 'row', height: hp('7%'), marginVertical: hp('1%'), alignItems: 'center', paddingHorizontal: '4%', borderRadius: 4, backgroundColor: 'rgb(236, 243, 254)', width: wp('60%'), justifyContent: 'space-between' }}>
                                <Text style={styles.titleText}>{item.file_name}</Text>
                                <Icon
                                    color={'#F1696A'}
                                    type='feather'
                                    name='x'
                                    onPress={() => {
                                        let temp = [...params.uploadList]
                                        temp.splice(index, 1)
                                        params.uploadSetMethod([...temp])
                                    }}
                                />
                            </View>
                        );
                    })
            }
        </View>
    );
}

export default UploadPo;

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: "#ffffff",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
    containerHeader: {
        width: Dimensions.get('window').width * 0.9,
        height: Dimensions.get('window').height,
        flex: 0.2,
        flexDirection: 'row',
        borderBottomWidth: 3,
        borderBottomColor: 'rgb(232,232,232)',
        justifyContent: 'space-between',

    },

    titleText: {
        fontSize: hp('2.2%'),
        color: '#3d87f1',
        fontFamily: 'SourceSansPro-SemiBold',
    },

    cancelText: {
        fontSize: hp('2.4%'),
        color: '#3d87f1',
        fontFamily: 'SourceSansPro-Regular',
    },

    emptyText: {
        fontSize: hp('2.4%'),
        color: 'rgb(179, 179, 179)',
        fontFamily: 'SourceSansPro-Regular',
    },

    images: {
        alignSelf: 'center',
    },

    containerBody: {
        flex: 2,
        display: 'flex',
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        justifyContent: 'flex-end',
        // paddingHorizontal: '5%'
    },

    btnStartNewProject: {
        marginTop: hp("2%"),
        width: wp("17%"),
        height: hp("4%"),
        backgroundColor: "#3d87f1",
        borderRadius: 4,
        marginHorizontal: wp('1%'),
        alignItems: "center",
        justifyContent: "center",
    },
    btnText: {
        color: "white",
        fontSize: hp("2.2%"),
        textAlign: "center",
        fontFamily: "SourceSansPro-Regular",
    },

});