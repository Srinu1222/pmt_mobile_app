import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TouchableOpacity,
    TextInput,
    KeyboardAvoidingView,
    ScrollView,
    Animated,
    Pressable
} from 'react-native';
import FastImage from 'react-native-fast-image';
import LottieView from 'lottie-react-native';
import { useDispatch, useSelector } from "react-redux";
import React, { useState, useRef, useMemo, useEffect } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import notifyMessage from '../../ToastMessages';
import { FlatList } from 'react-native-gesture-handler';
import Item from './DailyReportItem';
import EmptyDailyReport from '../EmptyScreens/DailyReport'
import * as actionCreators from '../../actions/daily-updates-action/index';
import Card from '../card'
import EmptyLoader from '../EmptyLoader'
import { FAB, Icon, BottomSheet, ButtonGroup, Input } from 'react-native-elements';
import { Card as PaperCard, RadioButton, Button, Portal, Dialog } from 'react-native-paper';
import { Keyboard } from 'react-native';
import Toast from "react-native-toast-message";
import { specificationsList } from "./constants"
import SpecificationHelper from './SpecificationHelper'
import { useSafeArea } from 'react-native-safe-area-context';
import DocumentBody from './DocumentBody'
import Header from './Header'

const DailyReport5 = ({ route, navigation }) => {


    var { title, project_id, token, completedList, onGoingList, tomorrowTasksList } = route.params

    const [uploadDocumentsList, setuploadDocumentsList] = useState([]);
    const [uploadPicturesList, setuploadPicturesList] = useState([]);

    const [isFilesUploadPage, setisFilesUploadPage] = useState(true);

    // Send Message
    const [messagesList, setmessagesList] = useState([]);
    const [inputText, setinputText] = useState('');
    const onPressChangeText = (text) => {
        setinputText(text)
    }
    const flatlistRef = useRef();
    useEffect(() => {
        setTimeout(() => (flatlistRef && flatlistRef.current) && flatlistRef.current.scrollToEnd({ animating: true }), 100)
    }, [messagesList, uploadDocumentsList, uploadPicturesList]);

    const onPressSend = (params) => {
        setmessagesList([...messagesList, inputText])
        setinputText('')
        Keyboard.dismiss()
    }


    // Dialog Component
    const [visible, setVisible] = React.useState(false);
    const updateVisibleState = (params) => {
        setVisible(false)
    }


    const MessagesListView = (props) => {
        const data = props.data
        return (
            <View style={{ alignItems: 'flex-end', marginHorizontal: wp('5%') }}>
                {
                    data.map(
                        (i, index) => {
                            return (
                                <View key={index} style={{ backgroundColor: '#3d87f1', borderRadius: 5, padding: '2%', marginVertical: hp('1%') }}>
                                    <Text style={{ color: 'white' }}>{i}</Text>
                                </View>
                            );
                        }
                    )
                }
            </View>
        );
    }

    var flatListData = [
        {
            'type': 'Spacer',
            'data': ''
        },
        {
            'type': 'body',
            'data': []
        },
        {
            'type': 'MessagesList',
            'data': messagesList
        }
    ]

    return (
        <View style={[styles.container]}>

            {
                visible && <DialogComponent
                    visible={visible}
                    updateState={updateVisibleState}
                    navigation={navigation}
                />
            }

            <Header title={title} updateState={updateVisibleState} />

            <KeyboardAvoidingView keyboardVerticalOffset={10} behavior='position'>
                <View style={{ height: Dimensions.get('window').height * 0.8 }}>
                    <FlatList
                        data={flatListData}
                        keyExtractor={(item, index) => index}
                        ref={flatlistRef}
                        renderItem={
                            ({ item, index }) => {
                                if (item.type === 'body') {
                                    return (<DocumentBody
                                        uploadList={isFilesUploadPage ? uploadDocumentsList : uploadPicturesList}
                                        uploadSetMethod={isFilesUploadPage ? setuploadDocumentsList : setuploadPicturesList}
                                        setisFilesUploadPage={setisFilesUploadPage}
                                        isFilesUploadPage={isFilesUploadPage}
                                        navigation={navigation}
                                        project_id={project_id}
                                        token={token}
                                        completedList={completedList}
                                        onGoingList={onGoingList}
                                        tomorrowTasksList={tomorrowTasksList}
                                        uploadDocumentsList={uploadDocumentsList}
                                        uploadPicturesList={uploadPicturesList}
                                    />);
                                } else if (item.type === 'MessagesList') {
                                    return (<MessagesListView data={item.data} />);
                                } else if (item.type === 'Spacer') {
                                    return (<View style={{ height: hp('45%') }}></View>);
                                }
                            }
                        }
                    />
                </View>

                <Input
                    placeholder='Type here...'
                    onChangeText={text => onPressChangeText(text)}
                    inputStyle={{ fontSize: hp('2%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                    value={inputText}
                    inputContainerStyle={{ borderBottomWidth: 0 }}
                    containerStyle={{ borderWidth: 0.2, marginVertical: hp('2%'), height: hp('7%'), borderColor: 'rgb(143, 143, 143)', borderRadius: 2 }}
                    rightIcon={(params) =>
                        <Icon
                            color={'#3d87f1'}
                            onPress={() => onPressSend()}
                            type='ionicon'
                            name='send-outline'
                        />
                    }
                />
            </KeyboardAvoidingView>
        </View >
    );
};

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: "#ffffff",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
    containerHeader: {
        width: Dimensions.get('window').width * 0.9,
        height: Dimensions.get('window').height,
        flex: 0.2,
        flexDirection: 'row',
        borderBottomWidth: 3,
        borderBottomColor: 'rgb(232,232,232)',
        justifyContent: 'space-between',

    },

    titleText: {
        fontSize: hp('3.15%'),
        color: 'rgb(68, 67, 67)',
        fontFamily: 'SourceSansPro-SemiBold',
    },

    cancelText: {
        fontSize: hp('2.4%'),
        color: '#3d87f1',
        fontFamily: 'SourceSansPro-Regular',
    },

    images: {
        alignSelf: 'center',
    },

    containerBody: {
        flex: 2,
        display: 'flex',
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        justifyContent: 'flex-end',
        // paddingHorizontal: '5%'
    },

    btnStartNewProject: {
        marginTop: hp("2%"),
        width: wp("17%"),
        height: hp("4%"),
        backgroundColor: "#3d87f1",
        borderRadius: 4,
        marginHorizontal: wp('1%'),
        alignItems: "center",
        justifyContent: "center",
    },
    btnText: {
        color: "white",
        fontSize: hp("2.2%"),
        textAlign: "center",
        fontFamily: "SourceSansPro-Regular",
    },

});

export default DailyReport5;

