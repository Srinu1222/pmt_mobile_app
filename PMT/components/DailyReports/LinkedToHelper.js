import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TouchableOpacity,
    TextInput,
    KeyboardAvoidingView,
    ScrollView,
    Animated,
    Pressable
} from 'react-native';
import FastImage from 'react-native-fast-image';
import LottieView from 'lottie-react-native';
import { useDispatch, useSelector } from "react-redux";
import React, { useState,useEffect } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import notifyMessage from '../../ToastMessages';
import { FlatList } from 'react-native-gesture-handler';
import Card from '../card'
import EmptyLoader from '../EmptyLoader'
import { FAB, Icon, BottomSheet, Badge, ButtonGroup, Input } from 'react-native-elements';
import { Card as PaperCard, Divider, RadioButton, Button, Portal, Dialog } from 'react-native-paper';
import { Keyboard } from 'react-native';
import DocumentPicker from 'react-native-document-picker';
import MyModalDropDown from './ModalDropDown';
import config from '../../config';
import axios from 'axios';
import Toast from "react-native-toast-message";
import * as actionCreators from '../../actions/daily-updates-action/index';
import * as ganttactionCreators from '../../actions/gantt-stage-action/index';

import DocumentViewDialogBox from './DocumentViewDialogBox';

const LinkedToHelper = (params) => {

    const {
        project_id,
        token,
        isFilesUploadPage,
        uploadDocumentsList,
        setuploadDocumentsList,
        uploadPicturesList,
        setuploadPicturesList,
        DocumentDataList,
        setDocumentDataList,
        PicturesDataList,
        setPicturesDataList
    } = params

    const Stages = [
        'PRE-REQUISITES', 'APPROVALS', 'ENGINEERING',
        'PROCUREMENT', 'MATERIAL HANDLING', 'CONSTRUCTION', 'SITE HAND OVER'
    ]

    const [isViewDocuments, setisViewDocuments] = useState(false);
    const [selectedEventName, setselectedEventName] = useState('');
    const [stageEventsList, setstageEventsList] = useState([]);
    const [stageEventsNamesList, setstageEventsNamesList] = useState([]);
    const [fileName, setfileName] = useState('');
    const [description, setDescription] = useState('');
    const [selectedStage, setselectedStage] = useState('');
    const [selectedEvent, setselectedEvent] = useState('');
    const [docactiveIndex, setdocactiveIndex] = useState(0);
    const [picactiveIndex, setpicactiveIndex] = useState(0);

    // useEffect(() => {
    //     setDescription('')
    //     setfileName('')

    // }, [isFilesUploadPage])

    const dispatch = useDispatch();
    const removeFile = (index, item) => {
        if (isFilesUploadPage) {
            DocumentDataList.splice(index, 1)
            setDocumentDataList([...DocumentDataList])
            uploadDocumentsList.splice(index, 1)
            setuploadDocumentsList([...uploadDocumentsList])
            setdocactiveIndex(docactiveIndex - 1)
        } else {
            PicturesDataList.splice(index, 1)
            setPicturesDataList([...PicturesDataList])
            uploadPicturesList.splice(index, 1)
            setuploadPicturesList([...uploadPicturesList])
            setpicactiveIndex(picactiveIndex - 1)
        }
    }

    const SetSelectedStage = (index, value) => {
        setselectedStage(value)
        axios.get(config.API_URL + `get_stage_events_basic_details/`, {
            headers: {
                Authorization: 'Token ' + token
            },
            params: {
                project_id: project_id,
                stage: value
            }
        })
            .then(result => {
                let stageEventObjectsList = result.data.stage_events_list
                setstageEventsList(stageEventObjectsList)
                let eventNamesList = stageEventObjectsList.map((i, index) => i.name)
                setstageEventsNamesList(eventNamesList)
            })
            .catch(err => {
                return //(err.response)
            });
    }

    const handleDescription = (text) => {
        setDescription(text)
        if (isFilesUploadPage) {
            let tempList = [...DocumentDataList]
            tempList[docactiveIndex] = { ...tempList[docactiveIndex], description: text }
            setDocumentDataList([...tempList])
        } else {
            let tempList = [...PicturesDataList]
            tempList[picactiveIndex] = { ...tempList[picactiveIndex], description: text }
            setPicturesDataList([...tempList])
        }
    }

    const handleFileName = (text) => {
        setfileName(text)
        if (isFilesUploadPage) {
            let tempList = [...DocumentDataList]
            tempList[docactiveIndex] = { ...tempList[docactiveIndex], title: text }
            setDocumentDataList([...tempList])
        } else {
            let tempList = [...PicturesDataList]
            tempList[picactiveIndex] = { ...tempList[picactiveIndex], title: text }
            setPicturesDataList([...tempList])
        }
    }

    const SetSelectedEvent = (index, value) => {

        let eventId = stageEventsList[index].id

        setselectedEventName(value)
        setselectedEvent(eventId)

        if (isFilesUploadPage) {
            let tempList = [...DocumentDataList]
            tempList[docactiveIndex] = { ...tempList[docactiveIndex], event_id: eventId, event_name: value, stage: selectedStage }
            setDocumentDataList([...tempList])
        } else {
            let tempList = [...PicturesDataList]
            tempList[picactiveIndex] = { ...tempList[picactiveIndex], event_id: eventId, event_name: value, stage: selectedStage }
            setPicturesDataList([...tempList])
        }
    }

    const onPressUploadDocument = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.allFiles],
            });
            let uploadFile = {
                uri: res.uri,
                type: res.type,
                name: res.name
            }
            if (isFilesUploadPage) {
                setuploadDocumentsList([...uploadDocumentsList, uploadFile])
                setdocactiveIndex(docactiveIndex + 1)
            } else {
                setuploadPicturesList([...uploadPicturesList, uploadFile])
                setpicactiveIndex(picactiveIndex + 1)
            }
            setfileName('')
            setDescription('')
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
            } else {
                throw err;
            }
        }
    }

    const UploadButton = () => {
        return (
            <View style={{ flexDirection: 'row' }}>
                <View style={{ borderLeftWidth: 2, borderLeftColor: 'rgb(143, 143, 143)', marginHorizontal: wp('3%') }}></View>
                <View
                    style={{ borderWidth: 1, borderRadius: 4, alignSelf: 'center', backgroundColor: '#3d87f1', borderColor: '#3d87f1', padding: '4%', width: wp('15%') }}
                >
                    <Text style={{ textAlign: 'center', color: '#fff' }}>View</Text>
                </View>
            </View>
        );
    }

    return (
            <View>
                <View
                    opacity={
                        isFilesUploadPage ?
                            (uploadDocumentsList.length == 0 ? 0.5 : 1)
                            :
                            (uploadPicturesList.length == 0 ? 0.5 : 1)
                    }
                >
                    <TouchableOpacity
                        onPress={() => setisViewDocuments(true)}
                        disabled={
                            isFilesUploadPage ?
                                (uploadDocumentsList.length == 0 ? true : false)
                                :
                                (uploadPicturesList.length == 0 ? true : false)
                        }
                    >
                        {
                            isViewDocuments &&
                            <DocumentViewDialogBox
                                visible={isViewDocuments}
                                setVisible={setisViewDocuments}
                                data={isFilesUploadPage ? DocumentDataList : PicturesDataList}
                                removeFile={removeFile}
                                isFilesUploadPage={isFilesUploadPage}
                            />
                        }
                        <View style={{ flexDirection: 'row', height: hp('7%'), marginVertical: hp('5%'), alignItems: 'center', paddingHorizontal: '4%', borderRadius: 4, backgroundColor: 'rgb(236, 243, 254)', width: wp('80%'), marginLeft: wp('5%'), justifyContent: 'space-between' }}>
                            <Text style={styles.titleText}>
                                {isFilesUploadPage ? 'Uploaded Files' : 'Uploaded Pictures'}
                                <Badge
                                    status="primary"
                                    value={isFilesUploadPage ? uploadDocumentsList.length : uploadPicturesList.length}
                                    containerStyle={{ position: 'absolute', top: -4, right: -4 }}
                                />
                            </Text>
                            <UploadButton />
                        </View>
                    </TouchableOpacity>
                </View>

                <View style={{ marginLeft: wp('5%'), width: wp('75%'), height: hp('6%'), marginTop: hp('-2%'), paddingHorizontal: wp('4%'), justifyContent: 'center', backgroundColor: '#f1f1f1', borderRadius: 5 }}>
                    <Text style={{ color: 'rgb(143, 143, 143)', fontSize: hp('2%'), fontFamily: 'SourceSansPro-Regular' }}>
                        {isFilesUploadPage ? 'Please Upload the Required Documents' : 'Please share picture of work done.'}
                    </Text>
                </View>
                <View style={{ width: wp('95%'), padding: '6.5%', }}>
                    <Text style={{ color: 'rgb(68, 67, 67)', fontSize: hp('2%'), fontFamily: 'SourceSansPro-Regular' }}>
                        {isFilesUploadPage ? 'Please link the Stage and Event of the document you are uploading and enter the file name.' : 'Please link the Stage and Event of the picture you are uploading and enter the file name.'}
                    </Text>
                </View>

                <MyModalDropDown
                    title={'Stage'}
                    data={Stages}
                    setFunction={SetSelectedStage}
                />

                <MyModalDropDown
                    title={'Event'}
                    data={stageEventsNamesList}
                    setFunction={SetSelectedEvent}
                />

                <Input
                    placeholder={'Enter File Name'}
                    onChangeText={(text) => {
                        if (text === '') {
                            handleFileName(null)
                            return;
                        }
                        handleFileName(text)
                    }
                    }
                    inputStyle={{ fontSize: hp('2.2%'), width: wp('90%'), color: 'rgb(68, 67, 67)' }}
                    value={fileName}
                    inputContainerStyle={{ borderBottomWidth: 0 }}
                    containerStyle={{ width: wp('80%'), marginVertical: hp('2%'), marginLeft: wp('5%'), backgroundColor: 'rgba(179,179, 179, 0.1)', height: hp('7%'), borderRadius: 4 }}
                />

                <Input
                    placeholder={'Description'}
                    onChangeText={(text) => {
                        if (text === '') {
                            handleDescription(null)
                            return;
                        }
                        handleDescription(text)
                    }}
                    inputStyle={{ fontSize: hp('2.2%'), width: wp('90%'), color: 'rgb(68, 67, 67)' }}
                    value={
                        description
                    }
                    multiline={true}
                    inputContainerStyle={{ borderBottomWidth: 0 }}
                    containerStyle={{ width: wp('80%'), marginBottom: hp('1%'), marginLeft: wp('5%'), backgroundColor: 'rgba(179,179, 179, 0.1)', borderRadius: 4 }}
                />

                <TouchableOpacity
                    onPress={() => onPressUploadDocument()}
                >
                    <View style={{ flexDirection: 'row', height: hp('7%'), alignItems: 'center', paddingHorizontal: '4%', borderRadius: 4, backgroundColor: 'rgb(236, 243, 254)', width: wp('80%'), marginLeft: wp('5%'), justifyContent: 'space-between' }}>
                        <Text style={styles.titleText}>
                            {isFilesUploadPage ? 'Upload Document' : 'Upload Picture'}
                        </Text>
                        <Icon
                            color={'#3d87f1'}
                            type='feather'
                            name={isFilesUploadPage ? 'file-plus' : 'camera'}
                        />
                    </View>
                </TouchableOpacity>
            </View >
    );
}


const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: "#ffffff",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    containerHeader: {
        width: Dimensions.get('window').width * 0.9,
        height: Dimensions.get('window').height,
        flex: 0.2,
        flexDirection: 'row',
        borderBottomWidth: 3,
        borderBottomColor: 'rgb(232,232,232)',
        justifyContent: 'space-between',
    },

    titleText: {
        fontSize: hp('2.2%'),
        color: '#3d87f1',
        fontFamily: 'SourceSansPro-SemiBold',
    },

    cancelText: {
        fontSize: hp('2.4%'),
        color: '#3d87f1',
        fontFamily: 'SourceSansPro-Regular',
    },

    images: {
        alignSelf: 'center',
    },

    containerBody: {
        flex: 2,
        display: 'flex',
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        justifyContent: 'flex-end',
        // paddingHorizontal: '5%'
    },

    btnStartNewProject: {
        marginTop: hp("2%"),
        width: wp("17%"),
        height: hp("4%"),
        backgroundColor: "#3d87f1",
        borderRadius: 4,
        marginHorizontal: wp('1%'),
        alignItems: "center",
        justifyContent: "center",
    },
    btnText: {
        color: "white",
        fontSize: hp("2.2%"),
        textAlign: "center",
        fontFamily: "SourceSansPro-Regular",
    },

});


export default LinkedToHelper;