import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TouchableOpacity,
    TextInput,
    KeyboardAvoidingView,
    ScrollView,
    Animated,
    Pressable
} from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import FastImage from 'react-native-fast-image';
import LottieView from 'lottie-react-native';
import { useDispatch, useSelector } from "react-redux";
import React, { useState, useRef, useMemo, useEffect } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { FAB, Icon, BottomSheet, ButtonGroup, Input } from 'react-native-elements';
import { Card as PaperCard, RadioButton, Divider, Button, Portal, Dialog } from 'react-native-paper';


const DocumentViewDialogBox = (params) => {

    const { visible, setVisible, data, removeFile } = params

    return (
        <Portal>
            <Dialog visible={visible} onDismiss={() => setVisible(false)}>
                <View style={{ width: wp('90%'), height: hp('60%'), padding: '4%', alignSelf: 'center', borderRadius: 4, backgroundColor: 'white' }}>
                    <View style={{ justifyContent: 'space-between', paddingVertical: '4%', flexDirection: 'row' }}>
                        <Text style={styles.linkedToTxt}>Linked To</Text>
                        <Icon
                            color={'rgb(143, 143, 143)'}
                            type='feather'
                            name={'x'}
                            onPress={() => setVisible(false)}
                        />
                    </View>
                    <View style={{  height: hp('50%'), borderRadius: 4, padding: '2%', }}>
                        <FlatList
                            data={data}
                            keyExtractor={(item, index) => index}
                            renderItem={({ item, index }) => {
                                return (
                                    <View
                                        key={index}
                                        style={{backgroundColor: 'rgb(236, 243, 254)',}}
                                    >
                                        <View style={{ flexDirection: 'row', paddingHorizontal: wp('2%'), alignItems: 'center', justifyContent: 'space-between' }}>
                                            <View style={{ width: wp('60%') }}>
                                                <Text style={[styles.eventTxt]}>{item?.stage}->{item?.event_name}</Text>
                                                <Text style={[styles.linkedToTxt]}>{item?.title}</Text>
                                            </View>
                                            <TouchableOpacity
                                                onPress={() => removeFile(index, item)}
                                            >
                                                < FastImage
                                                    style={{ width: wp('6%'), height: hp('3%') }}
                                                    source={require('../../assets/icons/removetask.png')
                                                    }
                                                    resizeMode={FastImage.resizeMode.contain}
                                                />
                                            </TouchableOpacity>
                                        </View>
                                        {
                                            data.length != index + 1 &&
                                            <Divider style={{ backgroundColor: 'rgba(0,0,0,0.1)', marginVertical: hp('1%') }} />
                                        }
                                    </View>
                                );
                            }
                            }
                        />
                    </View>
                </View>
            </Dialog>
        </Portal>
    );
}

const styles = StyleSheet.create({

    linkedToTxt: {
        fontSize: hp('2.2%'),
        color: '#3d87f1',
        fontFamily: 'SourceSansPro-SemiBold',
    },
    eventTxt: {
        fontSize: hp('2%'),
        color: 'rgb(68, 67, 67)',
        paddingVertical: hp('0.5%'),
        fontFamily: 'SourceSansPro-Regular',
    }
});
export default DocumentViewDialogBox;
