import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TouchableOpacity,
    TextInput,
    KeyboardAvoidingView,
    ScrollView,
    Animated,
    Pressable
} from 'react-native';
import FastImage from 'react-native-fast-image';
import LottieView from 'lottie-react-native';
import { useDispatch, useSelector } from "react-redux";
import React, { useState, useRef, useMemo, useEffect } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import notifyMessage from '../../ToastMessages';
import { FlatList } from 'react-native-gesture-handler';
import Item from '../DailyReports/DailyReportItem';
import EmptyDailyReport from '../EmptyScreens/DailyReport'
import Card from '../../components/card'
import EmptyLoader from '../EmptyLoader'
import { FAB, Icon, BottomSheet, ButtonGroup, Input } from 'react-native-elements';
import { Card as PaperCard, RadioButton, Button, Portal, Dialog } from 'react-native-paper';
import { Keyboard } from 'react-native';
import Toast from "react-native-toast-message";
import { specificationsList } from "./constants"
import SpecificationHelper from './SpecificationHelper'
import DialogComponent from './DialogComponent'
import Header from './Header'
import * as ganttActionCreators from "../../actions/gantt-stage-action/index";


const DailyReport4 = ({ route, navigation }) => {

    var { title, specificationsData, project_id, token, completedList, onGoingList, tomorrowTasksList } = route.params

    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(ganttActionCreators.getSpoc(token, { project_id: project_id }));
        // dispatch(ganttActionCreators.getDrawings());
    }, []);

    const spocReducer = useSelector((state) => {
        return state.spocReducers.spoc.get;
    });

    // Send Message
    const [messagesList, setmessagesList] = useState([]);
    const [inputText, setinputText] = useState('');
    const onPressChangeText = (text) => {
        setinputText(text)
    }
    const flatlistRef = useRef();
    useEffect(() => {
        setTimeout(() => (flatlistRef && flatlistRef.current) && flatlistRef.current.scrollToEnd({ animating: true }), 100)
    }, [messagesList]);

    const onPressSend = (params) => {
        setmessagesList([...messagesList, inputText])
        setinputText('')
        Keyboard.dismiss()
    }

    const MessagesListView = (props) => {
        const data = props.data
        return (
            <View style={{ alignItems: 'flex-end', marginHorizontal: wp('5%') }}>
                {
                    data.map(
                        (i, index) => {
                            return (
                                <View key={index} style={{ backgroundColor: '#3d87f1', borderRadius: 5, padding: '2%', marginVertical: hp('1%') }}>
                                    <Text style={{ color: 'white' }}>{i}</Text>
                                </View>
                            );
                        }
                    )
                }
            </View>
        );
    }

    const Specifications = (params) => {
        const { data, spoc_list } = params
        return (
            <View style={{ height: hp('74%'), borderWidth: 0, marginBottom: hp('2%') }}>
                <FlatList
                    data={data}
                    keyExtractor={(item, index) => index}
                    renderItem={
                        ({ item, index }) => <SpecificationHelper
                            selectedChecklistTitle={item.title}
                            selectedChecklistId={item.checklist_id}
                            event_id={item.event_id}
                            spoc_list={spoc_list}
                            token={token}
                            inventory={item.inventory}
                        />
                    }
                />
            </View>
        );
    }

    const onPressNext = (params) => {
        // navigation.navigate('dailyReports5', {
        //     'title': title,
        //     'project_id': project_id,
        //     'completedList': completedList,
        //     'onGoingList': onGoingList,
        //     'tomorrowTasksList': tomorrowTasksList
        // })
        navigation.navigate('reportSubmit')
    }

    const Buttons = (params) => {
        return (
            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginLeft: wp('5%') }}>
                <TouchableOpacity
                    onPress={() => navigation.goBack()}
                    style={[styles.btnStartNewProject, { backgroundColor: 'white', borderWidth: 1, borderColor: '#3d87f1' }]}
                >
                    <Text style={[styles.btnText, { color: '#3d87f1' }]}>Prev</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => onPressNext()}
                    style={[styles.btnStartNewProject]}
                >
                    <Text style={[styles.btnText]}>Submit</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => onPressNext()}
                    style={[styles.btnStartNewProject, { backgroundColor: 'white' }]}
                >
                    <Text style={[styles.btnText, { color: '#3d87f1' }]}>Skip</Text>
                </TouchableOpacity>
            </View>
        );
    }

    var flatListData = [
        {
            'type': 'Spacer',
            'data': ''
        },
        {
            'type': 'specifications',
            'data': specificationsData
        },
        {
            'type': 'MessagesList',
            'data': messagesList
        },
    ]

    // Dialog Component
    const [visible, setVisible] = React.useState(false);
    const updateVisibleState = (params) => {
        setVisible(false)
    }

    const renderDailyReport4 = (params) => {
        if (spocReducer.success.ok) {
            return (
                <View style={[styles.container]}>
                    {
                        visible && <DialogComponent
                            visible={visible}
                            updateState={updateVisibleState}
                            navigation={navigation}
                        />
                    }
                    <Header title={title} updateState={updateVisibleState} />
                    <KeyboardAvoidingView keyboardVerticalOffset={10} behavior='position'>
                        <View style={{ height: Dimensions.get('window').height * 0.75 }}>
                            <FlatList
                                data={flatListData}
                                keyExtractor={(item, index) => index}
                                ref={flatlistRef}
                                renderItem={
                                    ({ item, index }) => {
                                        if (item.type === 'Spacer') {
                                            return (<View style={{ height: hp('30%') }}></View>);
                                        } else if (item.type === 'specifications') {
                                            return (<Specifications data={item.data} spoc_list={spocReducer.success.data.members} />);
                                        } else if (item.type === 'MessagesList') {
                                            return (<MessagesListView data={item.data} />);
                                        }
                                    }
                                }
                            />
                        </View>

                        <View style={{borderWidth: 0}}>
                            <Buttons />
                            <Input
                                placeholder='Type here...'
                                onChangeText={text => onPressChangeText(text)}
                                inputStyle={{ fontSize: hp('2%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                                value={inputText}
                                inputContainerStyle={{ borderBottomWidth: 0 }}
                                containerStyle={{ borderWidth: 0.2, marginVertical: hp('2%'), height: hp('7%'), borderColor: 'rgb(143, 143, 143)', borderRadius: 2 }}
                                rightIcon={(params) =>
                                    <Icon
                                        color={'#3d87f1'}
                                        onPress={() => onPressSend()}
                                        type='ionicon'
                                        name='send-outline'
                                    />
                                }
                            />
                        </View>
                    </KeyboardAvoidingView>
                </View >
            );
        } else if (spocReducer.failure.error) {
            return notifyMessage(spocReducer.failure.message);
        } else {
            return (<EmptyLoader />)
        }
    }
    return renderDailyReport4();
};


const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: "#ffffff",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
    containerHeader: {
        width: Dimensions.get('window').width * 0.9,
        height: Dimensions.get('window').height,
        flex: 0.2,
        flexDirection: 'row',
        borderBottomWidth: 3,
        borderBottomColor: 'rgb(232,232,232)',
        justifyContent: 'space-between',

    },
    titleText: {
        fontSize: hp('3.15%'),
        color: 'rgb(68, 67, 67)',
        fontFamily: 'SourceSansPro-SemiBold',
    },

    cancelText: {
        fontSize: hp('2.4%'),
        color: '#3d87f1',
        fontFamily: 'SourceSansPro-Regular',
    },

    images: {
        alignSelf: 'center',
    },

    containerBody: {
        flex: 2,
        display: 'flex',
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        justifyContent: 'flex-end',
        // paddingHorizontal: '5%'
    },

    btnStartNewProject: {
        marginTop: hp("2%"),
        width: wp("17%"),
        height: hp("4%"),
        backgroundColor: "#3d87f1",
        borderRadius: 4,
        marginHorizontal: wp('1%'),
        alignItems: "center",
        justifyContent: "center",
    },
    btnText: {
        color: "white",
        fontSize: hp("2.2%"),
        textAlign: "center",
        fontFamily: "SourceSansPro-Regular",
    },

});

export default DailyReport4;

