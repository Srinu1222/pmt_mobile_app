import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TouchableOpacity,
    TextInput,
    KeyboardAvoidingView,
    ScrollView,
    Animated,
    Pressable
} from 'react-native';
import FastImage from 'react-native-fast-image';
import LottieView from 'lottie-react-native';
import { useDispatch, useSelector } from "react-redux";
import React, { useState, useRef, useMemo, useEffect } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import notifyMessage from '../../ToastMessages';
import { FlatList } from 'react-native-gesture-handler';
import Item from '../DailyReports/DailyReportItem';
import EmptyDailyReport from '../EmptyScreens/DailyReport'
import * as actionCreators from '../../actions/daily-updates-action/index';
import Card from '../../components/card'
import EmptyLoader from '../EmptyLoader'
import { FAB, Icon, BottomSheet, ButtonGroup, Input } from 'react-native-elements';
import { Card as PaperCard, RadioButton, Button, Portal, Dialog } from 'react-native-paper';
import { Keyboard } from 'react-native';
import Toast from "react-native-toast-message";
import { specificationsList } from "./constants"
import Header from './Header'



const DailyReport3 = ({ route, navigation }) => {
    const { completedList, onGoingList, todayTasksList, project_id, title, token } = route.params

    const [selectedCompletedTaskList, setselectedCompletedTaskList] = useState([]);
    const [selectedTomorrowTaskList, setselectedTomorrowTaskList] = useState([]);
    const [messagesList, setmessagesList] = useState([]);

    const dispatch = useDispatch();
    useEffect(() => {
        let temp = todayTasksList.filter(i => completedList.includes(i.title))
        setselectedCompletedTaskList([...temp])
        dispatch(actionCreators.getTomorrowTasks(token, project_id, false));
    }, []);

    const getMemberTasksReducer = useSelector((state) => {
        return state.dailyUpdatesReducer.tomorrowMemberTasks.get;
    });

    useEffect(() => {
        if (getMemberTasksReducer.success.ok) {
            let tomorrowTasks = []
            var tomorrowTaskList = getMemberTasksReducer?.success?.data?.tasks;
            let unfilteredTomorrowTasks = tomorrowTaskList.map((task) => { return task.title });
            tomorrowTasks = unfilteredTomorrowTasks.filter((task) => {if (!completedList.includes(task)) {
                return task
            }})
            setselectedTomorrowTaskList([...tomorrowTasks])
        }
    }, [getMemberTasksReducer.success.ok]);

    const [inputText, setinputText] = useState('');
    const onPressChangeText = (text) => {
        setinputText(text)
    }

    const onPressRemoveTask = (task, index) => {
        let x = 'Removed ' + task.toString() + 'from tomorrows task';
        Toast.show({
            text1: x,
            type: 'info',
            autoHide: true,
            position: 'bottom',
            visibilityTime: 1000
        })
        var temp = selectedTomorrowTaskList.filter(item => item !== task)
        setselectedTomorrowTaskList([...temp])
    }

    const flatlistRef = useRef();
    useEffect(() => {
        setTimeout(() => (flatlistRef && flatlistRef.current) && flatlistRef.current.scrollToEnd({ animating: true }), 100)
    }, [messagesList]);

    const onPressSend = (params) => {
        setmessagesList([...messagesList, inputText])
        setinputText('')
        Keyboard.dismiss()
    }

    const onPressNext = (params) => {
        let temp = []
        selectedCompletedTaskList.map((i, index) => {
            const isChecklistFound = specificationsList.includes(i.title.toString())
            const isOEMChecklistFound = i.title.toString().includes("OEM")
            const isSpecificationFilled = !(i.specification_filled)
            if (isChecklistFound || isOEMChecklistFound && isSpecificationFilled) {
                temp.push(i)
            }
        })

        // Remove temp hard code
        // temp = [
        //     {
        //         "specification_filled": false,
        //         "checklist_id": 1,
        //         "event_id": 1771,
        //         "event_name": "PV-Syst",
        //         "stage_name": "ENGINEERING",
        //         "status": false,
        //         "title": "List of All Drawings from Checklist"
        //     },

        //     {
        //         "specification_filled": false,
        //         "checklist_id": 2,
        //         "event_id": 1771,
        //         "event_name": "PV-Syst",
        //         "stage_name": "ENGINEERING",
        //         "status": false,
        //         "title": 'Type of Cable'
        //     },

        //     {
        //         "specification_filled": false,
        //         "checklist_id": 2,
        //         "event_id": 1771,
        //         "event_name": "PV-Syst",
        //         "stage_name": "ENGINEERING",
        //         "status": false,
        //         "title": 'Procurement Specifications'
        //     },

        //     {
        //         "specification_filled": false,
        //         "checklist_id": 2,
        //         "event_id": 1771,
        //         "event_name": "PV-Syst",
        //         "stage_name": "ENGINEERING",
        //         "status": false,
        //         "title": 'Final Specifications'
        //     },

        //     {
        //         "specification_filled": false,
        //         "checklist_id": 2,
        //         "event_id": 1771,
        //         "event_name": "PV-Syst",
        //         "stage_name": "ENGINEERING",
        //         "status": false,
        //         "title": 'Specifications of Revisions that are needed – Required: A way to specify the drawings that need revisions and specifying the revisions needed'
        //     },

        //     {
        //         "specification_filled": false,
        //         "checklist_id": 2,
        //         "event_id": 1771,
        //         "event_name": "PV-Syst",
        //         "stage_name": "ENGINEERING",
        //         "status": false,
        //         "title": 'Procurement Plan'
        //     },

        //     {
        //         "specification_filled": false,
        //         "checklist_id": 2,
        //         "event_id": 1771,
        //         "event_name": "PV-Syst",
        //         "stage_name": "ENGINEERING",
        //         "status": false,
        //         "title": 'Inspection Agency Details: To Enter Name, Contact person, Number, Email'
        //     },
        //     {
        //         "specification_filled": false,
        //         "checklist_id": 2,
        //         "event_id": 1771,
        //         "event_name": "PV-Syst",
        //         "stage_name": "ENGINEERING",
        //         "status": false,
        //         "title": 'OEM is good'
        //     },
        // ]

        if (temp.length != 0) {
            navigation.navigate('dailyReports4', {
                'specificationsData': temp,
                'title': title,
                'project_id': project_id,
                'completedList': selectedCompletedTaskList,
                'onGoingList': onGoingList,
                'tomorrowTasksList': selectedTomorrowTaskList
            })
        } else {
            try {
                const dailyUpdate = {
                    projectId: project_id,
                    todayCompletedTasks: selectedCompletedTaskList,
                    onGoingTasks: onGoingList,
                    tomorrowTasks: selectedTomorrowTaskList,
                };
                navigation.navigate('linkStageEventDocument', {
                    title: title,
                    project_id: project_id,
                    dailyUpdate: dailyUpdate
                })
            } catch (err) {
                //(err, 'wrong in post daily update api...')
            }
        }
    }

    const MemberTasksView = (props) => {
        const data = props.data
        return (
            <View>
                <View style={{ marginLeft: wp('5%'), width: wp('55%'), height: hp('6%'), alignItems: 'flex-start', paddingHorizontal: wp('4%'), justifyContent: 'center', backgroundColor: '#f1f1f1', borderRadius: 5 }}>
                    <Text style={{ color: 'rgb(143, 143, 143)', fontSize: hp('2%'), fontFamily: 'SourceSansPro-Regular' }}>Your tasks for tomorrow</Text>
                </View>

                <View style={{ marginLeft: wp('5%'), marginTop: hp('3%'), width: wp('90%'), borderWidth: 1, borderColor: 'rgb(232, 232, 232)', borderRadius: 5 }}>
                    <View style={{ height: hp('7%'), alignItems: 'flex-start', paddingLeft: '5%', justifyContent: 'center', backgroundColor: 'rgb(241, 241, 241)', borderRadius: 5 }}>
                        <Text style={{ color: '#3d87f1', fontSize: hp('1.9%'), fontFamily: 'SourceSansPro-Regular' }}>Do you wish to remove any tasks for tomorrow?</Text>
                    </View>
                    <View style={{ height: hp('26%') }}>
                        <FlatList
                            data={selectedTomorrowTaskList}
                            keyExtractor={(item, index) => index}
                            ListEmptyComponent={
                                <View style={{ height: 150, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={styles.emptyText}>No tasks to display.</Text>
                                </View>
                            }
                            renderItem={
                                ({ item, index }) => (
                                    <View style={{ padding: '5%' }}>
                                        <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                            <Text style={{ width: wp('50%'), color: 'rgb(143, 143, 143)', fontFamily: 'SourceSansPro-Regular' }}>{item}</Text>
                                            <TouchableOpacity
                                                onPress={() => { onPressRemoveTask(item, index) }}
                                                style={{ borderWidth: 0 }}
                                            >
                                                < FastImage
                                                    style={{ width: wp('6%'), height: hp('3%') }}
                                                    source={require('../../assets/icons/removetask.png')
                                                    }
                                                    resizeMode={FastImage.resizeMode.contain}
                                                />
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                )
                            }
                        />
                    </View>
                </View>
            </View>
        );
    }

    const Buttons = (params) => {
        return (
            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginLeft: wp('5%') }}>
                <TouchableOpacity
                    onPress={() => navigation.goBack()}
                    style={[styles.btnStartNewProject, { backgroundColor: 'white', borderWidth: 1, borderColor: '#3d87f1' }]}
                >
                    <Text style={[styles.btnText, { color: '#3d87f1' }]}>Prev</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => onPressNext()}
                    style={[styles.btnStartNewProject]}
                >
                    <Text style={[styles.btnText]}>Next</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => onPressNext()}
                    style={[styles.btnStartNewProject, { backgroundColor: 'white' }]}
                >
                    <Text style={[styles.btnText, { color: '#3d87f1' }]}>Skip</Text>
                </TouchableOpacity>
            </View>
        );
    }

    const MessagesListView = (props) => {
        const data = props.data
        return (
            <View style={{ alignItems: 'flex-end', marginHorizontal: wp('5%') }}>
                {
                    data.map(
                        (i, index) => {
                            return (
                                <View key={index} style={{ backgroundColor: '#3d87f1', borderRadius: 5, padding: '2%', marginVertical: hp('1%') }}>
                                    <Text style={{ color: 'white' }}>{i}</Text>
                                </View>
                            );
                        }
                    )
                }
            </View>
        );
    }

    // Dialog Component
    const [visible, setVisible] = React.useState(false);
    const updateVisibleState = (params) => {
        setVisible(false)
    }

    const renderDailyReport3 = () => {
        if (getMemberTasksReducer.success.ok) {
            let memberTasks = [];
            var tomorrowTaskList = getMemberTasksReducer.success.data.tasks;
            let unfilteredTomorrowTasks = tomorrowTaskList.map((task) => { return task.title });
            memberTasks = unfilteredTomorrowTasks.filter((task) => {if (!completedList.includes(task)) {
                return task
            }})
            var flatListData = [
                {
                    'type': 'Spacer',
                    'data': ''
                },
                {
                    'type': 'MemberTasksCard',
                    'data': memberTasks
                },
                {
                    'type': 'MessagesList',
                    'data': messagesList
                },
                {
                    'type': 'Buttons',
                    'data': []
                },

            ]

            return (
                <View style={[styles.container]}>

                    {
                        visible && <DialogComponent
                            visible={visible}
                            updateState={updateVisibleState}
                            navigation={navigation}
                        />
                    }

                    <Header title={title} updateState={updateVisibleState} />

                    <KeyboardAvoidingView keyboardVerticalOffset={10} behavior='position'>
                        <View style={{ height: Dimensions.get('window').height * 0.8 }}>
                            <FlatList
                                data={flatListData}
                                keyExtractor={(item, index) => index}
                                ref={flatlistRef}
                                renderItem={
                                    ({ item, index }) => {
                                        if (item.type === 'MemberTasksCard') {
                                            return (<MemberTasksView data={item.data} />);
                                        } else if (item.type === 'MessagesList') {
                                            return (<MessagesListView data={item.data} />);
                                        } else if (item.type === 'Spacer') {
                                            return (<View style={{ height: hp('30%') }}></View>);
                                        } else if (item.type === 'Buttons') {
                                            return (<Buttons />);
                                        }
                                    }
                                }
                            />
                        </View>

                        <Input
                            placeholder='Type here...'
                            onChangeText={text => onPressChangeText(text)}
                            inputStyle={{ fontSize: hp('2%'), width: wp('45%'), color: 'rgb(68, 67, 67)' }}
                            value={inputText}
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                            containerStyle={{ borderWidth: 0.2, marginVertical: hp('2%'), height: hp('7%'), borderColor: 'rgb(143, 143, 143)', borderRadius: 2 }}
                            rightIcon={(params) =>
                                <Icon
                                    color={'#3d87f1'}
                                    onPress={() => onPressSend()}
                                    type='ionicon'
                                    name='send-outline'
                                />
                            }
                        />
                    </KeyboardAvoidingView>
                </View >
            )
        } else if (getMemberTasksReducer.failure.error) {
            return notifyMessage(getMemberTasksReducer.failure.message);
        } else {
            return (<EmptyLoader />)
        }
    };

    return renderDailyReport3();
};

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: "#ffffff",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
    containerHeader: {
        width: Dimensions.get('window').width * 0.9,
        height: Dimensions.get('window').height,
        flex: 0.2,
        flexDirection: 'row',
        borderBottomWidth: 3,
        borderBottomColor: 'rgb(232,232,232)',
        justifyContent: 'space-between',

    },

    titleText: {
        fontSize: hp('3.15%'),
        color: 'rgb(68, 67, 67)',
        fontFamily: 'SourceSansPro-SemiBold',
    },

    cancelText: {
        fontSize: hp('2.4%'),
        color: '#3d87f1',
        fontFamily: 'SourceSansPro-Regular',
    },

    images: {
        alignSelf: 'center',
    },

    containerBody: {
        flex: 2,
        display: 'flex',
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        justifyContent: 'flex-end',
        // paddingHorizontal: '5%'
    },

    btnStartNewProject: {
        marginTop: hp("2%"),
        width: wp("17%"),
        height: hp("4%"),
        backgroundColor: "#3d87f1",
        borderRadius: 4,
        marginHorizontal: wp('1%'),
        alignItems: "center",
        justifyContent: "center",
    },
    btnText: {
        color: "white",
        fontSize: hp("2.2%"),
        textAlign: "center",
        fontFamily: "SourceSansPro-Regular",
    },

    emptyText: {
        fontSize: hp('2.4%'),
        color: 'rgb(179, 179, 179)',
        fontFamily: 'SourceSansPro-Regular',
    },

});

export default DailyReport3;

