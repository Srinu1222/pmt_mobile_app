import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TouchableOpacity,
    TextInput,
    KeyboardAvoidingView,
    ScrollView,
    Animated
} from 'react-native';
import FastImage from 'react-native-fast-image';
import LottieView from 'lottie-react-native';
import { useDispatch, useSelector } from "react-redux";
import React, { useState, useMemo, useEffect } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import notifyMessage from '../../ToastMessages';
import { FlatList } from 'react-native-gesture-handler';
import Item from '../DailyReports/DailyReportItem';
import EmptyDailyReport from '../EmptyScreens/DailyReport'
import { FAB, Icon, BottomSheet, ButtonGroup } from 'react-native-elements';
import { Card } from 'react-native-paper';
import {
    useCollapsibleSubHeader,
    CollapsibleSubHeaderAnimator,
} from 'react-navigation-collapsible';


const GetDailyReport = (props) => {

    const {
        onScroll /* Event handler */,
        containerPaddingTop /* number */,
        scrollIndicatorInsetTop /* number */,
        translateY,
    } = useCollapsibleSubHeader();

    const data = props.dailyReport

    var pending_list = []
    var updated_list = []

    for (var i = 0; i < data.length; i++) {
        if (data[i].update_required) {
            pending_list.push(data[i])
        } else {
            updated_list.push(data[i])
        }
    }

    const buttons = ['Pending Updates', 'Updated']
    const [selectedIndex, setselectedIndex] = React.useState(0);

    return (
        <View style={{ backgroundColor: '#ffffff' }}>
            <Card.Title
                style={{ backgroundColor: 'white', elevation: 5, paddingRight: wp('3%') }}
                title="Daily Report"
                titleStyle={styles.titleText}
                right={(params) =>
                    <Icon
                        color={'rgb(68, 67, 67)'}
                        onPress={() => props.navigation.navigate('dashboard')}
                        type='feather'
                        name='x'
                    />
                }
            />

            <View style={{ marginHorizontal: wp('8%'), borderWidth: 0 }}>
                <ButtonGroup
                    onPress={(index) => setselectedIndex(index)}
                    buttons={buttons}
                    containerStyle={{ height: hp('5.3%'), marginTop: hp('3%') }}
                    selectedIndex={selectedIndex}
                    buttonContainerStyle={{ backgroundColor: 'white' }}
                    selectedButtonStyle={{ backgroundColor: '#3d87f1', }}
                    textStyle={{
                        fontSize: hp('2%'),
                        fontFamily: 'SourceSansPro-Regular',
                        color: 'rgb(143, 143, 143)'
                    }}
                />
            </View>

            <View style={{ height: hp('100%')}}>
                <Animated.FlatList
                    backgroundColor='#ffffff'
                    data={selectedIndex == 0 ? pending_list : updated_list}
                    ListEmptyComponent={<EmptyDailyReport navigation={props.navigation}/>}
                    showsVerticalScrollIndicator
                    keyExtractor={(item, index) => index}
                    renderItem={({ item, index }) =>
                        <Item {...item} {...props} is_pending={selectedIndex == 0 ? true : false} />
                    }
                    onScroll={onScroll}
                    contentContainerStyle={{ paddingTop: containerPaddingTop, paddingLeft: hp('2%'), paddingRight: hp('2%'), paddingBottom: hp('10%') }}
                    scrollIndicatorInsets={{ top: scrollIndicatorInsetTop }}
                />
            </View>
        </View>
    );
}


const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        display: 'flex',
        alignItems: 'center',
        backgroundColor: "white",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
    containerHeader: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        flex: 0.2,
        flexDirection: 'row',
        borderBottomWidth: 3,
        borderBottomColor: 'rgb(232,232,232)',
        width: wp('90%'),
        justifyContent: 'space-between'
    },

    titleText: {
        fontSize: hp('3.15%'),
        color: 'rgb(68, 67, 67)',
        fontFamily: 'SourceSansPro-SemiBold',
    },

    image_logo: {
        width: wp('4%'),
        height: hp('2%'),
    },

    images: {
        alignSelf: 'center'
    },

    containerBody: {
        flex: 2,
        display: 'flex',
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        paddingHorizontal: '5%'
    },

    btnText: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-Regular'
    },

    btn: {
        borderWidth: 1,
        borderColor: 'rgb(232, 232, 232)',
        width: wp('40%'),
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: '2.5%'
    }

});

export default GetDailyReport;
