import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TouchableOpacity,
    TextInput,
    KeyboardAvoidingView,
    ScrollView,
    Animated,
    Pressable
} from 'react-native';
import FastImage from 'react-native-fast-image';
import LottieView from 'lottie-react-native';
import { useDispatch, useSelector } from "react-redux";
import React, { useState, useRef, useMemo, useEffect } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import notifyMessage from '../../ToastMessages';
import { FlatList } from 'react-native-gesture-handler';
import * as actionCreators from "../../actions/daily-updates-action/index";
import Card from '../card'
import EmptyLoader from '../EmptyLoader'
import { FAB, Icon, BottomSheet, ButtonGroup, Input } from 'react-native-elements';
import { Card as PaperCard, Divider, RadioButton, Button, Portal, Dialog } from 'react-native-paper';
import { Keyboard } from 'react-native';
import DocumentPicker from 'react-native-document-picker';


const DocumentBody = (params) => {

    const {
        uploadList,
        uploadSetMethod,
        setisFilesUploadPage,
        isFilesUploadPage,
        navigation,
        project_id,
        token,
        completedList,
        onGoingList,
        tomorrowTasksList,
        uploadDocumentsList,
        uploadPicturesList
    } = params

    const [fileName, setfileName] = useState('');
    const dispatch = useDispatch();

    const onPressUploadDocument = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.allFiles],
            });

            // Update Documents List
            let dict = {
                file_name: fileName,
                file_url: res.uri
            }
            uploadSetMethod([...uploadList, dict]);
            setfileName('')
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
            } else {
                throw err;
            }
        }
    }

    const onPressNext = () => {
        if (isFilesUploadPage) {
            setisFilesUploadPage(false)
        } else {
            const dailyUpdate = {
                projectId: project_id,
                todayCompletedTasks: completedList,
                onGoingTasks: onGoingList,
                tomorrowTasks: tomorrowTasksList,
                documentsList: uploadDocumentsList,
                PicturesList: uploadPicturesList
            };
            dispatch(actionCreators.postDailyUpdate(token, dailyUpdate));
            navigation.navigate('reportSubmit')
        }
    }

    return (
        <View>
            <View style={{ marginLeft: wp('5%'), width: wp('70%'), height: hp('6%'), alignItems: 'flex-start', paddingHorizontal: wp('4%'), justifyContent: 'center', backgroundColor: '#f1f1f1', borderRadius: 5 }}>
                <Text style={{ color: 'rgb(143, 143, 143)', fontSize: hp('2%'), fontFamily: 'SourceSansPro-Regular' }}>
                    {isFilesUploadPage ? 'Please Upload Required Documents' : 'Please share picture of work done.'}
                </Text>
            </View>
            <Input
                placeholder={'Enter File Name'}
                onChangeText={(text) => setfileName(text)}
                inputStyle={{ fontSize: hp('2.2%'), width: wp('90%'), color: 'rgb(68, 67, 67)' }}
                value={fileName}
                inputContainerStyle={{ borderBottomWidth: 0 }}
                containerStyle={{ width: wp('80%'), marginVertical: hp('2%'), marginLeft: wp('5%'), backgroundColor: 'rgba(179,179, 179, 0.1)', height: hp('7%'), borderRadius: 4 }}
            />
            <TouchableOpacity
                onPress={() => onPressUploadDocument()}
            >
                <View style={{ flexDirection: 'row', height: hp('7%'), alignItems: 'center', paddingHorizontal: '4%', borderRadius: 4, backgroundColor: 'rgb(236, 243, 254)', width: wp('80%'), marginLeft: wp('5%'), justifyContent: 'space-between' }}>
                    <Text style={styles.titleText}>
                        {isFilesUploadPage ? 'Upload Document' : 'Upload Picture'}
                    </Text>
                    <Icon
                        color={'#3d87f1'}
                        type='feather'
                        name={isFilesUploadPage ? 'file-plus' : 'camera'}
                    />
                </View>
            </TouchableOpacity>

            {
                uploadList.length != 0 &&
                <Divider style={{ backgroundColor: 'rgb(232,232,232)', marginHorizontal: hp('2.5%'), marginVertical: hp('2%'), height: hp('0.3%') }} />
            }

            {
                uploadList.map((i, index) => {
                    return (
                        <View key={index} style={{ flexDirection: 'row', height: hp('7%'), marginVertical: hp('1%'), alignItems: 'center', paddingHorizontal: '4%', borderRadius: 4, backgroundColor: 'rgb(236, 243, 254)', width: wp('60%'), marginLeft: wp('5%'), justifyContent: 'space-between' }}>
                            <Text style={styles.titleText}>{i.file_name}</Text>
                            <Icon
                                color={'#F1696A'}
                                type='feather'
                                name='x'
                                onPress={() => {
                                    let temp = [...uploadList]
                                    temp.splice(index, 1)
                                    uploadSetMethod([...temp])
                                }}
                            />
                        </View>
                    );
                })
            }

            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginLeft: wp('5%') }}>
                <TouchableOpacity
                    onPress={() => isFilesUploadPage ? navigation.goBack() : setisFilesUploadPage(true)}
                    style={[styles.btnStartNewProject, { backgroundColor: 'white', borderWidth: 1, borderColor: '#3d87f1' }]}
                >
                    <Text style={[styles.btnText, { color: '#3d87f1' }]}>Prev</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => onPressNext()}
                    style={[styles.btnStartNewProject]}
                >
                    <Text style={[styles.btnText]}>Next</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => onPressNext()}
                    style={[styles.btnStartNewProject, { backgroundColor: 'white' }]}
                >
                    <Text style={[styles.btnText, { color: '#3d87f1' }]}>Skip</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
}


const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: "#ffffff",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },
    containerHeader: {
        width: Dimensions.get('window').width * 0.9,
        height: Dimensions.get('window').height,
        flex: 0.2,
        flexDirection: 'row',
        borderBottomWidth: 3,
        borderBottomColor: 'rgb(232,232,232)',
        justifyContent: 'space-between',

    },

    titleText: {
        fontSize: hp('2.2%'),
        color: '#3d87f1',
        fontFamily: 'SourceSansPro-SemiBold',
    },

    cancelText: {
        fontSize: hp('2.4%'),
        color: '#3d87f1',
        fontFamily: 'SourceSansPro-Regular',
    },

    images: {
        alignSelf: 'center',
    },

    containerBody: {
        flex: 2,
        display: 'flex',
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        justifyContent: 'flex-end',
        // paddingHorizontal: '5%'
    },

    btnStartNewProject: {
        marginTop: hp("2%"),
        width: wp("17%"),
        height: hp("4%"),
        backgroundColor: "#3d87f1",
        borderRadius: 4,
        marginHorizontal: wp('1%'),
        alignItems: "center",
        justifyContent: "center",
    },
    btnText: {
        color: "white",
        fontSize: hp("2.2%"),
        textAlign: "center",
        fontFamily: "SourceSansPro-Regular",
    },

});


export default DocumentBody;