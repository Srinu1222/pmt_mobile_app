import React, {useEffect, useState, useRef} from 'react';
import {
  View,
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Dimensions,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  Keyboard,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useDispatch, useSelector} from 'react-redux';
import * as actionCreator from '../actions/auth-action/index';
import * as Animatable from 'react-native-animatable';
import Feather from 'react-native-vector-icons/Feather';
import {AuthContext} from '../context';
import notifyMessage from '../ToastMessages';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image';

const Login = ({navigation}) => {
  const [data, setData] = React.useState({
    email: '',
    password: '',
    token: '',
    check_textInputChange: false,
    secureTextEntry: true,
    isValidUser: true,
    isValidPassword: true,
    foundUser: false,
    first_run_on_use_effect: false,
  });

  const dispatch = useDispatch();

  const {signIn} = React.useContext(AuthContext);

  const loginReducer = useSelector(state => {
    return state.loginReducers.login.post;
  });

  const textInputChange = val => {
    if (val.trim().length >= 4) {
      setData({
        ...data,
        email: val,
        check_textInputChange: true,
        isValidUser: true,
      });
    } else {
      setData({
        ...data,
        email: val,
        check_textInputChange: false,
        isValidUser: false,
      });
    }
  };

  const handlePasswordChange = val => {
    if (val.trim().length >= 8) {
      setData({
        ...data,
        password: val,
        isValidPassword: true,
      });
    } else {
      setData({
        ...data,
        password: val,
        isValidPassword: false,
      });
    }
  };

  const updateSecureTextEntry = () => {
    setData({
      ...data,
      secureTextEntry: !data.secureTextEntry,
    });
  };

  const handleValidUser = val => {
    const isValid = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
      val,
    );
    if (isValid) {
      setData({
        ...data,
        isValidUser: true,
      });
    } else {
      setData({
        ...data,
        isValidUser: false,
      });
    }
  };

  const login = async() => {
    const formData = new FormData();
    formData.append('username', data.email);
    formData.append('password', data.password);
    await AsyncStorage.setItem('loginDetails',JSON.stringify(data) )

    dispatch(actionCreator.loginUser(formData));
  };

  const scrollViewRef = useRef();

  useEffect(() => {
    if (loginReducer.success.ok) {
      signIn(loginReducer.success.data.key);
    }
  }, [loginReducer]);

  return (
    <>
      <KeyboardAvoidingView>
        <ScrollView
        keyboardShouldPersistTaps={'always'}
        ref={scrollViewRef}
        onContentSizeChange={() => {
          scrollViewRef?.current?.scrollToEnd({animated: true});
        }}
        >
          <View style={styles.container}>
            <View style={styles.parent1}>
              <FastImage
                style={styles.logo}
                source={require('../assets/icons/SafEarth_Logo_Blue.png')}
                resizeMode={FastImage.resizeMode.contain}
              />
              <Text style={styles.title}>
                Login to your{' '}
                <Text
                  style={{
                    fontWeight: 'bold',
                    fontFamily: 'SourceSansPro-Bold',
                  }}>
                  PMT
                </Text>{' '}
                account
              </Text>
            </View>

            <View style={styles.parent2}>
              <TextInput
                placeholder="Email"
                placeholderTextColor="white"
                style={styles.textInput}
                autoCapitalize="none"
                onChangeText={val => textInputChange(val)}
                onEndEditing={e => handleValidUser(e.nativeEvent.text)}
                color={'#fff'}
                selectionColor={'#fff'}
              />
              {data.isValidUser ? null : (
                <Animatable.View animation="fadeInLeft" duration={500}>
                  <Text style={styles.errorMsg}>Enter a Valid email.</Text>
                </Animatable.View>
              )}
              <TextInput
                placeholder="Password"
                placeholderTextColor="white"
                secureTextEntry={data.secureTextEntry ? true : false}
                style={styles.textInput}
                autoCapitalize="none"
                onChangeText={val => handlePasswordChange(val)}
                selectionColor={'#fff'}
              />

              <TouchableOpacity
                style={styles.eyeSymbol}
                onPress={updateSecureTextEntry}>
                {data.secureTextEntry ? (
                  <Feather name="eye-off" color="white" size={18} />
                ) : (
                  <Feather name="eye" color="white" size={18} />
                )}
              </TouchableOpacity>

              {data.isValidPassword ? null : (
                <Animatable.View animation="fadeInLeft" duration={500}>
                  <Text style={styles.errorMsgForPassword}>
                    Password must be 8 characters long.
                  </Text>
                </Animatable.View>
              )}

              <TouchableOpacity style={styles.signIn} onPress={() => login()}>
                <Text
                  style={[
                    styles.textSign,
                    {
                      color: '#3A80E3',
                    },
                  ]}>
                  Sign In
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => {
                  navigation.push('Signup');
                }}
                style={{
                  top: hp('60%'),
                  position: 'absolute',
                  justifyContent: 'center',
                  alignContent: 'center',
                  alignSelf: 'center',
                }}>
                <Text
                  style={{
                    color: 'white',
                    fontSize: 14,
                    lineHeight: 18,
                    textAlign: 'center',
                    fontFamily: 'SourceSansPro-Regular',
                  }}>
                  Don't have an account?{' '}
                  <Text style={{textDecorationLine: 'underline'}}>Sign up</Text>
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    display: 'flex',
    flex: 1,
    // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
  parent1: {
    flex: 1,
    alignSelf: 'auto',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },

  parent2: {
    flex: 2,
    backgroundColor: '#3A80E3',
  },

  logo: {
    marginBottom: 70,
    marginVertical: 30,
    width: wp('38.9%'),
    height: hp('10.2%'),
  },

  title: {
    fontSize: 20,
    lineHeight: hp('3.3%'),
    color: '#444343',
    fontFamily: 'SourceSansPro-Regular',
  },

  textInput: {
    width: wp('79.4%'),
    left: 30,
    top: 60,
    height: hp('8%'),
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: 'white',
    marginBottom: 30,
    backgroundColor: 'rgba(100,160,241,0.5)',
    borderRadius: 5,
    fontFamily: 'SourceSansPro-Regular',
  },

  signIn: {
    width: wp('79.4%'),
    height: hp('8%'),
    left: 30,
    top: 60,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#05375a',
    backgroundColor: 'white',
    borderRadius: 5,
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: 'SourceSansPro-Regular',
  },

  textSign: {
    width: wp('16.1%'),
    fontSize: 18,
    lineHeight: 20,
    fontFamily: 'SourceSansPro-Regular',
  },

  errorMsg: {
    color: '#FF0000',
    fontSize: 14,
    marginTop: hp('4%'),
    left: 30,
    fontFamily: 'SourceSansPro-Regular',
  },

  errorMsgForPassword: {
    color: '#FF0000',
    fontSize: 14,
    marginTop: 5,
    left: 30,
    fontFamily: 'SourceSansPro-Regular',
  },

  eyeSymbol: {
    padding: 5,
    left: wp('75%'),
    bottom: 13,
  },
});

export default Login;
