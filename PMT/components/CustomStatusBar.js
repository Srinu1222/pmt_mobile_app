import React from 'react'
import { View, Text, Platform, StatusBar, StyleSheet,NavigationState  } from 'react-native'
import { useNavigationState,useNavigation  } from '@react-navigation/native'    




const CustomStatusBar = ({backgroundColor, ...props}) => {

    // const routes = useNavigationState(state => state.routes)
    // const currentRoute = routes[routes.length -1].name;
    
    //   //('CustomStatusBar currentRoute',currentRoute)
    return (
        <View 
            style={[styles.statusBar, {backgroundColor}]}
        >
            <StatusBar 
                style={[styles.statusBar, {backgroundColor}]}

                translucent
                backgroundColor={backgroundColor}
                {...props}
                barStyle="light-content"
             />
</View>
    )
}

const styles = StyleSheet.create({
    statusBar: {
        height: Platform.OS==="android"? StatusBar.currentHeight: 40
    }
})

export default CustomStatusBar
