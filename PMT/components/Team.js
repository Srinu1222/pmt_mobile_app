import { View, Text } from 'react-native'
import React, { useEffect, useState } from "react";
import * as actionCreator from '../actions/team-action/index';
import { useDispatch, useSelector } from "react-redux";
import GetTeams from '../components/teams/GetTeams'
import notifyMessage from '../ToastMessages';
import { parseSync } from '@babel/core';
import { heightPercentageToDP } from 'react-native-responsive-screen';
import EmptyInventory from '../components/EmptyScreens/EmptyInventory'
import EmptyLoader from '../components/EmptyLoader'


const Team = (props) => {

    const dispatch = useDispatch();

    const token = props.route.params.token

    const [reload, setReload] = useState(false);
    const changeReload = (params) => {
      setReload(!reload)
    }
    
    useEffect(() => {
        dispatch(actionCreator.getTeams(token, true));
    }, [reload]);

    const teamReducer = useSelector((state) => {
        return state.teamReducers.teams.get;
    });

    const renderTeam = () => {
        if (teamReducer.success.ok) {
            let data = teamReducer.success.data.team_members_list

            // FILTER DATA TO DISPLAY DEPARTMENT
            const departmentList = []
            data.map(
                (item, index) => {
                    if (!departmentList.includes(item.department)) {
                        departmentList.push(item.department)
                    }
                }
            )

            // FILTERING LOGIC
            const Departmentfilter = (item) => {
                return props.route.params?.selectedDepartmentList.includes(item.department);
            }

            if (props.route.params?.isFilter && props.route.params?.selectedDepartmentList.length > 0) {
                data = data.filter(Departmentfilter)
            }

            return <GetTeams style={{ backgroundColor: '#ffffff' }} changeReload={changeReload} departmentList={departmentList} navigation={props.navigation} team={data} />;
        } else if (teamReducer.failure.error) {
            return notifyMessage(teamReducer.failure.message);
        } else {
            return (
                < EmptyLoader />
            )
        }
    };
    return renderTeam();
};

export default Team;
