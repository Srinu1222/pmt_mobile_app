import React, { Component } from 'react';
import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
} from 'react-native';
import LottieView from 'lottie-react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';


export default class SideEmptyLoader extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (

                <View style={[styles.container, {marginLeft: this.props.portrait ? wp('15%') : wp('25%'), marginTop: this.props.portrait ? 0 : wp('-15%')}]}>
                    <LottieView
                        style={{ height: 180, width: 180 }}
                        source={require('../data.json')}
                        autoPlay
                        loop={false}
                    />
                </View>


        );
    }
}

const styles = StyleSheet.create({
    container: {
        // width: wp('40%'),
        // height: wp('10%'),
        display: 'flex',
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },

    text: {
        color: 'red',
        fontSize: 25,
    },
});