import React, { useRef, useEffect } from 'react';
import {
    View,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Text,
    TextInput,
    PermissionsAndroid,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
} from 'react-native'; import { Searchbar, Button } from 'react-native-paper';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import EmptyScreenHeader from './EmptyScreens/EmptyScreenHeader';
import { useDispatch, useSelector } from 'react-redux';
import { SearchBar } from 'react-native-elements';
import { FlatList } from 'react-native-gesture-handler';
import ActionSheet from 'react-native-actionsheet';
import Pagination from './Pagination';
import FastImage from 'react-native-fast-image';
import * as actionCreator from './../actions/project-docs-action/index';
import RNFetchBlob from 'rn-fetch-blob';
import { sortBy } from 'lodash';
import EmptyLoader from './EmptyLoader';
import ProjlistHelper from './Documents/ProjlistHelper';
import ProjectDocs from './ProjectDocuments/ProjectDocs';
import EmptyDocs from './ProjectDocuments/EmptyDocs';


 
const ProjectDocuments = (props) => {
    const { token,project_id } = props.route.params;
    const dispatch = useDispatch();
    const [searchQuery, setSearchQuery] = React.useState('');
    const [docslist, setdocslist] = React.useState('');
    const onChangeSearch = query => setSearchQuery(query);
    const [activeButton, setactiveButton] = React.useState(1);
    const [profileCreatedFor, handleProfileCreatedFor] = React.useState('');
    //  //"docsReducers.success.data.projects",docsReducers.success.data.projects)
    const projectDocsReducer = useSelector((state) => {
        return state.projectDocsReducer.projectDocs.get;
    });
    var ownerdata = [];
    for(var i in projectDocsReducer?.success?.data?.project_docs)
    if(ownerdata.indexOf(projectDocsReducer?.success?.data?.project_docs [i].owner)===-1){
        ownerdata.push(projectDocsReducer?.success?.data?.project_docs [i].owner);

    }

    useEffect(() => {
        dispatch(actionCreator.getProjectDocs(project_id, token));
    }, [project_id]);
   
  
 


    if (projectDocsReducer?.success?.ok) {
        if(projectDocsReducer?.success?.data?.project_docs?.length && projectDocsReducer?.success?.data?.project_docs?.length != 0){
            return (
                <ProjectDocs docslist1={projectDocsReducer?.success?.data?.project_docs} ownerdata={ownerdata} {...props} />
             );
        }else{
            return <EmptyDocs {...props}/>;

        }
    
                        }else {
        return <EmptyLoader />;
      }
};
const styles = StyleSheet.create({
    name_text: {
        fontSize: hp('2.5%'),
        fontFamily: 'SourceSansPro-SemiBold',
        color: 'rgb(68, 67, 67)',
    },

    email_text: {
        fontSize: hp('1.8%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
    },

    department_text: {
        fontSize: hp('2%'),
        fontFamily: 'SourceSansPro-SemiBold',
        color: 'rgb(194, 194, 194)',
    },

    item_card: {
        shadowColor: 'rgba(0,0,0,0.2)',
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.2,
        shadowRadius: 4.46,
        elevation: 2,
        borderWidth: 0,
        borderRadius: 4,
        borderLeftColor: 'rgb(61, 135, 241)',
        borderLeftWidth: wp('1.3%'),
        marginTop: 16.5,
        borderTopLeftRadius: 4,
        borderBottomLeftRadius: 4,
        padding: '5%',
        paddingRight: '4%'
    },

    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
        // marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    },


    parent2: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        flex: 11,
        backgroundColor: 'white',
        padding: '5%'

    },

    buy_now_text: {
        fontSize: hp('2.3%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(61, 135, 241)'
    },

    subscription_text: {
        fontSize: hp('1.8%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(194, 194, 194)'
    },

    option_text: {
        fontSize: hp('1.2%'),
        fontFamily: 'SourceSansPro-Regular',
        color: 'rgb(68, 67, 67)',
        alignSelf: 'center'

    },

});


export default ProjectDocuments
