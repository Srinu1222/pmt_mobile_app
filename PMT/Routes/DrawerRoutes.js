import React, { useState } from "react";
import { View, StyleSheet, Image, TouchableOpacity } from "react-native";
import {
  DrawerContentScrollView,
  DrawerItem,
  DrawerItemList,
} from "@react-navigation/drawer";
import {
  Avatar,
  Title,
  Caption,
  Paragraph,
  Drawer,
  Text,
  TouchableRipple,
  Switch,
} from "react-native-paper";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import Ionicons from "react-native-vector-icons/Ionicons";
import { color } from "react-native-reanimated";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import FastImageComponent from "../components/FastImageComponent";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import EmptyScreenHeader from '../components/EmptyScreens/EmptyScreenHeader'
import config from "../config";
import { AuthContext } from "../context";
import { useDispatch, useSelector } from "react-redux";
import { FAB, Icon as RNEIcon } from 'react-native-elements';


const DrawerRoutes = (props) => {
  const [isDarkTheme, setIsDarkTheme] = useState(false);
  const [activeSection, setActiveSection] = useState(true);
  const [activeScreen, setActiveScreen] = useState("home_dashboard");
  const [activeprojectScreen, setActiveprojectScreen] = useState(false);


  const toggleTheme = () => {
    setIsDarkTheme(!isDarkTheme);
  };

  const drawerReducer = useSelector((state) => {
    return state.getProjectReducers.selected_project.data;
  });

  const handleSectionClick = (sectionName) => {
    if (sectionName === "Home") {
      setActiveSection(!activeSection);
    } else if (sectionName === "Project Dashboard") {
      setActiveSection(!activeSection);
    }
  };

  const { signOut } = React.useContext(AuthContext);


  // //
  //   props.state.index,
  //   props.state.routes[props.state.index].name,
  //   "00000000000000000000000"
  // );
  // //props.state.routes[0].params, "00000000000000")

  return (
    <View style={[styles.mainContainer]}>
      <DrawerContentScrollView {...props}>
        <View style={styles.drawerContent}>
          {/* -------------------SafEarth Logo Section---------------- */}
          <View style={styles.safearthLogoSection}>
            {/* -------------------Section-1------------------- */}
            <View style={[styles.safearthLogoContainer]}>
              <Image
                source={require("../assets/images/android/SafEarth_Logo_Drawer/safearth_logo_2.png")}
                style={[styles.safearthLogo]}
                resizeMode={"contain"}
              />
            </View>
          </View>

          {/* ------------------Drawer Navigation Menu------------------------- */}
          <View style={[styles.homeSection]}>
            <Drawer.Section>
              <TouchableOpacity
                onPress={() => handleSectionClick("Home")}
                style={[styles.homeMenuContainer]}
              >
                <Text
                  style={
                    activeSection
                      ? [styles.homeText]
                      : [styles.inactiveHomeText]
                  }
                >
                  Company Dashboard
                </Text>
                <Text
                  style={
                    activeSection
                      ? [styles.homeText]
                      : [styles.inactiveHomeText]
                  }
                >
                  {activeSection ? "-" : "+"}
                </Text>
              </TouchableOpacity>
            </Drawer.Section>
            <Drawer.Section
              {...props}
              style={activeSection ? styles.drawerSection : { display: "none" }}
            >
              <DrawerItem
                activeBackgroundColor={"#ECF3FE"}
                activeTintColor={"#3d87f1"}
                focused={activeScreen === "home_dashboard"}
                icon={({ color, size }) => (
                  <Image
                    source={
                      activeScreen === "home_dashboard"
                        ? require("../assets/images/android/active_home_icons/icon_dashboard_drawer.png")
                        : require("../assets/images/android/inactive_home_icons/icon_dashboard_drawer.png")
                    }
                    style={[styles.iconStyle]}
                  />
                )}
                label="Dashboard"
                onPress={() => {
                  setActiveScreen("home_dashboard");
                  props.navigation.navigate("dashboardStackScreen");
                }}
              />
              <DrawerItem
                activeBackgroundColor={"#ECF3FE"}
                activeTintColor={"#3d87f1"}
                focused={activeScreen === "home_all_projects"}
                icon={({ color, size }) => (
                  <Image
                    source={
                      activeScreen === "home_all_projects"
                        ? require("../assets/images/android/active_home_icons/icon_all_projects_drawer.png")
                        : require("../assets/images/android/inactive_home_icons/icon_all_projects_drawer.png")
                    }
                    style={[styles.iconStyle]}
                  />
                )}
                label="All Projects"
                onPress={() => {
                  setActiveScreen("home_all_projects");
                  props.navigation.navigate("allProjectStackScreen");
                }}
              />
              <DrawerItem
                activeBackgroundColor={"#ECF3FE"}
                activeTintColor={"#3d87f1"}
                focused={activeScreen === "home_documents"}
                icon={({ color, size }) => (
                  <Image
                    source={
                      activeScreen === "home_documents"
                        ? require("../assets/images/android/active_home_icons/icon_documents_drawer.png")
                        : require("../assets/images/android/inactive_home_icons/icon_documents_drawer.png")
                    }
                    style={[styles.iconStyle]}
                  />
                )}
                label="Documents"
                onPress={() => {
                  setActiveScreen("home_documents");
                  props.navigation.navigate("documentsStackScreen");
                }}
              />
              <DrawerItem
                activeBackgroundColor={"#ECF3FE"}
                activeTintColor={"#3d87f1"}
                focused={activeScreen === "home_photos"}
                icon={() => <RNEIcon
                  iconStyle={{ fontSize: hp('3%') }}
                  name='camera'
                  type='feather'
                  color={activeScreen === "home_photos" ? '#3d87f1' : 'rgb(68, 67, 67)'}
                />
                }
                label="Photos"
                onPress={() => {
                  setActiveScreen("home_photos");
                  props.navigation.navigate("photosSection");
                }}
              />
              <DrawerItem
                activeBackgroundColor={"#ECF3FE"}
                activeTintColor={"#3d87f1"}
                focused={activeScreen === "home_sample_gantts"}
                icon={({ color, size }) => (
                  <Image
                    source={
                      activeScreen === "home_sample_gantts"
                        ? require("../assets/images/android/active_home_icons/icon_sample_gantts_drawer.png")
                        : require("../assets/images/android/inactive_home_icons/icon_sample_gantts_drawer.png")
                    }
                    style={[styles.iconStyle]}
                  />
                )}
                label="Sample Gantts"
                onPress={() => {
                  setActiveScreen("home_sample_gantts");
                  props.navigation.navigate("sampleGantts");
                }}
              />
              <DrawerItem
                activeBackgroundColor={"#ECF3FE"}
                activeTintColor={"#3d87f1"}
                focused={activeScreen === "home_team"}
                icon={({ color, size }) => (
                  <Image
                    source={
                      activeScreen === "home_team"
                        ? require("../assets/images/android/active_home_icons/icon_team_drawer.png")
                        : require("../assets/images/android/inactive_home_icons/icon_team_drawer.png")
                    }
                    style={[styles.iconStyle]}
                  />
                )}
                label="Team"
                onPress={() => {
                  setActiveScreen("home_team");
                  props.navigation.navigate("teamStackScreen");
                }}
              />
              <DrawerItem
                activeBackgroundColor={"#ECF3FE"}
                activeTintColor={"#3d87f1"}
                focused={activeScreen === "home_sub_contractor"}
                icon={({ color, size }) => (
                  <Image
                    source={
                      activeScreen === "home_sub_contractor"
                        ? require("../assets/images/android/active_home_icons/icon_subcontractor_drawer.png")
                        : require("../assets/images/android/inactive_home_icons/icon_subcontractor_drawer.png")
                    }
                    style={[styles.iconStyle]}
                  />
                )}
                label="Sub-Contractor"
                onPress={() => {
                  setActiveScreen("home_sub_contractor");
                  props.navigation.navigate("subContractor");
                }}
              />
              <DrawerItem
                activeBackgroundColor={"#ECF3FE"}
                activeTintColor={"#3d87f1"}
                focused={activeScreen === "home_inventory"}
                icon={({ color, size }) => (
                  <Image
                    source={
                      activeScreen === "home_inventory"
                        ? require("../assets/images/android/active_home_icons/icon_inventory_drawer.png")
                        : require("../assets/images/android/inactive_home_icons/icon_inventory_drawer.png")
                    }
                    style={[styles.iconStyle]}
                  />
                )}
                label="Inventory"
                onPress={() => {
                  setActiveScreen("home_inventory");
                  props.navigation.navigate("inventory");
                }}
              />
              <DrawerItem
                activeBackgroundColor={"#ECF3FE"}
                activeTintColor={"#3d87f1"}
                focused={activeScreen === "home_account"}
                icon={({ color, size }) => (
                  <Image
                    source={
                      activeScreen === "home_account"
                        ? require("../assets/images/android/active_home_icons/icon_account_drawer.png")
                        : require("../assets/images/android/inactive_home_icons/icon_account_drawer.png")
                    }
                    style={[styles.iconStyle]}
                  />
                )}
                label="Account"
                onPress={() => {
                  setActiveScreen("home_account");
                  props.navigation.navigate("account");
                }}
              />
              <DrawerItem
                activeBackgroundColor={"#ECF3FE"}
                activeTintColor={"#3d87f1"}
                focused={activeScreen === "home_project_analytics"}
                icon={({ color, size }) => (
                  <Image
                    source={
                      activeScreen === "home_project_analytics"
                        ? require("../assets/images/android/active_home_icons/icon_project_analytics_drawer.png")
                        : require("../assets/images/android/inactive_home_icons/icon_project_analytics_drawer.png")
                    }
                    style={[styles.iconStyle]}
                  />
                )}
                label="Project Analytics"
                onPress={() => {
                  setActiveScreen("home_project_analytics");
                  props.navigation.navigate("projectAnalytics");
                }}
              />
            </Drawer.Section>

            {/* ---------------------Project Section--------------------- */}
            <Drawer.Section>
              <TouchableOpacity
                disabled={drawerReducer == null ? true : false}
                onPress={() => handleSectionClick("Project Dashboard")}
                style={[styles.homeMenuContainer]}
              >
                <View>
                  <Text
                    style={
                      !activeSection
                        ? [styles.homeText]
                        : [styles.inactiveHomeText]
                    }
                  >
                    Project Dashboard
                </Text>
                  {
                    <Text
                      style={{ fontFamily: 'SourceSansPro-Regular', fontSize: hp('2%'), color: 'rgb(143, 143, 143)' }}
                    >
                      {drawerReducer && drawerReducer.name ? drawerReducer.name : 'No Projects'}
                    </Text>
                  }
                </View>

                <Text
                  style={
                    !activeSection
                      ? [styles.homeText]
                      : [styles.inactiveHomeText]
                  }
                >
                  {!activeSection ? "-" : "+"}
                </Text>
              </TouchableOpacity>
            </Drawer.Section>
            <Drawer.Section
              {...props}
              style={
                !activeSection ? styles.drawerSection : { display: "none" }
              }
            >
              <DrawerItem
                activeBackgroundColor={"#ECF3FE"}
                activeTintColor={"#3d87f1"}
                focused={activeScreen === "project_level_dashboard"}
                icon={({ color, size }) => (
                  <Image
                    source={
                      activeScreen === "project_level_dashboard"
                        ? require("../assets/images/android/active_home_icons/icon_dashboard_drawer.png")
                        : require("../assets/images/android/inactive_home_icons/icon_dashboard_drawer.png")
                    }
                    style={[styles.iconStyle]}
                  />
                )}
                label="Dashboard"
                onPress={() => {
                  setActiveScreen("project_level_dashboard");
                  
                  props.navigation.navigate("project_dashboard", { 
                    screen: 'projectStackScreen',
                    params: 
                    {
                      project_id: drawerReducer.id,
                      is_sub_contractor: drawerReducer.is_sub_contractor,
                      token:props.state.routes[0].params.token
                      // project_completion: drawerReducer.project_completion
                    }});
                }}
              />
              <DrawerItem
                activeBackgroundColor={"#ECF3FE"}
                activeTintColor={"#3d87f1"}
                focused={activeScreen === "project_level_gantt_chart"}
                icon={({ color, size }) => (
                  <Image
                    source={
                      activeScreen === "project_level_gantt_chart"
                        ? require("../assets/images/android/active_project_dashboard_icons/icon_gantt_chart_drawer.png")
                        : require("../assets/images/android/inactive_project_dashboard_icons/icon_gantt_chart_drawer.png")
                    }
                    style={[styles.iconStyle]}
                  />
                )}
                label="Gantt Chart"
                onPress={() => {
                  setActiveprojectScreen(true)
                  setActiveScreen("project_level_gantt_chart");
                
                  props.navigation.navigate("project_dashboard", { 
                    screen: 'GanttStackScreen',
                    params: 
                    {
                      project_id: drawerReducer.id,
                      is_sub_contractor: drawerReducer.is_sub_contractor,
                      token:props.state.routes[0].params.token
                      // project_completion: drawerReducer.project_completion
                    }});

                }}
              />
              <DrawerItem
                activeBackgroundColor={"#ECF3FE"}
                activeTintColor={"#3d87f1"}
                focused={activeScreen === "project_level_finance"}
                icon={({ color, size }) => (
                  <Image
                    source={
                      activeScreen === "project_level_finance"
                        ? require("../assets/images/android/active_project_dashboard_icons/icon_finance_drawer.png")
                        : require("../assets/images/android/inactive_project_dashboard_icons/icon_finance_drawer.png")
                    }
                    style={[styles.iconStyle]}
                  />
                )}
                label="Finance (Beta)"
                onPress={() => {
                  setActiveScreen("project_level_finance");
                  props.navigation.navigate("projectFinance", { project_id: drawerReducer.id,token:props.state.routes[0].params.token });
                }}
                options={{
                  headerTitle: props => <EmptyScreenHeader navigation={navigation} title='Dashboard' {...props} />,
                  headerTitleContainerStyle: { left: 0, right: 0 }
                }}
              />
              <DrawerItem
                activeBackgroundColor={"#ECF3FE"}
                activeTintColor={"#3d87f1"}
                focused={activeScreen === "project_level_documents"}
                icon={({ color, size }) => (
                  <Image
                    source={
                      activeScreen === "project_level_documents"
                        ? require("../assets/images/android/active_home_icons/icon_documents_drawer.png")
                        : require("../assets/images/android/inactive_home_icons/icon_documents_drawer.png")
                    }
                    style={[styles.iconStyle]}
                  />
                )}
                label="Documents"
                onPress={() => {
                  setActiveScreen("project_level_documents");

                  props.navigation.navigate("project_dashboard", { 
                    screen: 'projectdocumentsStackScreen',
                    params: 
                    {
                      project_id: drawerReducer.id,
                      is_sub_contractor: drawerReducer.is_sub_contractor,
                      token:props.state.routes[0].params.token
                      // project_completion: drawerReducer.project_completion
                    }});
                }}
              />
              <DrawerItem
                activeBackgroundColor={"#ECF3FE"}
                activeTintColor={"#3d87f1"}
                focused={activeScreen === "project_level_photos"}
                icon={() => <RNEIcon
                  iconStyle={{ fontSize: hp('3%') }}
                  name='camera'
                  type='feather'
                  color={activeScreen === "project_level_photos" ? '#3d87f1' : 'rgb(68, 67, 67)'}
                />
                }
                label="Photos"
                onPress={() => {
                  setActiveScreen("project_level_photos");
                  props.navigation.navigate("projectPhotos", {
                    is_sub_contractor: drawerReducer.is_sub_contractor,
                     projectId: drawerReducer.id,
                     projectName: drawerReducer.name
                    });
                }}
              />
              <DrawerItem
                activeBackgroundColor={"#ECF3FE"}
                activeTintColor={"#3d87f1"}
                focused={activeScreen === "project_level_reports"}
                icon={({ color, size }) => (
                  <Image
                    source={
                      activeScreen === "project_level_reports"
                        ? require("../assets/images/android/active_project_dashboard_icons/icon_reports_drawer.png")
                        : require("../assets/images/android/inactive_project_dashboard_icons/icon_reports_drawer.png")
                    }
                    style={[styles.iconStyle]}
                  />
                )}
                label="Reports (Beta)"
                onPress={() => {
                  setActiveScreen("project_level_reports");
                  props.navigation.navigate(
                    "projectReports",
                    {
                      project_id: drawerReducer.id,
                      is_sub_contractor: drawerReducer.is_sub_contractor,
                      token: props.state.routes[0].params.token
                    }
                  );
                }}
              />
              <DrawerItem
                activeBackgroundColor={"#ECF3FE"}
                activeTintColor={"#3d87f1"}
                focused={activeScreen === "project_level_materials_used"}
                icon={({ color, size }) => (
                  <Image
                    source={
                      activeScreen === "project_level_materials_used"
                        ? require("../assets/images/android/active_project_dashboard_icons/icon_material_used_drawer.png")
                        : require("../assets/images/android/inactive_project_dashboard_icons/icon_material_used_drawer.png")
                    }
                    style={[styles.iconStyle]}
                  />
                )}
                label="Materials Used (Beta)"
                onPress={() => {
                  setActiveScreen("project_level_materials_used");
                  props.navigation.navigate("projectMaterialsUsed",
                    {
                      project_id: drawerReducer.id,
                      is_sub_contractor: drawerReducer.is_sub_contractor,
                      token:props.state.routes[0].params.token
                    }
                  );


                  
                }}
              />
              <DrawerItem
                activeBackgroundColor={"#ECF3FE"}
                activeTintColor={"#3d87f1"}
                focused={activeScreen === "project_level_team"}
                icon={({ color, size }) => (
                  <Image
                    source={
                      activeScreen === "project_level_team"
                        ? require("../assets/images/android/active_home_icons/icon_team_drawer.png")
                        : require("../assets/images/android/inactive_home_icons/icon_team_drawer.png")
                    }
                    style={[styles.iconStyle]}
                  />
                )}
                label="Team"
                onPress={() => {
                  setActiveScreen("project_level_team");
                  props.navigation.navigate("projectTeam", { project_id: drawerReducer.id });
                }}
              />
              <DrawerItem
                activeBackgroundColor={"#ECF3FE"}
                activeTintColor={"#3d87f1"}
                focused={activeScreen === "project_analytics"}
                icon={({ color, size }) => (
                  <Image
                    source={
                      activeScreen === "project_analytics"
                        ? require("../assets/images/android/active_home_icons/icon_project_analytics_drawer.png")
                        : require("../assets/images/android/inactive_home_icons/icon_project_analytics_drawer.png")
                    }
                    style={[styles.iconStyle]}
                  />
                )}
                label="Project Analytics"
                onPress={() => {
                  setActiveScreen("project_analytics");

                  props.navigation.navigate("project_dashboard", { 
                    screen: 'AnalyticsStackScreen',
                    params: 
                    {
                      project_id: drawerReducer.id,
                      is_sub_contractor: drawerReducer.is_sub_contractor,
                      token:props.state.routes[0].params.token
                      // project_completion: drawerReducer.project_completion
                    }});
                }}
              />
              {/* <DrawerItem
                activeBackgroundColor={"#ECF3FE"}
                activeTintColor={"#3d87f1"}
                focused={activeScreen === "project_level_change_specification"}
                icon={({ color, size }) => (
                  <Image
                    source={
                      activeScreen === "project_level_change_specification"
                        ? require("../assets/images/android/active_project_dashboard_icons/icon_change_specification_drawer.png")
                        : require("../assets/images/android/inactive_project_dashboard_icons/icon_change_specification_drawer.png")
                    }
                    style={[styles.iconStyle]}
                  />
                )}
                label="Change Specification"
                onPress={() => {
                  setActiveScreen("project_level_change_specification");
                  props.navigation.navigate("projectChangeSpecification", { project_id: drawerReducer.id });
                }}
              />
              <DrawerItem
                activeBackgroundColor={"#ECF3FE"}
                activeTintColor={"#3d87f1"}
                focused={activeScreen === "project_level_specification"}
                icon={({ color, size }) => (
                  <Image
                    source={
                      activeScreen === "project_level_specification"
                        ? require("../assets/images/android/active_project_dashboard_icons/icon_specifications_drawer.png")
                        : require("../assets/images/android/inactive_project_dashboard_icons/icon_specifications_drawer.png")
                    }
                    style={[styles.iconStyle]}
                  />
                )}
                label="Specifications"
                onPress={() => {
                  setActiveScreen("project_level_specification");
                  props.navigation.navigate("projectSpecification", { project_id: drawerReducer.id });
                }}
              /> */}
            </Drawer.Section>
          </View>

          {/* -----------------------Switch Section---------------------------- */}
          {/* <Drawer.Section title="Preferences">
            <TouchableRipple
              onPress={() => {
                toggleTheme();
              }}
            >
              <View style={styles.preference}>
                <Text>Dark Theme</Text>
                <View pointerEvents="none">
                  <Switch value={isDarkTheme} />
                </View>
              </View>
            </TouchableRipple>
          </Drawer.Section> */}
        </View>
        <View style={styles.bottomDrawerSection}>
          <DrawerItem
            icon={({ color, size }) => (
              <Image
                source={require("../assets/images/android/inactive_home_icons/icon-help_and_feedback_drawer.png")}
                style={[styles.iconStyle]}
              />
            )}
            label="Help and Feedback"
            onPress={() => {
              props.navigation.navigate("Feedback");
            }}
          />
          <DrawerItem
            icon={({ color, size }) => (
              <Image
                source={require("../assets/images/android/inactive_home_icons/icon-settings-drawer.png")}
                style={[styles.iconStyle]}
              />
            )}
            label="Settings"
            onPress={() => {
              props.navigation.navigate("settings");
            }}
          />
          <DrawerItem
            icon={({ color, size }) => (
              <Image
                source={require("../assets/images/android/inactive_home_icons/icon-logout-drawer.png")}
                style={[styles.iconStyle]}
              />
            )}
            label="Logout"
            onPress={() => signOut()}
          />
        </View>
      </DrawerContentScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    marginTop: hp("-1.5%"),
  },
  drawerContent: {
    flex: 1,
    margin: 0,
  },
  safearthLogoSection: {
    // paddingLeft: 20,
  },
  safearthLogoContainer: {
    backgroundColor: "#3d87f1",
    height: hp("10%"),
    flex: 1,
    justifyContent: "center",
    alignItems: "flex-start",
    // paddingLeft: wp("6%"),
  },
  safearthLogo: {
    // width: 80,
    width: wp("28%"),
    height: hp("5%"),
    marginLeft: wp("5%"),
  },
  homeSection: {},
  homeMenuContainer: {
    flex: 1,
    flexDirection: "row",
    width: wp("70%"),
    justifyContent: "space-between",
    marginVertical: hp("2%"),
    paddingLeft: wp("8%"),
  },
  drawerSection: {
    marginLeft: wp("2%"),
  },
  homeText: {
    fontSize: 20,
    fontWeight: "bold",
    lineHeight: 25,
    color: "rgb(61,135,241)",
    fontFamily: "SourceSansPro-Regular",
  },
  inactiveHomeText: {
    fontSize: 20,
    fontWeight: "bold",
    lineHeight: 25,
    color: "rgb(68, 67, 67)",
    fontFamily: "SourceSansPro-Regular",
  },
  iconStyle: {
    width: wp("6.5%"),
    height: wp("6.5%"),
  },

  // title: {
  //   fontSize: 16,
  //   marginTop: 3,
  //   fontWeight: "bold",
  // },
  // caption: {
  //   fontSize: 14,
  //   lineHeight: 14,
  // },
  // row: {
  //   marginTop: 20,
  //   flexDirection: "row",
  //   alignItems: "center",
  // },
  // section: {
  //   flexDirection: "row",
  //   alignItems: "center",
  //   marginRight: 15,
  // },
  // paragraph: {
  //   fontWeight: "bold",
  //   marginRight: 3,
  // },
  bottomDrawerSection: {
    marginBottom: 15,
    marginLeft: wp("2%"),
    // borderTopColor: "#f4f4f4",
    // borderTopWidth: 1,
  },
  // preference: {
  //   flexDirection: "row",
  //   justifyContent: "space-between",
  //   paddingVertical: 12,
  //   paddingHorizontal: 16,
  // },
});

export default DrawerRoutes;
