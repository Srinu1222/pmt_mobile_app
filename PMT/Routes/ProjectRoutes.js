import React from "react";
import { Image } from 'react-native'
import { createStackNavigator } from "@react-navigation/stack";
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import Dashboard from '../components/Dashboard'
import EmptyScreenHeader from '../components/EmptyScreens/EmptyScreenHeader'
import { TransitionPresets } from '@react-navigation/stack';

import ProjectDashboard from '../components/ProjectDashboard';;

import ProjectGanttChart from "../components/ProjectGanttChart";
import GanttChartcomponent from "../components/GanttChart/GanttChartcomponent";
import ProjectDocuments from '../components/ProjectDocuments';
import ProjectPhotos from '../components/ProjectPhotos';
import ProjectAnalytics from '../components/ProjectAnalytics';
import StageScreen1 from '../components/DetailedEventPortraitView/StageScreen1';

const Tab = createMaterialBottomTabNavigator();

const DashboardStack = createStackNavigator();
const GanttStack = createStackNavigator();
const DocumentsStack = createStackNavigator();
const ProjectPhotosStack = createStackNavigator();

const AnalyticsStack = createStackNavigator();

const DailyReportStack = createStackNavigator();
const TeamStack = createStackNavigator();
const DailyReportsStack = createStackNavigator();


const DashboardStackScreen = ({ navigation, route }) => {
  return (
    <DashboardStack.Navigator
      screenOptions={{
        headerShown: false,
        headerMode: 'screen',
        ...TransitionPresets.ModalSlideFromBottomIOS,
      }}
    >
      <DashboardStack.Screen
        initialParams={ route.params }
        name="dashboard"
        component={ProjectDashboard}
      />
      
    </DashboardStack.Navigator>
  )
    }

  const GanttStackScreen = ({ navigation, route }) => {
    return (
    <GanttStack.Navigator
      screenOptions={{
        headerShown: false,
        headerMode: 'screen',
        ...TransitionPresets.ModalSlideFromBottomIOS,
      }}
    >
      <GanttStack.Screen
        initialParams={ route.params}
        name="ProjectGanttChart"
        component={ProjectGanttChart}
      />
      
      {/* <GanttStack.Screen
        initialParams={ route.params}
        navigationOptions={{ tabBarVisible: false}}
        name="GanttChartcomponent"
        component={GanttChartcomponent}
      /> */}
      <GanttStack.Screen
        initialParams={ route.params}
        navigationOptions={{ tabBarVisible: false}}
        name="DetailedEventPortraitView"
        component={StageScreen1}
      />
    </GanttStack.Navigator>
   
  )}

  export const ProjectPhotosStackScreen = ({navigation, route}) => (

    <ProjectPhotosStack.Navigator
      screenOptions={{
        headerShown: true,
        ...TransitionPresets.ModalSlideFromBottomIOS,
      }}
    >
      <ProjectPhotosStack.Screen
        initialParams={{ 
          token: route.params.token, 
          projectId: route.params.projectId, 
          projectName: route.params.projectName
         }}
        name="photos"
        component={ProjectPhotos}
        options={{ headerTitle: props => 
        <EmptyScreenHeader navigation={navigation} title='Photos' {...props} />, headerTitleContainerStyle: { left: 0, right: 0 } }}
      />
    </ProjectPhotosStack.Navigator>
  );
  

  const projectdocumentsStackScreen = ({ navigation, route }) => {

    return (
    <DocumentsStack.Navigator
      screenOptions={{
        headerShown: false,
        headerMode: 'screen'

      }}
    >
      <DocumentsStack.Screen
        initialParams={ route.params}
        name="documents"
        component={ProjectDocuments}
        options={{
          headerTitle: props => <EmptyScreenHeader navigation={props.navigation} title='Documents' {...props} />,
          headerTitleContainerStyle: { left: 0, right: 0 }
        }} />
      
  
    </DocumentsStack.Navigator>
  )}

const AnalyticsStackScreen = ({ navigation, route }) => {
  return (
  <AnalyticsStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: "#3d87f1",
      },
      headerTintColor: "#ffffff",
      headerTitleStyle: {
        fontWeight: "bold",
      },
      headerShown: false
    }}
  >
    <AnalyticsStack.Screen name="ProjectAnalytics" component={ProjectAnalytics} />

  </AnalyticsStack.Navigator>
)
  }





const ProjectRoutes = (props) => {
  return (
    <Tab.Navigator
      shifting={false}
      initialRouteName="dashboard"
      screenOptions={{ headerMode: 'screen', }}
      activeColor="#ffffff"
      barStyle={{ backgroundColor: '#3d87f1' }}>
      <Tab.Screen
        initialParams={ props.route.params.params}
        name="projectStackScreen"
        component={DashboardStackScreen}
        options={{
          tabBarLabel: 'Dashboard',
          tabBarIcon: ({ color }) => (
            // <OctIcons name="browser" color={color} size={26} />
            <Image
              source={
                color === '#ffffff'
                  ? require('../assets/icons/active_project_dashboard.png')
                  : require('../assets/icons/inactive_project_dashboard.png')
              }
              style={{ height: 26, width: 26 }}
            />
          ),
        }}
      />
      <Tab.Screen
        initialParams={props.route.params.params}
        name="GanttStackScreen"
        component={GanttStackScreen}
        options={{
          tabBarLabel: 'Gantt Chart',
          tabBarIcon: ({ color }) => (
            <Image
              source={
                color === '#ffffff'
                  ? require('../assets/icons/active_project_ganttchart.png')
                  : require('../assets/icons/inactive_project_ganttchart.png')
              }
              style={{ height: 26, width: 26 }}
            />
            // <MaterialCommunityIcons name="bell" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        initialParams={ props.route.params.params}
        name="projectdocumentsStackScreen"
        component={projectdocumentsStackScreen}
        options={{
          tabBarLabel: 'Documents',
          tabBarIcon: ({ color }) => (
            // <Icons name="document-outline" color={color} size={26} />
            <Image
              source={
                color === '#ffffff'
                  ? require('../assets/icons/active_project_documents.png')
                  : require('../assets/icons/inactive_project_documents.png')
              }
              style={{ height: 26, width: 26 }}
            />
          ),
        }}
      />
      <Tab.Screen
        initialParams={ props.route.params.params}
        name="AnalyticsStackScreen"
        component={AnalyticsStackScreen}
        options={{
          tabBarLabel: 'Analytics',
          tabBarIcon: ({ color }) => (
            // <AntDesignIcon name="team" color={color} size={26} />
            <Image
              source={
                color === '#ffffff'
                  ? require('../assets/icons/active_project_analytics.png')
                  : require('../assets/icons/inactive_project_analytics.png')
              }
              style={{ height: 26, width: 26 }}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default ProjectRoutes;
