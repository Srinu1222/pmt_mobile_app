import React from "react";
import { Image } from 'react-native'
import { createStackNavigator } from "@react-navigation/stack";
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import Dashboard from '../components/Dashboard'
import AllProjects from '../../PMT/components/AllProject'
import Team from '../components/Team'
import Documents from '../components/Documents'

import Doclist from '../components/Documents/Doclist';
import Projlist from '../components/Documents/Projlist';

import DailyReport from '../components/DailyReports/DailyReport'
import EmptyScreenHeader from '../components/EmptyScreens/EmptyScreenHeader'
import { TransitionPresets } from '@react-navigation/stack';
import ProjectPlanning1 from "../components/ProjectPlanning/projectPlanning1"
import ProjectStage from "../components/ProjectPlanning/ProjectStage";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import AddTeam from "../components/teams/AddTeam";
// import { Grid, Icon } from '@ant-design/react-native';
import DailyReport1 from '../components/DailyReports/DailyReport1';
import DailyReport3 from '../components/DailyReports/DailyReport3';
import DailyReport4 from '../components/DailyReports/DailyReport4';
import DailyReport5 from '../components/DailyReports/DailyReport5';
import LinkStageEvent from '../components/DailyReports/LinkStageEvent';
import DailyReportSubmit from '../components/DailyReports/DailyReportSubmit';
import ProjectSpecification1 from '../components/NewProject/ProjectSpecification';
import ProjectPlaning from '../components/NewProject/ProjectPlanning';
import NewProject from "../components/NewProjectBasic";
import CustomerDetails from '../components/NewProject/CustomerDetails';
import TeamDetails from '../components/NewProject/TeamDetails';
import SubContractorDetails from '../components/NewProject/SubContractorDetails';
import Databank from '../components/NewProject/Databank';
import Selectscreen from '../components/NewProject/Selectscreen'
import ResponsibilityMatrix from "../components/NewProject/ResponsibilityMatrix";
import StageScreen1 from '../components/DetailedEventPortraitView/StageScreen1'
import Photos from '../components/Photos';


const Tab = createMaterialBottomTabNavigator();

const DashboardStack = createStackNavigator();
const DailyReportStack = createStackNavigator();
const ProjectsStack = createStackNavigator();
const TeamStack = createStackNavigator();
const DocumentsStack = createStackNavigator();
const PhotosStack = createStackNavigator();
const DailyReportsStack = createStackNavigator();
const DetailedEventPortraitViewStack = createStackNavigator();


export const DetailedEventPortraitViewStackScreen = ({ navigation, route }) => (

  <DetailedEventPortraitViewStack.Navigator
    screenOptions={{
      headerShown: false,
      headerMode: 'screen',
      ...TransitionPresets.ModalSlideFromBottomIOS,
    }}
  >
    <DetailedEventPortraitViewStack.Screen
      initialParams={{
        token: route.params.token,
        project_id: route.params.project_id,
        event_id: route.params.event_id,
        event_name: route.params.event_name,
        fromLandScape: route.params.fromLandScape
      }}
      name="stagescreen1"
      component={StageScreen1}
    />
  </DetailedEventPortraitViewStack.Navigator>
);

export const DailyReportsStackScreen = ({ navigation, route }) => (
  <DailyReportsStack.Navigator
    screenOptions={{
      headerShown: false,
      headerMode: 'screen',
      ...TransitionPresets.ModalSlideFromBottomIOS,
    }}
  >
    <DailyReportsStack.Screen
      initialParams={{ token: route.params.token }}
      name="dailyReports"
      component={DailyReport}
    />
    <DailyReportsStack.Screen
      initialParams={{ token: route.params.token }}
      name="dailyReports1"
      component={DailyReport1}
    />
    <DailyReportsStack.Screen
      initialParams={{ token: route.params.token }}
      name="dailyReports3"
      component={DailyReport3}
    />
    <DailyReportsStack.Screen
      initialParams={{ token: route.params.token }}
      name="linkStageEventDocument"
      component={LinkStageEvent}
    />
    <DailyReportsStack.Screen
      initialParams={{ token: route.params.token }}
      name="dailyReports4"
      component={DailyReport4}
    />
    <DailyReportsStack.Screen
      initialParams={{ token: route.params.token }}
      name="dailyReports5"
      component={DailyReport5}
    />
    <DailyReportsStack.Screen
      initialParams={{ token: route.params.token }}
      name="reportSubmit"
      component={DailyReportSubmit}
    />
  </DailyReportsStack.Navigator>
);

const DashboardStackScreen = ({ navigation, route }) => (
  <DashboardStack.Navigator
    screenOptions={{
      headerShown: false,
      headerMode: 'screen',
      ...TransitionPresets.ModalSlideFromBottomIOS,
    }}
  >
    <DashboardStack.Screen
      initialParams={{ token: route.params.token }}
      name="dashboard"
      component={Dashboard}
    />
    <DashboardStack.Screen
      initialParams={{ token: route.params.token }} name="project_planning1" component={ProjectPlanning1} />
    <DashboardStack.Screen
      initialParams={{ token: route.params.token }} name="project_stage" component={ProjectStage}
    />
  </DashboardStack.Navigator>
);

const ProjectsStackScreen = ({ navigation, route }) => (
  <ProjectsStack.Navigator
    screenOptions={{
      headerShown: true,
      ...TransitionPresets.ModalSlideFromBottomIOS,
    }}
  >
    <ProjectsStack.Screen
      initialParams={{ token: route.params.token }}
      name="allProjects"
      component={AllProjects}
      options={{
        headerTitle: props => <EmptyScreenHeader navigation={navigation} title='All Projects' {...props} />,
        headerTitleContainerStyle: { left: 0, right: 0 },
      }}
    />
  </ProjectsStack.Navigator>
);

const DailyReportStackScreen = ({ navigation, route }) => (
  <DailyReportStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: "#3d87f1",
      },
      headerTintColor: "#ffffff",
      headerTitleStyle: {
        fontWeight: "bold",
      },
      headerShown: false
    }}
  >
    <DailyReportStack.Screen name="Daily Report" component={DailyReport} />
  </DailyReportStack.Navigator>
);

const TeamStackScreen = ({ navigation, route }) => (
  <TeamStack.Navigator
    screenOptions={{
      headerShown: true,
      ...TransitionPresets.ModalSlideFromBottomIOS,
    }}
  >
    <TeamStack.Screen
      initialParams={{ token: route.params.token }}
      name="team"
      component={Team}
      options={{ headerTitle: props => <EmptyScreenHeader navigation={navigation} title='Team' {...props} />, headerTitleContainerStyle: { left: 0, right: 0 } }}

    // options={{ headerTitle: props => <EmptyScreenHeader navigation={navigation} title='Team' {...props} />,
    // headerTitleContainerStyle: {left:0, right:0} }}
    />
    <TeamStack.Screen initialParams={{ token: route.params.token }} name="addTeam" component={AddTeam} options={{ headerShown: false }} />

  </TeamStack.Navigator>
);

export const PhotosStackScreen = ({ navigation, route }) => (
  <PhotosStack.Navigator
    screenOptions={{
      headerShown: true,
      ...TransitionPresets.ModalSlideFromBottomIOS,
    }}
  >
    <PhotosStack.Screen
      initialParams={{ token: route.params.token }}
      name="photos"
      component={Photos}
      options={{ headerTitle: props => <EmptyScreenHeader navigation={navigation} title='Photos' {...props} />, headerTitleContainerStyle: { left: 0, right: 0 } }}
    />
  </PhotosStack.Navigator>
);

const DocumentsStackScreen = ({ navigation, route }) => (
  <DocumentsStack.Navigator
    screenOptions={{
      headerShown: true
    }}
  >
    <DocumentsStack.Screen
      initialParams={{ token: route.params.token }}
      name="documents"
      component={Documents}
      options={{
        headerTitle: props => <EmptyScreenHeader navigation={navigation} title='Documents' {...props} />,
        headerTitleContainerStyle: { left: 0, right: 0 }
      }} />
    <DocumentsStack.Screen name="Document Name" component={Doclist} />
    <DocumentsStack.Screen name="Project Name" component={Projlist} />

  </DocumentsStack.Navigator>
)

const Routes = (props) => {
  return (
    <Tab.Navigator
      shifting={false}
      initialRouteName="dashboard"
      screenOptions={{ headerMode: 'screen', }}
      activeColor="#ffffff"
      barStyle={{ backgroundColor: '#3d87f1' }}>
      <Tab.Screen
        initialParams={{ token: props.route.params.token }}
        name="dashboardStackScreen"
        component={DashboardStackScreen}
        options={{
          tabBarLabel: 'Dashboard',
          tabBarIcon: ({ color }) => (
            // <OctIcons name="browser" color={color} size={26} />
            <Image
              source={
                color === '#ffffff'
                  ? require('../assets/images/android/active_dashboard_bottom_tab.png')
                  : require('../assets/images/android/inactive_dashboard_bottom_tab.png')
              }
              style={{ height: 26, width: 26 }}
            />
          ),
        }}
      />
      <Tab.Screen
        initialParams={{ token: props.route.params.token }}
        name="allProjectStackScreen"
        component={ProjectsStackScreen}
        options={{
          tabBarLabel: 'Projects',
          tabBarIcon: ({ color }) => (
            <Image
              source={
                color === '#ffffff'
                  ? require('../assets/images/android/active_projects_bottom_tab.png')
                  : require('../assets/images/android/inactive_projects_bottom_tab.png')
              }
              style={{ height: 26, width: 26 }}
            />
            // <MaterialCommunityIcons name="bell" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        initialParams={{ token: props.route.params.token }}
        name="teamStackScreen"
        component={TeamStackScreen}
        options={{
          tabBarLabel: 'Team',
          tabBarIcon: ({ color }) => (
            // <AntDesignIcon name="team" color={color} size={26} />
            <Image
              source={
                color === '#ffffff'
                  ? require('../assets/images/android/active_team_bottom_tab.png')
                  : require('../assets/images/android/inactive_team_bottom_tab.png')
              }
              style={{ height: 26, width: 26 }}
            />
          ),
        }}
      />
      <Tab.Screen
        initialParams={{ token: props.route.params.token }}
        name="documentsStackScreen"
        component={DocumentsStackScreen}
        options={{
          tabBarLabel: 'Documents',
          tabBarIcon: ({ color }) => (
            // <Icons name="document-outline" color={color} size={26} />
            <Image
              source={
                color === '#ffffff'
                  ? require('../assets/images/android/active_documents_bottom_tab.png')
                  : require('../assets/images/android/inactive_documents_bottom_tab.png')
              }
              style={{ height: 26, width: 26 }}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default Routes;
