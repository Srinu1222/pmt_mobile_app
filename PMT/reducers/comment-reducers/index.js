import {
    GET_COMMENT_LOADING,
    GET_COMMENT_SUCCESS,
    GET_COMMENT_RESET,
    GET_COMMENT_FAILURE,
    POST_COMMENT_LOADING,
    POST_COMMENT_SUCCESS,
    POST_COMMENT_RESET,
    POSt_COMMENT_FAILURE,
    POST_COMMENT_FAILURE
} from '../../actions/types';

const INITIAL_STATE = {
    comments: {
        get: {
            loading: false,
            reset: false,
            success: {
                ok: false,
                data: null,
            },
            failure: {
                error: false,
                message: '',
            },
        },
        post: {
            loading: false,
            reset: false,
            success: {
                ok: false,
                data: null,
            },
            failure: {
                error: false,
                message: '',
            },
        }
    }
};

const commentReducers = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_COMMENT_LOADING:
            return {
                ...state,
                comments: {
                    ...state.comments,
                    get: {
                        ...state.comments.get,
                        loading: true,
                        reset: false,
                        success: {
                            ...state.comments.get.success,
                            ok: false,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_COMMENT_SUCCESS:
            return {
                ...state,
                comments: {
                    ...state.comments,
                    get: {
                        ...state.comments.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.comments.get.success,
                            ok: true,
                            data: action.payload,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_COMMENT_FAILURE:
            return {
                ...state,
                comments: {
                    ...state.comments,
                    get: {
                        ...state.comments.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.comments.get.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: true,
                            message: action.payload.message,
                        },
                    },
                },
            };
        case GET_COMMENT_RESET:
            return {
                ...state,
                comments: {
                    ...state.comments.get,
                    post: {
                        ...INITIAL_STATE.comments.get,
                        reset: true,
                    },
                },
            }
        case POST_COMMENT_LOADING:
            return {
                ...state,
                comments: {
                    ...state.comments,
                    post: {
                        ...state.comments.post,
                        loading: true,
                        reset: false,
                        success: {
                            ...state.comments.post.success,
                            ok: false,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case POST_COMMENT_SUCCESS:
            return {
                ...state,
                comments: {
                    ...state.comments,
                    post: {
                        ...state.comments.post,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.comments.post.success,
                            ok: true,
                            data: action.payload,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case POST_COMMENT_FAILURE:
            return {
                ...state,
                comments: {
                    ...state.comments,
                    post: {
                        ...state.comments.post,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.comments.post.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: true,
                            message: action.payload.message,
                        },
                    },
                },
            };
        case POST_COMMENT_RESET:
            return {
                ...state,
                comments: {
                    ...state.comments.post,
                    post: {
                        ...INITIAL_STATE.comments.post,
                        reset: true,
                    },
                },
            }
    }
    return INITIAL_STATE;
};

export default commentReducers;