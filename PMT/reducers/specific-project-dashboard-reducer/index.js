import {
  GET_SPECIFIC_PROJECT_DETAILS_LOADING,
  GET_SPECIFIC_PROJECT_DETAILS_SUCCESS,
  GET_SPECIFIC_PROJECT_DETAILS_FAILURE,
} from "../../actions/types";

const INITIAL_STATE = {
  specificProjectDetails: {
    get: {
      loading: false,
      reset: false,
      success: {
        ok: false,
        data: null,
      },
      failure: {
        error: false,
        message: "",
      },
    },
  },
};

const specificProjectDetailsReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_SPECIFIC_PROJECT_DETAILS_LOADING:
      return {
        ...state,
        specificProjectDetails: {
          ...state.specificProjectDetails,
          get: {
            ...state.specificProjectDetails.get,
            loading: true,
            reset: false,
            success: {
              ...state.specificProjectDetails.get.success,
              ok: false,
            },
            failure: {
              error: false,
              message: "",
            },
          },
        },
      };

    case GET_SPECIFIC_PROJECT_DETAILS_SUCCESS:
      return {
        ...state,
        specificProjectDetails: {
          ...state.specificProjectDetails,
          get: {
            ...state.specificProjectDetails.get,
            loading: false,
            reset: false,
            success: {
              ...state.specificProjectDetails.get.success,
              ok: true,
              data: action.payload,
            },
            failure: {
              error: false,
              message: "",
            },
          },
        },
      };

    case GET_SPECIFIC_PROJECT_DETAILS_FAILURE:
      return {
        ...state,
        specificProjectDetails: {
          ...state.specificProjectDetails,
          get: {
            ...state.specificProjectDetails.get,
            loading: false,
            reset: false,
            success: {
              ...state.specificProjectDetails.get.success,
              ok: false,
              data: null,
            },
            failure: {
              error: true,
              message: action.payload.message,
            },
          },
        },
      };

    default:
      return state;
  }
};

export default specificProjectDetailsReducer;