import {
    GET_TICKET_LOADING,
    GET_TICKET_SUCCESS,
    GET_TICKET_FAILURE,
    GET_TICKET_RESET
} from '../../actions/types';

const INITIAL_STATE = {
    ticket: {
        get: {
            loading: false,
            reset: false,
            success: {
                ok: false,
                data: null,
            },
            failure: {
                error: false,
                message: '',
            },
        }
    }
};

const ticketReducers = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_TICKET_LOADING:
            return {
                ...state,
                ticket: {
                    ...state.ticket,
                    get: {
                        ...state.ticket.get,
                        loading: true,
                        reset: false,
                        success: {
                            ...state.ticket.get.success,
                            ok: false,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_TICKET_SUCCESS:
            return {
                ...state,
                ticket: {
                    ...state.ticket,
                    get: {
                        ...state.ticket.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.ticket.get.success,
                            ok: true,
                            data: action.payload,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_TICKET_FAILURE:
            return {
                ...state,
                ticket: {
                    ...state.ticket,
                    get: {
                        ...state.ticket.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.ticket.get.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: true,
                            message: action.payload.message,
                        },
                    },
                },
            };
        case GET_TICKET_RESET:
            return {
                ...state,
                ticket: {
                    ...state.ticket.get,
                    post: {
                        ...INITIAL_STATE.ticket.get,
                        reset: true,
                    },
                },
            }
    }
    return state;
}

export default ticketReducers;