import {
    GET_GANTT_EVENTS_LOADING,
    GET_GANTT_EVENTS_SUCCESS,
    GET_GANTT_EVENTS_FAILURE,
    GET_GANTT_EVENTS_RESET
} from '../../actions/types';

const INITIAL_STATE = {
    ganttevents: {
        get: {
            loading: false,
            reset: false,
            success: {
                ok: false,
                data: null,
            },
            failure: {
                error: false,
                message: '',
            },
        }
    }
};

const ganttEventReducers = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_GANTT_EVENTS_LOADING:
            return {
                ...state,
                ganttevents: {
                    ...state.ganttevents,
                    get: {
                        ...state.ganttevents,
                        loading: true,
                        reset: false,
                        success: {
                            ...state.ganttevents.success,
                            ok: false,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_GANTT_EVENTS_SUCCESS:
            return {
                ...state,
                ganttevents: {
                    ...state.ganttevents,
                    get: {
                        ...state.ganttevents.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.ganttevents.get.success,
                            ok: true,
                            data: action.payload,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_GANTT_EVENTS_FAILURE:
            return {
                ...state,
                ganttevents: {
                    ...state.ganttevents,
                    ganttevents: {
                        ...state.ganttevents,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.ganttevents.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: true,
                            message: action.payload.message,
                        },
                    },
                },
            };
        case GET_GANTT_EVENTS_RESET:
            return {
                ...state,
                ganttevents: {
                    ...state.ganttevents.get,
                    get: {
                        ...INITIAL_STATE.ganttevents.get,
                        reset: true,
                    },
                },
            }
    }
    return state;
}

export default ganttEventReducers;