import {
    GET_SITE_HANDOVER_LOADING,
    GET_SITE_HANDOVER_SUCCESS,
    GET_SITE_HANDOVER_FAILURE,
    GET_SITE_HANDOVER_RESET
} from '../../actions/types';

const INITIAL_STATE = {
    site: {
        get: {
            loading: false,
            reset: false,
            success: {
                ok: false,
                data: null,
            },
            failure: {
                error: false,
                message: '',
            },
        }
    }
};

const siteHandoverReducers = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_SITE_HANDOVER_LOADING:
            return {
                ...state,
                site: {
                    ...state.site,
                    get: {
                        ...state.site.get,
                        loading: true,
                        reset: false,
                        success: {
                            ...state.site.get.success,
                            ok: false,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_SITE_HANDOVER_SUCCESS:
            return {
                ...state,
                site: {
                    ...state.site,
                    get: {
                        ...state.site.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.site.get.success,
                            ok: true,
                            data: action.payload,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_SITE_HANDOVER_FAILURE:
            return {
                ...state,
                site: {
                    ...state.site,
                    get: {
                        ...state.site.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.site.get.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: true,
                            message: action.payload.message,
                        },
                    },
                },
            };
        case GET_SITE_HANDOVER_RESET:
            return {
                ...state,
                site: {
                    ...state.site.get,
                    post: {
                        ...INITIAL_STATE.site.get,
                        reset: true,
                    },
                },
            }
    }
    return state;
}

export default siteHandoverReducers;