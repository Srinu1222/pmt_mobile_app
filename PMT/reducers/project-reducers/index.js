import {
    POST_PROJECT_LOADING,
    POST_PROJECT_SUCCESS,
    POST_PROJECT_FAILURE,
    POST_PROJECT_RESET,
    GET_RESPONSIBILITY_MATRIX_LOADING,
    GET_RESPONSIBILITY_MATRIX_SUCCESS,
    GET_RESPONSIBILITY_MATRIX_FAILURE,
    UPDATE_SELECTED_PROJECT,
} from "../../actions/types";

const INITIAL_STATE = {
    projects: {
        post: {
            loading: false,
            reset: false,
            success: {
                ok: false,
                data: null,
            },
            failure: {
                error: false,
                message: '',
            },
        },
        defaultRights: {
            get: {
                loading: false,
                reset: false,
                success: {
                    ok: false,
                    data: null,
                },
                failure: {
                    error: false,
                    message: '',
                },
            }
        }
    },
};

const projectReducers = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case POST_PROJECT_LOADING:
            return {
                ...state,
                projects: {
                    ...state.projects,
                    post: {
                        ...state.projects.post,
                        loading: true,
                        reset: false,
                        success: {
                            ...state.projects.post.success,
                            ok: false,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case POST_PROJECT_SUCCESS:
            return {
                ...state,
                projects: {
                    ...state.projects,
                    post: {
                        ...state.projects.post,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.projects.post.success,
                            ok: true,
                            data: action.payload,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case POST_PROJECT_FAILURE:
            return {
                ...state,
                projects: {
                    ...state.login,
                    post: {
                        ...state.projects.post,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.projects.post.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: true,
                            message: action.payload.message,
                        },
                    },
                },
            };
        case POST_PROJECT_RESET:
            return {
                ...state,
                projects: {
                    ...state.projects.post,
                    post: {
                        ...INITIAL_STATE.projects.post,
                        reset: true,
                    },
                },
            };

        case GET_RESPONSIBILITY_MATRIX_LOADING:
            return {
                ...state,
                defaultRights: {
                    ...state.defaultRights,
                    get: {
                        ...state.defaultRights.get,
                        loading: true,
                        reset: false,
                        success: {
                            ...state.defaultRights.get.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: false,
                            message: ''
                        }
                    }
                }
            }

        case GET_RESPONSIBILITY_MATRIX_SUCCESS:
            return {
                ...state,
                defaultRights: {
                    ...state.defaultRights,
                    get: {
                        ...state.defaultRights.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.defaultRights.get.success,
                            ok: true,
                            data: action.payload,
                        },
                        failure: {
                            error: false,
                            message: ''
                        }
                    }
                }
            }

        case GET_RESPONSIBILITY_MATRIX_FAILURE:
            return {
                ...state,
                defaultRights: {
                    ...state.defaultRights,
                    get: {
                        ...state.defaultRights.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.defaultRights.get.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: true,
                            message: action.payload.message
                        }
                    }
                }
            }

        default:
            return state
    }
}

export default projectReducers;