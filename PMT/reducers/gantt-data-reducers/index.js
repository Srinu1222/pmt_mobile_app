import {
    GET_GANTT_DATA_LOADING,
    GET_GANTT_DATA_SUCCESS,
    GET_GANTT_DATA_FAILURE,
    GET_GANTT_DATA_RESET
} from '../../actions/types';

const INITIAL_STATE = {
    ganttdata: {
        get: {
            loading: false,
            reset: false,
            success: {
                ok: false,
                data: null,
            },
            failure: {
                error: false,
                message: '',
            },
        },
    },
};

const ganttDataReducers = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_GANTT_DATA_LOADING:
            //"data loading")
            return {
                ...state,
                ganttdata: {
                    ...state.ganttdata,
                    get: {
                        ...state.ganttdata.get,
                        loading: true,
                        reset: false,
                        success: {
                            ...state.ganttdata.get.success,
                            ok: false,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_GANTT_DATA_SUCCESS:
            //"data success")

            return {
                ...state,
                ganttdata: {
                    ...state.ganttdata,
                    get: {
                        ...state.ganttdata.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.ganttdata.get.success,
                            ok: true,
                            data: action.payload,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_GANTT_DATA_FAILURE:
            //"data failure")

            return {
                ...state,
                ganttdata: {
                    ...state.ganttdata,
                    get: {
                        ...state.ganttdata.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.ganttdata.get.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: true,
                            message: '',
                        },
                    },
                },
            };
        case GET_GANTT_DATA_RESET:
            //"data reset")

            return {
                ...state,
                ganttdata: {
                    ...state.ganttdata.get,
                    get: {
                        ...INITIAL_STATE.ganttdata.get,
                        reset: true,
                    },
                },
            }
    }
    return state
}

export default ganttDataReducers;