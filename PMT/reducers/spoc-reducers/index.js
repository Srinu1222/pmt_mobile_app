import {
    GET_SPOC_LOADING,
    GET_SPOC_SUCCESS,
    GET_SPOC_FAILURE,
    GET_SPOC_RESET
} from '../../actions/types';

const INITIAL_STATE = {
    spoc: {
        get: {
            loading: false,
            reset: false,
            success: {
                ok: false,
                data: null,
            },
            failure: {
                error: false,
                message: '',
            },
        }
    }
};

const spocReducers = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_SPOC_LOADING:
            return {
                ...state,
                spoc: {
                    ...state.spoc,
                    get: {
                        ...state.spoc.get,
                        loading: true,
                        reset: false,
                        success: {
                            ...state.spoc.get.success,
                            ok: false,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_SPOC_SUCCESS:
            return {
                ...state,
                spoc: {
                    ...state.spoc,
                    get: {
                        ...state.spoc.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.spoc.get.success,
                            ok: true,
                            data: action.payload,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_SPOC_FAILURE:
            return {
                ...state,
                spoc: {
                    ...state.spoc,
                    get: {
                        ...state.spoc.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.spoc.get.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: true,
                            message: action.payload.message,
                        },
                    },
                },
            };
        case GET_SPOC_RESET:
            return {
                ...state,
                spoc: {
                    ...state.spoc.get,
                    post: {
                        ...INITIAL_STATE.spoc.get,
                        reset: true,
                    },
                },
            }
    }
    return state;
}

export default spocReducers;