import { combineReducers } from "redux";
import signUpReducers from "./signup-reducers/index";
import loginReducers from "./login-reducers/index";
import projectReducers from "./project-reducers/index";
import allProjectReducers from "./all-project-reducers/index";
import docsReducers from "./docs-reducers/index";
import teamReducers from "./team-reducers/index";
import ganttReducers from "./gantt-reducers/index";
import preRequsiteReducers from "./pre-requisite-reducers/index";
import approvalReducers from "./approval-reducers/index";
import engineeringReducers from "./engineering-reducers/index";
import procurementReducers from "./procurement-reducers/index";
import materialHandlingReducers from "./material-handling-reducers/index";
import constructionReducers from "./constructors-reducers/index";
import siteHandoverReducers from "./site-handover-reducers/index";
import ganttEventReducers from "./gantt-events-reducers/index";
import ganttEventStageReducers from "./gantt-event-stage-reducers/index";
import eventDataReducers from "./event-data-reducers/index";
import commentReducers from "./comment-reducers/index";
import dailyUpdatesReducer from "./daily-updates-reducers/index";
import specificProjectDetailsReducer from "./specific-project-dashboard-reducer/index";
import commentFileReducers from "./comment-files-reducers/index";
import deleteReducers from "./delete-reducers/index";
import updateReducers from "./update-reducers/index";
import viewSpecificationReducers from "./view-specification-reducers/index";
import ticketReducers from "./ticket-reducers/index";
import spocReducers from "./spoc-reducers/index";
import projectLevelTeamReducer from "./project-level-team-reducer/index";
import uploadFileReducers from "./upload-file-reducers";
import ganttEventMenuReducers from "./gantt-event-menu-reducers/index";
import ganttDataReducers from "./gantt-data-reducers/index";
import inventoryReducer from "./inventory-reducer/index";
import materialUsedReducer from "./material-used-reducer/index";
import projectDocsReducer from "./project-docs-reducer/index";
import notificationReducers from './notification-reducers/index';
import drawingReducers from './drawing-reducers/index';
import companyProfileReducers from './profile-reducers/index';
import getSampleGanttReducers from './sample-gantts-reducers/index';
import sampleGanttDetailsReducers from './sample-gantts-details-reducers/index';
import ganttProjectReducers from './gantt-project-reducers/index'
import ganttProjectDetailsReducers from './gantt-project-details-reducers/index'
import projectFinanceReducer from './project-finance-reducer/index'
import getSampleEventsReducers from './sample-events-reducers/index'
import getSampleEventReducers from './sample-event-reducers/index'
import getProjectReducers from './get-project-reducers/index'
import postEventReducers from './post-event-reducer/index'
import specificationReducers from './project-specification-reducers/index'
import specificationEventReducers from './project-specification-events-reducers/index'
import subContractorReducers from './sub-contractor-reducers/index'
import subContractorProjectReducers from './sub-contarctors-project-reducers/index'
import taskListReducers from './task-list-reducers/index'

export default combineReducers({
  signUpReducers,
  loginReducers,
  projectReducers,
  allProjectReducers,
  docsReducers,
  teamReducers,
  ganttReducers,
  preRequsiteReducers,
  approvalReducers,
  engineeringReducers,
  procurementReducers,
  materialHandlingReducers,
  constructionReducers,
  siteHandoverReducers,
  ganttEventReducers,
  ganttEventStageReducers,
  eventDataReducers,
  commentReducers,
  commentFileReducers,
  dailyUpdatesReducer,
  specificProjectDetailsReducer,
  deleteReducers,
  updateReducers,
  viewSpecificationReducers,
  ticketReducers,
  spocReducers,
  projectLevelTeamReducer,
  uploadFileReducers,
  ganttEventMenuReducers,
  ganttDataReducers,
  inventoryReducer,
  materialUsedReducer,
  projectDocsReducer,
  notificationReducers,
  drawingReducers,
  companyProfileReducers,
  getSampleGanttReducers,
  sampleGanttDetailsReducers,
  ganttProjectReducers,
  ganttProjectDetailsReducers,
  projectFinanceReducer,
  getSampleEventsReducers,
  getSampleEventReducers,
  getProjectReducers,
  postEventReducers,
  specificationReducers,
  specificationEventReducers,
  subContractorReducers,
  subContractorProjectReducers,
  taskListReducers
})
