import {
    GET_SAMPLE_GANTTS_PROJECT_DETAILS_LOADING,
    GET_SAMPLE_GANTTS_PROJECT_DETAILS_SUCCESS,
    GET_SAMPLE_GANTTS_PROJECT_DETAILS_FAILURE
} from '../../actions/types';

const INITIAL_STATE = {
    ganttproject: {
        get: {
            loading: false,
            reset: false,
            success: {
                ok: false,
                data: null,
            },
            failure: {
                error: false,
                message: '',
            },
        },
    },
};

const ganttProjectReducers = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_SAMPLE_GANTTS_PROJECT_DETAILS_LOADING:
            return {
                ...state,
                ganttproject: {
                    ...state.ganttproject,
                    get: {
                        ...state.ganttproject.get,
                        loading: true,
                        reset: false,
                        success: {
                            ...state.ganttproject.get.success,
                            ok: false,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_SAMPLE_GANTTS_PROJECT_DETAILS_SUCCESS:
            return {
                ...state,
                ganttproject: {
                    ...state.ganttproject,
                    get: {
                        ...state.ganttproject.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.ganttproject.get.success,
                            ok: true,
                            data: action.payload,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_SAMPLE_GANTTS_PROJECT_DETAILS_FAILURE:
            return {
                ...state,
                ganttproject: {
                    ...state.ganttproject,
                    get: {
                        ...state.ganttproject.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.ganttproject.get.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: true,
                            message: action.payload.message,
                        },
                    },
                },
            };
    }
    return state;
}

export default ganttProjectReducers;