import {
    GET_DELETE_LOADING,
    GET_DELETE_SUCCESS,
    GET_DELETE_FAILURE,
    GET_DELETE_RESET
} from '../../actions/types';

const INITIAL_STATE = {
    delete: {
        get: {
            loading: false,
            reset: false,
            success: {
                ok: false,
                data: null,
            },
            failure: {
                error: false,
                message: '',
            },
        }
    }
};

const deleteReducers = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_DELETE_LOADING:
            return {
                ...state,
                delete: {
                    ...state.delete,
                    get: {
                        ...state.delete.get,
                        loading: true,
                        reset: false,
                        success: {
                            ...state.delete.get.success,
                            ok: false,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_DELETE_SUCCESS:
            return {
                ...state,
                delete: {
                    ...state.delete,
                    get: {
                        ...state.delete.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.delete.get.success,
                            ok: true,
                            data: action.payload,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_DELETE_FAILURE:
            return {
                ...state,
                delete: {
                    ...state.delete,
                    get: {
                        ...state.delete.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.delete.get.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: true,
                            message: action.payload.message,
                        },
                    },
                },
            };
        case GET_DELETE_RESET:
            return {
                ...state,
                delete: {
                    ...state.delete.get,
                    get: {
                        ...INITIAL_STATE.delete.get,
                        reset: true,
                    },
                },
            }
    }
    return INITIAL_STATE;
}

export default deleteReducers;    