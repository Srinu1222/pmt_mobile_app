import {
    GET_PROJECT_SPECIFICATION_EVENTS_LOADING,
    GET_PROJECT_SPECIFICATION_EVENTS_SUCCESS,
    GET_PROJECT_SPECIFICATION_EVENTS_FAILURE
} from '../../actions/types'

const INITIAL_STATE = {
    specEvent: {
        get: {
            loading: false,
            reset: false,
            success: {
                ok: false,
                data: null,
            },
            failure: {
                error: false,
                message: '',
            },
        }
    }
};

const specificationEventReducers = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_PROJECT_SPECIFICATION_EVENTS_LOADING:
            return {
                ...state,
                specEvent: {
                    ...state.specEvent,
                    get: {
                        ...state.specEvent.get,
                        loading: true,
                        reset: false,
                        success: {
                            ...state.specEvent.get.success,
                            ok: false,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_PROJECT_SPECIFICATION_EVENTS_SUCCESS:
            return {
                ...state,
                specEvent: {
                    ...state.specEvent,
                    get: {
                        ...state.specEvent.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.specEvent.get.success,
                            ok: true,
                            data: action.payload,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_PROJECT_SPECIFICATION_EVENTS_FAILURE:
            return {
                ...state,
                specEvent: {
                    ...state.specEvent,
                    get: {
                        ...state.specEvent.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.specEvent.get.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: true,
                            message: action.payload.message,
                        },
                    },
                },
            };
    }
    return state
}

export default specificationEventReducers;