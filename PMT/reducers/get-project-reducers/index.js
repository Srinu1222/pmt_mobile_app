import {
    GET_PROJECT_LOADING,
    GET_PROJECT_SUCCESS,
    GET_PROJECT_FAILURE,
    GET_PROJECT_RESET,
    UPDATE_SELECTED_PROJECT
} from '../../actions/types';

const INITIAL_STATE = {
    projects: {
        get: {
            loading: false,
            reset: false,
            success: {
                ok: false,
                data: null,
            },
            failure: {
                error: false,
                message: '',
            },
        },
    },
    selected_project: {
        data : null
   }
};

const getProjectReducers = (state = INITIAL_STATE, action) => {
    switch (action.type) {
                
        case UPDATE_SELECTED_PROJECT:
            return {
            ...state,
            selected_project: {
                ...state.selected_project,
                data: action.payload,
            }
        }
        case GET_PROJECT_LOADING:
            return {
                ...state,
                projects: {
                    ...state.projects,
                    get: {
                        ...state.projects.get,
                        loading: true,
                        reset: false,
                        success: {
                            ...state.projects.get.success,
                            ok: false,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_PROJECT_SUCCESS:
            return {
                ...state,
                projects: {
                    ...state.projects,
                    get: {
                        ...state.projects.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.projects.get.success,
                            ok: true,
                            data: action.payload,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_PROJECT_FAILURE:
            return {
                ...state,
                projects: {
                    ...state.projects,
                    get: {
                        ...state.projects.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.projects.get.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: true,
                            message: action.payload.message,
                        },
                    },
                },
            };
        case GET_PROJECT_RESET:
            return {
                ...state,
                projects: {
                    ...state.login.projects,
                    get: {
                        ...INITIAL_STATE.projects.get,
                        reset: true,
                    },
                },
            }
    }
    return state;
}

export default getProjectReducers;