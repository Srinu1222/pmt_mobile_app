import {
    GET_PROJECT_SPECIFICATION_LOADING,
    GET_PROJECT_SPECIFICATION_SUCCESS,
    GET_PROJECT_SPECIFICATION_FAILURE,
    GET_PROJECT_SPECIFICATION_RESET
} from '../../actions/types'

const INITIAL_STATE = {
    spec: {
        get: {
            loading: false,
            reset: false,
            success: {
                ok: false,
                data: null,
            },
            failure: {
                error: false,
                message: '',
            },
        }
    }
};

const specificationReducers = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_PROJECT_SPECIFICATION_LOADING:
            return {
                ...state,
                spec: {
                    ...state.spec,
                    get: {
                        ...state.spec.get,
                        loading: true,
                        reset: false,
                        success: {
                            ...state.spec.get.success,
                            ok: false,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_PROJECT_SPECIFICATION_SUCCESS:
            return {
                ...state,
                spec: {
                    ...state.spec,
                    get: {
                        ...state.spec.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.spec.get.success,
                            ok: true,
                            data: action.payload,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_PROJECT_SPECIFICATION_FAILURE:
            return {
                ...state,
                spec: {
                    ...state.spec,
                    get: {
                        ...state.spec.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.spec.get.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: true,
                            message: action.payload.message,
                        },
                    },
                },
            };
    }
    return state
}

export default specificationReducers;