import {
    GET_GANTTS_PROJECT_DETAILS_LOADING,
    GET_GANTTS_PROJECT_DETAILS_SUCCESS,
    GET_GANTTS_PROJECT_DETAILS_FAILURE
} from '../../actions/types';

const INITIAL_STATE = {
    ganttprojectdetails: {
        get: {
            loading: false,
            reset: false,
            success: {
                ok: false,
                data: null,
            },
            failure: {
                error: false,
                message: '',
            },
        },
    },
};

const ganttProjectDetailsReducers = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_GANTTS_PROJECT_DETAILS_LOADING:
            return {
                ...state,
                ganttprojectdetails: {
                    ...state.ganttprojectdetails,
                    get: {
                        ...state.ganttprojectdetails.get,
                        loading: true,
                        reset: false,
                        success: {
                            ...state.ganttprojectdetails.get.success,
                            ok: false,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_GANTTS_PROJECT_DETAILS_SUCCESS:
            return {
                ...state,
                ganttprojectdetails: {
                    ...state.ganttprojectdetails,
                    get: {
                        ...state.ganttprojectdetails.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.ganttprojectdetails.get.success,
                            ok: true,
                            data: action.payload,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_GANTTS_PROJECT_DETAILS_FAILURE:
            return {
                ...state,
                ganttprojectdetails: {
                    ...state.ganttprojectdetails,
                    get: {
                        ...state.ganttprojectdetails.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.ganttprojectdetails.get.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: true,
                            message: action.payload.message,
                        },
                    },
                },
            };
    }
    return state;
}

export default ganttProjectDetailsReducers;