import {
    GET_EVENT_DATA_LOADING,
    GET_EVENT_DATA_SUCCESS,
    GET_EVENT_DATA_FAILURE,
    GET_EVENT_DATA_RESET
} from '../../actions/types';

const INITIAL_STATE = {
    eventdata: {
        get: {
            loading: false,
            reset: false,
            success: {
                ok: false,
                data: null,
            },
            failure: {
                error: false,
                message: '',
            },
        }
    }
};

const eventDataReducers = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_EVENT_DATA_LOADING:
            return {
                ...state,
                eventdata: {
                    ...state.eventdata,
                    get: {
                        ...state.eventdata.get,
                        loading: true,
                        reset: false,
                        success: {
                            ...state.eventdata.get.success,
                            ok: false,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_EVENT_DATA_SUCCESS:
            return {
                ...state,
                eventdata: {
                    ...state.eventdata,
                    get: {
                        ...state.eventdata.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.eventdata.get.success,
                            ok: true,
                            data: action.payload,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_EVENT_DATA_FAILURE:
            return {
                ...state,
                eventdata: {
                    ...state.eventdata,
                    get: {
                        ...state.eventdata.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.eventdata.get.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: true,
                            message: action.payload.message,
                        },
                    },
                },
            };
        case GET_EVENT_DATA_RESET:
            return {
                ...state,
                eventdata: {
                    ...state.eventdata.get,
                    post: {
                        ...INITIAL_STATE.eventdata.get,
                        reset: true,
                    },
                },
            }
    }
    return state;
}

export default eventDataReducers;