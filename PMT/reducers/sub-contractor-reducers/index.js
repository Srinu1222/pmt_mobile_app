import {
    GET_SUB_CONTRACTOR_LOADING,
    GET_SUB_CONTRACTOR_SUCCESS,
    GET_SUB_CONTRACTOR_FAILURE,
    GET_SUB_CONTRACTOR_RESET
} from '../../actions/types';

const INITIAL_STATE = {
    subContractor: {
        get: {
            loading: false,
            reset: false,
            success: {
                ok: false,
                data: null,
            },
            failure: {
                error: false,
                message: '',
            },
        }
    }
};

const subContractorReducers = (state = INITIAL_STATE, action) => {

    switch (action.type) {
        case GET_SUB_CONTRACTOR_LOADING:
            return {
                ...state,
                subContractor: {
                    ...state.subContractor,
                    get: {
                        ...state.subContractor.get,
                        loading: true,
                        reset: false,
                        success: {
                            ...state.subContractor.get.success,
                            ok: false,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_SUB_CONTRACTOR_SUCCESS:
            return {
                ...state,
                subContractor: {
                    ...state.subContractor,
                    get: {
                        ...state.subContractor.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.subContractor.get.success,
                            ok: true,
                            data: action.payload,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_SUB_CONTRACTOR_FAILURE:
            return {
                ...state,
                subContractor: {
                    ...state.subContractor,
                    get: {
                        ...state.subContractor.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.subContractor.get.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: true,
                            message: action.payload.message,
                        },
                    },
                },
            };
        case GET_SUB_CONTRACTOR_RESET:
            return {
                ...state,
                subContractor: {
                    ...state.subContractor.get,
                    post: {
                        ...INITIAL_STATE.subContractor.get,
                        reset: true,
                    },
                },
            }
    }
    return state;
}

export default subContractorReducers;