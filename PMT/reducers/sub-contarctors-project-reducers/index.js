import {
    GET_SUB_CONTRACTOR_PROJECT_LOADING,
    GET_SUB_CONTRACTOR_PROJECT_SUCCESS,
    GET_SUB_CONTRACTOR_PROJECT_FAILURE,
    GET_SUB_CONTRACTOR_PROJECT_RESET
} from '../../actions/types';

const INITIAL_STATE = {
    subContractorProject: {
        get: {
            loading: false,
            reset: false,
            success: {
                ok: false,
                data: null,
            },
            failure: {
                error: false,
                message: '',
            },
        }
    }
};

const subContractorProjectReducers = (state = INITIAL_STATE, action) => {

    switch (action.type) {
        case GET_SUB_CONTRACTOR_PROJECT_LOADING:
            return {
                ...state,
                subContractorProject: {
                    ...state.subContractorProject,
                    get: {
                        ...state.subContractorProject.get,
                        loading: true,
                        reset: false,
                        success: {
                            ...state.subContractorProject.get.success,
                            ok: false,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_SUB_CONTRACTOR_PROJECT_SUCCESS:
            return {
                ...state,
                subContractorProject: {
                    ...state.subContractorProject,
                    get: {
                        ...state.subContractorProject.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.subContractorProject.get.success,
                            ok: true,
                            data: action.payload,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_SUB_CONTRACTOR_PROJECT_FAILURE:
            return {
                ...state,
                subContractorProject: {
                    ...state.subContractorProject,
                    get: {
                        ...state.subContractorProject.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.subContractorProject.get.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: true,
                            message: action.payload.message,
                        },
                    },
                },
            };
        case GET_SUB_CONTRACTOR_PROJECT_RESET:
            return {
                ...state,
                subContractorProject: {
                    ...state.subContractorProject.get,
                    post: {
                        ...INITIAL_STATE.subContractorProject.get,
                        reset: true,
                    },
                },
            }
    }
    return state;
}

export default subContractorProjectReducers;