import {
    GET_SAMPLE_GANTTS_LOADING,
    GET_SAMPLE_GANTTS_SUCCESS,
    GET_SAMPLE_GANTTS_FAILURE,
    GET_SAMPLE_GANTTS_RESET
} from "../../actions/types";

const INITIAL_STATE = {
    samplegantts: {
        get: {
            loading: false,
            reset: false,
            success: {
                ok: false,
                data: null,
            },
            failure: {
                error: false,
                message: "",
            },
        },
    },
};

const getSampleGanttReducers = (state = INITIAL_STATE, action) => {

    // //action.payload);
    switch (action.type) {
        case GET_SAMPLE_GANTTS_LOADING:
            return {
                ...state,
                samplegantts: {
                    ...state.samplegantts,
                    get: {
                        ...state.samplegantts.get,
                        loading: true,
                        reset: false,
                        success: {
                            ...state.samplegantts.get.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: false,
                            message: "",
                        },
                    },
                },
            };

        case GET_SAMPLE_GANTTS_SUCCESS:
            return {
                ...state,
                samplegantts: {
                    ...state.samplegantts,
                    get: {
                        ...state.samplegantts.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.samplegantts.get.success,
                            ok: true,
                            data: action.payload,
                        },
                        failure: {
                            error: false,
                            message: "",
                        },
                    },
                },
            };

        case GET_SAMPLE_GANTTS_FAILURE:
            return {
                ...state,
                samplegantts: {
                    ...state.samplegantts,
                    get: {
                        ...state.samplegantts.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.samplegantts.get.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: true,
                            message: action.payload.message,
                        },
                    },
                },
            };
        case GET_SAMPLE_GANTTS_RESET:
            return {
                ...state,
                samplegantts: {
                    ...state.samplegantts,
                    get: {
                        ...INITIAL_STATE.samplegantts.get,
                        reset: true
                    },
                },
            }
        default:
            return state;
    }
};

export default getSampleGanttReducers;
