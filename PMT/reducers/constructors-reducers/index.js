import {
    GET_CONSTRUCTION_LOADING,
    GET_CONSTRUCTION_SUCCESS,
    GET_CONSTRUCTION_FAILURE,
    GET_CONSTRUCTION_RESET
} from '../../actions/types';

const INITIAL_STATE = {
    construction: {
        get: {
            loading: false,
            reset: false,
            success: {
                ok: false,
                data: null,
            },
            failure: {
                error: false,
                message: '',
            },
        }
    }
};

const constructionReducers = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_CONSTRUCTION_LOADING:
            return {
                ...state,
                construction: {
                    ...state.construction,
                    get: {
                        ...state.construction.get,
                        loading: true,
                        reset: false,
                        success: {
                            ...state.construction.get.success,
                            ok: false,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_CONSTRUCTION_SUCCESS:
            return {
                ...state,
                construction: {
                    ...state.construction,
                    get: {
                        ...state.construction.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.construction.get.success,
                            ok: true,
                            data: action.payload,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_CONSTRUCTION_FAILURE:
            return {
                ...state,
                construction: {
                    ...state.construction,
                    get: {
                        ...state.construction.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.construction.get.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: true,
                            message: action.payload.message,
                        },
                    },
                },
            };
        case GET_CONSTRUCTION_RESET:
            return {
                ...state,
                construction: {
                    ...state.construction.get,
                    post: {
                        ...INITIAL_STATE.construction.get,
                        reset: true,
                    },
                },
            }
    }
    return state;
}

export default constructionReducers;