import {
    GET_NOTIFICATION_LOADING,
    GET_NOTIFICATION_SUCCESS,
    GET_NOTIFICATION_FAILURE,
    GET_NOTIFICATION_RESET
} from '../../actions/types';

const INITIAL_STATE = {
    notifications: {
        get: {
            loading: false,
            reset: false,
            success: {
                ok: false,
                data: null,
            },
            failure: {
                error: false,
                message: '',
            },
        }
    }
};

const notificationReducers = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_NOTIFICATION_LOADING:
            return {
                ...state,
                notifications: {
                    ...state.notifications,
                    get: {
                        ...state.notifications.get,
                        loading: true,
                        reset: false,
                        success: {
                            ...state.notifications.get.success,
                            ok: false,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_NOTIFICATION_SUCCESS:
            return {
                ...state,
                notifications: {
                    ...state.notifications,
                    get: {
                        ...state.notifications.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.notifications.get.success,
                            ok: true,
                            data: action.payload,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_NOTIFICATION_FAILURE:
            return {
                ...state,
                notifications: {
                    ...state.notifications,
                    get: {
                        ...state.notifications.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.notifications.get.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: true,
                            message: action.payload.message,
                        },
                    },
                },
            };
        case GET_NOTIFICATION_RESET:
            return {
                ...state,
                notifications: {
                    ...state.notifications.get,
                    post: {
                        ...INITIAL_STATE.notifications.get,
                        reset: true,
                    },
                },
            }
    }
    return state;
}

export default notificationReducers;