import {
    GET_ALL_PROJECT_LOADING,
    GET_ALL_PROJECT_SUCCESS,
    GET_ALL_PROJECT_FAILURE
} from '../../actions/types';

const INITIAL_STATE = {
    projects: {
        get: {
            loading: false,
            reset: false,
            success: {
                ok: false,
                data: null,
            },
            failure: {
                error: false,
                message: '',
            },
        },
    },
};

const allProjectReducers = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_ALL_PROJECT_LOADING:
            return {
                ...state,
                projects: {
                    ...state.projects,
                    get: {
                        ...state.projects.get,
                        loading: true,
                        reset: false,
                        success: {
                            ...state.projects.get.success,
                            ok: false,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_ALL_PROJECT_SUCCESS:
            return {
                ...state,
                projects: {
                    ...state.projects,
                    get: {
                        ...state.projects.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.projects.get.success,
                            ok: true,
                            data: action.payload,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_ALL_PROJECT_FAILURE:
            return {
                ...state,
                projects: {
                    ...state.projects,
                    get: {
                        ...state.projects.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.projects.get.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: true,
                            message: action.payload.message,
                        },
                    },
                },
            };
        case GET_ALL_PROJECT_FAILURE:
            return {
                ...state,
                projects: {
                    ...state.projects.get,
                    get: {
                        ...INITIAL_STATE.projects.post,
                        reset: true,
                    },
                },
            }
    }
    return state;
}

export default allProjectReducers;