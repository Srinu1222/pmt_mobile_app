import {
    GET_TASK_LIST_LOADING,
    GET_TASK_LIST_SUCCESS,
    GET_TASK_LIST_FAILURE,
    GET_TASK_LIST_RESET
} from '../../actions/types';

const INITIAL_STATE = {
    taskList: {
        get: {
            loading: false,
            reset: false,
            success: {
                ok: false,
                data: null,
            },
            failure: {
                error: false,
                message: '',
            },
        }
    }
};

const taskListReducers = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_TASK_LIST_LOADING:
            return {
                ...state,
                taskList: {
                    ...state.taskList,
                    get: {
                        ...state.taskList.get,
                        loading: true,
                        reset: false,
                        success: {
                            ...state.taskList.get.success,
                            ok: false,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_TASK_LIST_SUCCESS:
            return {
                ...state,
                taskList: {
                    ...state.taskList,
                    get: {
                        ...state.taskList.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.taskList.get.success,
                            ok: true,
                            data: action.payload,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_TASK_LIST_FAILURE:
            return {
                ...state,
                taskList: {
                    ...state.taskList,
                    get: {
                        ...state.taskList.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.taskList.get.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: true,
                            message: action.payload.message,
                        },
                    },
                },
            };
        case GET_TASK_LIST_RESET:
            return {
                ...state,
                taskList: {
                    ...state.taskList.get,
                    post: {
                        ...INITIAL_STATE.taskList.get,
                        reset: true,
                    },
                },
            }
    }
    return state;
}

export default taskListReducers;