import {
    GET_TEAMS_LOADING,
    GET_TEAMS_SUCCESS,
    GET_TEAMS_FAILURE,
    GET_TEAMS_RESET
} from '../../actions/types';

const INITIAL_STATE = {
    teams: {
        get: {
            loading: false,
            reset: false,
            success: {
                ok: false,
                data: null,
            },
            failure: {
                error: false,
                message: '',
            },
        }
    }
};

const teamReducers = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_TEAMS_LOADING:
            return {
                ...state,
                teams: {
                    ...state.teams,
                    get: {
                        ...state.teams.get,
                        loading: true,
                        reset: false,
                        success: {
                            ...state.teams.get.success,
                            ok: false,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_TEAMS_SUCCESS:
            return {
                ...state,
                teams: {
                    ...state.teams,
                    get: {
                        ...state.teams.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.teams.get.success,
                            ok: true,
                            data: action.payload,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_TEAMS_FAILURE:
            return {
                ...state,
                teams: {
                    ...state.teams,
                    get: {
                        ...state.teams.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.teams.get.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: true,
                            message: action.payload.message,
                        },
                    },
                },
            };
        case GET_TEAMS_RESET:
            return {
                ...state,
                teams: {
                    ...state.teams.get,
                    post: {
                        ...INITIAL_STATE.teams.get,
                        reset: true,
                    },
                },
            }
    }
    return state;
}

export default teamReducers;