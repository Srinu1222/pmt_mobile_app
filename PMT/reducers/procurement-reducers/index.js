import {
    GET_PROCUREMENT_LOADING,
    GET_PROCUREMENT_SUCCESS,
    GET_PROCUREMENT_FAILURE,
    GET_PROCUREMENT_RESET
} from '../../actions/types';

const INITIAL_STATE = {
    procurement: {
        get: {
            loading: false,
            reset: false,
            success: {
                ok: false,
                data: null,
            },
            failure: {
                error: false,
                message: '',
            },
        }
    }
};

const procurementReducers = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_PROCUREMENT_LOADING:
            return {
                ...state,
                procurement: {
                    ...state.procurement,
                    get: {
                        ...state.procurement.get,
                        loading: true,
                        reset: false,
                        success: {
                            ...state.procurement.get.success,
                            ok: false,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_PROCUREMENT_SUCCESS:
            return {
                ...state,
                procurement: {
                    ...state.procurement,
                    get: {
                        ...state.procurement.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.procurement.get.success,
                            ok: true,
                            data: action.payload,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_PROCUREMENT_FAILURE:
            return {
                ...state,
                procurement: {
                    ...state.procurement,
                    get: {
                        ...state.procurement.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.procurement.get.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: true,
                            message: action.payload.message,
                        },
                    },
                },
            };
        case GET_PROCUREMENT_RESET:
            return {
                ...state,
                procurement: {
                    ...state.procurement.get,
                    post: {
                        ...INITIAL_STATE.procurement.get,
                        reset: true,
                    },
                },
            }
    }
    return state;
}

export default procurementReducers;