import {
    GET_ENGINEERING_LOADING,
    GET_ENGINEERING_SUCCESS,
    GET_ENGINEERING_FAILURE,
    GET_ENGINEERING_RESET,
} from '../../actions/types';

const INITIAL_STATE = {
    engg: {
        get: {
            loading: false,
            reset: false,
            success: {
                ok: false,
                data: null,
            },
            failure: {
                error: false,
                message: '',
            },
        }
    }
};

const engineeringReducers = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_ENGINEERING_LOADING:
            return {
                ...state,
                engg: {
                    ...state.engg,
                    get: {
                        ...state.engg.get,
                        loading: true,
                        reset: false,
                        success: {
                            ...state.engg.get.success,
                            ok: false,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_ENGINEERING_SUCCESS:
            return {
                ...state,
                engg: {
                    ...state.engg,
                    get: {
                        ...state.engg.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.engg.get.success,
                            ok: true,
                            data: action.payload,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_ENGINEERING_FAILURE:
            return {
                ...state,
                engg: {
                    ...state.engg,
                    get: {
                        ...state.engg.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.engg.get.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: true,
                            message: action.payload.message,
                        },
                    },
                },
            };
        case GET_ENGINEERING_RESET:
            return {
                ...state,
                engg: {
                    ...state.engg.get,
                    get: {
                        ...INITIAL_STATE.engg.get,
                        reset: true,
                    },
                },
            }
    }
    return state;
}

export default engineeringReducers;