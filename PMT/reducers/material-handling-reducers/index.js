import {
    GET_MATERIAL_HANDLING_LOADING,
    GET_MATERIAL_HANDLING_SUCCESS,
    GET_MATERIAL_HANDLING_FAILURE,
    GET_MATERIAL_HANDLING_RESET
} from '../../actions/types';

const INITIAL_STATE = {
    material: {
        get: {
            loading: false,
            reset: false,
            success: {
                ok: false,
                data: null,
            },
            failure: {
                error: false,
                message: '',
            },
        }
    }
};

const materialHandlingReducers = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_MATERIAL_HANDLING_LOADING:
            return {
                ...state,
                material: {
                    ...state.material,
                    get: {
                        ...state.material.get,
                        loading: true,
                        reset: false,
                        success: {
                            ...state.material.get.success,
                            ok: false,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_MATERIAL_HANDLING_SUCCESS:
            return {
                ...state,
                material: {
                    ...state.material,
                    get: {
                        ...state.material.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.material.get.success,
                            ok: true,
                            data: action.payload,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_MATERIAL_HANDLING_FAILURE:
            return {
                ...state,
                material: {
                    ...state.material,
                    get: {
                        ...state.material.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.material.get.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: true,
                            message: action.payload.message,
                        },
                    },
                },
            };
        case GET_MATERIAL_HANDLING_RESET:
            return {
                ...state,
                material: {
                    ...state.material.get,
                    post: {
                        ...INITIAL_STATE.material.get,
                        reset: true,
                    },
                },
            }
    }
    return state;
}

export default materialHandlingReducers;