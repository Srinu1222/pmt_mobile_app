import {
    GET_VIEW_SPECIFICATION_LOADING,
    GET_VIEW_SPECIFICATION_SUCCESS,
    GET_VIEW_SPECIFICATION_FAILURE,
    GET_VIEW_SPECIFICATION_RESET
} from '../../actions/types';

const INITIAL_STATE = {
    viewspecifications: {
        get: {
            loading: false,
            reset: false,
            success: {
                ok: false,
                data: null,
            },
            failure: {
                error: false,
                message: '',
            },
        }
    }
};

const viewSpecificationReducers = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_VIEW_SPECIFICATION_LOADING:
            return {
                ...state,
                viewspecifications: {
                    ...state.viewspecifications,
                    get: {
                        ...state.viewspecifications.get,
                        loading: true,
                        reset: false,
                        success: {
                            ...state.viewspecifications.get.success,
                            ok: false,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_VIEW_SPECIFICATION_SUCCESS:
            return {
                ...state,
                viewspecifications: {
                    ...state.viewspecifications,
                    get: {
                        ...state.viewspecifications.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.viewspecifications.get.success,
                            ok: true,
                            data: action.payload,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_VIEW_SPECIFICATION_FAILURE:
            return {
                ...state,
                viewspecifications: {
                    ...state.viewspecifications,
                    get: {
                        ...state.viewspecifications.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.viewspecifications.get.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: true,
                            message: action.payload.message,
                        },
                    },
                },
            };
        case GET_VIEW_SPECIFICATION_FAILURE:
            return {
                ...state,
                viewspecifications: {
                    ...state.viewspecifications.get,
                    post: {
                        ...INITIAL_STATE.viewspecifications.get,
                        reset: true,
                    },
                },
            }
    }
    return state;
}

export default viewSpecificationReducers;