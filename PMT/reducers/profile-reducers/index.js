import {
    GET_PROFILE_LOADING,
    GET_PROFILE_SUCCESS,
    GET_PROFILE_FAILURE
} from '../../actions/types';

const INITIAL_STATE = {
    profile: {
        get: {
            loading: false,
            reset: false,
            success: {
                ok: false,
                data: null,
            },
            failure: {
                error: false,
                message: '',
            },
        }
    }
};

const companyProfileReducers = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_PROFILE_LOADING:
            return {
                ...state,
                profile: {
                    ...state.profile,
                    get: {
                        ...state.profile.get,
                        loading: true,
                        reset: false,
                        success: {
                            ...state.profile.get.success,
                            ok: false,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_PROFILE_SUCCESS:
            return {
                ...state,
                profile: {
                    ...state.profile,
                    get: {
                        ...state.profile.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.profile.get.success,
                            ok: true,
                            data: action.payload,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_PROFILE_FAILURE:
            return {
                ...state,
                profile: {
                    ...state.profile,
                    get: {
                        ...state.profile.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.profile.get.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: true,
                            message: action.payload.message,
                        },
                    },
                },
            };
    }
    return state;
}

export default companyProfileReducers;