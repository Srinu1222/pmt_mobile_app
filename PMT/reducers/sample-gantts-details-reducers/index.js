import {
    GET_SAMPLE_GANTT_DETAILS_LOADING,
    GET_SAMPLE_GANTT_DETAILS_SUCCESS,
    GET_SAMPLE_GANTTS_DETAILS_FAILURE,
    GET_SAMPLE_GANTTS_DETAILS_RESET
} from "../../actions/types";

const INITIAL_STATE = {
    sampleganttdetails: {
        get: {
            loading: false,
            reset: false,
            success: {
                ok: false,
                data: null,
            },
            failure: {
                error: false,
                message: "",
            },
        },
    },
};

const sampleGanttDetailsReducers = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_SAMPLE_GANTT_DETAILS_LOADING:
            return {
                ...state,
                sampleganttdetails: {
                    ...state.sampleganttdetails,
                    get: {
                        ...state.sampleganttdetails.get,
                        loading: true,
                        reset: false,
                        success: {
                            ...state.sampleganttdetails.get.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: false,
                            message: "",
                        },
                    },
                },
            };

        case GET_SAMPLE_GANTT_DETAILS_SUCCESS:
            return {
                ...state,
                sampleganttdetails: {
                    ...state.sampleganttdetails,
                    get: {
                        ...state.sampleganttdetails.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.sampleganttdetails.get.success,
                            ok: true,
                            data: action.payload,
                        },
                        failure: {
                            error: false,
                            message: "",
                        },
                    },
                },
            };

        case GET_SAMPLE_GANTTS_DETAILS_FAILURE:
            return {
                ...state,
                sampleganttdetails: {
                    ...state.sampleganttdetails,
                    get: {
                        ...state.sampleganttdetails.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.sampleganttdetails.get.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: true,
                            message: action.payload.message,
                        },
                    },
                },
            };
        case GET_SAMPLE_GANTTS_DETAILS_RESET:
            return {
                ...state,
                sampleganttdetails: {
                    ...state.sampleganttdetails,
                    get: {
                        ...INITIAL_STATE.sampleganttdetails.get,
                        reset: true
                    },
                },
            }
        default:
            return state;
    }
};

export default sampleGanttDetailsReducers;
