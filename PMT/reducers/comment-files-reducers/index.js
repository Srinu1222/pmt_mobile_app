import {
    GET_COMMENT_FILE_LOADING,
    GET_COMMENT_FILE_SUCCESS,
    GET_COMMENT_FILE_FAILURE,
    GET_COMMENT_FILE_RESET
} from '../../actions/types';

const INITIAL_STATE = {
    commentfiles: {
        get: {
            loading: false,
            reset: false,
            success: {
                ok: false,
                data: null,
            },
            failure: {
                error: false,
                message: '',
            },
        }
    }
};

const commentFileReducers = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_COMMENT_FILE_LOADING:
            return {
                ...state,
                commentfiles: {
                    ...state.commentfiles,
                    get: {
                        ...state.commentfiles.get,
                        loading: true,
                        reset: false,
                        success: {
                            ...state.commentfiles.get.success,
                            ok: false,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_COMMENT_FILE_SUCCESS:
            return {
                ...state,
                commentfiles: {
                    ...state.commentfiles,
                    get: {
                        ...state.commentfiles.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.commentfiles.get.success,
                            ok: true,
                            data: action.payload,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_COMMENT_FILE_FAILURE:
            return {
                ...state,
                commentfiles: {
                    ...state.commentfiles,
                    get: {
                        ...state.commentfiles.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.commentfiles.get.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: true,
                            message: action.payload.message,
                        },
                    },
                },
            };
        case GET_COMMENT_FILE_RESET:
            return {
                ...state,
                commentfiles: {
                    ...state.commentfiles.get,
                    post: {
                        ...INITIAL_STATE.commentfiles.get,
                        reset: true,
                    },
                },
            }
    }
    return state;
};

export default commentFileReducers;