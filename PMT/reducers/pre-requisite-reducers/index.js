import {
    GET_PREREQUISITS_LOADING,
    GET_PREREQUISITS_SUCCESS,
    GET_PREREQUISITS_FAILURE,
    GET_PREREQUISITS_RESET
} from '../../actions/types';

const INITIAL_STATE = {
    prerequisite: {
        get: {
            loading: false,
            reset: false,
            success: {
                ok: false,
                data: null,
            },
            failure: {
                error: false,
                message: '',
            },
        }
    }
};

const preRequsiteReducers = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_PREREQUISITS_LOADING:
            return {
                ...state,
                prerequisite: {
                    ...state.prerequisite,
                    get: {
                        ...state.prerequisite.get,
                        loading: true,
                        reset: false,
                        success: {
                            ...state.prerequisite.get.success,
                            ok: false,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_PREREQUISITS_SUCCESS:
            return {
                ...state,
                prerequisite: {
                    ...state.prerequisite,
                    get: {
                        ...state.prerequisite.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.prerequisite.get.success,
                            ok: true,
                            data: action.payload,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_PREREQUISITS_FAILURE:
            return {
                ...state,
                prerequisite: {
                    ...state.prerequisite,
                    get: {
                        ...state.prerequisite.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.prerequisite.get.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: true,
                            message: action.payload.message,
                        },
                    },
                },
            };
        case GET_PREREQUISITS_RESET:
            return {
                ...state,
                prerequisite: {
                    ...state.prerequisite.get,
                    post: {
                        ...INITIAL_STATE.prerequisite.get,
                        reset: true,
                    },
                },
            }
    }
    return state;
}

export default preRequsiteReducers;