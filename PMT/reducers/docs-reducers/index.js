import {
    GET_DOCS_LOADING,
    GET_DOCS_SUCCESS,
    GET_DOCS_FAILURE,
    GET_DOCS_RESET
} from '../../actions/types';

const INITIAL_STATE = {
    docs: {
        get: {
            loading: false,
            reset: false,
            success: {
                ok: false,
                data: null,
            },
            failure: {
                error: false,
                message: '',
            },
        }
    }
};

const docsReducers = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_DOCS_LOADING:
            return {
                ...state,
                docs: {
                    ...state.docs,
                    get: {
                        ...state.docs.get,
                        loading: true,
                        reset: false,
                        success: {
                            ...state.docs.get.success,
                            ok: false,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_DOCS_SUCCESS:
            return {
                ...state,
                docs: {
                    ...state.docs,
                    get: {
                        ...state.docs.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.docs.get.success,
                            ok: true,
                            data: action.payload,
                        },
                        failure: {
                            error: false,
                            message: '',
                        },
                    },
                },
            };
        case GET_DOCS_FAILURE:
            return {
                ...state,
                docs: {
                    ...state.docs,
                    get: {
                        ...state.docs.get,
                        loading: false,
                        reset: false,
                        success: {
                            ...state.docs.get.success,
                            ok: false,
                            data: null,
                        },
                        failure: {
                            error: true,
                            message: action.payload.message,
                        },
                    },
                },
            };
        case GET_DOCS_RESET:
            return {
                ...state,
                docs: {
                    ...state.docs.get,
                    get: {
                        ...INITIAL_STATE.docs.get,
                        reset: true,
                    },
                },
            }
    }
    return state;
}

export default docsReducers;    