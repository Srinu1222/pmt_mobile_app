export const LOGIN_USER_LOADING = 'login_user_loading'
export const LOGIN_USER_SUCCESS = 'login_user_success'
export const LOGIN_USER_FAILURE = 'login_user_failure'
export const LOGIN_USER_RESET = 'login_user_reset'
